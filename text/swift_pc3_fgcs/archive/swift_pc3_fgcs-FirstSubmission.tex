%%
%% This is file `elsarticle-template-num.tex',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% elsarticle.dtx  (with options: `numtemplate')
%% 
%% Copyright 2007, 2008 Elsevier Ltd.
%% 
%% This file is part of the 'Elsarticle Bundle'.
%% -------------------------------------------
%% 
%% It may be distributed under the conditions of the LaTeX Project Public
%% License, either version 1.2 of this license or (at your option) any
%% later version.  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.2 or later is part of all distributions of LaTeX
%% version 1999/12/01 or later.
%% 
%% The list of all files belonging to the 'Elsarticle Bundle' is
%% given in the file `manifest.txt'.
%% 

%% Template article for Elsevier's document class `elsarticle'
%% with numbered style bibliographic references
%% SP 2008/03/01

\documentclass[preprint,12pt]{elsarticle}

%% Use the option review to obtain double line spacing
%% \documentclass[authoryear,preprint,review,12pt]{elsarticle}

%% Use the options 1p,twocolumn; 3p; 3p,twocolumn; 5p; or 5p,twocolumn
%% for a journal layout:
%% \documentclass[final,1p,times]{elsarticle}
%% \documentclass[final,1p,times,twocolumn]{elsarticle}
%% \documentclass[final,3p,times]{elsarticle}
%% \documentclass[final,3p,times,twocolumn]{elsarticle}
%% \documentclass[final,5p,times]{elsarticle}
%% \documentclass[final,5p,times,twocolumn]{elsarticle}

%% if you use PostScript figures in your article
%% use the graphics package for simple commands
%% \usepackage{graphics}
%% or use the graphicx package for more complicated commands
%% \usepackage{graphicx}
%% or use the epsfig package if you prefer to use the old commands
%% \usepackage{epsfig}

%% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}
%% The amsthm package provides extended theorem environments
%% \usepackage{amsthm}

%% The lineno packages adds line numbers. Start line numbering with
%% \begin{linenumbers}, end it with \end{linenumbers}. Or switch it on
%% for the whole article with \linenumbers.
%% \usepackage{lineno}

\journal{Future Generation Computer Systems}

\begin{document}

\begin{frontmatter}

%% Title, authors and addresses

%% use the tnoteref command within \title for footnotes;
%% use the tnotetext command for theassociated footnote;
%% use the fnref command within \author or \address for footnotes;
%% use the fntext command for theassociated footnote;
%% use the corref command within \author for corresponding author footnotes;
%% use the cortext command for theassociated footnote;
%% use the ead command for the email address,
%% and the form \ead[url] for the home page:
%% \title{Title\tnoteref{label1}}
%% \tnotetext[label1]{}
%% \author{Name\corref{cor1}\fnref{label2}}
%% \ead{email address}
%% \ead[url]{home page}
%% \fntext[label2]{}
%% \cortext[cor1]{}
%% \address{Address\fnref{label3}}
%% \fntext[label3]{}

\title{Provenance Management in Swift}

%% use optional labels to link authors explicitly to addresses:
%% \author[label1,label2]{}
%% \address[label1]{}
%% \address[label2]{}

%\author{Ben Clifford}
%\author{Luiz M. R. Gadelha Jr.}
%\author{Marta Mattoso}
%\author{Michael Wilde}
%\author{Ian Foster}

\author[no]{Ben Clifford}
\ead{benc@hawaga.org.uk}
\author[coppe]{Luiz M. R. Gadelha Jr.}
\ead{gadelha@cos.ufrj.br}
\author[coppe]{Marta Mattoso}
\ead{marta@cos.ufrj.br}
\author[uc,anl]{Michael Wilde}
\ead{wilde@mcs.anl.gov}
\author[uc,anl]{Ian Foster}
\ead{foster@mcs.anl.gov}
\address[no]{No affiliation}
\address[coppe]{Federal University of Rio de Janeiro, Brazil}
\address[uc]{Computation Institute, University of Chicago, USA}
\address[anl]{Mathematics and Computer Science Division, Argonne National Laboratory, USA}

\begin{abstract}
The Swift parallel scripting language allows for the specification, execution and analysis of large-scale computations in parallel and distributed environments. The Swift system incorporates a data model for recording and querying provenance information. In this article we describe these capabilities and evaluate interoperability with other systems through the use of the Open Provenance Model. We describe Swift's provenance data model and compare it to the Open Provenance Model. We also describe activities performed within the Third Provenance Challenge which consisted of implementing a specific scientific workflow, performing provenance queries, and exchanging provenance information with other systems. Finally, we propose improvements to both the Open Provenance Model and Swift's provenance system. 
\end{abstract}

%\begin{keyword}
%data provenance \sep parallel scripting languages \sep scientific workflows
%% keywords here, in the form: keyword \sep keyword

%% PACS codes here, in the form: \PACS code \sep code

%% MSC codes here, in the form: \MSC code \sep code
%% or \MSC[2008] code \sep code (2000 is the default)

%\end{keyword}

\end{frontmatter}

%% \linenumbers

%% main text

\section{Introduction}

The automation of large scale computational scientific experiments can be accomplished through the use of workflow management systems, parallel scripting tools, and related systems that allow the definition of the activities, input and output data, and data dependencies of such experiments. The manual analysis of the data resulting from their execution is not feasible, due to the usually large amount of information. Provenance systems can be used to facilitate this task, since they gather details about the design and execution of these experiments, such as data artifacts consumed and produced by the activities. They allow the precise description of how the experiment was set up, and what happened during its execution. It also makes it easier to reproduce an experiment for the purpose of verification. 

The goal of the Open Provenance Model (OPM) \cite{MoFr08} is to standardize the representation of provenance information. OPM defines the entities {\em artifact}, {\em process}, and {\em agent} and the relations {\em used} (between an artifact and a process), {\em wasGeneratedBy} (between a process and an artifact), {\em wasControlledBy} (between an agent and a process), {\em wasTriggeredBy} (between two processes), and {\em wasDerivedFrom} (between two artifacts).

The Swift parallel scripting system \cite{swift} \cite{WiFo09} is a successor of the Virtual Data System (VDS) \cite{chimera} \cite{ZhWiFo06} \cite{ClFo08}. It allows the specification, management and execution of large-scale scientific workflows on parallel and distributed environments. The SwiftScript language is used for high-level specification of computations, it has features such as data types, data mappers, conditional and repetition flow controls, and sub-workflow composition. Its data model and type system is derived from XDTM \cite{xdtm}, which allows for the definition of abstract data types and objects without refering to their physical representation. If some dataset does not reside in main memory, its materialization is done through the use of data mappers. Procedures perform logical operations on input data, without modifying them. Swiftscript also allows procedures to be composed to define more complex computations. By analyzing the inputs and outputs of these procedures, the system determines data dependencies between them. This information is used to execute procedures that have no mutual data dependencies in parallel. It supports common execution managers for clustered systems and for grid environments, such as Falkon \cite{falkon}, which provides high job execution throughput. Swift logs a variety of information about each computation. This information can be exported to a relational database that uses a data model that is similar to OPM.

The objective of this paper is to present the local and remote provenance recording and analysis capabilities of Swift. In the sections that follow, we demonstrate the provenance capabilities of the Swift system and evaluate its interoperability with other systems through the use of OPM. We describe the provenance data model of the Swift system and compare it to OPM. We also describe activities performed within the Third Provenance Challenge which consisted of implementing a specific scientific workflow (LoadWorkflow), performing provenance queries, and exchanging provenance information with other systems.

\section{Data Model}

In Swift, data is represented by strongly-typed single-assignment variables. Data types can be {\em atomic} or {\em composite}. Atomic types are given by {\em primitive}, such as integers or strings, or {\em mapped} types. Mapped types are used for representing and accessing data stored in files. {\em Composite} types are given by structures and arrays. In the Swift runtime, data is represented by a {\em dataset handle}. Dataset handles are produced and consumed by Swift processes: invocations of external programs, and invocations of internal procedures, functions and operators. 


In the Swift provenance model, dataset handles and processes are recorded, as are the relations between them (either a process consuming a dataset handle as input, or a process producing a dataset handle as output). Each dataset handle and process is uniquely identified in time and space by a URI. This information is stored persistently in a relational database; we have also experimented with other database layouts \cite{ClGaMa09}.

The two key relational tables used to store the structure of the provenance graph are {\tt processes}, that stores brief information about processes (see Table 1), and {\tt dataset\_usage}, that stores produced and consumed relationships between processes and dataset handles (see Table 2). Other tables are used to record details about each process and dataset, and other relationships such as containment \cite{ClGaMa09}.

Consider this Swift program fragment, which first describes a procedure ({\tt sortProg}, which calls the executable {\tt sort}); then declares references to two files ({\tt f}, a reference to {\tt inputfile}, and {\tt g}, a reference to {\tt outputfile}); and finally calls the procedure {\tt sortProg}.


\footnotesize
\begin{verbatim}
  app (file o) sortProg(file i) {
    sort stdin=@i stdout=@o
  }
  file f <"inputfile">;
  file g <"outputfile">;
  g = sortProg(f);
\end{verbatim}
\normalsize

When this program is run, provenance records are generated as follows:

\begin{enumerate}
\item[(A)] An execution record is generated for the initial call to the {\tt sortProg(f)} procedure; 
\item[(B)] An execution record is generated for the {\tt @i} inside {\tt sortProg}, representing the evaluation of the {\tt @filename} function that Swift uses to determine the physical file name corresponding to the reference {\tt f}; 
\item[(C)] An execution record is generated for the {\tt @o} inside {\tt sortProg}, again representing the evaluation of the {\tt @filename} function, this time for the reference {\tt g}. 
\end{enumerate}

Dataset handles are recorded for:

\begin{enumerate}
\item[(Q)] the string {\tt "inputfile"}; 	
\item[(R)] the string {\tt "outputfile"}; 	
\item[(S)] the file variable {\tt f}; 	
\item[(T)] the file variable {\tt g}; 	
\item[(U)] the filename of {\tt i}; 	
\item[(V)] the filename of {\tt o}.	
\end{enumerate}

\begin{table}
\label{processes_table}
\begin{center} 
\caption{Database table {\tt processes}.}
\begin{tabular}{ | l | p{11cm} |  }
\hline	
  {\bf Row} & {\bf Definition}\\
\hline
  {\tt id}    & the URI identifying the process\\
\hline
  {\tt type} & the type of the process: execution, compound procedure, function, operator\\
\hline  
\end{tabular}
\end{center} 
\end{table}

\begin{table}
\begin{center} 
\caption{Database table {\tt dataset\_usage}.}
\begin{tabular}{ | l | p{9.8cm} |  }
\hline	
  {\bf Row} & {\bf Definition}\\
\hline
  {\tt process\_id} & a URI identifying the process end of the relationship\\
\hline
  {\tt dataset\_id} & a URI identifying the dataset handle end of  the relationship\\
\hline
  {\tt direction}   & whether the process is consuming or producing the dataset handle\\
\hline
  {\tt param\_name} & the parameter name of this relation\\
\hline
\end{tabular}
\end{center} 
\label{dataset_usage_table}
\end{table}

Input/output relations are recorded as:

\begin{itemize}
\item (A) takes (S) as an input;
\item (A) produces (T) as an output;
\item (B) takes (S) as an input;
\item (C) takes (T) as an input;
\item (B) produces (U) as an output.
\end{itemize}

One of the main concerns with using a relational model for representing provenance is the need for querying over the transitive relation expressed in the {\tt dataset\_usage} table. For example, after executing the fragment:


\footnotesize
\begin{verbatim}
  b = p(a);
  c = q(b);
\end{verbatim}
\normalsize


it might be desirable to find all dataset handles that lead to {\tt c}: that is, {\tt a} and {\tt b}. However simple SQL queries over the {\tt dataset\_usage} relation can only go back one step, leading to the answer {\tt b} but not to the answer {\tt a}. To address this problem, we generate a transitive closure table by an incremental evaluation system \cite{SQLTRANS}. This approach makes it straightforward to query over transitive relations using natural SQL syntax, at the expense of larger database size and longer import time.



The Swift provenance model is close to OPM, but there are some differences. Dataset handles correspond closely with OPM artifacts as immutable representations of data. However they do not correspond exactly. An OPM artifact has unique provenance. However, a dataset handle can have multiple provenance descriptions. For example, in this SwiftScript program:

\footnotesize
\begin{verbatim}
  int a = 7;
  int b = 10;
  int c[] = [a, b];
\end{verbatim}
\normalsize


the expression {\tt c[0]} evaluates to the dataset handle corresponding to the variable {\tt a}. That dataset handle has a provenance trace indicating it was assigned from the constant value {\tt 7}. However, that dataset handle now has additional provenance indicating that it was output by applying the array access operator {\tt []} to the array {\tt c} and the numerical value {\tt 0}.

In OPM, the artifact resulting from evaluating {\tt c[0]} is distinct from the artifact resulting from evaluating {\tt a}, although they may be annotated with an isIdenticalTo arc \cite{OPMcollections}.

\section{PC3 Queries}

The workflow selected for PC3 receives a set of CSV files containing astronomical data, stores the contents of these files in a relational database, and performs a series of validation steps on the database. This workflow makes extensive use of conditional and loop flow controls and database operations. A Java implementation of the component applications of the workflow was provided in the Provenance Challenge Wiki \cite{pc}, where our Swift implementation is also available. Swift has an application (local or remote) catalog where wrapper scripts that call these component applications were listed. Most of the inputs and outputs of the component applications are XML files,  so we defined a mapped variable type called {\tt xmlfile} for handling these files. Each workflow component application is declared in the SwiftScript program, such as:



\footnotesize
\begin{verbatim}
  (xmlfile output) ps_load_executable(xmlfile input, string s) {
    app {
      ps_load_executable_app @input s @output;
    }
  }
\end{verbatim}

\normalsize

Compound procedures that invoke other SwiftScript procedures instead of component programs, are also used in our implementation:

\footnotesize
\begin{verbatim}
  (boolean output) ps_load_executable_boolean(xmlfile input, string s) { 
    xmlfile xml_out = ps_load_executable(input, s); 
    output = extract_boolean(xml_out); 
  } 
\end{verbatim}
\normalsize

The procedural body of the SwiftScript code closely follows the LoadWorkflow specification since Swift has native support for decision and loop controls given, in this case, by the {\tt if} and {\tt foreach} constructs.

In our first attempt to implement LoadWorkflow in Swift, we found the use of the foreach loop problematic because the database routines are internal to the Java implementation and, therefore, Swift has no control over them. Since Swift tries to parallelize the {\tt foreach} iterations it ended up incorrectly parallelizing the database operations. It was necessary to serialize the execution of the workflow to avoid this problem. Most of the PC3 queries are for row-level database provenance. A workaround for gathering provenance about database oberations was implemented by modifying the application database so that for every row inserted or modified, an entry containing the execution identifier of the Swift process that performed the corresponding database operation is also inserted. 

{\em Core Query 1}. The first query asks, for a given application database row, which CSV files contributed to it. The strategy used to answer this query is to determine input CSV files that precede, in the transitivity table, the process that inserted the row. This query can be answered by first obtaining the Swift process identifier of the process that inserted the row from the annotations included in the application database. Finally, we query for filenames of datasets that contain CSV inputs in the set of predecessors of the process that inserted the row.

{\em Core Query 2}. The second query asks if the range check (IsMatchColumnRanges) was performed in a particular table, given that a user found values that were not expected in it. This is implemented with the following SQL query: 

\footnotesize
\begin{verbatim}
> select 
    dataset_values.value
  from
    processes, invocation_procedure_names, dataset_usage, dataset_values
  where
    type='compound' and
    procedure_name='is_match_table_column_ranges' and
    dataset_usage.direction='O' and
    dataset_usage.param_name='inputcontent' and
    processes.id = invocation_procedure_names.execute_id and
    dataset_usage.process_id = processes.id and
    dataset_usage.dataset_id = dataset_values.dataset_id;
\end{verbatim}
\normalsize

This returns the input parameter XML for all IsMatchColumnRanges calls. These are XML values, and it is necessary to examine the resulting XML to determine if it was invoked for the specific table. There is unpleasant cross-format joining necessary here to get an actual yes/no result properly, although we probably could use a {\tt LIKE} clause to examine the value.

{\em Core Query 3}. The third core query asks which operation executions were strictly necessary for the Image table to contain a particular (non-computed) value. This uses the additional annotations made, that only store which process originally inserted a row, not which processes have modified a row. So to some extent, rows are regarded a bit like artifacts (though not first order artifacts in the provenance database); and we can only answer questions about the provenance of rows, not the individual fields within those rows. That is sufficient for this query, though. First find the row that contains the interesting value and extract its {\tt IMAGEID}. Then find the process that created the {\tt IMAGEID} by querying the Derby database table {\tt P2IMAGEPROV}. This gives the process ID for the process that created the row. Now query the transitive closure table for all predecessors for that process (as in the first core query). This will produce all processes and artifacts that preceeded this row creation. Our answer differs from the sample answer because we have sequenced access to the database, rather than regarding each row as a proper first-order artifact. The entire database state at a particular time is a successor to all previous database accessing operations, so any process which led to any database access before the row in question is regarded as a necessary operation. This is undesirable in some respects, but desirable in others. For example, a row insert only works because previous database operations which inserted other rows did not insert a conflicting primary key - so there is data dependency between the different operations even though they operate on different rows. 

{\em Optional Query 1}. The computation halts due to failing an IsMatchTable-ColumnRanges check. How many tables were loaded successfully before the computation halted due to the failed check? The answer was given by querying how many load processes are known to the database (over all recorded computation), which can be restricted to a particular computation.

{\em Optional Query 2}. Which pairs of procedures in the workflow could be swapped and the same result still be obtained (given the particular data input)? In our Swift representation of the workflow, we control dataflow dependencies. So many activities that could be commuted are in our implementation run in parallel. One significant thing one cannot describe in SwiftScript (and so cannot answer from the provenance database using this method) is commuting operations on the database. From a Swift perspective, this is a limitation of our SwiftScript language rather than in the provenance implementation. The query lists which pairs Unix process executions (of which there are 50x50) have no data dependencies on each other. There are 2082 rows. This answer is deficient in a few ways. We do not take into account non-execute procedures (such as compound procedures, function invocations, and operator executions) - there are 253 processes in total, 50 being executes and the remainder being the other kinds of process. If we did that naively, we would not take into account compound procedures which contain other procedures (due to lack of support for nested processes - something like OPM accounts) and would come up with commutations which do not make sense.

\section{Provenance Challenge 3 and OPM}

One major goal of PC3 was to evaluate OPM. To this end, each team was asked to export the provenance data generated for LoadWorkflow in the OPM format, to import the OPM graph generated by the other teams, and to perform the proposed queries on the imported data. The OPM output for the LoadWorkflow run in Swift is available at the web page. Since OPM and the Swift provenance database use similar data models, it is fairly straightforward to build a tool to import data from an OPM graph into the Swift provenace database. However we observed that the OPM outputs from the various participating teams, including Swift, carry many details of the LoadWorkflow implementation that are system specific, such as auxiliary tasks that are not specifically related to the workflow. To answer the same queries it would be necessary to perform some manual interpretation of the imported OPM graph in order to identify the relevant processes and artifacts (datasets). 

In order to address the divergence between OPM and Swift provenance database data models the dataset handle implementation could be modified so that it supported dataset handles being aliases to other dataset handles, and so that any provenance creating aliasing behavior made such alias dataset handles instead of returning the original dataset handle. The alias dataset handle would behave identically to the dataset handle that it aliases, except that it would have different provenance reflecting both the provenance of the original dataset handle, and subsequent operations made to retrieve it. In the above example, then, {\tt c[0]} would return a newly created dataset handle that aliased the original dataset handle for {\tt a}.

The present Swift implementation of provenance uses a relational database. A number of other forms were briefly experimented with during development. The two most developed and interesting models were XML and Prolog. XML provides a semi-structured tree form for data. A benefit of this approach is that new data can be added to the database without needing an explicit schema to be known to the database. In addition, when used with a query language such as Xpath, certain transitive queries become straightforward with the use of the {\tt //} operator of Xpath which has specific benefits to provenance queries. Representing the data as Prolog tuples is a different representation than a traditional database, but provides a query interface that can express interesting queries flexibly.

{\em Naming}. OPM does not specify a naming mechanism for globally identifying artifacts outside of an OPM graph. Dataset handles are given a URI. That fits in with a proposed OPM modification to use a Dublin Core identifier to identify artifacts \cite{pc}. 

{\em Collections}. The Swift provenance implemenation has two models of representing containment for dataset handles contained inside other dataset handles (arrays and complex types):


\begin{enumerate}
\item constructor/accessor model: in this model, there are special processes called accessors and constructors corresponding to the {\tt []} array accessor and {\tt [1,2,3]} explicit construction syntax in SwiftScript. This model is proposed in OPM. In the Swift implementation, this is a cause of multiple provenances for dataset handles as discussed in the alias section elsewhere;
\item container/contained model: relations are stored directly between dataset handles indicating that one is contained inside the other, without intervening processes. These relations can always be inferred from the constructor/accessor model. 
\end{enumerate}

{\em Other OPM proposals}. The Swift entry made a minor proposal to change the XML schema to better reflect the perceived intentions of the OPM authors \cite{pc}. It was apparent that the present representation of hierarchical processes in OPM is insufficiently rich for some groups and that it would be useful to represent hierarchy of individual processes and their containing processes more directly. An OPM modification proposal for this is forthcoming. In Swift, this information is often available through the karajan thread ID which closely maps to the Swift process execution hierarchy: a Swift process contains another Swift process if its Karajan thread ID is a prefix of the second processes Karajan thread ID. The Swift provenance database stores values of artifacts/dataset handles when those values exist in-core (for example, when a dataset handle represents and integer or a string). There was some desire in the PC3 workshop for a standard way to represent this, and a modification proposal may be forthcoming.

\section{Concluding Remarks}

Swift was able to perform the activities proposed for the Third Provenance Challenge. This success illustrates its capabilities to support provenance collection and analysis. Its provenance model is close to OPM, which enables interoperability with other provenance systems. One important aspect of Swift is its support for scalable execution of large-scale computations on parallel and distributed environments, along with the collection of provenance information. Scalability issues of provenance systems were not yet explored in the Provenance Challenge series.

Swift development continues with the objective of improving its provenance capabilities and future work will concentrate on some of the following aspects:


{\em Provenance system scalability}. The present Swift provenance tracking model results in the generation and storage of large amounts of data. For smaller computations, such as LoadWorkflow, this is not a problem, but for larger computations (which is precisely the sort of computations for which Swift is particularly well suited) the provenance can become extremely large, and provenance queries can take a long time to execute. It may be desirable to provide options that can allow the programmer to request that Swift store less data albeit (presumably) with reduced accuracy. It may also be interesting to use distributed data management techniques to enable better scalability.

{\em Provenance query system}. It was clear from PC3 that although it is possible to express the provenance queries in SQL it is not always practical to do so, due to its poor transitivity support. One future objective is to make the provenance query system, which should include a specialized provenance query language, capable of being readily queried by scientists to let them do better science through validation, collaboration, and discovery. 



\bibliographystyle{plain}
%\bibliography{ref}

\begin{thebibliography}{10}

\bibitem{pc}
{Provenance Challenge Wiki}.
\newblock http://twiki.ipaw.info, 2009.

\bibitem{OPMcollections}
{Support for Collections}.
\newblock Proposal distributed during the Third Provenance Challenge, 2009.

\bibitem{ClFo08}
B.~Clifford, I.~Foster, J.~Voeckler, M.~Wilde, and Y.~Zhao.
\newblock Tracking provenance in a virtual data grid.
\newblock {\em Concurrency and Computation: Practice and Experience},
  20(5):565--575, 2008.

\bibitem{ClGaMa09}
B.~Clifford, L.~Gadelha, M.~Mattoso, M.~Wilde, and I.~Foster.
\newblock {Tracking Provenance in Swift}.
\newblock Technical Report ANL/MCS-TM-311, Argonne National Laboratory, 2009.

\bibitem{SQLTRANS}
G.~Dong, L.~Libkin, J.~Su, and L.~Wong.
\newblock {Maintaining Transitive Closure of Graphs in SQL}.
\newblock {\em International Journal of Information Technology}, 5, 1999.

\bibitem{chimera}
I.~Foster, J.~Vockler, M.~Wilde, and Y.~Zhao.
\newblock {Chimera: A Virtual Data System for Representing, Querying and
  Automating Data Derivation}.
\newblock In {\em Proc. 14th International Conference on Scientific and
  Statistical Database Management (SSDBM'02)}, pages 37--46, 2002.

\bibitem{MoFr08}
L.~Moreau, J.~Freire, J.~Futrelle, R.~McGrath, J.~Myers, and P.~Paulson.
\newblock {The Open Provenance Model: An Overview}.
\newblock In {\em Provenance and Annotation of Data and Processes (IPAW 2008)},
  volume 5272 of {\em LNCS}, pages 323--326. Springer, 2008.

\bibitem{xdtm}
L.~Moreau, Y.~Zhao, I.~Foster, J.~Voeckler, and M.~Wilde.
\newblock {XDTM: XML Dataset Typing and Mapping for Specifying Datasets}.
\newblock European Grid Conference (EGC 2005), 2005.

\bibitem{falkon}
I.~Raicu, Y.~Zhao, C.~Dumitrescu, I.~Foster, and M.~Wilde.
\newblock {Falkon: A Fast and Lightweight Task Execution Framework}.
\newblock In {\em Proc. ACM/IEEE Conference on High Performance Networking and
  Computing (Supercomputing 2007)}, 2007.

\bibitem{WiFo09}
M.~Wilde, I.~Foster, K.~Iskra, P.~Beckman, A.~Espinosa, M.~Hategan,
  B.~Clifford, and I.~Raicu.
\newblock {Parallel Scripting for Applications at the Petascale and Beyond}.
\newblock {\em IEEE Computer}, 42(11):50--60, November 2009.

\bibitem{swift}
Y.~Zhao, M.~Hategan, B.~Clifford, I.~Foster, G.~Laszewski, I.~Raicu,
  T.~Stef-Praun, and M.~Wilde.
\newblock {Swift: Fast, Reliable, Loosely Coupled Parallel Computation}.
\newblock In {\em Proc. 1st IEEE International Workshop on Scientific Workflows
  (SWF 2007)}, pages 199--206, 2007.

\bibitem{ZhWiFo06}
Y.~Zhao, M.~Wilde, and I.~Foster.
\newblock {Applying the Virtual Data Provenance Model}.
\newblock In {\em International Provenance and Annotation Workshop (IPAW
  2006)}, volume 4145 of {\em LNCS}, pages 148--161. Springer, 2006.

\end{thebibliography}




\end{document}
\endinput
