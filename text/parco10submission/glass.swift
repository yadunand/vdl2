type Text;
type Arc;
type Restart;
type Log;
type Active;

type GlassIn{
    Restart startfile;
    Active activefile;
}

type GlassOut{
    Arc arcfile;
    Active activefile;
    Restart restartfile;
    Restart startfile;
    Restart final;
    Log logfile;
}

app (GlassOut o) glassCavityRun
    (GlassIn i, string rad, string temp, string steps, string volume, string fraca,
     string energyfunction, string centerstring, string arctimestring)
{   glassRun
      "-a" @filename(o.final) "--lf" @filename(i.startfile) stdout=@filename(o.logfile)
      "--temp" temp "--stepsperparticle" steps "--energy_function" energyfunction
      "--volume" volume "--fraca" fraca
      "--cradius" rad "--ccoord" centerstring arctimestring;
}

GlassRun()
{
  string temp=@arg("temp","2.0");
  string steps=@arg("steps","10");
  string esteps=@arg("esteps","100");
  string ceqsteps=@arg("ceqsteps","100");
  string natoms=@arg("natoms","200");
  string volume=@arg("volume","200");
  string rlist=@arg("rlist","rlist");
  string clist=@arg("clist","clist");
  string fraca=@arg("fraca","0.5");
  string radii[] = readData(rlist);
  string centers[] = readData(clist);
  int nmodels=@toint( @arg("n","1") );
  int nsub=@toint( @arg("nsub","1") );
  string savearc=@arg("savearc","FALSE");
  string arctimestring;
  string energyfunction=@arg("energyfunction","softsphereratiosmooth");

  if(savearc=="FALSE") {
    arctimestring="--arc_time=10000000";
  }
  else{
    arctimestring="";
  }

  GlassIn modelIn[][][] <ext; exec="GlassCavityOutArray.map",
    rlist=rlist, clist=clist, steps=ceqsteps, n=nmodels, esteps=esteps, temp=temp,
    volume=volume, e=energyfunction, natoms=natoms, i="true">;

  GlassOut modelOut[][][][] <ext; exec="GlassCavityContinueOutArray.map",
    n=nmodels, nsub=nsub, rlist=rlist, clist=clist, ceqsteps=ceqsteps, esteps=esteps,
    steps=steps, temp=temp, volume=volume, e=energyfunction, natoms=natoms>;

  foreach rad,rindex in radii {
    foreach centerstring,cindex in centers {
      foreach model in [0:nmodels-1] {
        foreach job in [0:nsub-1] {
          if( !(@filename(modelOut[rindex][cindex][model][job].final)=="NULL") ) {
            modelOut[rindex][cindex][model][job] = glassCavityRun(
              modelIn[rindex][cindex][model], rad, temp, steps, volume, fraca,
              energyfunction, centerstring, arctimestring);
          }
        }
      }
    }
  }
}

GlassRun();