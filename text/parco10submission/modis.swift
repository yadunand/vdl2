type file;
type MODIS; type image;
type landuse;

# Define application program interfaces

app (landuse output) getLandUse (imagefile input, int sortfield)
{
  getlanduse @input sortfield stdout=@output;
}

app (file output, file tilelist) analyzeLandUse
    (MODIS input[], string usetype, int maxnum)
{
  analyzelanduse @output @tilelist usetype maxnum @filenames(input);
}

app (image output) colorMODIS (MODIS input)
{
  colormodis @input @output;
}

app (image output) assemble
    (file selected, image img[], string webdir)
{
  assemble @output @selected @filename(img[0]) webdir;
}

app (image grid) markMap (file tilelist) 
{
  markmap @tilelist @grid;
}

# Constants and command line arguments

int nFiles =      @toint(@arg("nfiles","1000"));
int nSelect =     @toint(@arg("nselect","12"));
string landType = @arg("landtype","urban");
string runID =    @arg("runid","modis-run");
string MODISdir=  @arg("modisdir","/home/wilde/bigdata/data/modis/2002");
string webDir =   @arg("webdir","/home/wilde/public_html/geo/");



# Input Dataset

image geos[] <ext; exec="modis.mapper",
  location=MODISdir, suffix=".tif", n=nFiles >;

# Compute the land use summary of each MODIS tile

landuse land[] <structured_regexp_mapper; source=geos, match="(h..v..)",
  transform=@strcat(runID,"/\\1.landuse.byfreq")>;

foreach g,i in geos {
    land[i] = getLandUse(g,1);
}

# Find the top N tiles (by total area of selected landuse types)

file topSelected <"topselected.txt">;
file selectedTiles <"selectedtiles.txt">;
(topSelected, selectedTiles) = analyzeLandUse(land, landType, nSelect);

# Mark the top N tiles on a sinusoidal gridded map

image gridMap <"markedGrid.gif">;
gridMap = markMap(topSelected);

# Create multi-color images for all tiles

image colorImage[] <structured_regexp_mapper;
          source=geos, match="(h..v..)",
          transform="landuse/\\1.color.png">;

foreach g, i in geos {
  colorImage[i] = colorMODIS(g);
}

# Assemble a montage of the top selected areas

image montage <single_file_mapper; file=@strcat(runID,"/","map.png") >; # @arg
montage = assemble(selectedTiles,colorImage,webDir);