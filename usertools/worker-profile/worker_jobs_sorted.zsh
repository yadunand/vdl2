#!/bin/zsh

# Lists worker job counts, sorted for plotting
# usage: worker_jobs_sorted.zsh <RESULT> <LOGS>
# The result is a tabular file compatible with plotter/lines.zsh
# Note that the output contains comments, this is OK for lines.zsh
# Output: <NUMBER> <JOB COUNT> # <ID> <LOG>

if [[ ${#*} == 0 ]]
then
  print "Lists worker job counts, sorted for plotting"
  print "usage: worker_jobs_spectrum.zsh <RESULT> <LOGS>"
  return 1
fi

PLOTS=$( dirname $0 )
TOOLS=${PLOTS}

source ${TOOLS}/helpers.zsh
[[ $? != 0 ]] && print "Could not source helpers.zsh!" && exit 1

source ${PLOTS}/worker_jobs_lib.zsh

RESULT=$1
shift
LOGS=${*}

# Output: <index> <jobs> <ID> <logfile>
worker_jobs ${LOGS} | sort -k 2 | nl -w1 | \
  awk '{ print $1 " " $3 " # " $2 " " $4 }' > ${RESULT}

return 0
