#!/bin/zsh

# Extract the PROFILE: lines from the end of the worker log
# Requires you to set $PROFILE=1 in worker.pl

PLOTS=$( dirname $0 )
TOOLS=${PLOTS}

source ${TOOLS}/helpers.zsh
[[ $? != 0 ]] && print "Could not source helpers.zsh!" && exit 1

LOG=$1
PROFILE=$2

checkvars LOG PROFILE

grep "PROFILE:" ${LOG} | awk '{ print $5 " " $6 " " $7 }' > ${PROFILE}

exit 0
