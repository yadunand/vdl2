#!/bin/zsh

# Lists worker job counts
# Uses worker_jobs_lib.zsh
# usage: worker_jobs.zsh <RESULT> <LOGS>

if [[ ${#*} == 0 ]]
then
  print "Lists worker job counts"
  print "usage: worker_jobs.zsh <RESULT> <LOGS>"
fi

RESULT=$1
shift

PLOTS=$( dirname $0 )
TOOLS=${PLOTS}

source ${TOOLS}/helpers.zsh
[[ $? != 0 ]] && print "Could not source helpers.zsh!" && exit 1

source ${PLOTS}/worker_jobs_lib.zsh

worker_jobs ${*} > ${RESULT}

return 0
