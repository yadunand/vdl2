
To use these tools, you have to turn on $PROFILE=1
in provider-coaster/resources/worker.pl

These tools generate tabular output data that may be
plotted with the vdl2/usertools/plotter

User tools are described below:

worker_jobs.zsh:

For each worker, obtain the number of jobs run.
Useful to sanity check what happened and get a sense
of load balance.

worker_jobs_sorted.zsh:

Sorted output for the above.

Example usage:

 ~/swift_tools/worker-profile> ./worker_jobs_sorted.zsh out.data ~/worker.log

 ~/swift_tools/worker-profile> ../plotter/lines.zsh jobs-meta.cfg tmp.eps out.data
PLOTTED: tmp.eps

worker_profile_util.zsh:

Extracts the load over time for a given worker log.
Example output is at:
http://www.ci.uchicago.edu/wiki/bin/view/SWFT/WorkerProfile

Example usage:

 ~/swift_tools/worker-profile> ./worker_profile_util.zsh ~/worker.log out.data 1
USAGE: 143.412
TOTAL: 39.985
UTIL:  3.587

~/swift_tools/worker-profile> ../plotter/lines.zsh load-meta.cfg tmp.eps out.data
PLOTTED: tmp.eps
