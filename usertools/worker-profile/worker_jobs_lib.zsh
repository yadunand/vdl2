
# Lists worker job counts from all given worker logs
# Should just require log level INFO
# 3-column output: <ID> <JOB COUNT> <LOG>
worker_jobs()
{
  if [[ ${#*} == 0 ]]
  then
    print "Lists worker job counts"
    print "usage: worker_jobs <LOGS>"
  fi

  LOGS=${*}
  for LOG in ${LOGS}
  do
    ID=$( sed -n '/.*ID.*/{s/.*ID=\(.*\)/\1/;p;q}' ${LOG} )
    JOBS=$( sed -n '/.*Ran a total.*/{s/.*of \(.*\) jobs/\1/;p;q}' ${LOG} )
    print ${ID} ${JOBS} ${LOG}
  done

  return 0
}
