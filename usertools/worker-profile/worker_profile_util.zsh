#!/bin/zsh

# WORKER PROFILE UTILIZATION
# Build a data file for plotting from a worker.pl profile log
# 2-column output format: <time from 0> <load>
# usage: worker_profile_util.zsh <LOG> <OUTPUT> <GRANULARITY>?
# Output is compatible with plotter/lines.zsh

LOG=$1
DATA=$2
# If given, set the output data time granularity in seconds
#          (default 0.1)
GRANULARITY=${3:-0.1}

PLOTS=$( dirname $0 )
TOOLS=${PLOTS}

source ${TOOLS}/helpers.zsh
[[ $? != 0 ]] && print "Could not source helpers.zsh!" && exit 1

CORES=1

checkvars LOG DATA

PROFILE=${LOG}.ptmp

${PLOTS}/worker_profile_extract.zsh ${LOG} ${PROFILE}

EVENTS=( $( < ${PROFILE} ) )

local -F 3 LOAD=0

local -F 3 TIMESTAMP TIME_PREV USAGE
local -F 3 START=${EVENTS[3]}
local -F 3 TIME=${START}
TIME_PREV=${TIME}

data()
{
  local -F 5 T=$1
  local LOAD=$2
  local COMMENT=$3
  print ${T} ${LOAD} ${COMMENT} >> ${DATA}
}

eventFORK()
{
  (( LOAD++ ))
}

eventTERM()
{
  (( LOAD-- ))
}

eventSTOP()
{
  RUNNING=0
}

# Truncate DATA
printf "" > ${DATA}

data $(( TIME-START )) ${LOAD}

i=4
RUNNING=1
while (( RUNNING ))
 do
 EVENT=${EVENTS[i]}
 PID=${EVENTS[i+1]}
 TIMESTAMP=${EVENTS[i+2]}

 while (( TIME < TIMESTAMP ))
  do
  data $(( TIME-START )) ${LOAD}
  (( TIME+=GRANULARITY ))
 done

 USAGE+=$(( (TIMESTAMP-TIME_PREV) * LOAD ))
 event${EVENT}

 data $(( TIMESTAMP-START )) ${LOAD} "#"

 TIME_PREV=${TIMESTAMP}
 (( i+=3 ))
done

typeset -F 3 TOTAL=$(( (TIMESTAMP-START) * CORES ))
typeset -F 3 UTIL=$(( USAGE/TOTAL ))

print "USAGE: ${USAGE}"
print "TOTAL: ${TOTAL}"
print "UTIL:  ${UTIL}"

exit 0
