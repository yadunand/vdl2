#!/bin/bash

# Script      : swift_falkon
# Author      : Allan Espinosa
# Email       : aespinosa@cs.uchicago.edu
# Description : Checks for Falkon services and generates sites.xml if
#               necessary

print_usage(){
  cat << EOF
Usage     : swift_falkon [falkon_id] [cores] [swift_args]
Arguments : [falkon_id]  - the Falkon job id of the reservation
            [cores]      - the number of cores you requested on the allocation
            [swift_args] - Regular arguments to Swift. At least the workflow
                           script should be supplied.
EOF
}

# function gen_sites
#   Generates sites.xml file from the resources.  This also prepares the Swift
#   working directory.
#   Args:  falkon_id cores  

gen_sites() {
  falkon_id=$1
  cores=$2
  sitefile=$3

  service_dir=$FALKON_HOME/users/$USER/$falkon_id/service
  status_file=$FALKON_HOME/users/$USER/$falkon_id/status

  service_units=$(( $cores / 256 ))
  if (( cores % 256 != 0 )); then
    service_units=$(($service_units + 1))
  fi
  logger_info "Confirming registration of $service_units services"
  while [ `cat $status_file` == "fservice_submitted" ]  || \
      [ ! -f $service_dir/0 ]; do
    logger_info "Waiting for services to be available"
    sleep 10
  done

  num_lines=`ls $service_dir | wc -l`
  for (( i = num_lines; i < service_units; i = num_lines )); do
    sleep 10
    num_lines=`ls $service_dir | wc -l`
  done
  logger_info "Found at least $service_units services"

  logger_info "Generating sites.xml for $cores cores"
  echo "<config>" > $sitefile
  for i in $service_dir/*; do
    service_ip=`cut -f 1 -d \  $i`
    service_port=`cut -f 2 -d \  $i`
    cat >> $sitefile << EOF
  <pool handle="FSERVICE-`basename $i`">
    <gridftp  url="local://localhost"/>
    <execution provider="deef" url="http://$service_ip:$service_port/wsrf/services/GenericPortal/core/WS/GPFactoryService"/>
    <workdirectory >$WORK/swift-runs</workdirectory>
  </pool>
EOF
  done
  echo "</config>" >> $sitefile
  
  if [ ! -d $WORK/swift-runs ]; then
    mkdir -p $WORK/swift-runs
  fi
}

# function update_tcdata
#   Replaces the "localhost" entries in tc.data with the appropriate ones
#   based on the falkon_id
#   Args: falkon_id tcfile

update_tcdata(){
  falkon_id=$1
  tcfile=$2
  
  service_dir=$FALKON_HOME/users/$USER/$falkon_id/service
  tmpfile=`mktemp`

  logger_info "Updating tc.data"

  for i in $service_dir/*; do
    echo $i
    cat $tcfile | sed s/localhost/FSERVICE-`basename $i`/ >> $tmpfile
  done

  cat $tmpfile > $tcfile
}

# Logging options
#

LOG4SH_CONFIGURATION='none' . $FALKON_HOME/patch/libexec/log4sh
log4sh_resetConfiguration
logger_addAppender stdout
logger_setLevel DEBUG
appender_setLayout stdout PatternLayout
appender_setPattern stdout '%-04r [%8p]:  %m%n'

SWIFTROOT=/work/01035/tg802895/swift
SITEFILE=./sites.xml
TCFILE=./tc.data

if [ $# -le 2 ] ; then
  print_usage
  exit 1
fi

FALKON_ID=$1
CORES=$2
shift 2

gen_sites $FALKON_ID $CORES $SITEFILE

if [ ! -f $TCFILE ]; then
  logger_warn "tc.data not found in current working directory.  Getting a copy of Swift defaults"
  cat $SWIFTROOT/etc/tc.data > tc.data
fi

update_tcdata $FALKON_ID $TCFILE

$SWIFTROOT/bin/swift -sites.file $SITEFILE -tc.file $TCFILE $@
