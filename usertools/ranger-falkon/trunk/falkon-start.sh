#!/bin/sh

# Script      : falkon-start.sh
# Author      : Allan Espinosa
# Email       : aespinosa@cs.uchicago.edu
# Description : Utility to reserve a Falkon resource.  It setups the Swift
#               sites.xml file to be able to use the resources.

print_usage() {
  cat << EOF
Usage   : falkon-start.sh [options]
Options : -q QUEUE_NAME - LRM queue to submit too. Default is
                          "development"
          -n NODES      - Number of nodes
          -t WALL_TIME  - Reservation time in minutes. Defaults to 60
                          minutes.
          -A PROJECT    - Your Teragrid project charge account
          -w RATIO      - Number of workers in a Falkon service. Defaults to
                          256.
EOF
}

# function pool_resource
#    Submits to the LRM a request of $CORES worth of Falkon resources
# Args: [cores] [walltime] [queue] [falkon_id] [project]
pool_resource() {
  cores=$1
  walltime=$2
  queue=$3
  falkon_id=$4
  project=$5
  ratio=$6


  service_dir=$FALKON_HOME/users/$USER/$falkon_id/service
  service_units=$(( $cores / $ratio ))
  if (( cores % ratio != 0 )); then
    service_units=$(($service_units + 1))
  fi
  submit_script=`which falkon-provision.sh`
  qsub -A $project -q $queue -l h_rt=$walltime -pe 16way $cores \
      $submit_script $falkon_id $ratio

}



# Logging options
LOG4SH_CONFIGURATION='none' . $FALKON_HOME/patch/libexec/log4sh
log4sh_resetConfiguration
logger_addAppender stdout
logger_setLevel DEBUG
appender_setLayout stdout PatternLayout
appender_setPattern stdout '%-04r [%8p]:  %m%n'

# Process command and options
QUEUE="development"
CORES=16
TIME="01:00:00"
PROJECT="TG-CCR080022N"
RATIO=256

if [ -z $FALKON_HOME ]; then
  logger_error "Environment variable FALKON_HOME not defined.  Please\    
      set it by doing a \"source falkon.env\""
  print_usage 
  exit 1
fi

if [ $# -lt 2 ]; then
  logger_error "Args required"
  print_usage
  exit 1
fi

while getopts ":q:n:t:A:" opt; do
  case $opt in 
    q  )  QUEUE=$OPTARG
          ;;
    n  )  CORES=$OPTARG 
          ;;
    A  )  PROJECT=$OPTARG 
          ;;
    w  )  RATIO=$OPTARG
          ;;
    t  )  min=$OPTARG
          hrs=`printf %02d $((min / 60))`
          min=`printf %02d $((min % 60))`
          TIME="$hrs:$min:00"
          ;;
    \? )  print_usage
          exit 1
  esac
done
shift $(($OPTIND - 1))

# Setup Falkon run environment
FALKON_ID=`falkon-id-get.sh nil`
JOBDIR=$FALKON_HOME/users/$USER/$FALKON_ID
if [[ ! -d $JOBDIR ]]; then
  mkdir -p $JOBDIR/pool
  mkdir -p $JOBDIR/service
  mkdir -p $JOBDIR/workers
  mkdir -p $JOBDIR/swift
fi

logger_info "Falkon Job ID is $FALKON_ID"

pool_resource $CORES $TIME $QUEUE $FALKON_ID $PROJECT $RATIO
logger_info "Falkon worker for $CORES cores requested to LRM"
