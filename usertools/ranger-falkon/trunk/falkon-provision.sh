#!/bin/bash
#$ -V
#$ -cwd
#$ -N fresource
#$ -j y

export JAVA_HOME=/work/01035/tg802895/jdk1.6.0_03
export PATH=/share/home/01035/tg802895/local/bin:$JAVA_HOME/bin:$PATH
source /work/01035/tg802895/falkon/patch/falkon.env


FALKON_ID=$1
RATIO=$2
SERVICE_DIR=$FALKON_HOME/users/$USER/$FALKON_ID/service

if [ ! -d $WORK/.falkon ] ; then
  mkdir -p $WORK/.falkon
fi
SERVICE_SCRIPT=`mktemp $WORK/.falkon/tmp.XXXXX`
echo $SERVICE_SCRIPT
cat > $SERVICE_SCRIPT << EOF
#!/share/home/01035/tg802895/local/bin/mpi_ruby

require 'scanf'

rank = MPI::Comm::WORLD.rank
service_rank = rank / $RATIO

service_ip = nil
service_port = nil

if rank == service_rank
  ENV["GLOBUS_HOSTNAME"] = "`/bin/hostname -i`"
  portnum = 50001 + rank

  service_file = File.new("$SERVICE_DIR/#{rank}", "w")
  service_file.puts "#{ENV['GLOBUS_HOSTNAME']} #{portnum}"
  service_file.close

  system "falkon-service-stdout.sh #{portnum} $FALKON_HOME/patch/Falkon.config"
else
  system "sleep 30s"

  service_file = File.new("$SERVICE_DIR/#{service_rank}", "r")
  service_ip, service_port = service_file.scanf("%s %s")
  service_file.close

  system "falkon-worker.sh #{service_ip} #{service_port}"
end


EOF
chmod 755 $SERVICE_SCRIPT
ibrun $SERVICE_SCRIPT
