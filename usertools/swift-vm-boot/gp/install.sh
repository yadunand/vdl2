#!/bin/bash

tar xvfz globus-provision-0.3.2.tar.gz
cd globus-provision-0.3.2
python setup.py install --user
cd ..
ln -s ~/.local/bin
ln -s ~/.local/lib
