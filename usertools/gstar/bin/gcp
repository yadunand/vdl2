#!/usr/bin/env perl
#
# $Id: gcp,v 1.5 2005/04/27 20:05:25 griphyn Exp $
#
# gcp: a simple utility to copy a file based on its LFN
#

use strict;
use Getopt::Long;
use File::Basename;

use GriPhyN::RLS;

my $guc = "$ENV{'GLOBUS_LOCATION'}" . "/bin/globus-url-copy";
my $glc = "$ENV{'GLOBUS_LOCATION'}" . "/bin/globus-rls-cli";
my $gstar_env = "$ENV{'HOME'}/.gstar/env";
my $pwd_url = "file://$ENV{'PWD'}/";
my ($rc,@pfn_list,$verbose,$lfn,$dest,$full_dest);
my ($grid_dest,$grid_lfn);
my $RLS_SERVER;

#
# First check environment variables
#

if (!$ENV{'RLS_SERVER'}) {
  $RLS_SERVER="rls://evitable.uchicago.edu";
} else {
  $RLS_SERVER=$ENV{'RLS_SERVER'};
}

my $GRIDFTP_HOME = $ENV{'GRIDFTP_HOME'};

GetOptions( "rls=s"     => \$RLS_SERVER,
            "verbose|v" => \$verbose,
            'help|h'    => \&usage );

# If not set then fall back to G* dot-file
# (provided that it does exist...)

if (! $RLS_SERVER ) {
   if ( -f $gstar_env ) {
      if ($verbose) { print "-> using G* dot-files\n"; }
        open(GSTAR_ENV,"$gstar_env");
        while(<GSTAR_ENV>) {
          chop;
          $RLS_SERVER = $_;
          $RLS_SERVER =~ s/RLS_SERVER=//;
        }
      close(GSTAR_ENV);
    }
}

#
# Find our default GridFTP server URL
#

if (! $GRIDFTP_HOME ) {
  if ( -f $gstar_env ) {
    if ($verbose) { print "-> using G* dot-files\n"; }
       open(GSTAR_ENV,"$gstar_env");
        while(<GSTAR_ENV>) {
          chop;
          if ( $_ =~ m/^GRIDFTP_HOME=/ ) {
           $GRIDFTP_HOME = $_;
           $GRIDFTP_HOME =~ s/GRIDFTP_HOME=//;
          }
        }
      close(GSTAR_ENV);
   }
}

$lfn = shift;
$dest = shift;

if ( ! $lfn && ! $dest ) {
 &usage;
}

#
# fully qualify $lfn (if not a grid URI)
#

if ( $lfn =~ m/^\/\// ) {
 $grid_lfn = 1;  # a flag to say we are locating this file in RLS
 # remove leading '//'
 $lfn =~ s/^\/\//\//;
} else {
 $lfn = $ENV{'PWD'} . "/" . "$lfn";
}

if ( $dest =~ m/^\/\//) {
 $grid_dest = 1;   # a flag to say we are cataloging this file
 my $dest_lfn = $dest;
 
 # remove leading '//'
 $dest_lfn =~ s/^\/\//\//;

 # give me basename
 $dest_lfn = basename($dest_lfn);
 $lfn = "file://" . $lfn;

 system("$guc $lfn $GRIDFTP_HOME/$dest_lfn");
 if ($? != 0 ) {
  print "ERROR: Couldn't copy to remote GridFTP server\n";
  exit 255;
 }

 # remove leading '//' again...
 $dest =~ s/^\/\//\//;
 system("$glc create $dest ${GRIDFTP_HOME}/${dest_lfn} $RLS_SERVER");
 if ($? != 0 ) {
  print "ERROR: Couldn't register file in RLS ($RLS_SERVER)\n";
  exit 255;
 }
 exit 0;
}

#
# Fix the case where our destination is a directory....
#

if ( -d $dest && $grid_dest == 0 ) {
 $full_dest = $lfn;
 if ( $full_dest =~ m/^\// ) { 
     $full_dest =~ s/.*\///;
 }
 $dest = $dest . "/" . $full_dest;
}


#
# display usage information
#

sub usage {
    print << "EOF";
Usage: @{[basename($0)]} [-v] [-rls rls] LFN filename
EOF
   exit 0;
}

# Check to make sure we have both valid LFN and DEST
if ( ! $lfn && ! $dest )  { usage; }

#
# Sanity checks
#

die( "ERROR: You must specify a valid RLS server URI (-rls option)\n",
     "Hint: Use rls://your.rls.server or rlsn://your.rls.server\n" )
    unless $RLS_SERVER =~ m{^rlsn?://};

#
# Add a new mode of operation. Check for leading '//'. This is a GridPFN.
#
# See Newcastle Connection paper
# gcp //chicago/foo.img //dartmouth/test.img
#

# check to see if dest is an absolute path
if ( $dest =~ /^\//) {
 $pwd_url="file://";
}

@pfn_list = GriPhyN::RLS::get_pfn($RLS_SERVER,$lfn);

my $pfn_list_s = @pfn_list;

if ($verbose) { print "found: $pfn_list_s replicas\n"; }
if ("$?" == 0 ) {
  foreach my $i (@pfn_list) {
   if ($verbose) { print "$guc $i ${pwd_url}/${dest}\n"; }
   system("$guc $i ${pwd_url}/${dest}");
   if ("$?" == 0) { next; }
  }
} else {
 print "error: couldn't find LFN\n";
 exit 1;
}

exit 0;
