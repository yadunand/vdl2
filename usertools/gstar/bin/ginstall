#!/usr/bin/env perl
#
# ginstall: install a Pacman[1] package on Grid3 sites
# 
# Requires: 
#   - A registered Pacman cache and package [ -p ]
#   - Information GridCat [2] (site|gatekeeper|$app|$data) 
#
# Optional:
#   - An alternative location [ -l ]
#   - An alternative site [ -s ]
#
# [1] Pacman (http://physics.bu.edu/~youssef/pacman/)
# [2] GridCat (http://www.ivdgl.org/grid3/catalog/)
#
#
# This file or a portion of this file is licensed under the terms of
# the Globus Toolkit Public License, found in file GTPL, or at
# http://www.globus.org/toolkit/download/license.html. This notice must
# appear in redistributions of this file, with or without modification.
#
# Redistributions of this Software, with or without modification, must
# reproduce the GTPL in: (1) the Software, or (2) the Documentation or
# some other similar material which is provided with the Software (if
# any).
#
# Copyright 1999-2004 University of Chicago and The University of
# Southern California. All rights reserved.
#
# $Id: ginstall,v 1.6 2005/05/10 15:05:09 griphyn Exp $
#

use strict;
use File::Basename;
use Getopt::Long qw(:config bundling);
use POSIX qw(setpgid);
use GriPhyN::GridCat;

# Globus commands
my $gjr = $ENV{'GLOBUS_LOCATION'} . '/bin/globus-job-run';
my $gpi = $ENV{'GLOBUS_LOCATION'} . '/bin/grid-proxy-info';

$main::version = 'unknown';
$_ = '$Revision: 1.6 $';      # don't edit, automatically updated by CVS
$main::version=$1 if /Revision:\s+([0-9.]+)/o;

# Use Pacman from Dartmouth 
my $PacmanLocation = "http://grid.dartmouth.edu/pacman/pacman-2.121.tar.gz";
#my $PacmanLocation = "http://physics.bu.edu/~youssef/pacman/sample_cache/tarballs/pacman-2.121.tar.gz";

# ginstall: location
my ($location, $package, $install_site);

# GridCat variables
my ($app, $data, $site, $gk, $rc);

# Some defaults (change $VO)
my $DefaultJobManager = "jobmanager-fork";

#
# Who is my VO?
#

my $VO;
if ( $ENV{'GSTAR_VO_NAME'} ) {
   $VO = $ENV{'GSTAR_VO_NAME'};
 } else {
   $VO = "ivdgl";
 }

#
# Usage
#

sub usage(;$){
   my $rc = shift || 0;
   print <<EOF;
Usage: @{[basename($0)]}
    --help
    --package   : Name of Package to install (Cache:Package)
    --vo        : VO name (default is \$GSTAR_VO_NAME - Failing that, ivdgl)
    --version   : Display version
    --location  : Alternate install location 
    --site      : Specific site to install (default is all)

 The GridCat DB used is selected by the GSTAR_GRIDCAT_DB variable (grid3
 is the default).
EOF
   exit($rc);
}

#
# Version function
#

sub version(;$){
 my $rc = shift || 0;
 print "$main::version\n";
 exit($rc);
}



sub check_globus_setup {
#
# from clean-rls
#

die( "ERROR: Unable to execute $gjr\n",
     "Hint: Check your \$GLOBUS_LOCATION environment variable.\n" )
    unless -x $gjr;

die( "ERROR: Unable to execute $gpi\n",
     "Hint: Check your \$GLOBUS_LOCATION environment variable.\n" )
    unless -x $gpi;

#
# Check that our grid proxy is valid before anything else
#
my $timeleft = `$gpi -timeleft`;
chomp($timeleft);
$timeleft += 0;                 # make numeric (type cast)
die( "ERROR: Insufficient time ($timeleft s) on user grid proxy\n",
     "Hint: Use grid-proxy-init to (re) initialize your user proxy\n" )
    if $timeleft < 3600;
}

sub check_job_mgr_fork { 
 # 
 # Check to see if we can run a simple test job first
 #  - force this to be a local job (no stage of true) 
 # 

 $gk = shift;

 system("$gjr ${gk}/${DefaultJobManager} -l /bin/true > /dev/null 2>&1"); 
 if ("$?" != 0 ) { return 2;  } else { return 0; }
}


###########################################################################
#  START InstallPackage
###########################################################################

sub InstallPackage {
my $InstallLocation;

if ($location) {
  $InstallLocation = "${app}/${VO}/$location";
} else {
  $InstallLocation = "${app}/${VO}";
}

open(OUTPUT,">submit_$site.sh");
printf OUTPUT <<EOF;
#!/bin/sh
#
# Install script created by @{[(basename $0)]}
# 

INSTALL_ROOT=$InstallLocation

# Check to see if VO directory exists
[ ! -d \${INSTALL_ROOT} ] && /bin/mkdir \${INSTALL_ROOT}

cd \${INSTALL_ROOT}
if [ "\$?" -ne 0 ]; then
  echo "ERROR: Could not switch to \$APP dir!"
  exit 255
fi

# Do some setup if the VO dir is 'uninitialized'
[ ! -d \${INSTALL_ROOT}/bin ] && /bin/mkdir \${INSTALL_ROOT}/bin
[ ! -d \${INSTALL_ROOT}/lib ] && /bin/mkdir \${INSTALL_ROOT}/lib
[ ! -d \${INSTALL_ROOT}/etc ] && /bin/mkdir \${INSTALL_ROOT}/etc

# Check to see if pacman already exists

if [ ! -f \${INSTALL_ROOT}/pacman/pacman ]; then

  # fetch from the BU server
  wget -q --output-document=- $PacmanLocation | tar -zxf - 
  if [ "\$?" -ne 0 ]; then 
    echo "ERROR: Could not install pacman!"
    exit 255
  fi
 
  # create a link
  ln -s pacman-2.121 pacman

fi

# Now we fix our PATH to include the Pacman dir
PATH=\$PATH:`pwd`/pacman; export PATH
    
# Install the requested package
#/usr/bin/yes | pacman -trust-registered-caches -remove $package
/usr/bin/yes | pacman -trust-registered-caches -get $package
if [ "\$?" -ne 0 ]; then 
  echo "ERROR: Could not install $package!"
  exit 255
fi

EOF
    close(OUTPUT);

#
# Now submit the job
#

system( "$gjr $gk/$DefaultJobManager -stage submit_${site}.sh > submit_${site}.log 2>&1");

#
# remove the fork job submit script and the log file
#

unlink("submit_${site}.sh");
unlink("submit_${site}.log");

# O.K: this is misleading, the above script could fail. How do we check
# true exit-code from GJR ?

return 0;
}

###########################################################################
#  END InstallPackage
###########################################################################

#
# Process options
#

GetOptions( 'package|p=s'  => \$package,
	    'location|l=s' => \$location,
	    'site|s=s' => \$install_site,
	    'version' => \&version,
	    'vo|v=s' => \$VO,
	    'help|h' => \&usage );

die("ERROR: No Package to install!\n") unless ( $package);


# First check globus enviroment (should be moved to module...)
my $rc = check_globus_setup;

#
# Pull a list of hosts out of GridCat 
#

# Query the GridCat
my $GridCat = GriPhyN::GridCat->new();

my $count = 0;
foreach my $id ( sort keys %{$GridCat} ) {
   $site = $GridCat->{$id}->{name};
   $gk = $GridCat->{$id}->{host_name};
   $app = $GridCat->{$id}->{appdir};
   $data = $GridCat->{$id}->{datadir};

   next if $gk =~ /__host_?name__/;

   if ($install_site) { 
     if ($site ne "$install_site") { next; }
  }

   print "Installing on ${site}...";
   if ("$app" eq "unknown") {
     print "failed: no \$app defined\n";
     next;
   }


  # Check to see if site will run a fork job for me
  my $rc = check_job_mgr_fork($gk);
  if ( $rc != 0 ) {
    print "failed: jobmanager-fork\n";
    next;
  }

  #
  # Run InstallPackage on each site
  #
  
  $rc = InstallPackage;
  if ( $rc == 0 ) {
     print "success\n";
  } else { 
     print "failed: installation\n";
  }

}

exit;

