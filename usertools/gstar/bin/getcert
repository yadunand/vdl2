#!/bin/bash
#
# $Id: getcert,v 1.1 2005/04/18 00:28:17 griphyn Exp $
#
#

necho() {
  printf "$*"
}

echoerr() {
  echo "$*" 1>&2
}

# Messy hack
#
if curl -k https://collegeca.dartmouth.edu >/dev/null 2>&1 ; then
  curl="curl -k"
elif curl https://collegeca.dartmouth.edu >/dev/null 2>&1 ; then
  curl="curl"
else
  echoerr "Can't figure out how to get curl to speak to the college ca...bailing out."
  exit 255
fi


_ASCII[48]="0"
_ASCII[49]="1"
_ASCII[50]="2"
_ASCII[51]="3"
_ASCII[52]="4"
_ASCII[53]="5"
_ASCII[54]="6"
_ASCII[55]="7"
_ASCII[56]="8"
_ASCII[57]="9"
_ASCII[65]="A"
_ASCII[66]="B"
_ASCII[67]="C"
_ASCII[68]="D"
_ASCII[69]="E"
_ASCII[70]="F"
_ASCII[71]="G"
_ASCII[72]="H"
_ASCII[73]="I"
_ASCII[74]="J"
_ASCII[75]="K"
_ASCII[76]="L"
_ASCII[77]="M"
_ASCII[78]="N"
_ASCII[79]="O"
_ASCII[80]="P"
_ASCII[81]="Q"
_ASCII[82]="R"
_ASCII[83]="S"
_ASCII[84]="T"
_ASCII[85]="U"
_ASCII[86]="V"
_ASCII[87]="W"
_ASCII[88]="X"
_ASCII[89]="Y"
_ASCII[90]="Z"
_ASCII[97]="a"
_ASCII[98]="b"
_ASCII[99]="c"
_ASCII[100]="d"
_ASCII[101]="e"
_ASCII[102]="f"
_ASCII[103]="g"
_ASCII[104]="h"
_ASCII[105]="i"
_ASCII[106]="j"
_ASCII[107]="k"
_ASCII[108]="l"
_ASCII[109]="m"
_ASCII[110]="n"
_ASCII[111]="o"
_ASCII[112]="p"
_ASCII[113]="q"
_ASCII[114]="r"
_ASCII[115]="s"
_ASCII[116]="t"
_ASCII[117]="u"
_ASCII[118]="v"
_ASCII[119]="w"
_ASCII[120]="x"
_ASCII[121]="y"
_ASCII[122]="z"
urlencode() {
  #    0-31 ASCII Control Characters These characters are not printable Unsafe
  #    32-47 Reserved Characters ' '!?#$%&'()*+,-./                     Unsafe
  #    48-57 ASCII Characters and Numbers 0-9                           Safe
  #    58-64 Reserved Characters :;<=>?@                                Unsafe
  #    65-90 ASCII Characters A-Z                                       Safe
  #    91-96 Reserved Characters [\]^_`                                 Unsafe
  #  97-122 ASCII Characters a-z                                        Safe
  #  123-126 Reserved Characters {|}~                                   Unsafe
  #  127 Control Characters ' '                                         Unsafe
  #  128-255 Non-ASCII Characters ' '                                   Unsafe

  local d

  if [ $# -gt 0 ] ; then
    printf "$*" | urlencode
    return
  fi

  hexdump -v -e '1/1 "%1d \n"' | while read d ; do
    if [ $d -le 47 ] ; then
      printf "%%%2.2X" $d
    elif [ $d -le 57 ] ; then
      printf ${_ASCII[d]}
    elif [ $d -le 64 ] ; then
      printf "%%%2.2X" $d
    elif [ $d -le 90 ] ; then
      printf ${_ASCII[d]}
    elif [ $d -le 96 ] ; then
      printf "%%%2.2X" $d
    elif [ $d -le 122 ] ; then
      printf ${_ASCII[d]}
    else
      printf "%%%2.2X" $d
    fi
  done
}


# files used for openssl certificate request
#
credDir="${HOME}/.globus"
reqFile="${credDir}/userreq.pem"
randFile="/tmp/random.$$"
sslConf="/tmp/sslconf.$$"


# More junk that we need...
#
certFile="$credDir/usercert.pem"
privKey="$credDir/userkey.pem"
debugFile="/tmp/server_response.$$.html"
dc1="edu"
dc2="dartmouth"
o="Dartmouth College"

password=""
verify_password=""


# check to see if we already have certificates
#
[ -f "$privKey" ] && [ -f "$certFile" ] && {
  echo
  echo "A private key and certificate were found in:"
  echo "$credDir"
  echo
  echo "If you want to proceed generating a new global certificate"
  echo "and key, then please first remove:"
  echo "1) $privKey"
  echo "2) $certFile"
  echo
  exit 255;
}


# obtain username & password 
#
necho "DND User Name: "
read username

QueryPassword() {
  if stty -echo ; then
    necho "DND Password: "
    read password
    echo
    necho "Verify Password: "
    read verifypassword
    echo
    stty echo
  else
    stty echo
    echoerr "$0: error setting terminal to *not* echo"
  fi

  if [ "X${password}" = "X${verifypassword}" ] ; then
    return 0
  else
    return 1
  fi
}

while : ; do
  if QueryPassword ; then
    break
  else
    echo
    echo "Password Mismatch - please try again"
  fi
done


# get some entropy for the random seed -- this is *really* lousy!
#
date > $randFile
[ -f /dev/urandom ] && dd if=/dev/urandom bs=1k count=100 >> $randFile
ls -ln /tmp >> $randFile


if cat > $sslConf ; then
  echo "SSL configuration initialized..."
else
  echoerr "Unable to initialize SSL configuration"
  exit 255
fi <<EOT1
#
# SSLeay configuration file.
# Dynamically generated for certificate issuing script
#

RANDFILE         = $randFile
####################################################################
[ req ]
default_bits            = 1024
default_keyfile         = $privKey
distinguished_name      = req_distinguished_name
prompt                  = no
encrypt_key             = yes
[ req_distinguished_name ]
# BEGIN CONFIG
CN = $username

# END CONFIG
EOT1


# Generate a keypair and the CSR
#
echo
echo " ----------  I M P O R T A N T  ----------"
echo "You will be asked to choose and enter a PEM password."
echo "This password is used to protect your private key from"
echo "being used by another user. You are encouraged to pick"
echo "a strong password that is different from your DND password"
echo "or any other password that you already have in use."
echo " ----------  I M P O R T A N T  ----------"
echo
cmd="${GLOBUS_LOCATION}/bin/openssl req  -new -out $reqFile -config $sslConf"
if  $cmd ; then
  true
else
  echoerr "Error while executing $cmd"
  exit 255
fi
chmod 600 $privKey             # Keep it safe, can be writable by user.
rm -f $randFile $sslConf          # Done with these


# pkcs10 request less the header and footer
#
if ed ${reqFile} >/dev/null 2>&1 ; then
  true
else
  echoerr "Failed to remove PKCS10 request header and footer...bailing out"
  exit 255
fi <<'EOF'
1
d
$
d
w
q
EOF


# Post the certificate request...
#
url="https://collegeca.dartmouth.edu/enrollment"
${curl} \
		-d "csrRequestorName=`urlencode $username`" \
		-d "cn=`urlencode $username`" \
		-d "pwd=`urlencode $password`" \
		-d "E=`urlencode $username`" \
		-d "OU=" \
		-d "O=`urlencode $o`" \
		-d "C=US" \
		-d "email=true" \
		-d "ssl_client=true" \
		-d "digital_signature=true" \
		-d "non_repudiation=true" \
		-d "key_encipherment=true" \
		-d "noPublish=false" \
		-d "authenticator=DNDEnroll" \
		-d "csrRequestorEmail=" \
		-d "csrRequestorPhone=" \
		-d "cryptprovider=1" \
		-d "Send=Submit" \
		-d "pkcs10Request=`urlencode < $reqFile`" \
		-d "subject=CN=$username, O=`urlencode $o`, C=US" \
		-d "requestFormat=keygen" \
		-d "certType=client" \
    $url \
  | tee ${debugFile} \
  | egrep '^record\.base64Cert=".*";$' \
  | sed -e 's/record.base64Cert="//; s/\\n/*/g ; s/";//' \
  | tr '*' '\n' > $certFile
if [ $? -ne 0 ] ; then
  echoerr "Certificate request failed: $url"
  exit 255
fi

if [ ! -s "${certFile}" ] ; then
  rm -f ${certFile}
  echoerr "Unable to locate a certificate in the server response. The most likely reason is that your DND Username/Password was incorrect; however, you should see $debugFile for additional details."
else
  if ${GLOBUS_LOCATION}/bin/openssl x509 -text -in ${certFile} ; then
    chmod 600 ${certFile}
    rm -f ${debugFile}
    echo "Your certificate was successfully extracted to $certFile"

    #
    # Now, we should try to post it...
    #
    curl -F cert=@${HOME}/.globus/usercert.pem -F submit=OK http://grid.dartmouth.edu/certupload.php


  else
    rm ${certFile}
    echoerr "openssl was unable to read your certificate...bailing out!"
    exit 255
  fi
fi
