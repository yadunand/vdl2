#!/bin/csh
#
# $Id: gstar-setup-env.csh,v 1.4 2005/05/10 17:51:16 griphyn Exp $
#
#
#
# GSTAR_GRIDCAT_DB
#
# - Valid values:
#   o osg
#   o grid3
#   o green
# - Default: grid3
#
# setenv GSTAR_GRIDCAT_DB grid3


if ( ! $?GSTAR_LOCATION) then
    echo "Please set GSTAR_LOCATION variable" 
    return 1
endif

#
# add toolkit to PATH and MANPATH
#
if ( $?PATH ) then
    set x="${GSTAR_LOCATION}/bin"
    set y=`echo $PATH | egrep '(^|:)'$x'($|:)' >>& /dev/null`
    [ $? != 0 ] && setenv PATH "${PATH}:${x}"
    unset x
    unset y
else
    # no PATH -- very suspicious
    setenv PATH "${GSTAR_LOCATION}/bin"
endif

#  Add our man pages
if ( $?MANPATH ) then
    set x="${GSTAR_LOCATION}/man"
    set y=`echo $MANPATH | egrep '(^|:)'$x'($|:)' >>& /dev/null`
    [ $? != 0 ] && setenv MANPATH "${MANPATH}:${x}"
    unset x
    unset y
else
    setenv MANPATH "${GSTAR_LOCATION}/man"
endif

#
# Take care of PERL5LIB
#
if ( $?PERL5LIB ) then
    set x="${GSTAR_LOCATION}/lib"
    set y=`echo $PERL5LIB | egrep '(^|:)'$x'($|:)' >>& /dev/null`
    [ $? != 0 ] && setenv PERL5LIB "${PERL5LIB}:${x}"
    unset x
    unset y
else
    setenv PERL5LIB "${GSTAR_LOCATION}/lib"
endif

#
# The C-Shell is evil
#

# We need to set the variable first
setenv GCWD

# Then define our aliases
# FIXME: Need to be able to go 'HOME' if no argument to gcd
#
alias gpwd  'if ($?GCWD) echo $GCWD'
alias gcd   'setenv GCWD  \!*'

