#!/bin/sh
#
# $Id: gstar-setup-env.sh,v 1.5 2005/05/10 17:51:16 griphyn Exp $
#
#

#
# GSTAR_GRIDCAT_DB
# 
# - Valid values:
#   o osg
#   o grid3
#   o green
# - Default: grid3
#
# GSTAR_GRIDCAT_DB=grid3 

if [ "X${GSTAR_LOCATION}" = "X" ];then
    echo "Please set GSTAR_LOCATION variable" 1>&2
    return 1
fi

#
# add toolkit to PATH and MANPATH
#
if [ "X${PATH}" = "X" ]; then
    PATH=${GSTAR_LOCATION}/bin
else
    x=${GSTAR_LOCATION}/bin
    if ! echo $PATH | egrep "(^|:)$x($|:)" >> /dev/null 2>&1; then
	PATH=${PATH}:$x
    fi
    unset x
fi

if [ "X${MANPATH}" = "X" ]; then
    MANPATH=${GSTAR_LOCATION}/man
else
    x=${GSTAR_LOCATION}/man
    if ! echo ${MANPATH} | egrep "(^|:)$x($|:)" >> /dev/null 2>&1; then
	MANPATH=${MANPATH}:$x
    fi
    unset x
fi

#
# Take care of PERL5LIB
#
if [ "X${PERL5LIB}" = "X" ]; then
    PERL5LIB=${GSTAR_LOCATION}/lib
else
    x=${GSTAR_LOCATION}/lib
    if ! echo ${PERL5LIB} | egrep "(^|:)$x($|:)" >> /dev/null 2>&1; then
	PERL5LIB=${PERL5LIB}:$x
    fi
    unset x
fi

export PATH MANPATH PERL5LIB

#
# gcd is now a shell function (for bourne shell)
#

gcd() {
 if [ "X${1}" != "X" ]; then
    OLDGCWD=${GCWD}; export OLDGCWD
    GCWD=$1; export GCD
  else 
     if [ -f ${HOME}/.gstar/env ]; then
        OLDGCWD=${GCWD}; export OLDGCWD
        GCWD=`grep '^HOME=' ${HOME}/.gstar/env | cut -d'=' -f2`   
        export GCWD
     fi
  fi
}

#
# gpwd is now a shell function (for bourne shell)
#

gpwd() {
  echo $GCWD
}


