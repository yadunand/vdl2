#
# This is the OO Perl Module for GridCat
#
# Contains support for:
#  o XML-RPC
#  o MySQL
#  o Postgres
#
# This file or a portion of this file is licensed under the terms of
# the Globus Toolkit Public License, found in file GTPL, or at
# http://www.globus.org/toolkit/download/license.html. This notice must
# appear in redistributions of this file, with or without modification.
#
# Redistributions of this Software, with or without modification, must
# reproduce the GTPL in: (1) the Software, or (2) the Documentation or
# some other similar material which is provided with the Software (if
# any).
#
# Copyright 1999-2004 University of Chicago and The University of
# Southern California. All rights reserved.
#
# $Id: GridCat.pm,v 1.12 2005/05/04 01:59:20 griphyn Exp $
#

package GriPhyN::GridCat;

use 5.008;
use strict;
use warnings;
use DBI;
use File::Spec;

# XML-RPC Support
require RPC::XML;
require RPC::XML::Client;

our $VERSION = '0.2.5';
our @ISA = ();

my $default;

my %db = (
    'grid3' => {
        xmlrpc => 'http://www.ivdgl.org/grid3/catalog/services.php',
        comment => 'Grid3 layout database',
        type => 'mysql',
        host => 'butternut.ucs.indiana.edu',
        port => '3306',
        name => 'grid3v8',
        user => undef,
        pass => undef },
    'osg' => {
        xmlrpc => 'http://www.ivdgl.org/osg-int/gridcat/services.php',
        comment => 'OSG ITB layout database',
        type => 'mysql',
        host => 'butternut.ucs.indiana.edu',
        port => '3307',
        name => 'osgitb',
        user => undef,
        pass => undef },
    'fallback' => {
        xmlrpc => undef,
        comment => 'Grid3 locally cached copy',
        type => 'Pg',
        host => 'griodine.uchicago.edu',
        port => '5432',
        name => 'grid3pro',
        user => 'gridcat',
        pass => 'geheim' },
     'green' => {
        xmlrpc => 'http://grid.dartmouth.edu/gridcat/services.php',
        comment => 'Dartmouth clusters (private)',
        type => 'xmlrpc',
        host => 'grid.dartmouth.edu',
        port => '3306',
        name => 'greengrid',
        user => undef,
        pass => undef }
);

# each database names attributes differently, sigh.
# extend, if you use other drivers than mysql or Pg.
my %quirks = ( 'mysql' => 'database', 'Pg' => 'dbname' );

if ($ENV{'GSTAR_GRIDCAT_DB'}) {
   $default = $ENV{'GSTAR_GRIDCAT_DB'};
} else { 
   $default = 'grid3';          # which default database
}

sub connectme($) {
    # purpose: connect to a database source
    # paramtr: $site (IN): hashref describing the connection parameters
    # returns: undef in case of error, or valid database handle
    my $site = shift;

    my $uri = 'dbi:' . $site->{type} . ':' . $site->{name};
    $uri .= ';host=' . $site->{host} if defined $site->{host};
    $uri .= ';port=' . $site->{port} if defined $site->{port};

    my $result;
    eval {	
	local $SIG{ALRM} = sub { die "Warning: Connection timed out\n" };
	alarm(15);
	$result = DBI->connect( $uri, $site->{user}, $site->{pass} ) ||
	    die( "Warning: Database connection failed: ", DBI->errstr, "\n" );
    };
    alarm(0);			# always reliably undo alarm
    warn( substr($@,0,-1), " for $uri\n" ) if $@;
    $result;
}
    
# Create a 'GridCat' object and fill with data from GridCat
sub new
{
    my $proto = shift;
    my $class = ref($proto) || $proto || __PACKAGE__;

    my $dbh;
    foreach my $site ( $db{$default} ) {

    # Deal with XML-RPC
    if ( $site->{type} eq "xmlrpc") {
	my $response;
	my $value;
        my @sites;
	my %self;
	my $gridcat;
	my $num_sites;

	my $gch = new RPC::XML::Client $site->{xmlrpc};

	# collect the list of sites
        $response = $gch->send_request('sitenames');
        $value = $response->value;

        # returns a string with newlines
        @sites = split('\n',$value);

	# total number of sites
	$num_sites = @sites;

	# hostnames	
        $response = $gch->send_request('getresult',"sites","cs_gatekeeper_hostname",
          "where cs_gatekeeper_hostname like '%'");
        $value = $response->value;
	my @cs_gatekeeper_hostname = split('\n',$value);

	# gatekeeper port	
        $response = $gch->send_request('getresult',"sites","cs_gatekeeper_port",
          "where cs_gatekeeper_port like '%'");
        $value = $response->value;
	my @cs_gatekeeper_port = split('\n',$value);

	# jobmanagers
        $response = $gch->send_request('getresult',"sites","jobmanagers",
          "where jobmanagers like '%'");
        $value = $response->value;
	my @jobmanagers = split('\n',$value);

	# num_cpus
        $response = $gch->send_request('getresult',"sites","num_cpus",
          "where num_cpus like '%'");
        $value = $response->value;
	my @num_cpus = split('\n',$value);

	# ncpus
        $response = $gch->send_request('getresult',"site_info","ncpus",
          "where ncpus like '%'");
        $value = $response->value;
	my @ncpus = split('\n',$value);

	# voname
        $response = $gch->send_request('getresult',"site_info","voname",
          "where ncpus like '%'");
        $value = $response->value;
	my @voname = split('\n',$value);

	# ncpurunning
        $response = $gch->send_request('getresult',"site_info","ncpurunning",
          "where ncpurunning like '%'");
        $value = $response->value;
	my @ncpurunning = split('\n',$value);

	# appdir
        $response = $gch->send_request('getresult',"site_info","appdir",
          "where appdir like '%'");
        $value = $response->value;
	my @appdir = split('\n',$value);

	# datadir
        $response = $gch->send_request('getresult',"site_info","datadir",
          "where datadir like '%'");
        $value = $response->value;
	my @datadir = split('\n',$value);

	# tmpdir
        $response = $gch->send_request('getresult',"site_info","tmpdir",
          "where tmpdir like '%'");
        $value = $response->value;
	my @tmpdir = split('\n',$value);

	# wntmpdir
        $response = $gch->send_request('getresult',"site_info","wntmpdir",
          "where wntmpdir like '%'");
        $value = $response->value;
	my @wntmpdir = split('\n',$value);

	# grid3dir
        $response = $gch->send_request('getresult',"site_info","grid3dir",
          "where grid3dir like '%'");
        $value = $response->value;
	my @grid3dir = split('\n',$value);

	#
	# storage 
	#

	# dataused
        $response = $gch->send_request('getresult',"site_se","dataused",
          "where dataused like '%'");
        $value = $response->value;
	my @dataused = split('\n',$value);

	# appused
        $response = $gch->send_request('getresult',"site_se","appused",
          "where appused like '%'");
        $value = $response->value;
	my @appused = split('\n',$value);

	# tmpused
        $response = $gch->send_request('getresult',"site_se","tmpused",
          "where tmpused like '%'");
        $value = $response->value;
	my @tmpused = split('\n',$value);

	# dataavail
        $response = $gch->send_request('getresult',"site_se","dataavail",
          "where dataavail like '%'");
        $value = $response->value;
	my @dataavail = split('\n',$value);

	# appavail
        $response = $gch->send_request('getresult',"site_se","appavail",
          "where appavail like '%'");
        $value = $response->value;
	my @appavail = split('\n',$value);

	# tmpavail
        $response = $gch->send_request('getresult',"site_se","tmpavail",
          "where tmpavail like '%'");
        $value = $response->value;
	my @tmpavail = split('\n',$value);

	my $i = 0;
	while (  $i < $num_sites ) {

		$gridcat->{"$i"}{'id'} = $sites[$i];
		$gridcat->{"$i"}{'name'} = $sites[$i];
		$gridcat->{"$i"}{'host_name'} = $cs_gatekeeper_hostname[$i];
		$gridcat->{"$i"}{'cs_gatekeeper_hostname'} = $cs_gatekeeper_hostname[$i];
		$gridcat->{"$i"}{'cs_gatekeeper_port'} = $cs_gatekeeper_port[$i];
		$gridcat->{"$i"}{'jobmanagers'} = $jobmanagers[$i];
		$gridcat->{"$i"}{'num_cpus'} = $num_cpus[$i];
		$gridcat->{"$i"}{'ncpus'} = $ncpus[$i];
		$gridcat->{"$i"}{'voname'} = $voname[$i];
		$gridcat->{"$i"}{'ncpurunning'} = $ncpurunning[$i];

		$gridcat->{"$i"}{'appdir'} = $appdir[$i];
		$gridcat->{"$i"}{'datadir'} = $datadir[$i];
		$gridcat->{"$i"}{'tmpdir'} = $tmpdir[$i];
		$gridcat->{"$i"}{'wntmpdir'} = $wntmpdir[$i];
		$gridcat->{"$i"}{'grid3dir'} = $grid3dir[$i];

		$gridcat->{"$i"}{'appused'} = $appused[$i];
		$gridcat->{"$i"}{'dataused'} = $dataused[$i];
		$gridcat->{"$i"}{'tmpused'} = $tmpused[$i];

		$gridcat->{"$i"}{'dataavail'} = $dataavail[$i];
		$gridcat->{"$i"}{'appavail'} = $appavail[$i];
		$gridcat->{"$i"}{'tmpavail'} = $tmpavail[$i];

		$i++
        }

	return $gridcat;
    }

	last if ( defined ($dbh = connectme($site)) );
    }
    die "ERROR: Unable to connect to any database!\n"
	unless defined $dbh;

    # THIS won't work as expected!!!
    END { $dbh->disconnect() if defined $dbh; }

    # create a join to slurp into RAM
    my $sth = $dbh->prepare( q{
    SELECT s.id,s.name,si.host_name,
    si.appdir,si.datadir,si.grid3dir,si.wntmpdir,si.tmpdir,
    s.cs_gatekeeper_hostname,s.cs_gatekeeper_port,
    s.jobmanagers,
    si.ncpus, si.ncpurunning, s.num_cpus, si.voname, r.result,
    se.id, se.appavail, se.appused, se.dataavail,
    se.dataused, se.tmpused, se.tmpavail, se.griddavail,
    se.griddused
    FROM sites s, site_info si, results r, site_se se
    WHERE s.id = si.id AND s.id = se.id }  ) ||
	die "prepare: $DBI::errstr";
    $sth->execute || die "execute: $DBI::errstr";

    # slurp
    my $self = $sth->fetchall_hashref('id') || 
	die "fetchall: $DBI::errstr\n";
    $sth->finish;
    $dbh->disconnect;
    undef $dbh;

    # return the perl way (last op result is method result)
    bless $self, $class;
}

1;

__END__
