#
#
# GriPhyN::RLS : Functions for dealing with RLS and namespace management.
#
#
# This file or a portion of this file is licensed under the terms of
# the Globus Toolkit Public License, found in file GTPL, or at
# http://www.globus.org/toolkit/download/license.html. This notice must
# appear in redistributions of this file, with or without modification.
#
# Redistributions of this Software, with or without modification, must
# reproduce the GTPL in: (1) the Software, or (2) the Documentation or
# some other similar material which is provided with the Software (if
# any).
#
# Copyright 1999-2004 University of Chicago and The University of
# Southern California. All rights reserved.
#
#
# $Id: RLS.pm,v 1.6 2005/05/09 01:36:38 griphyn Exp $
#

package GriPhyN::RLS;

use 5.008;
use strict;
use File::Spec;

use IPC::Open2;   # For globus-rls-cli reader & writer pipes
use POSIX qw(strftime);
use File::Basename;

our $VERSION = '0.2.0';
our @ISA = qw();

# Globus RLS CLI  binary
my $grc = File::Spec->catfile( $ENV{'GLOBUS_LOCATION'},
                     'bin', 'globus-rls-cli');

#
# create a new rls_stat structure
#

sub new {
  #
  # Define what our datastructure looks like:
  #
  my %rls_stat = (
   rls_mode =>     undef,
   rls_uid =>      undef,
   rls_gid =>      undef,
   rls_size =>     undef,
   rls_mtime =>    undef,
   rls_atime =>    undef,
   rls_ctime =>    undef
  );
  my $rls_stat = %rls_stat;
  return $rls_stat;
}

#
# Simple function to get a PFN from a LFN.
#

sub get_pfn {
   my $RLS = shift;
   my $LFN = shift;
   my (@pfn_list,@pfn);
   my @pfn_list = `$grc q l l $LFN $RLS 2>/dev/null`;
   foreach my $pfn (@pfn_list) {
      chop($pfn);
      $pfn =~ s/.*?://;
      push(@pfn,$pfn);
   }
   return @pfn;
}

#
# rls_readdir(server,lfn)
#

sub rls_readdir{
   my $rls = shift;
   my $lfn = shift;
   my @d_name;


   # test for existance
   # send stderr to /dev/null (LFN doesn't exist - we will generate the error)
   my $mode = `$grc q l l $lfn $rls > /dev/null 2>&1`;
   if ($? != 0) {
      return 2;    # Need something better, couldn't connect to RLS or
                   # find lfn, or find attribute.
   }

   # test for directory
   # send stderr to /dev/null (LFN doesn't exist - we will generate the error)
   my $mode = `$grc attribute query $lfn st_mode lfn $rls 2> /dev/null`;
   if ($? != 0) {
      return 20;  # Need something better, couldn't connect to RLS or
                   # find lfn, or find attribute.
   }
 
   # clean up mode
   $mode =~ s/.*int: //;
   $mode =~ s/[ \r\n]+$//;

   if ($mode < 4000) {
     return 20; # ENOTDIR 20 - Not a directory
   }

   #
   # Now we should have a directory 
   #

   my @temp_d_name = `$grc query lrc lfn $lfn $rls`;
   if ($? != 0) {
      return 20; # Need something better, couldn't connect to RLS or
                  # find lfn, or find attribute.
   }

   # remove LFN and push into d_name array 
   foreach my $i (@temp_d_name) {
     # remove lfn, newline, etc
     $i =~ s/.*: //;  # will this always work? 
     $i =~ s/[ \r\n]+$//;

     # push into d_name 
     if ($i ne '*') { 
        push(@d_name, $i); 
     }
   } 
    # I'm learning....
     @d_name;
}


#
#
sub rls_stat_priv{
   my $rls = shift;
   my $lfn = shift;

   open(RLS, "|$grc $rls 2>/dev/null");
   print RLS "attribute query $lfn st_mode lfn\n";
   print RLS "attribute query $lfn st_uid lfn\n";
   print RLS "attribute query $lfn st_gid lfn\n";
   print RLS "attribute query $lfn st_mtime lfn\n";
   print RLS "attribute query $lfn st_atime lfn\n";
   print RLS "attribute query $lfn st_ctime lfn\n";
   close(RLS);
}

#
# rls_stat(server,lfn)
#
# This needs to do a single call to RLS and parse the results
#

sub rls_stat{
   my $rls = shift;
   my $lfn = shift;

   # get a new rls_stat
   my %rls_stat = GriPhyN::RLS->new();
  
   #my $output = &rls_stat_priv($rls,$lfn);
   #print "$output\n";
 
   my $mode = `$grc attribute query $lfn st_mode lfn $rls 2>/dev/null`;
   my $uid = `$grc attribute query $lfn st_uid lfn $rls 2>/dev/null`;
   my $gid = `$grc attribute query $lfn st_gid lfn $rls 2>/dev/null`;
   my $ctime = `$grc attribute query $lfn st_ctime lfn $rls 2>/dev/null`;

   $uid =~ s/.*st_uid: string: //;
   chop($uid);
   $ctime =~ s/.*st_ctime: date: //;
   chop($ctime);
   # clean up mode
   $mode =~ s/.*int: //;
   $mode =~ s/[ \r\n]+$//;
   %{rls_stat}->{rls_mode}   = $mode || "0400";
   %{rls_stat}->{rls_uid}    = $uid || "owner";
   %{rls_stat}->{rls_gid}    = $gid || "group";
   %{rls_stat}->{rls_ctime}  = $ctime || "group";

   # populate
   #%{rls_stat}->{rls_mode}        = "0400";
   #%{rls_stat}->{rls_uid}         = "jed";
   #%{rls_stat}->{rls_gid}         = "ivdgl"; 
   #%{rls_stat}->{rls_size}        = "foo";
   #%{rls_stat}->{rls_mtime}       = "0x00";
   #%{rls_stat}->{rls_atime}       = "0x00";
   #%{rls_stat}->{rls_ctime}       = "0x00";

   # return
   return %rls_stat;
}

#
# rls_update(rls,lfn,rls_stat)
#

sub rls_update {
  my $rls = shift;
  my $lfn = shift;
  my %rls_stat = shift;

  %rls_stat->{rls_uid} = $ENV{'LOGNAME'};
  %rls_stat->{rls_gid} = $ENV{'LOGNAME'};
  %rls_stat->{rls_ctime} = strftime "%Y-%m-%d %H:%M:%S", localtime;
  %rls_stat->{rls_mode} = "1755";
 

  open(RLS, "|$grc $rls 2>/dev/null");
  print RLS "attribute delete $lfn st_uid lfn\n";
  print RLS "attribute delete $lfn st_gid lfn\n";
  print RLS "attribute delete $lfn st_ctime lfn\n";
  close(RLS);

  open(RLS, "|$grc $rls 2>/dev/null");
  print RLS "attribute add $lfn st_uid lfn string $rls_stat{rls_uid}\n";
  print RLS "attribute add $lfn st_gid lfn string $rls_stat{rls_gid}\n";
  print RLS "attribute add $lfn st_ctime lfn date \"$rls_stat{rls_ctime}\"\n";
  print RLS "attribute add $lfn st_mode lfn int \"$rls_stat{rls_mode}\"\n";
  close(RLS);
  return 0;
}


#
# rls_unlink(rls,lfn)
#

sub rls_unlink{
 my $RLS = shift;
 my $LFN = shift;
 my $grc = "$ENV{'GLOBUS_LOCATION'}/bin/globus-rls-cli";

 # Attempt to remove from the directory entry
 my $dirname = dirname($LFN);
 system("$grc delete $dirname $LFN $RLS > /dev/null 2>&1"); 

 # Remove each entry
 my @PFN_LIST = get_pfn($RLS,$LFN);
 foreach my $PFN (@PFN_LIST) {
   system("$grc delete $LFN $PFN $RLS > /dev/null 2>&1"); 
 }
}

#
# rls_mkdir(rls,lfn)
#

sub rls_mkdir{
   my $RLS = shift;
   my $lfn = shift;
   my $pfn = ".";
   my %rls_stat;

   system("$grc create $lfn $pfn $RLS > /dev/null 2>&1");
   if ($? != 0) {
     print "Error creating directory!\n";
     return 255;
   }

   # add to parent... 
   my $dirname = dirname($lfn);
   system("$grc add $dirname $lfn $RLS > /dev/null 2>&1"); 

   %rls_stat->{rls_uid} = $ENV{'LOGNAME'};
   %rls_stat->{rls_gid} = $ENV{'LOGNAME'};
   %rls_stat->{rls_ctime} = strftime "%Y-%m-%d %H:%M:%S", localtime;
   %rls_stat->{rls_mode} = "4755";

   open(RLS, "|$grc $RLS 2>/dev/null");
   print RLS "attribute add $lfn st_uid lfn string $rls_stat{rls_uid}\n";
   print RLS "attribute add $lfn st_gid lfn string $rls_stat{rls_gid}\n";
   print RLS "attribute add $lfn st_ctime lfn date \"$rls_stat{rls_ctime}\"\n";
   print RLS "attribute add $lfn st_mode lfn int $rls_stat{rls_mode}\n";
   close(RLS);
   return 0;
}

#
# rls_ln(rls,lfn,pfn)
#
# Create a new RLS directory entry and add attributes.
#

sub rls_ln{
 my $RLS = shift;
 my $LFN = shift;
 my $PFN = shift;
 my $grc = "$ENV{'GLOBUS_LOCATION'}/bin/globus-rls-cli";
  
 # Insert into directory....
 my $dirname = dirname($LFN);
 system("$grc add $dirname $LFN $RLS > /dev/null 2>&1"); 
 system("$grc create $LFN $PFN $RLS > /dev/null 2>&1"); 
}

1;

__END__

