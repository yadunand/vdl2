#!/bin/zsh

# User should have already sourced settings.sh

# If you get "Could not get coasters service" the
# service may already be running on SERVICE_PORT

# Process management is incomplete here, check for
# processes with ps

# Setup paths, get helper functions
# PC -> "persistent-coasters"
PC=$( cd $( dirname $0 ) ; /bin/pwd )
TOOLS=${PC} # In the future this might be ${SWIFT_HOME}/tools
source ${TOOLS}/helpers.zsh
source ${TOOLS}/settings.sh
[[ $? != 0 ]] && print "Could not load helpers.zsh!" && exit 1
[[ ${SWIFT} == "" ]] && SWIFT=$( which swift )
SWIFT_BIN=$( dirname ${SWIFT} )
export WORKER=${SWIFT_BIN}/worker.pl
[[ ${COASTER_SERVICE} == "" ]] && COASTER_SERVICE=$( which coaster-service )

# This is an arbitrary port number
export SERVICE_PORT=10985

export WORKER_LOGDIR=${PWD}/logs
mkdir -p ${WORKER_LOGDIR}
exitcode

SWIFT_OUT=${PWD}/swift.out

logvars PC COASTER_SERVICE WORKER_LOGDIR

# Get the function start-workers() from one of these files:
log $( declare WORKER_MODE )
if [[ ${WORKER_MODE} == "local" ]]
then
  source ${PC}/workers-local.zsh
elif [[ ${WORKER_MODE} == "ssh" ]]
then
  source ${PC}/workers-ssh.zsh
elif [[ ${WORKER_MODE} == "cobalt" ]]
then
  source ${PC}/workers-cobalt.zsh
else
  print "Unknown WORKER_MODE: ${WORKER_MODE}"
  false
fi
exitcode "Could not find start-workers()!"

# Obtain the URL to which Swift should connect
get_service_coasters()
{
  local OUT=$1
  LINE=()
  COUNT=0
  for COUNT in {1..3}
  do
    LINE=( $( grep --text "Started coaster service:" ${OUT} ) )
    if [[ ${#LINE} == 0 ]] then
      grep Error ${OUT} >& /dev/null && return 1
      sleep 3
    else
      break
    fi
  done
  [[ ${#LINE} == 0 ]] && return 1

  CONTACT=${LINE[-1]}
  print ${CONTACT}
  return 0
}

# Obtain the URL to which the workers should connect
get_service_local()
{
  local OUT=$1
  LINE=()
  COUNT=0
  for COUNT in {1..4}
  do
    LINE=( $( grep "Passive queue processor" ${OUT} ) )
    if [[ ${#LINE} == 0 ]] then
      sleep 4
    else
      break
    fi
    if grep "Error" ${OUT} >& /dev/null
    then
      print "coaster-service error!" >&2
      cat ${OUT} >&2
      return 1
    fi
  done
  [[ ${#LINE} == 0 ]] && return 1

  CONTACT=${LINE[-1]}
  print ${CONTACT}
  return 0
}

SIGNALS="EXIT INT QUIT"
cleanup_trap()
{
  print "cleanup_trap()..."
  eval trap - ${SIGNALS}
  [[ ${COASTER_SERVICE_PID} != "" ]] && kill ${COASTER_SERVICE_PID}
  [[ ${STARTWORKERS_PID}    != "" ]] && kill ${START_WORKERS_PID}
}
# eval trap cleanup_trap ${SIGNALS}

log "Starting the coaster service..."
SERVICE_OUTPUT=${WORKER_LOGDIR}/coaster-service.out
${COASTER_SERVICE} -nosec -p ${SERVICE_PORT} >& ${SERVICE_OUTPUT} &
COASTER_SERVICE_PID=${!}

sleep 1

SERVICE_COASTERS=$( get_service_coasters ${SERVICE_OUTPUT} )
exitcode "Could not get coasters service!"
export SERVICE_COASTERS
log "Coaster service on: ${SERVICE_COASTERS}"

WORK=${WORKER_WORK}
source ${PC}/setup.sh
exitcode "setup.sh failed!"

sleep 1

log "Passivate..."
{ ${SWIFT} \
  -config ${PC}/swift.properties   \
  -sites.file sites.passivate.xml  \
  -tc.file ${PC}/tc.passivate.data \
  ${PC}/passivate.swift < /dev/null >& ${SWIFT_OUT}
  exitcode "Swift failed: see ${SWIFT_OUT}"
  print "Swift finished."
} &
SWIFT_PID=${!}

sleep 1

SERVICE_LOCAL=$( get_service_local ${SWIFT_OUT} )
exitcode "get_service_local failed!"
export SERVICE_LOCAL
log "Local service on: ${SERVICE_LOCAL}"

sleep 1

log "Starting workers..."
start-workers ${SERVICE_LOCAL}
START_WORKERS_PID=${!}

sleep 1

cp sites.passivate.xml sites.xml
log "Created user sites file: sites.xml"
# cp tc.passivate.data tc.data
# log "Created user tc file: tc.data"

exit 0
