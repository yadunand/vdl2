
# Source this to get start_workers() for ssh
# hostnames must be in environment variable WORKER_HOSTS

start-workers()
{
  local URI=$1

  local TIMESTAMP=$(date "+%Y.%m%d.%H%M%S")
  local -Z 5 R=${RANDOM}
  ID="${TIMESTAMP}.${R}"

  for MACHINE in ${=WORKER_HOSTS}
  do
    pwd
    scp ${WORKER} ${MACHINE}:${WORKER_WORK}
    ssh ${MACHINE} \
      ${WORKER_WORK}/worker.pl ${URI} ${MACHINE} ${WORKER_LOGDIR} &
  done

  # TODO: manage these PIDs
  # START_WORKERS_PID=

  return 0
}
