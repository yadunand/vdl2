#!/bin/sh

# Substitutes PWD for _DIR_ in tc files 
# example usage: gentc.sh tc.something.data tc.data ${PWD}

# Input tc file, e.g., tc.local.data
INPUT=$1
# Output tc file, e.g., tc.data
OUTPUT=$2
DIR=$3

[[ ${DIR} == "" ]] && echo "No DIR!" && exit 1

crash()
{
    MSG=$1
    echo ${MSG}
    exit 1
}

[[ ${INPUT}  == "" ]] && crash "Not specified: INPUT"
[[ ${OUTPUT} == "" ]] && crash "Not specified: OUTPUT"

{
    sed "s@_DIR_@${DIR}@" 
} < ${INPUT} > ${OUTPUT}
