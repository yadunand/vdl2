
/**
 * Simple script that does nothing of consequence to
 * the CoasterService.  Settings are passed along
 * which configure the CoasterService.
 *
 * We can do some diagnostic stuff here.
 * */

type file;

app (file output) passivate (file script)
{
  sh @script stdout=@output;
}

file s<"system-info.sh">;
file o<"system-info.out">;

o = passivate(s);
