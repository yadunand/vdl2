
Persistent Coasters Helper Scripts
==================================

// This is an asciidoc file but should also be human-readable

== Usage

. Edit +settings.sh+
. Run +./start-service.zsh+
.. +start-service.zsh+ does the steps outlined above
. Start your application SwiftScripts with the generated +sites.xml+

== Concepts

* Worker mode: How to launch the worker.pl scripts

== Overview of persistent CoasterService process

. Source +settings.sh+
. Start coaster service
. Get URL to which Swift should connect from service output
. Run Swift once to send settings to CoasterService,
  putting CoasterService in passive mode
. Get URL to which workers should connect from Swift output
. Connect workers to CoasterService
. Run Swift for application

== Testing

After the service has started, you will have fresh sites.xml and tc.data
files.  You can then run:

-----------------
swift -sites.file sites.xml -tc.file tc.data system-info.swift
-----------------

which will run +system-info.sh+ on the worker and create +system-info.out+

Read +system-info.out+ to see if it worked.

== Input files

+settings.sh+::
Settings for this script suite: +NODES+, etc.
+sites.passivate-template.xml+::
Used to set Coaster service settings:
+jobsPerNode+, etc.
+nodeGranularity+ is not set here:
this is controlled by the +NODES+ value
+swift.properties+::
Normal Swift properties

== Output files

These are places you can look to see what happened.

* Worker logs go into +WORKER_LOGDIR+ (+$PWD/logs+)
** These will be named in accordance with the worker mode
* The coaster service standard output:
  +$WORKER_LOGDIR/coaster-service.out+
* The coaster service log: +cps-date.log+
* The Swift log: +job-id.log+
* +swift.log+ is a dummy file created by Swift and the Coaster service
  at start up.  The log is renamed at initialization time; you can
  ignore this file.
* The output from the Swift passivate task: +swift.out+

The Swift log contains information on the workflow, staging, etc.

The Coaster service log contains information on Block/Cpu connections, execution, etc.

