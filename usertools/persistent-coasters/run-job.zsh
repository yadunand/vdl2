#!/bin/zsh

# Launch a user job
# Use after running start-service.zsh

APP=$( cd $( dirname $0 ) ; /bin/pwd )
TOOLS=${APP} # In the future this might be ${SWIFT_HOME}/tools
source ${TOOLS}/helpers.zsh
[[ $? != 0 ]] && print "Could not load helpers.zsh!" && exit 1

SWIFT_OUT=logs/swift-user.out
swift -config swift.properties \
           -sites.file sites.xml \
           -tc.file tc.data \
           system-info.swift < /dev/null # >& ${SWIFT_OUT}
exitcode "Swift user job failed!"
