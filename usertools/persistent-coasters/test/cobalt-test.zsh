#!/bin/zsh

cqsub -q default \
      -p PTMAP \
      -t 20 \
      -n 1 \
      -C /home/wozniak/proj/persistent-coasters \
      -E /home/wozniak/proj/persistent-coasters/test.err \
      -o /home/wozniak/proj/persistent-coasters/test.out \
      /home/wozniak/proj/persistent-coasters/test-connect.zsh
