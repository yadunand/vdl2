
# Generates sites file
# TODO: extend to generate tc as well

source ${TOOLS}/coasters-setup.sh

export WORK

${TOOLS}/gensites.sh ${WORK} ${PC}/sites.passivate-template.xml \
                             sites.passivate.xml
[[ $? != 0 ]] && bail "sites problem (passivate)" && return

${TOOLS}/gensites.sh ${WORK} ${PC}/sites.persistent.xml sites.xml
[[ $? != 0 ]] && bail "sites problem (persistent)" && return

return 0
