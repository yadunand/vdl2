
# Source this to get start_workers() for cobalt
# This is not complete and will not work because you cannot
#  pass argument on the cqsub command line

start-workers()
{
  local URI=$1
  local TIMESTAMP=$(date "+%Y-%m-%d-%H-%M-%S")
  local -Z 5 R=${RANDOM}
  ID="${TIMESTAMP}.${R}"

  checkvars QUEUE PROJECT MAXTIME NODES WORKER_LOGDIR

  if [[ ${SERVICE_HOST} != "" ]]
    then
    # Override the local service IP address
    # ZSH sed-like substitution expansion
    URI=${URI/<->.<->.<->.<->/${SERVICE_HOST}}
  fi
  log "Workers connect to: ${URI}"

  # Make worker wrapper script that calls worker.pl with args
  export URI ID LOGDIR
  WORKER_WRAPPER=${WORKER_LOGDIR}/worker-cobalt.zsh
  m4 < ${PC}/worker-cobalt.m4.zsh > ${WORKER_WRAPPER}
  chmod u+x ${WORKER_WRAPPER}

  CQSUB_OUT=${WORKER_LOGDIR}/cqsub-job-id.txt
  CQSUB_ERR=${WORKER_LOGDIR}/cqsub.err
  for (( i=0 ; i<NODES ; i++ ))
  do
    # Launch it
    cqsub -q ${QUEUE}                      \
      -p ${PROJECT}                        \
      -t ${MAXTIME}                        \
      -n 1                                 \
      -C ${WORKER_LOGDIR}                  \
      -E ${WORKER_LOGDIR}/worker.${ID}.err \
      -o ${WORKER_LOGDIR}/worker.${ID}.out \
      -e "WORKER_LOGGING_LEVEL=DEBUG"      \
      ${WORKER_WRAPPER} > ${CQSUB_OUT} 2> ${CQSUB_ERR}
      # -n ${NODES}
    (( ${?} )) && break
    log "Cobalt JobID[${i}]: $( < ${CQSUB_OUT} )"
  done
  ERR_CHARS=$( wc -c < ${CQSUB_ERR} )
  if (( ERR_CHARS ))
    then
    print "Cobalt errors:"
    cat ${CQSUB_ERR}
    return 1
  fi

  return 0
}
