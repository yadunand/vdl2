#!/bin/zsh

# This is a template processed by m4

URI=esyscmd(printf $URI)
ID=esyscmd(printf $ID)
LOGDIR=esyscmd(printf $WORKER_LOGDIR)
WORKER=esyscmd(printf $WORKER)

export WORKER_LOGGING_LEVEL=esyscmd(printf $WORKER_LOGGING_LEVEL)

# Launch the worker
${WORKER} ${URI} ${ID} ${LOGDIR}
WORKER_RC=${?}

if [[ ${WORKER_RC} != 0 ]]
  then
  print "Worker failed: ${ID}"
  date
  exit 1
fi

exit 0
