#!/bin/zsh

# This is a template processed by m4

URI=http://140.221.82.124:38199
ID=2011-09-01-18-29-53.21666
LOGDIR=
WORKER=/home/wozniak/import/cog/modules/swift/dist/swift-svn/bin/worker.pl

${WORKER} ${URI} ${ID} ${LOGDIR}
WORKER_RC=${?}

if [[ ${WORKER_RC} != 0 ]]
  then
  print "Worker failed: ${ID}"
  exit 1
fi

exit 0
