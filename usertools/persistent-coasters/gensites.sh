#!/bin/sh

# Certain sites files may require environment variables,
#  see below.
# In particular, URL for the coasters persistent service

# Swift work directory, e.g., ${HOME}/work
WORK=$1
# Input sites file, e.g., sites.local.xml
INPUT=$2
# Output sites file, e.g., sites.xml
OUTPUT=$3

crash()
{
    MSG=$1
    echo ${MSG}
    exit 1
}

[[ ${WORK}   == "" ]] && crash "Not specified: WORK"
[[ ${INPUT}  == "" ]] && crash "Not specified: INPUT"
[[ ${OUTPUT} == "" ]] && crash "Not specified: OUTPUT"

[[ ${GLOBUS_HOSTNAME} == "" ]] && crash "Not specified: GLOBUS_HOSTNAME"

# If the sites file requires a PROJECT, QUEUE, N_GRAN, or N_MAX and
# it is not set in the environment, crash:
for TOKEN in PROJECT QUEUE N_GRAN N_MAX SLOTS
 do
 if grep _${TOKEN}_ ${INPUT} > /dev/null
   then
   if ! declare -p ${TOKEN} > /dev/null
     then
     printenv
     crash "Not specified: ${TOKEN}"
   fi
 fi
done

{
  echo "s/_NODES_/${NODES}/"
  echo "s/_HOST_/${GLOBUS_HOSTNAME}/"
  echo "s@_WORK_@${WORK}@"
  echo "s/_PROJECT_/${PROJECT}/"
  echo "s/_QUEUE_/${QUEUE}/"
  echo "s/_N_GRAN_/${N_GRAN}/"
  echo "s/_N_MAX_/${N_MAX}/"
  echo "s/_SLOTS_/${SLOTS}/"
  echo "s/_MAXTIME_/${MAXTIME}/"
  echo "s@_SERVICE_COASTERS_@${SERVICE_COASTERS:-NO_URL_GIVEN}@"
  echo "s@_SERVICE_PORT_@${SERVICE_PORT:-NO_PORT_GIVEN}@"
} > gensites.sed

sed -f gensites.sed < ${INPUT} > ${OUTPUT}
