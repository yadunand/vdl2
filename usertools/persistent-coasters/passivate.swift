
/**
 * Simple script that does nothing of consequence to
 * the CoasterService.  Settings are passed along
 * which configure the CoasterService.
 *
 * We can do some diagnostic stuff here.
 * */

type file;

app passivate ()
{
  sh "-c" "echo dummy swift job;";
}

app (file h) hostname ()
{
  hostname stdout=@h;
}

file h<"hostname.txt">;

passivate();

h = hostname();
