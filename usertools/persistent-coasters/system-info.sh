#!/bin/bash

# User script to be submitted by Swift using persistent Coasters
# Simply report some system info to stdout for diagnostics

hostname
hostname -d
echo
uname -a

exit 0
