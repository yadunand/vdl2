
# Keep all interesting settings in one place
# User should modify this to fit environment

# Location of SWIFT and COASTER_SERVICE.  If empty, PATH is referenced
export SWIFT=
export COASTER_SERVICE=

# Log level for the persistent-coasters scripts: DEBUG or NONE
export PC_LOGGING=DEBUG

# Where to place/launch worker.pl on the remote machine for sites.xml
export WORKER_WORK=/home/${USER}/work

# How to launch workers: local, ssh, or cobalt
export WORKER_MODE=cobalt # local

# Worker logging setting passed to worker.pl for sites.xml
export WORKER_LOGGING_LEVEL=DEBUG

# Worker host names for ssh
# WORKER_HOSTS="login1 login2"
export WORKER_HOSTS="$( print login{1,2}.mcs.anl.gov )"

# Allow the user to set the IP address of the service
#  Necessary on Eureka: use the 10.*** address
export SERVICE_HOST=10.40.9.151

# Some settings known to gensites, schedulers
NODES=4
QUEUE=default
# minutes
MAXTIME=$(( 20 ))
WORK=${HOME}/work
PROJECT=PTMAP
