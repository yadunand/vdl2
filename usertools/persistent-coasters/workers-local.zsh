
# Source this to get start_workers() for local
# Just starts a local worker.pl process

start-workers()
{
  local URI=$1

  ${WORKER} ${URI} LOCAL ${WORKER_LOGDIR} &

  # TODO: manage these PIDs
  # START_WORKERS_PID=

  return 0
}
