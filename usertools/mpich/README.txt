
See: http://www.ci.uchicago.edu/wiki/bin/view/SWFT/CoastersMpi

These patches are now all obsolete, everything is in MPICH trunk.

Contains the following patches:

mpich2-r7562.diff: works with MPICH2, revision 7562

mpich2-r7562.diff: works with MPICH2, revision 7757
slight improvement over r7562

mpich2-1.3.diff: obsolete due to MPICH changes


