
package plotter;

public class LineReaderException extends Exception
{
  public LineReaderException(String msg)
  {
    super(msg);
  }
}
