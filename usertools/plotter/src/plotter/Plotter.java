
package plotter;

import java.util.ArrayList;
import java.util.List;

import gnu.getopt.Getopt;

public class Plotter
{
  public static void main(String[] args)
  {
    try
    {
      Getopt plot = new Getopt( "Plotter", args, "bdhs");
      /*
       * Fix this check: Need to check to see if the specify one of the types of plots first
       * then check to see if the argument counts is correct.
       */
      if (args.length < 4 )
      {
        usage();
      }

      /* Gather the arguments except the first one */
      String[] tmp = new String[args.length - 1];
      for( int i = 0; i < tmp.length; ++i )
      {
        tmp[i] = args[i+1];
      }
      switch( plot.getopt() )
      {
      /* If asking for a single labeled Y-axis */
      case 's':
      {
        Lines l = new Lines();
        l.plotter( tmp );
        break;
      }
      /* If asking for a dual labeled Y-axis */
      case 'd':
      {
        Dual d = new Dual();
        d.plotter( tmp );
        break;
      }
      case 'b': 
      {
        String props = args[1];
        String output = args[2];
        List<String> dataFiles = dataFileList(args, 3);
        Bars.bars(props, output, dataFiles, true);
        break;
      }
      /* Do not understand option for what graph */
      default:
      {
        usage();
      }
      }
    }
    catch (UserInputException e)
    {
      System.out.println("input error: ");
      System.out.println(e.getMessage());
      // e.printStackTrace();
    }
  }

  static List<String> dataFileList(String[] args, int start)
  {
    List<String> result = new ArrayList<String>(args.length);
    for (int i = start; i < args.length; i++)
      result.add(args[i]);
    return result;
  }
  
  public static void usage()
  {
    System.err.println( "usage: [-s,-d] [<options>] <properties> <output> <data>*" );
    System.exit(2);
  }
}
