
# Source this to set up classpath for plotter runs
# classpath stored in ${CP}

if [[ $( uname ) == CYGWIN* ]]
then
    typeset -T CP cp ";"
    cp+=c:/cygwin${PLOTTERS}/src
    for jar in ${PLOTTERS}/lib/*.jar
    do
      cp+=c:/cygwin${jar}
    done
else
    typeset -T CP cp
    CP=${CLASSPATH}:${PLOTTERS}/src
    cp+=( ${PLOTTERS}/lib/*.jar )
fi
