#!/bin/zsh

# DUAL.ZSH

# Create plot with two data sets, two Y axes
# Arguments passed directly to Dual.java - see there for details
# usage: dual.zsh <properties> <output file> <data file>*

PLOTTERS=$( cd $( dirname $0 ) ; /bin/pwd )

# declare -p PLOTTERS

source ${PLOTTERS}/classpath.zsh
[[ $? != 0 ]] && print "Could not build CLASSPATH!" && return 1

java -ea -cp ${CP} plotter.Dual ${*}
