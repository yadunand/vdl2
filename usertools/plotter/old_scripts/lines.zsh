#!/bin/zsh

# LINES.ZSH
# Arguments passed directly to Lines.java - see there for details
# usage: lines.zsh <properties> <output file> <data file>*

PLOTTERS=$( cd $( dirname $0 ) ; /bin/pwd )

# declare -p PLOTTERS

source ${PLOTTERS}/classpath.zsh
[[ $? != 0 ]] && print "Could not build CLASSPATH!" && return 1

java -ea -cp ${CP} plotter.Lines ${*}
