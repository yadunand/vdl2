#!/bin/zsh

PLOTTERS=$( cd $( dirname $0 ) ; /bin/pwd )

# declare -p PLOTTERS

source ${PLOTTERS}/classpath.zsh
if [[ $? != 0 ]]
then
  print "Could not build CLASSPATH!"
  return 1
fi

if [[ ${PLOTTER_DEBUG} != "" ]]
then
  DEBUG="-agentlib:jdwp=transport=dt_socket,server=y,address=1230"
fi

JAR=${PLOTTERS}/lib/plotter.jar
if [[ ! -f ${JAR} ]]
then
  print "JAR file not found!"
  return 1
fi

java -ea ${DEBUG} -cp ${JAR}:${CP} plotter.Plotter ${*}
