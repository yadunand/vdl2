#!/bin/bash

PATH=$PATH:$CIOROOT/bin

set_vanilla()
{
  cat > ~/bin/swift << EOF
#!/bin/bash
/home/falkon/swift_vanilla/cog/modules/swift/dist/swift-svn/bin/swift $@
EOF
}

set_mtdm()
{
  cat > ~/bin/swift << EOF
#!/bin/bash
/home/falkon/swift_working/cog/modules/swift/dist/swift-svn/bin/swift $@
EOF
}

NSIZE="64 128 256 512 1024" 
for i in $NSIZE; do
  $CIOROOT/bin/falkon-start.sh default $i 60 4 1 4
  set_vanilla
  cobalt_id=`tail -1 $HOME/.falkonjobs | awk '{print $1}'`
  ./runblast.sh . common/UNIPROT_for_blast_14.0.seq \
      100001 104000 l out.run_`printf %03d $i`
  set_mtdm
  falkon_id=`tail -1 $HOME/.falkonjobs | awk '{print $2}'`
  /home/zzhang/DHT/bin/treebroadcast.sh $falkon_id common/common.tar
  ./runblast.sh . common/UNIPROT_for_blast_14.0.seq \
      100001 104000 l out.run_`printf %03d $i`
  qdel $cobalt_id
done
