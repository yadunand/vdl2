#!/bin/bash

while getopts ":a:b:d:l:" options; do
  case $options in
    a) export a=$OPTARG ;;
	b) export b=$OPTARG ;;
	d) export d=$OPTARG ;;
    l) export l=$OPTARG ;;
	*) exit 1;;
  esac
done

for (( i = a; i <= b; i++ )); do
  seqid=`printf %07d $i`
  if [[ $l == "l" ]]; then
    prefix=$d/`printf %04d $(( i / 1000 ))`
  else
    prefix=$d
  fi
  echo [$(($i-a))] $prefix/SEQ$seqid\_*.qry
done
