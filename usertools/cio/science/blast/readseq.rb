#!/usr/bin/env ruby
#
# Script: readseq.rb
# Description: Parses the a BLAST Fasta file and dumps each sequence to a 
#              file.

require 'fileutils'


fasta_db  = File.new(ARGV[0])
seq_start = ARGV[1].to_i
seq_end   = ARGV[2].to_i
prefix    = ARGV[3]            # Output dir prefix
pad       = ARGV[4]            # Limit files per directory? "l"

while true
  x = fasta_db.readline("\n>").sub(/>$/, "")
  if fasta_db.eof or fasta_db.lineno > seq_end
    break
  end
  x =~ />(.*)\n/
  seqname = $1
  if seq_start <= fasta_db.lineno
    dir = pad == "l" ? prefix + 
	    sprintf("/%04d", fasta_db.lineno / 1000) : prefix
    fname = sprintf "SEQ%07d_%s.qry", fasta_db.lineno, seqname
	FileUtils.mkdir_p dir
	file = File.new("#{dir}/#{fname}","w")
	file << x
	file.close
  end
  fasta_db.ungetc ?>
end
