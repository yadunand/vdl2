
DB=/disks/ci-gpfs/swift/blast/pir/UNIPROT_for_blast_14.0.seq
strace -tt -ff -o traceblast \
    /home/aespinosa/science/blast/ncbi.communicado/bin/blastall -p blastp -F F \
    -d $DB -i test.in -v 300 -b 300 -m8 -o test.out
  
