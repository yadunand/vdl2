type output;
type error;

type BlastDatabase;
type BlastQuery;

type BlastResult {
  output out;
  error err;
}

type BlastSummary;

app (BlastResult out) blastall(BlastQuery i, BlastDatabase db[]) {
  /*
  blastall "-p" "blastp" "-F" "F" "-d" @filename(db[0]) 
  */
  mockblast "-p" "blastp" "-F" "F" "-d" @filename(db[0]) 
      "-i" @filename(i) "-v" "300" "-b" "300" "-m8" 
      "-o" @filename(out.out) stderr=@filename(out.err);
}

app (BlastSummary sum) summarize(BlastResult out[]){
  cat @filenames(out.out) stdout=@sum;
}

string db=@arg("db");
string db_prefix=@arg("db_pref");
string start=@arg("start");
string end=@arg("end");
string limit=@arg("limit");
string runid=@arg("run");

BlastDatabase pir[] <filesys_mapper;location=db_prefix,
    pattern=@strcat(db, "*")>;
BlastResult out[] <ext;exec="mapper-out.sh", a=start, b=end,
    d=runid, l=limit>;
BlastQuery input[] <ext;exec="mapper-in.sh", a=start, b=end,
    d=runid, l=limit>;

foreach data,i in input {
  (out[i]) = blastall(data, pir);
}

BlastSummary sum <single_file_mapper;file=@strcat(runid,"/summary.out")>;
sum = summarize(out);
