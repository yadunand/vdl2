#!/bin/bash

# Script: swiftblast.sh [falkon_id] [blast_db] [start_seq] [end_seq] [outdir]
#         Invokes the swift workflow of reciprocal blast

if [ $# -lt 3 ]; then
  cat << EOF
ERROR   :  too few arguments
Usage   :  $0 [falkon_id][blast_db] [start_seq] [end_seq] [outdir]
Example :  Reciprocal blast on the 100th to 200th sequences
of UNIPRT.seq and dump results to run_100:
$0 UNIPROT.seq 100 200 run_100
EOF
fi

if [ -z $CIOROOT ]; then
  echo "ERROR: CIOROOT not defined"
  exit 1
fi

PATH=$PATH:$CIOROOT/bin

FALKON_ID=$1
BLAST_DB=$2
START_SEQ=$3
END_SEQ=$4

# Limit to 1k sequences per dir "l"
LIMIT=$5
RUNID=$6 # Default output directory of everything!

BLASTROOT=$CIOROOT/science/blast

if [ ! -d $RUNID ]; then
  mkdir -p $RUNID
fi

# Extract sequences

ruby readseq.rb `dirname $BLAST_DB`/fasta/`basename $BLAST_DB` \
    $START_SEQ $END_SEQ $RUNID $LIMIT

if [ $FALKON_ID == "." ]; then
  FALKON_ID=`tail -1 $HOME/.falkonjobs | awk '{print $2}'`
fi 

if [ $FALKON_ID == "-" ]; then # Real Falkon job
  tcfile=tc.local
  cp $tcfile tc.data
else
  tcfile=tc.bgp
  gentcdata 32 < $tcfile > tc.data
fi


if [ $FALKON_ID == "-" ]; then 
  echo "local"
  swift -sites.file ./sites.xml -tc.file tc.data blast.swift \
      -db=`basename $BLAST_DB` -db_pref=`dirname $BLAST_DB`\
      -limit=$LIMIT -start=$START_SEQ -end=$END_SEQ \
      -run=$RUNID
else # Real Falkon job
  fjob=`echo $FALKON_ID|awk '{printf "%.4d",$1}'`
  nodes=`cat $HOME/.falkonjobs | awk "\\$2==\"$fjob\" {print \\$4}"`
  fdir=/home/falkon/users/$USER
  if [ ! -d $fdir/$fjob ]; then
    echo $0: Falkon job id $fjob does not exist in $fdir. Last 5 jobs there are:
    (cd $fdir; ls -1 | tail -5)
    exit 1
  fi
  /usr/bin/time -p swift_bgp.sh $fjob $nodes $BLASTROOT/blast.swift \
      -db=`basename $BLAST_DB` -db_pref=`dirname $BLAST_DB`\
      -limit=$LIMIT -start=$START_SEQ -end=$END_SEQ \
      -run=$RUNID >&swift_out.$RUNID
fi
