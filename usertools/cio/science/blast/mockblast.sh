#!/bin/bash

while getopts ":i:d:o:p:F:v:b:m:8" options; do
  case $options in
	i) query=$OPTARG ;;
	d) database=$OPTARG ;;
	o) output=$OPTARG ;;
	p) ;;
	F) ;;
	v) ;;
	b) ;;
	m) ;;
	*) echo "error" $options
	   exit 1;;
  esac
done

seqname=`basename $database`
dir=`dirname $database`
db=$dir/$seqname.*

BLOCKS=1310272

dd=/fuse/bin/dd
ls=/fuse/bin/ls

$dd if=$query of=/dev/null bs=$BLOCKS #2> /dev/null

for i in $db; do
  if [ -h $i ]; then
    truefile=`readlink $i`
  else
    truefile=$i
  fi
  size=`$ls --block-size=$BLOCKS -s $truefile | cut -f 1 -d ' '`
  count=$(( size / 4))
  if [ $count -eq 0 ]; then
     count=$size
  fi
  echo $count
  $dd if=$i of=/dev/null bs=$BLOCKS count=$count  #2> /dev/null
done
sleep 41
$dd if=/dev/zero of=$output bs=15812 count=1 #2> /dev/null
