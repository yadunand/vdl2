#!/fuse/bin/bash

if [ -z "$2" ]; then
    echo "usage: $0 <FileName> <Rank>"
    echo "usage: $0 _concurrent/result-b3f6c91c-4229-4f65-8a6a-984edf5aae35-4-0-array//elt-0 235"
    exit 1
fi

LD_LIBRARY_PATH=/lib:/fuse/lib 
FILENAME=$1
RANK=$2

FIRSTPATH=${FILENAME%%/*}
SECONDPATH=${FILENAME%/*}

echo "mkdir ${FIRSTPATH}" > script
echo "mkdir ${SECONDPATH}" >> script
/home/zzhang/cio/bin/chtput.rb $FILENAME $RANK
/home/zzhang/chirp/bin/chirp -a address 127.0.0.1 < script
cp $FILENAME /dev/shm/share/${SECONDPATH}

exit 0