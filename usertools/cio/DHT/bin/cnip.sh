#!/fuse/bin/bash
LD_LIBRARY_PATH=/lib:/fuse/lib:/fuse/usr/lib
PATH=/fuse/bin:/fuse/usr/bin:$PATH

/home/iskra/ZeptoOS/packages/cnip/prebuilt/cn-ipfwd &

while [ ! -f /tmp/ifconfig.cmd ]; do
    sleep 1
done

. /tmp/ifconfig.cmd
ALLOCATION=$BG_SIZE
RANK=`echo $CONTROL_INIT | awk -F, '{print $4}'`
ITER=`expr $ALLOCATION / 128`
if [ $ITER = "0" ]
then
    echo 10.128.0.0 >> /dev/shm/DHTlist
fi
for (( i=0; i<$ITER; i++))
do
  J=`expr $i / 2`
  X=`expr $i \* 128`
  K=`expr $X \% 256`
  echo 10.128.$J.$K >> /dev/shm/DHTlist
done


if [ -d /dev/shm/share ];
    then
    rm -rf /dev/shm/share 
fi
mkdir /dev/shm/share
echo "address:192.168.1.* rwlda" >>/dev/shm/share/.__acl
echo "address:10.* rwlda" >> /dev/shm/share/.__acl
echo "address:127.0.0.1 rwlda" >> /dev/shm/share/.__acl

/home/zzhang/chirp/bin/chirp_server -r /dev/shm/share &

if [ "$RANK" = "0" ] || [ "$RANK" = "64" ];
then
    /home/zzhang/cio/bin/hashserver.rb &
#else    
    /home/zzhang/falkon/worker/run.worker-c-bgp.sh $1 $2 $3 $4 $5 $6 $7
fi

echo $RANK > /dev/shm/rank
sleep 3600
