#!/bin/bash

if [ -z "$2" ]; then
    echo "usage: $0 <FALKON_ID> <SRC_FILE>"
    exit 1
fi

FALKON_ID=$1
SRC_FILE=$2
SERVICE_LIST_FILE=/home/falkon/users/${USER}/$FALKON_ID/config/Client-service-URIs.config

NUM_LINES=`wc -l $SERVICE_LIST_FILE | awk '{printf"%d\n", $(1)}'`
NUM_READS=`expr $NUM_LINES - 1`
WORKING_DIR=`pwd`
DIR=`dirname $SRC_FILE`
FILENAME=`basename $SRC_FILE`
cd $DIR
DIRPATH=`pwd`
cd $WORKING_DIR

tail -n $NUM_READS $SERVICE_LIST_FILE | while read line; do
    echo $line | awk '{printf"%s\n", $(1)}'
    IP=$line
    echo $DIRPATH/$FILENAME
    ssh $IP /home/iskra/ZeptoOS/packages/zoid/src/daemon/svctool $DIRPATH/$FILENAME /dev/shm/share/$FILENAME &
done
