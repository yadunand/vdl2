#!/fuse/bin/bash

if [ -z "$2" ]; then
    echo "usage: $0 <FileName> <Rank>"
    echo "usage: $0 _concurrent/result-b3f6c91c-4229-4f65-8a6a-984edf5aae35-4-0-array//elt-0 235"
    exit 1
fi

LD_LIBRARY_PATH=/lib:/fuse/lib:/usr/lib
FILENAME=$1
RANK=$2

HASH=`basename $FILENAME | cut -d '-' -f 2 |awk '{print $1}'`
TOTALDHT=`wc -l /dev/shm/DHTlist | awk '{print $1}'`
INDEX_PRE=`expr $HASH \% $TOTALDHT`
INDEX=`expr $INDEX_PRE + 1`
IP=`head -n $INDEX /dev/shm/DHTlist | tail -n 1`
echo "HASH: $HASH"
echo "TOTALDHT: $TOTALDHT"
echo "INDEX: $INDEX"
echo "IP: $IP"

NAME=${FILENAME/\/\//\/}
PREFIX=""
BASE=`basename $FILENAME`
while [ "$NAME" != "$BASE" ]
do
  echo $NAME
  TEMP=${PREFIX}/${NAME%%/*}
  echo "mkdir ${TEMP#*/}" >> script
  PREFIX=${PREFIX}/${NAME%%/*}
  NAME=${NAME#*/}
done

DATE=`date`
echo "$DATE /home/zzhang/DHT/bin/dhtput.rb $FILENAME $RANK $IP" `pwd`>> /dev/shm/DHTregister 2>&1
/home/zzhang/DHT/bin/dhtput.rb $FILENAME $RANK $IP
/home/zzhang/chirp/bin/chirp -a address 127.0.0.1 < script
cp $FILENAME /dev/shm/share/${FILENAME}

exit 0