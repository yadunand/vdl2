#!/fuse/bin/bash

if [ -z "$1" ]; then
    echo "usage: $0 <FileName> <TargetPath>"
    echo "usage: $0 hello /dev/shm/share"
    exit 1
fi

LD_LIBRARY_PATH=/lib:/fuse/lib:/usr/lib
FILENAME=$1
FILEPATH=$2
echo "Full name : $FILENAME $FILEPATH"

HASH=`basename $FILENAME | cut -d '-' -f 2 |awk '{print $1}'`
TOTALDHT=`wc -l /dev/shm/DHTlist | awk '{print $1}'`
INDEX_PRE=`expr $HASH \% $TOTALDHT`
INDEX=`expr $INDEX_PRE + 1`
IP=`head -n $INDEX /dev/shm/DHTlist | tail -n 1`
LOCAL_RANK=`echo $CONTROL_INIT | awk -F, '{print $4}'`


RANK=`/home/zzhang/DHT/bin/dhtget.rb $FILENAME $IP`
echo "RANK: $RANK"
IP=`/home/zzhang/cio/bin/ipcal 10.128.0.0 $RANK`
echo "IP: $IP"
DATE=`date`
echo "$DATE ${LOCAL_RANK} /home/zzhang/chirp/bin/chirp_get -a address $IP $FILENAME $FILEPATH/$FILENAME" >> /home/zzhang/DHTcp
/home/zzhang/chirp/bin/chirp_get -a address $IP $FILENAME $FILEPATH/$FILENAME >> /home/zzhang/DHTcp 2>&1
