#!/fuse/bin/bash

LD_LIBRARY_PATH=/lib:/fuse/lib:/usr/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/zzhang/cio/lib
PATH=/fuse/bin:/fuse/usr/bin:$PATH

IP=`/sbin/ifconfig | grep inet | tail -n 1 | cut -d ':' -f 2 |awk '{print $1}'`
#mkdir -p /dev/shm/share/common
cp -r common/* /dev/shm/share/
#chmod -R 755 /dev/shm/share/common
#exit 1
# tree network path
DESTHOSTS=`seq 0 63 | sed "s/^/10.128.0./" | xargs`
#DESTHOSTS1=`seq 0 255 | sed "s/^/10.128.1./" | xargs`
echo ${DESTHOSTS/$IP/" "}
echo start
/home/zzhang/chirp/bin/chirp_distribute -a address -D 127.0.0.1 / ${DESTHOSTS/$IP/" "}
