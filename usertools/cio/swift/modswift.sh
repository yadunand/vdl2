#! /bin/bash

# this script should copy all the mods below this dir into the right place in th swift source tree.

COGDIR=src/cogkit
SWIFTDIR=$COGDIR/modules/swift

if [ ! -f $SWIFTDIR/libexec/wrapper.sh.vanilla ]; then
  mv $SWIFTDIR/libexec/wrapper.sh $SWIFTDIR/libexec/wrapper.sh.vanilla
fi 
cp -v libexec/wrapper.sh $SWIFTDIR/libexec/wrapper.sh

if [ ! -f $SWIFTDIR/libexec/vdl-int.k.vanilla ]; then
  mv $SWIFTDIR/libexec/vdl-int.k $SWIFTDIR/libexec/vdl-int.k.vanilla
fi
cp -v libexec/vdl-int.k $SWIFTDIR/libexec/vdl-int.k
