#!/home/espinosa/local/bin/ruby
#!/usr/bin/env ruby

# Script: chtget.rb [file] [hashserver_ip]
# Description: Registers a file [file] with rank rank to the hash service
#              [hashserver_ip]

require 'socket'

client = TCPSocket.new(ARGV[1], 9000)

client.print("get #{ARGV[0]}\n")
puts client.read
client.close
