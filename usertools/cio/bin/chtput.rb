#!/home/espinosa/local/bin/ruby
#!/usr/bin/env ruby

# Script: chtput.rb [file] [rank] [hashserver_ip]
# Description: Registers a file [file] with rank rank to the hash service
#              [hashserver_ip]

require 'socket'

client = TCPSocket.new(ARGV[2], 9000)

client.print "put #{ARGV[0]} #{ARGV[1]}"
client.close
