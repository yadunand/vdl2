#!/fuse/bin/bash
LD_LIBRARY_PATH=/lib:/fuse/lib:/fuse/usr/lib
PATH=/fuse/bin:/fuse/usr/bin:$PATH

i=0
RANK=`echo $CONTROL_INIT | awk -F, '{print $4}'`
IOPATH=`cat /fuse/tmp/iohost`
CHIRP_ADDR=`echo $CHIRP_ADD`
LOG=/dev/shm/collector
echo "CHIRP: $CHIRP_ADD" >> $LOG 2>&1
echo "START RANK: $RANK" >> $LOG 2>&1
function send()
{
    ext=$1
    cd /chirp/multi/${CHIRP_ADDR}@stripe/
    time tar cf /tmp/${RANK}_${i}_${ext}.tar `ls /dev/shm/share/stripe/root/` >> $LOG 2>&1
    DATE=`date`
    echo "${DATE}: ${i}th tar done with exit code $?" >> $LOG 2>&1
    time dd if=/tmp/${RANK}_${i}_${ext}.tar of=${IOPATH}/${RANK}_${i}_${ext}.tar bs=128k >> $LOG 2>&1
    DATE=`date`
    echo "${DATE}: ${i}th copy done with exit code $?" >> $LOG 2>&1
    rm /tmp/${MACH_ID}_${i}_${ext}.tar
    i=$[$i+1]
}

function loopminute()
{
    while true; do
	sleep 60

	num=`ls -l /dev/shm/share/stripe/root/ | wc -l` >> $LOG 2>&1
	DATE=`date`
	echo "${DATE}: there are $num result files in output" >> $LOG 2>&1
	if [ "${num}" -gt "1" ]; then
	    send minute
	fi
	DATE=`date`
        echo "${DATE}: loopminute over" >> $LOG 2>&1
    done
}

function loopsecond()
{
    while true; do
	sleep 1
	filesize=`ls -l /dev/shm/share/stripe/root/ | awk 'NR>1 {a[1]=a[1]+$5} END{print a[1]}'`
	if [ "${filesize}" -gt "268000000" ]; then
	    DATE=`date`
	    echo "${DATE}: $filesize reaches limit" >> $LOG 2>&1
	    mkdir ${FLAG}
	    send seconds
	    rmdir ${FLAG}
	    DATE=`date`
	    echo "${DATE}: $filesize reaches limit over" >> $LOG 2>&1
	fi	    
    done
}

#loopsecond &
loopminute