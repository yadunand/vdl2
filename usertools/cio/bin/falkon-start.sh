#!/bin/bash

if [ -z $CIOROOT ]; then
  echo "ERROR: CIOROOT env not defined"
  exit 1
fi
if [ -z $CIOARCH ]; then
  echo "ERROR: CIOARCH not defined"
  exit 1
fi
if [ -z $CHIRPROOT ]; then
  echo "ERROR: CHIRPROOT not defined"
  exit 1
fi
#$CIOROOT/libexec/falkon/falkon-start-$CIOARCH.sh $@
$CIOROOT/libexec/falkon/falkon-start-bgp_logging.sh $@
