#!/bin/bash

if [ -z "$3" ]; then 
  echo "usage: $0 <FALKON_ID> <NUM_NODES> <swift_script>"
  exit 1
fi

CUR_DIR=`pwd`

FALKON_JOB_ID=$1                                               
NUM_NODES=$2
SWIFT_SCRIPT=$3

SERVICE_LIST_FILE=/home/falkon/users/${USER}/${FALKON_JOB_ID}/config/Client-service-URIs.config
let NUM_ION=NUM_NODES/64

echo "waiting for at least ${NUM_NODES} nodes to register before submitting workload..."
falkon-wait-for-allocation.sh ${SERVICE_LIST_FILE} ${NUM_ION}
                                
echo "found at least ${NUM_NODES} registered, submitting workload..."

workdir=`pwd`/work

# Base entry for localhost

cat >sites.xml <<END
<config>
END

# Create one entry per falkon server (i.e. one per pset)

let s=0
tail +2 $SERVICE_LIST_FILE |
while read ip port; do
  server=`echo $s | awk '{printf "%.3d", $1}'`
  cat >>sites.xml <<END
  <pool handle="BGP_$server">
    <execution provider="deef" url="http://$ip:50001/wsrf/services/GenericPortal/core/WS/GPFactoryService"/>
    <gridftp url="local://localhost"/>
    <workdirectory>$workdir</workdirectory>
    <profile namespace="karajan" key="jobThrottle">2.54</profile>
    <profile namespace="karajan" key="initialScore">1000</profile>
  </pool>

END
  let s=$s+1
done

echo "</config>" >> sites.xml

shift 2   # leave $3 and following - swift script and args

if [ ! -f ./tc.data ]; then
  echo "ERROR: tc.data not found"
  exit 1
fi

swift </dev/null -sites.file ./sites.xml -tc.file ./tc.data $@
