#!/fuse/bin/bash

# Script: ciologic-bgp.sh
# Description: starts ciologic for the BlueGene

# function: start_iptorus
start_iptorus(){
  /soft/apps/ZeptoOS-2.0-V1R3M0/cnbin/cn-ipfwd &
  while [ ! -f /tmp/ifconfig.cmd ]; do
    sleep 1
  done
  . /tmp/ifconfig.cmd
}

# function: get_rank
#  return the rank of the node this is running on
get_rank(){
  echo $CONTROL_INIT | awk -F, '{print $4}'
}

# function: get_ip [rank]
#    given a rank of a node. return its ip address
get_ip(){
  rank=$1
  echo "10.128.$(( rank / 256)).$((rank % 256))"
}

ifs_head(){
  stripe_size=$1
  rank=`get_rank`

  echo "Rank $rank: Starting manager"

  cat > /tmp/manager_config.cfg << EOF
# the recommended stripe width
# this is the number of benefactors the client will strip the data among in the write operation
stripe_width = $stripe_size		

# the size fo the chunk - not fully implemented yet
# chunk_size = 1048576   

# the maximum possible number of benefactors in the system
max_num_ben = 4096

# (Optional) Log mode could be : DEBUG, VERBOS, ERROR, FATAL, OFF
log_mode = OFF

# (Optional) log file name 
# if not provide and the log mode is not OFF, the log messages will be sent to stdout
log_file = /home/espinosa/log/manager_$rank.log
EOF
  cd /tmp
  /home/espinosa/bin/manager 7005 &
  #/home/espinosa/bin/manager 7005 > /dev/null  2> /dev/null &
  ifs_slave $rank
}

# function: ifs_slave [head]
#  Starts the supporting data nodes for the stripe to the
#  [head] node.
 
ifs_slave(){
  ifs_rank=`get_ip $1`
  rank=`get_rank`

  sleep 30 # Wait for manager
  echo "Rank XX: starting benefactor"

  mkdir -p /dev/shm/share
  cat > /tmp/benefactor_config.cfg << EOF
# the hostname or the IP address of the manager 
manager_name = $ifs_rank

# manager port number
manager_port = 7005

# The path to the local directory where the benefactor will store the chunks
benefactor_path = /dev/shm/share

# Aggregation type, this could be <DISK> or <MEMORY>
# <DISK> is a typical setting for general workloads
# <MEMORY> is under development
aggregation_type = DISK

# The donated disk space size in MB
disk_space_size = 512

# The donated memory space size in MB, this is effective if aggregation type= MEMORY
memory_space_size = 512

# The manager update period in seconds
update_period = 5

# (Optional) The local address the benefactor should use - specially in multihomed machines 
#benefactor_address = 

# (Optional) Log mode could be : DEBUG, VERBOS, ERROR, FATAL, OFF
log_mode = OFF

# (Optional) log file name 
# if not provide and the log mode is not OFF, the log messages will be sent to stdout
log_file = /home/espinosa/log/benefactor_$rank.log

EOF
  cd /tmp
  /home/espinosa/bin/benefactor 
}

ifs_mount() {
  ifs_rank=`get_ip $1`
  rank=`get_rank`

  sleep 30 # Wait for manager
  echo "Rank $rank: Mounting IFS"

  cat > /tmp/flfs_config.cfg << EOF
# the hostname or the IP address of the manager 
manager_name = $ifs_rank


# manager port number
manager_port = 7005


# execution mode - not fully implemented yet - DEBUG is the only option for now		 
execution_mode = DEBUG


# Naming scheme - to name chunk by sequence number <SEQNUM> or by hash <HASH>
# <SEQNUM> the chunks will be named by sequence number, this is a typical setting for general workloads
# <HASH> this option is for content addressablity feature
chunk_naming = SEQNUM


# Commit scheme - specifies whether to overwrite, non-overwrite or version the previous copy.
# possible values include <NOOVERWRITE> , <OVERWRITE>, and <VERSIONING>
# <NOOVERWRITE> : writing a new file with the same name as an existing file will fail.
# <OVERWRITE>   : if a new file is stored in the system with the same file name as an existing file name,
#		  the new file will overwrite the old file
# <VERSIONING>  : if a new file is stored in the system with the same file name as an existing file name, 
#		  the new file will be store as a new version of the file
commit_scheme = NOOVERWRITE


# Number of chunks to reserve in the repository
num_reserve_chunks = 1024

# Write Interface type - to select the write interface type, the following are the write interfaces
# <SLIDINGWINDOWWRITE> :  Sliding window write interface, this is the typical setting.
# <INCREMENTALWRITE>   :  Incremental write interface
# <COMPLETELOCALWRITE> :  The Complete local write interface
# <INCREMENTALWRITE> and <COMPLETELOCALWRITE> use the local disk in the write operation. these two are not extensively tested.
write_interface_type = SLIDINGWINDOWWRITE


#the memory space allocated for the buffers in the write operations, in MB, 
# effects Sliding window interface only
memory_size = 256


# if <INCREMENTALWRITE> is the selected write interface <inc_write_file_size> spacifies 
# the size of the temporary local files in number of chunks
inc_write_file_size = 64


# if <INCREMENTALWRITE> of <COMPLETELOCALWRITE> is the selected write interface <local_write_directory> spacifies 
# the path to the directory where the temporary files will be saved
local_write_directory = /tmp/FLIncW

# Read Interface type - to select the read interface type, currently <FBR> is only implemented
# <FBR> :  Fixed Buffer per Request read interface
read_interface_type = FBR


# if <FBR> is the selected read interface <fbr_request_buffer_size> specifies 
# the number of chunks allocated for every request
fbr_request_buffer_size = 4

# Number of threads per write agent ( there is an agent per benefactor )
num_threads_per_agent = 1


# Cache update period in seconds, if this value is set to 0 then the cache is disabled
cache_update_period = 5


# (Optional) Log mode could be : DEBUG, VERBOS, ERROR, FATAL, OFF
log_mode = OFF

# (Optional) log file name 
# if not provide and the log mode is not OFF, the log messages will be sent to stdout
log_file = /home/espinosa/log/mosastore_$rank.log

EOF
  mkdir -p /dataifs
  cd /tmp
  /home/espinosa/bin/mosastore -o direct_io -o sync_read /dataifs &
  #/home/espinosa/bin/mosastore -o direct_io -o sync_read /dataifs -d 2> /dev/null > /dev/null &
}

# Main
#

# Check sanity of environment
if [ -z $CIOROOT ]; then
  echo "CIOROOT not defined"
  exit 1
fi
if [ -z $CIOARCH ]; then
  echo "CIOARCH note defined"
  exit 1
fi

# BGP specific initialization
LD_LIBRARY_PATH=/lib:/fuse/lib:/fuse/usr/lib
PATH=/fuse/bin:/fuse/usr/bin:$PATH

# Initiate IP over Torus
start_iptorus

# Process args
PART_SIZE=$8 # also BG_SIZE
IFS_NUM=$9
STRIPE_SIZE=${10}

# Compute rank
RANK=`get_rank`
IFS_GRP=$(( PART_SIZE / IFS_NUM ))
IFS_RANK=$(( RANK / IFS_GRP + 1 ))
IFS_SLAVE=$(( IFS_RANK + STRIPE_SIZE ))
export IFS_RANK
export CHIRP_ADD=`get_ip $IFS_RANK`

# Save rank information
echo $RANK > /dev/shm/RANK
echo $IFS_RANK > /dev/shm/IFS_RANK
echo $IFS_SLAVE > /dev/shm/IFS_SLAVE

#Core CDM logic
if [ $IFS_NUM -ne 0 ]; then
	if [ $RANK -eq $IFS_RANK ]; then
		ifs_head $STRIPE_SIZE
	elif [[ $RANK -lt $IFS_SLAVE && $RANK -gt $IFS_RANK ]]; then
		ifs_slave $IFS_RANK
    else
      if [ $RANK -eq 0 ]; then
        ifs_mount $IFS_RANK
        $CIOROOT/libexec/falkon/runworker-bgp.sh $1 $2 $3 $4 $5 $6 $7
      fi
    fi
else
  if [ $RANK -eq 0 ]; then
    ifs_mount $IFS_RANK
    $CIOROOT/libexec/falkon/runworker-bgp.sh $1 $2 $3 $4 $5 $6 $7
  fi
fi

# Quick hack
# Make sure this job does not finish
# Guarantee to not miss any background processes
sleep 30d
