#!/fuse/bin/bash

# Script      : ifsslave.sh
# Description : Allocates space in the compute node to contribute
#               space to the head IFS.

cd /tmp

DATACN=$1

export LD_LIBRARY_PATH=/lib:/fuse/lib:/usr/lib

if [ -d /dev/shm/indiv ]; then
  rm -rf /dev/shm/indiv 
fi
mkdir /dev/shm/indiv
cat > /tmp/benefactor_config.cfg << EOF
# the hostname or the IP address of the manager 
manager_name = $DATACN

# manager port number
manager_port = 7005

# The path to the local directory where the benefactor will store the chunks
benefactor_path = /dev/shm/indiv

# Aggregation type, this could be <DISK> or <MEMORY>
# <DISK> is a typical setting for general workloads
# <MEMORY> is under development
aggregation_type = DISK

# The donated disk space size in MB
disk_space_size = 512

# The donated memory space size in MB, this is effective if aggregation type= MEMORY
memory_space_size = 512

# The manager update period in seconds
update_period = 5

# (Optional) The local address the benefactor should use - specially in multihomed machines 
#benefactor_address = 
EOF
cd /tmp
sleep 30
#RANK=`/home/espinosa/bin/rank`
#/home/espinosa/bin/benefactor  -d >&2 2> /home/espinosa/log/$RANK.ben
/home/espinosa/bin/benefactor  2>&1 > /dev/null
