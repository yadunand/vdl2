#!/bin/sh

# Script      : ifshead.sh
# Description : Starts the IFS directory service


DATACN=$1
STR=$2

export LD_LIBRARY_PATH=/lib:/fuse/lib:/usr/lib

if [ -d /dev/shm/indiv ];
then
  rm -rf /dev/shm/indiv 
fi
mkdir /dev/shm/indiv
cat > /tmp/benefactor_config.cfg << EOF
# the hostname or the IP address of the manager 
manager_name = $DATACN

# manager port number
manager_port = 7005

# The path to the local directory where the benefactor will store the chunks
benefactor_path = /dev/shm/indiv

# Aggregation type, this could be <DISK> or <MEMORY>
# <DISK> is a typical setting for general workloads
# <MEMORY> is under development
aggregation_type = DISK

# The donated disk space size in MB
disk_space_size = 512

# The donated memory space size in MB, this is effective if aggregation type= MEMORY
memory_space_size = 512

# The manager update period in seconds
update_period = 5

# (Optional) The local address the benefactor should use - specially in multihomed machines 
#benefactor_address = 
EOF

cat > /tmp/manager_config.cfg << EOF
# the recommended stripe width
# this is the number of benefactors the client will strip the data among in the write operation
stripe_width = $STR		

# the size fo the chunk - not fully implemented yet
# chunk_size = 1048576   

# the maximum possible number of benefactors in the system
max_num_ben = 4096
EOF
RANK=`/home/espinosa/bin/rank`
cd /tmp
#/home/espinosa/bin/manager 7005 > /home/espinosa/log/$RANK.mgr  2> /home/espinosa/log/$RANK-err.mgr &
/home/espinosa/bin/manager 7005 2>&1 > /dev/null &

sleep 30
#/home/espinosa/bin/benefactor  -d >&2 2> /home/espinosa/log/$RANK.ben
/home/espinosa/bin/benefactor 2>&1 > /dev/null
