#!/usr/bin/env ruby -n

BEGIN {
  puts "Timestamp\t\
Total messages sent\t\
Total bytes sent\t\
Total messages received\t\
Total bytes received\t\
IP fwd messages sent\t\
IP fwd bytes sent\t\
IP fwd messages received\t\
IP fwd bytes received\t\
Stream messages sent\t\
Stream bytes sent\t\
Stream messages received\t\
Stream bytes received\t\
Broadcast messages sent\t\
Broadcast bytes sent\t\
Internal messages sent\t\
Internal bytes sent\t\
Internal messages received\t\
Internal bytes received\t\
Plugin 1 messages sent\t\
Plugin 1 bytes sent\t\
Plugin 1 messages received\t\
Plugin 1 bytes received\t\
Plugin 5 messages sent\t\
Plugin 5 bytes sent\t\
Plugin 5 messages received\t\
Plugin 5 bytes received\t\
Plugin 2 messages sent\t\
Plugin 2 bytes sent\t\
Plugin 2 messages received\t\
Plugin 2 bytes received"

}

print "#{$_.split(':')[1].chomp.gsub(/\t/,'')}\t"

if $_ =~ /Plugin 2 bytes received/
  print "\n"
end
