#!/home/aespinosa/local/bin/ruby -na
#

BEGIN {
  READ = 'read\('
  OPEN = 'open\('
  WRITE = 'write\('
  $desc = {}
  $read = Hash.new(0)
  $write = Hash.new(0)
  $desc[0] = "STDIN"
  $desc[1] = "STDOUT"
  $desc[2] = "STDERR"
  def tosec(time)
    x=time.split(/:/)
	x[0].to_f * 3600.0 + x[1].to_f * 60.0 + x[2].to_f
  end
}

now = $F[0]
retcode = $_.split("=")[1].to_i
if $_ =~ /\(.*\)/
  args = $&.gsub( /^\(|\)$/, "" ).split ','
end

if $. == 1
  start = tosec(now)
end

if $_ =~ /#{READ}/
 file = $desc[ args[0].to_i ]
 $read[file] += retcode
elsif $_ =~ /#{WRITE}/
 file = $desc[ args[0].to_i ]
 $write[file] += retcode
elsif $_ =~ /#{OPEN}/ and retcode > -1
  $desc[retcode] = args[0].gsub(/"/,"")
end

prev = now

END {
  puts "Read summary\n------------"
  $read.each { |file, size|
    puts "#{file}: #{size} bytes"
  }
  puts "Write summary\n-----------"
  $write.each { |file, size|
    puts "#{file}: #{size} bytes"
  }
}
