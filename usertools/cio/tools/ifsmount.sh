#!/bin/sh


# Script      : ifsmount.sh
# Description : Mounts the IFS associated with this Worker CN

DATACN=$1
RANK=`/home/espinosa/bin/rank`

export LD_LIBRARY_PATH=/lib:/fuse/lib:/usr/lib
cd /tmp

cat > /tmp/flfs_config.cfg << EOF
# the hostname or the IP address of the manager 
manager_name = $DATACN


# manager port number
manager_port = 7005


# execution mode - not fully implemented yet - DEBUG is the only option for now		 
execution_mode = DEBUG


# Naming scheme - to name chunk by sequence number <SEQNUM> or by hash <HASH>
# <SEQNUM> the chunks will be named by sequence number, this is a typical setting for general workloads
# <HASH> this option is for content addressablity feature
chunk_naming = SEQNUM


# Commit scheme - specifies whether to overwrite, non-overwrite or version the previous copy.
# possible values include <NOOVERWRITE> , <OVERWRITE>, and <VERSIONING>
# <NOOVERWRITE> : writing a new file with the same name as an existing file will fail.
# <OVERWRITE>   : if a new file is stored in the system with the same file name as an existing file name,
#		  the new file will overwrite the old file
# <VERSIONING>  : if a new file is stored in the system with the same file name as an existing file name, 
#		  the new file will be store as a new version of the file
commit_scheme = NOOVERWRITE


# Number of chunks to reserve in the repository
num_reserve_chunks = 1024

# Write Interface type - to select the write interface type, the following are the write interfaces
# <SLIDINGWINDOWWRITE> :  Sliding window write interface, this is the typical setting.
# <INCREMENTALWRITE>   :  Incremental write interface
# <COMPLETELOCALWRITE> :  The Complete local write interface
# <INCREMENTALWRITE> and <COMPLETELOCALWRITE> use the local disk in the write operation. these two are not extensively tested.
write_interface_type = SLIDINGWINDOWWRITE


#the memory space allocated for the buffers in the write operations, in MB, 
# effects Sliding window interface only
memory_size = 256


# if <INCREMENTALWRITE> is the selected write interface <inc_write_file_size> spacifies 
# the size of the temporary local files in number of chunks
inc_write_file_size = 64


# if <INCREMENTALWRITE> of <COMPLETELOCALWRITE> is the selected write interface <local_write_directory> spacifies 
# the path to the directory where the temporary files will be saved
local_write_directory = /tmp/FLIncW

# Read Interface type - to select the read interface type, currently <FBR> is only implemented
# <FBR> :  Fixed Buffer per Request read interface
read_interface_type = FBR


# if <FBR> is the selected read interface <fbr_request_buffer_size> specifies 
# the number of chunks allocated for every request
fbr_request_buffer_size = 4

# Number of threads per write agent ( there is an agent per benefactor )
num_threads_per_agent = 1
# Cache update period in seconds
cache_update_period = 5
dbg_file_name = /dev/null


EOF
#/home/espinosa/bin/mosastore -o direct_io -o sync_read /dataifs
#/home/espinosa/bin/mosastore -o sync_read /dataifs -d >&2 2> /home/espinosa/log/$RANK.mos &
/home/espinosa/bin/mosastore -o sync_read /dataifs &

