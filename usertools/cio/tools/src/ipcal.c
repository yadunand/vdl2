#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[])
{
	unsigned long network;
	unsigned long rank;
	char dot;
	unsigned long a, b, c, d;
	network = 0;
	sscanf(argv[1], "%d.%d.%d.%d", &a, &b, &c, &d);
	network = network | (a << 24) | (b << 16) | (c << 8) | d;
	sscanf( argv[2], "%d", &rank );
	unsigned long ip = network | rank;
	printf("%ld.%ld.%ld.%ld\n", (ip & 0xff000000)>>24, (ip & 0x00ff0000)>>16, (ip & 0x0000ff00)>>8, ip & 0x000000ff);
	return 0;
}

