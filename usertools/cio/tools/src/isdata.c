/*
 isdata.c
   Utility to get the role of the CN in the CIO ecosystem

 gcc -mpowerpc -Wall -I/bgsys/drivers/ppcfloor/arch/include/  -o isdata isdata.c 

 aespinosa@cs.uchicago.edu
*/

#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <common/bgp_personality_inlines.h>

static _BGP_Personality_t pers;

int main(int argc, char* argv[])
{
    int fd;
    int rc;

    fd = open("/proc/personality", O_RDONLY);
    if( fd < 0 )
    {
	    fprintf(stderr,"Unable to open /proc/personality");
	    exit(1);
    }
    rc = read(fd, (char*)&pers, sizeof(_BGP_Personality_t));
    if( rc!=sizeof(_BGP_Personality_t) )
    {
	    fprintf(stderr,"Failed to read /proc/personality");
	    exit(1);
    }
    close(fd);
/*
 * usage:
 *  isdata [PART_SIZE] [DNUM] [STR]
 *    [ PART_SIZE ] - total number of nodes/ size of job partition
 *    [ DNUM ] - number of data CNs in the partition
 *    [ STR ] - number of data CNs to stripe together
 *
 */
    
    int psize, dnum, str;
    sscanf( argv[1], "%d", &psize );
    sscanf( argv[2], "%d", &dnum );
    sscanf( argv[3], "%d", &str );
    int *target_rank = (int*) malloc( dnum * sizeof(int) );

    int i;
    int flag = 0;

    for ( i = 0; i < dnum; i++ )
    {
        target_rank[i] = psize * ( 2*i + 1 ) / ( 2*dnum );
        if ( pers.Network_Config.Rank == target_rank[i] )
        {
            printf("head");
            flag = 1;
            break;
        }
        else if ( target_rank[i] < pers.Network_Config.Rank 
            && pers.Network_Config.Rank < target_rank[i] + str )
        {
            printf("slave");
            flag = 1;
            break;
        }
    }
    if( !flag )
    {
        printf("app");
    }
    return 0;
}
