/*
 BGP personality sample code


 gcc -mpowerpc -Wall -I/bgsys/drivers/ppcfloor/arch/include/  -o pers pers.c 

 kazutomo@mcs.anl.gov
*/

#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <common/bgp_personality_inlines.h>

static _BGP_Personality_t pers;

int main()
{
    int fd;
    int rc;

    fd = open("/proc/personality", O_RDONLY);
    if( fd < 0 ) {
	fprintf(stderr,"Unable to open /proc/personality");
	exit(1);
    }
    rc = read(fd, (char*)&pers, sizeof(_BGP_Personality_t));
    if( rc!=sizeof(_BGP_Personality_t) ) {
	fprintf(stderr,"Failed to read /proc/personality");
	exit(1);
    }
    close(fd);

    printf("%d\n",pers.Network_Config.Rank);

    return 0;
}

