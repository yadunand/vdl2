#!/home/espinosa/local/bin/ruby

require 'socket'
require 'scanf'

server = TCPServer.new( nil, 9000 )
file = Hash.new { |hash, key| hash[key] = Array.new }

while true
  Thread.start server.accept do |session|
    (method, fname, rank) = session.gets.scanf "%s %s %d\n"
    if method == "put"
      file[fname] << rank
    elsif method == "get"
      session.print "#{file[fname][ rand( file[fname].size ) ]}\n"
    end
    session.close
  end
end
