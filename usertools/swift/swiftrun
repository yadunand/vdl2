
# swiftrun scriptname.swift -arg1 ... -argN site1 ... siteN

export SCRIPT=$1
shift

declare -a swiftargs
declare -a sites

for a in $*; do
  case $a in
    -*)
      swiftargs=( "${swiftargs[@]}" "$a" )
      ;;
    *)
      sites=( "${sites[@]}" "$a" )
      ;;
  esac
done

# Ensure that proxy has > 1 hour remaining

$SWIFT_HOME/bin/grid-proxy-info | grep timeleft | grep ' h,' >/dev/null
if [ $? -ne 0 ]; then
    echo $0: Proxy has less than 60 minutes remaining - exiting.
    exit 1
fi

# Create a sites.xml file in cwd for this run

site ${sites[@]} >sites.xml

# Run Swift in a subshell to capture stdout/stderr to swift.out

( echo $0: Swift script $SCRIPT starting at `date`
  echo   running on sites: ${sites[@]}
  echo
  swift </dev/null \
   -sites.file ./sites.xml \
   -tc.file ./tc.data \
   $SCRIPT ${swiftargs[@]}
  echo
  echo $0: Swift Script $SCRIPT ended at `date` with exit code $?
) >swift.out 2>&1
