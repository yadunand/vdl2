
// ----------------------------------------------------------------------
// This code is developed as part of the Java CoG Kit project
// The terms of the license can be found at http://www.cogkit.org/license
// This message may not be removed or altered.
// ----------------------------------------------------------------------

package org.globus.cog.abstraction.impl.file.wonky;

/**
 * Extends the base class TaskHandlerImpl  in org.globus.cog.core.impl.file 
 */
public class TaskHandlerImpl
    extends org.globus.cog.abstraction.impl.file.TaskHandlerImpl {
}
