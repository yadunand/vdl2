//----------------------------------------------------------------------
//This code is developed as part of the Java CoG Kit project
//The terms of the license can be found at http://www.cogkit.org/license
//This message may not be removed or altered.
//----------------------------------------------------------------------

package org.globus.cog.abstraction.impl.execution.wonky;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.globus.cog.abstraction.impl.common.AbstractDelegatedTaskHandler;
import org.globus.cog.abstraction.impl.common.StatusImpl;
import org.globus.cog.abstraction.impl.common.execution.JobException;
import org.globus.cog.abstraction.impl.common.task.IllegalSpecException;
import org.globus.cog.abstraction.impl.common.task.InvalidSecurityContextException;
import org.globus.cog.abstraction.impl.common.task.InvalidServiceContactException;
import org.globus.cog.abstraction.impl.common.task.TaskSubmissionException;
import org.globus.cog.abstraction.impl.common.util.NullOutputStream;
import org.globus.cog.abstraction.impl.common.util.OutputStreamMultiplexer;
import org.globus.cog.abstraction.interfaces.FileLocation;
import org.globus.cog.abstraction.interfaces.JobSpecification;
import org.globus.cog.abstraction.interfaces.ServiceContact;
import org.globus.cog.abstraction.interfaces.Status;
import org.globus.cog.abstraction.interfaces.Task;

public class JobSubmissionTaskHandler extends AbstractDelegatedTaskHandler implements 
        Runnable {
    private static Logger logger = Logger.getLogger(JobSubmissionTaskHandler.class);

    static Random r = new Random();

    static List firstJobList = Collections.synchronizedList(new ArrayList());
    static Map siteStates = Collections.synchronizedMap(new HashMap());

    static int globalJobNumber = 0;
    int jobNumber = globalJobNumber++; // racy?

    List siteOptions;

    String siteName;


    public static final int BUFFER_SIZE = 1024;

    private Thread thread = null;
    private Process process;
    private volatile boolean killed;

    public void submit(Task task) throws IllegalSpecException,
            InvalidSecurityContextException, InvalidServiceContactException,
            TaskSubmissionException {
        checkAndSetTask(task);
        logger.info("Submitting wonky job "+jobNumber);


        ServiceContact serviceContact = task.getService(0)
            .getServiceContact();
        String server = serviceContact.getContact();

        logger.debug("Contact is: "+server);
        String[] splitServer = server.split("/");
        siteName = splitServer[0];
        siteOptions = Arrays.asList(splitServer);

        if(!failDelay("submitting")) {
            task.setStatus(Status.FAILED);
            return;
        }
        task.setStatus(Status.SUBMITTING);
        JobSpecification spec;
        try {
            spec = (JobSpecification) task.getSpecification();
        } catch (Exception e) {
            throw new IllegalSpecException(
                    "Exception while retrieving Job Specification", e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(spec.toString());
        }

        Collection attributeNames = spec.getAttributeNames();
        Iterator attributeIterator = attributeNames.iterator();

        if(siteOptions.contains("strictattr")) {
            logger.debug("Strict attribute checking is enabled");
            while(attributeIterator.hasNext()) {
                String attrName = (String)attributeIterator.next();  
                if(!siteOptions.contains("permitattr=" + attrName)) {
                    logger.error("Job specification attribute was passed that should not have been: "+attrName);
                    task.setStatus(Status.FAILED);
                    return;
                }
            }
        }

        try {
            if (logger.isInfoEnabled()) {
                logger.info("Submitting task " + task);
            }
            synchronized(this) {
                this.thread = new Thread(this);
                if (task.getStatus().getStatusCode() != Status.CANCELED) {
                    if(!failDelay("submitted")) {
                        task.setStatus(Status.FAILED);
                        return;
                    }
                    task.setStatus(Status.SUBMITTED);
                    this.thread.start();
                    if (spec.isBatchJob()) {
                        task.setStatus(Status.COMPLETED);
                    }
                }
            }
        } catch (Exception e) {
            throw new TaskSubmissionException("Cannot submit job", e);
        }
    }

    public void suspend() throws InvalidSecurityContextException,
            TaskSubmissionException {
        logger.info("Suspend called on wonky job "+jobNumber);
    }

    public void resume() throws InvalidSecurityContextException,
            TaskSubmissionException {
        logger.info("Resume called on wonky job "+jobNumber);
    }

    public void cancel(String message) throws InvalidSecurityContextException,
            TaskSubmissionException {
        logger.info("Cancel called on wonky job "+jobNumber);
        synchronized(this) {
            killed = true;
            process.destroy();
            getTask().setStatus(new StatusImpl(Status.CANCELED, message, null));
        }
    }

    private static final FileLocation REDIRECT_LOCATION = FileLocation.MEMORY
            .and(FileLocation.LOCAL);

    public void run() {
        try {
            JobSpecification spec = (JobSpecification) getTask()
                    .getSpecification();

            File dir = null;
            if (spec.getDirectory() != null) {
                dir = new File(spec.getDirectory());
            }

            if(siteOptions.contains("wrongdirectory")) {
                File newdir = new File("/tmp");
                // this is to so that we don't use /tmp as the wrong
                // directory if that happens to be what the user asked for
                if(dir.equals(newdir)) {
                    dir = new File("/");
                } else {
                    dir = newdir;
                }
                logger.info("Deliberately using wrong working directory: "+dir);
            }


            logger.info("Wonky job in queue, job number "+jobNumber);
            String[] cmdarray = buildCmdArray(spec, jobNumber);
            try {
                logger.info("wonky site is "+siteName);
                if(siteOptions.contains("firstfast") && !firstJobList.contains(siteName)) {
                    logger.info("not sleeping - this is the first job on site "+siteName+", "+jobNumber);
                    firstJobList.add(siteName);
                } else {
                    double qmean=0;
                    double qstddev=0;

                    Iterator it = siteOptions.iterator();
                    while(it.hasNext()) {
                        String op = (String) it.next();
                        logger.debug("Checking: "+op);
                        if(op.startsWith("qmean=")) {
                            qmean = Double.parseDouble(op.substring(6));
                            logger.debug("qmean = "+qmean);
                        } else if(op.startsWith("qstddev=")) {
                            qstddev = Double.parseDouble(op.substring(8));
                            logger.debug("qstddev = "+qstddev);
                        }
                    }

                    double qdelay = qmean + qstddev * r.nextGaussian();

                    Thread.sleep((long)(qdelay*1000));
                }
            } catch(InterruptedException ie) {
                logger.warn("fake queue delay interrupted for job "+jobNumber);
            }
            logger.info("Wonky job running now");
            process = Runtime.getRuntime().exec(cmdarray, buildEnvp(spec), dir);
            if(!failDelay("active")) {
                getTask().setStatus(Status.FAILED);
                return;
            }
            getTask().setStatus(Status.ACTIVE);

            // reusable byte buffer
            byte[] buf = new byte[BUFFER_SIZE];
            /*
             * This should be interleaved with stdout processing, since a
             * process could block if its STDOUT is not consumed, thus causing a
             * deadlock
             */
            processIN(spec.getStdInput(), dir, buf);

            if (!FileLocation.NONE.equals(spec.getStdOutputLocation())) {
                String out = processOUT(spec.getStdOutput(), spec
                        .getStdOutputLocation(), dir, buf, process
                        .getInputStream());
                if (out != null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("STDOUT from job: " + out);
                    }
                    getTask().setStdOutput(out);
                }
            }

            if (!FileLocation.NONE.equals(spec.getStdErrorLocation())) {
                String err = processOUT(spec.getStdError(), spec
                        .getStdErrorLocation(), dir, buf, process
                        .getErrorStream());
                if (err != null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("STDERR from job: " + err);
                    }
                    getTask().setStdError(err);
                }
            }

            if (spec.isBatchJob()) {
                return;
            }

            int exitCode = process.waitFor();
            if (logger.isDebugEnabled()) {
                logger.debug("Exit code was " + exitCode);
            }
            if (killed) {
                return;
            }

            if(siteOptions.contains("nofailonexit"))  {
                // suppress failures caused by exit code
                getTask().setStatus(Status.COMPLETED);
            } else { // normal fail behaviour
                if (exitCode == 0) {
                    if(failDelay("completed")) {
                        if (siteOptions.contains("runawayjob") && jobNumber == 0) {
                            System.out.println("Wonky job completion notification gets lost");
                        }
                        else {
                            getTask().setStatus(Status.COMPLETED);
                        }
                    } else {
                        getTask().setStatus(Status.FAILED);
                    }
                } else {
                    throw new JobException(exitCode);
                }
            }
        } catch (Exception e) {
            if (killed) {
                return;
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Exception while running local executable", e);
            }
            failTask(null, e);
        }
    }

    protected void processIN(String in, File dir, byte[] buf)
            throws IOException {
        if (in != null) {
            OutputStream out = process.getOutputStream();

            File stdin;
            if (dir != null) {
                stdin = new File(dir, in);
            } else {
                stdin = new File(in);
            }

            FileInputStream file = new FileInputStream(stdin);
            int read = file.read(buf);
            while (read != -1) {
                out.write(buf, 0, read);
                read = file.read(buf);
            }
            file.close();
        }
    }

    protected String processOUT(String out, FileLocation loc, File dir,
            byte[] buf, InputStream pin) throws IOException {

        OutputStream os = null;
        ByteArrayOutputStream baos = null;
        if (FileLocation.MEMORY.overlaps(loc)) {
            baos = new ByteArrayOutputStream();
            os = baos;
        }
        if ((FileLocation.LOCAL.overlaps(loc) || FileLocation.REMOTE
                .equals(loc))
                && out != null) {
            if (os != null) {
                os = new OutputStreamMultiplexer(os, new FileOutputStream(out));
            }
            else {
                os = new FileOutputStream(out);
            }
        }
        if (os == null) {
            os = new NullOutputStream();
        }

        int len = pin.read(buf);
        while (len != -1) {
            os.write(buf, 0, len);
            len = pin.read(buf);
        }
        os.close();
        if (baos != null) {
            return baos.toString();
        } else {
            return null;
        }
    }

    private String[] buildCmdArray(JobSpecification spec, int jobid) {
        List arguments = spec.getArgumentsAsList();
        String[] cmdarray = new String[arguments.size() + 1];

        logger.debug("jobid "+jobid+" :"); // this loop is a console race...
        cmdarray[0] = spec.getExecutable();
        Iterator i = arguments.iterator();
        int index = 1;
        while (i.hasNext()) {
            String s = (String)i.next();
            logger.debug(s);
            cmdarray[index++] = s;

        }
        logger.debug("end of args");
        return cmdarray;
    }

    private String[] buildEnvp(JobSpecification spec) {
        Collection names = spec.getEnvironmentVariableNames();
        if (names.size() == 0) {
            /*
             * Questionable. An envp of null will cause the parent environment
             * to be inherited, while an empty one will cause no environment
             * variables to be set for the process. Or so it seems from the
             * Runtime.exec docs.
             */
            return null;
        }
        String[] envp = new String[names.size()];
        Iterator i = names.iterator();
        int index = 0;
        while (i.hasNext()) {
            String name = (String) i.next();
            envp[index++] = name + "=" + spec.getEnvironmentVariable(name);
        }
        return envp;
    }

    boolean failDelay(String name) {
        double failRate = getWonkyParam(name+"fail");
        double delayTime = getWonkyParam(name+"delay");
        double failFirstCount = getWonkyParam(name+"failfirstcount");

        if(failFirstCount > 0) {
            logger.info("State "+name+" will fail first "+failFirstCount+" times");
            String key = getSiteName() + ":" + name + "failfirstcountsofar";
            Integer iObj = (Integer)siteStates.get(key);
            if(iObj == null) {
                iObj = new Integer(0);
                siteStates.put(key,iObj);
                
            } 
            int i = iObj.intValue();
            i++;
            siteStates.put(key,new Integer(i));
            logger.info("new value is "+i);
            if(i<=failFirstCount) {  // not failed enough
                logger.info("Failing because early job");
                return false; 
            }
        }

        logger.info("State "+name+" with probability of failure "+failRate+": ");
        if(Math.random()<failRate) {
            logger.info("Fail.");
            return false;
        } else {
            logger.info("Success.");
            try { Thread.sleep((long)(delayTime * 1000)); }
            catch(InterruptedException ie) {
                logger.warn("Warning: wonky delay was interrupted");
            }
            return true;
        }
    }

    double getWonkyParam(String name) {
        Iterator it = siteOptions.iterator();
        while(it.hasNext()) {
            String op = (String) it.next();
            logger.debug("Checking: "+op+" against '"+name+"='");

            if(op.startsWith(name+"=")) {
                String value = op.substring(name.length()+1);
                logger.debug("Parameter "+name+" has value "+value);
                return Double.parseDouble(value);
            }
        }
        return 0;
    }

    String getSiteName() {
        String siteName = (String) siteOptions.get(0);
        logger.info("Site name is "+siteName);
        return siteName;
    }
}
