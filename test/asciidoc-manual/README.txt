
This is the asciidoc version of the Swift User Guide

Notes:

AsciiDoc User Guide:
http://www.methods.co.nz/asciidoc/userguide.html

Just run:
asciidoc manual.txt
which generates manual.html

Word-wrap at column 70 via emacs ESC-q (fill-paragraph).

To get word-wrap to work well in bullet and numbered lists, you must
provide a blank line between the items.

Numbered lists are not working right in some cases after a code
listing block- that's why there are some uses of the [start=*]
attribute.

Installing asciidoc:

$ wget http://sourceforge.net/projects/asciidoc/files/asciidoc/8.6.4/asciidoc-8.6.4.tar.gz/download
$ tar -xzf asciidoc-8.6.4.tar.gz
$ cd asciidoc-8.6.4
$ ./configure --prefix $HOME/asciidoc
$ make install
$ PATH=$PATH:$HOME/asciidoc/bin
$ asciidoc -n manual.txt # creates manual.html

Add -a toc for table of contents.
