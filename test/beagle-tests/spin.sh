#!/bin/sh

# On the Beagle compute node: 1 MLOOP = 22 sec

MLOOPS=$1

M=$(( 1024*1024 ))
X=0
for (( j=0 ; j<${M} ; j++ ))
do
  for (( i=0 ; i<${MLOOPS} ; i++ ))
  do
    (( X++ ))
    (( X = X % 4000 ))
  done
done

exit 0
