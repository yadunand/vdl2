#!/bin/sh

JOBS=$1
DURATION=$2

echo $JOBS $DURATION

SPIN=/home/wozniak/pbs-tests/spin.sh

job()
{
  ID=$1

  START=$( date +%s )

  HOST=$( hostname )
  # sleep ${DURATION}
  ${SPIN} ${DURATION}

  STOP=$( date +%s )

  echo "${ID}: ${HOST} ${START} ${STOP}"
}

for (( i=0 ; i<$JOBS ; i++ ))
do
  echo fork
  job ${i} &
done

wait

exit 0
