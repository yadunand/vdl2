
Simple tests to verify the number of
cores given on the Beagle PBS system.

You can run sleep or spin as the user job.
spin.sh is good to verify that you are
actually getting the full performance of all
of the requested cores.
