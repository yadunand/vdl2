
#include <builtins.swift>
#include <assert.swift>
#include <files.swift>
#include <io.swift>
#include <string.swift>
#include <sys.swift>

app (file o) mysim5 (int timesteps, int rnge, file bias, int count)
{
  "random5.sh" timesteps rnge bias count @stdout=o;
}

app (file o) analyze (file s[])
{
  "avg.sh" s @stdout=o;
}


main
{
  file sims[];
  int  nsim  = toint(argv("nsim",  "10"));

  int  steps = toint(argv("steps", "1"));
  int  rnge = toint(argv("range", "100"));
  int  count = toint(argv("count", "10"));

  string work = argv("work");

  file bias = input_file(work+"/data/bias.dat");

  foreach i in [0:nsim-1] {
    file simout <work+sprintf("/data/sim_%i.out",i)>;
    simout = mysim5(steps,rnge,bias,count);
    sims[i] = simout;
  }

  file stats<work+"/output/average.out">;
  stats = analyze(sims);
  wait (stats) {
    printf("SUCCESS!");
  }
}
