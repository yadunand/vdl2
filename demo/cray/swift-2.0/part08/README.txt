
SUMMARY:

This test runs the p8.swift script, running some simple scripts as
apps and producing output in the user directory.

USAGE:

Make a work directory called, say, WORK

$ cd WORK

# Copy in the Swift script:

$ cp -r /home/users/p01577/Public/demo/part08/p8.swift .

# Copy in the input data:

$ cp -r /home/users/p01577/Public/demo/part08/data .

$ PATH=$PATH:/home/users/p01577/Public/demo/bin

$ run-swift-2-qsub.sh p8.swift --work=$PWD

# Inspect the output:

$ ls output

$ ls *.OU

FURTHER:

run-swift-2-qsub.sh compiles and runs the Swift script.  It sets
everything up, including the locations of Swift programs.

run-swift-2-qsub.sh is user code.  You can edit this
to change compiler options, run time settings, etc.

p8.swift is also user code.

run-swift-2-qsub.sh calls turbine-aprun-run.zsh which is the provided
interface to Swift.  This script is documented at:
http://www.mcs.anl.gov/exm/local/guides/turbine-sites.html#_raven
