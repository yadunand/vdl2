#!/bin/bash

SCRIPT_SWIFT=$1
shift
ARGS=${*}

SCRIPT_TCL=${SCRIPT_SWIFT%.swift}.tcl

stc ${SCRIPT_SWIFT} ${SCRIPT_TCL} || exit 1

export TURBINE_LOG=0
turbine ${SCRIPT_TCL} ${ARGS}



