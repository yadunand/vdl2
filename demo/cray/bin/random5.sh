#! /bin/bash

runtime=${1:-0}
range=$2
biasfile=$3
n=$4

sleep $runtime

read scale offset <$biasfile

for ((i=0;i<n;i++)); do
  echo $(( ($RANDOM%range)*scale+offset))
done
