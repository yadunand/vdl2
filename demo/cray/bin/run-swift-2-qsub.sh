#!/bin/bash

PATH+=:/home/users/p01577/Public/turbine/bin
PATH+=:/home/users/p01577/Public/turbine/scripts/submit/cray
PATH+=:/home/users/p01577/Public/stc/bin

echo Turbine tools:
which turbine || exit 1
which turbine-aprun-run.zsh || exit 1
echo STC:
which stc || exit 1
echo

SCRIPT_SWIFT=$1
shift
ARGS=${*}

if [[ ${SCRIPT_SWIFT} == "" ]]
then
  echo "No script!"
  exit 1
fi

SCRIPT_TCL=${SCRIPT_SWIFT%.swift}.tcl

stc ${SCRIPT_SWIFT} ${SCRIPT_TCL} || exit 1

export TURBINE_LOG=0
export TURBINE_OUTPUT=${PWD}
export NODES=3
export PPN=3
turbine-aprun-run.zsh ${SCRIPT_TCL} ${ARGS}
