type file;

app (file o) mysim5 (int timesteps, int range, file bias, int count)
{
  random5 timesteps range @filename(bias) count stdout=@filename(o);
}

app (file o) analyze (file s[])
{
  average @filenames(s) stdout=@filename(o);
}

file sims[];
int  nsim  = @toInt(@arg("nsim",  "10"));

int  steps = @toInt(@arg("steps", "1"));
int  range = @toInt(@arg("range", "100"));
int  count = @toInt(@arg("count", "10"));

file bias<"data/bias.dat">;

foreach i in [0:nsim-1] {
  file simout <single_file_mapper; file=@strcat("data/sim_",i,".out")>;
  simout = mysim5(steps,range,bias,count);
  sims[i] = simout;
}

file stats<"output/average.out">;
stats = analyze(sims);
