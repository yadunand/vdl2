
TOPIC: Run the "simulations" on the parallel cluster


Til now we've been running our "simulations" on the local login host.

Now we'll run them on the parallel cluster.

The script will be the same as previous, but we change the tool catalog to
indicate that our "simulation app" (random4.sh) should be run on the
"westmere" partition of the Midway cluster using the "slurm" scheduler:

We changed file "tc" from:

localhost random4  random4.sh
localhost average  avg.sh

to:

westmere  random4  random4.sh
localhost average  avg.sh

And we added the "sites.xml" file which specifies two "pools" of resources,
the "local host" pool (which we've been using till now) and the "westmere" pool.

To run, we now add the sites.xml file to the swift command line:

$ swift -tc.file tc -sites.file sites.xml p7.swift -steps=10 -range=1000 -count=6 -nsim=100

Note that we're now asking for 100 simulations to be run, each of which
is to run for 10 steps (ie 10 seconds).

The sites file looks like this:

<config>

  <pool handle="localhost">
    <execution provider="local"/>
    <filesystem provider="local"/>
    <workdirectory>/scratch/midway/{env.USER}/swiftwork</workdirectory>
  </pool>

  <pool handle="westmere">
    <execution provider="coaster" jobmanager="local:slurm"/>

    <!-- Set partition and account here: -->
    <profile namespace="globus" key="queue">westmere</profile>
    <profile namespace="globus" key="ppn">12</profile>
    <!-- <profile namespace="globus" key="project">pi-wilde</profile> -->

    <!-- Set number of jobs and nodes per job here: -->
    <profile namespace="globus" key="slots">1</profile>
    <profile namespace="globus" key="maxnodes">1</profile>
    <profile namespace="globus" key="nodegranularity">1</profile>
    <profile namespace="globus" key="jobsPerNode">12</profile> <!-- apps per node! -->
    <profile namespace="karajan" key="jobThrottle">.11</profile> <!-- eg .11 -> 12 -->

    <!-- Set estimated app time (maxwalltime) and requested job time (maxtime) here: -->
    <profile namespace="globus" key="maxWalltime">00:15:00</profile>
    <profile namespace="globus" key="maxtime">1800</profile>  <!-- in seconds! -->

    <!-- Set data staging model and work dir here: -->
    <filesystem provider="local"/>
    <workdirectory>/scratch/midway/{env.USER}/swiftwork</workdirectory>

    <!-- Typically leave these constant: -->
    <profile namespace="globus" key="slurm.exclusive">false</profile>
    <profile namespace="globus" key="highOverAllocation">100</profile>
    <profile namespace="globus" key="lowOverAllocation">100</profile>
    <profile namespace="karajan" key="initialScore">10000</profile>
  </pool>

</config>

Try:

- running more app() calls at once:  change jobsPerNode and jobThrottle

- requesting more than 1 slurm job at once: change slots

- requesting nodes in units greater than 1: change maxnodes and nodeGranularity
