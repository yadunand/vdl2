
module load java # to get Oracle/Sun JDK
# module load swift

export PATHPREFIX="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../bin" && pwd )"
echo
echo   Setting PATHPREFIX=$PATHPREFIX

PATH=/home/users/p01532/swift/rev/0.94/bin:$PATHPREFIX:$PATH
echo
echo   Setting PATH=$PATH
echo
mkdir -p $HOME/.swift

if [ -f $HOME/.swift/swift.properties ]; then
  echo Adding properties to end of $HOME/.swift/swift.properties
else
  echo creating $HOME/.swift/swift.properties
fi
echo

mkdir -p $HOME/.swift
cat >>$HOME/.swift/swift.properties <<END

# Properties for Swift Tutorial 

sites.file=./sites.xml
tc.file=./apps

wrapperlog.always.transfer=true
sitedir.keep=true
file.gc.enabled=false
status.mode=provider

execution.retries=0
lazy.errors=false

use.wrapper.staging=false
provider.staging.pin.swiftfiles=true

# Edit this line to change data management mode:
use.provider.staging=true  # SET PROVIDER STAGING

END
