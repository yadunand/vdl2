type file;

app (file o) mysim5 (int timesteps, int range, file bias, int count)
{
  random5 timesteps range @filename(bias) count stdout=@filename(o);
}

app (file o) analyze (file s[])
{
  average @filenames(s) stdout=@filename(o);
}

file sims[];                               # Array of files to hold each simulation output

int  nsim  = @toInt(@arg("nsim",  "10"));  # number of simulation programs to run

int  steps = @toInt(@arg("steps", "1"));   # number of "steps" each simulation
                                           # (==seconds of runtime)
int  range = @toInt(@arg("range", "100")); # range of the generated random numbers

int  count = @toInt(@arg("count", "10"));  # number of random numbers generated per simulation

file bias<"data/bias.dat">;                # Input data file to "bias" the numbers:
                                           # 1 line: scale offset ( N = n*scale + offset)

foreach i in [0:nsim-1] {
  file simout <single_file_mapper; file=@strcat("data/output/sim_",i,".out")>;
  simout = mysim5(steps,range,bias,count);
  sims[i] = simout;
}

file stats<"output/average.out">;         # Final output file: average of all "simulations"
stats = analyze(sims);
