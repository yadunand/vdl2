
tracef("Hello, World!\n");

#  this is a one-line comment

// as it this

/* This is a
   multi-line c

*/

/*Remove the comments marked <--, one at a time,
  to try the following examples
  using "swift p0.swift"
*/


//   (1) Declare, assign, and trace variables:

/* <-- (1)
int n;
string str;

n=99;
str = "\n\n hi there! \n\n";

tracef("\n n=%i str=%s\n\n", n, str);


//   (2) Declare and trace a  vector

/* <-- (2)
int[] ivec = [12,43,56,87,123];
tracef("ivec: %q\n",ivec);
trace("ivec",ivec);


//   (3) foreach: do each loop body in parallel

/* <-- (3)
foreach i, value in ivec {
  tracef("i=%i value=%i\n", i, value);
}


//   (4) Use an array constructor to control a foreach loop

/* <-- (4)
foreach j, value in [0:9] {
  tracef("j=%i value=%i\n", j, value);
}


//   (5) Data flow evaluation: the statements below execute in reverse order

/* <-- (5)
int a, b, c;

c = 1234;
a = b;
b = c;

trace("dataflow", a, b, c);


//   (6) The iterate statement executes sequentially

/* <-- (6)
iterate iter {
  trace("iter",iter);
} until(iter==10);

*/

// TO ADD: functions, if, select...  Associative arrays... More?
