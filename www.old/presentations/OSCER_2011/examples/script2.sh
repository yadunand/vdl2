#!/bin/bash

echo "COMMAND: $0 $*"

set -x

date "+%m/%d/%Y %I:%M%p"

{
  INPUT1=$1
  INPUT2=$2
  OUTPUT=$3

  cat $INPUT1 $INPUT2 || exit 1

  touch $OUTPUT || exit 1
} 2>&1

exit 0
