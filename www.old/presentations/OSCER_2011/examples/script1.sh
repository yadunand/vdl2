#!/bin/bash

set -x

date "+%m/%d/%Y %I:%M%p"

{
  INPUT1=$1
  INPUT2=$2
  OUTPUT1=$3
  OUTPUT2=$4

  cat $INPUT1 $INPUT2 || exit 1

  touch $OUTPUT1 $OUTPUT2 || exit 1
} 2>&1

exit 0
