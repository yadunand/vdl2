
type file;

app (file o) copy(file i)
{
  cp @i @o;
}

(string name) makeFileName()
{
  name = @strcat("input", ".txt");
  tracef("FILENAME: %s\n", name);
}

string s = makeFileName();

file input<single_file_mapper;file=s>;
file output<"output.txt"> = copy(input);

