
type file;

app (file o, file log) script(file i[])
{
  script2 @filenames(i) @o stdout=@log;
}

(string result[]) makeFilenames()
{
  foreach i in [0:1]
  {
    string s = @strcat("input", i, ".txt");
    result[i] = s;
  }
}

string a[] = makeFilenames();
file input[]<array_mapper;files=a>;
foreach f, i in a
{
  tracef("FILENAME: %s\n", f);
}

file output<"output.txt">;
file log<"log.txt">;
(output, log) = script(input);
