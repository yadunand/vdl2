
type file;

app (file o) copy (file i)
{
  cp @i @o;
}

file input<"input.txt">;
file output<"output.txt"> = copy(input);

// Lines may be reversed
