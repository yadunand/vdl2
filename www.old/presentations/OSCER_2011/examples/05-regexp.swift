
type file;

app (file o) convert (file i)
{
  convert @i @o;
}

app (file o) rotate(int d, file i)
{
  convert "-rotate" d @i @o;
}

file franklin_jpg<"franklin.jpg">;
file franklin_png<regexp_mapper;
                  source=@filename(franklin_jpg),
                  match="(.*).jpg",
                  transform="\\1-0.png">;

tracef("JPG: %M\n", franklin_jpg);
tracef("PNG: %M\n", franklin_png);

franklin_png = convert(franklin_jpg);

foreach i in [1:5]
{
  int d = i*60;
  string s = @strcat("franklin-", d, ".png");
  tracef("ROTATE: %s\n", s);
  file f<single_file_mapper;file=s> = rotate(d, franklin_png);
}

