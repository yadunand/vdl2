
type file;

app (file o) annotate(int d, file i)
{
  annotate @i d @o;
}

app (file o) makelog(int d, file i)
{
  echo "degrees:" d stdout=@o;
}

file franklin_pngs[]<filesys_mapper;
                     prefix="franklin",
                     suffix=".png">;

file franklin_notes[]<structured_regexp_mapper;
                      source=franklin_pngs,
                      match="(.*).png",
                      transform="\\1-note.png">;

file franklin_logs[]<structured_regexp_mapper;
                     source=franklin_notes,
                     match="(.*)-note.png",
                     transform="\\1.txt">;

foreach f,i in franklin_pngs
{
  tracef("ANNOTATE: %M -> %M\n", franklin_pngs[i], franklin_notes[i]);
  string s = @filename(f);
  string n = @strcut(s, "franklin-(.*).png");
  int d = @toint(n);
  franklin_notes[i] = annotate(d, franklin_pngs[i]);
}

foreach p,j in franklin_notes
{
  tracef("LOG: %M -> %M\n", franklin_notes[j], franklin_logs[j]);
  string s = @filename(p);
  string n = @strcut(s, "franklin-(.*)-note.png");
  int d = @toint(n);
  franklin_logs[j] = makelog(d, franklin_notes[j]);
}
