
type file;

app (file o1, file o2, file log) script(file i1, file i2)
{
  script1 @i1 @i2 @o1 @o2 stdout=@log;
}

file input1<"input1.txt">;
file input2<"input2.txt">;
file output1<"output1.txt">;
file output2<"output2.txt">;
file log<"log.txt">;
(output1, output2, log) = script(input1, input2);
