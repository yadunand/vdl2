#!/bin/bash

set -x

date "+%m/%d/%Y %I:%M%p"

{
  INPUT=$1
  DEGREES=$2
  OUTPUT=$3

  sleep $(( $DEGREES / 60 ))

  convert $INPUT       \
    -stroke "#000C"    \
    -pointsize 24      \
    -gravity northwest \
    -fill blue         \
    -annotate +20+20 $DEGREES $OUTPUT || exit 1
} 2>&1

exit 0
