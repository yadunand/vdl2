<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<title>Swift</title>
		<link href="../css/style2.css" rel="stylesheet" type="text/css" />
	</head>

	<body id="home">
		<!-- entire page container -->
		<!-- comment -->
		<div id="container">
			<!-- header -->
			<div id="header">
				<?php require('../inc/header.php') ?>
			</div>
			<!-- end header -->
			<!-- nav -->
			<div id="nav">
				<?php require('../inc/nav.php') ?>
			</div>
			<!-- end nav -->
			<!-- content container -->
			<div id="content">
				<!-- left side content -->
				<div id="left">
				
<h1>HOME</h1>

<p>
	<strong>Swift</strong> is a system for the <strong>rapid</strong> and
	<strong>reliable</strong> specification, execution, and management of
	<strong>large-scale</strong> science and engineering
	<strong>workflows</strong>. It supports applications that execute many tasks
	coupled by disk-resident <strong>datasets</strong> - as is common, for
	example, when analyzing large quantities of data or performing parameter
	studies or ensemble simulations.
</p>

<ul>

	The open source Swift software combines:
	
	<li>A simple scripting language to enable the concise, high-level
	specifications of complex parallel computations, and mappers for accessing
	diverse data formats in a convenient manner.</li>

	<li>An execution engine that can manage the dispatch of many (10,000)
	tasks to many (100) processors, whether on parallel computers, campus
	grids, or multi-site grids.</li>

</ul>

<p>
	Swift users span the physical sciences, biological sciences, social
	sciences, humanities, computer science, and education. Swift users have
	achieved multiple-order-of-magnitude savings in program development and
	execution time.
</p>

<p>
	Swift builds on and includes technology previously distributed as the
	GriPhyN Virtual Data System.
</p>

<div class="image">
	<img src="../images/workflow.jpg" alt="workflow"/>
</div>
<div class="caption">"Swift applied to computational neuroscience. On the left, a small workflow from functional MRI study in 	aphasia; on the right, a map of brain activation clusters caused by various stimuli." 
</div>

<p>
The Swift project is supported by the National Science Foundation with          
additional support from NIH, Argonne National Laboratory and the University     
of Chicago Computation Institute.
</p>

				</div>
				<!-- end left side content -->
				<!-- right side content -->
				<div id="right">
					<?php require('../inc/home_sidebar.php') ?>
				</div>
				<!-- end right side content -->
			</div>
			<!-- end content container-->
			<!-- footer -->
			<div id="footer"><?php require('../inc/footer.php') ?></div> 
			<!-- end footer -->
		</div>
		<!-- end entire page container -->

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>

	</body>
</html>
