function initjs() {
	selectCurrent();
	//decorateLinks();
}

function decorateLinks() {
	for (i in document.links) {
		link = document.links[i];
		if (link.className == "menulink") {
			ld = link.parentNode.innerHTML;
			index = ld.indexOf(">");
			link.parentNode.innerHTML = ld.substr(0, index) + 
				" onMouseOver=\"highlight(" + i + ");\"" +
				" onMouseOut=\"unhighlight(" + i + ");\"" +
				ld.substr(index);
		}
	}
}

function highlight(i) {
	link = document.links[i];
	link.style.color = "#6060ff";
}

function unhighlight(i) {
	link = document.links[i];
	link.style.color = "#000000";
}

function selectCurrent() {
	docname = document.URL;
	for (i in document.links) {
		link = document.links[i];
		if (link.className == "menulink") {
			if (docname.indexOf(link.href) != -1) {
				link.className = "selected";
			}
		}
	}
}
