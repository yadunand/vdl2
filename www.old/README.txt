
PERMISSIONS: 

Remember to set umask to 077 to make sure that the vdl2-svn 
group has write access to all of this stuff, including .svn 
files. 

GUIDES: 

Formerly,

/guides was SVN trunk/docs

Now,

/guides/trunk is SVN trunk/docs
/guides/release-0.91 is SVN branches/1.0/docs
/guides/release-0.92 is SVN branches/release-0.92/docs
/guides/current is SVN branches/1.0/docs

/docs/index.php will be updated to have multiple versions for QS
Guide, Tutorial, User Guide and Log Processing Tools.  The current
versions will be labeled "current" and "trunk".  Other versions could
easily be exposed in the future.

