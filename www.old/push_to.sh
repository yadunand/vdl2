#!/bin/bash

# USAGE:
# 1) check out a working copy of vdl2/www,
# 2) make edits,
# 3) run this script in your working copy directory,
#    a) use DESTINATION=~/public_html to preview,
#    b) use DESTINATION=/ci/www/projects/swift for the live site
#       on a CI machine

# File permissions
CHMOD_DIRECTORY_MODE="775"
CHMOD_FILE_MODE="664"
GROUP="vdl2-svn"

# Verify command line arguments
if [ -n "$1" ]; then
   DESTINATION=$1
else
   echo Error: Specify directory to copy to
   exit 1
fi

# Make sure we are calling this script in the same directory as the script is located
pushd $(dirname $(readlink -f $0)) > /dev/null 2>&1

# Long list of files to explicitly include follows
FILES="
inc/footer.php
inc/home_sidebar.php
inc/papers_sidebar.php
inc/docs_sidebar.php
inc/about_sidebar.php
inc/downloads_sidebar.php
inc/support_sidebar.php
inc/header.php
inc/links_sidebar.php
inc/nav.php
inc/apps_sidebar.php
inc/side_content.php
shBrushVDL2.js
bric-flow-thm.jpg
about/index.php
links/index.php
template_info/template.php
docs/index.php
docs/license.php
docs/tracking_policy.php
docs/usage_data.php
presentations/nothing
presentations/SULI_2011/suli.pptx
presentations/SULI_2011/suli.pdf
presentations/SULI_2011/suli-examples.tgz
updatenodocs.sh
dhtml.js
index.html
style.css
papers/jogc_03.pdf
papers/HealthGrid-2007-VDL2Bric.submitted-revised.pdf
papers/challenge-editorial.pdf
papers/OpportunisticAlgoritmForSchedulingWokflows.pdf
papers/Chimera2002.pdf
papers/clag_paper.pdf
papers/Swift-SWF07.pdf
papers/GridResourcesForGADU.pdf
papers/sigmod-swf-vdl.pdf
papers/BioGrid2005.pdf
papers/ModelAndArchForDataCollab2003.pdf
papers/SwiftParallelScripting.pdf
papers/p57-zhao.pdf
papers/Kickstarting2006.pdf
papers/VirtualDataInCMS.pdf
papers/VirtualDataProvenance.pdf
papers/cpe2000.pdf
papers/SwiftLanguageForDistributedParallelScripting.pdf
papers/index.php
papers/SwiftForSocialSciences-2007.pdf
papers/AutomatingClimateScience.pdf
papers/XDTM_egc05.pdf
papers/p37-special-sw-section-6.pdf
papers/SDSS-SC02.pdf
papers/egc05.pdf
papers/Falkon_SC07_v24.pdf
papers/vonLaszewski-workflow-book.pdf
papers/VDS-CMS.pdf
papers/SwiftMontage.pdf
css/style.css
css/style1col.css
css/style2.css
images/workflow.jpg
images/graphics/h_backgrnd.gif
images/graphics/swift_sketch.gif
images/graphics/banner2.gif
images/graphics/h_ckgrnd.gif
images/graphics/h_backgrnd2.gif
images/graphics/banner.gif
images/workflow.gif
shCoreu.js
apps/style2.css
apps/index.php
README.txt
bric-flow.jpg
sitemap.xml
downloads/release-notes-0.6.txt
downloads/release-notes-0.9.txt
downloads/release-notes-0.5.txt
downloads/release-notes-0.7.txt
downloads/index.php
downloads/release-notes-0.8.txt
main/index.php
support/index.php
update.sh
"

# Make sure destination directory exists
if [ ! -d "$DESTINATION" ]
then
   mkdir -v $DESTINATION || exit 1
   chgrp $GROUP $DESTINATION > /dev/null
   chmod $CHMOD_DIRECTORY_MODE $DESTINATION
fi

# Copy files to destination
for FILE in $FILES
do
   destination_dir=`dirname $FILE`
   mkdir -pv $DESTINATION/$destination_dir
   chgrp $GROUP $DESTINATION/$destination_dir
   chmod $CHMOD_DIRECTORY_MODE $DESTINATION/$destination_dir
   cp -u $FILE $DESTINATION/$destination_dir
   chgrp $GROUP $DESTINATION/$FILE
   chmod $CHMOD_FILE_MODE $DESTINATION/$FILE
done

popd > /dev/null 2>&1
