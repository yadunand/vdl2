<h3>HIGHLIGHTS</h3>
<h4>Technology</h4>
<p><em>Parallel Scripting for Applications at the Petascale and Beyond (2009)</em> [<a href="../papers/SwiftParallelScripting.pdf" target="_blank">pdf</a>]</p><br />
<p><em>A Notation and System for Expressing and Executing Cleanly Typed Workflows on Messy Scientific Data (2005)</em> [<a href="http://people.cs.uchicago.edu/~yongzh/pub/sigmod-swf-vdl.pdf" target="_blank">pdf</a>]</p><br />
<p><em>XDTM: XML Data Type and Mapping for Specifying Datasets (2005)</em> [<a href="XDTM_egc05.pdf" target="_blank">pdf</a>] </p><br />
<p><em>The Virtual Data Grid: A New Model and Architecture for Data-Intensive Collaboration (2003)</em> [<a href="http://www-db.cs.wisc.edu/cidr/cidr2003/program/p18.pdf">pdf</a>] </p><br />
<p><em>Falkon: a Fast and Light-weight tasK executiON framework (2007)</em> [<a href="Falkon_SC07_v24.pdf">pdf</a>] </p><br />
<p><em>Swift: Fast, Reliable, Loosely Coupled Parallel Computation (2007)</em> [<a href="Swift-SWF07.pdf">pdf</a>] </p><br />

<h4>Applications</h4>
<p><em>Accelerating Medical Research using the Swift Workflow System (2007)</em> [<a href="HealthGrid-2007-VDL2Bric.submitted-revised.pdf" target="_blank">pdf</a>]</p><br />
<p><em>
The QuarkNet/Grid Collaborative Learning e-Lab (2006)</em> [<a href="http://quarknet.fnal.gov/talks/clag_paper.pdf" target="_blank">pdf</a>]</p><br />
<p><em>Using Multiple Grid Resources for Bioinformatics Applications in GADU (2006)</em> [<a href="http://csdl2.computer.org/persagen/DLAbsToc.jsp?resourcePath=/dl/proceedings/&amp;toc=comp/proceedings/ccgrid/2006/2585/02/2585toc.xml&amp;DOI=10.1109/CCGRID.2006.182  -- Applications in bioinformatics" target="_blank">pdf</a>] </p>


