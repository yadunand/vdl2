<h3>OTHER DOCUMENTATION</h3>

<h4><a href="../guides/quickstartguide.php">Swift Quickstart Guide</a></h4>
<h4><a href="../guides/tutorial.php">Swift Tutorial</a></h4>
<h4><a href="../guides/userguide.php">Swift User Guide</a></h4>
<h4><a href="../guides/languagespec.php">SwiftScript Language Reference</a></h4>


<!--<h3>WHATEVER</h3>
<p>All the highlights and releases can go here</p>
<h4>Something Else</h4>
<div class='featurebox_side'>
<p>Perhaps sometrhing featured.</p>
</div>-->
