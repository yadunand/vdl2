<h3>RELATED LINKS</h3>

<h4>Globus Toolkit</h4>

<p>
	Swift uses the <a href="http://www.globus.org/">Globus Toolkit</a> as
	middleware to talk to various resources.
</p>

<h4>Java CoG Kit</h4>

<p>
	The <a href="http://wiki.cogkit.org/index.php/Main_Page">CoG
	Kit</a> provides a high level uniform interface to different versions of the
	Globus Toolkit. It also contains the Karajan language and interpreter which
	is used as an underlying execution engine for Swift.
</p>

<h4>TeraGrid</h4>

<p>
	"<a href="http://www.teragrid.org/">TeraGrid</a> is an open scientific
	discovery infrastructure combining leadership class resources at nine
	partner sites to create an integrated, persistent computational resource."
</p>
