
<h3>What&apos;s New? </h3>
<h4>Paper published</h4>
<p><em>Swift: A Language for Distributed Parallel Scripting</em> [<a href="../papers/SwiftLanguageForDistributedParallelScripting.pdf" target="_blank">pdf</a>]</p><br />
<h4>Paper published</h4>
<p><em>Parallel Scripting for Applications at the Petascale and Beyond</em> [<a href="../papers/SwiftParallelScripting.pdf" target="_blank">pdf</a>]</p><br />
<h4>SWIFT 0.92.1 RELEASE - 2011/04/13</h4>
<p>
The latest release of Swift, v0.92.1, is available from the
<a href="../downloads/">downloads page</a>.
</p>
<br>
<div class='featurebox_side'>
<p><em>You need to run an analysis pipeline on a thousand functional magnetic resonance imaging (fMRI) files. The files are in some funny directory structure, the TeraGrid is miles away, and neither writing an MPI program nor performing the many analysis tasks manually is attractive. Swift addresses such challenges. A few lines of SwiftScript describe how your analysis procedures should be applied to the many fMRI images. Swift tools dispatch the thousands of tasks to TeraGrid nodes. Your computation is performed quickly and reliably, and the results are documented with data about how they were computed. Life is good.</em></p>
</div>
