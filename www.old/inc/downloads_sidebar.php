<h3>GETTING STARTED </h3>
<p>
<strong>Swift Quickstart Guide</strong> [<a href="../guides/quickstartguide.php">html</a>]</p>
<p>Use the Quickstart Guide to help you install and configure swift and run a simple 'Hello World' example.</p>

<h4>BUG REPORTS  </h4>

<p>We have a Bugzilla, at <a href="http://bugzilla.mcs.anl.gov/swift/">bugzilla.mcs.anl.gov/swift/</a>. </p>

