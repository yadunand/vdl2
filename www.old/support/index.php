<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Swift: Support</title>
		<link href="../css/style2.css" rel="stylesheet" type="text/css" />
	</head>

	<body id="support">
		<!-- entire page container -->
		<div id="container">
			<!-- header -->
			<div id="header">
				<?php require('../inc/header.php') ?>
			</div>
			<!-- end header -->
			<!-- nav -->
			<div id="nav">
				<?php require('../inc/nav.php') ?>
			</div>
			<!-- end nav -->
			<!-- content container -->
			<div id="content">
				<!-- left side content -->
				<div id="left">
				
<h1>SUPPORT</h1>

<h2>Mailing Lists</h2>
	<table class="mailinglists">
		<tr>
			<th width="10%" valign="top">List</th>
			<th width="20%" valign="top" nowrap="nowrap">List Info<br/>(subscribe here)</th>
			<th width="8%" valign="top">Archives</th>
			<th width="30%" valign="top">Purpose</th>

		</tr>
		<tr>
			<td valign="top">
				<a href="mailto:swift-user@ci.uchicago.edu">swift-user</a>
			</td>
			<td align="center" valign="top">
				<a href="https://lists.ci.uchicago.edu/cgi-bin/mailman/listinfo/swift-user">list page</a>
			</td>
			<td align="center" valign="top">
				<a href="http://lists.ci.uchicago.edu/pipermail/swift-user/">archives</a>
			</td>
			<td valign="top">
				Swift user support and general questions. 
			</td>
		</tr>
		<tr>
			<td valign="top">
				<a href="mailto:swift-devel@ci.uchicago.edu">swift-developer</a>			
			</td>
			<td align="center" valign="top">
				<a href="https://lists.ci.uchicago.edu/cgi-bin/mailman/listinfo/swift-devel">list page</a>
			</td>
			<td align="center" valign="top">
				<a href="http://lists.ci.uchicago.edu/pipermail/swift-devel/">archives</a>
			</td>

			<td valign="top">
				Swift developers mailing list.
			</td>
		</tr>
	</table>

</div>

				<!-- end left side content -->
				<!-- right side content -->
				<div id="right">
					<?php require('../inc/support_sidebar.php') ?>
				</div>
				<!-- end right side content -->
			</div>
			<!-- end content container-->
			<!-- footer -->
			<div id="footer"><?php require('../inc/footer.php') ?></div> 
			<!-- end footer -->

		</div>
		<!-- end entire page container -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
	</body>
</html>
