dp.sh.Brushes.VDL2 = function()
{
	var keywords =	'int float integer string date boolean uri any true false null namespace include type if else switch case default while foreach in step app repeat until';

	this.regexList = [
		{ regex: dp.sh.RegexLib.SingleLineCComments,							css: 'comment' },		// one line comments
		{ regex: dp.sh.RegexLib.MultiLineCComments,								css: 'comment' },		// multiline comments
		{ regex: dp.sh.RegexLib.DoubleQuotedString,								css: 'string' },		// strings
		{ regex: dp.sh.RegexLib.SingleQuotedString,								css: 'string' },		// strings
		{ regex: new RegExp('\\b([\\d]+(\\.[\\d]+)?|0x[a-f0-9]+)\\b', 'gi'),	css: 'number' },		// numbers
		{ regex: new RegExp('\\@[a-z0-9]+\\b', 'gi'),									css: 'function' },		
		{ regex: new RegExp(this.GetKeywords(keywords), 'gm'),					css: 'keyword' }		// java keyword
		];

	this.CssClass = 'dp-vdl2';
}

dp.sh.Brushes.VDL2.prototype	= new dp.sh.Highlighter();
dp.sh.Brushes.VDL2.Aliases	= ['vdl2'];
