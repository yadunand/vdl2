<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<title>Swift: Docs &amp; Papers</title>
		<link href="../css/style2.css" rel="stylesheet" type="text/css" />
	</head>

	<body id="docs">
		<!-- entire page container -->
		<div id="container">
			<!-- header -->
			<div id="header">
				<?php require('../inc/header.php') ?>
			</div>
			<!-- end header -->
			<!-- nav -->
			<div id="nav">
				<?php require('../inc/nav.php') ?>
			</div>
			<!-- end nav -->
			<!-- content container -->
			<div id="content">
				<!-- left side content -->
				<div id="left">

<h1>DOCUMENTATION </h1>
<h3>Swift Quickstart Guide</h3>

<ul>
	<li>Swift Quickstart Guide [<a href="../guides/quickstartguide.php">html</a>]</li>
</ul>

<p>
	Use the Quickstart Guide to help you install and configure swift and run a
	simple 'Hello World' example.
</p>

<h3>Swift Tutorials</h3>

<ul>
	<li><strong>latest (0.92)</strong>
        [<a href="../guides/tutorial.php">html</a>] [<a href="../guides/tutorial.pdf">pdf</a>]
        </li>
	<li><strong>trunk</strong>
        [<a href="../guides/trunk/tutorial/tutorial.html">html</a>] [<a href="../guides/trunk/tutorial/tutorial.pdf">pdf</a>]
        </li>
</ul>


<ul>
	<li><strong>Swift Tutorial (on your own machine)</strong> [<a href="../guides/tutorial.php">html</a>] [<a href="../guides/tutorial.pdf" onClick="javascript: pageTracker._trackPageview('/swift/guides/tutorial.php');">pdf</a>]</li>
</ul>

<p>
	This Swift tutorial carries on where the Quickstart Guide leaves off,
	introducing you to Swift environment and the SwiftScript language in more
	depth.
</p>

<ul>
	<li><strong>Swift Tutorial (on a UC machine)</strong> [<a href="http://www.ci.uchicago.edu/osgedu/schools/swiftlab/">html</a>] </li>
</ul>

<p>
	This tutorial (part of the Open Science Grid teaching lab) introduces
	you to the Swift environment and the SwiftScript language in more
	depth using a provided training account where installation of the
	software has already been performed.
</p>


<h3>Swift User Guide</h3>
<ul>
<p>
	The User Guide provides more detailed reference documentation and background
	information on swift. It is assumed that the reader is already familiar with
	the material in the Quickstart and Tutorial documents.
</p>
	<li><strong>trunk</strong>
          [<a href="../guides/trunk/userguide/userguide.html">html</a>] [<a href="../guides/trunk/userguide/userguide.pdf">pdf</a>]
        </li>
	<li><strong>latest (0.92)</strong>
          [<a href="../guides/release-0.92/userguide/">multi-page html</a>]
        </li>
	<li><strong>historical (0.91)</strong>
          [<a href="../guides/release-0.91/userguide/">multi-page html</a>]
        </li>
</ul>

<h3 id="SULI">Argonne Summer Student Presentation</h3>
<p>
	This is a presentation that features some Swift examples. <br>
        Given for Argonne summer students, July 15, 2011. <br>
        The examples require a Swift installation and ImageMagick.
</p>
<ul>
<li>Slides: [<a href="../presentations/SULI_2011/suli.pptx">pptx</a>]
            [<a href="../presentations/SULI_2011/suli.pdf">pdf</a>]</li>
<li>Examples: [<a href="../presentations/SULI_2011/suli-examples.tgz">tgz</a>]</li>
</ul>

<h3>Log Processing tools</h3>
<ul>
	<li>Swift log processing tools [<a href="../guides/log-processing.php">html</a>] [<a href="../guides/log-processing.pdf" onClick="javascript: pageTracker._trackPageview('/swift/guides/log-processing.pdf');">pdf</a>]</li>
</ul>

<p>
	The Swift log processing tools can be used to analyse the log files of
	Swift runs - as a webpage, and in several processed text formats. This
	document gives some usage information for those tools.
</p>

<h3>OSG tutorial</h3>

<p>
This is an OSG-oriented tutorial that touches on Swift.
[<a href="http://www.ci.uchicago.edu/osgedu/modules/#workflow_swift">html</a>]
</p>

<h3>Historical - SwiftScript Language</h3>

<p>

	The SwiftScript language reference was a specification for an
	earlier version of the language, and is probably of little interest to the majority of users.

	<ul>
    	<li> SwiftScript Language Reference - latest work in progress [<a href="../guides/historical/languagespec.php">html</a>] [<a href="../guides/languagespec.pdf" onClick="javascript: pageTracker._trackPageview('/swift/guides/historical/languagespec.pdf');">pdf</a>]</li>
	    <li> SwiftScript Language Reference v 0.6 [<a href="../guides/languagespec-0.6.php">html</a>] [<a href="../guides/historical/languagespec.pdf" onClick="javascript: pageTracker._trackPageview('/swift/guides/languagespec-0.6.pdf');">pdf</a>]</li>
	</ul>
</p>

			</div>

				<!-- end left side content -->
				<!-- right side content -->
				<div id="right">
					<?php require('../inc/docs_sidebar.php') ?>
				</div>
				<!-- end right side content -->
			</div>
			<!-- end content container-->
			<!-- footer -->
			<div id="footer"><?php require('../inc/footer.php') ?></div>
			<!-- end footer -->

		</div>
		<!-- end entire page container -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
	</body>
</html>
