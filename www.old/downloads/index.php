<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Swift: Downloads</title>
		<link href="../css/style2.css" rel="stylesheet" type="text/css" />
	</head>

	<body id="downloads">
		<!-- entire page container -->
		<div id="container">
			<!-- header -->
			<div id="header">
				<?php require('../inc/header.php') ?>
			</div>
			<!-- end header -->
			<!-- nav -->
			<div id="nav">
				<?php require('../inc/nav.php') ?>
			</div>
			<!-- end nav -->
			<!-- content container -->
			<div id="content">
				<!-- left side content -->
				<div id="left">
				
<h1>DOWNLOADS</h1>
<h2><a name="stable"></a>Latest Release</h2>

<h3>Swift 0.92.1 - 2011/04/13</h3>
<p>Precompiled binary distribution
[<a href="../packages/swift-0.92.1.tar.gz"
onClick="javascript: pageTracker._trackPageview('/swift/packages/swift-0.92.1.tar.gz');"
>swift-0.92.1.tar.gz</a>]

<h2>Source Code</h2>

<p>
	The code is
<a href="http://www.ci.uchicago.edu/trac/swift/browser/trunk">browsable online</a>, or accessible through
<a href="http://subversion.tigris.org">Subversion</a>.
</p>

<p>
	You need 
	<a href="http://ant.apache.org">Apache Ant</a>
	for the actual build, and a <a href="http://java.sun.com">Java JDK</a> &ge; 1.5.
	<br/>
	So here are the steps:
</p>

<p>
	Checkout CoG:
	<div class="highlight">svn co \
https://cogkit.svn.sourceforge.net/svnroot/cogkit/tags/swift_0.92.1 cog</div>
	<br/>
</p>

<p>
	Checkout Swift:
	<div class="highlight">cd cog/modules</div>
	<div class="highlight">svn co https://svn.ci.uchicago.edu/svn/vdl2/tags/release-0.92.1 swift</div>
	<br/>
</p>

<p>
	Change directory to the swift module:
	<div class="highlight">cd swift</div>
	<br/>
</p>

<p>
	Build the thing:
	<div class="highlight">ant dist</div>
	<br/>
</p>

<p>
	The dist directory will contain the complete build.
</p>

<h3>Development Version</h3>

<p>
	<b>Note:</b> The development code has the highest chance of containing buggy and
	untested code. If you need stability please use the <b>latest release</b>.
</p>

<p>
	Checkout CoG:
	<div class="highlight">svn co \
https://cogkit.svn.sourceforge.net/svnroot/cogkit/trunk/current/src/cog</div>
	<br/>
</p>

<p>
	Checkout Swift:
	<div class="highlight">cd cog/modules</div>
	<div class="highlight">svn co https://svn.ci.uchicago.edu/svn/vdl2/trunk swift</div>
	<br/>
</p>

<p>
	Change directory to the swift module:
	<div class="highlight">cd swift</div>
	<br/>
</p>

<p>
	Build the thing:
	<div class="highlight">ant dist</div>
	<br/>
</p>

<p>
	The dist directory will contain the complete build.
</p>

<h2>Older releases</h2>


<h3>Swift 0.91 - 2010/12/23</h3>
<p>Precompiled binary distribution
[<a href="../packages/swift-0.91.tar.gz"
onClick="javascript: pageTracker._trackPageview('/swift/packages/swift-0.91.tar.gz');"
>swift-0.91.tar.gz</a>]

<h3>Swift 0.9 - 2009/04/27</h3>
<p>[<a href="../packages/swift-0.9.tar.gz"
onClick="javascript: pageTracker._trackPageview('/swift/packages/swift-0.9.tar.gz');"
>swift-0.9.tar.gz</a>]</p>


<h3>Swift 0.8 - 2009/01/30</h3>
<p>Swift v0.8 is a development release intended to release functionality
and fixes that have gone in to trunk since v0.7.
[<a href="../packages/swift-0.8.tar.gz"
onClick="javascript: pageTracker._trackPageview('/swift/packages/swift-0.8.tar.gz');"
>swift-0.8.tar.gz</a>]
[<a href="../packages/swift-0.8-stripped.tar.gz"
onClick="javascript: pageTracker._trackPageview('/swift/packages/swift-0.8-stripped.tar.gz');"
>swift-0.8-stripped.tar.gz</a> - for installation on systems with existing grid stacks]
[<a href="release-notes-0.8.txt"
onClick="javascript: pageTracker._trackPageview('/swift/packages/release-notes-0.8.txt');"
>release-notes-0.8.txt</a>]
</p>
<!--
<p>
As an alternative to the above traditional Swift packaging, Swift can be
downloaded and installed using
<a href="http://physics.bu.edu/~youssef/pacman/">pacman</a>, a package
manager commonly used on the Open Science Grid.
</p>
<p>
There are two installation targets:<br />
The first will install Swift alongside an existing VDT installation<br />
<pre>
pacman -get http://www.ci.uchicago.edu/~benc/pacman:swift-0.7
</pre></p>
<p>
The second will install Swift as well as a number
of supporting packages from the VDT software release to support use of the
DOE CA and use of VOMS. (These packages are also available in a regular OSG or
VDT installation): <br />
<pre>
pacman -get http://www.ci.uchicago.edu/~benc/pacman:swift-tools
</pre>
</p> -->



<h3>Swift 0.7 - 2008/11/11</h3>
<p>Swift v0.7 is a development release intended to release functionality
and fixes that have gone in to trunk since v0.7.
[<a href="../packages/vdsk-0.7.tar.gz">vdsk-0.7.tar.gz</a>]
[<a href="release-notes-0.7.txt">release-notes-0.7.txt</a>]
</p>
<p>
As an alternative to the above traditional Swift packaging, Swift can be
downloaded and installed using
<a href="http://physics.bu.edu/~youssef/pacman/">pacman</a>, a package
manager commonly used on the Open Science Grid.
</p>
<p>
There are two installation targets:<br />
The first will install Swift alongside an existing VDT installation<br />
<pre>
pacman -get http://www.ci.uchicago.edu/~benc/pacman:swift-0.7
</pre></p>
<p>
The second will install Swift as well as a number
of supporting packages from the VDT software release to support use of the
DOE CA and use of VOMS. (These packages are also available in a regular OSG or
VDT installation): <br />
<pre>
pacman -get http://www.ci.uchicago.edu/~benc/pacman:swift-tools
</pre>
</p>



<h3>Swift 0.6 - 2008/08/25</h3>
<p>Swift v0.6 is a development release intended to release functionality
and fixes that have gone in to trunk since v0.5.
[<a href="../packages/vdsk-0.6.tar.gz">vdsk-0.6.tar.gz</a>]
[<a href="release-notes-0.6.txt">release-notes-0.6.txt</a>]
</p>
<p>
As an alternative to the above traditional Swift packaging, Swift can be
downloaded and installed using
<a href="http://physics.bu.edu/~youssef/pacman/">pacman</a>, a package
manager commonly used on the Open Science Grid.
</p>
<p>
This will install Swift alongside an existing VDT installation:<br />
<pre>
pacman -get http://www.ci.uchicago.edu/~benc/pacman:swift-0.6
</pre> </p>

<h3>Swift 0.5 - 2008/04/16</h3>
<p>Swift v0.5 is a development release intended to release functionality
and fixes that have gone in to trunk since v0.4.
[<a href="../packages/vdsk-0.5.tar.gz">vdsk-0.5.tar.gz</a>]
[<a href="release-notes-0.5.txt">release-notes-0.5.txt</a>]
</p>
<h3>Swift 0.4 - 2008/03/18</h3>
<p>Swift v0.4 is a development release intended to release functionality
and fixes that have gone in to trunk since v0.3. More details are contained
in the <a href="../packages/release-notes-0.4.txt">release notes</a>.
[<a href="../packages/vdsk-0.4.tar.gz">vdsk-0.4.tar.gz</a>]
[<a href="../packages/release-notes-0.4.txt">release-notes-0.4.txt</a>]
</p>

<h3>Swift 0.3 - 2007/10/04</h3>
<p>Swift v0.3 is a development release intended to release functionality
and fixes that have gone in to trunk since v0.2.
[<a href="../packages/vdsk-0.3.tar.gz">vdsk-0.3.tar.gz</a>]
</p>

<h3>Swift 0.2 - 2007/07/19</h3>
<p>Swift v0.2 is a development release intended to release functionality
and fixes that have gone in to trunk since v0.1.
[<a href="../packages/vdsk-0.2.tar.gz">vdsk-0.2.tar.gz</a>]
</p>

<h3>Swift 0.1 - 2007/03/02</h3>
<p>Swift v0.1 is a development release intended to bring functionality
and fixes that have gone in to trunk since version 0 in a known release.
[<a href="../packages/vdsk-0.1.tar.gz">vdsk-0.1.tar.gz</a>]
</p>

<h3>Swift 0 RC3</h3>
<p>
	The 3rd release candidate of Swift version 0:  
	[<a	href="../packages/vdsk-0-rc3.tar.gz">tar.gz</a>] A bug that caused applications
	correctly declared in tc.data to not be recognized was fixed For getting
	started with Swift, please take a look at the documentation page	
</p>

<h3>Swift 0 RC2</h3>

<p>
	The 2nd release candidate of Swift version 0:  
	[<a	href="../packages/vdsk-0-rc1.tar.gz">tar.gz</a>] This release candidate
	features improvements in startup speed and the addition of ENV:: and
	GLOBUS:: profiles.	
</p>

<h3>Swift 0 RC1</h3>

<p>
	The 1st release candidate of Swift version 0:  
	[<a	href="../packages/vdsk-0-rc1.tar.gz">tar.gz</a>]
</p>

<h3>Swift 0 RC0</h3>

<p>
	The 0th release candidate of Swift version 0: 
	[<a href="../packages/vdsk-0-rc0.tar.gz">tar.gz</a>]
</p>


				</div>
				<!-- end left side content -->

				<!-- right side content -->
				<div id="right">
					<?php require('../inc/downloads_sidebar.php') ?>
				</div>
				<!-- end right side content -->
			</div>
			<!-- end content container-->

			<!-- footer -->
			<div id="footer"><?php require('../inc/footer.php') ?></div> 
			<!-- end footer -->

		</div>
		<!-- end entire page container -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
	</body>
</html>
