<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<title>Swift: Papers</title>
		<link href="../css/style2.css" rel="stylesheet" type="text/css" />
	</head>

	<body id="papers">
		<!-- entire page container -->
		<div id="container">
			<!-- header -->
			<div id="header">
				<?php require('../inc/header.php') ?>
			</div>
			<!-- end header -->
			<!-- nav -->
			<div id="nav">
				<?php require('../inc/nav.php') ?>
			</div>
			<!-- end nav -->
			<!-- content container -->
			<div id="content">
				<!-- left side content -->
				<div id="left">
				
<h1>PAPERS</h1>

<h3>Swift Technology and SwiftScript Application Papers</h3>

<div class="publication">
	<span class="authors">Michael Wilde, Mihael Hategan, Justin M. Wozniak, Ben Cliﬀord, Daniel S. Katz, Ian Foster</span>
        <span class="tite">Swift: A language for distributed parallel scripting</span>
	<span class="source">Parallel Computing</span>
        <span class="date">2011</span>
        [ <a href="SwiftLanguageForDistributedParallelScripting.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Joe DeBartolo, Glen Hocky, Michael Wilde, Jinbo Xu, Karl F. Freed, and Tobin R. Sosnick</span>
	<span class="title">Protein Structure Prediction Enhanced with Evolutionary Diversity: SPEED</span>
	<span class="source">Protein Science Journal</span>
	<span class="date">Jan 2010</span>
</div>

<div class="publication">
	<span class="authors">Michael Wilde, Ian Foster, Kamil Iskra, Pete Beckman, Zhao Zhang, Allan Espinosa, Mihael Hategan, Ben Clifford, Ioan Raicu</span>
	<span class="title">Parallel Scripting for Applications at the Petascale and Beyond</span>
	<span class="source">Computer, Vol. 42, No. 11</span>
	<span class="date">2009</span>
	[ <a href="SwiftParallelScripting.pdf">pdf</a> ] 
</div>

<div class="publication">
	<span class="authors">Andriy Fedorov, Benjamin Clifford, Simon K. Warﬁeld, Ron Kikinis, Nikos Chrisochoides 
</span>
	<span class="title">Non-Rigid Registration for Image-Guided Neurosurgery on the TeraGrid: A Case Study 
</span>
	<span class="source">College of William and Mary Technical Report</span>
	<span class="date">2009</span>
	[ <a href="http://www.wm.edu/as/computerscience/documents/cstechreports/WM-CS-2009-05.pdf">pdf</a> ] 
</div>

<div class="publication">
	<span class="authors">Stef-Praun, T., Madeira, G., Foster, I., and Townsend, R.</span>
	<span class="title">Accelerating solution of a moral hazard problem with Swift</span>
	<span class="source">e-Social Science</span>
	<span class="date">2007</span>
	[ <a href="SwiftForSocialSciences-2007.pdf">pdf</a> ] 
</div>

<div class="publication">
	<span class="authors">Stef-Praun, T., Clifford, B., Foster, I., Hasson, U., Hategan, M., Small, S., Wilde, M and Zhao,Y.</span>
	<span class="title">Accelerating Medical Research using the Swift Workflow System</span>
	<span class="source">Health Grid</span>
	<span class="date">2007</span>
	[ <a href="HealthGrid-2007-VDL2Bric.submitted-revised.pdf">pdf</a> ] 
</div>

<div class="publication">
	<span class="authors">Zhao Y., Hategan, M., Clifford, B., Foster, I., vonLaszewski, G., Raicu, I., Stef-Praun, T. and Wilde, M</span>
	<span class="title">Swift: Fast, Reliable, Loosely Coupled Parallel Computation</span>
	<span class="source">IEEE International Workshop on Scientific Workflows</span>
	<span class="date">2007</span>
	[ <a href="Swift-SWF07.pdf">pdf</a> ] 
</div>

<div class="publication">
	<span class="authors">Raicu, I., Zhao Y., Dumitrescu, C., Foster, I. and Wilde, M</span>
	<span class="title">Falkon: a Fast and Light-weight tasK executiON framework </span>
	<span class="source">Supercomputing Conference</span>
	<span class="date">2007</span>
	[ <a href="Falkon_SC07_v24.pdf">pdf</a> ] 
</div>


<div class="publication">
	<span class="authors">Zhao, Y.,Dobson, J., Moreau, L., Foster, I. and Wilde, M</span>
	<span class="title">A Notation and System for Expressing and Executing Cleanly Typed Workflows on Messy Scientific Data</span>
	<span class="source">SIGMOD</span>
	<span class="date">2005</span>
	[ <a href="sigmod-swf-vdl.pdf">pdf</a> ] 
</div>


<h3>Research leading to Swift and SwiftScript</h3>

<div class="publication">
	<span class="authors">Zhao, Y., Wilde, M. and Foster, I.</span>
	<span class="title">Virtual Data Language: A Typed Workflow Notation for Diversely Structured Scientific Data.</span>
	<span class="source">Taylor, I.J., Deelman, E., Gannon, D.B. and Shields, M. eds. Workflows for eScience, Springer,</span>
	<span class="date">2007,</span>
	<span class="pages">258-278.</span>
</div>

<div class="publication">
	<span class="authors">Zhao, Y., Dobson, J., Foster, I., Moreau, L. and Wilde, M.</span>
	<span class="title">A Notation and System for Expressing and Executing Cleanly Typed Workflows on Messy Scientific Data.</span>
	<span class="source">SIGMOD Record 34 (3),</span>
	<span class="pages">37-43</span>
	[ <a href="p37-special-sw-section-6.pdf">pdf</a> ] 	
</div>

<div class="publication">
	<span class="authors">Moreau, L., Zhao, Y., Foster, I., Voeckler, J. and Wilde, M.,</span>
	<span class="title">XDTM: XML Data Type and Mapping for Specifying Datasets.</span>
	<span class="source">European Grid Conference,</span>
	<span class="date">2005.</span>
	[ <a href="egc05.pdf">pdf</a> ] 	
</div>

<div class="publication">
	<span class="authors">Foster, I., Voeckler, J., Wilde, M. and Zhao, Y.,</span>
	<span class="title">The Virtual Data Grid: A New Model and Architecture for Data-Intensive Collaboration.</span>
	<span class="source">Conference on Innovative Data Systems Research,</span>
	<span class="date">2003.</span>
	[ <a href="ModelAndArchForDataCollab2003.pdf">pdf</a> ]
</div>

<h3>Karajan Technology used in Swift</h3>

<div class="publication">
	<span class="authors">von Laszewski, G., Hategan, M. and Kodeboyina, D.</span>
	<span class="title">Java CoG Kit Workflow.</span>
	<span class="source">Taylor, I.J., Deelman, E., Gannon, D.B. and Shields, M. eds. Workflows for Science,</span>
	<span class="date">2007,</span>
	<span class="pages">340-356.</span>
	[ <a href="vonLaszewski-workflow-book.pdf">pdf</a> ]
</div>

<h3>Virtual Data Language and Virtual Data System - predecessors to Swift</h3>

<div class="publication">
	<span class="authors">Zhao, Y., Wilde, M. and Foster, I.,</span>
	<span class="title">Applying the Virtual Data Provenance Model.</span>
	<span class="source">International Provenance and Annotation Workshop, Chicago, Illinois,</span>
	<span class="date">2006.</span>
	[ <a href="VirtualDataProvenance.pdf">pdf</a> ] 	
</div>

<div class="publication">
	<span class="authors">V&ouml;ckler, J.-S., Mehta, G., Zhao, Y., Deelman, E. and Wilde, M.,</span>
	<span class="title">Kickstarting Remote Applications.</span>
	<span class="source">2nd International Workshop on Grid Computing Environments,</span>
	<span class="date">2006.</span>
	[ <a href="Kickstarting2006.pdf">pdf</a> ] 
</div>

<div class="publication">
	<span class="authors">Zhao, Y., Wilde, M., Foster, I., Voeckler, J., Jordan, T., Quigg, E. and Dobson, J.,</span>
	<span class="title">Grid Middleware Services for Virtual Data Discovery, Composition, and Integration.</span>
	<span class="source">2nd International Workshop on Middleware for Grid Computing,</span>
	<span class="date">2004.</span>
	[ <a href="p57-zhao.pdf">pdf</a> ] 	
</div>


<div class="publication">
	<span class="authors">Foster, I., Voeckler, J., Wilde, M. and Zhao, Y.,</span>
	<span class="title">Chimera: A Virtual Data System for Representing, Querying, and Automating Data Derivation.</span>
	<span class="source">14th Intl. Conf. on Scientific and Statistical Database Management, Edinburgh, Scotland,</span>
	<span class="date">2002.</span>
	[ <a href="Chimra2002.pdf">pdf</a> ] 
</div>

<div class="publication">
	<span class="authors">V&ouml;ckler, J.-S., Wilde, M. and Foster, I.</span>
	<span class="title">The GriPhyN Virtual Data System.</span>
	<span class="source">Technical Report GriPhyN-2002-02,</span>
	<span class="date">2002.</span>
</div>

<div class="publication">
	<span class="authors">Zhao, Y., Wilde, M., Foster, I., Voeckler, J., Dobson, J., Gilbert, E., Jordan, T. and Quigg, E.</span>
	<span class="title">Virtual Data Grid Middleware Services for Data-intensive Science.</span>
	<span class="source">Concurrency and Computation: Practice and Experience, 18 (6),</span>
	<span class="pages">595-608.</span>
	[ <a href="cpe2000.pdf">pdf</a> ] 
</div>

<h3>VDL Applications - predecessors to Swift</h3>

<div class="publication">
	<span class="authors">Nefedova, V., Jacob, R., Foster, I., Liu, Y., Liu, Z., Deelman, E., Mehta, G. and Vahi, K.,</span>
	<span class="title">Automating Climate Science: Large Ensemble Simulations on the TeraGrid with the GriPhyN Virtual Data System.</span>
	<span class="source">2nd IEEE International Conference on eScience and Grid Computing,</span>
	<span class="date">2006.</span>
	[ <a href="AutomatingClimateScience.pdf">pdf</a> ] 	
</div>

<div class="publication">
	<span class="authors">Horn, J.V., Dobson, J., Woodward, J., Wilde, M., Zhao, Y., Voeckler, J. and Foster, I.</span>
	<span class="title">Grid-Based Computing and the Future of Neuroscience Computation.</span>
	<span class="source">Methods in Mind, MIT Press,</span>
	<span class="date">2006.</span>
</div>

<div class="publication">
	<span class="authors">Sulakhe, D., Rodriguez, A., Wilde, M., Foster, I. and Maltsev, N.,</span>
	<span class="title">Using Multiple Grid Resources for Bioinformatics Applications in GADU.</span>
	<span class="source">IEEE/ACM International Symposium on Cluster Computing and Grid,</span>
	<span class="date">2006.</span>
	[ <a href="GridResourcesForGADU.pdf">pdf</a> ] 	
</div>

<div class="publication">
	<span class="authors">Sulakhe, D., Rodriguez, A., D'Souza, M., Wilde, M., Nefedova, V., Foster, I. and Maltsev, N.</span>
	<span class="title">GNARE: An Environment for Grid-Based High-Throughput Genome Analysis.</span>
	<span class="source">Journal of Clinical Monitoring and Computing.</span>
	[ <a href="BioGrid2005.pdf">pdf</a> ] 	
</div>

<div class="publication">
	<span class="authors">Arbree, A., Avery, P., Bourilkov, D., Cavanaugh, R., Katageri, S., Graham, G., Rodriguez, J., Voeckler, J. and Wilde, M.,</span>
	<span class="title">Virtual Data in CMS Production.</span>
	<span class="source">Computing in High Energy and Nuclear Physics,</span>
	<span class="date">2003.</span>
	[ <a href="VDS-CMS.pdf">pdf</a> ] 
</div>

<div class="publication">
	<span class="authors">Arbree, A., Avery, P., Bourilkov, D., Cavanaugh, R., Rodriguez, J., Graham, G., Wilde, M. and Zhao, Y.,</span>
	<span class="title">Virtual Data in CMS Analysis.</span>
	<span class="source">Computing in High Energy and Nuclear Physics,</span>
	<span class="date">2003.</span>
	[ <a href="VirtualDataInCMS.pdf">pdf</a> ] 	
</div>

<div class="publication">
	<span class="authors">Annis, J., Zhao, Y., Voeckler, J., Wilde, M., Kent, S. and Foster, I.,</span>
	<span class="title">Applying Chimera Virtual Data Concepts to Cluster Finding in the Sloan Sky Survey.</span>
	<span class="source">SC2002, Baltimore, MD,</span>
	<span class="date">2002.</span>
	[ <a href="SSDS-SC02.pdf">pdf</a> ] 
</div>

<div class="publication">
	<span class="authors">Bardeen, M., Gilbert, E., Jordan, T., Nepywoda, P., Quigg, E., Wilde, M. and Zhao, Y.</span>
	<span class="title">The QuarkNet/Grid Collaborative Learning e-Lab.</span>
	<span class="source">Future Generation Computer Systems, 22 (6),</span>
	<span class="pages">700-708.</span>
	[ <a href="clag_paper.pdf">pdf</a> ] 
</div>

<div class="publication">
	<span class="authors">Zhao, Y.</span>
	<span class="title">Virtual Galaxy Clusters: An Application of the GriPhyN Virtual Data Toolkit to Sloan Digital Sky Survey Data.</span>
	<span class="source">MS thesis, University of Chicago, GriPhyN-2002-06,</span>
	<span class="date">2002.</span>
</div>


<h3>Related Workflow Scheduling and Provenance Research</h3>

<div class="publication">
	<span class="authors">Malewicz, G., Foster, I., Rosenberg, A. and Wilde, M.,</span>
	<span class="title">A Tool for Prioritizing DAGMan Jobs and Its Evaluation.</span>
	<span class="source">IEEE International Symposium on High Performance Distributed Computing,</span>
	<span class="date">2006.</span>
	[ <a href="jogc_03.pdf">pdf</a> ] 	
</div>

<div class="publication">
	<span class="authors">Meyer, L., Scheftner, D., Voeckler, J., Mattoso, M., Wilde, M. and Foster, I.,</span>
	<span class="title">An Opportunistic Algorithm for Scheduling Workflows on Grids.</span>
	<span class="source">VECPAR'06, Rio De Janiero,</span>
	<span class="date">2006.</span>
	[ <a href="OpportunisticAlgoritmForSchedulingWokflows.pdf">pdf</a> ] 
</div>

<div class="publication">
	<span class="authors">Moreau, L. and others,</span>
	<span class="title">The First Provenance Challenge,</span>
	<span class="source">Concurrency and Computation: Practice and Experience.</span>
	[ <a href="challenge-editorial.pdf">pdf</a> ] 
</div>


	
			</div>

				<!-- end left side content -->
				<!-- right side content -->
				<div id="right">
					<?php require('../inc/papers_sidebar.php') ?>
				</div>
				<!-- end right side content -->
			</div>
			<!-- end content container-->
			<!-- footer -->
			<div id="footer"><?php require('../inc/footer.php') ?></div> 
			<!-- end footer -->

		</div>
		<!-- end entire page container -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
	</body>
</html>
