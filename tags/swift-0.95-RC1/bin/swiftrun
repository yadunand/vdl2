#!/usr/bin/perl -w

use strict;
use warnings;
use FindBin qw($Bin);
use lib "$FindBin::Bin/../lib/perl";
use File::Basename;
use File::Path;
use Getopt::Long qw(GetOptionsFromArray);
use Cwd;

Getopt::Long::Configure("pass_through");

my %properties = ();		# Hash storing all swift properties
my @property_files = ();	# List of swift property files to be read
my $option_tcfile;		# Value of -tc.file option
my $option_sitesfile;		# Value of -sites.file option
my $option_siteslist;		# Value of -sites option, comma separated
my $option_config;		# Value of -config option
my $option_listconfig; 		# List configuration files used, values, then exit
my $option_help;                # Print help
my $option_version;             # Print version number
my @sites;			# List of sites from -sites in array
my $run_directory;		# Run directory to be created, run.NNNN
my @new_swift_args;		# New command line arguments
my @original_swift_args;	# Original arguments


# List of xml shortcuts
my %xml_aliases = (
                   'allocationstepsize'        => 'globus.allocationstepsize',
                   'delaybase'                 => 'karajan.delaybase',
                   'highoverallocation'        => 'globus.highoverallocation',
                   'initialscore'              => 'karajan.initialscore',
                   'internalhostname'          => 'globus.internalhostname',
                   'jobspernode'               => 'globus.jobspernode',
                   'jobthrottle'               => 'karajan.jobthrottle',
                   'jobtype'                   => 'globus.jobtype',
                   'lowoverallocation'         => 'globus.lowoverallocation',
                   'maxnodes'                  => 'globus.maxnodes',
                   'maxsubmitrate'             => 'karajan.maxsubmitrate',
                   'maxtime'                   => 'globus.maxtime',
                   'maxwalltime'               => 'globus.maxwalltime',
                   'nodegranularity'           => 'globus.nodegranularity',
                   'overallocationdecayfactor' => 'globus.overallocationdecayfactor',
                   'provider'                  => 'provider.coaster',
                   'queue'                     => 'globus.queue',
                   'slots'                     => 'globus.slots',
                   'spread'                    => 'globus.spread',
                   'remotemonitorenabled'      => 'globus.remotemonitorenabled',
                   'reserve'                   => 'globus.reserve',
);

# Verify a property file exists and add it to the search path
sub add_property_file {
   my $property_file = $_[0];
   if( -e "$property_file" ) {
      push( @property_files, $property_file );
   }
}

# Create a string with a single pool entry
sub create_pool_entry {
   my $site = $_[0];
   my $pool = "\n<pool handle=\"" . $site . "\">\n";
   while( my( $key, $value ) = each( %properties ) ) {
      if( $key =~ /^\s*site.$site/ ) {
         $pool .= &property_to_xml( $key, $value );
      }
   }
   $pool .= "</pool>\n";
   return $pool;
}

# Given a string in format site.beagle.globus.jobsPerNode=12, convert to xml
sub property_to_xml {
   my $key = $_[0];
   my $value = $_[1];

   my ( $site, $sitename, $namespace, $property ) = split(/\./, $key);

   # Allow exceptions for swift properties with dot separators (ie key=slurm.properties)
   if ( defined($property) && $namespace =~ m/^slurm|^condor/ ) { 
      ( $site, $sitename, $namespace, $property ) = split(/\./, $key);
      $property = "$namespace.$property";
      $namespace = "globus";
   }

   # In the case of a shortened definition, check for aliases 
   if ( !defined( $property ) ) {
      if ( defined( $xml_aliases{ lc( $namespace )})) { 
         ( $namespace, $property ) = split(/\./, $xml_aliases{ lc($namespace) });
      }
   }     
   
   # Make sure to handle special cases that are not in the namespace=x, key=y, format
   if    ( $namespace eq "provider" )      { return "<execution provider=\"" . $property . "\" jobmanager=\"" . $value . "\"/>\n"; }
   elsif ( $namespace eq "workdirectory" ) { return "<workdirectory>$value</workdirectory>\n"; }
   elsif ( $namespace eq "filesystem" )    { return "<filesystem provider=\"" . $value . "\"/>\n"; }
   else                                    { return "<profile namespace=\"" . $namespace . "\" key=\"" . $property . "\">" . $value . "</profile>\n"; }
} 

# Write apps to file in tc.data format
sub write_apps {
   my $app_filename = $_[0];
   open( APP_FILE, ">$run_directory/$app_filename" ) || die "Unable to open $run_directory/$app_filename\n";
   print APP_FILE "* * * INSTALLED INTEL32::LINUX null\n";

   # Find app definitions
   while( my( $key, $value ) = each( %properties ) ) {
      if( $key =~ /^\s*app./ ) {
         my ( $app, $site, $command ) = split(/\./, $key);

         # Handle wildcards for site names
         if ( $site eq "*" ) { 
            foreach my $nsite( @sites ) {
               print APP_FILE "$nsite $command $value\n";
            }
         } 

         # Handle apps for defined sites
         elsif ( grep( /^$site$/, @sites ) ) {
            print APP_FILE "$site $command $value\n";
         }
      }
   }
   # Update command line args
   push( @new_swift_args, "-tc.file $run_directory/$app_filename" );
}

# Write properties to file
sub write_properties {
   my $property_filename = $_[0];
   open( PROPERTY_FILE, ">$run_directory/$property_filename" ) || die "Unable to open $run_directory/$property_filename\n";
   while( my( $key, $value ) = each( %properties ) ) {
      # Ignore properties that start with app. or site.
      next if $key =~ /^\s*app./;
      next if $key =~ /^\s*site/;
      print PROPERTY_FILE "$key=$value\n";
   }
   push( @new_swift_args, "-config $run_directory/$property_filename" );
}

# Check if a site is defined
sub site_is_defined {
   my $site = $_[0];
   my $isDefined=0;
   while( my( $key, $value ) = each( %properties ) ) {
     if( $key =~ m/^site\.$site/i ) {
        $isDefined=1;
      }
   }
   return $isDefined;
}

# Check if a service is defined
sub service_is_defined {
   my $site = $_[0];
   my $isDefined=0;
   while( my( $key, $value ) = each( %properties ) ) {
      if( $key =~ m/^service\.$site/i ) {
         $isDefined=1;
      }
   }
   return $isDefined;
}

# Verify a service is running
sub service_is_running {
   my $site = $_[0];
   my $service_is_running = 0;
   my $service_directory = "$ENV{HOME}/.swift/service/$site";
   if( -d "$service_directory" ) {
      if ( -f "$service_directory/pid" ) {
         my $pid = `cat $service_directory/pid`;
         $service_is_running = kill 0, $pid;
      }
   }
   return $service_is_running;
}

# Retrieve a service sites.xml
sub retrieve_service_xml {
   my $site = $_[0];
   my $sitexmlfile = "$ENV{HOME}/.swift/service/$site/sites.xml";
   my @xml;
   open(SXML, $sitexmlfile) || die "Unable to open $sitexmlfile: $!\n";
   while(<SXML>) {
      if(m/^<\?xml|^<config|config>$/i){ next; }
         push(@xml, $_);
   }
   close(SXML);
   return @xml;
}

sub swiftquit {
   print STDERR $_[0] . "\n";
   print " --SWIFTRUNQUIT ";
   rmtree $run_directory || die "Failed to remove $run_directory: $!\n";
   exit 1;
} 

# Write sites.xml to disk
sub write_sites {
   my $filename = $_[0];
   open(SITES, ">$run_directory/$filename") || die "Unable to open $run_directory/$filename\n";
   print SITES "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
   print SITES "<config xmlns=\"http://www.ci.uchicago.edu/swift/SwiftSites\">\n";

   if(defined ($properties{site}) && !defined($option_siteslist) ) { 
      push(@sites, split(',', $properties{site}))
   } 

   foreach my $site(@sites) {
      # XML from site definition
      if ( &site_is_defined($site) ) {
         print SITES &create_pool_entry($site);
      }

      # XML from service definition 
      elsif ( &service_is_defined($site)) {
         if( ! &service_is_running($site) ) {
            &swiftquit("Service process for $site is not running");
         }
         print SITES &retrieve_service_xml($site);
      }

      # Can't find definition
      else {
         &swiftquit("Site $site is not defined");
      }
   }

   print SITES "\n</config>\n";
   close(SITES);
   push(@new_swift_args, "-sites.file $run_directory/$filename");
}

# Getopt::Long can't accept dots in arguments, work around to make things easy
foreach my $arg(@ARGV) {
   $arg =~ s/sites.file/sitesfile/g;
   $arg =~ s/tc.file/tcfile/g;
}

# Command line arguments we care about
@original_swift_args = @ARGV;
GetOptionsFromArray(
           \@original_swift_args, 
           'config=s'        => \$option_config,
           'help'            => \$option_help,
           'listconfig'      => \$option_listconfig,
           'site|sites=s'    => \$option_siteslist,
           'sitesfile=s'     => \$option_sitesfile,
           'tcfile=s'        => \$option_tcfile,
           'version'         => \$option_version,
);

# Set @sites array from comma separated list of sites
if(defined($option_siteslist)) {
   @sites = split(',', $option_siteslist);
}

# Verify Swift is found in PATH
my $swift_etc_directory = dirname(dirname(`which swift`)) . "/etc";
if( ! -d $swift_etc_directory ) { die "Unable to find a valid Swift installation"; }

# Set the search order for properties
&add_property_file("$swift_etc_directory/swift.properties");
&add_property_file("$ENV{SWIFT_SITE_CONF}/swift.properties") if defined($ENV{SWIFT_SITE_CONF});
&add_property_file("$ENV{HOME}/.swift/swift.properties");
&add_property_file("swift.properties");

# Set property values
foreach my $property_file(@property_files) {
   open(PROPERTIES, $property_file) || die "Unable to open $property_file";

   while( <PROPERTIES> ) {
      chomp;
      next if /^\s*#/ || /^(\s)*$/; # Ignore blank lines and comments
      $_ =~ s/^\s+//;               # Remove leading whitespace

      # Handle brackets 
      if( /^site\.|^service\./ && /{/ ) { 
         my $prefix = (split /\s+{/)[0];
         while( <PROPERTIES> ) {
            chomp;
            next if /^\s*#/ || /^(\s)*$/; 
            $_ =~ s/^\s+//;               
            if( /^}/ ) { last; } 
            my ($key, $value) = split('=', ($prefix . ".$_"), 2);
            if($key eq "sites") { $key = "site"; }
            $value =~ s/\$(\w+)/$ENV{$1}/g;
            $properties{ $key } = $value;
         }
      }

      else {
         my ($key, $value) = split('=', $_, 2);
         if($key eq "sites") { $key = "site"; }
         if(defined($key) && defined($value) && length($key) && length($value)) {
            $value =~ s/\$(\w+)/$ENV{$1}/g; # Substitute environment variables
            $properties{ $key } = $value;
         }
      }
   }
}

# If -listconfig is specified, print values and exit
if( $option_listconfig ) {
   print "\n";
   foreach my $site(@property_files) {
      print "Reading from: $site\n";
   }
   print "\nValues:\n";
   foreach my $key (sort keys %properties) {
      print "\t$key=$properties{$key}\n";
   }
   print "\n";
   exit;
}

# If -version or -help is specified, pass and exit
if ( $option_help ) {
   print "-help\n";
   exit;
} elsif ( $option_version ) {
   print "-version\n";
   exit;
}

# Create/increment run directory
my $run_number = 0;
my @run_directories = <run???>;

if(@run_directories) {
   $run_number = $run_directories[-1];
   $run_number =~ s/run//g;
   $run_number++;
}

$run_directory = getcwd() . "/" . sprintf "run%03s", $run_number;
mkdir($run_directory) || die "Unable to create run directory $run_directory\n";
@new_swift_args = ("-Dscript.dir=" . $run_directory . "/scripts");

# Write swift configuration files
&write_sites("sites.xml") if defined( $option_siteslist ) || defined($properties{site});
&write_apps("apps")    if !defined( $option_tcfile ) && ( defined( $option_siteslist ) || defined($properties{site}));
&write_properties("cf")   if !defined( $option_config );

# Repair args with dots
foreach my $arg(@ARGV) {
   $arg =~ s/sitesfile/sites.file/g;
   $arg =~ s/tcfile/tc.file/g;
}

# Make sure -sites isn't in the arguments we send back to swift
while( @ARGV ) {
   my $option = shift;
   if ( $option eq "-site" || $option eq "-sites" ) { shift; }
   else { push( @new_swift_args, $option ); }
}

push( @new_swift_args, sprintf( "-logfile $run_directory/run%03s.log", $run_number ));

foreach my $arg( @new_swift_args ) {
   print "$arg ";
}

print "\n";
