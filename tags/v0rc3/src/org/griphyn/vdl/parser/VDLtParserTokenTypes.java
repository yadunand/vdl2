// $ANTLR 2.7.5 (20050128): "VDL.g" -> "VDLtLexer.java"$

package org.griphyn.vdl.parser;

import org.antlr.stringtemplate.*;
import java.util.List;
import java.util.Iterator;

public interface VDLtParserTokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int LITERAL_namespace = 4;
	int ID = 5;
	int STRING_LITERAL = 6;
	int SEMI = 7;
	int LITERAL_type = 8;
	int LCURLY = 9;
	int LBRACK = 10;
	int RBRACK = 11;
	int COMMA = 12;
	int RCURLY = 13;
	int ASSIGN = 14;
	int COLON = 15;
	int LT = 16;
	int GT = 17;
	int LPAREN = 18;
	int RPAREN = 19;
	int LITERAL_int = 20;
	int LITERAL_string = 21;
	int LITERAL_float = 22;
	int LITERAL_date = 23;
	int LITERAL_uri = 24;
	int LITERAL_bool = 25;
	int LITERAL_break = 26;
	int LITERAL_continue = 27;
	int LITERAL_for = 28;
	int LITERAL_if = 29;
	int LITERAL_else = 30;
	int LITERAL_foreach = 31;
	int LITERAL_in = 32;
	int LITERAL_while = 33;
	int LITERAL_repeat = 34;
	int LITERAL_until = 35;
	int LITERAL_switch = 36;
	int LITERAL_case = 37;
	int LITERAL_default = 38;
	int LITERAL_app = 39;
	int AT = 40;
	int LITERAL_stdin = 41;
	int LITERAL_stdout = 42;
	int LITERAL_stderr = 43;
	int LITERAL_service = 44;
	int LITERAL_wsdlURI = 45;
	int LITERAL_portType = 46;
	int LITERAL_operation = 47;
	int LITERAL_request = 48;
	int LITERAL_response = 49;
	int OR = 50;
	int AND = 51;
	int EQ = 52;
	int NE = 53;
	int LE = 54;
	int GE = 55;
	int PLUS = 56;
	int MINUS = 57;
	int STAR = 58;
	int DIV = 59;
	int MOD = 60;
	int NOT = 61;
	int DOT = 62;
	int INT_LITERAL = 63;
	int FLOAT_LITERAL = 64;
	int LITERAL_true = 65;
	int LITERAL_false = 66;
	int LITERAL_null = 67;
	int NUMBER = 68;
	int WS = 69;
	int SL_CCOMMENT = 70;
	int SL_SCOMMENT = 71;
	int ML_COMMENT = 72;
	int ESC = 73;
	int EXPONENT = 74;
}
