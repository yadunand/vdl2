/*
 * XML Type:  Mode
 * Namespace: http://www.griphyn.org/2006/08/vdl
 * Java type: org.griphyn.vdl.model.Mode
 *
 * Automatically generated - do not modify.
 */
package org.griphyn.vdl.model.impl;
/**
 * An XML Mode(@http://www.griphyn.org/2006/08/vdl).
 *
 * This is an atomic type that is a restriction of org.griphyn.vdl.model.Mode.
 */
public class ModeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements org.griphyn.vdl.model.Mode
{
    
    public ModeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ModeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
