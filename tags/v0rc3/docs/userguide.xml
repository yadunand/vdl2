<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd" [] >

<article>
	<articleinfo revision="0.1">
		<title>The VDL2 User Guide</title>
		<author>
			<firstname>Yong</firstname>
			<surname>Zhao</surname>
		</author>
		<abstract>
			<formalpara>
				<title></title>
				<para>
VDL2 is an improved version of the Virtual Data Language that supports dataset typing and mapping, dataset iteration, conditional branching, sub-workflow composition and other advanced features. In this document, we give a few examples to illustrate how to write VDL2 programs to exploit such features. VDL2 syntax has two flavors; one is in a textual format, the other in XML. For simplification purpose, all examples in this document are written in the textual format, which can be automatically translated into the XML format using one of the tools in our virtual data system. 				
				</para>
			</formalpara>
		</abstract>
	</articleinfo>

	<sect1>
		<title>Overview</title>
		<para>
Before we get into the details of the examples, let’s first go over some of the major components in VDL2		
		</para>
		<sect2>
			<title>Type System</title>
			<para>
The VDL2 type system is based on the XDTM (XML dataset typing and mapping) model, where a dataset’s logical structure is described via (a subset of) XML Schema, which defines primitive scalar data types such as Boolean, Integer, String, Float, and Date, and also allows for the definition of complex types via the composition of simple and complex types. 
			</para>
			<para>
We use a C-like syntax to represent VDL types. For instance, we can define an order as composed of an array of items, where each item has a name and a quantity:

<programlisting>
type order {
	item[] items;
}

type item {
	string name;
	integer quantity;
}
</programlisting>

		</para>
		
		<para>
There is also an any type where we do not care about the inner logical structure; for instance, when we deal with a binary file, and we don’t need to look at the actual content of that file in the workflow. The syntax for such a declaration is as follows:

<programlisting>
type binaryfile {}
</programlisting>

A dataset’s physical representation is defined by a mapping descriptor, which defines how each element in the dataset’s logical schema is stored in, and fetched from, physical structures such as directories, files, and database tables.  
		</para>
		
		<para>
In order to permit reuse for different datasets, mapping functions may be parameterized for such things as dataset locations. Thus, in order to access a dataset, we need to know three things: its type schema, its mapping descriptor, and the value(s) of any parameter(s) associated with the mapping descriptor. For example, if we want to describe a dataset, whose logical structure is defined by binaryfile, and whose physical representation is a file called “file1.bin” located at “/home/yongzh/data/”, then the dataset can be declared as follows:		

<programlisting>
binaryfile f1&lt;fixed_mapper;file="/home/yongzh/data/file1.bin"&gt;
</programlisting>

		</para>
				
		<para>
		The above example declares a dataset called f1, which uses a fixed_mapper to map a file from a specific location. What the fixed_mapper does is merely returning the file name to the system. VDL2 also has a simplified syntax for such cases since fixed_mapper is the default mapper used:

<programlisting>
binaryfile f1&lt;"/home/yongzh/data/file1.bin"&gt;
</programlisting>

		</para>		
				
		</sect2>
		
		<sect2>
			<title>Procedures</title>
			<para>
			Datasets are operated on by procedures, which take typed data as input, perform computations on those data, and produce typed data as output. An atomic procedure defines an interface to an executable program or service, while a compound procedure composes calls to atomic procedures, compound procedures, and/or control statements. 
			</para>
			
			<para>
			A procedure declaration is like a function definition, where we specify the function name and the formal parameters. VDL procedures can take multiple inputs and produce multiple outputs, and inputs are organized to the right of the function name where outputs to the left. For instance:

<programlisting>
(type3 out1, type4 out2) myproc (type1 in1, type2 in2)
</programlisting>

			The above example declares a procedure called myproc, which has two inputs in1 (of type type1) and in2 (of type type2) and two outputs out1 (of type type3) and out2 (of type type4).
			</para>
	
			<para>
	A formal parameter can have a default value, in which case this parameter is also deemed an optional parameter. When we call a procedure, passing in the actual parameters, we allow both positional parameter and keyword parameter passing, provided that all optional parameters have to be declared after the required parameters and any optional parameter has to be bound using keyword parameter passing. So for instance if we declare a procedure myproc1:

<programlisting>
(binaryfile bf) myproc1 (int i, string s="foo")
</programlisting>

			Then the procedure can be called like this

<programlisting>
binaryfile mybf = myproc1(1);
</programlisting>

			or like this supplying the value for the optional parameter s:

<programlisting>
binaryfile mybf = myproc1 (1, s="bar");
</programlisting>

			</para>
			
			<para>
			The body of an atomic procedure specifies how to invoke an external executable program or Web Service, and how logical data types are mapped to command line arguments or SOAP messages. A complete specification for myproc1 can be:

<programlisting>
(binaryfile bf) myproc1 (int i, string s="foo") {
	app {
		myapp1 i s @filename(bf);
	}
}			
</programlisting>

Which specifies that myproc1 invokes an executable, passing the values of i, s and the file name of bf as command line arguments. The @filename notation serves as a mapping function denoting that the argument should be mapped as a file name, and since the notation is often required in invoking applications, often we can omit the filename part and use the @ sign only.			
			</para>
			A compound procedure contains a set of calls to other procedures. Shared variables in the body of a compound procedure specify data dependencies and thus the execution sequence of the procedure calls. For simple illustration, we define a compound procedure in below:

<programlisting>
(type2 b) foo_bar (type1 a) {
	type3 c;
	c = foo(a);    // c holds the result of foo
	b = bar(c);    // c is an input to bar
}
</programlisting>

		</sect2>
		
		
		<sect2>
			<title>"Control Constructs"</title>
			<para>
			Control constructs are special control entities in a workflow to control the direction of execution. VDL provides if, switch, foreach, and while constructs, with syntax and semantics similar to comparable constructs in high-level languages. 
			</para>
			
			<para>
			The foreach construct is used for dataset iteration. It iterates over each of the items in a composite dataset, and applies some operations to that item. We illustrate the use of the foreach construct in the following example using the type order defined above:

<programlisting>
check_order (order o) {
	foreach item it in o.items {
		check_item(it);
	}
}
</programlisting>

			</para>
		</sect2>
	</sect1>
	
	
	<sect1>
		<title> Simple Examples </title>
		
		<sect2>
		<title>Hello World</title>
		
		<para>
		The hello world example calls to the UNIX command echo to print the text “hello world” and redirect the standard output into a file. Please note that the echo command in the procedure body is only a logical command, it needs to be mapped to an executable by means of a transformation catalog.

<programlisting>
type file {} //define a type for file
(file t) echo (string s) { //procedure declaration
	app {
		echo s stdout=@filename(t); //redirect stdout to a file
	}
}

file hw = echo("hello world"); //procedure call
</programlisting>

		</para>
		</sect2>	
		
		<sect2>
		<title>Default Value</title>
		
		<para>
		A similar example to the hello world, but the procedure has a parameter with a default value.

<programlisting>
type file {}		

(file t) echo (string s="hello world") { //s has a default value
    app {
		echo s stdout=@filename(t); //redirect stdout to a file
	}
}

file hw1, hw2;
hw1 = echo(); // procedure call using the default value
hw2 = echo(s="hello again"); // using a different value
</programlisting>

		</para>

		</sect2>
		
		<sect2>
		<title>A Two-Step Workflow</title>
		
		<para>
		This workflow is essentially doing a UNIX pipe

<programlisting>
ls *.txt | wc
</programlisting>

that counts the number of ".txt" file and save the output to a file.
		
<programlisting>
type file {}		

(file f) ls (string s) { 
	app {
		ls s stdout=@filename(f);
	}
}

(file c) wc (file f) { 
	app {
		wc stdin=@filename(f) stdout=@filename(c);
	}
}

file list, count;
list = ls("*.txt");
count = wc(list);
</programlisting>

		</para>
		</sect2>
		
		<sect2>
		<title>Array Example</title>
		
		<para>
		This example shows how to map an array into a command line argument.

<programlisting>
type file {}

(file t) echo (string s[]) {
	app {
		echo s[*] stdout=@filename(t);
	}
}

string s[] = ["how","are","you"];
file hw = echo(s);
</programlisting>

		</para>
		</sect2>
		
		<sect2>
		<title>Array Iteration Example</title>
		
		<para>
		This example shows how to use arrays and the foreach construct.

<programlisting>
type file {}

(file f) echo (string s) {
	app {
		echo s stdout=@filename(f);
	}
}

(file fa[]) echo_batch (string sa[]) {
	foreach string s, i in sa {
		fa[i] = echo(s);
	}
}

string sa[] = ["hello","hi there","how are you"];
file fa[];
fa = echo_batch(sa);
</programlisting>

		</para>
		</sect2>
		
		<sect2>
		<title>Array Mapper</title>
		
		<para>
		This example shows how to map a list of files that follow a certain pattern. Note the print function is a built-in Karajan function; we use it here just for printing out the file names.

<programlisting>
type file {};

file files[]&lt;simple_mapper;pattern="*",location="/usr/bin"&gt;;

foreach f in files {
	print(f);
}
</programlisting>

		</para>
		</sect2>
		
		<sect2>
		<title>Range</title>
		
		<para>
		Range specifies a range of values, with a starting value, an ending value, and an incremental step. Range is very useful in parameter sweeping. It is generally used to initialize an array with a range of values, which can be then iterated over.

<programlisting>
int nums[] = [0:20:2];  // generate a list 0, 2, 4, 6, 8 …
foreach num in nums {
	print(num);
}
</programlisting>

		</para>
		</sect2>
		
	</sect1>
	
	<sect1>
		<title>Application Examples</title>
		<sect2>
			<title>Big Diamond</title>
		</sect2>

		<sect2>
			<title>Quarknet Detector</title>
		</sect2>

		<sect2>
			<title>fMRI</title>
		</sect2>

		<sect2>
			<title>CADGrid</title>
		</sect2>

		<sect2>
			<title>BRIC</title>
		</sect2>

	</sect1>
	
</article>