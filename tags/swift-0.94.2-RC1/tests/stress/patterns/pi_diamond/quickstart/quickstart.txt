Swift Quickstart
================

Download and Install
--------------------
// This statment sounds bad.
This section will get swift into your environment.

Get Java
~~~~~~~~
Since Swift is written in Java, Java Runtime Environment 1.5 or greater is a required by Swift. 
We recommend Oracle Java, which can be downloaded from the Oracle http://www.oracle.com/technetwork/java/javase/downloads/index.html[website]. 


Get stable Swift
~~~~~~~~~~~~~~~~
Download the latest packaged binaries from the swift downloads http://swift-lang.org/downloads/index.php[page].
Untar the file, and export the path. Bash users could append their .bashrc
file with the export statement to have swift in their path over multiple sessions.

-----
wget http://swift-lang.org/packages/swift-latest.tar.gz ~
tar -xzf ~/swift-latest.tar.gz
export PATH=~/swift-<version>/bin:$PATH
swift -version
-----

Running the examples
--------------------

There is an examples directory in your swift-<version> folder.
We will go over a montecarlo simulation style workflow which finally gives us the value of pi.

In the swift-<version> 

These examples gets you to run a simple swift program.

-----
cd examples/tutorial
swift hello.swift
-----


More documentation on how to run Swift can be found at 
http://www.ci.uchicago.edu/swift/docs/index.php.
