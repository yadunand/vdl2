Swift Documentation
===================

General principles:

 - sections and subsections are ordered from <sect1>, <sect2>, or using
   arbitrary depth <section> tags
 - code samples are given inside <programlisting> tags, which will cause
   syntax highlighting to happen automatically
 - user interactions / screen output are given inside <screen> tags.
 - be careful to escape in-text "<" to "&lt;"
 - there are some conventions for using id attributes at various
   places in the documents - for example, some tutorial sections use
   'tutorial.<something>'; profile entries in the user guide use
   'profile.<namespace>.<key>'. Try to keep id attributes unique across
   the entire document set.

The first time guides are built in a particular checkout, it is necessary
to place the docbook formatting stylesheets under the formatting/docbook/
directory. This can be done with a symlink if docbook is installed elsewhere.

For example:

A) On the CI network, /home/hategan/docbook contains a docbook installation that
can be linked like this:

$ cd formatting
$ ln -s /home/hategan/docbook/ docbook


B) on benc's os x machine:

# install docbook from DarwinPorts
$ sudo port install docbook-xsl

# setup links
$ cd formatting
$ ln -s /opt/local/share/xsl/docbook-xsl/ docbook

C) in general:

 1) Install Apache

 2) Install PHP (cf. http://dan.drydog.com/apache2php.html)

 3) Add these lines to Apache's httpd.conf:

    AddHandler php5-script php
    AddType text/html       php

 4) Update httpd.conf, adding index.php:

    DirectoryIndex index.html index.php

 5) Make sure perms are correct

 6) Create formatting/docbook link (see above) 

 7) Create fop link

Once the links are set up, the buildguides.sh script will build all guides
as php.  Run it with no parameters, like this:

$ ./buildguides.sh

or use make to get HTML documents like this:

$ make userguide.html

