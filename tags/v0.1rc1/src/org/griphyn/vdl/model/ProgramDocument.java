/*
 * An XML document type.
 * Localname: program
 * Namespace: http://www.griphyn.org/2006/08/vdl
 * Java type: org.griphyn.vdl.model.ProgramDocument
 *
 * Automatically generated - do not modify.
 */
package org.griphyn.vdl.model;


/**
 * A document containing one program(@http://www.griphyn.org/2006/08/vdl) element.
 *
 * This is a complex type.
 */
public interface ProgramDocument extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ProgramDocument.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s846598305CE89DEF7AE9D98B8ED64120").resolveHandle("program20c0doctype");
    
    /**
     * Gets the "program" element
     */
    org.griphyn.vdl.model.ProgramDocument.Program getProgram();
    
    /**
     * Sets the "program" element
     */
    void setProgram(org.griphyn.vdl.model.ProgramDocument.Program program);
    
    /**
     * Appends and returns a new empty "program" element
     */
    org.griphyn.vdl.model.ProgramDocument.Program addNewProgram();
    
    /**
     * An XML program(@http://www.griphyn.org/2006/08/vdl).
     *
     * This is a complex type.
     */
    public interface Program extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Program.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s846598305CE89DEF7AE9D98B8ED64120").resolveHandle("program6bd0elemtype");
        
        /**
         * Gets the "types" element
         */
        org.griphyn.vdl.model.TypesDocument.Types getTypes();
        
        /**
         * True if has "types" element
         */
        boolean isSetTypes();
        
        /**
         * Sets the "types" element
         */
        void setTypes(org.griphyn.vdl.model.TypesDocument.Types types);
        
        /**
         * Appends and returns a new empty "types" element
         */
        org.griphyn.vdl.model.TypesDocument.Types addNewTypes();
        
        /**
         * Unsets the "types" element
         */
        void unsetTypes();
        
        /**
         * Gets array of all "procedure" elements
         */
        org.griphyn.vdl.model.Procedure[] getProcedureArray();
        
        /**
         * Gets ith "procedure" element
         */
        org.griphyn.vdl.model.Procedure getProcedureArray(int i);
        
        /**
         * Returns number of "procedure" element
         */
        int sizeOfProcedureArray();
        
        /**
         * Sets array of all "procedure" element
         */
        void setProcedureArray(org.griphyn.vdl.model.Procedure[] procedureArray);
        
        /**
         * Sets ith "procedure" element
         */
        void setProcedureArray(int i, org.griphyn.vdl.model.Procedure procedure);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "procedure" element
         */
        org.griphyn.vdl.model.Procedure insertNewProcedure(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "procedure" element
         */
        org.griphyn.vdl.model.Procedure addNewProcedure();
        
        /**
         * Removes the ith "procedure" element
         */
        void removeProcedure(int i);
        
        /**
         * Gets array of all "variable" elements
         */
        org.griphyn.vdl.model.Variable[] getVariableArray();
        
        /**
         * Gets ith "variable" element
         */
        org.griphyn.vdl.model.Variable getVariableArray(int i);
        
        /**
         * Returns number of "variable" element
         */
        int sizeOfVariableArray();
        
        /**
         * Sets array of all "variable" element
         */
        void setVariableArray(org.griphyn.vdl.model.Variable[] variableArray);
        
        /**
         * Sets ith "variable" element
         */
        void setVariableArray(int i, org.griphyn.vdl.model.Variable variable);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "variable" element
         */
        org.griphyn.vdl.model.Variable insertNewVariable(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "variable" element
         */
        org.griphyn.vdl.model.Variable addNewVariable();
        
        /**
         * Removes the ith "variable" element
         */
        void removeVariable(int i);
        
        /**
         * Gets array of all "dataset" elements
         */
        org.griphyn.vdl.model.Dataset[] getDatasetArray();
        
        /**
         * Gets ith "dataset" element
         */
        org.griphyn.vdl.model.Dataset getDatasetArray(int i);
        
        /**
         * Returns number of "dataset" element
         */
        int sizeOfDatasetArray();
        
        /**
         * Sets array of all "dataset" element
         */
        void setDatasetArray(org.griphyn.vdl.model.Dataset[] datasetArray);
        
        /**
         * Sets ith "dataset" element
         */
        void setDatasetArray(int i, org.griphyn.vdl.model.Dataset dataset);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "dataset" element
         */
        org.griphyn.vdl.model.Dataset insertNewDataset(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "dataset" element
         */
        org.griphyn.vdl.model.Dataset addNewDataset();
        
        /**
         * Removes the ith "dataset" element
         */
        void removeDataset(int i);
        
        /**
         * Gets array of all "assign" elements
         */
        org.griphyn.vdl.model.Assign[] getAssignArray();
        
        /**
         * Gets ith "assign" element
         */
        org.griphyn.vdl.model.Assign getAssignArray(int i);
        
        /**
         * Returns number of "assign" element
         */
        int sizeOfAssignArray();
        
        /**
         * Sets array of all "assign" element
         */
        void setAssignArray(org.griphyn.vdl.model.Assign[] assignArray);
        
        /**
         * Sets ith "assign" element
         */
        void setAssignArray(int i, org.griphyn.vdl.model.Assign assign);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "assign" element
         */
        org.griphyn.vdl.model.Assign insertNewAssign(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "assign" element
         */
        org.griphyn.vdl.model.Assign addNewAssign();
        
        /**
         * Removes the ith "assign" element
         */
        void removeAssign(int i);
        
        /**
         * Gets array of all "call" elements
         */
        org.griphyn.vdl.model.Call[] getCallArray();
        
        /**
         * Gets ith "call" element
         */
        org.griphyn.vdl.model.Call getCallArray(int i);
        
        /**
         * Returns number of "call" element
         */
        int sizeOfCallArray();
        
        /**
         * Sets array of all "call" element
         */
        void setCallArray(org.griphyn.vdl.model.Call[] callArray);
        
        /**
         * Sets ith "call" element
         */
        void setCallArray(int i, org.griphyn.vdl.model.Call call);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "call" element
         */
        org.griphyn.vdl.model.Call insertNewCall(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "call" element
         */
        org.griphyn.vdl.model.Call addNewCall();
        
        /**
         * Removes the ith "call" element
         */
        void removeCall(int i);
        
        /**
         * Gets array of all "foreach" elements
         */
        org.griphyn.vdl.model.Foreach[] getForeachArray();
        
        /**
         * Gets ith "foreach" element
         */
        org.griphyn.vdl.model.Foreach getForeachArray(int i);
        
        /**
         * Returns number of "foreach" element
         */
        int sizeOfForeachArray();
        
        /**
         * Sets array of all "foreach" element
         */
        void setForeachArray(org.griphyn.vdl.model.Foreach[] foreachArray);
        
        /**
         * Sets ith "foreach" element
         */
        void setForeachArray(int i, org.griphyn.vdl.model.Foreach foreach);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "foreach" element
         */
        org.griphyn.vdl.model.Foreach insertNewForeach(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "foreach" element
         */
        org.griphyn.vdl.model.Foreach addNewForeach();
        
        /**
         * Removes the ith "foreach" element
         */
        void removeForeach(int i);
        
        /**
         * Gets array of all "if" elements
         */
        org.griphyn.vdl.model.If[] getIfArray();
        
        /**
         * Gets ith "if" element
         */
        org.griphyn.vdl.model.If getIfArray(int i);
        
        /**
         * Returns number of "if" element
         */
        int sizeOfIfArray();
        
        /**
         * Sets array of all "if" element
         */
        void setIfArray(org.griphyn.vdl.model.If[] xifArray);
        
        /**
         * Sets ith "if" element
         */
        void setIfArray(int i, org.griphyn.vdl.model.If xif);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "if" element
         */
        org.griphyn.vdl.model.If insertNewIf(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "if" element
         */
        org.griphyn.vdl.model.If addNewIf();
        
        /**
         * Removes the ith "if" element
         */
        void removeIf(int i);
        
        /**
         * Gets array of all "while" elements
         */
        org.griphyn.vdl.model.While[] getWhileArray();
        
        /**
         * Gets ith "while" element
         */
        org.griphyn.vdl.model.While getWhileArray(int i);
        
        /**
         * Returns number of "while" element
         */
        int sizeOfWhileArray();
        
        /**
         * Sets array of all "while" element
         */
        void setWhileArray(org.griphyn.vdl.model.While[] xwhileArray);
        
        /**
         * Sets ith "while" element
         */
        void setWhileArray(int i, org.griphyn.vdl.model.While xwhile);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "while" element
         */
        org.griphyn.vdl.model.While insertNewWhile(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "while" element
         */
        org.griphyn.vdl.model.While addNewWhile();
        
        /**
         * Removes the ith "while" element
         */
        void removeWhile(int i);
        
        /**
         * Gets array of all "repeat" elements
         */
        org.griphyn.vdl.model.Repeat[] getRepeatArray();
        
        /**
         * Gets ith "repeat" element
         */
        org.griphyn.vdl.model.Repeat getRepeatArray(int i);
        
        /**
         * Returns number of "repeat" element
         */
        int sizeOfRepeatArray();
        
        /**
         * Sets array of all "repeat" element
         */
        void setRepeatArray(org.griphyn.vdl.model.Repeat[] repeatArray);
        
        /**
         * Sets ith "repeat" element
         */
        void setRepeatArray(int i, org.griphyn.vdl.model.Repeat repeat);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "repeat" element
         */
        org.griphyn.vdl.model.Repeat insertNewRepeat(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "repeat" element
         */
        org.griphyn.vdl.model.Repeat addNewRepeat();
        
        /**
         * Removes the ith "repeat" element
         */
        void removeRepeat(int i);
        
        /**
         * Gets array of all "switch" elements
         */
        org.griphyn.vdl.model.Switch[] getSwitchArray();
        
        /**
         * Gets ith "switch" element
         */
        org.griphyn.vdl.model.Switch getSwitchArray(int i);
        
        /**
         * Returns number of "switch" element
         */
        int sizeOfSwitchArray();
        
        /**
         * Sets array of all "switch" element
         */
        void setSwitchArray(org.griphyn.vdl.model.Switch[] xswitchArray);
        
        /**
         * Sets ith "switch" element
         */
        void setSwitchArray(int i, org.griphyn.vdl.model.Switch xswitch);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "switch" element
         */
        org.griphyn.vdl.model.Switch insertNewSwitch(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "switch" element
         */
        org.griphyn.vdl.model.Switch addNewSwitch();
        
        /**
         * Removes the ith "switch" element
         */
        void removeSwitch(int i);
        
        /**
         * Gets array of all "continue" elements
         */
        org.apache.xmlbeans.XmlObject[] getContinueArray();
        
        /**
         * Gets ith "continue" element
         */
        org.apache.xmlbeans.XmlObject getContinueArray(int i);
        
        /**
         * Returns number of "continue" element
         */
        int sizeOfContinueArray();
        
        /**
         * Sets array of all "continue" element
         */
        void setContinueArray(org.apache.xmlbeans.XmlObject[] xcontinueArray);
        
        /**
         * Sets ith "continue" element
         */
        void setContinueArray(int i, org.apache.xmlbeans.XmlObject xcontinue);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "continue" element
         */
        org.apache.xmlbeans.XmlObject insertNewContinue(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "continue" element
         */
        org.apache.xmlbeans.XmlObject addNewContinue();
        
        /**
         * Removes the ith "continue" element
         */
        void removeContinue(int i);
        
        /**
         * Gets array of all "break" elements
         */
        org.apache.xmlbeans.XmlObject[] getBreakArray();
        
        /**
         * Gets ith "break" element
         */
        org.apache.xmlbeans.XmlObject getBreakArray(int i);
        
        /**
         * Returns number of "break" element
         */
        int sizeOfBreakArray();
        
        /**
         * Sets array of all "break" element
         */
        void setBreakArray(org.apache.xmlbeans.XmlObject[] xbreakArray);
        
        /**
         * Sets ith "break" element
         */
        void setBreakArray(int i, org.apache.xmlbeans.XmlObject xbreak);
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "break" element
         */
        org.apache.xmlbeans.XmlObject insertNewBreak(int i);
        
        /**
         * Appends and returns a new empty value (as xml) as the last "break" element
         */
        org.apache.xmlbeans.XmlObject addNewBreak();
        
        /**
         * Removes the ith "break" element
         */
        void removeBreak(int i);
        
        /**
         * Gets the "targetNamespace" attribute
         */
        java.lang.String getTargetNamespace();
        
        /**
         * Gets (as xml) the "targetNamespace" attribute
         */
        org.apache.xmlbeans.XmlAnyURI xgetTargetNamespace();
        
        /**
         * True if has "targetNamespace" attribute
         */
        boolean isSetTargetNamespace();
        
        /**
         * Sets the "targetNamespace" attribute
         */
        void setTargetNamespace(java.lang.String targetNamespace);
        
        /**
         * Sets (as xml) the "targetNamespace" attribute
         */
        void xsetTargetNamespace(org.apache.xmlbeans.XmlAnyURI targetNamespace);
        
        /**
         * Unsets the "targetNamespace" attribute
         */
        void unsetTargetNamespace();
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static org.griphyn.vdl.model.ProgramDocument.Program newInstance() {
              return (org.griphyn.vdl.model.ProgramDocument.Program) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static org.griphyn.vdl.model.ProgramDocument.Program newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (org.griphyn.vdl.model.ProgramDocument.Program) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.griphyn.vdl.model.ProgramDocument newInstance() {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.griphyn.vdl.model.ProgramDocument newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.griphyn.vdl.model.ProgramDocument parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.griphyn.vdl.model.ProgramDocument parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.griphyn.vdl.model.ProgramDocument parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.griphyn.vdl.model.ProgramDocument parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.griphyn.vdl.model.ProgramDocument parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.griphyn.vdl.model.ProgramDocument parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.griphyn.vdl.model.ProgramDocument parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.griphyn.vdl.model.ProgramDocument parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.griphyn.vdl.model.ProgramDocument parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.griphyn.vdl.model.ProgramDocument parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.griphyn.vdl.model.ProgramDocument parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.griphyn.vdl.model.ProgramDocument parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.griphyn.vdl.model.ProgramDocument parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.griphyn.vdl.model.ProgramDocument parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.griphyn.vdl.model.ProgramDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.griphyn.vdl.model.ProgramDocument parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.griphyn.vdl.model.ProgramDocument) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
