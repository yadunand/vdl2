
module load swift

export SBATCH_RESERVATION=swift

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo Adding $DIR/bin to PATH:

PATH=$DIR/bin:$PATH

echo $PATH

mkdir -p $HOME/.swift

if [ -f $HOME/.swift/swift.properties ]; then
  echo Adding properties to end of $HOME/.swift/swift.properties
else
  echo creating $HOME/.swift/swift.properties
fi

cat >>$HOME/.swift/swift.properties <<END

# Properties for Swift Tutorial 

status.mode=provider
use.provider.staging=false
use.wrapper.staging=false
wrapperlog.always.transfer=true
execution.retries=0
lazy.errors=false
provider.staging.pin.swiftfiles=false
sitedir.keep=true
file.gc.enabled=false
tcp.port.range=50000,51000
END
