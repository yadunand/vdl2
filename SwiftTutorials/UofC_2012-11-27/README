
INTRODUCTION TO SWIFT

SETUP

Obtain the tutorial package

Change directory into the created directory called "tutorial"

This directory contains:

bin:     script tools for the tutorial
scripts: Swift scripts for the tutorial

An output directory will be created by the Swift scripts


Use bin/setup.sh to add tutorial programs to your PATH

Run:

source bin/setup.sh

EXERCISE 1 - Run a simple command under Swift

This script simply runs the getinfo command found in bin/getinfo.sh

You can run this on the login node by typing:

bin/getinfo.sh

Inspect getinfo.sh to see what it does.

The Swift syntax in scripts/script01.swift directs the output of
getinfo.sh to file output/info.txt

Run the Swift script by typing:

runswift scripts/script01.swift

Check that the output is created in the output directory

EXERCISE 2 - Run parallel jobs

The Swift syntax in scripts/script02.swift runs the getinfo
script 25 times in parallel.  Each job produces an output
file with a different name.

runswift scripts/script02.swift

Check that the output is created successfully

EXERCISE 3 - Run parallel jobs with pipelines

The Swift syntax in scripts/script02.swift runs the getinfo
script 25 times in parallel.  Each job produces an output
file with a different name.  Each output file is then read by the
filterinfo task, which produces output in _concurrent

Inspect bin/filterinfo.sh to see what it does to the output of getinfo.sh

runswift scripts/script03.swift

EXERCISE 4 - Name files in pipelines

In Exercise 3, the final outputs were named by Swift.
Exercise 4 is similar to 3 but allows you to name the final
outputs.  Edit script04.swift to name the files in an appropriate
way.

EXERCISE 5 - Reduction

Exercise 5 allows you to collect all of the filtered outputs
and summarize them into one file.  This Swift script allows
suminfo.sh to read all the files and collect a statistic.

Inspect bin/suminfo.sh to see what it will do to the files it reads.

CLEAN UP

Simply run: bin/cleanup and enter y to confirm
