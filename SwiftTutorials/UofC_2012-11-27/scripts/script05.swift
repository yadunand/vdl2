type file;

app (file o) getinfo ()
{
  getinfo stdout=@o;
}

app (file o) filter (file i)
{
  filterinfo @i @o;
}

app (file o) reduce (file i[])
{
  suminfo @filenames(i) stdout=@o;
}

file filtered[];

foreach i in [0:24] {
  string fname = @strcat("output/info.",i,".txt");
  file info <single_file_mapper; file=fname>;
  info = getinfo();
  filtered[i] = filter(info);
}

file sum<"output/sum.data">;

sum = reduce(filtered);

