# Load modules
module unload swift
module load swift/0.95-RC1
echo Swift version is $(swift -version)

# Add applications to $PATH
TUTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PATH=$TUTDIR/bin:$TUTDIR/app:$PATH
