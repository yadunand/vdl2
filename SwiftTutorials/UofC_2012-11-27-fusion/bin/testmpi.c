#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <string.h>

int main (argc, argv)
     int argc;
     char *argv[];
{
  int rank, size;

  MPI_Init (&argc, &argv);			/* starts MPI */
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);	/* get current process id */
  MPI_Comm_size (MPI_COMM_WORLD, &size);	/* get number of processes */

  FILE *fp, *command;
  char filename[100];
  char hostname[1024];

  sprintf(filename, "output.%d.txt", rank);
  printf("Trying to write output to %s\n", filename);
  fp = fopen(filename, "w");

  if (fp == NULL) {
     printf("Unable to write file\n");
     exit(0);
  } 

  command = popen("/bin/hostname -f", "r");
  if(command == NULL) {
     printf("Failed to run command\n");
     exit(1);
  }

  while (fgets(hostname, sizeof(hostname)-1, command) != NULL) {
    hostname[(strlen(hostname)-1)] = '\0';
    fprintf(fp, "Hostname: %s, rank: %d\n", hostname, rank);
  }

  fclose(fp);
  printf("Done writing output file %s\n", filename);

  MPI_Finalize();

  // Create a tar file of results (only once)
  if(rank == 0) {
     system("mkdir output");
     system("tar cvf output/results.tar output*.txt");
  }
  return 0;
}
