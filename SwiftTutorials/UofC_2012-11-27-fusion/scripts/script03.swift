type file;

app (file o) getinfo ()
{
  getinfo stdout=@o;
}

app (file o) filterinfo (file i)
{
  filterinfo @i @o;
}

foreach i in [0:24] {
  string fname = @strcat("output/info.",i,".txt");
  file info <single_file_mapper; file=fname>;
  info = getinfo();
  file filtered = filterinfo(info);
}

