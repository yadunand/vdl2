# @dispatch=WORKER
# (int v) leaf_main(string arg[], string env[]) "leaf_main" "0.0" "leaf_main";

sed -e '/mainapp/i@dispatch=WORKER' -e '/mainapp/s/^.*mainapp \(.*\);/(int v) \1_main(string arg[]) "\1_main" "0.0" "\1_main_wrap";/'
