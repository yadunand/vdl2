#!/bin/bash -eu

rm -fv *.o *.so *.a mockdock
rm -fv *_wrap*
rm -fv static_leaf_main{,.c} extension.c
rm -fv make-package.tcl pkgIndex.tcl
rm -fv user-code.{swift,tcl}
rm -fv user-leaf.{c,h,tcl}
rm -fv run-*.out
