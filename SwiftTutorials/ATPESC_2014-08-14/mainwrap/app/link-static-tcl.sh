#!/bin/bash -eu

bgxlc_r -c -I . \
  -I /home/wozniak/Public/sfw/ppc64/bgxlc/static_r/tcl-8.5.12/include \
  static_leaf_main_tcl.c


bgxlc_r -o static_leaf_main_tcl static_leaf_main_tcl.o leaf_main.a  \
   -L /home/wozniak/Public/sfw/ppc64/bgxlc/static_r/tcl-8.5.12/lib \
   -ltcl8.5
