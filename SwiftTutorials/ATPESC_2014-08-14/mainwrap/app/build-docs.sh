#!/bin/bash

# Build asciidoc html from README.txt

asciidoc -a toc -a toclevels=2  -a max-width=750px  -a textwidth=80 -o README.html README.txt
