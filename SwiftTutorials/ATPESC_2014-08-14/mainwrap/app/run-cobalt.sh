#!/bin/bash -eu

show_machine_files()
{
  ( cd $GENLEAF_HOME/settings
    ls machine.*.sh | sed 's/machine\.\(.*\).sh/ \t * \1/' )
}

usage()
{
  echo "run-cobalt.sh: usage:"
  echo
  printf "\t\t run-cobalt.sh <MACHINE> <PROCS>\n"
  echo
  printf "\t where: \n"
  printf "\t\t MACHINE     is the machine type\n"
  printf "\t\t PROCS       is the number of MPI processes\n"
  echo
  echo "known machine types:"
  show_machine_files
}

crash()
{
  MSG=$*
  echo "crash!"
  echo $MSG
  exit 1
}

TURBINE_RUN=$( which turbine-cobalt-run.zsh ; true ) 
if [[ $TURBINE_RUN == "" ]]
then
  echo "Could not find turbine-cobalt-run.zsh in PATH!"
  exit 1
fi

TURBINE_HOME=$( cd $( dirname $TURBINE_RUN )/../../.. ; /bin/pwd )
GENLEAF_HOME=$TURBINE_HOME/scripts/main-wrap

echo "TURBINE_HOME=$TURBINE_HOME"

if [[ ${#*} != 2 ]]
then
  usage
  exit 1
fi

MACHINE=$1
numprocs=$2

# Assert that numprocs is a number: 
if ! (( $numprocs + 1 )) ; then 
  echo "PROCS must be a number: received: $numprocs"
  exit 1
fi 

USER_SWIFT=user-code.swift

MACHINE_FILE=$TURBINE_HOME/scripts/main-wrap/settings/machine.$MACHINE.sh
[[ -f $MACHINE_FILE ]] || crash "unknown machine type: $MACHINE"
source $MACHINE_FILE

USER_TCL=user-code.tcl
[[ -f $USER_TCL ]] || crash "could not find compiled Swift runtime code: $USER_TCL"

rm -rf work
mkdir work

export TURBINE_OUTPUT=$PWD/work

ln -s $PWD/peptides $TURBINE_OUTPUT
ln -s $PWD/prot.txt $TURBINE_OUTPUT

ENV="--env ADLB_DEBUG=0:ADLB_TRACE=0"

# $TURBINE_RUN -n $numprocs -s settings.bgq.sh $USER_TCL 
export PROJECT=ExM
set -x 
qsub -n $numprocs -t 5 $ENV $PWD/static_leaf_main
