
/**
   The mockdock application code
   May be run as
   1) stand-alone executable via Makefile
   2) as Swift library via genleaf
*/

#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "mockdock.h"

static void usage(void){
  printf("usage: mockdock <protfile> <peptfile> <delay>\n");
}

int main(int argc, char** argv){

    if (argc != 4){
      usage();
      exit(EXIT_FAILURE);
    }
    char* protfile = argv[1];
    char* peptfile = argv[2];
    int   runtime  = atoi(argv[3]);

    int fd1 = open(protfile, O_RDONLY);
    if (fd1 < 0){
      printf("could not open: %s\n", protfile);
      exit(1);
    }

    int fd2 = open(peptfile, O_RDONLY);
    if (fd2 < 0){
      printf("could not open: %s\n", peptfile);
      exit(1);
    }
    sleep(runtime);
    printf("result number: %d\n", dock(fd1, fd2));

    close(fd1);
    close(fd2);

    return 0;
}

size_t maxsize=10*1024*1024;

int dock(int fd1, int fd2){

  char* buf = malloc(maxsize);

  int len1 = read(fd1, buf, maxsize);
  assert(len1 >= 0); 

  int len2 = read(fd2, buf, maxsize);
  assert(len2 >= 0); 

  return(len1 * 1000000 + len2);
}
