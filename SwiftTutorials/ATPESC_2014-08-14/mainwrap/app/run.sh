#!/bin/bash -eu

# Run the user program locally using normal Turbine
# Creates run-*.out containing a copy of the output

export TURBINE_USER_LIB=$PWD

TIME=$( date +%H:%M:%S )
OUTPUT=run-$TIME.out

turbine -l -n 4 user-code.tcl |& tee $OUTPUT
