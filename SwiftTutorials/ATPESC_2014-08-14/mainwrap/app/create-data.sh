#!/bin/bash -eu

# Creates input data for mockdock

usage() {
  echo "gendata: usage:"
  echo
  printf "\t\t gendata <NUMPEPT> <NUMENT>"
  echo
  printf "\t where: \n"
  printf "\t\t NUMPEPT     is the number of peptides in each entry\n"
  printf "\t\t NUMENT      is the number of entries\n"
}

if [[ ${#*} != 2 ]] 
then
  usage 
  exit 1
fi

numpept=$1
nument=$2

# Clean up
rm -rf peptides

mkdir -p peptides 
pushd peptides 2>&1 > /dev/null

for j in $(seq 1 $nument) ; do
  echo $RANDOM
done > master.txt

for i in $(seq 1 $numpept) ; do
  #symlink to master
  ln -s master.txt pept${i}.txt
done

popd 2>&1 > /dev/null

echo $RANDOM > prot.txt

#Create dummy prot files for nested loop
mkdir -p prots
pushd prots 2>&1 > /dev/null
for k in $(seq 1 20) ; do
    echo $RANDOM > prot${k}.txt
done
popd 2>&1 > /dev/null
