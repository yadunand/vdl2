
PATH=/home/yadunand/bin/swift-0.95-main/bin:/home/yadunand/bin/jdk1.7.0_25/bin:$PATH

echo Swift version is $(swift -version)

# Add applications to $PATH

TUTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PATH=$TUTDIR/bin:$TUTDIR/app:$PATH

( cd src/md; make; make install )

( cd src/c-ray; make; make install )

