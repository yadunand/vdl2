#! /bin/sh

SSHSOCKET=~/.ssh/$USER@cetus.alcf.anl.gov
ssh -M  -N -o ControlPath=$SSHSOCKET $USER@cetus.alcf.anl.gov

# ssh -M -f -N -o ControlPath=$SSHSOCKET $USER@cetus.alcf.anl.gov

exit

The options have the following meaning:

-M instructs SSH to become the master, i.e. to create a master socket that will be used by the slave connections
-f makes SSH to go into the background after the authentication
-N tells SSH not to execute any command or to expect an input from the user; that’s good because we want it only to manage and keep open the master connection and nothing else
-o ControlPath=$SSHSOCKET – this defines the name to be used for the socket that represents the master connection; the slaves will use the same value to connect via it
Thanks to -N and -f the SSH master connection will get out of the way but will stay open and such usable by subsequent ssh/scp invocations. This is exactly what we need in a shell script. If you just do something manually than you can leave out -N and -f and use directly this connection for whatever you need while you can also open a slave connection in another terminal window. Just don’t forget that once the master connection exits slaves won’t work.

2. Open and close other connections without re-authenticating as you like

Now you can do as many ssh/scp operations as you like and you won’t be prompted for a password. You only always have to provide the command with the same ControlPath, which we ensure by having stored it into the variable SSHSOCKET:

ssh -o ControlPath=$SSHSOCKET myUsername@targetServerName "echo 'Hello from the remote server!'; ls"
...
scp -o ControlPath=$SSHSOCKET myUsername@targetServerName:remoteFile.txt ./