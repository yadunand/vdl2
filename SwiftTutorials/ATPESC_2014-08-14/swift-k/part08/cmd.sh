#! /bin/sh

SSHSOCKET=~/.ssh/$USER@cetus.alcf.anl.gov

ssh -o ControlPath=$SSHSOCKET $USER@cetus.alcf.anl.gov $*

# scp -o ControlPath=$SSHSOCKET myUsername@targetServerName:remoteFile.txt ./