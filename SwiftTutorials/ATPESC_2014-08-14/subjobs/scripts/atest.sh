#! /bin/bash

BLOCKS="VST-20040-31371-128
VST-22040-33371-128
VST-02040-13371-128
VST-00040-11371-128"

ba=($BLOCKS)

for i in ${!ba[*]}; do
  echo ba[$i]=${ba[$i]}
done
