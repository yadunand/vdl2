#!/bin/bash

set -x

genblocks()
{
  # PARTITION=VST-22060-33171-32
  PARTITION=$1
  
  corner=$(echo $PARTITION | sed -e 's/VST-//' -e 's/-.*//')
  
  echo 'obase=2; for(i=0;i<32;i++) {print i; print "\n"}' | bc |
  for corner_coord in $(awk -v corner=$corner '{printf("%05d\n", $1+corner)}' ); do
    corner_node=$(/soft/cobalt/bgq_hardware_mapper/coord2hardware.py $corner_coord)
    echo $corner_node
  done
}

export corners=$(genblocks $COBALT_PARTNAME)

BLOCK=$COBALT_PARTNAME

for corner in $corners; do
  echo "$0: BLOCK=$BLOCK corner=$corner"
  echo "DOING: runjob  --block $BLOCK --corner $corner --shape 1x1x1x1x1 : `pwd`/mpicatnap in.data out.data.$corner 3"
  runjob  --block $BLOCK --corner $corner --shape 1x1x1x1x1 --ranks-per-node 4 : `pwd`/mpicatnap in.data out.data.$corner 3 &
done

wait

exit 0

nsub=${#BLOCK_ARRAY[@]}
for ((b=0;b<nb;b++))
do
  echo script3.sh: BLOCK $b: ${ba[$SWIFT_JOB_SLOT]}
done


export PATH=/home/ketan/swift-0.95-RC6/bin:$PATH
#cd /home/wilde/swift/lab

swift -config cf -tc.file apps -sites.file localcoast.xml catsnsleepmpi.swift -n=100 -s=2

