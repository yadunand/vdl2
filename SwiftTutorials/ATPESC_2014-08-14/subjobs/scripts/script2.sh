#!/bin/bash

export BLOCKS=$(get-bootable-blocks --size 32 $COBALT_PARTNAME)

export BLOCK_ARRAY=($BLOCKS)

for BLOCK in $BLOCKS
do
  boot-block --block $BLOCK &
done
wait

#for BLOCK in $BLOCKS
nb=${#BLOCK_ARRAY[@]}
for ((b=0;b<nb;b++))
do
  export SWIFT_JOB_SLOT=$b
  $(pwd)/runjob.sh $(pwd)/mpicatnap in.data out.data 3 &
done
wait

for BLOCK in $BLOCKS
do
   boot-block --block $BLOCK --free &
done
wait
