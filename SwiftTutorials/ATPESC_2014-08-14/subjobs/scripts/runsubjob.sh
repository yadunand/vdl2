#! /bin/bash

BLOCK=$COBALT_PARTNAME
SWIFT_SUBBLOCK_ARRAY=($SWIFT_SUBBLOCKS)
SUBBLOCK=${SWIFT_SUBBLOCK_ARRAY[$SWIFT_JOB_SLOT]}

echo $0: running BLOCK=$BLOCK SLOT=$SWIFT_JOB_SLOT SUBBLOCK=$SUBBLOCK
echo $0: running cmd: $0 args: $*

echo $0: running runjob  --block $BLOCK --corner $SUBBLOCK --shape 1x1x1x1x1 : $*

sleep 1
runjob --strace 0 --block $BLOCK --corner $SUBBLOCK --shape 1x1x1x1x1 --ranks-per-node 1 : $*
sleep 1

exit 0
