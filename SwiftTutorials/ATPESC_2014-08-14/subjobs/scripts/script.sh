#!/bin/bash

BLOCKS=`get-bootable-blocks --size 128 $COBALT_PARTNAME`

echo BLOCKS="$BLOCKS"

for BLOCK in $BLOCKS
do
  boot-block --block $BLOCK &
done
wait

for BLOCK in $BLOCKS
do
  runjob --block $BLOCK : `pwd`/mpicatnap in.data out.data 3 &
done
wait

for BLOCK in $BLOCKS
do
   boot-block --block $BLOCK --free &
done
wait

