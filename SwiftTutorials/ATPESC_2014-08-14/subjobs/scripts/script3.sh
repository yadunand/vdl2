#!/bin/bash

set -x

export BLOCKS=$(get-bootable-blocks --size 32 $COBALT_PARTNAME)

echo script3.sh: BLOCKS=$BLOCKS

if [ "_${BLOCKS}" = "_" ]; then
  echo ERROR: script3.sh: BLOCKS is null.
  exit 1
fi

export BLOCK_ARRAY=($BLOCKS)

for BLOCK in $BLOCKS
do
  boot-block --block $BLOCK &
done
wait

# #for BLOCK in $BLOCKS
# nb=${#BLOCK_ARRAY[@]}
# for ((b=0;b<nb;b++))
# do
#   export SWIFT_JOB_SLOT=$b
#   /home/wilde/swift/lab/runjob.sh   `pwd`/mpicatnap in.data out.data 3 &
# done
# wait

nb=${#BLOCK_ARRAY[@]}
for ((b=0;b<nb;b++))
do
  echo script3.sh: BLOCK $b: ${ba[$SWIFT_JOB_SLOT]}
done

export PATH=/home/wilde/swift/rev/swift-0.94.1/bin:$PATH
cd /home/wilde/swift/lab

swift -config cf -tc.file apps -sites.file localcoast.xml catsnsleepmpi.swift -n=100 -s=2

for BLOCK in $BLOCKS
do
   boot-block --block $BLOCK --free &
done
wait
