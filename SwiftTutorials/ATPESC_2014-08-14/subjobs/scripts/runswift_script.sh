#!/bin/bash

genblocks()
{
  # PARTITION=VST-22060-33171-32
  PARTITION=$1
  
  corner=$(echo $PARTITION | sed -e 's/VST-//' -e 's/-.*//')
  
  echo 'obase=2; for(i=0;i<32;i++) {print i; print "\n"}' | bc |
  for corner_coord in $(awk -v corner=$corner '{printf("%05d\n", $1+corner)}' ); do
    corner_node=$(/soft/cobalt/bgq_hardware_mapper/coord2hardware.py $corner_coord)
    echo $corner_node
  done
}

export SWIFT_SUBBLOCKS=$(genblocks $COBALT_PARTNAME)

echo $0: SWIFT_SUBBLOCKS=$SWIFT_SUBBLOCKS

if [ "_$SWIFT_SUBBLOCKS" = _ ]; then
  echo ERROR: $0: SWIFT_SUBBLOCKS is null.
  exit 1
fi

export SWIFT_SUBBLOCK_ARRAY=($SWIFT_SUBBLOCKS)

nsb=${#SWIFT_SUBBLOCK_ARRAY[@]}
for ((sb=0;sb<nsb;sb++))
do
  echo $0: SUBBLOCK $sb: ${SWIFT_SUBBLOCK_ARRAY[$sb]}
done

export PATH=/home/ketan/swift-0.95-RC6/bin:$PATH
#cd /home/wilde/swift/lab   # FIXME

swift $* 

# swift -config cf -tc.file apps -sites.file localcoast.xml catsnsleepmpi.swift -n=100 -s=2
