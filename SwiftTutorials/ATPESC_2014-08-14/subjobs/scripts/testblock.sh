#!/bin/bash

set -x

genblocks()
{
  # PARTITION=VST-22060-33171-32
  PARTITION=$1
  
  corner=$(echo $PARTITION | sed -e 's/VST-//' -e 's/-.*//')
  
  echo 'obase=2; for(i=0;i<32;i++) {print i; print "\n"}' | bc |
  for corner_coord in $(awk -v corner=$corner '{printf("%05d\n", $1+corner)}' ); do
    corner_node=$(/soft/cobalt/bgq_hardware_mapper/coord2hardware.py $corner_coord)
    echo $corner_node
  done
}

export SWIFT_SUBBLOCKS=$(genblocks $COBALT_PARTNAME | head -8)

BLOCK=$COBALT_PARTNAME

for ((iter=0;iter<4;iter++)); do

echo $0: starting iter $iter
echo 

for corner in $SWIFT_SUBBLOCKS; do
  echo $0: BLOCK=$BLOCK corner=$corner
  echo DOING: runjob  --block $BLOCK --corner $corner --shape 1x1x1x1x1 : `pwd`/mpicatnap in.data out.data.$corner 3
  sleep 1
  runjob  --block $BLOCK --corner $corner --shape 1x1x1x1x1 --ranks-per-node 1 : `pwd`/mpicatnap in.data out.data.$corner 0 &
  sleep 1
done

wait

done

exit 0
