qsub --env BG_SHAREDMEMSIZE=32MB -t 20 -n 2 --proccount 2  \
  --mode c1 \
  /projects/ExM/ketan/openmp-gnu-july16-mini/bin/FlexPepDocking.staticmpi.linuxxlcdebug \
  -database /home/ketan/minirosetta_database/ \
  -pep_refine \
  -s /home/ketan/hlac-97-D/hlac-97-D-AAADAAAAL_complex_0001.pdb \
  -ex1 \
  -ex2aro \
  -use_input_sc \
  -nstruct 1 -overwrite \
  -scorefile AAADAAAAL_complex_91R.sc

