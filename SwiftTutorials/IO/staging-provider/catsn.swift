type file;

app (file o) cat (file i)
{
  cat @i stdout=@o;
}

file input_files[]<filesys_mapper; location="../data", pattern="tmp.*">;

foreach j,i in input_files {
  file output<single_file_mapper; file=@strcat("data/", i, ".out")>;
  output = cat(j);
}
