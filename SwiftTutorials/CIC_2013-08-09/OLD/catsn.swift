type file;

// app declaration
app (file o) cat (file i){
  cat @i stdout=@o;
}

// input data
file data<"data.txt">;

/*  
*  output data mapped to files with 
*  prefix f and suffix .out in the outdir
*  directory
*/

file out[]<simple_mapper; location="outdir", prefix="f.",suffix=".out">;

/*
* @arg means n could be specified as commandline argument
* foreach loop executes 1 to n calls of cat in parallel
*/
foreach j in [1:@toInt(@arg("n","1"))] {
  out[j] = cat(data);
}

