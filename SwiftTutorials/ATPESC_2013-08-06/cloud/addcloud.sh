#!/bin/bash

#Add cloud resource pool to a sites file.
#Usage: ./addcloud.sh <dirname>
#Will add the cloud resource pool to the sites.xml file in <dirname> provided as an arg

EXPECTED_ARGS=1

if [ $# -ne $EXPECTED_ARGS ]
then
    echo "Expecting 1 arg : Got $*"
    echo "usage $0 <targetdirectory>"
    exit 1
fi

sed -e 's/<config>//' -e 's/<\/config>//' sites.xml > poolentry.xml

cloudpool=$(cat poolentry.xml)

#escape some characters and add pool entry to sites file
sed "/<\/config>/i\
$(echo $cloudpool | sed -e 's/\\/\\\\/g' -e 's/\//\\\//g')" $1/sites.xml > $1/both.xml

echo "Done."
echo "To run with the new setup, cd into $1 and use '-sites.file both.xml' option with Swift commandline"
echo "eg. swift <p<xx>.swift> -sites.file both.xml"

