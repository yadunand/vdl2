
global const string numpy = "from numpy import *\n\n";

typedef matrix string;

(matrix A) eye(int n)
{
  string command = sprintf("repr(eye(%i))", n);
  string code = numpy+command;
  matrix t = python(code);
  A = replace_all(t, "\n", "", 0);
}

(matrix A) ones(int n)
{
  string command = sprintf("repr(ones(%i))", n);
  string code = numpy+command;
  matrix t = python(code);
  A = replace_all(t, "\n", "", 0);
}

(matrix M) matrix_scale(matrix A, float s)
{
  string command = sprintf("repr(%s*%s)", A, s);
  string code = numpy+command;
  matrix t = python(code);
  M = replace_all(t, "\n", "", 0);
}

(matrix M) matrix_set(matrix A, int row, int column, float s)
{
  string fragment =
    """
A = %s
A[%i,%i] = %f
repr(A)
""";
  string command = sprintf(fragment, A, row, column, s);
  string code = numpy+command;
  matrix t = python(code);
  M = replace_all(t, "\n", "", 0);
}

(matrix M) matrix_add(matrix A1, matrix A2)
{
  string command = sprintf("repr(%s+%s)", A1, A2);
  string code = numpy+command;
  matrix t = python(code);
  M = replace_all(t, "\n", "", 0);
}

(float d) determinant(matrix A)
{
  string command = sprintf("repr(linalg.det(%s))", A);
  string code = numpy+command;
  string t = python(code);
  d = tofloat(t);
}
