#!/bin/bash -e

ATPESC=/projects/ATPESC2013/workflow

if ! cd part11-swift-py-r/code
then
  echo Run this from the top-level ATPESC_2013-08-06 directory
  exit 1
fi

stc dets.swift

which turbine-cobalt-run.zsh
turbine-cobalt-run.zsh -s $ATPESC/vesta.cfg -n 10 -t 5 \
  -e LD_LIBRARY_PATH=$ATPESC/R-3.0.1/lib64/R/lib \
  -e R_HOME=$ATPESC/R-3.0.1/lib64/R \
  dets.tcl

