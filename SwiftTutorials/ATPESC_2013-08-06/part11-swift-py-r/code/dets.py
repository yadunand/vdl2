
from numpy import *

N = 3
U = 7

dets = zeros(U-1)
for i in range(1,U):
    A = ones(N)
    A = A + i*eye(N)
    A[2,0] = (U-i+1)**3
    print(A)
    d = linalg.det(A)
    print(d)
    dets[i-1] = abs(d)
    print

print "dets:", dets
print "max:", max(dets)

