
import io;
import math;
import python;
import string;
import R;

import numpy;

global const int N = 3;
global const int U = 7;

main
{
  // Define our collection of determinants:
  float dets[];
  foreach i in [1:U-1]
  {
    // For U, i, construct a matrix via Numpy:
    A = matrix_add(matrix_scale(eye(N), itof(i)), ones(N));
    B = matrix_set(A, 2, 0, (U-i+1)**3);
    // Obtain its determinant via Numpy:
    v = determinant(B);
    // Store the determinant in a Swift array:
    dets[i] = abs_float(v);
    printf("dets[%i]=%.2f", i, v);
  }

  // Build a fragment of R code with the dets:
  code = sprintf("max(%s)", string_from_floats(dets));
  r = R(code);
  printf("dets: max: %f", r);
}
