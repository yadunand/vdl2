#!/bin/bash -e

pushd part11-swift-py-r/code >& /dev/null
# Strip comments, blank lines; prepend shell prompt ($)
grep -A 20 stc run-dets.sh | \
  grep -v -e "^$\|#" | \
  sed 's/^/$ /' > run-dets.sh.txt
popd >& /dev/null

~davidk/asciidoc-8.6.4/asciidoc -a toc -a toplevels=2 -a stylesheet=~davidk/swift-0.94/cog/modules/swift/docs/stylesheets/asciidoc.css -a max-width=800px -o tutorial.html README

