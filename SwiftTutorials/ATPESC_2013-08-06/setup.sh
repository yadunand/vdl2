export SWIFT_HEAP_MAX=4G

# Add Swift and java to PATH
export PATH=/projects/ATPESC2013/workflow/jdk1.7.0_25/bin:/projects/ATPESC2013/workflow/swift-0.94.1/bin:$PATH

echo -e "\\nUsing swift release from: \\c"
which swift
swift -version

# Cleanup swift.log
if [ -f "swift.log" ]; then
   rm swift.log > /dev/null 2>&1
fi

echo -e "Using java from: \\c"
which java
java -version

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo -e "\\nAdding $DIR to PATH: "
PATH=$DIR:$PATH

if [ -e $HOME/.swift ]; then
  savedotswift=$(mktemp -d $HOME/.swift.save.XXXX)
  echo -e "Saving $HOME/.swift in $savedotswift \\n"
  mv $HOME/.swift/* $savedotswift
else
  mkdir -p $HOME/.swift
fi

cat >>$HOME/.swift/swift.properties <<END

# Properties for Swift Tutorial

sites.file=sites.xml
tc.file=apps

wrapperlog.always.transfer=true
sitedir.keep=true
file.gc.enabled=false
status.mode=provider

execution.retries=0
lazy.errors=false

use.wrapper.staging=false
use.provider.staging=true
provider.staging.pin.swiftfiles=true

END

# Swift/T settings:

# Source this to add Swift tools to your PATH
# We put everything in the beginning of the PATH

# STC compiler and Turbine run time
PATH=/projects/ATPESC2013/workflow/stc/bin:${PATH}
PATH=/projects/ATPESC2013/workflow/turbine/scripts/submit/cobalt:${PATH}

# Python and R compiled for login nodes with bgxlc:
PATH=/projects/ATPESC2013/workflow/R-3.0.1/bin:${PATH}
PATH=/projects/ATPESC2013/workflow/Python-2.7.3/bin:${PATH}

