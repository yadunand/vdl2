#!/bin/bash
###########
#start_coaster_workers.sh
#
#This script will start a coaster service on Tukey. It will then create 
#reverse ssh tunnel from Vesta to Tukey and submits a worker on the vesta 
#login node via qsub.
###########

LPORT_F=local_port
SPORT_F=service_port
LOCALSYS=login1.tukey.pub.alcf.anl.gov
REMOTESYS=vestalac1.pub.alcf.anl.gov

# -local is not working properly
# TODO: Check if this is a reproducible bug.
# removing -local fixes the problem
coaster-service -passive -nosec -localportfile $LPORT_F -portfile $SPORT_F > coaster-log 2>&1 &
echo "Waiting for coaster-service to start ...(3 seconds)"
sleep 3
LOCAL_PORT=$(cat $LPORT_F)
SERV_PORT=$(cat $SPORT_F)

 
# Start a reverse tunnel on the remote system connecting the
# the LOCAL_PORT on the LOCALSYS to that on the REMOTESYS.
ssh -f -n -N $REMOTESYS -R $LOCAL_PORT:$LOCALSYS:$LOCAL_PORT

#Start a worker on the remote system
#WORKER_STRING="./worker.pl http://localhost:$LOCAL_PORT 1 . &"

# Submit a worker to the queue on the remote system
# -n 1         : ask for 1 node
# -t 120       : request for a walltime of 2 hours (I do not know the max time)
# -q default   : submit to default queue
# --mode script: specify type of submitted job 
WORKER_STRING="cp /home/wilde/swift/rev/swift-0.94.1/bin/worker.pl ~/ ; \
qsub -n 1 -t 120 -q default --mode script worker.pl http://localhost:$LOCAL_PORT 1 ."
ssh -f $REMOTESYS $WORKER_STRING

# Generate the sites.xml file
cat <<EOF > tmp
<config>
  <pool handle="persistent-coasters">
    <execution provider="coaster-persistent"
               url="http://127.0.0.1:$SERV_PORT"
               jobmanager="local:local"/>
    <profile namespace="globus" key="workerManager">passive</profile>
    <profile namespace="globus" key="jobsPerNode">4</profile>
    <profile key="jobThrottle" namespace="karajan">100</profile>
    <profile namespace="karajan" key="initialScore">10000</profile>
    <filesystem provider="local" url="none" />
    <workdirectory>/home/$USER/swiftwork</workdirectory>
  </pool>
</config>
EOF

if [ -f "sites.xml" ];then
    cp sites.xml sites.xml.bak
fi;
mv tmp sites.xml

echo "Done!"

exit 0;





