#!/bin/bash


LAST_LOG=`ls -t | grep p10.*log  | head -1`
if [ ! -f "$LAST_LOG" ]
then
    echo "No recent logs found"
    exit 0
fi
hosts=`grep "host=" $LAST_LOG | grep "Cpu worker started" | grep -oh "host=.*\ " | sort | uniq`

for host in $hosts
do
  echo $host | grep "uc3-c.*" >/dev/null     && echo "Cycle seeder : $host"
  echo $host | grep "uct2-c.*" >/dev/null    && echo "Cycle seeder : $host"
  echo $host | grep "iut2-c.*" >/dev/null    && echo "Cycle seeder : $host"
  echo $host | grep "uct3-c.*" >/dev/null    && echo "Cycle seeder : $host"
  echo $host | grep "appcloud.*" >/dev/null  && echo "Cycle seeder : $host"
done