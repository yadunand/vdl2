export SWIFT_HEAP_MAX=4G

if which swift >& /dev/null; then
  :
else
  echo "Trying to load swift module:"
  module load swift
  if which swift >& /dev/null; then
    Swift loaded from module
  else
    echo "Cant find a swift to run"
    return
  fi
fi

echo -e "\\nUsing swift release from: \\c"
which swift
swift -version

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo Adding $DIR to PATH:
PATH=$DIR:$PATH

if [ -e $HOME/.swift ]; then
  savedotswift=$(mktemp -d $HOME/.swift.save.XXXX)
  echo Saving $HOME/.swift in $savedotswift
  mv $HOME/.swift/* $savedotswift
else
  mkdir -p $HOME/.swift
fi

cat >>$HOME/.swift/swift.properties <<END

# Properties for Swift Tutorial 

sites.file=sites.xml
tc.file=apps

wrapperlog.always.transfer=true
sitedir.keep=true
file.gc.enabled=false
status.mode=provider

execution.retries=0
lazy.errors=false

use.wrapper.staging=false
use.provider.staging=true
provider.staging.pin.swiftfiles=true

END
