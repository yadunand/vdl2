#! /bin/bash

runtime=${1:-0}
range=${2:-100}
biasfile=${3:-nobias}
scale=${4:-1}
n=${5:-1}

if [ $biasfile = nobias ]; then
  offset=0
else
  read offset <$biasfile
fi

# run for some number of "timesteps"

sleep $runtime

# emit n "simulation results", scaled and biased by the specified argument values

for ((i=0;i<n;i++)); do
  echo $(( ($RANDOM%range)*scale+offset))
done
