
import "lib/types";
import "lib/fMRI";
import "lib/AIR";
import "lib/FSL";
import "lib/ImageMagick";

# Full version of align() function

(Volume alignedVol) align (Volume ref, Volume subject)
{
  file firstwarp = alignlinear(ref, subject,
                     ["-m","15","-t1","55","-t2","55","-x","1","-r","8","-c","0.1","-h","8"]);
  file refinedWarp = alignwarp(ref, subject, firstwarp,
                      ["-m","1","12","-b1",".1",".1",".1","-b2",".1",".1",".1",
                       "-h","10000","-s","1024","2","2","-c",".01"]);
  alignedVol = reslice(refinedWarp,subject);
}

string dataDir = @arg("d","data");

Volume referenceVolume <ext; exec="volmapper",name="data/reference">;
Volume inputVolumes [] <ext; exec="volmapper",prefix=@strcat(dataDir,"/anatomy")>;
Volume outputVolumes[];

# Align all subjects

foreach v, i in inputVolumes {
  Volume aligned <ext; exec="volmapper",name=@strcat("work/aligned",i)>;
  aligned = align(referenceVolume, v);
  outputVolumes[i] = aligned;
}

# Create the "averaged" atlas

Volume atlasVolume <ext; exec="volmapper",name="output/atlas">;
atlasVolume = softmean(outputVolumes);

# Extract x, y, z slices for inspection

file xSlice<"output/atlas-x.png">;
file ySlice<"output/atlas-y.png">;
file zSlice<"output/atlas-z.png">;

# Convert x, y, z planes to viewable files

xSlice = convert(slicer(atlasVolume,"x"));
ySlice = convert(slicer(atlasVolume,"y"));
zSlice = convert(slicer(atlasVolume,"z"));
