#!/bin/sh

AIR5=$HOME/software/AIR5.3.0


clean=y

if [ $clean = y ]; then
  rm *warp atlas* resliced*
fi

warp()
{
time $AIR5/bin/alignlinear reference.img anatomy$1.img initwarp$1.warp -m 15 -t1 55 -t2 55 -x 1 -r 8 -c 0.1 -h 8 
time $AIR5/bin/align_warp reference.img anatomy$1.img warp$1.warp -m 1 12  -b1 .1 .1 .1  -b2 .1 .1 .1  -h 10000 -v -s 1024 2 2 -c .01 -f initwarp$1.warp # -q -s 1024 2 2 -c .01
}

if true; then

warp 1
warp 2
warp 3
warp 4

time  $AIR5/bin/reslice warp1.warp resliced1 -a anatomy1
time  $AIR5/bin/reslice warp2.warp resliced2 -a anatomy2
time  $AIR5/bin/reslice warp3.warp resliced3 -a anatomy3
time  $AIR5/bin/reslice warp4.warp resliced4 -a anatomy4

time  $AIR5/bin/softmean atlas.hdr y null resliced1.img resliced2.img resliced3.img resliced4.img
fi

export FSLOUTPUTTYPE=NIFTI

time $fsl/bin/slicer atlas.hdr -x .5 atlas-x.pgm
time $fsl/bin/slicer atlas.hdr -y .5 atlas-y.pgm
time $fsl/bin/slicer atlas.hdr -z .5 atlas-z.pgm
time convert atlas-x.pgm atlas-x.gif
time convert atlas-y.pgm atlas-y.gif
time convert atlas-z.pgm atlas-z.gif
