
import "lib/types";

app (file warp) alignlinear (file rh, file ri, file sh, file si)
{
  alignlinear @rh @sh @warp "-m" "12" "-q";
}

file refhdr <"data/reference.hdr">;  // Reference volume
file refimg <"data/reference.img">;

file subhdr <"data/anatomy4.hdr">;   // One subject volume
file subimg <"data/anatomy4.img">;

file output <"warp.out">;            // Output alignment

output = alignlinear(refhdr,refimg,subhdr,subimg);
