
fMRI Data Processing demo for OHBM 2013

* Setup

  # Get the scripts from svn

  svn co https://svn.ci.uchicago.edu/svn/vdl2/SwiftTutorials/OHBM_2013-06-16 
  cd OHBM_2013-06-16

  # Set default swift.properties in $HOME/.swift.  Points to "." for apps (tc) and sites.xml

  mkdir -p ~/.swift
  cp ~/.swift/swift.properties ~/.swift/swift.properties.save # Backup yours, if needed

  cp swift.properties    ~/.swift   # for ssh staging      (see
  cp swift.propertes.ps ~/.swift    # for provider staging  below)

  # for scp staging, set your auth.defaults:

  mv auth.defaults.example auth.defaults  # AND EDIT to set your login and ssh key
  cp $HOME/.ssh/auth.defaults $HOME/.ssh/auth.defaults.save # Backup as needed
  cp auth.defaults $HOME/.ssh/auth.defaults

  # Get swift

  module load swift  # or set PATH as below

  # NOTE: CURRENT TESTING SHOULD USE THIS SWIFT: (with the latest provider-staging fixes)

  PATH=/project/wilde/swift/src/0.94/cog/modules/swift/dist/swift-svn/bin:$PATH

  # Run setup: sets env var(s) and ensures java is loaded    # <== DONT FORGET !!!

  source setup.sh

* Generate test data directories (example):

  ./makedata data_100 100   # creates 100 anatomical image volumes in directory data_100

The generated data consists of links to a single file, for ease of setup and demo.


* To run:

  # Latest demo code versions:

  atlas00.swift:  Just run one app, no Volume struct types
  atlas01.swift:  Run a loop with volume struct types
  atlas02.swift:  The fill workflow, with align() as a Swift function (for local execution)
  atlas03.swift:  Full wowrkflow with align.sh, suitable for remote execution

  genatlas.swift: The original workflow, now obsolete.

  In the examples below, use atlas00.swift through atlas02.swift,
  and atlas03.swift for all runs on beagle and cloud.

  # APPROXIMATE *SUGGESTED* DEMO SEQUENCE

    # Show first Swift script:
    
    ./clean.sh
    swift atlas00.swift

    # Show struct data types for fMRI "ANALYZE 7.5" Volumes, @arg, and parallel foreach loop

    ./clean.sh
    ./makedata.sh data_10 10
    swift atlas01.swift -d=data_10

      # While above are running, do this in another window to view its activity:

      watch ps -u $USER -H

    # Show same script running on a cluster

    ./clean.sh
    ./makedata.sh data_50 50
    swift -tc.file apps.midway atlas01.swift -d=data_50

    # Show compound functions, imported libs, and full workflow

    ./clean.sh
    swift atlas02.swift -d=data_2

    ./clean.sh
    swift -tc.file apps.midway atlas02.swift -d=data_50

   # Show full script, optimized for remote exec, running on beagle

   ./clean.sh
   swift -tc.file apps.beagle atlas02.swift -d=data_20

   # Show full script running on Amazon EC2

   ./clean.sh
   # Start cloud resources and services here (document!)
   swift -tc.file apps.amazon atlas02.swift -d=data_20

   # Grand finale: show full script running on multiple resources

  ./clean.sh
   swift -tc.file apps.everywhere atlas02.swift -d=data_20

### Obsolete below this line.... clean up vvvvvv


  # On localhost:

  swift genatlas.swift             # processes data/ directory by default
  swift genatlas.swift -d=data_100 # process data_100/ directory

  # With most parallel work on a midway parition (edit sites.xml to set partition)

  swift -tc.file apps.midway genatlas.swift

  # From midway to beagle using provider staging:

  swift -config swift.properties.ps -tc.file apps.beagle genatlas.swift -d=data_100

  # Choices for -tc.file are:

  apps         # default, runs on localhost
  apps.beagle  # on beagle, 8 nodes
  apps.midway  # on midway westmere, 1 node
  apps.amazon  # on Amazon EC2 - needs start-coaster-service, see below
  apps.beagle+midway # both of these

The output files of the workflow are placed under output/,
intermediate files under work/.

The 3 final png output files (a "slice" of the final atlas) should
look like this:

  http://www.ci.uchicago.edu/~wilde/atlas-x.png
  http://www.ci.uchicago.edu/~wilde/atlas-y.png
  http://www.ci.uchicago.edu/~wilde/atlas-z.png

* Notes:

  The "aligin" initial stage runs 3 AIR tools under as a single shell
  script, to reduce staging between these steps.

  A swift version of this function is included as well

