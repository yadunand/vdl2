type file;

# import fMRIdefs

type Volume {
  file header;
  file image;
};



# import AIR

app (Volume atlas) softmean (Volume v[])
{
  softmean @filename(atlas.header) "y" "null" @filenames(v);
}

app (file pgm) slicer (Volume v, string axis)
{
  slicer @filename(v.header) @strcat("-",axis) .5 @filename(pgm);
}

app (file o) convert (file i)
{
  convert @i @o;
}

app (file warp) alignlinear (Volume ref, Volume subject, string params[])
{
  alignlinear @filename(ref.header) @filename(subject.header) @filename(warp) params;
}

app (file warp) alignwarp (Volume ref, Volume subject, file initWarp, string params[])
{
  align_warp @filename(ref.header) @filename(subject.header) @filename(warp) "-f" @filename(initWarp) params;
}

app (Volume v) reslice (file warp, Volume subject)
{
  reslice @filename(warp) @filename(v.header) "-a" @filename(subject.header);
}



# start of genatlas workflow

file   alignScript<"align.sh">;  /* This script does:
   alignlinear $ref $anat init.warp \
    -m 15 -t1 55 -t2 55 -x 1 -r 8 -c 0.1 -h 8 
  align_warp  $ref $anat refined.warp \
    -m 1 12 -b1 .1 .1 .1  -b2 .1 .1 .1 -f init.warp -h 10000 -s 1024 2 2 -c .01
  reslice refined.warp $resliced -a $anat
  */

app (Volume alignedVol) align_sh (file script, Volume reference, Volume input)
{
  sh @script @filename(reference.image) @filename(input.image) @filename(alignedVol.image);
}

(Volume alignedVol) align (Volume ref, Volume subject)
{
  file firstwarp = alignlinear(ref, subject,
                     ["-m","15","-t1","55","-t2","55","-x","1","-r","8","-c","0.1","-h","8"]);
  file refinedWarp = alignwarp(ref, subject, firstwarp,
                      ["-m","1","12","-b1",".1",".1",".1","-b2",".1",".1",".1",
                       "-h","10000","-s","1024","2","2","-c",".01"]);
  alignedVol = reslice(refinedWarp,subject);
}


# Start code here

string dataDir = @arg("d","data");

Volume referenceVolume<ext; exec="volmapper",name="data/reference">;
Volume inputVolumes []<ext; exec="volmapper",prefix=@strcat(dataDir,"/anatomy")>;
Volume outputVolumes[];

foreach v, i in inputVolumes {
  Volume aligned<ext; exec="volmapper",name=@strcat("work/aligned",i)>;
  // aligned = align_sh(alignScript,referenceVolume, v);
  aligned = align(referenceVolume, v);
  outputVolumes[i] = aligned;
}

Volume atlasVolume<ext; exec="volmapper",name="output/atlas">;
atlasVolume = softmean(outputVolumes);

file xSlice<"output/atlas-x.png">;
file ySlice<"output/atlas-y.png">;
file zSlice<"output/atlas-z.png">;

xSlice = convert(slicer(atlasVolume,"x"));
ySlice = convert(slicer(atlasVolume,"y"));
zSlice = convert(slicer(atlasVolume,"z"));





/******* The following code to be removed

string testNames[] = imageNames(outputVolumes);
foreach n, i in testNames {
  tracef("testNames[%i] = %s\n", i, n);
}

app (Volume atlas) softmeanALT (Volume v[]) # A better version
{
  softmean @filename(atlas.header) "y" "null" imageNames(v); # puts only the .img on command line
}


(string onames[]) imageNames (Volume v[])
{
  string names[] = @filenames(v);
  foreach n, i in names {
    if ( i %% 2 == 0) {
      // tracef("n[%i] = %s\n", i, n);
      onames[i] = n;
    }
  }
}

*******/