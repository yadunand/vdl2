#!/bin/sh

ref=$1      # Input:  reference volume
anat=$2     # Input:  subject volume
resliced=$3 # Ouput:  rrealigned and resliced volume

time $AIR5/bin/alignlinear $ref $anat init.warp \
  -m 15 -t1 55 -t2 55 -x 1 -r 8 -c 0.1 -h 8 
time $AIR5/bin/align_warp  $ref $anat refined.warp \
  -m 1 12 -b1 .1 .1 .1  -b2 .1 .1 .1 -f init.warp -h 10000 -s 1024 2 2 -c .01
time $AIR5/bin/reslice refined.warp $resliced -a $anat
