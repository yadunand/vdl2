#export SBATCH_RESERVATION=osg  # for Midway 

export SWIFT_HEAP_MAX=4G

# SWIFTREL=/home/wilde/swift/src/0.94/cog/modules/swift/dist/rc5.1/bin

if echo $PATH | sed -e 's/:/\n/g' | grep $PWD >/dev/null; then
  echo PATH is already set, not resetting
else
   PATH=$PWD/bin:.:$PATH
fi

if which swift >& /dev/null; then
  echo "Using swift from PATH"
else
  echo "Trying to load swift module:"
  module load swift
  if which swift >& /dev/null; then
    Swift loaded from module
  else
    echo "Cant find a swift to run"
    return
  fi
fi

echo -e "\\nUsing swift release from: \\c"
which swift
swift -version

echo -e "Cleaning test.* dirs and creating input/ dirs...\\n"

(cd test.beagle;   clean; makeinput 100)
(cd test.fusion;   clean; makeinput 100)
(cd test.local;    clean; makeinput 10)
(cd test.mcs;      clean; makeinput 100)
(cd test.midway;   clean; makeinput 100)
(cd test.multiple; clean; makeinput 3000)
(cd test.raven;    clean; makeinput 100)
(cd test.stampede; clean; makeinput 100)
(cd test.uc3;      clean; makeinput 100)

for d in test.*; do
  (cd $d; echo -e "$d: $(find input -type f -print | wc -l) files, $(grep sleep getlanduse.pl | sed -e 's/[^0-9]//g') second task duration" )
done



