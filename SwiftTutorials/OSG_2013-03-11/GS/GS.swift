type file;

app (file output) GS (float Tolerance_Ratio, int Dimensions, int Nagents, int RunNumber, file gs_binary, file gs_script)
{
  bash @filename(gs_script) Tolerance_Ratio Dimensions Nagents RunNumber @filename(gs_binary) stdout=@filename(output);
}

float [] tol_ratio=[0.40,0.41,0.42,0.43,0.44,0.45,0.46,0.47,0.48,0.49,0.50];
int [] Dim=[1,2,3,4,5];
int [] NA=[10, 100, 1000, 10000];
int [] repetitions = [1:100];

file simulation_binary <"/scratch/midway/davidkelly999/GS/Group_Simulation">;
file simulation_script <"/scratch/midway/davidkelly999/GS/run_GS.sh">;

foreach Tol in tol_ratio {
   foreach D in Dim {
      foreach na in NA {
          foreach rep in repetitions {
              file outfile <single_file_mapper; file=@strcat("Tolerance_Ratio_",Tol,"/Dim_",D,"/NAgents_",na,"/",rep,".out")>;
              outfile = GS (Tol, D, na, rep, simulation_binary, simulation_script);
         }
     }
  }
}

