#!/bin/bash
Tolerance_Ratio=$1 
Dimensions=$2
Nagents=$3  
Run_Number=$4
executable=$5

#define rarely changing variables
Groups=0
debug=0
Output="grouping"
stopAt=1000

rundirname="Tolerance_Ratio_"${Tolerance_Ratio}/"Dim_"${Dimensions}/"NAgents_"$Nagents
chmod +x $executable

START=$SECONDS
$executable $Tolerance_Ratio $Dimensions $Nagents $stopAt $Groups $Run_Number $debug $Output 2>&1
echo $(( $SECONDS - $START )) 2>&1
