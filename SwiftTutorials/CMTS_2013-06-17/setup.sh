
echo -e "\nNOTE: This command must be run as \"source setup.sh\", not directly! \n"
echo -e "Initializing Swift tutorial environment\n"

module load swift

# export SBATCH_RESERVATION=swift

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -e "Adding $DIR/bin to PATH\n"

PATH=$DIR/bin:$PATH

# echo -e "PATH=$PATH\n"

mkdir -p $HOME/.swift

if [ -f $HOME/.swift/swift.properties ]; then
  echo -e "Adding properties to end of $HOME/.swift/swift.properties\n"
else
  echo -e "creating $HOME/.swift/swift.properties\n"
fi

cat >>$HOME/.swift/swift.properties <<END

#   Swift runtime properties for CMTS Cyber Tutorial 

# Get sites and apps lists from these files in cwd:

sites.file=sites.xml
tc.file=apps

# Data and status staging modes:

use.provider.staging=false
provider.staging.pin.swiftfiles=false
use.wrapper.staging=false
status.mode=provider

# For better debugging:

wrapperlog.always.transfer=true
execution.retries=0
lazy.errors=false
sitedir.keep=true
file.gc.enabled=false
file.gc.enabled=false

END
