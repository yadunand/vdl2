type file;

global string cwd = @arg("cwd",".");

app (file blist) genConfigs (dir, string max, string delta)
{
  gensweep dir max delta stdout=@blist;
}

app (file rmsdout, file rmsd) rmsd (file i[], string id)
{
  rmsd id;
}

app (file plot_data) analyze (file[] rmsdfiles)
{
  analyze @filenames(rmsdfiles) stdout=@plot_data;
}

global file pdb      <"cg_clath_cage.pdb">;
global file psf      <"cg_clath_cage.psf">;

string sweepMax=@arg("max","0.6"), sweepDelta=@arg("delta","0.5");

sweep(string beads[],string set)
{
  file rmsds[];
  foreach beadid, i in beads {
    file conf   <single_file_mapper; file=@strcat(set,"/CONF/",beadid,".conf")>;
    file inp    <single_file_mapper; file=@strcat(set,"/PAR/", beadid,".inp")>;
    file vmdtcl <single_file_mapper; file=@strcat(set,"/VMD/", beadid,".tcl")>;
  
    file log    <single_file_mapper; file=@strcat(set,"/OUT/", beadid,".log")>;
    file dat    <single_file_mapper; file=@strcat(set,"/RMSD/",beadid,".dat")>;
  
    tracef("Processing bead ID %s\n", beadid);
  
    (log,dat) = rmsd([conf,pdb,psf,inp,vmdtcl], beadid);
    rmsds[i] = dat;
  }
   
  file plot_out <single_file_mapper; file=@strcat(set,"/OUT/plot_pd.txt")>;
  plot_out = analyze(rmsds);
}


string bead_set_1[] = readData(genConfigs("0.2", "0.1"));

# string bead_set_2[] = readData(genConfigs("0.5", "0.5"));

sweep(bead_set_1,"set1");





