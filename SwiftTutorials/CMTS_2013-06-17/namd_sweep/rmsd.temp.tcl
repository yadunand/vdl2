#====================================
# THis script will measure the 
# the average angles, bond lengths
# and dihedral angles.
#
# -FXV Oct, 2011
#===================================

mol delete all

# set atom selection
set seltext "type HB AK" 

# Set the initial frame
set inifrm 200
set inifrm 1

set psf cg_clath_cage.psf
set pdb cg_clath_cage.pdb
set dcd OUT/FILEID.dcd

# Load input files
mol load psf $psf
# Load trajectory
mol addfile $pdb type pdb
mol addfile $dcd type dcd first $inifrm last -1 waitfor all

# Get molecule information
set mol [molinfo top]

# Get the number of frames
set nmfrm [molinfo $mol get numframes]
puts "number of frames $nmfrm"

# open rmsd file
set frmsd [open RMSD/FILEID.dat w]

# Select atoms
set ref [atomselect top $seltext frame 0] 
	
# Loop over the number of frames
for {set ifrm 0} {$ifrm <= $nmfrm} {incr ifrm} {
	
	set sel [atomselect top $seltext frame $ifrm]
	
	set irmsd [measure rmsd $ref $sel]
	
	lappend rmsdlist $irmsd


}

# Analysis
puts $frmsd "B1 B2 [vecmean $rmsdlist]"

exit

