#!/usr/bin/env perl

use strict;
use warnings;

# Create NAMD input file for short simulations
# to sweep the nonbonded parameter space
#
# ARGS: dir max delta  # FIXME: add bead names and epsilon

chdir($ARGV[0]);

# Create dirs

system("mkdir  -p CONF PAR VMD RMSD OUT");

# Bead names

my $b1name = "PX";
my $b2name = "DT";

# Bead ranges

my $b1min=0.0;
my $b2min=0.0;

my $b1max = $ARGV[1];
my $b2max = $ARGV[1];
my $del =   $ARGV[2];
my $eps =   "-0.01";

my $b1=$b1min;
while ($b1<=$b1max) {
  my $b2=$b2min;
  while ($b2<=$b2max) {

    my $s1=sprintf("%05.2f",$b1);
    my $s2=sprintf("%05.2f",$b2);
    my $fileid="${b1name}_${s1}_${b2name}_${s2}";

    print("$fileid\n");
    system("sed -e s/FILEID/${fileid}/g cg_clath_cage.temp.conf                 > CONF/$fileid.conf");
    system("sed -e s/FILEID/${fileid}/g -e s/B1/$s1/ -e s/B2/$s2/ rmsd.temp.tcl > VMD/$fileid.tcl");
    system("sed -e s/HBEPS/$eps/ -e s/PXEPS/-$s1/ -e s/DTEPS/-$s2/ -e s/AKEPS/$eps/ -e s/LTEPS/$eps/  par_cg_clath.temp.inp > PAR/$fileid.inp");

    $b2 = $b2+$del;
  }
  $b1 = $b1+$del;
}
