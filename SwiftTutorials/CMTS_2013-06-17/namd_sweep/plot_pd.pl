#!/usr/bin/env perl

use strict;
use warnings;

# This code will run many short simulations in order to 
# sweep the nonbonded parameter space

# File names
open (OFILE, ">", "rmsd_pd.dat");

# Bead names
my $b1name = "PX";
my $b2name = "DT";

# Bead ranges
my $b1min=0.0;
my $b1max=10.0;

my $b2min=0.0;
my $b2max=10.0;

my $del=0.50;

# Set large number to find RMSD min
my $rmsdmin = 123456789.0;
my $b1rmsdmin=0.0;
my $b2rmsdmin=0.0;


my $b1=$b1min;

while ($b1<=$b1max) 
{
	my $b2=$b2min;

	while ($b2<=$b2max)
	{
	


		if (-s "RMSD/rmsd.$b1name${b1}_$b2name${b2}.dat")
		{	
			open(INPUT, "<", "RMSD/rmsd.$b1name${b1}_$b2name${b2}.dat");
			my $_ = <INPUT>;

			my @data = split(/ +/,$_);
        		my $rmsd=$data[2]*1.0;
			
			# Find Minimum
			if ($rmsd<$rmsdmin)
			{
				$rmsdmin=$rmsd;
				$b1rmsdmin=$b1;
				$b2rmsdmin=$b2;
			}
	
			print OFILE "$b1 $b2 $rmsd\n";			
			close(INPUT);
		}		

		$b2 = $b2+$del;
	
	}

	print OFILE " \n";
	$b1 = $b1+$del;

}

close(OFILE);

print "Minimum Values\n";
print "RMSD = $rmsdmin, eps_$b1name = $b1rmsdmin, eps_$b2name = $b2rmsdmin\n" ; 
