#! /bin/sh

id=$1

module load namd
module load vmd

  mpiexec      namd2 CONF/$id.conf  > OUT/$id.log 2>&1 
# mpiexec -n 8 namd2 CONF/$id.conf  > OUT/$id.log 2>&1 

vmd -dispdev text -e VMD/$id.tcl >> OUT/$id.log 2>&1
