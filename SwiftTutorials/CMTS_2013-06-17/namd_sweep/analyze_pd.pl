#!/usr/bin/env perl

use strict;
use warnings;
use File::Basename;

my $rmsdmin = 123456789.0;  # Set large number to find min RMSD
my $b1rmsdmin=0.0;
my $b2rmsdmin=0.0;
my $b1name;
my $b2name;

for(my $i=0; $i<=$#ARGV; $i++) {

  my $filename =  $ARGV[$i];
  my $basename = basename($filename,".dat");
  my @params = split(/_/,$basename);

  $b1name = $params[0];
  my $b1 =  $params[1];
  $b2name = $params[2];
  my $b2 =  $params[3];

  open(INPUT, "<", $filename);
  my $_ = <INPUT>;
  my @data = split(/ +/,$_);
  my $rmsd=$data[2]*1.0;
			
  # Find Minimum
  if ($rmsd<$rmsdmin) {
    $rmsdmin=$rmsd;
    $b1rmsdmin=$b1;
    $b2rmsdmin=$b2;
  }
	
  printf "%0.3f %0.3f %0.9f\n", $b1, $b2, $rmsd;			
  close(INPUT);
}		

printf "\nMinimum Values: RMSD=%0.9f eps_$b1name=%0.3f eps_$b2name=%0.3f\n", $rmsdmin, $b1rmsdmin, $b2rmsdmin; 
