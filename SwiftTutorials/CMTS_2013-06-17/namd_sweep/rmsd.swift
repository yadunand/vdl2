type file;

global string cwd = @arg("cwd",".");

app (file blist) genConfigs (string max, string delta)
{
  gensweep cwd max delta stdout=@blist;
}

app (file rmsdout, file rmsd) rmsd (file i[], string id)
{
  rmsd id;
}

app (file plot_data) analyze (file[] rmsdfiles)
{
  analyze @filenames(rmsdfiles) stdout=@plot_data;
}

file pdb      <"cg_clath_cage.pdb">;
file psf      <"cg_clath_cage.psf">;

file rmsds[];

string sweepMax=@arg("max","0.6"), sweepDelta=@arg("delta","0.5");

string beadids[] = readData(genConfigs(sweepMax, sweepDelta));


foreach beadid, i in beadids {
  file conf   <single_file_mapper; file=@strcat("CONF/",beadid,".conf")>;
  file inp    <single_file_mapper; file=@strcat("PAR/", beadid,".inp")>;
  file vmdtcl <single_file_mapper; file=@strcat("VMD/", beadid,".tcl")>;

  file log    <single_file_mapper; file=@strcat("OUT/", beadid,".log")>;
  file dat    <single_file_mapper; file=@strcat("RMSD/",beadid,".dat")>;

  tracef("Processing bead ID %s\n", beadid);

  (log,dat) = rmsd([conf,pdb,psf,inp,vmdtcl], beadid);
  rmsds[i] = dat;
}

file plot_out <"OUT/plot_pd.txt">;
plot_out = analyze(rmsds);
