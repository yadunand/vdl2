#! /bin/sh

# Print the lowest N numbers from standard input (deafult: lowest 3)

sort -n | head -${1:-3}
