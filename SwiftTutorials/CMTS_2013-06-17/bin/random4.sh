#! /bin/sh

runtime=${1:-0}
range=${2:-30000}
n=${3:-10}

if [ -f "$3" ]; then
   n=$( cat $3 )
fi

sleep $runtime

for ((i=0;i<n;i++)); do
  echo $(($RANDOM%range))
done

