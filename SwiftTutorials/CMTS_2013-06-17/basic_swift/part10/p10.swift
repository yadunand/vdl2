type qvalfile;
type coordfile;
type shotfile;

type shootargs{
    string coords;
    string qvals;
    int numphi;
    int numshots;
    int nummolec;
    float photons;
    int parallel;
    string output_filename;
}

app (shotfile outfile) shootsim (shootargs args, coordfile coords, qvalfile qvals) {
     echo "polshoot" "-s" @coords "-f" @qvals "-x" args.numphi "-n" args.numshots
                     "-m" args.nummolec "-g" args.photons "-p" args.parallel "-o" @outfile
                     stdout=@outfile; # Note double use of outfile, just for testing/demo
}

shootargs myargs[] = readData("particles.dat");

foreach a in myargs {
    shotfile  output <single_file_mapper; file=a.output_filename>;
    coordfile coords <single_file_mapper; file=a.coords>;
    qvalfile  qvals  <single_file_mapper; file=a.qvals>;
    output = shootsim(a, coords, qvals);
}

