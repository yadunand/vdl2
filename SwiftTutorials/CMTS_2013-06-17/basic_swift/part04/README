
TOPIC:   Run a program in a foreach() loop with explicitly named output files.


Study the program:

  @strcat() is a Swift builtin function (the @name() form is used for many but not all builtins)

  Note that @strcat() coerces numeric args to strings 

  <"filename"> was a shorthand for <single_file_mapper;...> but the latter is more flexible.


Run the program:

  $ swift p4.swift

Look at the output:

  $ cd output
  $ ls -l
  $ cat *


Try these exercises:

  o Adjust the script to take the number of simulations to do from the command line

    @arg("argname") returns arguments after the .swift script on the swift command line:

      swift -tc.file tc p4.swift -myarg1=value1 ...

    @arg("argname","default") returns "default" if the argname is not given on the command line

    @toInt() converts strings to integers:

      @toInt(@arg("myArg1"))


  o Add some output tracing.

    trace(expr1,...) traces expressions on stdout.

    tracef() allows some simple formatting ala printf (but with slighly different codes)
    see: http://www.swiftlang.org/guides/trunk/userguide/userguide.html#_tracef

    Try inserting a few traces.  But remember: Swift is *very* concurrent!

  o Use readData() to read the output files back in to the Swift script and print their values.

  o Use readData() to read both a scalar and an array

  o create a version of Random that prints pairs, and read these into arrays

  o Experiment with sparse arrays

  o Experiment with string-indexed arrays (hash tables)


