type file;

app (file o) mysim4 (int timesteps, int range_value, int count_value)
{
  random4 timesteps range_value count_value stdout=@filename(o);
}

app (file o) analyze (file s[])
{
  average @filenames(s) stdout=@filename(o);
}

(file o) generate_randoms (int timesteps, int range_value, int count_value)
{
  file tmp;
  tmp = mysim4(timesteps, range_value, 1);
  int rand = readData(tmp);
  o = mysim4(timesteps, range_value, rand);
}


file sims[];
int  nsim  = @toInt(@arg("nsim",  "10"));
int  steps = @toInt(@arg("steps", "1"));
int  range = @toInt(@arg("range", "100"));
int  count = @toInt(@arg("count", "10"));

foreach i in [0:nsim-1] {
  file simout <single_file_mapper; file=@strcat("output/sim_", i, ".tmp.out")>;
  simout = generate_randoms(steps, range, count);
  sims[i] = simout;
}

file stats<"output/average.out">;
stats = analyze(sims);
