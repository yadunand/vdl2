type file;

app (file o) mysim ()
{
  random stdout=@filename(o);
}

app (file o) analyze (file s[])
{
  average @filenames(s) stdout=@filename(o);
}

file sims[];

int nsim = @toInt(@arg("nsim","10"));

foreach i in [0:nsim-1] {
  file simout <single_file_mapper; file=@strcat("output/sim_",i,".out")>;
  simout = mysim();
  sims[i] = simout;
}

file stats<"output/average.out">;
stats = analyze(sims);
