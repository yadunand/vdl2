
import "lib/types";

type Volume {
  file header;
  file image;
};

app (file warp) alignlinear (Volume ref, Volume subject, string params[])
{
  alignlinear @filename(ref.header) @filename(subject.header) @filename(warp) params;
}

string dataDir = @arg("d","data");  // Get a command-line argument, -d

file alignment;

Volume refVolume  <ext; exec="volmapper",name="data/reference">;
Volume subjects[] <ext; exec="volmapper",prefix=@strcat(dataDir,"/anatomy")>;
file   warps[]    <simple_mapper; location="warps",prefix="warp">;

foreach subjectVolume, i in subjects {
  warps[i] = alignlinear(refVolume, subjectVolume, ["-m","12","-q"]);
}
