
import "lib/types";
import "lib/fMRI";
import "lib/AIR";
import "lib/FSL";
import "lib/ImageMagick";

# Perform align() function in an external shell script

file   alignScript<"align.sh">;

app (Volume alignedVol) align_sh (file script, Volume reference, Volume input)
{
  sh @script @filename(reference.image) @filename(input.image) @filename(alignedVol.image);
}

string dataDir = @arg("d","data");

Volume referenceVolume <ext; exec="volmapper",name="data/reference">;
Volume inputVolumes [] <ext; exec="volmapper",prefix=@strcat(dataDir,"/anatomy")>;
Volume outputVolumes[];

foreach v, i in inputVolumes {
  Volume aligned <ext; exec="volmapper",name=@strcat("work/aligned",i)>;
  aligned = align_sh(alignScript,referenceVolume, v);
  outputVolumes[i] = aligned;
}

Volume atlasVolume <ext; exec="volmapper",name="output/atlas">;
atlasVolume = softmean(outputVolumes);

file xSlice<"output/atlas-x.png">;
file ySlice<"output/atlas-y.png">;
file zSlice<"output/atlas-z.png">;

xSlice = convert(slicer(atlasVolume,"x"));
ySlice = convert(slicer(atlasVolume,"y"));
zSlice = convert(slicer(atlasVolume,"z"));
