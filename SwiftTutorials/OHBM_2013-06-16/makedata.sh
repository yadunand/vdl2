#! /bin/sh

datadir=${1:-data_10}
nfiles=${2:-10}

rm -rf $datadir
mkdir $datadir
cp data/anatomy1.{hdr,img} $datadir

n=0
( cd  $datadir
  mv anatomy1.hdr s.hdr
  mv anatomy1.img s.img
  for id in $(seq -w 000 999); do
      n=$((n+1))
      if [ $n -gt $nfiles ]; then
        break;
      else
        ln s.img anatomy$id.img
        ln s.hdr anatomy$id.hdr
      fi
  done
  rm s.{img,hdr}
)
