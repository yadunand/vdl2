
app (Volume atlas) softmean (Volume v[])
{
  softmean @filename(atlas.header) "y" "null" @filenames(v);
}

app (file warp) alignlinear (Volume ref, Volume subject, string params[])
{
  alignlinear @filename(ref.header) @filename(subject.header) @filename(warp) params;
}

app (file warp) alignwarp (Volume ref, Volume subject, file initWarp, string params[])
{
  align_warp @filename(ref.header) @filename(subject.header) @filename(warp) "-f" @filename(initWarp) params;
}

app (Volume v) reslice (file warp, Volume subject)
{
  reslice @filename(warp) @filename(v.header) "-a" @filename(subject.header);
}
