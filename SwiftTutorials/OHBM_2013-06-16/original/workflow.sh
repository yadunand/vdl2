#!/bin/sh

#
# Original workflow from PC1 and DBIC
#

AIR5.2.5/bin/align_warp anatomy1.img reference.img warp1.warp -m 12 -q
AIR5.2.5/bin/align_warp anatomy2.img reference.img warp2.warp -m 12 -q
AIR5.2.5/bin/align_warp anatomy3.img reference.img warp3.warp -m 12 -q
AIR5.2.5/bin/align_warp anatomy4.img reference.img warp4.warp -m 12 -q

AIR5.2.5/bin/reslice warp1.warp resliced1
AIR5.2.5/bin/reslice warp2.warp resliced2
AIR5.2.5/bin/reslice warp3.warp resliced3
AIR5.2.5/bin/reslice warp4.warp resliced4

AIR5.2.5/bin/softmean atlas.hdr y null resliced1.img resliced2.img resliced3.img resliced4.img

fsl/bin/slicer atlas.hdr -x .5 atlas-x.pgm
fsl/bin/slicer atlas.hdr -y .5 atlas-y.pgm
fsl/bin/slicer atlas.hdr -z .5 atlas-z.pgm

convert atlas-x.pgm atlas-x.gif
convert atlas-y.pgm atlas-y.gif
convert atlas-z.pgm atlas-z.gif
