#!/bin/sh

AIR5=$HOME/software/AIR5.3.0
fsl=/project/wilde/software/fsl-5.0.4/fsl

if true; then

#time  $AIR5/bin/align_warp anatomy1.img reference.img warp1.warp -m 1 12 -s 1024 2 2 -c .01 -b1 .1 .1 .1 -b2 .1 .1 .1  -h 10000 -v -j # -q

time  $AIR5/bin/align_warp reference.img anatomy1.img warp1.warp -m 1 12  -b1 .1 .1 .1  -b2 .1 .1 .1  -h 10000 -v -s 1024 2 2 -c .01  -f out # -q -s 1024 2 2 -c .01


time  $AIR5/bin/align_warp anatomy2.img reference.img warp2.warp -m 1 12 -s 64 2 2 -c .01 -b1 .1 .1 .1 -b2 .1 .1 .1  # -q
time  $AIR5/bin/align_warp anatomy3.img reference.img warp3.warp -m 1 12 -s 64 2 2 -c .01 -b1 .1 .1 .1 -b2 .1 .1 .1  # -q
time  $AIR5/bin/align_warp anatomy4.img reference.img warp4.warp -m 1 12 -s 64 2 2 -c .01 -b1 .1 .1 .1 -b2 .1 .1 .1  # -q
time  $AIR5/bin/reslice warp1.warp resliced1
time  $AIR5/bin/reslice warp2.warp resliced2
time  $AIR5/bin/reslice warp3.warp resliced3
time  $AIR5/bin/reslice warp4.warp resliced4
time  $AIR5/bin/softmean atlas.hdr y null resliced1.img resliced2.img resliced3.img resliced4.img
fi

export FSLOUTPUTTYPE=NIFTI

time $fsl/bin/slicer atlas.hdr -x .5 atlas-x.pgm
time $fsl/bin/slicer atlas.hdr -y .5 atlas-y.pgm
time $fsl/bin/slicer atlas.hdr -z .5 atlas-z.pgm
time convert atlas-x.pgm atlas-x.gif
time convert atlas-y.pgm atlas-y.gif
time convert atlas-z.pgm atlas-z.gif
