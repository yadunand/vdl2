type file;

app (file o) cat (file i)
{
  cat @i stdout=@o;
}

file out[]<simple_mapper; location="output", prefix="catsn.",suffix=".out">;

foreach j in [1:toInt(arg("n","100"))] {
  file data <"data.txt">;
  out[j] = cat(data);
}
