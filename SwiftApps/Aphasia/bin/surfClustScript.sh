#!/bin/bash
# the input ARGV[0] is the original rand.brain.1 (or obs.imit.all)
# the input ARGV[1] is the centrolized thresholds file

 export PATH=/home/tstef/local/afni/linux_gcc32:$PATH

 export LD_LIBRARY_PATH=/home/tstef/local/BRIC/sumaLibs

tar zxvf $1
echo "invoke surfclust $2 $3  *.names"

# `SurfClust -spec colin_lh_mesh140_std.spec -surf_A colin_lh_mesh140_std.pial.asc -input $1 0 -rmm
 cat BRAIN*.dset >> brain.final.dset


