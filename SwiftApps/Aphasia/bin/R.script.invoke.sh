#!/bin/bash
# invoke as R.script.invoke.sh <script.R>

#export R_LIBS=/home/tiberius/local/APHASIA/lib
export R_LIBS=/home/uhasson/R.libs

export R_APHASIA_INPUT_FILE=$2
export R_APHASIA_OUTPUT_FILE=$3

/soft/R-2.4.0-r1/bin/R CMD BATCH -vanilla $1
