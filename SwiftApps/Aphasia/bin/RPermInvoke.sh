#!/bin/bash
# invoke as R.script.invoke.sh <script.R>

#export R_LIBS=/home/tiberius/local/APHASIA/lib
export R_LIBS=/home/uhasson/R.libs

export R_PERM_START=$2
export R_PERM_SIZE=$3

/soft/R-2.4.0-r1/bin/R CMD BATCH -vanilla $1

tar zcvf $4 ccf*
