type file{};

(file o) mysqlInvoke (file script) {
	app {
		mysqlInvoke @filename(script) @filename(o);
	}
}

executionWrapper (file mysqlFile){
	file outputFile<simple_mapper; prefix="output-{VDL:RUNID}", suffix="txt">;
	outputFile = mysqlInvoke(mysqlFile);	
}

//this file in the local directory will contain the mysql script that you are attempting to execute
file mysqlScript<"mysql.script">;
executionWrapper(mysqlScript);




