CREATE TABLE PUBLIC.ExperimentInfo (
       ID CHAR(13) NOT NULL
     , UserID VARCHAR(32)
     , RunDate TIMESTAMP
     , PRIMARY KEY (ID)
)TYPE=InnoDB;

CREATE TABLE PUBLIC.ExperimentExecution (
       ID CHAR(13) NOT NULL
     , WorkflowName VARCHAR(32)
     , InputScripts BLOB
     , InputParams VARCHAR(100)
     , PRIMARY KEY (ID)
     , INDEX (ID)
     , CONSTRAINT FK_ExperimentExecution_1 FOREIGN KEY (ID)
                  REFERENCES PUBLIC.ExperimentInfo (ID)
)TYPE=InnoDB;

CREATE TABLE ExperimentResults (
       ID CHAR(13) NOT NULL
     , OutputData TEXT
     , Notes VARCHAR(2000)
     , LogFile VARCHAR(100)
     , GraphFiles VARCHAR(100)
     , TimingInfo VARCHAR(100)
     , AdditionalFiles TEXT
     , PRIMARY KEY (ID)
     , INDEX (ID)
     , CONSTRAINT FK_ExperimentResults_1 FOREIGN KEY (ID)
                  REFERENCES ExperimentInfo (ID)
)TYPE=InnoDB;

