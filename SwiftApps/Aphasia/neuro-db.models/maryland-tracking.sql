CREATE TABLE ExperimentInfo (
       ID CHAR(13) NOT NULL
     , UserID VARCHAR(32)
     , RunDate TIMESTAMP
     , PRIMARY KEY (ID)
)TYPE=InnoDB;

CREATE TABLE ExperimentExecution (
       ID CHAR(13) NOT NULL
     , WorkflowScript TEXT
     , InputScripts BLOB
     , InputParams VARCHAR(100)
     , PRIMARY KEY (ID)
     , INDEX (ID)
     , CONSTRAINT FK_ExperimentExecution_1 FOREIGN KEY (ID)
                  REFERENCES ExperimentInfo (ID)
)TYPE=InnoDB;

CREATE TABLE ExperimentResults (
       ID CHAR(13) NOT NULL
     , OutputData BLOB
     , Notes VARCHAR(2000)
     , LogFile TEXT
     , GraphFiles BLOB
     , TimingInfo VARCHAR(100)
     , AdditionalFiles BLOB
     , PRIMARY KEY (ID)
     , INDEX (ID)
     , CONSTRAINT FK_ExperimentResults_1 FOREIGN KEY (ID)
                  REFERENCES PUBLIC.ExperimentInfo (ID)
)TYPE=InnoDB;

