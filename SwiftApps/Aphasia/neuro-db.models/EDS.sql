CREATE TABLE DataTable (
       SUBJECT SMALLINT NOT NULL
     , NODES INT NOT NULL
     , StoryAction FLOAT
     , StorySpace FLOAT
     , StoryTime FLOAT
     , BlockAction FLOAT
     , BlockSpace FLOAT
     , BlockTime FLOAT
     , EventAction0 FLOAT
     , EventAction1 FLOAT
     , EventAction2 FLOAT
     , EventAction3 FLOAT
     , EventAction4 FLOAT
     , EventAction5 FLOAT
     , EventAction6 FLOAT
     , EventAction7 FLOAT
     , EventAction8 FLOAT
     , EventAction9 FLOAT
     , EventSpace0 FLOAT
     , EventSpace1 FLOAT
     , EventSpace2 FLOAT
     , EventSpace3 FLOAT
     , EventSpace4 FLOAT
     , EventSpace5 FLOAT
     , EventSpace6 FLOAT
     , EventSpace7 FLOAT
     , EventSpace8 FLOAT
     , EventSpace9 FLOAT
     , EventTime0 FLOAT
     , EventTime1 FLOAT
     , EventTime2 FLOAT
     , EventTime3 FLOAT
     , EventTime4 FLOAT
     , EventTime5 FLOAT
     , EventTime6 FLOAT
     , EventTime7 FLOAT
     , EventTime8 FLOAT
     , EventTime9 FLOAT
     , TappingAction FLOAT
     , PRIMARY KEY (SUBJECT, NODES)
);

CREATE TABLE DescriptorTable (
       SUBJECT SMALLINT NOT NULL
     , NODES INT NOT NULL
     , ROI SMALLINT
     , BlockActionReliable INT
     , BlockSpaceReliable INT
     , BlockTimeReliable INT
     , PRIMARY KEY (SUBJECT, NODES)
);

