<%@ page import="java.sql.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>

<%

if(System.getProperty("experimentdb.name") == null){
	ServletContext context = getServletContext();
	String home = context.getRealPath("").replace('\\', '/');
	String tempdir = context.getAttribute("javax.servlet.context.tempdir").toString();

	String pfFile = home + "/WEB-INF/experiment-mgmt.properties";
	File pf = new File(pfFile);
	if (pf.canRead()) {
    	Properties prop = new Properties();
     	try{
        	prop.load(new FileInputStream(pf));
            for ( Enumeration e = prop.propertyNames(); e.hasMoreElements(); ) {
                     String key = (String) e.nextElement();
                     String value = prop.getProperty(key);
                     System.setProperty(key, value);
                     //out.println("key: " + key + " value: " + value);
             }
		} catch (Exception e){
         	out.println("Cannot set system properties");
         	return;
		}
	} else {
     	out.println("Couldn't read the elab System properties file: " + pfFile);
		return;
	}
} else {
	//system properties have already been read
}
	
String dbName = (String)System.getProperty("experimentdb.name");
String dbUser = (String)System.getProperty("experimentdb.user");
String dbPass = (String)System.getProperty("experimentdb.pass");

Connection conn = null;

try {
	Class.forName("com.mysql.jdbc.Driver").newInstance();
     conn = DriverManager.getConnection("jdbc:mysql://localhost/"+dbName, dbUser, dbPass);

    	//out.println("Connected to the DB");
    // Do something with the Connection
}catch(ClassNotFoundException e){
	out.println("Database driver not found.");
} catch (SQLException ex) {
    // handle any errors
    out.println("SQLException: " + ex.getMessage());
    out.println("SQLState: " + ex.getSQLState());
    out.println("VendorError: " + ex.getErrorCode());
}

if (conn == null){
    out.println("There was an error in the user database connection code.");
    return;
}
Statement s = null;
try {
    s = conn.createStatement();
} catch (SQLException se) {
    out.println("We got an exception while creating a statement:" +
         "that probably means we're no longer connected to the user data database.");
    se.printStackTrace();
    return;
}
ResultSet rs = null;


%>