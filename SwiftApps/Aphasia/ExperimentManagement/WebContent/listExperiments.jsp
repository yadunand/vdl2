<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ include file="mysql-jdbc.jsp" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Experiment Management List</title>
</head>
<body>

<jsp:include page="header.jsp" />

<h3>Experiments matching your request:</h3>

<table border=1>
<%
String queryType = request.getParameter("queryType");
//out.println("Query: "+queryType+" type");
String queryParam = request.getParameter("searchParam");
//out.println("Search for: "+queryParam+" items");
//TODO: sanitize the searchParam string
String sanitizedQueryParam=queryParam.replaceAll("[^a-zA-Z0-9_-]","");
out.println("Search for param: "+sanitizedQueryParam);

if (s!=null){
	//out.println("We have a live DB connection");

	String sqlCommand="";
	if (sanitizedQueryParam.length()==0){
		// no condition means select all
		sqlCommand="select * from ExperimentInfo;";
	}else{
		if (queryType.equalsIgnoreCase("date")){
			sqlCommand="select * from ExperimentInfo where RunDate like '%"+sanitizedQueryParam+"%';";		}
		if (queryType.equalsIgnoreCase("workflow")){
			sqlCommand=" select ExperimentInfo.ID, ExperimentInfo.UserID, ExperimentInfo.RunDate  from ExperimentInfo INNER JOIN ExperimentExecution on ExperimentInfo.ID=ExperimentExecution.ID where WorkflowName like '%"+sanitizedQueryParam+"%';";		}
		if (queryType.equalsIgnoreCase("params")){
			sqlCommand=" select ExperimentInfo.ID, ExperimentInfo.UserID, ExperimentInfo.RunDate  from ExperimentInfo INNER JOIN ExperimentExecution on ExperimentInfo.ID=ExperimentExecution.ID where InputParams like '%"+sanitizedQueryParam+"%';";		}
		if (queryType.equalsIgnoreCase("outputs")){
			sqlCommand=" select ExperimentInfo.ID, ExperimentInfo.UserID, ExperimentInfo.RunDate  from ExperimentInfo INNER JOIN ExperimentResults on ExperimentInfo.ID=ExperimentResults.ID where OutputData like '%"+sanitizedQueryParam+"%';";		}
	}	
	try{
		rs=s.executeQuery(sqlCommand);
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        
        out.println("<tr>");
        for (int i = 1; i <= columnCount; i++)
        	out.println("<td><b>" + metaData.getColumnName(i) + "</td>");
        out.println("</tr>");

        while (rs.next()){
        	out.println("<tr>");
            //for (int i = 1; i <= columnCount; i++)
            //	out.println("<td>" + rs.getString(i) + "</td>");
			out.println("<td><a href=experimentDetails.jsp?experiment="+rs.getString(1)+">"+rs.getString(1)+"</a></td>");
			out.println("<td>"+rs.getString(2)+"</td><td>"+rs.getString(3)+"</td>");
        }
        out.println("</tr>");
	}catch(SQLException sqle){
		out.println("SQL Exception: "+sqle.getLocalizedMessage());
		return;
	}
}else{
	out.println("NO DB connection");
}

%>

</table>

<jsp:include page="footer.jsp" />
