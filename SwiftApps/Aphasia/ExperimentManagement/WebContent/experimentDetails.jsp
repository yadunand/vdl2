<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ include file="mysql-jdbc.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Experiment Details</title>
</head>
<body>

<jsp:include page="header.jsp" />

<table border=1>
<%
String experiment = request.getParameter("experiment");
out.println("<h3>Experiment "+experiment+"</h3>");

if (s!=null){
	//out.println("We have a live DB connection");
	String sqlCommand="select * from ExperimentExecution where ID='"+experiment+"';";
	try{
		rs=s.executeQuery(sqlCommand);
		ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        
		// I will only process one row (there is only one experiment with the ID given as a parameter)
        if (!rs.next()){
        	out.println("No such experiment ID: "+experiment);
        	return;
        }
        for (int i = 1; i <= columnCount; i++){
        	out.println("<tr><td><b>" + metaData.getColumnName(i) + "</td>");
			out.println("<td>" + rs.getString(i) + "</td></tr>");
		}

    	sqlCommand="select * from ExperimentResults where ID='"+experiment+"';";
		rs=s.executeQuery(sqlCommand);
		metaData = rs.getMetaData();
        columnCount = metaData.getColumnCount();
        
		// I will only process one row (there is only one experiment with the ID given as a parameter)
        if (!rs.next()){
        	out.println("No such experiment ID: "+experiment);
        	return;
        }
		//skip the experiment ID being displayed again
        for (int i = 2; i <= columnCount; i++){
        	out.println("<tr><td><b>" + metaData.getColumnName(i) + "</td>");
			out.println("<td>" + rs.getString(i) + "</td></tr>");
		}
            
	
	}catch(SQLException sqle){
		out.println("SQL Exception: "+sqle.getLocalizedMessage());
		return;
	}
}else{
	out.println("NO DB connection");
}

%>

</table>


<jsp:include page="footer.jsp" />
