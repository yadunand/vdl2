<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Experiment Management Tools</title>
</head>
<body>
<h2> Query the Experiment Database </h2>

<jsp:include page="header.jsp" />

<form method=post action="listExperiments.jsp">

Query Type:<br>
<select name=queryType>
<option value="date">Search Experiments by Date</option>
<option value="workflow">Search by workflow name</option>
<option value="params">Search by parameters passed to the workflow</option>
<option value="outputs">Search by the names of output files generated</option>
</select>
<p>
The query of type specified above should contain the following string as a parameter
<br>(The parameter below will be used in a "<font color=green>like '% param %'"</font> condition)
<br>Note: Will strip away all the characters not in the set <font color=green> [a-zA-Z_0-9_-]</font>
<br>
<textarea name=searchParam cols=35 rows=5>
</textarea>
<br>
<font color=green>Example: "2007-06" or "aphasia" or ".log"
<br>
Note: No entry in this field will list all the experiments in the database
</font>
<p>
<input type="submit">
</form>

<jsp:include page="footer.jsp" />

</body>
</html>