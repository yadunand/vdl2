#!/usr/bin/python

#
# Assuptions on file names:
# 
#

import sys
import glob
import re
import os 
import pwd
import shutil
import MySQLdb
import commands
from stat import *
from datetime import *

#experimentID="kt72h8b5zuv12"

# invoke: ./saveExperiment.py experimentID dbUser dbPass
    
def main():
    if len(sys.argv) < 4:
        print "Use like this: ./saveExperiment.py <expID> <dbUser> <dbPass>"
        sys.exit(1)
    experimentID=sys.argv[1]
    if len(experimentID) != 13:
        print "Invalid experiment ID, it should have 13 characters"
        sys.exit(1)
    print "Searching for experiment %s ... " % experimentID
    
    experimentMatch="*%s*" % experimentID
    inputs=glob.glob(experimentMatch)
    if len(inputs) < 1:
        print "No experiment with such an ID seems to be present, try again"
        sys.exit(1)
    #print "Files to be processed"
    saveFiles = [] 

    print "Start gathering Experiment INFO"

    for input in inputs: 
        #print input
        if input[-4:] == ".log":
            logfile=input
        elif input[-4:] == ".dot":
            dotfile=input
        else:
            #print "Process some output file %s" % input
            saveFiles.append(input)

    print "TO SAVE LOG File %s" % logfile
    print "TO SAVE GRAPH File %s" % dotfile
    #savefiles contain any files that include the ID in their name
    #this is determined by the way the user develops his own code
    print "TO SAVE: OUTPUT files %s" % saveFiles 

    file_stats = os.stat(logfile)
    logFileOwner = file_stats[ST_UID]
    print "EXPERIMENT OWNER: %s" % logFileOwner
    
    creationTime = file_stats[ST_CTIME]
    modificationTime = file_stats[ST_MTIME]
        
    printableCreationTime=datetime.fromtimestamp(creationTime)
    printableModificationTime=datetime.fromtimestamp(modificationTime)

    print "TO SAVE: executionDate %s" % printableCreationTime

   
    logFile=open(logfile)
    parametersString=''
    parameters={}
    wfFileNamePart = None
    invocationsIndex=0 #this counts how many times I am making a function invocation (to gather the parameters)
    for line in logFile.readlines():
        #m=re.match('.* Running job (\w*-\w*) (\w*) with arguments (\[\w*\]) in .*',line)
        # second group is the transformation name
        # third group is the list of parameters
        # fourth group is the name of the workflow file
        # fifth is the ID of the workflow
        m=re.match('.* Running job (\w*-\w*) (\w*) with arguments (\[.*\]) in (.*)\-(\w{13})\/.*',line)
        if m!=None:
            #print m.group(2,3,4,5)
            wfFileNamePart=m.group(4)
            if parametersString != m.group(3):
                parametersString=m.group(3)
                parametersString=parametersString.lstrip('[')
                parametersString=parametersString.rstrip(']')
                #print parametersString
                parameters[invocationsIndex]=[]
                for parameter in parametersString.split(','):
                    parameters[invocationsIndex].append(parameter.lstrip())
                invocationsIndex=invocationsIndex+1

    print "TO SAVE: Workflow invocation Parameters"
    print parameters

    if wfFileNamePart== None:
        print "Bad execution, I could not find any experiment invocation"
        sys.exit(1)
            
    wfFileNames=glob.glob(wfFileNamePart+'*.dtm')
    wfFileIndex=0
    print "Choose which one is the proper workflow file to save"
    for files in wfFileNames:
        print "%d    %s" % (wfFileIndex,files)
        wfFileIndex=wfFileIndex+1
    selectIndex=sys.stdin.readline()
    #print selectIndex
    m=re.match('(\d+)', selectIndex)
    while m==None:
        print "Not a number, try again"
        sys.exit(1)
        #selectIndex=sys.stdin.readline()
        #m=re.match('(\d+)', selectIndex)

    intIndex=int(m.group(1))
    if intIndex > len(wfFileNames)-1:
        print "Selection out of range"
        sys.exit(1)
        
    print "TO SAVE: workflow file"
    print wfFileNames[intIndex]
    
    #if some parameter is both in the log and in the script, under a file declaration, I should save it
    externalDependencies=[]
    wf=open(wfFileNames[intIndex])
    for line in wf.readlines():
        #m=re.match('\s*file \w*\s*<.*\"(.*)\".*>;',line) #this matches all the file mapper declarations
        m=re.match('\s*file \w*\s*<.*\"(.*)\">;',line) #this only matches file mappers that end with the filename "foo">;
        #m=re.match('\s*file *',line)
        if m != None:
            #print line
            #print m.group(1)
            externalDependencyFileName=m.group(1)
            #print "Also save " + externalDependencyFileName
            k=0
            while k<len(parameters):
                #print parameters[k]
                #if "tiberius" in parameters[k]:
                if externalDependencyFileName.lstrip('/') in parameters[k]:
                    #print parameters[k]
                    #print "Save file "  + externalDependencyFileName
                    if externalDependencyFileName not in externalDependencies:
                        externalDependencies.append(externalDependencyFileName)
                k=k+1
    
    
    print "TO SAVE: Workflow dependencies"
    print externalDependencies
    
    additionalFiles=[]
    print "Do you want addtional files to be saved ? (y/n)"
    wantMoreFiles=(sys.stdin.readline()).rstrip()
    additionalFilesIndex=0 
    while wantMoreFiles == "y":
        print "Please specify the regexp matching those filenames or the full file name"
        selectRegexp=(sys.stdin.readline()).rstrip()
        #print selectRegexp
        additionalFileNames=glob.glob(selectRegexp)
        for file in additionalFileNames:
            print "[ %d ]   %s " % (additionalFilesIndex, file)
            additionalFilesIndex=additionalFilesIndex+1
        print "Do you want these to be saved ? (others can be added soon) (y/n)"
        wantSelection=(sys.stdin.readline()).rstrip()
        if wantSelection=="y":
            for file in additionalFileNames:
                additionalFiles.append(file)
        
        print "Do you want addtional files to be saved ? (y/n)"
        wantMoreFiles=(sys.stdin.readline()).rstrip()
    
    print "TO SAVE: Additional files:"
    print additionalFiles

    notesBuffer=""
    print "Enter your comments, end with a line containing a single dot"
    while 1:
        readLine = sys.stdin.readline().rstrip()
        if readLine=='.':
            break
        notesBuffer = notesBuffer+readLine+"\n"
    
    print "TO SAVE: Your comments"
    print notesBuffer

#    insertExpInfo="insert into ExperimentInfo values(%s, %d, %s);" % (experimentID, logFileOwner, printableCreationTime)
#    print insertExpInfo    

    #
    #  =========== DB Save =============
    #

    print "START SAVE OPERATIONS"
    #if len(sys.argv) < 3:
    #    print "username and password"
    #    sys.exit(1)  
    
    #gather the INFO
    workflowFile=wfFileNames[intIndex]
    inputParams=parameters
    notes=notesBuffer
    logFile=".swiftrun/"+experimentID+"/"+logfile
    graphFiles=".swiftrun/"+experimentID+"/"+dotfile
    timingInfo=printableCreationTime

    
    print "Database user:"
    #dbUser = sys.stdin.readline().rstrip()
    dbUser = sys.argv[2]
    print "Database password for %s:" % dbUser
    #dbPass = sys.stdin.readline().rstrip()
    dbPass = sys.argv[3]
    try:
        connection = MySQLdb.connect(host="localhost", user=dbUser, passwd=dbPass, db="maryland" ) 
        cursor = connection.cursor()
        #sqlCommand="show tables;"
        sqlCommand="select ID from ExperimentInfo;"
        cursor.execute(sqlCommand) 
        data=cursor.fetchall()
        for row in data:
          if experimentID in row:
            print "ERROR Already in the DB"
            sys.exit(1)


        userName=pwd.getpwuid(logFileOwner).pw_name
        userHome=pwd.getpwuid(logFileOwner).pw_dir
        cacheLocation=userHome+"/.swiftrun/"+experimentID
    
        #create the destination if it does not exist
        if not os.path.isdir(userHome+"/.swiftrun"):
            os.mkdir(userHome+"/.swiftrun")
        if not os.path.isdir(userHome+"/.swiftrun/"+experimentID):
            os.mkdir(userHome+"/.swiftrun/"+experimentID)
    
        #workflow file is relative to the local dir
        shutil.copyfile(workflowFile,cacheLocation+"/"+workflowFile)
    
        
        #handle the input dependencies, these might be absolute paths
        inputScripts="";
        for deps in externalDependencies:
            depPathComps=os.path.split(deps)
            depFileName=depPathComps[1]
            inputScripts=inputScripts+cacheLocation+"/"+depFileName+"\n"
            #also copy the file to the destination
            shutil.copyfile(deps,cacheLocation+"/"+depFileName)
    
        #handle the outputs defined by the name containing the experiment ID
    
        outputData=""
        for outs in saveFiles:
            outputData=outputData+cacheLocation+"/"+outs+"\n"
            #also copy the file to the destination
            shutil.copyfile(outs,cacheLocation+"/"+outs)
    
        xtraFiles=""
        for xtra in additionalFiles:
            xtraFiles=xtraFiles+cacheLocation+"/"+xtra+"\n"
            #also copy the file to the destination
            shutil.copyfile(xtra,cacheLocation+"/"+xtra)



    
        print "Will add Experiment to the DB"
        # Insert ExpID, owner, Date
        sqlCommand="insert into ExperimentInfo values(\"%s\", \"%s\", \"%s\");" % (experimentID, logFileOwner,printableCreationTime)
        cursor.execute(sqlCommand)
        
        # populate the execution info    
        sqlCommand="insert into ExperimentExecution values(\"%s\", \"%s\", \"%s\", \"%s\");" % (experimentID, workflowFile, inputScripts, inputParams)
        cursor.execute(sqlCommand)
        
        # populate the results info
        sqlCommand="insert into ExperimentResults values(\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\");" % (experimentID, outputData, notes, logFile, graphFiles, timingInfo, xtraFiles)
        cursor.execute(sqlCommand)        
        print "Done with DB inserting"         
        
    except MySQLdb.OperationalError, message:
        errorMessage = "Error %d:\n%s" % (message[ 0 ], message[ 1 ] ) 
        print errorMessage
    else:
        print "DB Connection successful"
        cursor.close()
        connection.close()
    
 
main()
#validateInputs()
#cacheOps()
#dbOps()

