###################################################################################
# 2007-01-24, Eric Covey                                                          #
#                                                                                 #
# runWavelets.R: script reads the epoched data that was saved by trial type by    #
# the epocher.R script.  This script reads these files in sequence, and performs  #
# a cwt for each epoch of each channel of each trial type for each subject.       #
# Results are saved in multiple files, named by subject, trial type, and channel. #
#                                                                                 #
# The functionality of the scripts mysort.m and mysorttest.m has been integrated  #
# into the epocher.R script, but can be separated out again if desired.           #
#                                                                                 #
# Next goal: write this script as a function that takes analysis parameters as    #
# arguments, for generality and ease of use.  This step is in progress, but is    #
# not complete, and therefore functions are not included in this script.          #
###################################################################################

meanNA <- function(x=0){return(mean(x,na.rm=TRUE))}

library(Rwave)

#channels <- 1:28

# Set number of octaves and voices per octave for the cwt
octaves <- 8
voices <- 16
#subnums <- 101;
subnum <- Sys.getenv("R_SID_SUBJ_NO")
trialType <- Sys.getenv("R_SID_TRIAL_TYPE") 


#for(subnum in subnums){
#        for(trialType in c("FB")){
                # load the appropriate data
                #load(paste(subnum, "_", trialType, "-epochs.Rdata", sep=""))
		load(paste(subnum, "_.", trialType,sep=""))
                
		for(channel in 1:dim(typedEpochs)[2]){

                        #print(paste("Analyzing subject", subnum, "trial type", trialType, "channel", channel, "at", date()))

                        # Initialize the array that will hold the results
                        results <- array(NA, dim=c(dim(typedEpochs)[1], octaves * voices, dim(typedEpochs)[3]))

                        for(epoch in 1:dim(typedEpochs)[3]){
                                # Run the cwt for this epoch
                                results[,, epoch] <- abs(cwt(typedEpochs[,channel,epoch], noctave=octaves, nvoice=voices, plot=FALSE))
                        }

                        # Save this chunk of the results with appropriate filename
                        #save(results, file=paste(subnum, "-", trialType, "channel", channel, "_cwt-results.Rdata", sep=""))
			

			avgResults <- apply(results,c(1,2), meanNA);
                        save(avgResults, file=paste(subnum, "-", trialType, "channel", channel, "_cwt-avgResults.Rdata", sep=""))
                }
#        }
#}


