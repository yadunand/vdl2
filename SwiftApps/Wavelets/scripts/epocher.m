
number = 101;
strnumber = int2str(number);
original= importdata (['rh1.' strnumber ' cropped.dat']);

%cut cut channels 28, 30, 31, 32, 33. make channel 29 the new channel 28.
original(:,28) = original(:,29);
i=33;
while i>=29
    original(:,i)=[];
    i=i-1;
end

%import event file
raw=importdata(['rh1.' strnumber '.ev2']); 
preev=raw.data;

%set time range for epoch pre and post event (ms) 
prems = 500;
postms = 750;

%set samplingrate (hz)
samplingrate = 250;

spms = 1000/samplingrate;
pre = round(prems/spms);
post = round(postms/spms);

%report epoch size
roundedepochsize = ['-' int2str(pre*spms) 'ms to ' int2str(post*spms) 'ms']

%% filter event file to select trial onset, latency, put in new "ev" matrix
ev=[];
i=1;
k=1;
preevsize=size(preev);
while k<=preevsize(1,1);
    if preev(k,2)>200;
        ev(i,1)=preev(k,2);             
        ev(i,2)=preev(k,6);
        i=i+1;
    else
    end
    k=k+1;
end
x=size(original);
pointsperepoch=pre+post;

%number of channels (1->horizon) to include in new epoch. x(1,2) = all 28.
horizon=x(1,2);
new=[];
k=1;
i=1;
hrunner=1;
evsize = size(ev);

%create new matrix of epoched data
while k<=evsize(1,1)
    evlatency = ev(k,2);
    hrunner=1;
    while hrunner<=horizon
        i=1;
        while i<=pointsperepoch
            new(i,hrunner, k)= original(evlatency-pre+i-1,hrunner);
            i=i+1;
        end
        hrunner=hrunner+1;
    end
    k=k+1;
end

%subtract mean of each channel (over epoch) from each epoch, put in newer
%matrix
newer=[];
k=1;
baseline=mean(new);
while k<=evsize(1,1)
    hrunner=1;
    while hrunner<=horizon
        i=1;
        while i<=pointsperepoch      
            newer(i, hrunner, k)= new(i,hrunner,k)-baseline(1,hrunner,k);
            i=i+1;
        end
        hrunner=hrunner+1;
    end
    k=k+1;
end
new=[];
%transpose for compatibility
k=1;
while k<=evsize(1,1)
    hrunner=1;
    while hrunner<=horizon
        i=1;
        while i<=pointsperepoch      
            newest(hrunner, i, k)= newer(i,hrunner,k);
            i=i+1;
        end
        hrunner=hrunner+1;
    end
    k=k+1;
end
%report size
sizeofresult = size(newest)
%save
save(['' strnumber 'data.mat'], 'newest')




           