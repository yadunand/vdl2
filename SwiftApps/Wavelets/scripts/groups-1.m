%%%%%%%%%%%%%%%%%%%%%%MatrixMF_new function is called by this code%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%a0_r3_matrixes;
%v=linspace(-2.44,4.9103*2,45);         %consumption grid
clear all
load grid;%Obtain the grids
load param;%Obtain the parameters
load iteration;%Obtains the current iteration in the dynnamic problem.
v=grid;
vv1=kron(ones(1,sg1),v);
vv2=kron(v,ones(1,sg1));
a1=[effs];                   %effort 1 levels
a2=[effs];                   %effort 2 levels
q1=[2 20];                  %output 1 levels
q2=[2 20];                  %output 2 levels

sg=length(v)^2;

%load 0results_interin_4  % Obtains the surplus function for the interim stage 
load output.csv;
S_interin=output;
load solutions_index.csv;
index_i_r=[solutions_index zeros(5,681)] ; %684=1035-351
load solutions_prob.csv;
prob_i_r=[solutions_prob zeros(5,684)];


% Elements for each state.
v1v1=kron(ones(1,16*1),vv1);%Interim utility of individual 1.
v2v2=kron(ones(1,16*1),vv2);%Interim utility of individual 2.
ss=kron(ones(1,16*1),S_interin(:)');%Interim stage surplus
q1q1=kron(ones(1,8*1),kron([2 20],ones(1,sg)));%Output of individual 1.
q2q2=kron(ones(1,4*1),kron([2 20],ones(1,2*sg)));%Output of individual 2.
a1a1=kron(ones(1,2),kron([effs],ones(1,4*sg)));%Effort of individual 1
a2a2=kron(ones(1,1),kron([effs],ones(1,8*sg)));%Effort of individual 2

load tech  %Obtains the technological matrix  P     
    
V=[effs(1) effs(2) effs(1) effs(2); effs(1) effs(1) effs(2) effs(2)].^0.5;% 

CO1=sparse(kron(sparse(eye(1*16)),ones(1,sg))); %left side
P1=P';PP=P1(:);clear P1                %transforms the probability matrix in a vector.
AA=sparse(kron(eye(4),[1;1;1;1;]).*kron(PP,[1 1 1 1]));
BB=kron(AA,ones(1,sg*4));
CO2=kron(sparse(eye(1)),BB);
CO=CO2-CO1;
clear AA BB PP CO1 CO2;

%Probability equality constraint
COe=ones(size(v1v1));

%Upper and lower bounds
LO=zeros(size(v1v1));
UP=ones(size(v1v1));

L=linspace(0,1,101);
W=[linspace(-4,81.1111,15) linspace(83,125,10) linspace(130,185,27)];

d=0;

UTIL1=zeros(101,52);
UTIL2=zeros(101,52);
ST=zeros(101,52);
Ind_g=zeros(10,101*52);
Pr_g=zeros(10,101*52);
    
obj=q1q1+q2q2+ss;

% setting the arguments for linprogr

%inequality matrix
EQ=[CO;COe];            %equality matrix
beq=[zeros(16,1);1];  %equality intercept
%for d1=1:101
	for d1=1:1
% REPLACE THE LINE BELOW, DO INLINE FUNCTION
%    IN=[matrixMF_new(L(d1));-obj];  

%%%%%%%%%% THIS IS THE INLINED matrixMF_new fuction %%%%%%%%%%%%

lambda=L(d1);
clear MF
v=grid;
vv1=kron(ones(1,sg1),v);%%%%%%%%%%%NEW
vv2=kron(v,ones(1,sg1));%%%%%%%%%%%%NEW
a1=[effs];                   %effort 1 levels
a2=[effs];                   %effort 2 levels
q1=[2 20];                  %output 1 levels
q2=[2 20];                  %output 2 levels
%mi=linspace(0,1,101);       %mi grid

sg=length(v)^2;

%load 0results_i_g
% Elements for each state.
v1v1=kron(ones(1,16*1),vv1);
v2v2=kron(ones(1,16*1),vv2);
%vv_ind=kron(ones(1,16*1),[1:sg]);
q1q1=kron(ones(1,8*1),kron([2 20],ones(1,sg)));
q2q2=kron(ones(1,4*1),kron([2 20],ones(1,2*sg)));
a1a1=kron(ones(1,2),kron([effs],ones(1,4*sg)));
a2a2=kron(ones(1,1),kron([effs],ones(1,8*sg)));

%utility with weights given by mi

ff=v1v1*lambda+(1-lambda)*v2v2;                   % no effort
f=v1v1*lambda+(1-lambda)*v2v2-lambda*(a1a1.^0.5)-(1-lambda)*(a2a2.^0.5);

load tech

V=[effs(1) effs(2) effs(1) effs(2); effs(1) effs(1) effs(2) effs(2)].^0.5;%[2 6 2 6; 2 2 6 6].^0.5;

M1=sparse(blkdiag(P(1,:).^(-1),P(2,:).^(-1),P(3,:).^(-1),P(4,:).^(-1)));
Palt=[P(2,:);P(3,:);P(4,:);P(1,:);P(3,:);P(4,:)...
        ;P(1,:);P(2,:);P(4,:);P(1,:);P(2,:);P(3,:)];
M2=kron(sparse(eye(1)),kron(kron(M1,[1;1;1]).*...
    [Palt Palt Palt Palt],ones(1,sg)));;

count=1;
I=sparse(eye(4));
MI=sparse(eye(1));
PR=sparse(kron(kron(kron(I,MI),ones(1,sg*4)),[1;1;1]));

eff=[effs(2) effs(1); effs(1) effs(2);effs(2) effs(2);effs(1) effs(1);effs(1) effs(2);effs(2) effs(2);effs(1) effs(1);
effs(2) effs(1);effs(2) effs(2);effs(1) effs(1);effs(2) effs(1);effs(1) effs(2)];

count=1;


    for c2=1:12;
        %MF(count,:)=sparse(M2(count,:).*(ff-lambda*(eff(c2,1)^0.5)-(1-lambda)*(eff(c2,2)^0.5)))-sparse(PR(count,:).*f);

	thispart=M2(count,:).*((ff-lambda*(eff(c2,1)^0.5))-(1-lambda)*(eff(c2,2)^0.5));
	thatpart=PR(count,:).*f;

	MF(count,:)=full(thispart-thatpart);

        count=count+1;
    end

%disp("PRINTING MF")
%MF

IN=[MF;-obj];

%%%%%%%%%%%%% END OF MATRIX_MF_NEW.M inlined %%%%%%%%%%%%%%



    fob=L(d1)*(v1v1-a1a1.^0.5)+(1-L(d1))*(v2v2-a2a2.^0.5);
    
	for d2=1:1
	%for d2=1:52
		d=d+1;
		b=[zeros(12,1);-W(d2)];  %inequality intercept
		%linear programming

		%[vobj,X,lambda,status,colstat,it]=lpcplex(-fob,[IN;EQ],[b;beq],LO,UP,[1:length(b)],[],15000);%Linear programming
		% Print out the parameters for the linear program

		disp(" WE ARE DONE")
		% INTRODUCE CLP as the alternate solver

		%continued original code
%		Sup(d)=vobj;
%
%		%Storing results
%		UTIL1(d1,d2)=[(v1v1-a1a1.^0.5)]*X;
%		UTIL2(d1,d2)=[(v2v2-a2a2.^0.5)]*X;
%		ST(d1,d2)=status;
%		k=find(X>0.00001);
%		if status==1;
%			Ind_g(1:length(k),d)=k;
%			Pr_g(1:length(k),d)=X(k);
%		end
%		status
%		if status>1;
%			UTIL1(d1,d2)=-20;
%			UTIL2(d1,d2)=-20; 
%		end
%		load iteration;
%		[d1 d2 iteration]
%		%results have been stored.
	end
end

%save a0groups_results_5 UTIL1 UTIL2 ST Pr_g Ind_g
%save a0groups_results_4 UTIL1 UTIL2 ST 
%Boundaries
%save a0groups_results_4 UTIL1 UTIL2 ST Pr_g Ind_g W
