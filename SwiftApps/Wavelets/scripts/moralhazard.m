clear all
%beta=0.5;
%beta=0.5;
%mu=0.3;
%load Sup_total_3;
load iteration
load limits
load Surplus
load param
Sup_total_3=Surplus(:,:,iteration-1);
clear Surplus
for a1=1:40;
    for a2=1:40
        if Sup_total_3==-500
            Sup_total_3=-150;
        end 
    end
end

%v=linspace(-2.44,4.9103,30); 
v=linspace(ll_p(iteration),lu_p(iteration),30);
%u=linspace(-2.44,4.9103*2,45); 
%u=linspace(0,(1.22+7)^0.5,45); u=u.^2; u=u-1.22; 
u=linspace(0,(lu_a(iteration)-ll_a(iteration))^0.5,sg1); u=u.^2; u=u+ll_a(iteration); 

%utility grids
grid_regimes=linspace(ll_p(iteration),lu_p(iteration),40);
grid_total=linspace(ll_p(iteration),lu_p(iteration),30);                       
grid=u;
save grid grid grid_regimes grid_total
clear ll_p ll_a lu_p lu_a iteration;                     
                       
%c1=[0:25];
%c1=linspace(0,40,26);c2=c1;
%c1=[linspace(0,9,10) linspace(10,40,16)];c2=c1;
%c2=[0:25];
%%%VERSION 13 of the code%%%%%%c2=[linspace(0,7,7) linspace(9,19,5) linspace(21,40,6)]; c1=c2;
c2=[linspace(0,8,13) linspace(9,23,4) 45]; c1=c2;
cons=c2;save cons cons

c1c1=kron(ones(1,30*30*sg2),c1);
c2c2=kron(kron(ones(1,30*30),c2),ones(1,sg2));

ww1=kron(kron(ones(1,30),v),ones(1,sg2*sg2));
iw1=kron(kron(ones(1,30),[1:30]),ones(1,sg2*sg2));

ww2=kron(v,ones(1,30*sg2*sg2));
iw2=kron([1:30],ones(1,30*sg2*sg2));

ss=kron(Sup_total_3(:)',ones(1,sg2*sg2));

LO=zeros(size(ss));
UP=ones(size(ss));
S_interin=zeros(sg1,sg1);%%%%%%%%%%%%NEW
u1_c_r=zeros(30);
u2_c_r=zeros(30);

obj=-c1c1-c2c2+beta*ss;

prob_i_r=zeros(5,1035);
index_i_r=zeros(5,1035);

count=0;

EQ=[(c1c1.^ra+ww1*beta);(c2c2.^ra+ww2*beta);-ones(size(ss))];


%%%%%%%%%%%%% start setting up the lp_problem %%%%%%%%%%%%%%%%%%%%%

[lp_solve_m,lp_solve_n] = size(EQ);
lp_handle = octlpsolve('make_lp', lp_solve_m, lp_solve_n);
octlpsolve('set_verbose', lp_handle, 3);
octlpsolve('set_mat', lp_handle, [-EQ]);

octlpsolve('set_obj_fn', lp_handle, -obj);
octlpsolve('set_maxim', lp_handle); % default is solving minimum lp.

%make all constraints to be equalities, replace the 3 with length(b) - defined later
lp_ineq_val=ones(1,3)*0;

for i = 1:length(lp_ineq_val)
  if lp_ineq_val(i) < 0
        con_type = 1;
  elseif lp_ineq_val(i) == 0
        con_type = 3;
  else
        con_type = 2;
  end
  octlpsolve('set_constr_type', lp_handle, i, con_type);
end

  for i = 1:length(LO)
    octlpsolve('set_lowbo', lp_handle, i, LO(i));
  end

  for i = 1:length(UP)
    octlpsolve('set_upbo', lp_handle, i, UP(i));
  end

%%%%%%%%% end setting up the common part of the LP problem %%%%%%%%%%%%%%%%%%

for j=1:sg1;   %NEW
	for s=1:j;
		
		% move this in front of the loop, by Tibi. It seems not to depend on j and s
		% EQ=[(c1c1.^ra+ww1*beta);(c2c2.^ra+ww2*beta);-ones(size(ss))];
		count=count+1;
		b=[u(j);u(s);-1];
		load iteration
%		[j s iteration]
		%IN=[]
		
%		[vobj,X,lambda,status,colstat,it]=lpcplex(-obj,[-EQ],[-b],LO,UP,[],[],1500000);
%		save("-text",[int2str(j),"_",int2str(s),"_objective.txt"],"obj");
%		save("-text",[int2str(j),"_",int2str(s),"_A.txt"],"EQ");
%		save("-text",[int2str(j),"_",int2str(s),"_b.txt"],"b");
		
		%%%%%%% continue with seeting up the changing part of the dynamic problem %%%%
		octlpsolve('set_rh_vec', lp_handle, [-b]);
		
		% save the MPS problem descriptions
		octlpsolve('write_mps',lp_handle,["moral_hazard_4.3",int2str(j),"_",int2str(s),".mps"]);
	
%		g=find(X>0.001);
%		index_i_r(1:length(g),count)=g;
%		prob_i_r(1:length(g),count)=X(g);
%		S_interin(j,s)=-vobj;
%		S_interin(s,j)=-vobj;
%		u1_c_r(j,s)=(c1c1.^ra+ww1*beta)*X;
%		u2_c_r(j,s)=(c2c2.^ra+ww2*beta)*X;
%		if status>1
%			S_interin(j,s)=-1000^7;
%			S_interin(s,j)=-1000^7;
%		end
%		[j s]
	end
end

%save 0results_interin_4 index_i_r prob_i_r S_interin

