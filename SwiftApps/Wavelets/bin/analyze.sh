# This function executes the runWavelets.R script and 
# it packages the output into a file $2
# NOTE: DEPENDENCY: The output files from the runWavelets scripts are 
# hard-coded into this file

#!/bin/bash

export R_LIBS=/home/tiberius/local/SIDGrid/lib
#export R_SID_SUBJ_NO=$2
export R_SID_CHANNEL=$2
export R_SID_TRIAL_TYPE=$3
#export R_BRIC_PERM_DATA=$4
/app/R/bin/R CMD BATCH --vanilla $1

#/bin/tar zcvf $4 *_cwt-results.Rdata
