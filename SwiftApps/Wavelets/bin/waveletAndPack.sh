# This function executes the runWavelets.R script and 
# it packages the output into a file $2
# NOTE: DEPENDENCY: The output files from the runWavelets scripts are 
# hard-coded into this file

#!/bin/bash

export R_LIBS=/home/tiberius/local/SIDGrid/lib
#export R_SID_OUTPUT=$2
#export R_BRIC_ALL_DATA=$3
#export R_BRIC_PERM_DATA=$4
/home/tiberius/local/R-2.4.0/bin/R CMD BATCH --vanilla $1

/bin/tar zcvf $2 *_cwt-results.Rdata
