#! /bin/bash

reallist=$1                   # full pathname of list of realizations to pfrepp
                              #   reallist must have format:
                              #   path id (with this header line)

outdir=$2                     # full pathname of output dir

                              # scriptlet to filter lists of generated pfrepp scripts to run:
scriptfilter=${3:-"grep pfrepp_script.atmos_ | grep -v atmos_daily"}

runtype=${4:-dryrun}          # User should specify dryrun | realrun for this arg

if [ $runtype = dryrun ]; then
  dryrun=true
else # dryrun=anything else requests generated scripts to be run ("real run")
  dryrun=false
fi

bindir=$(dirname $0)
bindir=$(cd $bindir; pwd)

echo bindir=$bindir

# Create new runNNN directory below current working dir where this script was run from

rundir=$( echo run??? | sed -e 's/^.*run//' | awk '{ printf("run%03d\n", $1+1)}' )
mkdir $rundir

cat >$rundir/cf <<END

wrapperlog.always.transfer=true
sitedir.keep=true
execution.retries=0
lazy.errors=false
use.wrapper.staging=false

END

cat >$rundir/sites.xml <<END

<config>

   <pool handle="local">
     <execution provider="local" />
     <profile namespace="karajan" key="jobThrottle">.01</profile>
     <profile namespace="karajan" key="initialScore">10000</profile>
     <filesystem provider="local"/>
     <workdirectory>$PWD/$rundir</workdirectory>
   </pool>

  <pool handle="cobalt">
    <execution provider="coaster" jobmanager="local:cobalt" url="eureka.alcf.anl.gov" />
    <filesystem provider="local" />
    <!-- <profile namespace="globus"key="internalHostname">172.17.3.11</profile> -->
    <profile namespace="globus"  key="project">prec_sense</profile>
    <!-- <profile namespace="globus"  key="project">prec_sense</profile> -->
    <profile namespace="globus"  key="queue">default</profile>
    <profile namespace="karajan" key="jobthrottle">2.56</profile>
    <profile namespace="karajan" key="initialScore">10000</profile>
    <profile namespace="globus"  key="jobsPerNode">8</profile>
    <!-- <profile namespace="globus"  key="workerLoggingLevel">DEBUG</profile> -->
    <!-- <profile namespace="globus"  key="workerLoggingDirectory">/home/jonmon/Workspace/Swift/tests/catsnsleep</profile> -->
    <profile namespace="globus"  key="slots">32</profile>
    <profile namespace="globus"  key="maxTime">28800</profile> <!-- 8hrs -->
    <profile namespace="globus"  key="maxWallTime">07:45:00</profile>
    <profile namespace="globus"  key="lowoverallocation">100</profile>
    <profile namespace="globus"  key="highoverallocation">100</profile>
    <profile namespace="globus"  key="nodeGranularity">1</profile>
    <profile namespace="globus"  key="maxNodes">1</profile>
    <workdirectory>$PWD/$rundir</workdirectory>
  </pool>

</config>

END

cat >$rundir/tc <<END

local   genpfrepps  $bindir/genpfrepps.sh
cobalt  sh          /bin/sh

END

cp $1 $bindir/pfrepps.swift $rundir

cd $rundir
echo Running in directory $rundir
runid=$(basename $rundir)

echo RUNNING: swift -config cf -tc.file tc -sites.file sites.xml pfrepps.swift \
      -outdir=$outdir \
      -reallist=$reallist \
      -scriptfilter="$scriptfilter" \
      -dryrun=$dryrun 

swift -config cf -tc.file tc -sites.file sites.xml pfrepps.swift >& swift.out \
      -outdir=$outdir \
      -reallist=$reallist \
      -scriptfilter="$scriptfilter" \
      -dryrun=$dryrun 
