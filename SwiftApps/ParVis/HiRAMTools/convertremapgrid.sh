#!/bin/sh

# This script is designed to remap our low resolution remap grid to our target remap grid.
cd C90_to_360x180

/gpfs/home/ckerr/bin/fms/tools/x86_64/bin/fregrid.exe --input_mosaic /intrepid-fs0/users/lzamboni/persistent/bin/fms/pfrepp/remap_file/version1_C90_to_180x90/C90_mosaic.nc  --interp_method conserve_order2 --nlon 360 --nlat 180 --remap_file C90_to_360x180_remap_file.nc  

