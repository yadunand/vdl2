
type file;

string outdir=@arg("outdir","NO-OUTDIR-SPECIFIED");
string groupfile=@arg("groupfile","groups");

app (file sout, file serr) combine (string dir, string pattern)
{
  combine dir pattern stdout=@sout stderr=@serr;
}

string group[] = readData(groupfile);

foreach g, i in group {
  file so<single_file_mapper; file=@strcat("stdlog/stdout.",i)>;
  file se<single_file_mapper; file=@strcat("stdlog/stderr.",i)>;
  (so,se) = combine(outdir,g);
}
