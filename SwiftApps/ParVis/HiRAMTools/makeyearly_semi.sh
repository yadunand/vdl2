#! /bin/bash

# Create a fast (local-filesystem) working dir 

if mkdir -p /scratch/wilde; then
  scratch=/scratch/wilde
elif mkdir -p $HOME/scratch; then
  scratch=$HOME/scratch
else
  echo $0: cant create scratch dir
  exit 11
fi

tmp=$(mktemp -d $scratch/makeyearly_XXXXXX)
startdir=$PWD
cd $tmp

echo running in $PWD

qtrdec1=$1  # 1st quarter-decade dir
qtrdec2=$2  # 2nd quarter-decade dir
qtrdec3=$3  # 2nd quarter-decade dir
destbase=$4 # base dir for output years

echo destbase=$destbase qtrdec1=$1 qtrdec2=$2 qtrdec3=$3

# Add cdo and nco tools to PATH and Intel compiler v11 libs to LD_LIBRARY_PATH

#export PATH=/soft/apps/cdo-1.4.7/bin:/soft/apps/nco-4.0.9/bin:$PATH
export PATH=/soft/apps/nco-4.0.9/bin:$PATH
export LD_LIBRARY_PATH=/soft/apps/intel-fc-11.0.074/lib/intel64:/soft/apps/intel-cc-11.0.074/lib/intel64:/soft/apps/netcdf-3.6.3/lib:/usr/lib:/usr/X11R6/lib:/usr/local/lib

fname=$(basename $qtrdec1 .nc)
#file 2 should always be the target year
firstyear=$(basename $(dirname $qtrdec2) | sed -e 's/....$//')

# process only the time-based files

if echo $fname | egrep -v 'grid_spec|scalar'; then

  # Merge 2 quarter-decade files into one half-decade file
#for simplicity at this point, keep the script the same

  ncrcat $qtrdec1 $qtrdec2 $qtrdec3 theyears.nc

  # Extract each year from the half-decade file

  ncks -d time,"${firstyear}-01-01 00:00:00","${firstyear}-12-31 23:59:59" theyears.nc  $firstyear.nc
  

fi  

# Copy the years from temporary to permanent filesystem


  destdir=$destbase/${firstyear}0101
  mkdir -p $destdir
  if echo $fname | egrep -v 'grid_spec|scalar'; then
    dd if=$firstyear.nc of=$destdir/$fname.nc bs=8M
  else
    dd if=$qtrdec1 of=$destdir/$fname.nc bs=8M
  fi


rm -rf $tmp

# Delete when this works:

  # Old method:
  #
  # cdo mergetime $qtrdec1 $qtrdec2 ${fname}.nc
  # cdo splityear ${fname}.nc ${fname}.
  # Yields, eg, from atmos_month.tile1:
  # atmos_month.tile1.1950.nc
  # atmos_month.tile1.1951.nc
  # atmos_month.tile1.1952.nc
  # atmos_month.tile1.1953.nc
  # atmos_month.tile1.1954.nc

  # New method:
  #
  # ncrcat input1.nc input2.nc 5years.nc 
  # ncks -d time,"$yyyy-01-01 00:00:00","$yyyy-12-31 23:59:59" 5years.nc  year1.nc

