#!/bin/sh

# $1 should be name of the realization

PATHTODIR="/intrepid-fs0/projects/SU_Climate/post/data/pfrepp/"
PATHTODIR+=$1
PATHTODIR+=/atmos/av/annual_1year

PATHTOTARGETDIR="/intrepid-fs0/projects/SU_Climate/post/data/averages/"
PATHTOTARGETDIR+=$1
PATHTOTARGETFILE="$PATHTOTARGETDIR/atmos.1979-1999.ann.nc"

echo $PATHTODIR
echo $PATHTOTARGETDIR
echo $PATHTOTARGETFILE
#build the target directory if needed.
if [ ! -d "$PATHTOTARGETDIR" ]; then
	mkdir $PATHTOTARGETDIR
fi

if [ -f "$PATHTOTARGETFILE" ]; then
	rm $PATHTOTARGETFILE
fi

cd $PATHTODIR

ncra -v precip atmos.1979.ann.nc atmos.1980.ann.nc atmos.1981.ann.nc atmos.1982.ann.nc atmos.1983.ann.nc atmos.1984.ann.nc atmos.1985.ann.nc atmos.1986.ann.nc atmos.1987.ann.nc atmos.1988.ann.nc atmos.1989.ann.nc atmos.1990.ann.nc atmos.1991.ann.nc atmos.1992.ann.nc atmos.1993.ann.nc atmos.1994.ann.nc atmos.1995.ann.nc atmos.1996.ann.nc atmos.1997.ann.nc atmos.1998.ann.nc atmos.1999.ann.nc "$PATHTOTARGETFILE"
