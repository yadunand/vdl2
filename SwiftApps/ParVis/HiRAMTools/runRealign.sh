#!/bin/sh

#SOURCEDIR is the directory where all of the history files are located
#SOURCEDIR=/intrepid-fs0/users/lzamboni/persistent/enstdIc2/history
SOURCEDIR=/intrepid-fs0/projects/SU_Climate/tlmoore/originals/enstdIc2/history/

#FILELISTDIR is a subdirectory in the SOURCEDIR that contains the filesnames
#FILELISTDIR=/intrepid-fs0/users/lzamboni/persistent/enstdIc2/history/19500101
FILELISTDIR=/intrepid-fs0/projects/SU_Climate/tlmoore/originals/enstdIc2/history/19500101

STARTYEAR=1950
ENDYEAR=2000
SUBYEARS[0]=0
SUBYEARS[1]=1

#TARGETDIR is the location new files will be created
TARGETDIR=/intrepid-fs0/projects/SU_Climate/tlmoore/processed/enstdIc2/history


#need to start with a list of files

cd $FILELISTDIR
TARGETFILENAMES=( )
for aFile in *
do
#	echo $aFile
	TARGETFILENAMES=("${TARGETFILENAMES[@]}" "${aFile}");
done
##echo "${TARGETFILENAMES}"
#let's organize years
YEARS=( )
while [ $((STARTYEAR)) -lt $((ENDYEAR)) ]
do
	YEARS=("${YEARS[@]}" "${STARTYEAR}");
	STARTYEAR=$[$STARTYEAR+1]
done

#validate starting info

#for aYear in ${YEARS[@]}
#do 
#   echo $aYear
#done

#for aName in ${TARGETFILENAMES[@]}
#do
#   echo $aName
#done

#for aSubyear in ${SUBYEARS[@]}
#do
#	echo $aSubyear
#done

 
# Here we loop through the possibilities to create our files

echo "starting"
for aYear in ${YEARS[@]}
do
	#echo "Doing ${aYear}"
	for aName in ${TARGETFILENAMES[@]}
	do
		#echo $aName
		for aSubyear in ${SUBYEARS[@]}
		do
			echo "${aYear} ${aName} ${aSubyear}"
			/home/tlmoore/Documents/realizationRealignScript.sh -y $aYear -s $SOURCEDIR -t $TARGETDIR -f $aName -p $aSubyear
		done		
	done

done 
