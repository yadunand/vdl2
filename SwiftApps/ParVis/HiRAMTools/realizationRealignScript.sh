#!/bin/sh

#set defaults
YEAR="1950"
WORKDIR=""
TARGETDIR=""
FILENAME=""
GROUP="0"

while getopts "hy:s:t:f:p:" opt; do
	case $opt in
	h)
		echo "reorganize realization output v. 0.1 help" >&2
		echo "options:" >&2
		echo "-h - help" >&2
		echo "-y - set the year, e.g. 1950. Must be between 1900-2999" >&2
		echo "-s - original history file directory.(e.g. /intrepid-fs0/users/lzamboni/persistent/enstdIc2/history)" >&2
		echo "-t - target directory - similar to the above, but a writable directory.)"  >&2
		echo "-f - filename without full path. (e.g. atmos_4xdaily.tile1.nc.0000)"  >&2
		echo "-p - 6 month period.  0 Jan-Jun, 1 Jul-Dec"  >&2
		exit 1
	;;
	\?)
		echo "Invalid option : -$OPTARG" >&2
		exit 1
	;;
	y) YEAR="$OPTARG";;
	s) WORKDIR="$OPTARG";;
	t) TARGETDIR="$OPTARG";;
	f) FILENAME="$OPTARG";;
	p) GROUP="$OPTARG";;
	esac
done

# Now we need to test the input options for validity
#strip trailing slashes if present
WORKDIR=`echo "${WORKDIR}" | sed -e "s/\/*$//" `
TARGETDIR=`echo "${TARGETDIR}" | sed -e "s/\/*$//" `

if [ ! -d "$WORKDIR" ]; then
 	echo "$WORKDIR does not appear to be a valid directory"
	exit 1        
fi
if [ ! -d "$TARGETDIR" ]; then
        echo "$TARGETDIR does not appear to be a valid directory"
        exit 1
fi

#echo ${WORKDIR}
#echo ${TARGETDIR}

#test for empty file name

LEN1=$(echo ${#FILENAME})
if [ "$LEN1" -lt "1" ]; then
	echo "Invalid file name"
	exit 1
fi

#GROUP should always be less than 2
if [ "$GROUP" -gt "1" ]; then
	echo "Group setting must be 0 or 1"
	exit 1
fi

if [ "$GROUP" -lt "0" ]; then
        echo "Group setting must be 0 or 1"
        exit 1
fi

#now we test the year.

if [ "$YEAR" -gt "2999" ]; then
	echo "This script only accepts years from 1900-2999"
	exit 1
fi

if [ "$YEAR" -lt "1900" ]; then
        echo "This script only accepts years from 1900-2999"
        exit 1
fi

#we have a valid year. 

PRIORYEAR=$(($YEAR-1))
NEXTYEAR=$(($YEAR+1))
 
#cd to working dir
cd $WORKDIR

#read a list of directories for our target year
DIRS=( )
for aDir in *
do
   if [[ "$aDir" == "$PRIORYEAR"* ]]; then
	DIRS=("${DIRS[@]}" "${aDir}")
   fi
   if [[ "$aDir" == "$YEAR"* ]]; then
        DIRS=("${DIRS[@]}" "${aDir}")
   fi
done	
#DIRS=`ls -d ${PRIORYEAR}*/`
#DIRS+=`ls -d ${YEAR}*/`

for aDir in ${DIRS[*]}
do
	echo "${aDir}"
done


TARGETFILE="$TARGETDIR/$YEAR"
if [ "$GROUP" = "0" ]; then
	STARTDATE="$YEAR-1-1 0:00:0.1"
	ENDDATE="$YEAR-7-1 0:00:0.0"
	TARGETFILE="${TARGETFILE}0101"
else
        STARTDATE="$YEAR-7-1 0:00:0.1"
        ENDDATE="$NEXTYEAR-1-1 0:00:0.0"
	TARGETFILE="${TARGETFILE}0701"
fi
echo $STARTDATE
echo $ENDDATE

#build a list of files we'll use
SOURCEFILES=""

for ASOURCEDIR in ${DIRS[*]} 
do
	#echo $ASOURCEDIR
	SOURCEFILES="$SOURCEFILES $WORKDIR/$ASOURCEDIR/$FILENAME"
done

#echo $SOURCEFILES
#echo $TARGETFILE

if [ ! -d "$TARGETFILE" ]; then
	mkdir $TARGETFILE
fi

TARGETFILE="$TARGETFILE/$FILENAME"

#echo $TARGETFILE

#if the target file already exists, let's remove it.

if [ -f "$TARGETFILE" ]
then
    echo "File already exists! Removing"
    rm -Rf $TARGETFILE 
fi

#at this point, we're ready to give things a shot at concatenation, or other testing.

cmd="ncrcat -d time,'${STARTDATE}','${ENDDATE}' ${SOURCEFILES} ${TARGETFILE}"
echo $cmd
eval $cmd
