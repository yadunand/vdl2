#!/bin/csh 

### Using modified  template!

set FMS = /home/lzamboni
set FMS = $HOME

# source $FMS/bin/fms/sys/environ.cshrc - expanded below and modified:

# set up netcdf-4.1.2.beta2 and nco-3.9.2

unlimit stacksize

# setenv PATH /soft/apps/cdo-1.4.7/bin:/soft/apps/nco-4.0.9/bin:/soft/apps/netcdf-4.1.2-beta2/bin:$PATH
# nco-3.9.2 for pfrepp

setenv PATH /soft/apps/nco-3.9.2/bin:/soft/apps/netcdf-4.1.2-beta2/bin:$PATH

setenv LIBRARY_PATH /soft/apps/netcdf-4.1.2-beta2/lib:/soft/apps/intel-fc-11.0.074/lib/intel64:/soft/apps/intel-cc-11.0.074/lib/intel64:/usr/lib:/usr/X11R6/lib:/usr/local/lib
setenv LD_LIBRARY_PATH $LIBRARY_PATH
setenv CPATH /soft/apps/netcdf-4.1.2-beta2/include
setenv FPATH /soft/apps/netcdf-4.1.2-beta2/include

# end of modified environ.cshrc 

set GCP = /bin/cp

set init_clock = `date +%s`

set history_dir = AAAAAAA
set starttime   = BBBBBBB
set inputmosaic = CCCCCCC
set nlon        = DDDDDDD
set nlat        = EEEEEEE
set remapfile   = FFFFFFF
set numyears    = GGGGGGG
set numtiles    = HHHHHHH
set component   = IIIIIII
set FREGRID     = JJJJJJJ 
set nccombine   = LLLLLLL
set TIMEAVG     = MMMMMMM
set PLEV        = NNNNNNN
set zinterp     = OOOOOOO
set removetmp   = PPPPPPP
set MERGE       = QQQQQQQ
set nummonths   = RRRRRRR
set filename    = SSSSSSS
set mode        = TTTTTTT
set root_ppdir  = UUUUUUU
set use_bbcp    = VVVVVVV
set interp_method = WWWWWWW
set MONTH_IN_A_YEAR = 12

#set scratch = /scratch/$USER
#set scratch = /intrepid-fs1/users/$USER/scratch
set scratch = /intrepid-fs0/users/$USER/scratch

mkdir -p $scratch

#tmpdir could be changed depending on system
#set tmpdir = /intrepid-fs0/users/$USER/scratch/pfrepp
 set tmpdir = `mktemp -d $scratch/pfrepp.XXXXXX`

echo pfrepp: running in tmpdir $tmpdir

# print out all the arguments
echo "*****     NOTE from pfrepp.csh: Below are the list of arguments     ******"
echo "**************************************************************************"
echo ""
echo "history_dir = $history_dir"
echo "starttime   = $starttime"
echo "inputmosaic = $inputmosaic"
echo "nlon        = $nlon"
echo "nlat        = $nlat"
echo "remapfile   = $remapfile"
echo "numyears    = $numyears"
echo "numtiles    = $numtiles"
echo "component   = $component"
echo "FREGRID     = $FREGRID"
echo "nccombine   = $nccombine"
echo "TIMEAVG     = $TIMEAVG"
echo "PLEV        = $PLEV"
echo "zinterp     = $zinterp"
echo "removetmp   = $removetmp"
echo "MERGE       = $MERGE"
echo "nummonths   = $nummonths"
echo "filename    = $filename"
echo "mode        = $mode"
echo "use_bbcp    = $use_bbcp"
echo ""
echo "**************************************************************************"

set yearname = `echo $starttime | awk '{printf("%04s", $1)}'`
set begindate = ${starttime}0101
set begindate_dir = $history_dir/$begindate
set workdir = $tmpdir/tmp.$component.$filename.$mode.$yearname
set mydir = $cwd

if( $mode != "time_series" && $mode != "time_average" ) then
   echo "Error from pfrepp_template.csh: mode should be time_series or time_arerage"
endif

# when inputmosaic is none, there will be no horizontal interpolation.
set do_horiz_interp = 1
if( $inputmosaic == "" ) set do_horiz_interp = 0
# when zinterp is "none", there will be no vertical interpolation.
set do_vert_interp = 1
if( $zinterp == "none" ) set do_vert_interp = 0
#make sure workdir does not exist
if( -e $workdir ) then
   echo "pfrepp: $workdir already exist, please remove this directory before running pfrepp"
   exit 1
endif
mkdir -p $workdir
if( $status != 0 ) then
   echo "Failed at make directory $workdir"
   exit 1
endif
cd $workdir

   #first copy the data to workdir
if($numyears != 0) then
   @ year = $starttime
   @ end_year = $starttime + $numyears - 1
   @ runningtime_cp = 0
   while ( $year <= $end_year )
      set yearname = `echo $year | awk '{printf("%04s", $1)}'`

      @ month = 1
      while ( $month <= ${MONTH_IN_A_YEAR} )
         set monthname = `echo $month | awk '{printf("%02s", $1)}'`
         set datename = ${yearname}${monthname}01
         set datedir = $history_dir/${datename}
         cd $datedir

         set filelist = (`ls -1 ${filename}.*nc`)
         cd $workdir
         foreach file ( $filelist)
            set clock1 = `date +%s`
            if( $use_bbcp == 1 ) then
               $GCP $datedir/$file ${datename}.$file
            else
              ln -s $datedir/$file ${datename}.$file
            endif
            set clock2 = `date +%s`
            @ runningtime_cp = ${runningtime_cp} + $clock2 - $clock1
            if( $status != 0 ) then
               echo "Failed at cp $datedir/$file ${datename}.$file"
               exit 1
            endif
         end
         @ month = $month + $nummonths
      end
      @ year = $year + 1
   end
   echo "pfrepp: the running time for cp timeseries is ${runningtime_cp} seconds"
endif

set name = `echo $filename | sed 's/_/ /' | awk '{printf $2}'`

if($numyears == 0) then
   set datedir = $history_dir/$starttime
   if( ! -e $datedir ) then
      #nccombine must be executed before pfrepp is executed.
      echo "directory $history_dir/$starttime does not exist, which means the data is combined"
      exit 1
   endif
   cd $datedir
   set filelist = (`ls -1 ${filename}.*nc`)   
   cd $workdir
   foreach file ( $filelist )
      if( $use_bbcp == 1 ) then
         $GCP $datedir/$file ${starttime}.$file
      else
         ln -s $datedir/$file ${starttime}.$file
      endif
      if( $status != 0 ) then
         echo "Failed at cp $datedir/$file ${starttime}.$file"
         exit 1
      endif
   end
   #timeseries
   set ppdir = $root_ppdir/${component}/ts/$name
   if( ! -e $ppdir ) mkdir -p $ppdir
   if( $do_vert_interp == 0 ) then
      set output_file = ${starttime}.$filename
   else
      set output_file = ${component}.${starttime}
      set clock1 = `date +%s`
      $PLEV -a -i ${starttime}.$filename -o ${output_file} -n $numtiles
      if( $status != 0 ) then
        echo "Failed at running $PLEV -a -i ${starttime}.$filename -o ${output_file}  -n $numtiles"
        exit 1
      endif
      set clock2 = `date +%s`
      @ runningtime = $clock2 - $clock1
      echo "pfrepp: the running time for PLEV is $runningtime seconds"
   endif

   if( $do_horiz_interp == 0 ) then
      set fregrid_out = ${output_file}.nc
   else
      set fregrid_out = $filename.${starttime}.nc
      set clock1 = `date +%s`
      $FREGRID -a ${output_file} -b $fregrid_out -g $inputmosaic -n $nlon -m $nlat -r $remapfile -p ${interp_method}
      if( $status != 0 ) then
         echo "Failed at running $FREGRID -a plev.$filename.${starttime} -b $fregrid_out -g $inputmosaic -n $nlon -m $nlat -r $remapfile -p ${interp_method}"
         exit 1
      endif
      set clock2 = `date +%s`
      @ runningtime = $clock2 - $clock1
      echo "pfrepp: the running time for fregrid is $runningtime seconds"  
   endif
   # mv $fregrid_out $ppdir/${component}.${starttime}.nc
   foreach i(1 2 3 4 5 6 7 8 9 0)
     dd if=$fregrid_out of=$ppdir/${component}.${starttime}.nc bs=4M
     if ( $status == 0 ) then
        break
     endif
   end

else if( $mode == "time_average" ) then # numyears > 0, time average
   @ endtime = $starttime + $numyears - 1
   set start_year = `echo $starttime | awk '{printf("%04s", $1)}'`
   set end_year =  `echo $endtime | awk '{printf("%04s", $1)}'`

   #merge the files
   set clock1 = `date +%s`
   $MERGE -s $start_year -n $numyears -m $nummonths -c $filename -t $numtiles
   if( $status != 0 ) then
      echo "Failed at running $MERGE -a $history_dir -s $start_year -n $numyears -m $nummonths -c $filename -t $numtiles"
      exit 1
   endif
   set clock2 = `date +%s`
   @ runningtime = $clock2 - $clock1
   echo "pfrepp: the running time for merge is $runningtime seconds"      

   # timeAverage (monthly  averages)
   set clock1 = `date +%s`
   $TIMEAVG -s $starttime -f monthly -c $component -t $numtiles -n $numyears # the output will be in the format $starttime-$endtime.${component}.$month.tile#.nc.

   if( $status != 0 ) then
      echo "Failed at running $TIMEAVG -s $starttime -f monthly -c $component -t $numtiles -n $numyears"
      exit 1
   endif

   set clock2 = `date +%s`
   @ runningtime = $clock2 - $clock1
   echo "pfrepp: the running time for timeAverage monthly_${numyears}yr is $runningtime seconds"

   set ppdir = $root_ppdir/$component/av/monthly_${numyears}yr/
   if( ! -e $ppdir ) mkdir -p $ppdir
   #loop through 12 months
   @ month = 0
   @ runningtime_plev = 0
   @ runningtime_fregrid = 0
   while ( $month < $MONTH_IN_A_YEAR )
      @ month = $month + 1
      set monthname = `echo $month | awk '{printf("%02s", $1)}'`
      if( $do_vert_interp == 0 ) then
         set output_file = $start_year-$end_year.${component}.$monthname
      else
      	 set output_file = plev.${component}.$monthname
         set clock1 = `date +%s`
         $PLEV -a -i $start_year-$end_year.${component}.$monthname -o ${output_file}  -n $numtiles
         if( $status != 0 ) then
            echo "Failed at running $PLEV -a -i $start_year-$end_year.${component}.$monthname -o ${output_file} -n $numtiles"
            exit 1
         endif
         set clock2 = `date +%s`
         @ runningtime_plev = ${runningtime_plev} + $clock2 - $clock1
      endif

      if( $do_horiz_interp == 0 ) then
         set fregrid_out = ${output_file}.nc
      else
         set fregrid_out = ${component}.$start_year-$end_year.$monthname.nc
         set clock1 = `date +%s`
         $FREGRID -a ${output_file} -b $fregrid_out  -g $inputmosaic -n $nlon -m $nlat -r $remapfile -p ${interp_method}
         if( $status != 0 ) then
            echo "Failed at running $FREGRID -a ${output_file} -b $fregrid_out -g $inputmosaic -n $nlon -m $nlat -r $remapfile -p ${interp_method}"
            exit 1
         endif
         set clock2 = `date +%s`
         @ runningtime_fregrid = ${runningtime_fregrid} + $clock2 - $clock1
      endif
      
      #move data to pp directory.
#     mv ${fregrid_out} $ppdir/${component}.$start_year-$end_year.$monthname.nc
      foreach i(1 2 3 4 5 6 7 8 9 0)
        dd if=${fregrid_out} of=$ppdir/${component}.$start_year-$end_year.$monthname.nc bs=4M
        if ( $status == 0 ) then
          break
        endif
      end
   end

   echo "pfrepp: the running time for plev monthly_${numyears}yr is ${runningtime_plev} seconds"
   echo "pfrepp: the running time for fregrid monthly_${numyears}yr is ${runningtime_fregrid} seconds"

   # timeAverage (annual averages)
   # first check if the nanual average file exist or not.
   set clock1 = `date +%s`

   set ppdir = $root_ppdir/${component}/av/annual_1year
   if( ! -e $ppdir ) mkdir -p $ppdir
   @ year = $start_year - 1
   set annual_exist = 0
   while ( $year < $end_year )
      @ year = $year + 1
      set yearname = `echo $year | awk '{printf("%04s", $1)}'`
      set output_file = $ppdir/${component}.$yearname.ann.nc
      if( ${annual_exist} == 0 ) then
         if( -f $output_file ) then
            set annual_exist = 1
         endif
      else
         if( ! -f $output_file ) then
            echo "ERROR from pfrepp.csh: $output_file does not exist"
            exit 1
         endif
      endif
   end

   if( $annual_exist == 1 ) then
      echo "NOTE from pfrepp.csh: annual_1year already created"
   else
      set clock1 = `date +%s`
      $TIMEAVG -s $starttime -f annual_1year -c ${component} -t $numtiles -n $numyears # the output will be in the format $starttime-$endtime.${component}.$month.tile#.nc   
      if( $status != 0 ) then
         echo "Failed at running $TIMEAVG -s $starttime -f annual -c ${component} -t $numtiles -n $numyears"
         exit 1
      endif
      set clock2 = `date +%s`
      @ runningtime = $clock2 - $clock1
      echo "pfrepp: the running time for timeAverage annual is $runningtime seconds"
   
      @ year = $start_year - 1
      @ runningtime_plev = 0
      @ runningtime_fregrid = 0
   
      while ( $year < $end_year )
         @ year = $year + 1
         set yearname = `echo $year | awk '{printf("%04s", $1)}'`
         if( $do_vert_interp == 0 ) then
            set output_file = $yearname.${component}
         else
            set output_file =  plev.${component}.$yearname
            set clock1 = `date +%s`
            $PLEV -a -i $yearname.${component} -o ${output_file} -n $numtiles
            if( $status != 0 ) then
               echo "Failed at running $PLEV -a -i $yearname.${component} -o ${output_file} -n $numtiles"
               exit 1
            endif
            set clock2 = `date +%s`
            @ runningtime_plev = ${runningtime_plev} + $clock2 - $clock1
         endif

         if( $do_horiz_interp == 0 ) then
            set fregrid_out = ${output_file}.nc
         else
            set fregrid_out = ${component}.$yearname.ann.nc     
            set clock1 = `date +%s`
            $FREGRID -a ${output_file} -b $fregrid_out -g $inputmosaic -n $nlon -m $nlat -r $remapfile -p ${interp_method}
            if( $status != 0 ) then
               echo "Failed at running $FREGRID -a ${output_file} -b $fregrid_out -g $inputmosaic -n $nlon -m $nlat -r $remapfile -p ${interp_method}"
               exit 1
            endif
            set clock2 = `date +%s`
            @ runningtime_fregrid = ${runningtime_fregrid} + $clock2 - $clock1
         endif
      
         #move data to pp directory.
#        mv ${fregrid_out} $ppdir/${component}.$yearname.ann.nc
         foreach i(1 2 3 4 5 6 7 8 9 0)
           dd if=${fregrid_out} of=$ppdir/${component}.$yearname.ann.nc bs=4M
           if ( $status == 0 ) then
             break
           endif
         end
      end

      echo "pfrepp: the running time for plev annual is ${runningtime_plev} seconds"
      echo "pfrepp: the running time for fregrid annual is ${runningtime_fregrid} seconds"
   endif

   #timeAverage (annual $numyears averages), only when numyears != 1
   if( $numyears != 1 ) then
      set ppdir = $root_ppdir/${component}/av/annual_${numyears}year
      if( ! -e $ppdir ) mkdir -p $ppdir
      set clock1 = `date +%s`
      $TIMEAVG -s $starttime -f annual -i $root_ppdir/${component}/av/annual_1year -c ${component}  -t $numtiles -n ${numyears}
      if( $status != 0 ) then
         echo "Failed at running $TIMEAVG -s $starttime -f annual -i $root_ppdir/${component}/av/annual_1year -c ${component}  -t $numtiles -n ${numyears}"
         exit 1
      endif
      set clock2 = `date +%s`
      @ runningtime = $clock2 - $clock1
      echo "pfrepp: the running time for timeAverage annual_${numyears}year is $runningtime seconds"
   
#     mv ${component}.$start_year-$end_year.ann.nc $ppdir
      foreach i(1 2 3 4 5 6 7 8 9 0)
        dd if=${component}.$start_year-$end_year.ann.nc of=$ppdir/$start_year-$end_year.ann.nc bs=4M
        if ( $status == 0 ) then
          break
	endif
      end
   endif
else  # timeseries, numyears > 0 
   @ runningtime_plev = 0
   @ runningtime_fregrid = 0
   set ppdir = $root_ppdir/$component/ts/$name
   if( ! -e $ppdir ) mkdir -p $ppdir

   @ year = $starttime
   @ end_year = $starttime + $numyears - 1
   while ( $year <= $end_year ) 
      set yearname = `echo $year | awk '{printf("%04s", $1)}'`
	          
      @ month = 1
      while ( $month <= ${MONTH_IN_A_YEAR} )
         set monthname = `echo $month | awk '{printf("%02s", $1)}'`
         set datename = ${yearname}${monthname}01
         set datedir = $history_dir/${datename}

         if( $do_vert_interp == 0 ) then
            set output_file = ${datename}.$filename
         else
            set output_file = ${component}.${datename}
            set clock1 = `date +%s`
            $PLEV -a -i ${datename}.$filename -o ${output_file} -n $numtiles
            if( $status != 0 ) then
               echo "Failed at running $PLEV -a -i ${datename}.$filename -o ${output_file} -n $numtiles"
               exit 1
            endif
            set clock2 = `date +%s`
            @ runningtime_plev = ${runningtime_plev} + $clock2 - $clock1   
         endif

         if( $do_horiz_interp == 0 ) then
            set fregrid_out = ${output_file}.nc
         else
            set fregrid_out = $filename.${datename}.nc
            set clock1 = `date +%s`
            $FREGRID -a ${output_file} -b $fregrid_out -g $inputmosaic -n $nlon -m $nlat -r $remapfile -p ${interp_method}
            if( $status != 0 ) then
               echo "Failed at running $FREGRID -a ${output_file} -b $fregrid_out -g $inputmosaic -n $nlon -m $nlat -r $remapfile -p ${interp_method}"
               exit 1
            endif
            set clock2 = `date +%s`
            @ runningtime_fregrid = ${runningtime_fregrid} + $clock2 - $clock1
         endif
#        mv $fregrid_out $ppdir/${component}.${datename}.nc
         foreach i(1 2 3 4 5 6 7 8 9 0)
           dd if=$fregrid_out of=$ppdir/${component}.${datename}.nc bs=4M
	   if ( $status == 0 ) then
             break
	   endif
         end
         if( $status != 0 ) then
            echo "Failed at mv $fregrid_out $ppdir/${component}.${datename}.nc"
            exit 1
         endif

         @ month = $month + $nummonths
      end
      @ year = $year + 1
   end
   echo "pfrepp: the running time for plev timeseries  is ${runningtime_plev} seconds"
   echo "pfrepp: the running time for fregrid timeseries is ${runningtime_fregrid} seconds"
endif

set end_clock = `date +%s`
@ runningtime = $end_clock - $init_clock
echo "pfrepp: the total running time is $runningtime seconds"

if( $removetmp == 1 ) then
   cd $mydir
   rm -rf $workdir
endif

exit 0
