#! /bin/bash

realdir=$1                    # full pathname of realization dir
outdir=$2                     # full pathname of output dir
realname=$(basename $realdir) # realization name (eg en1eo12Ic1)
outdir=$outdir/$realname

bindir=$(dirname $0)
bindir=$(cd $bindir; pwd)

echo bindir=$bindir

# patterns for filename matching

yyyy="[0-9][0-9][0-9][0-9]"
yyyymmdd="${yyyy}[0-9][0-9][0-9][0-9]"

#find $realdir/history/$yyyymmdd  |
find $realdir/history/19????01  |
  grep .nc.$yyyy      |
  sed -e 's/.....$//' | sort | uniq > groups
echo Created $real.groups

# /bin/ls -1 -d ${hdir}* >dirs
# 
# echo '=== ' $(basename $0) doing dirs:
# cat dirs
# 
# for d in $(cat dirs); do
#   find $d | grep .nc.[0-9][0-9][0-9][0-9]  | sed -e 's/.....$//' | sort | uniq
# done >groups

# Create new runNNN directory

rundir=$( echo run??? | sed -e 's/^.*run//' | awk '{ printf("run%03d\n", $1+1)}' )
mkdir $rundir

cat >$rundir/cf <<END

wrapperlog.always.transfer=true
sitedir.keep=true
execution.retries=10
lazy.errors=true
use.wrapper.staging=false

END

cat >$rundir/sites.xml <<END

<config>

   <pool handle="local">
     <execution provider="local" />
     <profile namespace="karajan" key="jobThrottle">.01</profile>
     <profile namespace="karajan" key="initialScore">10000</profile>
     <filesystem provider="local"/>
     <workdirectory>$PWD/swiftwork</workdirectory>
   </pool>

  <pool handle="cobalt">
    <execution provider="coaster" jobmanager="local:cobalt" url="eureka.alcf.anl.gov" />
    <filesystem provider="local" />
    <!-- <profile namespace="globus"key="internalHostname">172.17.3.11</profile> -->
    <profile namespace="globus"  key="project">prec_sense</profile>
    <profile namespace="globus"  key="queue">default</profile>
    <profile namespace="karajan" key="jobthrottle">2.56</profile>
    <profile namespace="karajan" key="initialScore">10000</profile>
    <profile namespace="globus"  key="jobsPerNode">8</profile>
    <!-- <profile namespace="globus"  key="workerLoggingLevel">DEBUG</profile> -->
    <!-- <profile namespace="globus"  key="workerLoggingDirectory">/home/jonmon/Workspace/Swift/tests/catsnsleep</profile> -->
    <profile namespace="globus"  key="slots">32</profile>
    <profile namespace="globus"  key="maxTime">28000</profile> <!-- ~ 8hrs -->
    <profile namespace="globus"  key="maxWallTime">05:20:00</profile>
    <profile namespace="globus"  key="lowoverallocation">100</profile>
    <profile namespace="globus"  key="highoverallocation">100</profile>
    <profile namespace="globus"  key="nodeGranularity">1</profile>
    <profile namespace="globus"  key="maxNodes">1</profile>
    <workdirectory>$PWD/$rundir/swiftwork</workdirectory>
  </pool>

</config>

END

cat >$rundir/tc <<END

cobalt  combine $bindir/combine.sh

END

cp groups $bindir/combine.swift $rundir

cd $rundir
echo Running in directory $rundir
runid=$(basename $rundir)
mkdir -p $outdir/$runid

swift -config cf -tc.file tc -sites.file sites.xml combine.swift -outdir=$outdir/$runid -groupfile=groups >& swift.out


