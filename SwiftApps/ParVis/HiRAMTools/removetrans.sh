#!/bin/sh

for d in 19*
do
	echo $d
	cd $d
	rm -Rf *.trans
	cd ..
done
