#! /bin/bash

cdir=$1     # combined dir
outbase=$2  # base of outdir for equivalent yearly files

bindir=$(dirname $0)
bindir=$(cd $bindir; pwd)

echo bindir=$bindir

# Enable one of the following patterns to name the output dir:

outsuffix=$(basename $cdir)                              # cdir ends in rname, use rname
outsuffix=$(basename $(dirname $cdir))                   # cdir ends in rname/rundir, use rname
outsuffix=$(basename $(dirname $cdir))/$(basename $cdir) # cdir ends in rname/rundir, use rname/rundir
                                                         # Default assumes cdir is a rundir from combine script
                                                         # New rundir will be appended to end of outsuffix

STRIPPATH="\/done"
outsuffix=${outsuffix//$STRIPPATH/}
# generate the list of files to process: do every half-decade

echo f1 f2 f3  >ncfiles
#we need to step through every year and create out target file list
for y in $(seq 1950 1 1999); do

    startyear=${y}0101;

#All cases use 3 directories
#if 1950, then we have the following
    if [[ "$y" == "1950"* ]]; then
        timestep1=${y}0101;
        timestep2=${y}0201;
        timestep3=${y}0801;
    else
        PRIORYEAR=$(($y-1))
        timestep1=${PRIORYEAR}0801;
        timestep2=${y}0201;
        timestep3=${y}0801;
    fi

#  nextyear=$((y+1))0101

#this line does the following at sed
#    ^\\(.*\\)/$startyear/\\(.*\\), searches for everything before startyear and saves it to 1
#    \\(.*\\) searches for everything after startyear and saves as 2
# builds a new line with full path for first file, full path for second and targets the output directory
#  find $cdir/$startyear -type f | fgrep .nc | sed -e "s,^\\(.*\\)/$startyear/\\(.*\\),\\1/$startyear/\\2 \\1/$nextyear/\\2 $outdir,"

#in the 2 year version, we only want the first file
	  find $cdir/$timestep1 -type f | fgrep .nc | sed -e "s,^\\(.*\\)/$timestep1/\\(.*\\),\\1/$timestep1/\\2 \\1/$timestep2/\\2  \\1/$timestep3/\\2  $outdir,"

done >>ncfiles



#now we have a list of the 2 year files and the outdir

# Create new runNNN directory

rundir=$( echo run??? | sed -e 's/^.*run//' | awk '{ printf("run%03d\n", $1+1)}' )
mkdir $rundir

# Generate Swift config files (cf, tc, sites.xml):

cat >$rundir/cf <<END

wrapperlog.always.transfer=true
sitedir.keep=true
execution.retries=10
lazy.errors=true
status.mode=provider
use.wrapper.staging=false

END

cat >$rundir/sites.xml <<END

<config>

   <pool handle="local">
     <execution provider="local" />
     <profile namespace="karajan" key="jobThrottle">.01</profile>
     <profile namespace="karajan" key="initialScore">10000</profile>
     <filesystem provider="local"/>
     <workdirectory>$PWD/swiftwork</workdirectory>
   </pool>

  <pool handle="cobalt">
    <execution provider="coaster" jobmanager="local:cobalt" url="eureka.alcf.anl.gov" />
    <filesystem provider="local" />
    <!-- <profile namespace="globus"key="internalHostname">172.17.3.11</profile> -->
    <!-- <profile namespace="globus"  key="project">MTCScienceApps</profile> -->
    <profile namespace="globus"  key="project">prec_sense</profile>
    <profile namespace="globus"  key="queue">default</profile>
    <profile namespace="karajan" key="jobthrottle">2.56</profile>
    <profile namespace="karajan" key="initialScore">10000</profile>
    <profile namespace="globus"  key="jobsPerNode">8</profile>
    <!-- <profile namespace="globus"  key="workerLoggingLevel">DEBUG</profile> -->
    <!-- <profile namespace="globus"  key="workerLoggingDirectory">/home/jonmon/Workspace/Swift/tests/catsnsleep</profile> -->
    <profile namespace="globus"  key="slots">32</profile>
    <profile namespace="globus"  key="maxTime">28000</profile> <!-- ~ 8hrs -->
    <profile namespace="globus"  key="maxWallTime">00:20:00</profile>
    <profile namespace="globus"  key="lowoverallocation">100</profile>
    <profile namespace="globus"  key="highoverallocation">100</profile>
    <profile namespace="globus"  key="nodeGranularity">1</profile>
    <profile namespace="globus"  key="maxNodes">1</profile>
    <workdirectory>$PWD/$rundir/swiftwork</workdirectory>
  </pool>

</config>

END

cat >$rundir/tc <<END

cobalt makeyearly_semi $bindir/makeyearly_semi.sh

END

# Place file list and swift script in run dir, set params, make output dir, and run script

cp ncfiles $bindir/makeyearly_semi.swift $rundir

cd $rundir
echo Running in directory $rundir
runid=$(basename $rundir) # un-needed?
outdir=$outbase/$outsuffix/$runid
mkdir -p $outdir

swift -config cf -tc.file tc -sites.file sites.xml makeyearly_semi.swift -rundir=$outdir -ncfiles=ncfiles >& swift.out

