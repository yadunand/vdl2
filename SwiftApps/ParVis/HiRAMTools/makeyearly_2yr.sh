#! /bin/bash

# Create a fast (local-filesystem) working dir 

if mkdir -p /scratch/wilde; then
  scratch=/scratch/wilde
elif mkdir -p $HOME/scratch; then
  scratch=$HOME/scratch
else
  echo $0: cant create scratch dir
  exit 11
fi

tmp=$(mktemp -d $scratch/makeyearly_XXXXXX)
startdir=$PWD
cd $tmp

echo running in $PWD

# the following is not needed for a two-year case
#qtrdec1=$1  # 1st quarter-decade dir
#qtrdec2=$2  # 2nd quarter-decade dir

the2year=$1
destbase=$2 # base dir for output years

echo destbase=$destbase 

# Add cdo and nco tools to PATH and Intel compiler v11 libs to LD_LIBRARY_PATH

#export PATH=/soft/apps/cdo-1.4.7/bin:/soft/apps/nco-4.0.9/bin:$PATH
export PATH=/soft/apps/nco-4.0.9/bin:$PATH
export LD_LIBRARY_PATH=/soft/apps/intel-fc-11.0.074/lib/intel64:/soft/apps/intel-cc-11.0.074/lib/intel64:/soft/apps/netcdf-3.6.3/lib:/usr/lib:/usr/X11R6/lib:/usr/local/lib

fname=$(basename $the2year .nc)
firstyear=$(basename $(dirname $the2year) | sed -e 's/....$//')

# process only the time-based files
echo $fname
if echo $fname | egrep -v 'grid_spec|scalar'; then

#this script has proven unreliable for some reason.  I'm going to guess it's a fs problem
#this time attempt to cp the 2 year file to scratch

cp $the2year ./current2yearfile.nc
SOURCE_FILESIZE=`stat -c %s ./current2yearfile.nc`
echo "Source File Size: ${SOURCE_FILESIZE}"


  # Extract each year from the half-decade file
echo "processing ${fname}"
  for (( y=$firstyear; y<$firstyear+2; y++ )); do
    echo "Processing ${fname} for ${y}"
    ncks -d time,"${y}-01-01 00:00:00","${y}-12-31 23:59:59" current2yearfile.nc  $y.nc
    RESULT_FILESIZE==`stat -c %s ${y}.nc`
    echo "Result File Size for ${fname}: ${y} - ${RESULT_FILESIZE}"
  done

fi  

# Copy the years from temporary to permanent filesystem

for (( y=$firstyear; y<$firstyear+2; y++ ))

do
  destdir=$destbase/${y}0101
  mkdir -p $destdir
  if echo $fname | egrep -v 'grid_spec|scalar'; then
    dd if=$y.nc of=$destdir/$fname.nc bs=8M
  else
    dd if=$the2year of=$destdir/$fname.nc bs=8M
  fi
done

rm -rf $tmp

# Delete when this works:

  # Old method:
  #
  # cdo mergetime $qtrdec1 $qtrdec2 ${fname}.nc
  # cdo splityear ${fname}.nc ${fname}.
  # Yields, eg, from atmos_month.tile1:
  # atmos_month.tile1.1950.nc
  # atmos_month.tile1.1951.nc
  # atmos_month.tile1.1952.nc
  # atmos_month.tile1.1953.nc
  # atmos_month.tile1.1954.nc

  # New method:
  #
  # ncrcat input1.nc input2.nc 5years.nc 
  # ncks -d time,"$yyyy-01-01 00:00:00","$yyyy-12-31 23:59:59" 5years.nc  year1.nc


