#! /bin/bash

# src=/intrepid-fs0/users/lzamboni/persistent

src=/intrepid-fs0/users/wilde/persistent/LZ/combined
dest=$PWD

real=$1

if [ _$1 = _ ]; then
  echo realization to copy was not specified - exiting; exit 1
fi

if [ -e _$1 ]; then
  echo $1 already exits - exiting; exit 1
fi

cd $src/$real
dirs=$(find * -type d -print)

for d in $dirs; do
  mkdir -p $dest/$real/$d
  for f in $d/*.nc; do
    ln -s $PWD/$f $dest/$real/$f
  done
done
