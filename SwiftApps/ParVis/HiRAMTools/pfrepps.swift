type file;

type realization {
  string path;
  string id;
}

type script {
  string path;
  string id;
}

global string outdir =    @arg("outdir", "");
       string reallist =  @arg("reallist", "");
       string scriptfilter = @arg("scriptfilter","grep pfrepp_script.atmos_|grep -v atmos_daily.SKIPME");
       string dryrun = @arg("dryrun","true");  # default to doing a "dry run"

app (file scripts, file std_out, file std_err) genpfrepps (string realid, string realpath, string filter)
{
  genpfrepps realid realpath outdir @scripts filter stdout=@std_out stderr=@std_err;
}

app (file std_out, file std_err) sh (string script)
{
  sh "-c" script stdout=@std_out stderr=@std_err;
}

if(outdir == "") {
  tracef("pfrepps.swift: ERROR: output directory (swift argument outdir) not specified.");
}

if(reallist == "") {
  tracef("pfrepps.swift: ERROR: realization list (swift argument reallist) not specified");
}

if(reallist != "" && outdir != "") {
  realization reali[] = readData(reallist);
  foreach r,i in reali {
    tracef("%s %s\n", r.id, r.path);
    file sofile <single_file_mapper; file=@strcat(outdir,"/",r.id,".gen.out")>;
    file sefile <single_file_mapper; file=@strcat(outdir,"/",r.id,".gen.err")>;
    file scriptlist <single_file_mapper; file=@strcat(outdir,"/",r.id,"/scripts/scriptlist")>;
    (scriptlist,sofile,sefile) = genpfrepps(r.id, r.path, scriptfilter);
    script scripts[] = readData(scriptlist);
    foreach s in scripts {
      string cmd = @strcat(outdir,"/",r.id,"/scripts/",s.path);
      file ssofile <single_file_mapper; file=@strcat(s.path,".",r.id,".out")>;
      file ssefile <single_file_mapper; file=@strcat(s.path,".",r.id,".err")>;
      if( dryrun != "true" ) {
	(ssofile, ssefile) = sh(cmd);
      }
      else {
   	tracef("Running script: %s: %s\n", s.id, s.path);
      }
    }
  }
}
else {
  tracef("pfrepps.swift: EXIT: exiting due to argument errors.");
} 