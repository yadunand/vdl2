#! /bin/sh

realid=$1
realdir=$2
outdir=$3
scriptlist=$PWD/$4
scriptfilter=$5

mkdir -p $outdir/$realid/scripts
cd       $outdir/$realid/scripts

tcsh /home/wilde/LZ/HiRAMTools/pfrepp.csh  \
    -a $realdir \
    -b $outdir/$realid \
    -s 1950 \
    -i /intrepid-fs0/users/lzamboni/persistent/bin/fms/pfrepp/remap_file/version1_C90_to_180x90/C90_mosaic.nc \
    -x 360 -y 180  \
    -r /intrepid-fs0/projects/SU_Climate/post/HiRAMTools/C90_to_360x180/C90_to_360x180_remap_file.nc \
    -m 12 -n 5 -q \
    -u

echo $0: scriptfilter=$scriptfilter

if [ "_$scriptfilter" != _ ]; then
  echo $0: using scriptfilter
  scripts=$(/bin/ls -1 pfrepp_script* | sh -c "$scriptfilter" )
else
  scripts=$(/bin/ls -1 pfrepp_script* )
fi

echo id path >$scriptlist
for f in $scripts; do
  echo $realid $f >>$scriptlist
done

exit

#    -a /intrepid-fs0/users/lzamboni/persistent/yearly-nco/en1eo12Ic3/run001/run001/ \

# datasets needed by LZ:

Pfrepp-month.time_average
Pfrepp-month.time_series
Pfrepp-daily.time_series (4X???)
Pfrepp-8xdaily.time_series
Pfrepp-8xdaily_instant.time_series
Pfrepp-4xdaily.time_series

which means:

pfrepp_script.atmos_month.time_average.1950*
pfrepp_script.atmos_month.time_series.1950*
pfrepp_script.atmos_4xdaily.time_series.1950*
pfrepp_script.atmos_8xdaily.time_series.1950*
pfrepp_script.atmos_8xdaily_instant.time_series.1950*
pfrepp_script.atmos_daily.time_series.1950*

pfrepp_script.ice_month.time_average.1950*
pfrepp_script.ice_month.time_series.1950*
