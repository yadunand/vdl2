#! /bin/sh

# ls -1d en*/history/[0-9][0-9][0-9][0-9]????   eur$ ls -1d en*/history/[0-9][0-9][0-9][0-9]????

# The following can be made into parameters to this script. Set here for now.

realdir=/intrepid-fs0/users/lzamboni/persistent           # Base dir of realizations
realtodo=real4.todo                                       # File of names of realization subdirs to process
realdone=real4.done                                       # File of names of realization subdirs completed
outdir=/intrepid-fs0/users/wilde/persistent/LZ/combined4  # Base dir for combined output

# Helpful patterns for filename matching

yyyy="[0-9][0-9][0-9][0-9]"
yyyymmdd="${yyyy}[0-9][0-9][0-9][0-9]"

mkdir -p $outdir

while true; do

  real=$(diff $realtodo $realdone | grep '^<' | sed -e 's/^..//' | head -1) # next realization to do
  if [ _$real = _ ]; then
    break
  fi

  echo Combining $real

  find $realdir/$real/history/$yyyymmdd  |
    grep .nc.$yyyy      |
    sed -e 's/.....$//' | sort | uniq > $real.groups

  echo Created $real.groups

  echo  ./combine_realization.sh    \
    "$realdir/$real/history/$yyyymmdd" \
    $outdir/$real 

  ./combine_realization.sh    \
    "$realdir/$real/history/$yyyymmdd" \
    $outdir/$real 

  echo Completed $real
  echo $real >>$realdone

done

echo All combines are done.

