#! /bin/sh

export PATH=$PATH:/soft/apps/nco-3.9.9/bin
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/soft/apps/nco-3.9.9/lib

d1=19500101
d2=19520701
f=ice_month.nc

o=testyears
y1=$o/19500101
y2=$o/19510101
y3=$o/19520101
y4=$o/19530101
y5=$o/19540101

mkdir -p $y1 $y2 $y3 $y4 $y5

ncks -F -d time,1,12    $d1/$f $y1/$f

ncks -F -d time,13,24   $d1/$f $y2/$f

ncks -F -d time,25,30   $d1/$f $y3/$f.a
ncks -F -d time,1,6     $d2/$f $y3/$f.b
ncrcat $y3/$f.a $y3/$f.b       $y3/$f

ncks -F -d time,7,18    $d2/$f $y4/$f

ncks -F -d time,19,30   $d2/$f $y5/$f
