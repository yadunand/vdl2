#! /bin/bash

cdir=$1     # combined dir
outbase=$2  # base of outdir for equivalent yearly files

bindir=$(dirname $0)
bindir=$(cd $bindir; pwd)

echo bindir=$bindir

# Enable one of the following patterns to name the output dir:

outsuffix=$(basename $cdir)                              # rname
outsuffix=$(basename $(dirname $cdir))                   # rname/rundir, use rname
outsuffix=$(basename $(dirname $cdir))/$(basename $cdir) # rname/rundir, use both

# generate the list of files to process: do every half-decade

echo f1 f2 >ncfiles
echo f1 f2 >>ncfiles

# for y in $(seq 1950 5 1997); do
#   startyear=${y}0101; 
#   nextyear=$((y+2))0701
#   find $cdir/$startyear -type f | fgrep .nc | sed -e "s,^\\(.*\\)/$startyear/\\(.*\\),\\1/$startyear/\\2 \\1/$nextyear/\\2 $outdir,"
# done >>ncfiles

# Create new runNNN directory

rundir=$( echo run??? | sed -e 's/^.*run//' | awk '{ printf("run%03d\n", $1+1)}' )
mkdir $rundir

# Generate Swift config files (cf, tc, sites.xml):

cat >$rundir/cf <<END

wrapperlog.always.transfer=true
sitedir.keep=true
execution.retries=10
lazy.errors=true
status.mode=provider

END

cat >$rundir/sites.xml <<END

<config>

   <pool handle="local">
     <execution provider="local" />
     <profile namespace="karajan" key="jobThrottle">.01</profile>
     <profile namespace="karajan" key="initialScore">10000</profile>
     <filesystem provider="local"/>
     <workdirectory>$PWD/swiftwork</workdirectory>
   </pool>

  <pool handle="cobalt">
    <execution provider="coaster" jobmanager="local:cobalt" url="eureka.alcf.anl.gov" />
    <filesystem provider="local" />
    <!-- <profile namespace="globus"key="internalHostname">172.17.3.11</profile> -->
    <profile namespace="globus"  key="project">MTCScienceApps</profile>
    <profile namespace="globus"  key="queue">default</profile>
    <profile namespace="karajan" key="jobthrottle">2.56</profile>
    <profile namespace="karajan" key="initialScore">10000</profile>
    <profile namespace="globus"  key="jobsPerNode">8</profile>
    <!-- <profile namespace="globus"  key="workerLoggingLevel">DEBUG</profile> -->
    <!-- <profile namespace="globus"  key="workerLoggingDirectory">/home/jonmon/Workspace/Swift/tests/catsnsleep</profile> -->
    <profile namespace="globus"  key="slots">32</profile>
    <profile namespace="globus"  key="maxTime">28000</profile> <!-- ~ 8hrs -->
    <profile namespace="globus"  key="maxWallTime">00:20:00</profile>
    <profile namespace="globus"  key="lowoverallocation">100</profile>
    <profile namespace="globus"  key="highoverallocation">100</profile>
    <profile namespace="globus"  key="nodeGranularity">1</profile>
    <profile namespace="globus"  key="maxNodes">1</profile>
    <workdirectory>$PWD/$rundir/swiftwork</workdirectory>
  </pool>

</config>

END

cat >$rundir/tc <<END

cobalt makeyearly $bindir/makeyearly-cdo-fail.sh

END

# Place file list and swift script in run dir, set params, make output dir, and run script

cp ncfiles $bindir/makeyearly.swift $rundir

cd $rundir
echo Running in directory $rundir
runid=$(basename $rundir) # un-needed?
outdir=$outbase/$outsuffix/$runid
mkdir -p $outdir

swift -config cf -tc.file tc -sites.file sites.xml makeyearly.swift -rundir=$outdir -ncfiles=ncfiles >& swift.out
