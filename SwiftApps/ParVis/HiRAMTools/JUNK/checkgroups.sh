#! /bin/bash

# /intrepid-fs0/users/lzamboni/persistent/en1eo8Ic1/history/19500101/atmos_4xdaily.tile1.nc

sed -e 's/^.*history.//' groups | sort >todo
find /intrepid-fs0/users/wilde/persistent/en1eo8Ic1/combined/run016 -type f -print | fgrep .nc | sed -e 's/^.*run016.//' | sort >done

echo
echo diff: "<todo >done"
echo
diff todo done

echo
echo last 20 files produced were:
echo
ls -ltd /intrepid-fs0/users/wilde/persistent/en1eo8Ic1/combined/run016/*/* | head -20

echo
echo File lengths:
echo
ls -ltd /intrepid-fs0/users/wilde/persistent/en1eo8Ic1/combined/run016/*/* | awk '{print $5}' | sort | uniq -c
