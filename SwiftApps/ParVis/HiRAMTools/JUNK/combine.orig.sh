#! /bin/bash

set -o errexit
set -x

outdir=$1
inpattern=$2
date=$(basename $(dirname $inpattern))
basepattern=$(basename $inpattern)

infiles="$2*"

mode=direct

if [ $mode = scratch ]; then

  mkdir -p /scratch/$USER
  
  tmp=$(mktemp -d /scratch/$USER/combine_XXXXXX)
  
  cd $tmp
  
  for f in $infiles; do
    dd if=$f of=$(basename $f) bs=4M
  done
  
  # Add nco tools to PATH and Intel compiler v11 libs to LD_LIBRARY_PATH # FIXME: this is precautionary and can likely be removed.
  
  export PATH=/soft/apps/nco-3.9.2/bin:$PATH
  export LD_LIBRARY_PATH=/soft/apps/intel-fc-11.0.074/lib/intel64:/soft/apps/intel-cc-11.0.074/lib/intel64:/soft/apps/netcdf-3.6.3/lib:/usr/lib:/usr/X11R6/lib:/usr/local/lib
  
  # FIXME: ^^^ end precaution
  
  /home/ckerr/bin/fms/tools/x86_64/bin/mppnccombine.exe out.nc ${basepattern}*
  
  mkdir -p $outdir/$date
  dd if=out.nc of=$outdir/$date/$basepattern.trans bs=4M
  mv $outdir/$date/$basepattern.trans $outdir/$date/$basepattern
  
  cd ..
  rm -rf $tmp
  
else # $mode = direct

  export PATH=/soft/apps/nco-3.9.2/bin:$PATH
  export LD_LIBRARY_PATH=/soft/apps/intel-fc-11.0.074/lib/intel64:/soft/apps/intel-cc-11.0.074/lib/intel64:/soft/apps/netcdf-3.6.3/lib:/usr/lib:/usr/X11R6/lib:/usr/local/lib
  
  mkdir -p $outdir/$date
  /home/ckerr/bin/fms/tools/x86_64/bin/mppnccombine.exe $outdir/$date/$basepattern $infiles
  
fi

