#! /bin/bash

set -x

outdir=$1
inpattern=$2
date=$(basename $(dirname $inpattern))
basepattern=$(basename $inpattern)

infiles="$2*"

mppncombine=/home/ckerr/bin/fms/tools/x86_64/bin/mppnccombine.exe
mppncombine=/home/ckerr/bin/fms/tools/ppc64/bin/mppnccombine.exe

mode=scratch # default

scratch=/dev/shm

badnodes="loginx"
thisnode=$(hostname)
if echo $badnodes | grep -q $thisnode ; then
  echo Running on node $thisnode - is in badnode list $badnodes. Will run in direct mode.
  mode=direct
else
  # Try to make a temporary work dir under /scratch - this can fail on bad nodes
  mkdir -p $scratch/$USER
  tmp=$(mktemp -d $scratch/$USER/combine_XXXXXX)
  if [ $? != 0 ]; then
    echo Failed to create tmp dir on $scratch/$USER. Will run in direct mode.
    mode=direct
  fi
fi

mode=direct

set -o errexit # Set this after we know if tmp dir was created ok

export PATH=/soft/apps/nco-3.9.2/bin:$PATH
export LD_LIBRARY_PATH=/soft/apps/intel-fc-11.0.074/lib/intel64:/soft/apps/intel-cc-11.0.074/lib/intel64:/soft/apps/netcdf-3.6.3/lib:/usr/lib:/usr/X11R6/lib:/usr/local/lib
  
if [ $mode = scratch ]; then
  
  cd $tmp
  
  for f in $infiles; do
    dd if=$f of=$(basename $f) bs=4M
  done
  
  # Add nco tools to PATH and Intel compiler v11 libs to LD_LIBRARY_PATH # FIXME: this is precautionary and can likely be removed.
  
  # FIXME: ^^^ end precaution
  
  $mppncombine out.nc ${basepattern}*
  
  mkdir -p $outdir/$date
  dd if=out.nc of=$outdir/$date/$basepattern.trans bs=4M
  mv $outdir/$date/$basepattern.trans $outdir/$date/$basepattern
  
  cd ..
  rm -rf $tmp
  
else # $mode = direct

  mkdir -p $outdir/$date
  $mppncombine $outdir/$date/$basepattern $infiles
  
fi

