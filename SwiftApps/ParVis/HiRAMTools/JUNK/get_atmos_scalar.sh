#! /bin/bash

src=/intrepid-fs0/users/lzamboni/persistent
dest=/intrepid-fs0/users/wilde/persistent/LZ/combined

f=atmos_scalar.nc

reals=$(cat /home/wilde/LZ/realizations)

for r in $reals; do
  cd $src/$r/history
  years=$( /bin/ls -1d [0-9]??????? )
  for y in $years; do
    cp $y/$f $dest/$r/$y
  done
done

