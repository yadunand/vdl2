#! /bin/sh

reals=/intrepid-fs0/users/wilde/persistent/LZ/combined
cd $reals

for real in *; do
  echo Realization: $real
  ( cd $real
    for y in *; do
      echo "  Year: " $y
      ( cd $y
        for f in *.nc; do
          echo -n $f; ncdump -h $f | grep 'time =' || echo
        done | awk '{print "    ",$1,$7}' | grep 'atmos.*\.tile[0-9].* (1' | grep -v scalar
      )
    done
  )
done
