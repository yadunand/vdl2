#! /bin/bash

set -x

infile=$1  # must be full path
outdir=$2  # must be full path

realdir=$(dirname $(dirname $1))  # realization dir, e.g., en*
realname=$(basename $realdir)      # realization name
f=$(basename $1)
d1=$(dirname $1)
yfirst=$(echo $(basename $d1) | sed -e 's/....$//')
ymid=$((yfirst+2))
#ydir1=${yfirst}0101
d2=$realdir/${ymid}0701

period=$(basename $f .nc | sed -e 's/^.*_//' -e 's/\..*$//' -e 's/_.*//')

leap()
{
  case $1 in
    1952|1956|1960|1964|1968|1972|1976|1980|1984|1988|1992|1996|2000) echo 1 ;;
    *) echo 0 ;;
  esac
}

units()
{
  p=$1 # period
  y=$2 # year
  case $period in
    4xdaily)   echo $((4*(365+$(leap $y)))) ;;
    8xdaily)   echo $((8*(365+$(leap $y)))) ;;
    daily)     echo $((1*(365+$(leap $y)))) ;;
    month)     echo 12                     ;;
    * )
  esac
}

mkdir -p /scratch/wilde
tmp=$(mktemp -d /scratch/wilde/makeyearly_XXXXXX)
startdir=$PWD
cd $tmp

export PATH=$PATH:/soft/apps/nco-3.9.9/bin
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/soft/apps/nco-3.9.9/lib

o=$outdir/$realname

yd1=$o/$((yfirst+0))0101
yd2=$o/$((yfirst+1))0101
yd3=$o/$((yfirst+2))0101
yd4=$o/$((yfirst+3))0101
yd5=$o/$((yfirst+4))0101

y1=$((yfirst+0))
y2=$((yfirst+1))
y3=$((yfirst+2))
y4=$((yfirst+3))
y5=$((yfirst+4))

set +x
e=echo

$e mkdir -p $yd1 $yd2 $yd3 $yd4 $yd5

$e dd if=$d1/$f of=$f bs=4M           # copy in first 2.5 year fle

su=1                                  # start unit
u1=$(units $period $y1)
eu=$u1                                # end unit

$e ncks -F -d time,$su,$eu $f out.nc   # extract year 1
$e dd if=out.nc of=$yd1/$f bs=4M
$e rm out.nc

su=$((eu+1))
u2=$(units $period $y2)
eu=$((eu+$u2))

$e ncks -F -d time,$su,$eu $f out.nc  # extract year 2
$e dd if=out.nc of=$yd2/$f bs=4M
$e rm out.nc

su=$((eu+1))
u3=$(units $period $y3)
heu=$((u3/2))
eu=$((eu+$heu))

$e ncks -F -d time,$su,$eu $f tmp.a.nc # extract year 3 part a
$e dd if=$d2/$f of=$f bs=4M            # copy in second 2.y year file

su=1
eu=$heu

$e ncks -F -d time,$su,$eu $f tmp.b.nc # extract year 3 part b

$e ncrcat tmp.a.nc tmp.b.nc out.nc     # merge year3 parts a and b
$e dd if=out.nc of=$yd3/$f bs=4M
$e rm out.nc tmp.a.nc tmp.b.nc

su=$((eu+1))
u4=$(units $period $y4)
eu=$((eu+$u4))

$e ncks -F -d time,$su,$eu $f out.nc   # extract year 4
$e dd if=out.nc of=$yd4/$f bs=4M
$e rm out.nc

su=$((eu+1))
u5=$(units $period $y5)
eu=$((eu+$u4))

$e ncks -F -d time,$su,$eu $f out.nc  # extract year 5
$e dd if=out.nc of=$yd5/$f bs=4M
$e rm out.nc $f

$e cd $startdir
rm -rf $tmp

exit

######################
#

the origin of the problem is that in the first 6 month of the year (Jan-Jun)
there are less days than in the second half of the year (Jul-Dec), and that
2.5 years refers to the # of months.

The difference is 3 days in a normal year, 2 in a leap year.

So the variations are not simply due by the presence of a leap year. These are
the categories.  Let me define these before:
 
# normal= no leap in the 2.5 directory.
# 1st half=directoryName is yyyy0101
# 2nd half=directoryName is yyyy0701
# 
# N1 normal, 1st half -->3644 steps (911 days*4)
# N2 normal, 2nd half -->3656 steps (914 days*4)
# L1 leap, 1st half -->3648 steps   (912 days*4)
# L2 leap, 2nd half -->3660 steps.  (915 days*4)

# leap: 1952 1956 1960 1964 1968 1972 1976 1980 1984 1988 1992 1996 2000

# Jan, Feb, Mar, Apr, May, June = (31+28+31+30+31+30) = 181

# N-year: 181+184 x4= 724 + 736 = 1460
# L-year: 182+184 x4= 728 + 736 = 1474


19500101 1   L1: 3648 
1951     1
1952     / L

19520701 /   N2: 3656
1953     1
1954     1
----
19550101 1   L1: 3648
1956     1 L
1957     /

19570701 +   N2: 3656
1958     1
1959     1
----
19600101 1 L L1: 3648
1961     1
1962     /

19620701 +   L2: 3660
1963     1
1964     1 L
----
19650101 1   N1: 3648
1966     1
1967     /

19670701 +   L2: 3660
1968     1 L
1969     1
----
19700101 1   L1: 3648
1971     1
1972     / L

19720701 +   N2: 3656
1973     1
1974     1
----
19750101 1   L1: 3648
1976     1 L
1977     /

19770701 +   N2: 3656
1978     1
1979     1
----
19800101 1 L L1: 3648
1981     1
1982     /

19820701 +   L2: 3660
1983     1
1984     1 L
----
19850101 1   N1: 3648
1986     1
1987     /

19870701 +   L2: 3660
1988     1 L
1989     1
----
19900101 1   L1: 3648
1991     1
1992     / L

19920701 +   N2: 3656
1993     1
1994     1
----
19950101 1   L1: 3648
1995     1
1996     / L

19970701 +   N2: 3656
1998     1
1999     1
----
