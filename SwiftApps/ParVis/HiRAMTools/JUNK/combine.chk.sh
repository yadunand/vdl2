#! /bin/bash

set -o errexit

mkdir -p /scratch/$USER

tmp=$(mktemp -d /scratch/$USER/combine_XXXXXX)

echo mktemp error code: $?

cd $tmp

outdir=$1
inpattern=$2
date=$(basename $(dirname $inpattern))
basepattern=$(basename $inpattern)

infiles="$2*"
for f in $infiles; do
  dd if=$f of=$(basename $f) bs=4M
done

/home/ckerr/bin/fms/tools/x86_64/bin/mppnccombine.exe out.nc ${basepattern}*

mkdir -p $outdir/$date
dd if=out.nc of=$outdir/$date/$basepattern.trans bs=4M
mv $outdir/$date/$basepattern.trans $outdir/$date/$basepattern

cd ..
rm -rf $tmp



