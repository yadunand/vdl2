#! /bin/bash

set -o errexit

# Create a fast (local-filesystem) working dir

echo About to run failing mkdir

mkdir -p /NOscratch/wilde

echo Should not get here!
exit 42

