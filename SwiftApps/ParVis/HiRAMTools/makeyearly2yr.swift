
type file;

type ncrec {
  string f1;
}

string rundir=@arg("rundir","NO-OUTDIR-SPECIFIED");
string ncfile=@arg("ncfile","ncfiles");

app (file sout, file serr) makeyearly (string dir, string f1)
{
  makeyearly_2yr f1 dir stdout=@sout stderr=@serr;
}

ncrec nc[] = readData(ncfile);

foreach n,i in nc {
  file so <single_file_mapper; file=@strcat("stdlog/stdout.",i)>;
  file se <single_file_mapper; file=@strcat("stdlog/stderr.",i)>;
  (so,se) = makeyearly(rundir,n.f1);
}


