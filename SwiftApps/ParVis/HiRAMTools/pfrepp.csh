#!/bin/csh 
#NOTE : Currently this tool will only postprocess atmos data and assume the name
#       of the files starts with "atmos". 

  set template  = $0:h/pfrepp_template.csh
  set environ   = INTERACTIVE
  set remapfile = "none"

#  ----- parse input argument list ------
  set historydir = ""
  set ppdir = ""
  set starttime = ""
  @ numyears   = 5
  @ nummonths  = 12
  set zinterp_atm   = "ncep"
  set zinterp_ocn   = "none"

  set do_not_run = 0
  @ ntiles_lnd = 6
  @ ntiles_atm = 6
  @ ntiles_ocn = 1
  set nlon_atm       = 0
  set nlat_atm       = 0
  set nlon_ocn       = 0
  set nlat_ocn       = 0
  set mosaic_atm = ""
  set mosaic_ocn = ""
  set remapfile_atm = "none"
  set remapfile_ocn = "none"   
  set remove_tmp = 1
  set time_average_only = 0
  set use_bbcp = 0

# currently there is no horizontal interpolation for ocean/ice data.
#set argv = (`getopt a:s:n:m:t:T:i:I:x:y:r:z:qdh $*`)

set argv = (`getopt a:b:cs:n:m:t:T:i:I:x:X:y:Y:r:R:z:Z:uqdwh $*`)

while ("$argv[1]" != "--")
    switch ($argv[1])
        case -a:
            set historydir = $argv[2]; shift argv; breaksw;
        case -b:
            set ppdir = $argv[2]; shift argv; breaksw;
        case -c:
            set use_bbcp = 1; breaksw;
        case -s:
            set starttime = $argv[2]; shift argv; breaksw;
        case -n:
            @ numyears  = $argv[2]; shift argv; breaksw;
        case -m:
            @ nummonths  = $argv[2]; shift argv; breaksw;
        case -t:
            @ ntiles_atm = $argv[2]; shift argv; breaksw;
        case -T:
            @ ntiles_ocn = $argv[2]; shift argv; breaksw;
        case -i:
            set mosaic_atm = $argv[2]; shift argv; breaksw;
        case -I:
            set mosaic_ocn = $argv[2]; shift argv; breaksw;
        case -x:
            set nlon_atm = $argv[2]; shift argv; breaksw;
        case -y:
            set nlat_atm = $argv[2]; shift argv; breaksw;
        case -X:
            set nlon_ocn = $argv[2]; shift argv; breaksw;
        case -Y:
            set nlat_ocn = $argv[2]; shift argv; breaksw;	    
        case -r:
            set remapfile_atm = $argv[2]; shift argv; breaksw;
        case -R:
            set remapfile_ocn = $argv[2]; shift argv; breaksw;
        case -z:
            set zinterp_atm =$argv[2]; shift argv; breaksw;
        case -Z:
            set zinterp_ocn =$argv[2]; shift argv; breaksw;
        case -u:
            set do_not_run = 1; breaksw;
        case -q:
            set environ = BATCH; breaksw;
        case -d:
            set remove_tmp = 0; breaksw;
        case -w:
            set time_average_only = 1; breaksw;
        case -h:
            set help ; breaksw
    endsw
    shift argv
end

#---- print out user guide when -h option is selected.
if( ! $?help) then
   if ($historydir == "") then
      echo "ERROR from pfrepp.csh: historydir is not set through -a"
      set help
   endif

   if ($starttime == "") then
      echo "ERROR from pfrepp.csh: starttime is not set through -s"
      set help
   endif

   if($zinterp_atm != "ncep" ) then
      echo "ERROR from pfrepp.csh: zinterp_atm set through -z should be 'ncep', contact developer"
      exit 1
   endif

   # when mosaic_atm is defined, nlon_atm, nlat_atm should be defined.
   if( $mosaic_atm != "" ) then
      #first check to make sure mosaic_atm exist
      if( ! -e $mosaic_atm ) then
         echo "ERROR from pfrepp.csh: $mosaic_atm does not exist"
         exit 1
      endif

      if (${ntiles_atm} != 6 ) then
         echo "ERROR from pfrepp.csh: ntiles_atm set through -t should be 6 when mosaic_atm is set, contact developer"
         exit 1
      endif

      if ($nlon_atm == 0) then
         echo "ERROR from pfrepp.csh: nlon_atm is not set through -x"
         set help
      endif

      if ($nlat_atm == 0) then
         echo "ERROR from pfrepp.csh: nlat_atm is not set through -y"
         set help
      endif
   else
      if ( $ntiles_atm != 1 ) then
         echo "Error from pfrepp.csh: ntiles_atmos should be 1 when mosaic_atm is not set"
         exit 1
      endif

      if ($nlon_atm != 0) then
         echo "NOTE from pfrepp.csh: there will be no horizontal interpolation when input grid is lat-lon, nlon_atm need not to be set"
         @ nlon_atm = 0
      endif
	 
      if ($nlat_atm != 0) then
         echo "NOTE from pfrepp.csh: there will be no horizontal interpolation when input grid is lat-lon, nlat_atm need not to be set"
         @ nlat_atm = 0
      endif

   endif

   # currently there will be no horizontal and vertical interpolation for ocean/ice data.
   if( $ntiles_ocn != 1 ) then
      echo "Error from pfrepp.csh: ntiles_ocn should be 1"
      exit 1
   endif

   if( $mosaic_ocn != "" ) then
      echo "NOTE from pfrepp.csh: there will be no horizontal interpolation for ocean/ice data, mosaic_ocn need not to be set"
      set mosaic_ocn = ""
   endif

   if ($nlon_ocn != 0) then
      echo "NOTE from pfrepp.csh: there will be no horizontal interpolation for ocean/ice data, nlon_ocn need not to be set"
      @ nlon_ocn = 0
   endif

   if ($nlat_ocn != 0) then
      echo "NOTE from pfrepp.csh: there will be no horizontal interpolation for ocean/ice data, nlat_ocn need not to be set"
      @ nlat_ocn = 0
   endif

   if ( $remapfile_ocn != "none" ) then
      echo "NOTE from pfrepp.csh: there will be no horizontal interpolation for ocean/ice data, remapfile_ocn need not to be set"
      set remapfile_ocn = "none"
   endif

   if ($zinterp_ocn != "none") then
      echo "NOTE from pfrepp.csh: there will be no vertical interpolation for ocean/ice data, zinterp_ocn need not to be set"
      set  zinterp_ocn = "none"
   endif

   set len = `echo ${starttime} | awk '{print length($0)}'`

   if( $numyears == 0 ) then
      if( $len == 4 ) then
         set starttime = ${starttime}0101
      endif
   else
      if( $len != 4 ) then
          echo "ERROR from pfrepp.csh: starttime must be in the format 'yyyy' when numyears is not zero"
          exit 1
      endif
   endif
endif

if ($?help) then
cat<<EOF

  USAGE:  ./pfrepp.csh -a historydir -s starttime [-b ppdir] [-n numyears] [-m nummonths]
           [-i mosaic_atm] [-x nlon_atm] [-y nlat_atm] [-t ntiles_atm] [-r remapfile_atm]
	   [-I mosaic_ocn] [-X nlon_ocn] [-Y nlat_ocn] [-T ntiles_ocn] [-R remapfile_atm]
           [-q] [-u] [-w] [-h] 

  Required arguments:

          -a historydir    : source directory of the files to be combined
          
          -s starttime     : starting time to be processed. starttime could be in the
                             format yyyymmdd or yyyy ( the date will be assumed as yyyy0101).
                             starttime must be in the format yyyy when you want to do time
                             averaging.
			   
  Optional arguments:
			   
          -b ppdir         : directory to store post-processing data. The default will be historydir:h/pp

          -n numyears      : number of years data to be processed. 
                             The default value is 5 and time averaging will be performed only
                             when numyears is greater than 0. When numyears = 0, the starttime could be in the 
                             format yyyymmdd or yyyy ( the date will be assumed as yyyy0101). 
                             When starttime is in the format yyyymmdd, numyears will be assumed to 
                             be 0. 

          -m nummonths     : number of months of output data in each history files. Its
                             value must be divided by 12. Default value is 1.
			   
          -i mosaic_atm    : atmos mosaic grid file for atmos input data: historydir/date

          -x nlon_atm      : number of longitude grid cells of the uniform lat-lon atmos output grid 

          -y nlat_atm      : number of latitude grid cells of the uniform lat-lon atmos output grid 

          -t ntiles_atm    : number of tiles in the atmos input data. The default is 6.
	                     Its value could be 1 or 6.
                                    
          -r remapfile_atm : contains remap information to regrid atmos data from input cubic sphere grid 
                             to lat-lon grid. It is strongly recommended to use the option to 
                             improve performance. 

          -I mosaic_ocn    : ocean mosaic grid file for ocean input data: historydir/date

          -X nlon_ocn      : number of longitude grid cells of the uniform lat-lon ocean output grid 

          -Y nlat_ocn      : number of latitude grid cells of the uniform lat-lon ocean output grid 

          -T ntiles_ocn    : number of tiles in the ocean input data. The default is 6.
	                     Its value could be 1 or 6.
                                    
          -R remapfile_ocn : contains remap information to regrid ocean data from input tripolar grid 
                             to lat-lon grid. It is strongly recommended to use the option to 
                             improve performance. 

          -u               : When set, all the runscripts will be generated but will not be executed.
	  
          -q               : execute in BATCH mode; INTERACTIVE mode is default

          -d               : do not remove the tmpdir when -d is set. Default will remove the tmpdir.

          -w               : When set, will only process time averaging ( no time series ).

          -h               : print out help message

    Example (remap data from C720 onto 1/4 degree lat-lon grid):

      pfrepp.csh -a /project/k14/zliang/bin/scripts/pfrepp/test -s 1980 
                 -i /project/k14/zliang/bin/scripts/pfrepp/test/grid/C48_mosaic.nc
                 -x 144 -y 90 -r /project/k14/zliang/bin/scripts/pfrepp/test/grid/C48_to_144x90.nc

      Frequently used remap files are maintained in the standard location: /home/project/k14/ckerr/bin/fms/remap_files
EOF
exit 1
endif

 if( $ppdir == "" ) then
    set ppdir = $historydir:h/pp
 endif

  echo "ENVIRONMENT IS:     $environ"

setenv architecture `uname -m`

if  ( $architecture == "x86_64" ) then
  set FREGRID =  $0:h:h/fregrid/fregrid.csh
  set NCCOMBINE = $0:h:h/combine/combine.csh
  set TIMEAVG = $0:h:h/time_average/time_average.csh
  set PLEVEL = $0:h:h/plevel/plevel.csh
  set MERGE  = $0:h:h/merge/merge.csh
else
  echo "ERROR: pfrepp.csh is only supported on x86_64"
  exit 1
endif

# Submit a post-processing job for each atmosphere data.
# for monthly data, need to do both time averging and
# time series when numyears is non-zero.

foreach component ( "atmos" "land" "ocean" "ice" )
   if( $component == "atmos" ) then
      set inputmosaic = ${mosaic_atm}
      set nlon = ${nlon_atm}
      set nlat = ${nlat_atm}
      set remapfile = ${remapfile_atm}
      set ntiles = ${ntiles_atm}
      set zinterp = ${zinterp_atm}
      set interp_method = "conserve_order2"
   else if( $component == "land" ) then
      set inputmosaic = ${mosaic_atm}
      set nlon = ${nlon_atm}
      set nlat = ${nlat_atm}
      set remapfile = ${remapfile_atm}
      set ntiles = ${ntiles_atm}
      set zinterp = "none"
      set interp_method = "conserve_order1"
   else # ocean or ice
      set inputmosaic = ${mosaic_ocn}
      set nlon = ${nlon_ocn}
      set nlat = ${nlat_ocn}
      set remapfile = ${remapfile_ocn}
      set ntiles = ${ntiles_ocn}
      set zinterp = ${zinterp_ocn}
      set interp_method = "none"
   endif

   if( $numyears == 0 ) then
      set datedir = $historydir/$starttime
   else
      set datedir = $historydir/${starttime}0101
   endif
   if( ! -d $datedir ) then
      echo "pfrepp: $datedir does not exist" 
      exit 1 
   endif

   set curdir = $cwd
   cd $datedir
   
   if( $ntiles == 1 ) then
      set filelist = (`ls -1 ${component}*.nc`)
   else
      set filelist = (`ls -1 ${component}*.tile1.nc`)
   endif

   if( $#filelist == 0 ) then
      continue 
   endif

   cd $curdir

   foreach file ( $filelist )
      set filename = `echo $file | sed 's/\./ /g' | awk '{printf $1}'`
      set name = `echo $filename | sed 's/_/ /' | awk '{printf $2}'`
      if( ${time_average_only} == 0 ) then
         set mode = "time_series"

         if( $numyears == 0 ) then
            set yearname = $starttime
         else
            set yearname = `echo $starttime | awk '{printf("%04s", $1)}'`
         endif
         set runscript = pfrepp_script.$filename.$mode.$yearname
         echo "\
            s|AAAAAAA|${historydir}|g\
            s|BBBBBBB|${yearname}|g \
            s|CCCCCCC|${inputmosaic}|g \
            s|DDDDDDD|${nlon}|g \
            s|EEEEEEE|${nlat}|g \
            s|FFFFFFF|${remapfile}|g \
            s|GGGGGGG|${numyears}|g \
            s|HHHHHHH|${ntiles}|g \
            s|IIIIIII|${component}|g \
            s|JJJJJJJ|${FREGRID}|g \
            s|LLLLLLL|${NCCOMBINE}|g \
            s|MMMMMMM|${TIMEAVG}|g \
            s|NNNNNNN|${PLEVEL}|g \
            s|OOOOOOO|${zinterp}|g \
            s|PPPPPPP|${remove_tmp}|g\
            s|QQQQQQQ|${MERGE}|g\
            s|RRRRRRR|${nummonths}|g\
            s|SSSSSSS|${filename}|g \
            s|TTTTTTT|${mode}|g \
            s|VVVVVVV|${use_bbcp}|g \
            s|WWWWWWW|${interp_method}|g \
            s|UUUUUUU|${ppdir}|g"  \
         > sed_file
         cat $template | sed -f sed_file > $runscript
         rm sed_file
         chmod +x $runscript

         if( $do_not_run == 1 ) then
            echo "pfrepp.csh: -u is set, will not execute ./$runscript"
         else if ( $environ == "BATCH" ) then
            echo " submitting  $runscript"
            qsub -A R.climate -n 1 -t 600 ./$runscript
         else
            echo " running $runscript interactively "
            ./$runscript
         endif
      endif

      #when name is month and numyears is greater than 0, need to do time average.
      if( $numyears > 0 && $name == "month" ) then

         set mode = "time_average"
         set runscript = pfrepp_script.$filename.$mode.$starttime
         echo "\
            s|AAAAAAA|${historydir}|g\
            s|BBBBBBB|${starttime}|g \
            s|CCCCCCC|${inputmosaic}|g \
            s|DDDDDDD|${nlon}|g \
            s|EEEEEEE|${nlat}|g \
            s|FFFFFFF|${remapfile}|g \
            s|GGGGGGG|${numyears}|g \
            s|HHHHHHH|${ntiles}|g \
            s|IIIIIII|${component}|g \
            s|JJJJJJJ|${FREGRID}|g \
            s|LLLLLLL|${NCCOMBINE}|g \
            s|MMMMMMM|${TIMEAVG}|g \
            s|NNNNNNN|${PLEVEL}|g \
            s|OOOOOOO|${zinterp}|g \
            s|PPPPPPP|${remove_tmp}|g\
            s|QQQQQQQ|${MERGE}|g\
            s|RRRRRRR|${nummonths}|g\
            s|SSSSSSS|${filename}|g \
            s|TTTTTTT|${mode}|g \
            s|VVVVVVV|${use_bbcp}|g \
            s|WWWWWWW|${interp_method}|g \
            s|UUUUUUU|${ppdir}|g" \
         > sed_file
         cat $template | sed -f sed_file > $runscript
         rm sed_file
         chmod +x $runscript
         if( $do_not_run == 1 ) then
            echo "pfrepp.csh: -u is set, will not execute ./$runscript"
         else if ( $environ == "BATCH" ) then
            echo " submitting  $runscript"
            qsub -A R.climate -n 1 -t 600 ./$runscript
         else
            echo " running $runscript interactively "
            ./$runscript
         endif
      endif
  
   end   
end

exit 0
