#! /bin/sh

script=$2

sh -c "$script" | awk '{ printf("[%d] %s\n",NR-1,$0) }'
