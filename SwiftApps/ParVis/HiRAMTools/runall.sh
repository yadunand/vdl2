#! /bin/sh

bindir=$(dirname $0)
bindir=$(cd $bindir; pwd)

echo bindir=$bindir

realtodo=real.todo                                       # File of full path names of realization dirs to process
realdone=real.done                                       # File of full path names of realization dirs completed
outdir=/intrepid-fs0/users/wilde/persistent/LZ/yearly      # Base dir for processed output

source $bindir/runall.config   # set default params
if [ -f ./runall.config ]; then
  source ./runall.config       # set local params
fi

echo script=$script


mkdir -p $outdir

while true; do

  real=$(diff $realtodo $realdone | grep '^<' | sed -e 's/^..//' | head -1) # next realization to do
  if [ _$real = _ ]; then
    break
  fi

  echo RunAll: Processing $real

  echo  Running: $bindir/$script    \
    $real \
    $outdir 

  $bindir/$script    \
    $real \
    $outdir 

  echo RunAll: Completed $real
  echo $real >>$realdone

done

echo RunAll: all realizations are done.
