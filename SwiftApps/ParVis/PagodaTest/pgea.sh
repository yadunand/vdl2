#! /bin/sh
NPROCS=${1:-8}
NFILES=${2:-99999}
echo using NPROCS=$NPROCS NFILES=$NFILES

OUTDIR=$(pwd)/output
INDIR=/fusion/group/climate/Parvis/atmos/b40.1850.track1.2deg

cd $INDIR
for f in $(ls -1 *.nc | head -${NFILES}); do
  echo doing: $f
  mpirun -np $NPROCS /fusion/gpfs/home/mickelso/soft/pagoda-dev-r587/bin/pgflint -O --file_format=64bit -C \
    -x -v GW,HYAM,HYBM,HYAI,HYBI,P0 \
    -w 0.3444444537162781,0.0 \
    --path=/fusion/group/climate/Parvis/atmos/b40.1850.track1.2deg $f $f \
    $OUTDIR/$(basename $OUTDIR/$f .nc).int.nc
done

cat <<ENDCOMMENT >/dev/null

(file out)Sum_Monthly_Files(string f[])
{
	app {ncea "-O" f @out;}	
}

(file out)Sum_Wgt_Files(file f1, file f2, file f3)
{
	app {ncea "-O" "-y" "ttl" @f1 @f2 @f3 @filename(out);}
}

(file out)Sum_Wgt_Files_12Months(file f1, file f2, file f3, file f4, file f5, file f6, file f7, file f8, file f9, file f10, file f11, file f12)
{
	app {ncea "-O" "-y" "ttl" @f1 @f2 @f3 @f4 @f5 @f6 @f7 @f8 @f9 @f10 @f11 @f12 @out;}
}

(file out)Rename_to_Climo(file input[])
{
	app {mv @filename(input) @filename(out);}
}
(file out)Copy_to_Climo(string input)
{
        app {cp input  @filename(out);}
}  

Edit_File_Attributes(int var, file input)
{
	app {ncatted "-O" "-a" @strcat("yrs_averaged,global,c,c,",var) @filename(input);}
}

(file out)Apply_Weights(string n_t_var, string weights, string f)
{
	app {ncflint "-O" "-C" "-x" "-v" n_t_var "-w" @strcat(weights,",0.0") f f @filename(out);}
}

(file out)Ensemble_Averages(file f[])
{
	app {ncea "-O" @filenames(f) @filename(out);}
}

(file out)Calc_Climo_File2(string f[])
{
        app {ncea "-O" f @filename(out);}
}

(file out)Calc_Significance(file f[])
{
	app {ncrcat "-O" @filenames(f) @filename(out);}
}

ENDCOMMENT
