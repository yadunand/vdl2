#! /bin/sh
NPROCS=${1:-8}
NFILES=${2:-99999}
DATASET=${3:-/fusion/group/climate/Parvis/atmos/taylor}
DATASET=${3:-/fusion/group/climate/Parvis/atmos/f40_amip_025d_b06c4_207jp.cam2}
DATASET=${3:-/fusion/group/climate/Parvis/atmos/HRC06}

INCLUDEVARS="-v CLDHGH,CLDICE,CLDLIQ,CLDMED,CLDLOW,CLDTOT,CLOUD,DCQ,DTCOND,DTV,FICE,FLDS,FLNS,FLNSC,FLNT,FLNTC,FLUT,FLUTC,FSDS,FSDSC,FSNS,FSNSC,FSNTC,FSNTOA,FSNTOAC,FSNT,ICEFRAC,LANDFRAC,LHFLX,LWCF,OCNFRAC,OMEGA,OMEGAT,P0,PBLH,PRECC,PRECL,PRECSC,PRECSL,PS,PSL,Q,QFLX,QRL,QRS,RELHUM,SHFLX,SNOWHICE,SNOWHLND,SOLIN,SRFRAD,SWCF,T,TAUX,TAUY,TGCLDIWP,TGCLDLWP,TMQ,TREFHT,TS,U,UU,V,VD01,VQ,VT,VU,VV,Z3"

EXCLUDEVARS="-x -v GW,HYAM,HYBM,HYAI,HYBI,P0"

VARS=$INCLUDEVARS

PAGODA=/fusion/gpfs/home/mickelso/soft/pagoda-dev-r587/bin

DSNAME=$(basename $DATASET)
echo using NPROCS=$NPROCS NFILES=$NFILES

OUTDIR=$(pwd)/output-$DSNAME
mkdir -p $OUTDIR

cd $DATASET
for f in $(ls -1 *.nc | head -${NFILES}); do
  echo doing: $f
  mpirun -np $NPROCS $PAGODA/pgflint -O --file_format=64bit -C \
    $VARS \
    -w 0.3444444537162781,0.0 \
    --path=$DATASET $f $f \
    $OUTDIR/$(basename $OUTDIR/$f .nc).int.nc
done

exit

# --path=/fusion/group/climate/Parvis/atmos/b40.1850.track1.1deg.006
# b40.1850.track1.1deg.006.cam2.h0.0100-01.nc \
# b40.1850.track1.1deg.006.cam2.h0.0100-01.nc \

#  b40.1850.track1.2deg.003.cam2.h0.0509-04.nc \
#  b40.1850.track1.2deg.003.cam2.h0.0509-04.nc \

# -x -v GW,HYAM,HYBM,HYAI,HYBI,P0 \

#  pg_wgt_month.01.nc

# INDIR=/fusion/group/climate/Parvis/atmos/b40.1850.track1.2deg

