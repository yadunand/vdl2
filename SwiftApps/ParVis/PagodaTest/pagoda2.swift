#! /bin/sh

# NPROCS=${1:-8}
# NFILES=${2:-99999}
# DATASET=${3:-/fusion/group/climate/Parvis/atmos/taylor}
# DATASET=${3:-/fusion/group/climate/Parvis/atmos/f40_amip_025d_b06c4_207jp.cam2}
# DATASET=${3:-/fusion/group/climate/Parvis/atmos/HRC06}

type ncfile;

global string includevars="CLDHGH,CLDICE,CLDLIQ,CLDMED,CLDLOW,CLDTOT,CLOUD,DCQ,DTCOND,DTV,FICE,FLDS,FLNS,FLNSC,FLNT,FLNTC,FLUT,FLUTC,FSDS,FSDSC,FSNS,FSNSC,FSNTC,FSNTOA,FSNTOAC,FSNT,ICEFRAC,LANDFRAC,LHFLX,LWCF,OCNFRAC,OMEGA,OMEGAT,P0,PBLH,PRECC,PRECL,PRECSC,PRECSL,PS,PSL,Q,QFLX,QRL,QRS,RELHUM,SHFLX,SNOWHICE,SNOWHLND,SOLIN,SRFRAD,SWCF,T,TAUX,TAUY,TGCLDIWP,TGCLDLWP,TMQ,TREFHT,TS,U,UU,V,VD01,VQ,VT,VU,VV,Z3";

global string excludevars="GW,HYAM,HYBM,HYAI,HYBI,P0";

app (ncfile o) pgflint (ncfile i)
{
  pgflint "--file_format=64bit" "-C"
    "-v" includevars 
    "-w" "0.3444444537162781,0.0"
    "-O" @i @i @o ;
}

app (ncfile o) pgea (ncfile i[])
{
  pgea4 "-O" "-y" "ttl" @filenames(i) @o;
}

app (ncfile o) pgeassn (ncfile m1, ncfile m2, ncfile m3)
{
  pgea2 "-O" "-y" "ttl" @m1 @m2 @m3 @o;
}

#string syear[] = ["0110","0111","0112","0113","0114","0115","0116","0117","0118","0119","0120"];
string smon[]  = ["00","01","02","03","04","05","06","07","08","09","10","11","12"];
string ssnname[] = ["JFM","AMJ","JAS","OND"]; 

#int years[]  = [110:120];

int toyear=@toint(@arg("toyear","110"));

int years[]  = [110:toyear];
int months[] = [1:12];
int seasons[] = [1,4,7,10];

string dsdir=@arg("dataset","/fusion/group/climate/Parvis/atmos/HRC06");
string outdir=@arg("outdir","outdir");
string dsname="/HRC06.cam2.h0.0";

# foreach f,i in infile {
# foreach i in [0:@toint(@arg("n","1"))-1] {

ncfile lint[][];  # [year][month]

foreach y in years {
  foreach m in months {
    ncfile infile  <single_file_mapper; file=@strcat(dsdir,dsname,y,"-",smon[m],".nc")>;
    ncfile outfile <single_file_mapper; file=@strcat(outdir,dsname,y,"-",smon[m],".lint.nc")>;
    outfile = pgflint(infile);
    lint[y][m] = outfile;
  }
}

ncfile ssnavg[][]; # [year][ssn]

foreach y in years {
  foreach s, sn in seasons {
    ncfile f<single_file_mapper; file=@strcat(outdir,dsname,y,"-",ssnname[sn],".season.nc")>;
    f = pgeassn(lint[y][s], lint[y][s+1], lint[y][s+2]);
    ssnavg[y][sn] = f;
  }
}

ncfile yearavg[];

foreach year, y in lint {
  ncfile f<single_file_mapper; file=@strcat(outdir,dsname,y,".yearly.nc")>;
  f = pgea(year);
  yearavg[y] = f;
}

ncfile gf<single_file_mapper; file=@strcat(outdir,dsname,"000.grandavg.nc")>;
gf = pgea(yearavg);

#--- From AMWG Diag script, for later use:

/*

(file out)Sum_Monthly_Files(string f[])
{
	app {ncea "-O" f @out;}	
}

(file out)Sum_Wgt_Season(file f1, file f2, file f3)
{
	app {ncea "-O" "-y" "ttl" @f1 @f2 @f3 @out;}
}

(file out)Sum_Wgt_Year(file f1, file f2, file f3, file f4, file f5, file f6, file f7, file f8, file f9, file f10, file f11, file f12)
{
	app {ncea "-O" "-y" "ttl" @f1 @f2 @f3 @f4 @f5 @f6 @f7 @f8 @f9 @f10 @f11 @f12 @out;}
}

(file out)Ensemble_Averages(file f[])
{
	app {ncea "-O" @filenames(f) @filename(out);}
}

*/


