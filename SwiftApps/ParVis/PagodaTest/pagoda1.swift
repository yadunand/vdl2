#! /bin/sh

# NPROCS=${1:-8}
# NFILES=${2:-99999}
# DATASET=${3:-/fusion/group/climate/Parvis/atmos/taylor}
# DATASET=${3:-/fusion/group/climate/Parvis/atmos/f40_amip_025d_b06c4_207jp.cam2}
# DATASET=${3:-/fusion/group/climate/Parvis/atmos/HRC06}

type ncfile;

global string includevars="CLDHGH,CLDICE,CLDLIQ,CLDMED,CLDLOW,CLDTOT,CLOUD,DCQ,DTCOND,DTV,FICE,FLDS,FLNS,FLNSC,FLNT,FLNTC,FLUT,FLUTC,FSDS,FSDSC,FSNS,FSNSC,FSNTC,FSNTOA,FSNTOAC,FSNT,ICEFRAC,LANDFRAC,LHFLX,LWCF,OCNFRAC,OMEGA,OMEGAT,P0,PBLH,PRECC,PRECL,PRECSC,PRECSL,PS,PSL,Q,QFLX,QRL,QRS,RELHUM,SHFLX,SNOWHICE,SNOWHLND,SOLIN,SRFRAD,SWCF,T,TAUX,TAUY,TGCLDIWP,TGCLDLWP,TMQ,TREFHT,TS,U,UU,V,VD01,VQ,VT,VU,VV,Z3";

global string excludevars="GW,HYAM,HYBM,HYAI,HYBI,P0";

app (ncfile o) pgflint (ncfile i)
{
  pgflint "--file_format=64bit" "-C"
    "-v" includevars 
    "-w" "0.3444444537162781,0.0"
    "-O" @i @i @o ;
}

string dsdir=@arg("dataset","/fusion/group/climate/Parvis/atmos/HRC06");
string outdir=@arg("outdir","outdir");

ncfile infile[]  <filesys_mapper; location=dsdir, suffix=".nc">;
ncfile outfile[] <simple_mapper; location=outdir, suffix=".nc">;

# foreach f,i in infile {

foreach i in [0:@toint(@arg("n","1"))-1] {
  outfile[i] = pgflint(infile[i]);
}