#! /bin/sh
NPROCS=${1:-8}
echo using NPROCS=$NPROCS

OUTDIR=$(pwd)/output
INDIR=/fusion/group/climate/Parvis/atmos/b40.1850.track1.2deg

cd $INDIR
for f in $(ls -1 *.nc | head -1); do
  mpirun -np $NPROCS /fusion/gpfs/home/mickelso/soft/pagoda-dev-r587/bin/pgflint -O --file_format=64bit -C \
    -x -v GW,HYAM,HYBM,HYAI,HYBI,P0 \
    -w 0.3444444537162781,0.0 \
    --path=/fusion/group/climate/Parvis/atmos/b40.1850.track1.2deg $f $f \
    $OUTDIR/$(basename $OUTDIR/$f .nc).int.nc
done

exit

# --path=/fusion/group/climate/Parvis/atmos/b40.1850.track1.1deg.006
# b40.1850.track1.1deg.006.cam2.h0.0100-01.nc \
# b40.1850.track1.1deg.006.cam2.h0.0100-01.nc \

#  b40.1850.track1.2deg.003.cam2.h0.0509-04.nc \
#  b40.1850.track1.2deg.003.cam2.h0.0509-04.nc \

# -x -v GW,HYAM,HYBM,HYAI,HYBI,P0 \

#   pg_wgt_month.01.nc