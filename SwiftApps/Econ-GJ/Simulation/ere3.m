function[e3]=ere3(x)

global Faio f g h4 GJdel GJsig; 

e3 = ( ( Faio*x + (1-Faio)*GJdel )^( 1-GJsig ) ) * ( h4-x ) / (f*g);
