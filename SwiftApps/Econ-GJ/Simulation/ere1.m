function[e1]=ere1(x)

global Faio f g h1 GJdel GJsig;

e1 = ( ( Faio*x + (1-Faio)*GJdel )^( 1-GJsig ) ) * ( x-h1 ) / (f*g);
