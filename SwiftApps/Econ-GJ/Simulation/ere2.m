function[e2]=ere2(x)

global Faio f g GJdel GJsig;

e2 = ( ( Faio*x + (1-Faio)*GJdel )^( 1-GJsig ) ) / f;
