function[W]=GJcrsw0(k)

global GJsig WA;

W=WA*(k.^(1-GJsig));
