function[G1]=Gfai1(x);
global fai h1 f g GJdel;

G1=((x-GJdel)/(fai*x+(1-fai)*GJdel))*((x-h1)/(f*g));
