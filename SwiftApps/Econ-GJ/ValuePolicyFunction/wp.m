function[wp]=wp(x)
% given shock x, return w

global GJKJ SSS FFF GJdel GJA GJnp GJkA GJkB GJq;

y=GJKJ*SSS*(FFF*x+(1-FFF)*GJdel);
if y<GJkA
	wp=gjWo(y);
elseif y>GJkB
	wp=gjV(y-GJq);
else
	wp=gjWchebap(y);
end
