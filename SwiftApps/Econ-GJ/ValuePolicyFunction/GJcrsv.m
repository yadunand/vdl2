function[V]=GJcrsv(k)

global GJsig VA;

V=VA*(k.^(1-GJsig));