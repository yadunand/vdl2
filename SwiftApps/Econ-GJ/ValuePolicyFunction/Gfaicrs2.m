function[G2]=Gfaicrs2(x);
global fai h4 f g GJdel GJsig;

G2=((x-GJdel)/((fai*x+(1-fai)*GJdel)^GJsig))/f;
