function[y]=gjInifun(x);

% This is Log. Use gjInifunCr for CRRA.
global IAB GJsig;

y=IAB(1)*((x^(1-GJsig))/(1-GJsig))+IAB(2);

