function[G2]=Gfai2(x);
global fai h4 f g GJdel;

G2=((x-GJdel)/(fai*x+(1-fai)*GJdel))/f;
