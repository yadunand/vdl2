function[G]=Gfai(fi);
global h1 h2 h3 h4 f g fai;

fai=fi;
G=quadlg('Gfai1',h1,h2)+quadlg('Gfai2',h2,h3)+quadlg('Gfai3',h3,h4);
