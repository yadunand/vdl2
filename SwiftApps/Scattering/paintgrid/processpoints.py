#! /usr/bin/env python

import sys
import time

dataFileName = sys.argv[1]
pointFileName = sys.argv[2]
runTime = float(sys.argv[3])

n = 0;
pixel = [];
#with open(dataFileName, "rb") as f:
f = open(dataFileName, "rb")
byte = f.read(1)
while byte != "":
    pixel.append(byte)
    byte = f.read(1)

print "Data file has ", len(pixel), " pixels"

f = open(pointFileName)
lines = f.read().splitlines()
f.close()

print "Processing ", len(lines), " points in model space"
print "Runtime is ", runTime, " seconds per point"

for line in lines:
  xyz = line.split()
  x = float(xyz[0]) * .001
  y = float(xyz[1]) * .001
  z = float(xyz[2]) * .001
  func = 0.0
  for p in pixel:
    v=float(ord(p))
    func += v*x + v*x + v*z
  time.sleep(runTime)
  print " %10f %10f %10f     %10f" % (x, y, z, func)
