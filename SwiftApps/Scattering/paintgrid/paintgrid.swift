type file;

# External app to generate sets of input data points

app (file plist) genPoints (file pyscript, file paramFile)
{
  local_python @pyscript @paramFile runDir stdout=@plist;
}

# External app to process a set of data points (a mockup of paintGrid)

app (file ofile) processPoints (file pyscript, file imageFile, file points)
{
  python @pyscript @imageFile @points runTime stdout=@ofile;
}

# The actual python scripts for the app() functions above

file genPoints_script     <"genpoints.py">;
file processPoints_script <"processpoints.py">;

# Command line args to this script

file   params   <single_file_mapper;file=@arg("params", "genpoints.params")>;
file   image    <single_file_mapper;file=@arg("image",  "data.0001.tiny")>;
global string runTime = @arg("runTime","0.0");
global string runDir  = @java("java.lang.System","getProperty","user.dir");

# Main script:
#   Call genPoints to make a set of files, each of which contains a set of data points to process
#   The params file specifies the range of points to generate, and how to batch them into files
#   (In this example the input points are triples in 3-space)

string pointSetFileNames[]=readData(genPoints(genPoints_script,params));
file   pointSets[] <array_mapper; files=pointSetFileNames>;

foreach pointSet, i in pointSets {
  file ofile<single_file_mapper; file=@strcat("out/out.",@strcut(@strcat("00000",i),"(.....$)"))>;
  ofile = processPoints(processPoints_script, image, pointSet);
}
