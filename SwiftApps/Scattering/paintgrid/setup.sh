# Add Swift and java to PATH
export SWIFT_HEAP_MAX=4G
export PATH=/clhome/WILDE/swift/rev/swift-0.94.1/bin:$PATH

echo -e "\\nUsing swift release from: \\c"
which swift
swift -version

echo -e "Using java from: \\c"
which java
java -version
echo

# Cleanup swift.log
if [ -f "swift.log" ]; then
   rm swift.log > /dev/null 2>&1
fi

# Create data files
if [ ! -f "data.0001.tif" ]; then
   echo Creating data.0001.tif
   dd bs=8M  count=1 if=/dev/zero of=data.0001.tif
   echo
fi

if [ ! -f "data.0001.tiny" ]; then
   echo Creating data.0001.tiny
   dd bs=800 count=1 if=/dev/zero of=data.0001.tiny
   echo
fi

if [ -e $HOME/.swift ]; then
  savedotswift=$(mktemp -d $HOME/.swift.save.XXXX)
  echo -e "Saving $HOME/.swift in $savedotswift \\n"
  mv $HOME/.swift/* $savedotswift
else
  mkdir -p $HOME/.swift
fi

# Try to make things easier by assuming username are the same on orthros and beagle
sed -i s/davidk/$USER/g start-beagle 

cat >>$HOME/.swift/swift.properties <<END
# Properties for Swift Tutorial
sites.file=sites.xml
tc.file=apps

# Data and status staging modes:
use.provider.staging=true
provider.staging.pin.swiftfiles=false
use.wrapper.staging=false
status.mode=provider

# For better debugging:
wrapperlog.always.transfer=true
execution.retries=0
lazy.errors=false
sitedir.keep=true
file.gc.enabled=false
file.gc.enabled=false
END


