#! /usr/bin/env python

import sys
import os

x = y = z = 0
n = filenum = 0

# default param values, may be overridden by .params file (argv[1]) :

minx = 0.0
maxx = 5.0
miny = 0.0
maxy = 5.0
minz = 0.0
maxz = 5.0
incr = 1.0
tuplesPerFile = 25
filePrefix = "seq"
outDir = "out"
maxFiles = 100000

# xrange function for floats

def xfrange(start, stop, step):
  while start < stop:
    yield start
    start += step

# read in user params if specified

if len(sys.argv) > 1:
  execfile(sys.argv[1]) in globals()
    
if len(sys.argv) > 2:
  runDir = sys.argv[2]
else:
  runDir = "missingRunDir";
    
# Generate sequences of point values in the parameter space

os.chdir(runDir)
os.system("mkdir -p " + outDir)

for x in xfrange(minx,maxx,incr):
  for y in xfrange(miny,maxy,incr):
    for z in xfrange(minz,maxz,incr):
      if n % tuplesPerFile == 0 :
        filename = outDir + "/" + filePrefix + ("%05d" % filenum)
        print filename
        of = file(filename,"w")
        filenum += 1
      print >> of, x, y, z
      n += 1
