#!/bin/bash -e

# This assumes that docs will be built on communicado or bridled
~davidk/asciidoc-8.6.4/asciidoc -a toc -a toplevels=2 -a stylesheet=~davidk/swift-0.94/cog/modules/swift/docs/stylesheets/asciidoc.css -a max-width=800px -o tutorial.html README
