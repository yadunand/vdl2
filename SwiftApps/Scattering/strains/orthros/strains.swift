type file;

app (file o, file e, file fb, file nrd, file ori) strains (file pos, file extra, file param, file spotsti)
{
   strains stdout=@o stderr=@e;
}

file extrainfo <"InputAllExtraInfoFittingAll.csv">;
file params <"paramstest.txt">;

int spots[] = readData("spots.txt");

foreach i in spots {
   file SpotsToIndex <single_file_mapper; file=@strcat("SpotsToIndex.", i, ".csv")>;
   SpotsToIndex=writeData(i);

   string spotlong = @strcut(@strcat("0000000", i), "([0-9]........$)");
   file bestpos <single_file_mapper; file=@strcat("Output/BestPos_", spotlong, ".csv")>;
   file fitbest <single_file_mapper; file=@strcat("Results/FitBest_", spotlong, ".csv")>;
   file nrdiff <single_file_mapper; file=@strcat("Results/NrDiffIndexerFit_", spotlong, ".csv")>;
   file orient <single_file_mapper; file=@strcat("Results/OrientPosIndexer_", spotlong, ".csv")>;

   file olog <single_file_mapper; file=@strcat("logs/o.", i, ".log")>;
   file elog <single_file_mapper; file=@strcat("logs/e.", i, ".log")>;
   (olog, elog, fitbest, nrdiff, orient) = strains(bestpos, extrainfo, params, SpotsToIndex);
}

