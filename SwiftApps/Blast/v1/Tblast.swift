import files;
import io;
import string;
import sys;

app (file out) formatdb (file i) {
  "/root/bin/formatdb.sh" i out;
}

app (file o) blastapp (file i, file d, string p, string e, string f, file db) {
 "/root/bin/blastall.sh" i d p e f db o;
}

app (file o) blastmerge (file o_frags[]) {
  "/root/parallelblast_2.0.9/blastmerge" o o_frags;
}

(file o[]) split_database(file d, int n) "turbine" "0.1"
[
"""
set f [ swift_filename &<<d>> ];
exec -ignorestderr /root/parallelblast_2.0.9/fastasplitn $f <<n>>;
set L [ glob frag* ];
turbine::swift_array_build <<o>> $L file;
"""
];

main{
 string prog_name="blastp";

 string expectation_value="0.1";
 string filter_query_sequence="F";
 int np=toint(argv("np"));
 file out[];

 nr="/root/blast/nr.trm";
 seq="/root/blast/sequence.seq";

 file formatdbout[];
 //file outfile <"output.html">;
 file dbin=input_file(nr);
 file query_file=input_file(seq);
 file partition[];
 partition=split_database(dbin, np);

 foreach i in [0:np-1] {
  file t<sprintf("frag%i.tgz", i)> = formatdb(partition[i]);
  formatdbout[i] = t; 
  file q<sprintf("blastout%i.html", i)> = blastapp(query_file, partition[i], prog_name, expectation_value, filter_query_sequence, formatdbout[i]);
  out[i] = q;
 }
 file blast_output_file<"output.html"> = blastmerge(out);
}
