type file;

type fasta;
type query;

string blast_type      = arg("p", "blastp");
string database        = arg("d", "nr.sub1");
string query           = arg("q", "sequence.seq");
string num_partitions  = arg("n", "10");
string expectation     = arg("e", "0.1"); 
string filter_query    = arg("F", "F");

tracef("database=%s\n", database);

query  query_file <single_file_mapper;file=query>;

app (file dblist) split_database (string d, string n){
  bash "-c" strcat("SPLITFRAGTEMPLATE=/tmp/db%3.3d fastasplitn ", d, " ", n, " 2>&1 ",
                   "| grep Opening | sed -e 's/^.* //'") stdout=@dblist;
}

app (file o) fblast(fasta d, query i, string p, string e, string f){
  fblastall @d "-p" p "-i" @i "-d" "fmtdb" "-o" @o "-e" e "-m" "0" "-F" f;
}

app (file o) blastmerge(file o_frags[]){
  blastmerge "-t" @o filenames(o_frags);
}

# Split the database, file names into db[] array

file dbparts <"dbparts">;
dbparts=split_database(database, num_partitions);

string db[] = readData(dbparts);
trace(db);

# BLAST the query against each database partition

file out[];
foreach partname,i in db {
  fasta dbpart<single_file_mapper; file=partname>;
  out[i]=fblast(dbpart, query_file, blast_type, expectation, filter_query);
}

# Merge the BLAST outputs

file merged <single_file_mapper;file=arg("o", "merged.txt")>;
merged = blastmerge(out);
