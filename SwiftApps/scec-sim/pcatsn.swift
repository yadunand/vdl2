type file;

app (file o, file e) bash (file script)
{
  bash @script stdout=@o stderr=@e;
}

file input_files[] <filesys_mapper; location="scripts", pattern="*.sh">;

foreach a in [1:@toint(@arg("n", "1"))] {
   foreach i in input_files {
      file output_file <single_file_mapper; file=@strcat(@filename(i), ".", a, ".out")>;
      file error_file <single_file_mapper; file=@strcat(@filename(i), ".", a, ".err")>;
      (output_file, error_file) = bash(i);
   }
}
