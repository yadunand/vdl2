#!/usr/bin/perl
use strict;

sub getScript {
   my $ruptureFiles = shift;
   my $sgtFiles = shift;
   my $script_contents;
   local $/;
   
   open(SCRIPT, "scec-sim.sh") || die "Unable to open scec-sim.sh";
   $script_contents = <SCRIPT>;
   close(SCRIPT);
   
   $script_contents =~ s/_RUPTUREFILE_ARRAY_HERE_/$ruptureFiles/g;
   $script_contents =~ s/_SGTFILE_ARRAY_HERE_/$sgtFiles/g;
   return $script_contents;
}

my $records_per_script=shift;
my $rupture_prefix="/gpfs/pads/swift/aespinosa/science/cybershake/RuptureVariations";
my $script_prefix="scripts/scec-sim";

my $inRuptures=0;
my $num_records=0;

open(RUP_FILE, "rup_map.txt") || die "Unable to open rup_map.txt";

my $sgtFiles = "";
my $ruptureFiles="";
my $rupture_prefix_2;
my $rupture_counter=0;

while(<RUP_FILE>) {
   chomp;
   
   if(/======/) { 
      $num_records++; 
      $ruptureFiles .= "\"";
      $sgtFiles .= "\"";
      if($num_records < $records_per_script) {
         $ruptureFiles .= ", \"";
         $sgtFiles .= ", \"";
      }
   }

   if($num_records >= $records_per_script) { 
      open(SCRIPT_OUTPUT, ">$script_prefix$rupture_counter.sh") || die "Unable to write script";
      print SCRIPT_OUTPUT getScript($ruptureFiles, $sgtFiles);
      close(SCRIPT_OUTPUT);
      $ruptureFiles = "";
      $sgtFiles = "";
      $num_records=0;
      $rupture_counter++;
   }

   # Rupture Location ==> 7_0
   if(/Rupture Location/) {
      my $nums = (split(' ', $_))[-1];
      (my $dir1, my $dir2) = split('_', $nums);
      $rupture_prefix_2 = "$rupture_prefix/$dir1/$dir2";
   }
   if(/Ruptures/) { $inRuptures=1; }
   if(/Sub SGT/) { $inRuptures=0; }

   if($inRuptures) {
      my $filename = (split)[7];
      if($filename) {
         if($ruptureFiles) {
            $ruptureFiles .= " $rupture_prefix_2/$filename";
         } 
         else {
            $ruptureFiles = "\"$rupture_prefix_2/$filename";
         }
      }
   }

   else {
      my $filename = (split)[7];
      if($filename) {
         if($sgtFiles) {
            $sgtFiles .= " $filename";
         }
         else {
            $sgtFiles = "\"$filename";
         }  
      }
   }
}

close(RUP_FILE);
