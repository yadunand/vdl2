#!/bin/bash -x

getTempfile() {
   DIR=${1-.}
   TMPFILE=$( mktemp -p $DIR )
   TMPFILE=$( basename $TMPFILE )
   echo $TMPFILE
}

# Retry command: command, maxtries, initial delay
retryCommand() {
   command=$1   
   maxTries=$2
   delay=$3

   nTries=1
   status=256

   until [ $status == 0 ] ; do
      echo About to run $command
      $command
      status=$?

      if [ $status != 0 ] ; then
         echo "Failed (exit code $status)... retry $nTries"
         sleep $(( $delay * $nTries))
      else
         break
      fi

      nTries=$(($nTries + 1))
      if [ $nTries -gt $maxTries ] ; then
         echo "Number of re-tries exceeded. Exit code: $status"
         exit $status
      fi
done
}

getFile() {
   filename=$1;
   tag=$2;
   mkdir -p ruptures

   getCommand="/usr/bin/time -f $tag:%e \
      globus-url-copy $GRIDFTP_OPTS $GRIDFTP/$filename \
      file://$PWD/ruptures/$(basename $filename)" 

   retryCommand "$getCommand" 5 10 &
}

runProcess() {
   isPositive=$RANDOM
   let "isPositive %= 2"
   
   offset=$RANDOM
   let "offset %= 60"

   timeToSleep=300

   if [ $isPositive == "1" ]; then
      let "timeToSleep += offset"
   else
      let "timeToSleep -= offset"
   fi

   mkdir -p ruptures
   outputFile=$(getTempfile ruptures)
   dd if=/dev/urandom of=ruptures/$outputFile bs=1M count=10

   sleep $timeToSleep

   myCommand="/usr/bin/time -f OutputTime:%e \
      globus-url-copy $GRIDFTP_OPTS file://$PWD/ruptures/$outputFile \
      $GRIDFTP/gpfs/pads/projects/CI-CCR000013/davidk/osg-store/$outputFile"
   retryCommand "$myCommand" 5 10   
}

echo Start time: $( date )
echo Hostname is: $( hostname -f )
echo Usage $( w | head -1 )
echo Disk usage $( df -h )
echo Current directory: $PWD
echo Environment variables
env
echo

# Parameters that control gridftp transfers
STAGGER_VALUE=300
GRIDFTP="gsiftp://gridftp.pads.ci.uchicago.edu:2812"
TCPBS=16777216
BS=16777216 
PARALLELISM=16
#GRIDFTP_OPTS="-blksize $BS -debug 2 -parallel $PARALLELISM -passive -tcpbuf $TCPBS"
GRIDFTP_OPTS=""

# These arrays get populated by parser.pl
RUPTUREFILE_ARRAY=(_RUPTUREFILE_ARRAY_HERE_)
SGTFILE_ARRAY=(_SGTFILE_ARRAY_HERE_)

LENGTH=${#RUPTUREFILE_ARRAY[@]}
if [ $LENGTH -ne ${#SGTFILE_ARRAY[@]} ]; then
   echo Array file mismatch
   exit 1
fi

# Stagger
SLEEPTIME=$RANDOM
let "SLEEPTIME %= $STAGGER_VALUE"
sleep $SLEEPTIME
 
for i in $( seq 0 $LENGTH)
do
   RLIST=${RUPTUREFILE_ARRAY[$i]}
   files=$( echo $RLIST | sed s/,//g )
   for file in $files
   do
      getFile "$file" "RuptureInput" &
   done
   wait

   SLIST=${SGTFILE_ARRAY[$i]}
   files=$( echo $SLIST | sed s/,//g )
   for file in $files
   do
      getFile "$file" "SGTInput" &
   done
   wait
   
   runProcess
done

echo Stop time: $( date )

exit 0

