function[P]=Prfq2(k)

%works for nq=2 only. SPEEDED UP VERSION OF Prf4a
global rho grz eta


%cc=((1-la)/(1-la^(nq-1)));
if abs(rho)<10^-5
    P(1,:)=min(max(1-k^eta*grz.^(1-eta),.01),.99);      %prob. of failure for C-D
else

P(1,:)=min(max(1-(eta*k^rho+(1-eta)*grz.^rho).^(1/rho),.01),.99);      %prob. of failure
end

P(2,:)=1-P(1,:);

