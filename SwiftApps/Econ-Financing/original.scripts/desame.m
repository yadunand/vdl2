function[y]=desame(x)

n=length(x);
ind=[1:n];

xs=sort(x);
xd=diff(xs);
in1=ind(xd>10^-5);

y=[xs(in1),xs(n)];
