function[u]=u(c,z)

global sig gam xi


%u=-5*ones(1,length(c));

%u=max(-5,(1-sig)^-1*c.^(1-sig)-z.^ga);
u=(1-sig)^-1*c.^(1-sig)-xi*z.^gam;

if abs(sig-1)<10^-4
    u=log(c)-xi*z.^gam;
end
u(c<0)=-5;