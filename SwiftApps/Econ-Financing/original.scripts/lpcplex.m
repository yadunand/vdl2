% LPCPLEX - Solves a linear program (LP) using the CPLEX solver.
%
% LPCPLEX solves the linear programming problem:
%
%       min  c'*x
%       s.t  A*x ? b
%            l <= x <= u
%
% where ? can be =, <= , or >=.
%
% Syntax:
% [obj,x,lambda,status,colstat,it] = lpcplex(c,A,b,l,u,le,ge,maxIter)
%
% c, A, and b are required arguments.
% If l and u are omitted, x is assumed non-negative.
% le and ge are indices corresponding to constraints with <= and >=,
%   respectively.
% all constraints are equality by default
% maxiter is 10000 by default.
% This function calls the mex-function lpcplexc.dll which uses cplex71.dll to 
% carry out the optimization.
% Based on LP_CPLEX.M by David R. Musicant
% Updated for CPLEX 7.1 and optimized by Alexander Karaivanov

function [obj,x,lambda,status,colstat,it] = lpcplex(c,A,b,l,u,le,ge,maxIter)

  % Initial error checking
  [m,n]=size(A);
    
  c = c(:);
  mc = length(c);
  if mc ~= n
    error('A and c are of incompatible sizes.');
  end
  
  b = b(:);
  mb = length(b);
  if mb ~= m
    error('A and b are of incompatible sizes.');
  end
  
  % Assign default values if not provided
  if nargin<4
    l = zeros(mc,1);
  else
    l = l(:);
  end

  if nargin<5
    u = inf*ones(mc,1);
  else
    u = u(:);
  end

  if nargin<6
    le = [];
  else
    le = le(:);
  end
  
  if nargin<7
    ge = [];
  else
    ge = ge(:);
  end
    
  if nargin<8
    maxIter = 10000;
  end

  % More error checking
  if length(l) ~= n,
    error('A and l are of incompatible sizes');
  end;

  if length(u) ~= n,
    error('A and u are of incompatible sizes');
  end;

  if max(le) > m | length(le) > m,
    error('A and le are incompatible');
  end;

  if max(ge) > m | length(ge) > m,
    error('A and ge are incompatible');
  end;
 
  % Make the CPLEX call
  [obj,x,lambda,status,colstat,it] = lpcplexc(c,A,b,l,u,le,ge,maxIter);

  % Output based on status
  switch(status)
   case 1,
    %disp(sprintf('Optimal solution found.'));
   case 2,
    disp(sprintf('Problem infeasible'));
   case 3,
    %disp(sprintf('Problem unbounded.'));
   case 4,
    disp(sprintf('Objective limit exceeded in Phase II.'));
   case 5,
    disp(sprintf('Iteration limit exceeded in Phase II.'));
   case 6,
    disp(sprintf('Iteration limit exceeded in Phase I.'));
   case 7,
    disp(sprintf('Time limit exceeded in Phase II.'));
   case 8,
    disp(sprintf('Time limit exceeded in Phase I.'));
   case 9,
    disp(sprintf('Problem non-optimal, singularities in Phase II.'));
   case 10,
    disp(sprintf('Problem non-optimal, singularities in Phase I.'));
   case 11,
    disp(sprintf('Optimal solution found, unscaled infeasibilities.'));
   case 12,
    disp(sprintf('Aborted in Phase II.'));
   case 13,
    disp(sprintf('Aborted in Phase I.'));
   case 19,
    disp(sprintf('Infeasible or unbounded.'));
  end;


