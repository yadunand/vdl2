function[cr]=FIkkqTD(param)

%sorry, see comments in MHkkqTD code. This one is basically same.
 

warning off
global nk nw nz nt nq grz grk grq N indk indw grw
global sig gam la rho be del eta xi ike
global kmin kmax wmin wmax mombaseE rnums unifr2
global gmm cgrid newsim sol n rnums0 rnumsi
global imaxc unirnums oldpars


%rnums is the RNs for errors (from rnums3)
%rnums0 is RNs for wdraws (from baseMHlq2)
%load curr


%Dimensions
nk=5;       %capitals
%nw=11+nk;      %promised utilities
nw=5;      %promised utilities

nt=7;       %transfers
nq=2;       %outputs
nz=3;       %efforts
vtol=10^-6;
%xi=0.1;
%gam=2;


stde=param(1);

if oldpars==1
sig=param(2);
gam=param(3);
eta=0.8;
xi=0.1;
elseif oldpars==0
    eta=param(2);
    xi=param(3);
    sig=0.5;
    gam=2;
else
sig=param(2);
gam=param(3);
eta=param(7);
xi=param(8);
    
end

rho=param(4);
meanw=param(5);
stdw=param(6);

%Parameters

%1. production
% at=.5;
% bt=.5;
la=.5;
del=0.05;
%sig=0.5;
%gam=2;
%eta=0.8;
%del=1;
%rho=0;

%2. preferences
R=1/.95;
be=.95;
qmin=0.005;
qmax=0.13;
cmax=0.1;
%qmax=3;
%sig=0.5;
%sig=0.5;
%ga=2;

%Grids
br=0;
kmin=0;
%kmin=.3;
kmax=1;
grk=[0,.03,.2,.4,1];
%grk=linspace(kmin,kmax,nk);

tg1=[];
for i=1:nk
    tg1=[tg1,grk-(1-del)*grk(i)];
end
%tg1=tg1(tg1>=0);

tmin=-.95;
tmax=1.1;
%tmax=3;
grt=sort(desame([tg1,linspace(tmin,tmax,nt)]));
nt=length(grt);



%qmin=0.5;
%qmax=3;                 %3 or 1 if lowq
grq=linspace(qmin,qmax,nq);

%  zmin=10^-4;
%  zmax=1-10^-4;

zmin=0.01;
zmax=1;

grz=linspace(zmin,zmax,nz);

T=kron(ones(1,nz*nq*nw*nk),grt);
Kp=kron(ones(1,nw),kron(grk,ones(1,nq*nz*nt)));

cmin=0;

%only for sigma>1
%cc=T-Kp;
%cmin=min(cc(cc>0));

%wmin=(1/(1-be))*u(cmin,zmin);

wmin=10^-4;                %ONLY NOW
%wmin=(1/(1-be))*u(cmin,zmax);
%wmin=39.7;
%wmin=-100;
%wmax=30;
wmax=(1/(1-be))*u(cmax,zmin);
grw=linspace(wmin,wmax,nw);
%wmin=32;
%wmax=43;

%grw=[linspace(wmin,10,10),linspace(13,wmax,10)];

%Stuff to save
gridsz=[nk,nw,nt,nq,nz];
gridmax=[tmax,kmax,qmax,zmax];
gridmin=[tmin,kmin,qmin,zmin];
params=[la del R be sig gam rho];


indk=1:nk;
indw=1:nw;

%number of variables
N=nt*nz*nq*nw*nk;

%Construct matrices
%order of change is: t,q,z,k',w'
Q=kron(ones(1,nw*nk*nz),kron(grq,ones(1,nt)));
Wp=kron(grw,ones(1,nk*nq*nt*nz));
Z=kron(ones(1,nw*nk),kron(grz,ones(1,nq*nt)));
ind=1:N;


%Objective

 %adding up
Aeq2=ones(1,N);
beq2=1;
beq3=zeros(nz*nq,1);
b=zeros(nz*(nz-1),1);

%options=optimset('Display','off');

crit=1;
crit2=1;
step=0;

% load resNZOro-1
% tam2=v1m;
%tam2=-(tmax/(1-1/R))*ones(nw,nk);
%tam2=zeros(nw*nk,1);
%tam2=(-tmax/(1-1/R))*ones(nw*nk,1);
load currvOC



while crit>vtol && step<20
    step=step+1;
   %load matr
    tam1=tam2;
    br0=0;
              %initial guess
%         tam1=chevalc2(nd,w2,k2,c1);

%         tmm1=chevalc2(nd,grw,grk,c1);
        Vp=kron(tam1',ones(1,nt*nz*nq));
        
        f=-Q+T-(R^-1)*Vp;
%         lb=zeros(size(f));
%         ub=ones(size(f));
% save matr T Kp Wp Z Q
% clear Q T Kp Wp Z
 fff=zeros(nw,nk);
 
 

 for j=1:nk      %k
     %disp(j)
     
%      load matr
%      pack
    k=grk(j);
    
    P=Prfq2(k); 
    C=T+(1-del)*k-Kp;
    

    zz=ind(C>=0); % & Kp>=(1-del)*k);   %position of zpe's
    zn=length(zz);
    
%     Cz=C(zz);
%     Tz=T(zz);
     Kpz=Kp(zz);
     Wpz=Wp(zz);
     uz=Q(zz)-T(zz);           %instantaneous utility
     Qz=Q(zz);
     
     Cz=C(zz);
%     Qz=Q(zz);
%     Zz=Z(zz);
%Vpz=Vp(zz);
   
 %promise keeping    
Aeq1=u(C(zz),Z(zz))+be*Wp(zz);
clear C


%mother nature
 br=0;

 Aeq3=sparse(zeros(nz*nq,zn));
 for iz=1:nz
    rhs=repmat([zeros(1,(iz-1)*nt*nq),ones(1,nt*nq),zeros(1,nq*nt*nz-nt*nq-(iz-1)*nt*nq)],1,nk*nw);
    
    for iq=1:nq
        br=br+1;
        
        lhs=repmat([zeros(1,(br-1)*nt),ones(1,nt),zeros(1,nq*nz*nt-nt-(br-1)*nt)],1,nk*nw);
     
        Aeq3(br,:)=lhs(zz)-P(iq,iz)*rhs(zz);
       
    end
end
%Aeq3=sparse(Aeq3(:,zz));
clear lhs rhs



%ICC
% A=sparse(zeros(nz*(nz-1),length(zz)));
% 
% br2=0;
% for iz=1:nz
%     for jz=[1:iz-1,iz+1:nz]
%        br2=br2+1;
%   lhs=repmat([zeros(1,(iz-1)*nt*nq),ones(1,nt*nq),zeros(1,nz*nt*nq-nt*nq-(iz-1)*nt*nq)],1,nk*nw).*[u(T+(1-del)*k-Kp,grz(iz)*ones(1,N))+be*Wp];
%   tm1=kron(P(:,jz)',ones(1,nt))./kron(P(:,iz)',ones(1,nt));
%   rhs=repmat([zeros(1,(iz-1)*nt*nq),tm1,zeros(1,nz*nt*nq-nt*nq-(iz-1)*nt*nq)],1,nk*nw).*[u(T+(1-del)*k-Kp,grz(jz)*ones(1,N))+be*Wp];
%    
%   A(br2,:)=rhs(zz)-lhs(zz);
%   %A(br,zz)=0;
%  
% end
% end

%eval(['save ICCx',num2str(j),' A lhs rhs tm1 P'])

%A=sparse(A);
%clear T Kp Wp
%clear lhs rhs tm1
%eval(['save ICCconMH',num2str(j),' A'])
%clear A zz P
%mother nature
% eval(['load MNconMH',num2str(j)])
% %Aeq3=sparse(Aeq3(:,zz));
%    
% 
% %ICC
% eval(['load ICCconMH',num2str(j)])

%  A=[];
%  b=[];
   A1=sparse([Aeq1;Aeq2(zz);Aeq3]);  
   le=[];
   
   clear Aeq1 Aeq3
   %A1=sparse([Aeq1;Aeq2(zz);Aeq3]);  
   
   %le=[];
   
   
    for i=1:nw    %w
        
        br0=br0+1;
       %current state
    w=grw(i);
       
    %disp(br0)
    beq1=w;
 
  b1=[beq1;beq2;beq3];
  %beq=[beq1;beq2;beq3];
  
   %b1=[beq1;beq2;beq3];
        
     
      try
      [v1,x,lambda,flag1,colstat,it] = lpcplex(f(zz),A1,b1,zeros(size(zz)),ones(size(zz)),le);  
      catch
          cr=50;
          return
      end
      %[x,v1] = linprog(f(zz),[],[],A1,beq,zeros(size(zz)),ones(size(zz))); 
   
  sol(i,j).x=x(x>0);
  sol(i,j).kp=Kpz(x>0);
  sol(i,j).wp=Wpz(x>0);
  sol(i,j).c=Cz(x>0);
  sol(i,j).cL=Cz(x'>0 & Qz==grq(1));
  sol(i,j).cH=Cz(x'>0 & Qz==grq(2));
  sol(i,j).q=Qz(x>0);
  sol(i,j).prL=sum(x(Qz==grq(1)));
  sol(i,j).xL=x(x'>0 & Qz==grq(1));       %probs of Ql
  sol(i,j).xH=x(x'>0 & Qz==grq(2));       %probs of Qh
  
  Pvec(i,j)=uz(x>0)*x(x>0);
      %str(i,j).x=x(x>0);
%       disp([w,k])
%         disp(Cz(x>0))
%         disp(Tz(x>0))
%         disp(Kpz(x>0))
%         disp(Wpz(x>0))
%         disp(Qz(x>0))
%         disp(Zz(x>0))
%         disp(Vpz(x>0))
%         disp(-v1)
%         pause
        %disp([max(x(C<0)),-v1,w,k])
        %pause
        if flag1~=1 && flag1~=11
            fff(i,j)=1;
            sol(i,j).x=1;
            sol(i,j).kp=k;
            sol(i,j).wp=wmax;
        end
         
%           else
      %   v1m(i,j)=-v1;
%     end
    jj=j+nk*(i-1);          %k changes first
    Qmat(jj,:)=nextmatocPI(sol,i,j)';

    end
    
clear zz    
    
end
% v1m(nw,:)=-tmax/(1-1/R);
fff2=fff';
fff2=fff2(:);
Pvec=Pvec';  
Pvec=Pvec(:); %stack chg k first

Pvec(fff2==1)=qmin-tmax;
    
tam2=inv(eye(nk*nw)-be*Qmat)*Pvec;

tmq2=tam2(1:nk);
tmq2(fff2(1:nk)==1)=0;
tam2(1:nk)=tmq2;

clear Pvec
%  v1m
%  pause
    %tam2(fff2==1)=tam1(fff2==1);                %here if fff is bad value remains zero, not good.
    crit=norm(tam1(fff2==0)-tam2(fff2==0));
   
end
%v1m=reshape(tam2,nk,nw)';
%tam2=v1m;
save currvOC tam2



err=stde*max(grk)*rnums';          %errors
erri=stde*max(grk)*rnumsi;

wdraw=meanw+stdw*rnums0;

%put them on the grid
indw=[1:nw];
indq=[1:nw*nk];
indk=[1:nk];

%freqsum=[0,cumsum(freqbaseE)];
%freqsum(end)=freqsum(end)+1;
%freqk=[0,freqsum(nw:nw:nw*nk)];
%freqk(end)=freqk(end)+1;
distc=zeros(nk*nk*nq,1);   %will store the frequencies of (k,k',q) here

try

for i=1:n
    [jj,kk]=min(abs(wdraw(i)-grw));   %closest point
    iw(i)=indw(kk);         %the index of w
    %ik(i)=floor((i-1)/(n/nk))+1;   %the index of k (with the error)
    %ttt0=histc(i,freqsum);
            
    kkr(i)=grk(ike(i))-erri(i);         %remove "errors" from k
    ttt=hist(kkr(i),grk);
    ikr(i)=indk(ttt==1);             %the new cell for k
  
    indx=1:length(sol(iw(i),ikr(i)).x);
    xx=[0,cumsum(sol(iw(i),ikr(i)).x)'];
    uu=indx(histc(unifr2(i),xx)>0);      %index in lottery
    
    ike2(i)=indk(hist(sol(iw(i),ikr(i)).kp(uu)+err(i),grk)>0);   %index of k'(with error)
    indLL(i)=1+(sol(iw(i),ikr(i)).q(uu)==grq(2));
    
    %order is k',q,k
    %tmp=ike2(i)+(indLL(i)-1)*nk+nk*nq*(ike(i)-1);
    tmp=ike(i)+(ike2(i)-1)*nk+nk*nk*(indLL(i)-1);     %order exactly as in R file
%     
    %tmp=indcc(i)+(indLL(i)-1)*nc;  %index in cccc
    distc(tmp)=distc(tmp)+1;
end

catch
    cr=50;
    return
end

%stack moments to use in criterion

if newsim
%distc=distkkq(:)/n;
%distc=distckq(:);                   %for LR simulation
momest=[distc([1:imaxc-1,imaxc+1:end])/n];
else
%momest=[distk(1:end-1)'/n;transk2(:)/(n/nk)];
end




penlt=0;

%mombase0=min(1,max(0,mombase0));           %CRUCIAL!!!
%momest
%pause
pen=0;
indz=find(mombaseE>0);

if gmm==1
cr=sum(((momest-mombaseE).^2)./max(momest,mean(momest)));
cr=cr+penlt;
%cr=(momest-mombase)'*om1*(momest-mombase);
%cr=(momest-mombase)'*eye((nk+1)*(nk-1))*(momest-mombase);
else
%LL criterion


% 
if newsim
%NEW ONE with c,q,k
%mme1=momest(1:nk*(length(cgrid)-1)*2-1);
%mme2=reshape(momest(nk*(length(cgrid)-1)*2:end),nk-1,nk)';
mme1=momest;

% mmb1=mombaseE(1:nk*(length(cgrid)-1)*2-1);
% mmb2=reshape(mombaseE(nk*(length(cgrid)-1)*2:end),nk-1,nk)';
mmb1=mombaseE;

tmp2=(1-sum(mmb1))*log(max(10^-6,1-sum(mme1)));

if sum(mme1)>=1
    disp('blah')
    disp(sum(momest))
   pen=-5;
end

% for i=1:nk
%     tmp2=tmp2+(1-sum(mmb2(i,:)))*log(max(10^-6,1-sum(mme2(i,:))));
% end

else
% mme=reshape(momest,nk-1,nk+1)';             %for distk
% mmb=reshape(mombaseE,nk-1,nk+1)';
% tmp2=0;
% 
% for i=1:nk+1
%     tmp2=tmp2+(1-sum(mmb(i,:)))*log(max(10^-6,1-sum(mme(i,:))));
% end

end


cr=-(sum(mombaseE.*log(max(10^-6,momest)))+tmp2)-pen;

%cr=-(sum(mombase0(indz).*log(momest(indz)))+tmp2)-pen;
end


%momest=mean(momest0,2);
save currm momest param
