function[mat]=nextmatocPI(sol,iw0,ik0)

global nw nk grk grw indk indw



mat=zeros(nw,nk);
le=length(sol(iw0,ik0).x);    %all new positive probs
    %ind=[1:le];
    
    for j=1:le
        kkp=sol(iw0,ik0).kp(j);
      
        wwp=sol(iw0,ik0).wp(j);
        
        %next state
        ik=indk(abs(grk-kkp)<2*10^-4);
        iw=indw(abs(grw-wwp)<2*10^-4);
        mat(iw,ik)=mat(iw,ik)+sol(iw0,ik0).x(j);
    end
        
mat=mat';
mat=mat(:);