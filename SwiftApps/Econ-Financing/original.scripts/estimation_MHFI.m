clear all

%MAIN PROGRAM THAT ESTIMATES THE FI AND MH model regimes using Thai data

%keep random numbers constant
global mombase gmm cgrid newsim sol n newsim gmm rnums0
global nb nk n imaxc rnums nw grw unirnums ike imaxc2
global mombaseE unifr2 rnumsi rnumsJ oldpars panl


datas.g=[0,0];

oldpars=1;    %which parameters are estimated: 0 is new, 1 is old, 2 is both

for testt=[0,1,2];            %various optimization routines, first grid search, 1 is fminsearch, 3 patternsearch
%for testt=[2]

datas(1).str1='MHkkqTD';       %code for MH to run, here uses kk'q Thai data from basekkqTD (1998-99)
datas(2).str1='FIkkqTD';

datas(1).str2='MHmlekkqNT';    %save the grid search results here
datas(2).str2='FBmlekkqNT';

datasx(1).str0='1';
datasx(2).str0='3';
datasx(3).str0='2';

datas(1).str3=['estMHmlekkqNT',datasx(testt+1).str0];    %save estimation result here
datas(2).str3=['estFBmlekkqNT',datasx(testt+1).str0];

vv2=[1:2];    %model selector
%vv2=2;

%test load
datas(1).str4='estMHmlekkqNT1';             %to rerun patternsearch after fminsearch
datas(2).str4='estFBmlekkqNT1';

%custom load
%datas(vv2).str4='estMHmlekkqTD4';
%custom save
%datas(vv2).str3='estMHmlekkqTD5';


%load same random numbers from old times
load baseMHlq2
rnums0=rnums;           %RNs for the w draws


clear rnums stdw meanw freqbase mombase mombase2 lvecbase

%Load the data being matched
load basekkqTD


%LOAD VARIOUS RANDOM NUMBERS USED
load uniferr2
% load uniferr        %try same numbers as in data
% unifr2=unifr;


load rnums3
load rnumsi
load rnumsJ
%load unirnums
load unirnums2           %5000
%load unirnums3


%Parameter bounds
boundsL1=[0,0,0,-Inf,0,0]';

if oldpars==1
boundsH1=[1,5,10,50,20,10]';     %del=0.05 sig, gam

else
boundsH1=[1,1,5,50,20,10]';     %del=0.05  xi, eta
end

nk=5;
nw=5;
nc=length(cgrid);

%vf initial guess
tam2=zeros(nk*nw,1);
save currvOC tam2

%Set maximization options for the various routines
options=optimset('display','iter','maxiter',300,'maxfunevals',500);
%goptions=psoptimset('Display','Iter','CompletePoll','on','MeshContraction',0.67,'MeshExpansion',1.5);
goptions=psoptimset('Display','Iter','CompletePoll','on','MeshContraction',0.67,'MeshExpansion',1.5,...
    'SearchMethod',@GPSPositiveBasisNp1,'Cache','on','CacheSize',10000,'PlotFcn',@psplotbestf);

%goptions1=gaoptimset('Display','iter','StallTimeLimit',200);
fv=[];

for vv=vv2            %run for each regime
  
clear fv
disp('===================================')
disp(['starting new run....',num2str(testt+1)])
disp(datas(vv).str2)
    
gmm=datas(1).g(vv);
newsim=1;

if oldpars
    load parmat5TD   %for sig, theta
else
    load parmat5TD2  %for xi, eta
end

init=1;

%RUN GRID SEARCH FIRST
if testt==0
nn=size(parv,1);
for jj=init:nn
   
      tam2=zeros(nk*nw,1);
      save currvOC tam2

    fv(jj)=feval(datas(vv).str1,parv(jj,:));
    if jj/5==floor(jj/5)
        
        disp(jj)
    disp(['current best is: ',num2str(min(fv(init:end)))])
    end
end
eval(['save ',datas(vv).str2,' fv'])
clear fv
end

if testt<2
eval(['load ',datas(vv).str2])

[fvs,uu]=sort(fv(init:end));

fv2=fvs(fvs==fvs(1));       %all the ones with min crit
n2=length(fv2);
end

%run patternsearch after the fminsearch now
if testt==2
eval(['load ',datas(vv).str4])

n2=1;
param0=x;

end



for i=1:n2
if testt<2
param0=parv(init-1+uu(i),:);
end

disp(param0)

tam2=zeros(nk*nw,1);
save currvOC tam2

%the various routines, the LL can have local extrema so I pick best result
if testt==0
[x,fval]=fminsearch(datas(vv).str1,param0,options);
else
%[x,fval]=fminunc('mh32fbEST',param0,options,mombase);
%[x,endPop,bPop,traceInfo]=ga(bounds,'mh32fbESTga',[],inPop,[1e-4 1 1],'maxGenTerm',40)
[x,fval,flag]=patternsearch(eval(['@',datas(vv).str1]),param0,[],[],[],[],boundsL1,boundsH1,[],goptions);
end

xv(i,:)=x;
fv3(i)=fval;
tam2=zeros(nk*nw,1);
save currvOC tam2

cr2=feval(datas(vv).str1,x);          %to get the right moments
disp([cr2,fval])
load currm
mommat(:,i)=momest;
disp([x,fval])
end

[kk,ll]=min(fv3);
momest=mommat(:,ll);
x=xv(ll,:);
fval=fv3(ll);

%Save results
eval(['save ',datas(vv).str3,' x mombaseE momest fval'])

end

end


