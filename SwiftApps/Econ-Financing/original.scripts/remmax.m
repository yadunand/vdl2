function[yvec,imax]=remmax(xvec)

xvec=xvec(:);
[i,u]=max(xvec);
yvec=[xvec(1:u-1);xvec(u+1:end)];
imax=u;