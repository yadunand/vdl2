type file;

// Lammps app definition
app (file _dump) lammps_app (string _lammps_exe, file _in, file _geo, file _eam, file _tinmeam, file _libmeam)
{
   bgsh _lammps_exe "-in" @_in; 
}

// Pizza app definition
app (file _xyz) pizza_app (file _pizza_src, file _dump)
{
   python @_pizza_src @_dump @_xyz;
}

// Pasta app definition
app (file _fdf) pasta_app (file _pasta_src, file _xyz, file _fdf_tmp)
{
  bash @_pasta_src @_xyz @_fdf_tmp @_fdf;
}

/* smeagol lead inputs
Au.psf Au.sic.psf input.fdf
O.psf O.sic.psf Ti.psf Ti.sic.psf */

//Smeagol lead app definition
app (file _tindm, file _tinhst, file _bulklft, file _bulkrgt, file _out) smeagol_lead_app (string _smeagol_exe, file _input_leadsfdf, file[] _psf)
{
   bgsh _smeagol_exe stdin=@_input_leadsfdf stdout=@_out; 
}

//Smeagol transport V0 app definition
app (file _out) smeagol_v0_app ( string _smeagol_exe, file _au3dm, file _au3hst,
                                 file _bulklft, file _bulkrgt,
                                 file _inputfdf, file[] _psf)
{
  bgsh _smeagol_exe stdin=@_inputfdf stdout=@_out; 
}

/* == Generate fdfs app definition == */
app (file _fdf) gen_fdf(file _genfdf_src, float _r, file _infdf)
{
  bash @_genfdf_src _r @_infdf stdout=@_fdf;
}

/* == Smeagol transport Vn app definition == */
app (file _trc_out, file _out) smeagol_vn_app (string _smeagol_exe, file _au3dm, file _au3hst, file _bulklft, file _bulkrgt, file _inputfdf, file[] _psf )
{
  bgsh _smeagol_exe stdin=@_inputfdf stdout=@_out;
}

/* == Input files definitions: Make sure these files are available in the current directory == */
file lammps_in <"in.lammps">;
file lammps_geo <"TiN-Ta-HfO2-TiN.geo">;
file lammps_eam <"Ta.lammps.eam">;
file tin_meam <"TiN.meam">;
file lib_meam <"library.meam">;

file fdf_leads<"input_leads.fdf">;
file fdf_v0<"input_t0.fdf">;

file psf[] <filesys_mapper; pattern="*.psf">;

file pizza_src<"lastlammpsdump2xyz.py">;
file pasta_src<"pasta">;
file genfdf_src<"genfdf">;

//string lammps_exe="/soft/applications/lammps/24Apr13/lmp_bgq_xlomp";
string lammps_exe="/home/knight/public/lammps/5Nov14/lmp_bgq_xlomp";
string smeagol_exe="/gpfs/mira-fs1/projects/Oxygen_defects_ALCC/share/bin/smeagol-1.2_csg";


/* == Output files definitions == */
file xyz <"forsmeagol.xyz">;
file lammps_dump <"lammps.dump">;
file lammps_out <"lammps.stdout">;
file fdf_proc<"input_proc.fdf">;

file smeagol_lead_out <"smeagol_out/smeagol.lead.out">;
file smeagol_transport_out<"smeagol_out/smeagol.transport.out">;
file tindm <"TiN.DM">;
file tinhst <"TiN.HST">;

file bulklft <"bulklft.DAT">;
file bulkrgt <"bulkrgt.DAT">;

file tio2auhm<"TiO2Au.HM">;
file tio2audm<"TiO2Au.DM">;

string Vs[]=["0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9"];


//Invoke lammps
(lammps_dump) = lammps_app (lammps_exe, lammps_in, lammps_geo, lammps_eam, tin_meam, lib_meam);

//Invoke smeagol lead
(tindm, tinhst, 
 bulklft, bulkrgt,
 smeagol_lead_out) = smeagol_lead_app (smeagol_exe, fdf_leads, psf);

//Invoke pizza
(xyz) = pizza_app(pizza_src, lammps_dump);


//Invoke smeagol transport V0
 (smeagol_transport_out) = smeagol_v0_app(smeagol_exe, tindm, tinhst, bulklft, bulkrgt, fdf_v0, psf);

//Invoke pasta
(fdf_proc) = pasta_app(pasta_src, xyz, fdf_v0);

foreach v,i in Vs
{
// == generate fdfs for different voltages ==
string fdfname = strcat("input_", v, ".fdf");
file fdf_proc_vn <single_file_mapper; file=fdfname>;

fdf_proc_vn = gen_fdf(genfdf_src, toFloat(v), fdf_proc);

string outname = strcat("smeagol_out/bias_", v, ".out");
file smeagol_transport_outn <single_file_mapper; file=outname>;

file smeagol_trc_out <single_file_mapper; file=strcat("0.TiO2Au_", v, ".TRC")>;

(smeagol_trc_out, smeagol_transport_outn) = smeagol_vn_app(smeagol_exe, tindm, tinhst, bulklft, bulkrgt, fdf_proc_vn, psf);

}
