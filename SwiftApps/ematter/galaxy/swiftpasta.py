#!/usr/bin/env python

"""
This is the Swift driver for Galaxy. It receives inputs from the Swift
Galaxy tool and depending on the options specified, builds a
canonical Swift/K or Swift/T script and runs over specified computational
environment.

swiftpasta.py
 "${_src}"
 "${_xyz}" 
 "${_infdf}"
 "${_outfdf}"
 "${local_or_compute}"

"""

import subprocess
import sys
import os
import distutils.spawn
import traceback
import fnmatch
import re
from os.path import expanduser


def genargs(args):
    for a in args:
        yield a

def main():
    myargs = genargs(sys.argv)
    try:
        this=next(myargs)
        src = next(myargs)
        xyz = next(myargs)
        infdf = next(myargs)
        outfdf = next(myargs)
        local_or_compute = next(myargs)
    except:
        traceback.print_exc()
        sys.exit(1)

    homedir = expanduser("~")
        
    baseloc = homedir+"/SwiftApps/ematter/galaxy"
    #which swift
    swift = homedir+"/swift-0.95/cog/modules/swift/dist/swift-svn/bin/swift"
    swiftargs="-src="+src+" -xyz="+xyz+" -infdf="+infdf+" -outfdf="+outfdf
    siteval=baseloc+"/sites.local.xml"

    if local_or_compute == "compute":
        siteval=baseloc+"/sites.blues.xml"

    #Invocation
    exitcode=subprocess.check_call([swift, "-sites.file", siteval, "-tc.file", baseloc+"/apps", "-config", baseloc+"/cf", baseloc+"/pasta.swift", swiftargs])
    #print exitcode

if __name__=='__main__':
    main()
