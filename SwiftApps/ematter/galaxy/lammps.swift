type file;


/* == App function definitions == */

/* == Lammps app definition == */
app (file _dump) lammps_app (file _in, file _geo, file _eam)
{
   //lammps "-in" @_in; 
   lmpwrap @_in @_geo @_eam @_dump;
}

file infile <single_file_mapper; file=arg("infile")>;
file geo <single_file_mapper; file=arg("geo")>;
file eam <single_file_mapper; file=arg("eam")>;

file dump <single_file_mapper; file=arg("dump")>;

(dump) = lammps_app (infile, geo, eam);

