#!/usr/bin/env python

"""
This is the Swift driver for Galaxy. It receives inputs from the Swift
Galaxy tool and depending on the options specified, builds a
canonical Swift/K or Swift/T script and runs over specified computational
environment.

  swiftlammps.py
   "${_in}"
   "${_geo}" 
   "${_eam}"
   "${_dump}"
   "${local_or_compute}"

"""

import subprocess
import sys
import os
import distutils.spawn
import traceback
import fnmatch
import re
from os.path import expanduser

def setwdir():
    return subprocess.check_output(["mktemp", "-d", "/tmp/swift-gal.XXXX"])

def genargs(args):
    for a in args:
        yield a

def main():
    myargs = genargs(sys.argv)
    try:
        this=next(myargs)
        infile = next(myargs)
        geo = next(myargs)
        eam = next(myargs)
        dump = next(myargs)
        local_or_compute = next(myargs)
    except:
        traceback.print_exc()
        sys.exit(1)

    homedir = expanduser("~")
        
    baseloc = homedir+"/SwiftApps/ematter/galaxy"
    #which swift
    swift = homedir+"/swift-0.95/cog/modules/swift/dist/swift-svn/bin/swift"
    swiftargs="-infile="+infile+" -geo="+geo+" -eam="+eam+" -dump="+dump
    siteval=baseloc+"/sites.local.xml"

    if local_or_compute == "compute":
        siteval=baseloc+"/sites.blues.xml"

    #Invocation
    exitcode=subprocess.check_call([swift, "-sites.file", siteval, "-tc.file", baseloc+"/apps", "-config", baseloc+"/cf", baseloc+"/lammps.swift", swiftargs])
    #print exitcode

    #Populate output file
    #outlist=subprocess.check_output(["find", outloc, "-type", "f", "-iname", "*.out"])
    #f=open(outlistfile, "wb")
    #f.write("The results files are as follows:\n")
    #f.write(outlist)
    #f.close()

if __name__=='__main__':
    main()
