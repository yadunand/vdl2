type file;


/* == App function definitions == */

/* == Lammps app definition == */
app (file _xyz) pizza_app (file _src, file _dump)
{
   
   //python @_src @_dump @_xyz; 
   pizzawrap @_src @_dump @_xyz;
}

file src <single_file_mapper; file=arg("src")>;
file dump <single_file_mapper; file=arg("dump")>;

file xyz <single_file_mapper; file=arg("xyz")>;


(xyz) = pizza_app (src, dump);

