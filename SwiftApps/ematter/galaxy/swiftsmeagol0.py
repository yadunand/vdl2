#!/usr/bin/env python

"""
This is the Swift driver for Galaxy. It receives inputs from the Swift
Galaxy tool and depending on the options specified, builds a
canonical Swift/K or Swift/T script and runs over specified computational
environment.

  swiftsmeagol0.py
   "${_au3dm}"
   "${_au3hst}"
   "${_aupsf}"
   "${_ausicpsf}" 
   "${_bulklft}"
   "${_bulkrgt}"
   "${_inputfdf}"
   "${_opsf}"
   "${_osicpsf}"
   "${_tipsf}"
   "${_tisicpsf}"
   "${_tio2auhm}"
   "${_tio2audm}"
   "${_out}"
   "${local_or_compute}"

"""

import subprocess
import sys
import os
import distutils.spawn
import traceback
import fnmatch
import re
from os.path import expanduser

def genargs(args):
    for a in args:
        yield a

def main():
    myargs = genargs(sys.argv)
    try:
        this=next(myargs)
        au3dm = next(myargs)
        au3hst = next(myargs)
        aupsf = next(myargs)
        ausicpsf = next(myargs)
        bulklft = next(myargs)
        bulkrgt = next(myargs)
        inputfdf = next(myargs)
        opsf = next(myargs)
        osicpsf = next(myargs)
        tipsf = next(myargs)
        tisicpsf = next(myargs)
        tio2auhm = next(myargs)
        tio2audm = next(myargs)
        out = next(myargs)
        local_or_compute = next(myargs)
    except:
        traceback.print_exc()
        sys.exit(1)

    homedir = expanduser("~")
        
    baseloc = homedir+"/SwiftApps/ematter/galaxy"
    
    #which swift
    swift = homedir+"/swift-0.95/cog/modules/swift/dist/swift-svn/bin/swift"
    swiftargs="-au3dm="+au3dm+" -au3hst="+au3hst+" -aupsf="+aupsf+" -ausicpsf="+ausicpsf+" -bulklft="+bulklft+" -bulkrgt="+bulkrgt+" -inputfdf="+inputfdf+" -opsf="+opsf+" -osicpsf="+osicpsf+" -tipsf="+tipsf+" -tisicpsf="+tisicpsf+" -tio2auhm="+tio2auhm+" -tio2audm="+tio2audm+" -out="+out
    
    siteval=baseloc+"/sites.local.xml"

    if local_or_compute == "compute":
        siteval=baseloc+"/sites.blues.xml"

    #Invocation
    exitcode=subprocess.check_call([swift, "-sites.file", siteval, "-tc.file", baseloc+"/apps", "-config", baseloc+"/cf", baseloc+"/smeagol0.swift", swiftargs])
    #print exitcode

if __name__=='__main__':
    main()
    
