type file;


app (file _tio2auhm, file _tio2audm,
     file _out)                  smeagol0_app ( file _au3dm, file _au3hst, file _aupsf,
                                 file _ausicpsf, file _bulklft, file _bulkrgt,
                                 file _inputfdf, file _opsf, file _osicpsf,
                                 file _tipsf, file _tisicpsf )
{
  mpiexec  "/home/heinonen/smeagol-1.2-Feb-2014/smeagol-1.2_blues/Src/smeagol-1.2_csg" stdin=@_inputfdf stdout=@_out;
}

file au3dm <single_file_mapper; file=arg("au3dm")>;
file au3hst <single_file_mapper; file=arg("au3hst")>;
file aupsf <single_file_mapper; file=arg("aupsf")>;
file ausicpsf <single_file_mapper; file=arg("ausicpsf")>;
file bulklft <single_file_mapper; file=arg("bulklft")>;
file bulkrgt <single_file_mapper; file=arg("bulkrgt")>;
file inputfdf <single_file_mapper; file=arg("inputfdf")>;
file opsf <single_file_mapper; file=arg("opsf")>;
file osicpsf <single_file_mapper; file=arg("osicpsf")>;
file tipsf <single_file_mapper; file=arg("tipsf")>;
file tisicpsf <single_file_mapper; file=arg("tisicpsf")>;

file tio2auhm <single_file_mapper; file=arg("tio2auhm")>;
file tio2audm <single_file_mapper; file=arg("tio2audm")>;
file out <single_file_mapper; file=arg("out")>;


(tio2auhm, tio2audm, out) = smeagol0_app (au3dm, au3hst, aupsf,
                                                         ausicpsf, bulklft, bulkrgt,
                                                         inputfdf, opsf, osicpsf,
                                                         tipsf, tisicpsf);
                               
