type file;

app (file _outfdf) pasta_app (file _src, file _xyz, file _infdf)
{
  bash @_src @_xyz @_infdf @_outfdf;
}

file src   <single_file_mapper; file=arg("src")>;
file xyz   <single_file_mapper; file=arg("xyz")>;
file infdf <single_file_mapper; file=arg("infdf")>;

file outfdf <single_file_mapper; file=arg("outfdf")>;


(outfdf) = pasta_app (src, xyz, infdf);

