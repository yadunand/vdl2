type file;


app (file _au3dm, file _au3hst, 
     file _bulklft, file _bulkrgt,
     file _out) smeagol_lead_app (file _aupsf, file _ausicpsf,
                                  file _inputfdf, file _opsf,
                                  file _osicpsf, file _tipsf,
                                  file _tisicpsf)
{
   // mpiexec "/home/heinonen/smeagol-1.2-Feb-2014/smeagol-1.2_blues/Src/smeagol-1.2_csg" stdin=@_inputfdf stdout=@_out;
   smeagol_leadwrap @_aupsf @_ausicpsf @_inputfdf @_opsf @_osicpsf @_tipsf @_tisicpsf @_au3dm @_au3hst @_bulklft @_bulkrgt @_out;
 }

file aupsf <single_file_mapper; file=arg("aupsf")>;
file ausicpsf <single_file_mapper; file=arg("ausicpsf")>;
file inputfdf <single_file_mapper; file=arg("inputfdf")>;
file opsf <single_file_mapper; file=arg("opsf")>;
file osicpsf <single_file_mapper; file=arg("osicpsf")>;
file tipsf <single_file_mapper; file=arg("tipsf")>;
file tisicpsf <single_file_mapper; file=arg("tisicpsf")>;
file au3dm <single_file_mapper; file=arg("au3dm")>;
file au3hst <single_file_mapper; file=arg("au3hst")>;
file bulklft <single_file_mapper; file=arg("bulklft")>;
file bulkrgt <single_file_mapper; file=arg("bulkrgt")>;
file out <single_file_mapper; file=arg("out")>;


(au3dm, au3hst, 
bulklft, bulkrgt, out) = smeagol_lead_app (aupsf, ausicpsf, 
			 inputfdf, opsf,
                         osicpsf, tipsf,
                         tisicpsf);
                               
