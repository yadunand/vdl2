type file;


/* == App function definitions == */

/* == Lammps app definition == */
app (file _outfdf) genfdf_app (file _src, float _v, file _infdf)
{
   bash @_src _v @_infdf stdout=@_outfdf;
}

file src <single_file_mapper; file=arg("src")>;
float v = toFloat(arg("v"));
file infdf <single_file_mapper; file=arg("infdf")>;

file outfdf <single_file_mapper; file=arg("outfdf")>;

(outfdf) = genfdf_app (src, v, infdf);

