#!/usr/bin/env python

"""
This is the Swift driver for Galaxy. It receives inputs from the Swift
Galaxy tool and depending on the options specified, builds a
canonical Swift/K or Swift/T script and runs over specified computational
environment.

swiftsmeagol_lead.py
  "${_aupsf}"
  "${_ausicpsf}" 
  "${_inputfdf}"
  "${_opsf}"
  "${_osicpsf}"
  "${_tipsf}"
  "${_tisicpsf}"
  "${_au3dm}"
  "${_au3hst}"
  "${_bulklft}"
  "${_bulkrgt}"
  "${_out}"
  "${local_or_compute}"

"""

import subprocess
import sys
import os
import distutils.spawn
import traceback
import fnmatch
import re
from os.path import expanduser

def genargs(args):
    for a in args:
        yield a

def main():
    myargs = genargs(sys.argv)
    try:
        this=next(myargs)
        aupsf = next(myargs)
        ausicpsf = next(myargs)
        inputfdf = next(myargs)
        opsf = next(myargs)
        osicpsf = next(myargs)
        tipsf = next(myargs)
        tisicpsf = next(myargs)
        au3dm = next(myargs)
        au3hst = next(myargs)
        bulklft = next(myargs)
        bulkrgt = next(myargs)
        out = next(myargs)
        local_or_compute = next(myargs)
    except:
        traceback.print_exc()
        sys.exit(1)

    homedir = expanduser("~")
        
    baseloc = homedir+"/SwiftApps/ematter/galaxy"
    
    #which swift
    swift = homedir+"/swift-0.95/cog/modules/swift/dist/swift-svn/bin/swift"
    swiftargs="-aupsf="+aupsf+" -ausicpsf="+ausicpsf+" -inputfdf="+inputfdf+" -opsf="+opsf+" -osicpsf="+osicpsf+" -tipsf="+tipsf+" -tisicpsf="+tisicpsf+" -au3dm="+au3dm+" -au3hst="+au3hst+" -bulklft="+bulklft+" -bulkrgt="+bulkrgt+" -out="+out
    
    siteval=baseloc+"/sites.local.xml"

    if local_or_compute == "compute":
        siteval=baseloc+"/sites.blues.xml"

    #Invocation
    exitcode=subprocess.check_call([swift, "-sites.file", siteval, "-tc.file", baseloc+"/apps", "-config", baseloc+"/cf", baseloc+"/smeagol_lead.swift", swiftargs])
    #print exitcode

if __name__=='__main__':
    main()
    
