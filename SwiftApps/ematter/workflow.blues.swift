/* ==
This is a Swift workflow script for the ematter application. The script contains the following parts:

-- A one-line type declaration
-- App function definitions
-- Input files and variables definitions
-- Output files definitions
-- Invocation of app functions
== */

type file;


/* == App function definitions == */

/* == Lammps app definition == */
app (file _dump) lammps_app (file _in, file _geo, file _eam)
{
   lammps "-in" @_in; 
}

/* == Pizza app definition == */
app (file _xyz) pizza_app (file _pizza_src, file _dump)
{
   python @_pizza_src @_dump @_xyz; 
}

/* == Pasta app definition == */
app (file _fdf) pasta_app (file _pasta_src, file _xyz, file _fdf_tmp)
{
  bash @_pasta_src @_xyz @_fdf_tmp @_fdf;
}

/* == 
smeagol lead inputs
Au.psf Au.sic.psf input.fdf
O.psf O.sic.psf Ti.psf Ti.sic.psf
== */

/* == Smeagol lead app definition == */
app (file _au3dm, file _au3hst, 
     file _bulklft, file _bulkrgt,
     file _out) smeagol_lead_app (file _aupsf, file _ausicpsf,
                                  file _inputfdf, file _opsf,
                                  file _osicpsf, file _tipsf,
                                  file _tisicpsf)
{
   mpiexec "/home/heinonen/smeagol-1.2-Feb-2014/smeagol-1.2_blues/Src/smeagol-1.2_csg" stdin=@_inputfdf stdout=@_out; 
}

/* == 
smeagol transport V0 inputs
Au3.DM Au3.HST Au.psf
Au.sic.psf bulklft.DAT bulkrgt.DAT
input.fdf O.psf O.sic.psf
Ti.psf Ti.sic.psf
== */

/* == Smeagol transport V0 app definition == */
app (file _tio2auhm, file _tio2audm,
     file _out) smeagol_v0_app ( file _au3dm, file _au3hst, file _aupsf,
                                 file _ausicpsf, file _bulklft, file _bulkrgt,
                                 file _inputfdf, file _opsf, file _osicpsf,
                                 file _tipsf, file _tisicpsf )
{
  mpiexec  "/home/heinonen/smeagol-1.2-Feb-2014/smeagol-1.2_blues/Src/smeagol-1.2_csg" stdin=@_inputfdf stdout=@_out; 
}

/* == Generate fdfs app definition == */
app (file _fdf) gen_fdf(file _genfdf_src, float _r, file _infdf)
{
  bash @_genfdf_src _r @_infdf stdout=@_fdf;
}

/* == Smeagol transport Vn app definition == */
app (file _trc_out, file _out) smeagol_vn_app ( file _au3dm,
                   file _au3hst, file _aupsf,
                   file _ausicpsf, file _bulklft,
                   file _bulkrgt, file _inputfdf,
                   file _opsf, file _osicpsf, 
                   file _tipsf, file _tisicpsf )
{
  mpiexec  "/home/heinonen/smeagol-1.2-Feb-2014/smeagol-1.2_blues/Src/smeagol-1.2_csg" stdin=@_inputfdf stdout=@_out; 
}

/* == Input files definitions: Make sure these files are available in the current directory == */
file lammps_in <"in.lammps">;
file lammps_geo <"AuTiO.geo">;
file lammps_eam <"Au_u3.eam">;

file fdf_leads<"input_leads.fdf">;
file fdf_v0<"input_t0.fdf">;

file aupsf <"Au.psf">;
file ausicpsf <"Au.sic.psf">;
file opsf <"O.psf">;
file osicpsf <"O.sic.psf">;
file tipsf <"Ti.psf">;
file tisicpsf <"Ti.sic.psf">;

file pizza_src<"lastlammpsdump2xyz.py">;
file pasta_src<"pasta">;
file genfdf_src<"genfdf">;

/* == Output files definitions == */
file xyz <"forsmeagol.xyz">;
file lammps_dump <"lammps.dump">;
file fdf_proc<"input_proc.fdf">;

file smeagol_lead_out <"smeagol_out/smeagol.lead.out">;
file smeagol_transport_out<"smeagol_out/smeagol.transport.out">;
file au3dm <"Au3.DM">;
file au3hst <"Au3.HST">;

file bulklft <"bulklft.DAT">;
file bulkrgt <"bulkrgt.DAT">;

file tio2auhm<"TiO2Au.HM">;
file tio2audm<"TiO2Au.DM">;

/* == unused
file[] smeagol_transport_outn<simple_mapper; location="smeagol_out",
                              prefix="smeagol.", suffix=".out">;
file[] fdf_proc_vn;
== */


/* == unused for now
float Vstart=0.1; //== Change this value to change the starting voltage ==
float Vend=1.0; //== Change this value to change the ending voltage ==
float Vstep=0.1;
== */

string Vs[]=["0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9","1.0"];

/* == End files and variable declaration == */


/* == Invocation of apps == */

/* == Invoke lammps == */
(lammps_dump) = lammps_app (lammps_in, lammps_geo, lammps_eam);

/* == Invoke pizza == */
(xyz) = pizza_app(pizza_src, lammps_dump);


/* == Invoke smeagol lead == */
(au3dm, au3hst, 
 bulklft, bulkrgt,
 smeagol_lead_out) = smeagol_lead_app (aupsf, ausicpsf, fdf_leads,
                                       opsf, osicpsf,
                                       tipsf, tisicpsf);


/* == Invoke smeagol transport V0 == */
(tio2auhm, tio2audm, 
 smeagol_transport_out) = smeagol_v0_app(au3dm, au3hst,
                                         aupsf, ausicpsf,
                                         bulklft, bulkrgt,
                                         fdf_v0, opsf, osicpsf,
                                         tipsf, tisicpsf);

/* == Invoke pasta == */
(fdf_proc) = pasta_app(pasta_src, xyz, fdf_v0);


/* ==
In a parallel 'foreach' loop, invoke gen_fdf and smeagol for the n V calculations
== */

foreach v,i in Vs
{
/* == generate fdfs for different voltages == */
string fdfname = strcat("input_", v, ".fdf");
file fdf_proc_vn <single_file_mapper; file=fdfname>;

fdf_proc_vn = gen_fdf(genfdf_src, toFloat(v), fdf_proc);

string outname = strcat("smeagol_out/bias_", v, ".out");
file smeagol_transport_outn <single_file_mapper; file=outname>;

file smeagol_trc_out <single_file_mapper; file=strcat("0.TiO2Au_", v, ".TRC")>;

(smeagol_trc_out, smeagol_transport_outn) = smeagol_vn_app(au3dm, au3hst,
                                            aupsf, ausicpsf,
                                            bulklft, bulkrgt,
                                            fdf_proc_vn, opsf,
                                            osicpsf, tipsf,
                                            tisicpsf);


}


