#!/usr/bin/env python

# Set the import path to find the Pizza tools.
# Warning! Nonportable.

import sys
sys.path.append('/home/ketan/pizza-2Jul14/src')
import dump, xyz

if __name__ == '__main__':
	# read the dump file (the first cl arg)
	d = dump.dump(sys.argv[1])
	# Get array of times
	t = d.time()
	# Convert the list of dumped states to xyz (in memory)
	x = xyz.xyz(d)
	# Dump just the last time instance to a file (the second cl arg)
	# This can be easily changed to use any other time slice between 0 
	# and len(t)-1 inclusive.  This will require additional validation
	# of the additional input parameter, though.
	x.single(t[-1],sys.argv[2])	

