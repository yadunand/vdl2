
/* ==
This is a Swift workflow script for the ematter application. The script contains the following parts:

-- A one-line type declaration
-- App function definitions
-- Input files and variables definitions
-- Output files definitions
-- Invocation of app functions
== */

type file;


/* == App function definitions == */

/* == Lammps app definition == */
app (file _dump) lammps_app (file _in, file _geo, file _eam, file _tinmeam, file _libmeam)
{
   lammps "-in" @_in; 
}

/* == Pizza app definition == */
app (file _xyz) pizza_app (file _pizza_src, file _dump)
{
   python @_pizza_src @_dump @_xyz; 
}

/* == Pasta app definition == */
app (file _fdf) pasta_app (file _pasta_src, file _xyz, file _fdf_tmp)
{
  bash @_pasta_src @_xyz @_fdf_tmp @_fdf;
}

/* == Smeagol lead app definition == */
app (file _tindm, file _tinhst, 
     file _bulklft, file _bulkrgt,
     file _out) smeagol_lead_app (file _smeagol_leadwrap, file _input_leadsfdf, file[] _psf)
{
  localbash @_smeagol_leadwrap @_input_leadsfdf stdout=@_out; 
}

/* == Smeagol transport V0 app definition == */
app (file _tio2auhm, file _tio2audm,
     file _out) smeagol_v0_app ( file _smeagol_t0wrap, file _au3dm, file _au3hst,
                                 file _bulklft, file _bulkrgt,
                                 file _inputfdf, file[] _psf)
{
  //mpiexec  "/home/heinonen/smeagol-1.2-Feb-2014/smeagol-1.2_blues/Src/smeagol-1.2_csg" stdin=@_inputfdf stdout=@_out; 
  localbash @_smeagol_t0wrap @_inputfdf stdout=@_out; 
}

/* == Generate fdfs app definition == */
app (file _fdf) gen_fdf(file _genfdf_src, float _r, file _infdf)
{
  bash @_genfdf_src _r @_infdf stdout=@_fdf;
}

/* == Smeagol transport Vn app definition == */
app (file _trc_out, file _out) smeagol_vn_app (
                               file _smeagol_tnwrap, file _au3dm, file _au3hst,
                               file _bulklft, file _bulkrgt,
                               file _inputfdf, file[] _psf )
{
  //mpiexec  "/home/heinonen/smeagol-1.2-Feb-2014/smeagol-1.2_blues/Src/smeagol-1.2_csg" stdin=@_inputfdf stdout=@_out; 
  localbash @_smeagol_tnwrap @_inputfdf stdout=@_out; 
}

/* == Input files definitions: Make sure these files are available in the current directory == */
file smeagol_leadwrap<"smeagol_leadwrap">;
file smeagol_t0wrap<"smeagol_t0wrap">;
file smeagol_tnwrap<"smeagol_tnwrap">;

file lammps_in <"in.lammps">;
file lammps_geo <"TiN-Ta-HfO2-TiN.geo">;
file lammps_eam <"Ta.lammps.eam">;
file tin_meam <"TiN.meam">;
file lib_meam <"library.meam">; 

file fdf_leads<"input_leads.fdf">;
file fdf_v0<"input_t0.fdf">;

file pizza_src<"lastlammpsdump2xyz.py">;
file pasta_src<"pasta">;
file genfdf_src<"genfdf">;

file psf[] <filesys_mapper; pattern="*.psf">;

/* == Output files definitions == */
file xyz <"forsmeagol.xyz">;
file lammps_dump <"lammps.dump">;
file fdf_proc<"input_proc.fdf">;

file smeagol_lead_out <"smeagol_out/smeagol.lead.out">;
file smeagol_transport_out<"smeagol_out/smeagol.transport.out">;
file tindm <"TiN.DM">;
file tinhst <"TiN.HST">;

file bulklft <"bulklft.DAT">;
file bulkrgt <"bulkrgt.DAT">;

file tio2auhm<"TiO2Au.HM">;
file tio2audm<"TiO2Au.DM">;

/* == unused for now
file[] smeagol_transport_outn<simple_mapper; location="smeagol_out",
                              prefix="smeagol.", suffix=".out">;
file[] fdf_proc_vn;

float Vstart=0.1; //== Change this value to change the starting voltage ==
float Vend=1.0; //== Change this value to change the ending voltage ==
float Vstep=0.1;
== */

string Vs[]=["0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9","1.0"];

/* == End files and variable declaration == */

/* == Invocation of apps == */

/* == Invoke lammps == */
(lammps_dump) = lammps_app (lammps_in, lammps_geo, lammps_eam, tin_meam, lib_meam);

/* == Invoke pizza == */
(xyz) = pizza_app(pizza_src, lammps_dump);


/* == Invoke smeagol lead == */
(tindm, tinhst, 
 bulklft, bulkrgt,
 smeagol_lead_out) = smeagol_lead_app (smeagol_leadwrap, fdf_leads, psf);


/* == Invoke smeagol transport V0 == */
(tio2auhm, tio2audm, 
 smeagol_transport_out) = smeagol_v0_app(smeagol_t0wrap, tindm, tinhst,
                                         bulklft, bulkrgt,
                                         fdf_v0, psf);

/* == Invoke pasta == */

(fdf_proc) = pasta_app(pasta_src, xyz, fdf_v0);

/* ==
In a parallel 'foreach' loop, invoke gen_fdf and smeagol for the n V calculations
== */

foreach v,i in Vs
{

/* == generate fdfs for different voltages == */
string fdfname = strcat("input_", v, ".fdf");
file fdf_proc_vn <single_file_mapper; file=fdfname>;

fdf_proc_vn = gen_fdf(genfdf_src, toFloat(v), fdf_proc);

string outname = strcat("smeagol_out/bias_", v, ".out");
file smeagol_transport_outn <single_file_mapper; file=outname>;
file smeagol_trc_out <single_file_mapper; file=strcat("0.TiO2Au_", v, ".TRC")>;

(smeagol_trc_out, smeagol_transport_outn) = smeagol_vn_app(smeagol_tnwrap, tindm, tinhst, bulklft, bulkrgt, fdf_proc_vn, psf);

}

