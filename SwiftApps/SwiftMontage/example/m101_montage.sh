#!/bin/bash

mImgtbl raw_dir images-rawdir.tbl

START=$(date +%s)

mProjExec -p raw_dir images-rawdir.tbl template.hdr proj_dir stats.tbl
rm -f proj_dir/*area*
mImgtbl proj_dir images.tbl
# mAdd -p pro_jdir -n images.tbl template.hdr final/uncorrected.fits
# mJPEG -gray final/uncorrected.fits 20% 99.98% loglog -out final/uncorrected.jpg
mOverlaps images.tbl diffs.tbl
mDiffExec -p proj_dir -n diffs.tbl template.hdr diff_dir
mFitExec diffs.tbl fits.tbl diff_dir
mBgModel images.tbl fits.tbl corrections.tbl
mBgExec -p proj_dir -n images.tbl corrections.tbl corr_dir
mAdd -p corr_dir -n images.tbl template.hdr final/mosaic.fits
mJPEG -gray final/mosaic.fits 20% 99.98% loglog -out final/mosaic.jpg

END=$(date +%s)
DIFF=$(( $END - $START ))
echo "It took $DIFF seconds"
