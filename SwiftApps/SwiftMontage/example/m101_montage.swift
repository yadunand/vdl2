import "../scripts/Swift_Montage_Types";
import "../scripts/Swift_Montage_Apps";
import "../scripts/Swift_Montage_Batch";

MosaicData template <"template.hdr">;
Table images_tbl <"images.tbl">;
Table difference_tbl <"diffs.tbl">;
Table fits_images_tbl <"fits.tbl">;
Table corrections_tbl <"corrections.tbl">;
Table stat_tbl <"stats.tbl">;
Image uncorrected_mos <"final/uncorrected.fits">;
JPEG uncorrected_jpg_img <"final/uncorrected.jpg">;
Image corrected_mos <"final/corrected.fits">;
JPEG corrected_jpg_img <"final/corrected.jpg">;
Image raw_image_files[] <filesys_mapper; location = "raw_dir", suffix = ".fits">;
Image projected_images[];
Image difference_images[];
Image corrected_images[];

projected_images = mProjectPPBatch( raw_image_files, template );
images_tbl = mImgtbl( projected_images );
uncorrected_mos = mAdd( projected_images, images_tbl, template );
uncorrected_jpg_img = mJPEG( uncorrected_mos );
difference_tbl = mOverlaps( images_tbl );
difference_images = mDiffBatch( difference_tbl, template );
fits_images_tbl = mFitBatch( difference_images, difference_tbl );
corrections_tbl = mBgModel( images_tbl, fits_images_tbl );
corrected_images = mBgBatch( projected_images, images_tbl, corrections_tbl );
corrected_mos = mAdd( corrected_images, images_tbl, template );
corrected_jpg_img = mJPEG( corrected_mos );
