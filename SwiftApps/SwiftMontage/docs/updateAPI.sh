#!/bin/bash
echo "Generating SwiftMontageAPI html file"
asciidoc -a toclevels=3 SwiftMontageAPI.adoc

echo "Generating SwiftMontageAPI pdf file"
a2x --format=pdf --icons --no-xmllint --dblatex-opts "-P doc.publisher.show=0 -P latex.output.revhistory=0" SwiftMontageAPI.adoc
