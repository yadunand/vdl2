#!/usr/bin/python

import sys

args = sys.argv;

file_tbl_1 = open( args[1], 'r' );
file_tbl_o = open( args[3], 'w' );
i = 0;
not_broke = True;

for line_tbl_1 in file_tbl_1:

    l1 = line_tbl_1.split();

    if l1[0] != "|" and l1[0] != "\\datatype":

        file_tbl_2 = open( args[2], 'r' );

        for line_tbl_2 in file_tbl_2:

            l2 = line_tbl_2.split();

            if l2[0] != "|" and l2[0] != "\\datatype":         

                if int(l2[0]) == int(l1[0]):
                    str_out = "["+str(i)+"].fname = "+l1[len(l1) - 1]+"\n";
                    str_out += "["+str(i)+"].a = "+l2[1]+"\n";
                    str_out += "["+str(i)+"].b = "+l2[2]+"\n";
                    str_out += "["+str(i)+"].c = "+l2[3]+"\n";
                    file_tbl_o.write( str_out );
                    i = int(i);
                    i+=1;
                    not_broke=False;
                    break;

        if not_broke:
            str_out = "["+str(i)+"].fname = "+l1[len(l1) - 1]+"\n";
            str_out += "["+str(i)+"].a = 0\n";
            str_out += "["+str(i)+"].b = 0\n";
            str_out += "["+str(i)+"].c = 0\n";
            file_tbl_o.write( str_out );
            i = int(i);
            i+=1;
        file_tbl_2.close();

file_tbl_1.close();
file_tbl_o.close();
