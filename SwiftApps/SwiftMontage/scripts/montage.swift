import "SwiftMontage";

Header header <"header.hdr">;
Table images_tbl <"images.tbl">;
Table difference_tbl <"diffs.tbl">;
Table fits_images_tbl <"fits.tbl">;
Table rectification_tbl <"rectification.tbl">;
Table stat_tbl <"stats.tbl">;
Image rectified_mos <"final/rectified.fits">;
JPEG rectified_jpg_img <"final/rectified.jpg">;
Image projected_images[];
Image difference_images[];
Image rectified_images[];

Image raw_image_files[] <filesys_mapper; location = "raw_dir", suffix = ".fits">;

projected_images = mProjectBatch( "proj_dir", raw_image_files, header );

images_tbl = mImgtbl( projected_images );

difference_tbl = mOverlaps( images_tbl );

( difference_images, fits_images_tbl ) = mDiffFitBatch( "proj_dir", "diff_dir", "stat_dir", @filenames(projected_images), difference_tbl, header );

rectification_tbl = mBgModel( images_tbl, fits_images_tbl );

rectified_images = mBackgroundBatch( "rect_dir", projected_images, images_tbl, rectification_tbl );

rectified_mos = mAdd( rectified_images, images_tbl, header );

rectified_jpg_img = mJPEG( rectified_mos );
