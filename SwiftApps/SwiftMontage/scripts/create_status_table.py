#!/usr/bin/python

# Input diff_tbl; Output Stat_tbl

def space( num ):
    spc = "";

    for i in range( 7 - num + 1 ):
        spc += " ";

    return spc



import sys

args = sys.argv;

diff_tbl = open( args[1], 'r' );
stat_tbl = open( args[2], 'w' );

stat_tbl.write( "| cntr1 | cntr2 |              stat              |\n" );
stat_tbl.write( "| int   | int   |              char              |\n" );

for diff_line in diff_tbl:

    line = diff_line.split();

    if line[0] != "|":
        str_out = " " + line[0] + space(len(line[0])) + line[1] + space(len(line[0])) + "stat." + line[4] + "\n";
        stat_tbl.write( str_out );

diff_tbl.close();
stat_tbl.close();



