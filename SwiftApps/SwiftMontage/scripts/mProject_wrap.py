#!/usr/bin/python

# args[0] = -X
# args[1] = in.fits
# args[2] = out.fits(could be symlink, script checks for that)
# args[3] = header.file

import sys
import os
import subprocess

args = sys.argv[1:]
proj = args[2]

if os.path.islink(proj):
    proj = os.readlink(args[2])

##########################################################
#                                                        #
#        Choose between mProject and mProjectPP          #
#       Need to write a couple of C programs that        #
#         model what mProjExec does for chosen.          #
#                                                        #
##########################################################

cmd = "mProject "+args[0]+" "+args[1]+" "+proj+" "+args[3]

ret = subprocess.call(cmd, close_fds=True,
                      shell=True, cwd=os.getcwd())

sys.exit(ret)
