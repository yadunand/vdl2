( Image rect_imgs[] ) mBackgroundBatch( string dest, Image imgs[], Table img_tbl, Table rect_tbl )
{
    Table back_list = Background_list( img_tbl, rect_tbl );

    BackgroundStruct back_struct[] = readData2( back_list );

    foreach background_entry, i in back_struct
    {
        Image orig_img <single_file_mapper; file = @dirname( imgs[1] ) + "/" + @background_entry.fname>;
        Image rect_img <regexp_mapper; source = @orig_img, match = ".*\\/(.*)", transform = dest+"/\\1">;

        float a = background_entry.a;
        float b = background_entry.b;
        float c = background_entry.c;
        rect_img = mBackground( orig_img, a, b, c );

        rect_imgs[ i ] = rect_img;
    }
}


( Image diff_imgs[] ) mDiffBatch( string src, string dest, Table diff_tbl, Header hdr )
{
    DiffStruct diffs[] <csv_mapper; file = diff_tbl, skip = 1, hdelim="| ">;

    foreach d_entry, i in diffs
    {
        Image img_1 <single_file_mapper; file = @strcat( src, "/", @d_entry.plus )>;
        Image img_2 <single_file_mapper; file = @strcat( src, "/", @d_entry.minus )>;

        Image diff_img <single_file_mapper; file = @strcat( dest, "/", @d_entry.diff )>;
        diff_img = mDiff( img_1, img_2, hdr );

        diff_imgs[ i ] = diff_img;
    }
}


( Image diff_imgs[], Table fits_tbl ) mDiffFitBatch( string src, string dest, string stat, string files[], Table diff_tbl, Header hdr )
{
    DiffStruct diffs[] <csv_mapper; file = diff_tbl, skip = 1, hdelim="| ">;
    Status stats[];

    Table status_tbl = create_status_table( diff_tbl );

    foreach d_entry, i in diffs
    {
        Image img_1 <single_file_mapper; file = @strcat( src, "/", @d_entry.plus )>;
        Image img_2 <single_file_mapper; file = @strcat( src, "/", @d_entry.minus )>;

        Image diff_img <single_file_mapper; file = @strcat( dest, "/", @d_entry.diff )>;
	Status stat_file <single_file_mapper; file = @strcat( stat,"/stat.", @d_entry.diff )>;

        ( diff_img, stat_file ) = mDiffFit( img_1, img_2, hdr );

	diff_imgs[ i ] = diff_img;
        stats[ i ] = stat_file;
    }
    fits_tbl = mConcatFit( status_tbl, stats );
}


( Table fits_tbl ) mFitBatch( string stat, Image diff_imgs[], Table diff_tbl )
{
    Status stats[] <structured_regexp_mapper; source = diff_imgs, match = ".*\\/(.*)", transform = stat + "/stat.\\1">;

    Table status_tbl = create_status_table( diff_tbl );

    foreach img, i in stats
    {
        stats[ i ] = mFitplane ( diff_imgs[i] );
    }

    fits_tbl = mConcatFit( status_tbl, stats );
}


( Image proj_imgs[] ) mProjectBatch( string dest, Image raw_imgs[], Header hdr )
{
    foreach img, i in raw_imgs
    {
        Image proj_img <regexp_mapper; source = @img, match = ".*\\/(.*)", transform = dest + "/proj_\\1">;
//        proj_img = mProject( img, hdr );
        proj_img = mProjectPP( img, hdr );

        proj_imgs[ i ] = proj_img;
    }
}
