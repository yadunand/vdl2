#!/usr/bin/python

# args[0] = -n(o-areas)
# args[1] = in1.fits
# args[2] = in2.fits
# args[3] = out.fits
# args[4] = header.file

import sys
import os
import subprocess

args = sys.argv[1:]
diff = args[3]

if os.path.islink(diff):
    diff = os.readlink(args[3])

cmd = "mDiff "+args[0]+" "+args[1]+" "+args[2]+" "+diff+" "+args[4]

ret = subprocess.call(cmd, close_fds=True,
                      shell=True, cwd=os.getcwd())

if not os.access( os.path.dirnamt(diff, os.F_OK)):
    os.mkdir(os.path.dirname(diff))

open(diff, "a").close()

sys.exit(0)
