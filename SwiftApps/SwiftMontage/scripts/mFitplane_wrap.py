#!/usr/bin/python

# args[0] = -s
# args[1] = status.file
# args[2] = in.fits

import sys
import os
import subprocess

args = sys.argv[1:]
stat = args[1]

if os.path.islink(stat):
    stat = os.readlink(args[1])

cmd = "mStat "+args[0]+" "+args[1]+" "+args[2]

ret = subprocess.call(cmd, close_fds=True,
                      shell=True, cwd=os.getcwd())

if not os.access( os.path.dirnamt(stat, os.F_OK)):
    os.mkdir(os.path.dirname(stat))

open(stat, "a").close()

sys.exit(0)
