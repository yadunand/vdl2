type Image;
type Header;
type Table;
type JPEG;
type Status;

type BackgroundStruct
{
    string fname;
    float a;
    float b;
    float c;
};


type DiffStruct
{
	int cntr1;
	int cntr2;
	Image plus;
	Image minus;
	Image diff;
};
