app ( Image mos ) mAdd( Image imgs[], Table img_tbl, Header hdr )
{
    mAdd "-p" @dirname( imgs[0] ) "-n" @img_tbl @hdr @mos;
}


app ( Image rc_img ) mBackground( Image img, float a, float b, float c )
{
    mBackground_wrap "-n" @img @rc_img a b c;
}


app ( Table rect_tbl ) mBgModel( Table img_tbl, Table fits_tbl )
{
    mBgModel @img_tbl @fits_tbl @rect_tbl;
}


app ( Table fits_tbl ) mConcatFit( Table status_tbl, Status stats[] )
{
    mConcatFit @status_tbl @fits_tbl @dirname( stats[0] );
}


app ( Image diff_img ) mDiff( Image proj_img_1, Image proj_img_2, Header hdr )
{
    mDiff_wrap "-n" @proj_img_1 @proj_img_2 @diff_img @hdr;
}


app ( Image diff_img, Status stat ) mDiffFit( Image img_1, Image img_2, Header hdr )
{
    mDiffFit_wrap "-n" "-s" @stat @img_1 @img_2 @diff_img @hdr;
}


app ( Status stat ) mFitplane( Image diff_img )
{
    mFitplane_wrap "-s" @stat @diff_img;
}


app ( Table img_tbl ) mImgtbl( Image imgs[] )
{
    mImgtbl @dirname( imgs[0] ) @img_tbl;
}


app ( Header hdr ) mMakeHdr( Table img_tbl )
{
    mMakeHdr @img_tbl @hdr;
}


app ( JPEG mos_img_jpg ) mJPEG( Image mos_img )
{
    mJPEG "-gray" @mos_img "0s" "max" "gaussian-log" "-out" @mos_img_jpg;
}


app ( Table diff_tbl ) mOverlaps( Table img_tbl )
{
    mOverlaps @img_tbl @diff_tbl;
}


app ( Image proj_img ) mProjectPP( Image raw_img, Header hdr )
{
    mProjectPP_wrap "-X" @raw_img @proj_img @hdr;
//    mProjectPP "-X" @raw_img @proj_img @hdr;
}


app ( Image proj_img ) mProject( Image raw_img, Header hdr )
{
    mProject_wrap "-X" @raw_img @proj_img @hdr;
//    mProject "-X" @raw_img @proj_img @hdr;
}

app ( Image shrunk ) mShrink( Image original, int factor )
{
    mShrink @original @shrunk factor;
}

/*----------------------------------------------------------------------------------*/
/*                                                                                  */
/*                                Util Scripts                                      */
/*                                                                                  */
/*----------------------------------------------------------------------------------*/

app ( Table back_tbl ) Background_list( Table imgs_tbl, Table rects_tbl )
{
    Background_list @imgs_tbl @rects_tbl @back_tbl;
}


app ( Table stat_tbl ) create_status_table( Table diff_tbl )
{
    create_status_table @diff_tbl @stat_tbl;
}
