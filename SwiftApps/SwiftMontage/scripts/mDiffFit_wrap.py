#!/usr/bin/python

import sys
import os
import subprocess

# args[0] = "-n(o-areas)"
# args[1] = -s
# args[2] = status.file(could be symlink, script checks for that)
# args[3] = in1.fits
# args[4] = in2.fits
# args[5] = out.fits(could be symlink, script checks for that)
# args[6] = header.file

args = sys.argv[1:]
stat = args[2]
diff = args[5]

if os.path.islink(stat):
    stat = os.readlink(args[2])

if os.path.islink(diff):
    diff = os.readlink(args[5])

cmd = "mDiffFit "+args[0]+" "+args[1]+" "+stat+" "+args[3]+" "+args[4]+" "+diff+" "+args[6]

ret = subprocess.call(cmd, close_fds=True,
                      shell=True, cwd=os.getcwd())

if not os.access( os.path.dirname(stat), os.F_OK ):
    os.mkdir(os.path.dirname(stat))

if not os.access( os.path.dirname(diff), os.F_OK ):
    os.mkdir(os.path.dirname(diff))

open(stat, "a").close()
open(diff, "a").close()
sys.exit(0)
