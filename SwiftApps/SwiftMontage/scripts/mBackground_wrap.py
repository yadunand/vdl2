#!/usr/bin/python

import sys
import os
import subprocess

# args[0] = -n(o-areas)
# args[1] = in.fits
# args[2] = out.fits(could be symlink, script checks for that)
# args[3] = coefficient a
# args[4] = coefficient b
# args[5] = coefficient c

args = sys.argv[1:]
back = args[2]

if os.path.islink(back):
    back = os.readlink(args[2])

cmd = "mBackground "+args[0]+" "+args[1]+" "+back+" "+args[3]+" "+args[4]+" "+args[5]

ret = subprocess.call(cmd, close_fds=True,
                      shell=True, cwd=os.getcwd())

sys.exit(ret)
