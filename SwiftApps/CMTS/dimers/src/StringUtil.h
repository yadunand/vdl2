#ifndef STRING_UTIL_DEFINED

#include <string>
#include <vector>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int StringToInt( const char * str, int base, const char * file, int lineno );
double StringToDouble( const char * str, const char * file, int lineno );
int IsDelim( char test, const char * delimiters, int ndelim );
int StripString( char * str, const char *whitespace );
int TokenizeString( char * str, const char * delimiters, char ** pointers, int maxptrs );
int TokenizeString( const char * source, const char * delimiters, std::vector< std::string > &results );
int TokenizeLine( FILE * f, const char *delimiters, char *line_buffer, std::vector<std::string> &results );

#define STRING_UTIL_DEFINED
#endif
