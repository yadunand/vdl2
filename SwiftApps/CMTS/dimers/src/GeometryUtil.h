#ifndef GEOMETRYUTIL_INCLUDED

#include "definitions.h"

#include <assert.h>
#include <math.h>

/*
	Where we recieve or pass back data, data type is FLOAT_TYPE. However, function-local variables are double precision.
*/

#define DOT_PRODUCT( v1, v2 ) ( v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2] )

FLOAT_TYPE * GenerateSphere( int theta_res, int phi_res, FLOAT_TYPE sphere_radius, int *npoints );

void CartesianToSpherical( FLOAT_TYPE *p1, FLOAT_TYPE *p2, FLOAT_TYPE *r, FLOAT_TYPE *theta, FLOAT_TYPE *phi );
void SphericalToCartesian( FLOAT_TYPE r, FLOAT_TYPE theta, FLOAT_TYPE phi, FLOAT_TYPE *x, FLOAT_TYPE *y, FLOAT_TYPE *z );

void CentreOfMass( int N, FLOAT_TYPE *coords, FLOAT_TYPE *mass, FLOAT_TYPE *COM );

// rotate on point rotates ... about the specified point! Can pass COM for molecular rotations etc.
// point = NULL rotates about point (0.0, 0.0, 0.0)
void RotateAboutPoint( int N, FLOAT_TYPE *xyz, FLOAT_TYPE *point, FLOAT_TYPE *axis, FLOAT_TYPE theta );

// pass 2 random numbers on the range 0 -> 1!
void UniformSpherePoint( FLOAT_TYPE random_number1, FLOAT_TYPE random_number2, FLOAT_TYPE *vec );


FLOAT_TYPE l2_norm_difference( int len, FLOAT_TYPE *v1, FLOAT_TYPE *v2 );

#define INIT_TETRAHEDRON 0
#define INIT_OCTAHEDRON 1
#define INIT_ICOSAHEDRON 2

#include <vector>

class Subdivider
{
	public:
	
		int n_vertices, n_faces, n_edges;
		float *vertices;
		int *faces;
	
		int edge_walk;
		int *start, *end, *midpoint;
	
		// initial generation stuff
		void InitTetrahedron();
		void InitOctahedron();
		void InitIcosahedron();
		// refinement of structures
		int SearchMidpoint( int index_start, int index_end );
		void Subdivide();
		
		// enclosing mesh production for a set of atoms, where r is the mesh distance from the atoms
		FLOAT_TYPE * BuildShell( int n_atoms, FLOAT_TYPE *atom_pos, FLOAT_TYPE r, int *n_shell_points );
		
		Subdivider( int init_as, int n_subdivisions );
		~Subdivider();
};


#define GEOMETRYUTIL_INCLUDED
#endif
