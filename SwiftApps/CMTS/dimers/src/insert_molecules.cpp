#include <stdio.h>
#include <math.h>

#include <map>
#include <vector>
#include <string>

#include "StringUtil.h"
#include "GeometryUtil.h"
#include "Ran1.h"

/*
	This inserts one or more copies of a lammps config file into another lammps config file.
	Bonding info and molecular assignments etc are preserved, and incremented as appropriate.
	
	The program assumes you're inserting into a sphere, and will insert in the whole sphere
	volume or onto the surface as specified. Also specified is a cutoff distance to any existing
	site in the simulation to avoid steric clashes.
	
	g++ -Wall -Wextra -pedantic -O3 config_insert.cpp StringUtil.cpp GeometryUtil.cpp Ran1.cpp -o config_insert
*/

struct atom_data
{
	int serial, mol, type;
	double x, y, z;
	double vx, vy, vz;
};
struct bond_data
{
	int serial, type, i, j;
};
struct improper_data
{
	int serial, type, i, j, k, l;
};
struct sim_data
{
	double minx,maxx;
	double miny,maxy;
	double minz,maxz;
	
	int n_atoms, n_bonds, n_impropers;
	int n_atom_types, n_bond_types, n_improper_types;
	
	std::map<int, double> masses;
	std::map<int, struct atom_data> atoms;
	std::map<int, struct bond_data> bonds;
	std::map<int, struct improper_data> impropers;
};
void add_atom( struct sim_data &sd, int type, int mol, double x, double y, double z, double vx, double vy, double vz )
{
	struct atom_data ad;
	
	if( sd.masses.find( type ) == sd.masses.end() )
	{
		printf( "%s(): unable to find atom type %d in defined masses.\n", __func__, type );
		return;
	}
	
	sd.n_atoms++;
	
	ad.serial = sd.n_atoms;
	ad.mol = mol;
	ad.type = type;
	ad.x = x;
	ad.y = y;
	ad.z = z;
	ad.vx = vx;
	ad.vy = vy;
	ad.vz = vz;
	
	sd.atoms[ ad.serial ] = ad;
}
void add_bond( struct sim_data &sd, int type, int i, int j )
{
	struct bond_data bd;
	
	if( sd.atoms.find( i ) == sd.atoms.end() )
	{
		printf( "%s(): unable to find atom index %d in defined atoms.\n", __func__, i );
		return;
	}
	if( sd.atoms.find( j ) == sd.atoms.end() )
	{
		printf( "%s(): unable to find atom index %d in defined atoms.\n", __func__, j );
		return;
	}

	sd.n_bonds++;
	
	bd.serial = sd.n_bonds;
	bd.type = type;
	bd.i = i;
	bd.j = j;
	
	sd.bonds[ bd.serial ] = bd;
}
void add_improper( struct sim_data &sd, int type, int i, int j, int k, int l )
{
	struct improper_data id;
	
	sd.n_impropers++;
	
	id.serial = sd.n_impropers;
	id.type = type;
	id.i = i;
	id.j = j;
	id.k = k;
	id.l = l;
	
	sd.impropers[ id.serial ] = id;
}
int get_largest_molecule_id( struct sim_data &sd )
{
	std::map<int, struct atom_data>::iterator it;
	int largest;
	
	largest = 0;
	
	for( it = sd.atoms.begin(); it != sd.atoms.end(); it++ )
	{
		if( it->second.mol > largest ) largest = it->second.mol;
	}
	return largest;
}

void load_data(
	FILE * f,
	struct sim_data &sd
	)
{
	char line_buffer[1024];
	const char *delimiters = " \t\n";
	int line_no;

	std::string state;
	std::vector<std::string> tokens;
	std::map<std::string,int> states;
	
	std::map<int, struct atom_data>::iterator a_it;
	std::map<int, struct bond_data>::iterator b_it;
	std::map<int, struct improper_data>::iterator i_it;
	std::map<int,int> int_map;
	
	states["Masses"] = 1;
	states["Atoms"] = 1;
	states["Velocities"] = 1;
	states["Bonds"] = 1;
	states["Impropers"] = 1;
	// following are in case the coeffs are included in the file, so we don't get confused, eg "Bond Coeffs"
	states["Bond"] = 1;
	states["Improper"] = 1;
	
	state = "";
	
	sd.minx = sd.maxx = 0.0;
	sd.miny = sd.maxy = 0.0;
	sd.minz = sd.maxz = 0.0;
	sd.n_atoms = sd.n_atom_types = 0;
	sd.n_bonds = sd.n_bond_types = 0;
	sd.n_impropers = sd.n_improper_types = 0;
	sd.masses.clear();
	sd.atoms.clear();
	sd.bonds.clear();
	sd.impropers.clear();
	
	TokenizeLine( f, delimiters, line_buffer, tokens ); // skip leading line.
	
	while( TokenizeLine( f, delimiters, line_buffer, tokens ) != -1 )
	{
		line_no++;
		
		if( tokens.size() < 1 ) continue;
		
		if( states.find( tokens[0] ) != states.end() )
		{
			//printf( "State switch: '%s' => '%s'\n", state.c_str(), tokens[0].c_str() );
			state = tokens[0];
			continue;
		}

		if( state == "" )
		{
			// load cell etc, put into metadata
			if( tokens.size() > 3 )
			{
				if( tokens[2]  == "xlo" && tokens[3] == "xhi" ) 
				{
					sd.minx = atof( tokens[0].c_str() );
					sd.maxx = atof( tokens[1].c_str() );
				}
				if( tokens[2]  == "ylo" && tokens[3] == "yhi" ) 
				{
					sd.miny = atof( tokens[0].c_str() );
					sd.maxy = atof( tokens[1].c_str() );
				}
				if( tokens[2]  == "zlo" && tokens[3] == "zhi" ) 
				{
					sd.minz = atof( tokens[0].c_str() );
					sd.maxz = atof( tokens[1].c_str() );
				}
			}
			else if( tokens.size() == 2 && tokens[1] == "atoms" ) { sd.n_atoms = atoi( tokens[0].c_str() ); }
			else if( tokens.size() == 2 && tokens[1] == "bonds" ) { sd.n_bonds = atoi( tokens[0].c_str() ); }
			else if( tokens.size() == 2 && tokens[1] == "impropers" ) { sd.n_impropers = atoi( tokens[0].c_str() ); }
			else if( tokens.size() == 3 && tokens[1] == "atom" ) { sd.n_atom_types = atoi( tokens[0].c_str() ); }
			else if( tokens.size() == 3 && tokens[1] == "bond" ) { sd.n_bond_types = atoi( tokens[0].c_str() ); }
			else if( tokens.size() == 3 && tokens[1] == "improper" ) { sd.n_improper_types = atoi( tokens[0].c_str() ); }
		}
		else if( state == "Masses" )
		{
			sd.masses[ atoi( tokens[0].c_str() ) ] = atof( tokens[1].c_str() );
		}
		else if( state == "Atoms" )
		{
			struct atom_data ad;
			
			ad.serial = atoi( tokens[0].c_str() );
			ad.mol = atoi( tokens[1].c_str() );
			ad.type = atoi( tokens[2].c_str() );
			
			ad.x = atof( tokens[3].c_str() );
			ad.y = atof( tokens[4].c_str() );
			ad.z = atof( tokens[5].c_str() );

			ad.vx = 0.0;
			ad.vy = 0.0;
			ad.vz = 0.0;
			
			// assume zero velocoties by default ; will be overwritten in Velocities section, where exists
			
			sd.atoms[ ad.serial ] = ad;
		}
		else if( state == "Velocities" )
		{
			int serial;
			
			serial = atoi( tokens[0].c_str() );
			if( sd.atoms.find( serial ) == sd.atoms.end() )
			{
				printf( "%s(): '%s' : unable to find atom with serial %d\n", __func__, state.c_str(), serial );
				exit( -1 );
			}
			
			sd.atoms[serial].vx = atof( tokens[1].c_str() );
			sd.atoms[serial].vy = atof( tokens[2].c_str() );
			sd.atoms[serial].vz = atof( tokens[3].c_str() );
		}
		else if( state == "Bonds" )
		{
			struct bond_data bd;
			
			bd.serial = atoi( tokens[0].c_str() );
			bd.type = atoi( tokens[1].c_str() );
			bd.i = atoi( tokens[2].c_str() );
			bd.j = atoi( tokens[3].c_str() );
			
			sd.bonds[ bd.serial ] = bd;
			
			//printf( "[%d] Bond: %d, %d, %d, %d\n", (int)sd.bonds.size(), bd.serial, bd.type, bd.i, bd.j );
		}
		else if( state == "Impropers" )
		{
			struct improper_data id;
			
			id.serial = atoi( tokens[0].c_str() );
			id.type = atoi( tokens[1].c_str() );
			id.i = atoi( tokens[2].c_str() );
			id.j = atoi( tokens[3].c_str() );
			id.k = atoi( tokens[4].c_str() );
			id.l = atoi( tokens[5].c_str() );
			
			sd.impropers[ id.serial ] = id;
			//printf( "[%d] Improper: %d, %d, %d, %d, %d, %d\n", (int)sd.impropers.size(), id.serial, id.type, id.i, id.j, id.k, id.l );
		}
	}
	
	// error tests, and build system info.
	
	for( a_it = sd.atoms.begin(); a_it != sd.atoms.end(); a_it++ )
	{
		if( sd.masses.find( a_it->second.type ) == sd.masses.end() )
		{
			printf( "%s(): unable to find atom mass for type %d\n", __func__, a_it->second.type );
			exit( -1 );
		}
	}
	if( sd.n_atoms != (int)sd.atoms.size() )
	{
		printf( "%s(): system declares %d atoms, but only have data for %d\n", __func__, sd.n_atoms, (int)sd.atoms.size() );
		exit( -1 );
	}
	
	for( b_it = sd.bonds.begin(); b_it != sd.bonds.end(); b_it++ )
	{
		if( sd.atoms.find( b_it->second.i ) == sd.atoms.end() )
		{
			printf( "%s(): unable to find atom serial %d in bond definition\n", __func__, b_it->second.i );
			exit( -1 );
		}
		if( sd.atoms.find( b_it->second.j ) == sd.atoms.end() )
		{
			printf( "%s(): unable to find atom serial %d in bond definition\n", __func__, b_it->second.j );
			exit( -1 );
		}
		if( b_it->second.type > sd.n_bond_types )
		{
			printf( "%s(): bond type %d is larger than declared number of types %d\n", __func__, b_it->second.type, sd.n_bond_types );
			exit( -1 );
		}
		//printf( "Bond: %d %d %d %d\n", b_it->second.serial, b_it->second.type, b_it->second.i, b_it->second.j );
	}
	if( sd.n_bonds != (int)sd.bonds.size() )
	{
		printf( "%s(): system declares %d bonds, but only have data for %d\n", __func__, sd.n_bonds, (int)sd.bonds.size() );
		exit( -1 );
	}

	for( i_it = sd.impropers.begin(); i_it != sd.impropers.end(); i_it++ )
	{
		if( sd.atoms.find( i_it->second.i ) == sd.atoms.end() )
		{
			printf( "%s(): unable to find atom serial %d in improper definition\n", __func__, i_it->second.i );
			exit( -1 );
		}
		if( sd.atoms.find( i_it->second.j ) == sd.atoms.end() )
		{
			printf( "%s(): unable to find atom serial %d in improper definition\n", __func__, i_it->second.j );
			exit( -1 );
		}
		if( sd.atoms.find( i_it->second.k ) == sd.atoms.end() )
		{
			printf( "%s(): unable to find atom serial %d in improper definition\n", __func__, i_it->second.k );
			exit( -1 );
		}
		if( sd.atoms.find( i_it->second.l ) == sd.atoms.end() )
		{
			printf( "%s(): unable to find atom serial %d in improper definition\n", __func__, i_it->second.l );
			exit( -1 );
		}
		if( i_it->second.type > sd.n_improper_types )
		{
			printf( "%s(): improper type %d is larger than declared number of types %d\n", __func__, i_it->second.type, sd.n_improper_types );
			exit( -1 );
		}
	}
	if( sd.n_impropers != (int)sd.impropers.size() )
	{
		printf( "%s(): system declares %d impropers, but only have data for %d\n", __func__, sd.n_impropers, (int)sd.impropers.size() );
		exit( -1 );
	}
	
}

void save_data(
	FILE * f,
	struct sim_data &sd
	)
{
	std::map<int, double>::iterator m_it;
	std::map<int, struct atom_data>::iterator a_it;
	std::map<int, struct bond_data>::iterator b_it;
	std::map<int, struct improper_data>::iterator i_it;

	fprintf( f, "Autogenerated!\n" );
	fprintf( f, "\n" );
	fprintf( f, "%d atoms\n", sd.n_atoms );
	fprintf( f, "%d bonds\n", sd.n_bonds );
	fprintf( f, "%d impropers\n", sd.n_impropers );
	fprintf( f, "\n" );
	fprintf( f, "%d atom types\n", sd.n_atom_types );
	fprintf( f, "%d bond types\n", sd.n_bond_types );
	fprintf( f, "%d improper types\n", sd.n_improper_types );
	fprintf( f, "\n" );
	fprintf( f, "%+g %+g xlo xhi\n", sd.minx, sd.maxx );
	fprintf( f, "%+g %+g ylo yhi\n", sd.miny, sd.maxy );
	fprintf( f, "%+g %+g zlo zhi\n", sd.minz, sd.maxz );
	fprintf( f, "\n" );
	fprintf( f, "Masses\n" );
	fprintf( f, "\n" );
	for( m_it = sd.masses.begin(); m_it != sd.masses.end(); m_it++ ) fprintf( f, "%d %g\n", m_it->first, m_it->second );
	fprintf( f, "\n" );
	fprintf( f, "Atoms\n" );
	fprintf( f, "\n" );
	for( a_it = sd.atoms.begin(); a_it != sd.atoms.end(); a_it++ ) fprintf( f, "%d %d %d %g %g %g\n", a_it->first, a_it->second.mol, a_it->second.type, a_it->second.x, a_it->second.y, a_it->second.z );
	fprintf( f, "\n" );
	fprintf( f, "Velocities\n" );
	fprintf( f, "\n" );
	for( a_it = sd.atoms.begin(); a_it != sd.atoms.end(); a_it++ ) fprintf( f, "%d %g %g %g\n", a_it->first, a_it->second.vx, a_it->second.vy, a_it->second.vz );
	fprintf( f, "\n" );
	fprintf( f, "Bonds\n" );
	fprintf( f, "\n" );
	for( b_it = sd.bonds.begin(); b_it != sd.bonds.end(); b_it++ ) fprintf( f, "%d %d %d %d\n", b_it->first, b_it->second.type, b_it->second.i, b_it->second.j );
	fprintf( f, "\n" );
	fprintf( f, "Impropers\n" );
	fprintf( f, "\n" );
	for( i_it = sd.impropers.begin(); i_it != sd.impropers.end(); i_it++ ) fprintf( f, "%d %d %d %d %d %d\n", i_it->first, i_it->second.type, i_it->second.i, i_it->second.j, i_it->second.k, i_it->second.l );
	fprintf( f, "\n" );
}

double reset_cog( struct sim_data &sd )
{
	std::map<int, struct atom_data>::iterator it;
	double max_extent_from_cog_sq;
	double COG[3], COG2[3];
	
	COG[0]  = COG[1]  = COG[2]  = 0.0;
	COG2[0] = COG2[1] = COG2[2] = 0.0;
	
	for( it = sd.atoms.begin(); it != sd.atoms.end(); it++ )
	{
		COG[0] += it->second.x;
		COG[1] += it->second.y;
		COG[2] += it->second.z;
	}
	COG[0] /= sd.n_atoms;
	COG[1] /= sd.n_atoms;
	COG[2] /= sd.n_atoms;
	printf( "COM was: %g, %g, %g\n", COG[0], COG[1], COG[2] );
	max_extent_from_cog_sq = -1.0;
	for( it = sd.atoms.begin(); it != sd.atoms.end(); it++ )
	{
		double dr[3], extent_sq;
		
		it->second.x -= COG[0];
		it->second.y -= COG[1];
		it->second.z -= COG[2];
		
		COG2[0] += it->second.x;
		COG2[1] += it->second.y;
		COG2[2] += it->second.z;
		
		// COG now at origin, or should be, so just use the coords.
		dr[0] = it->second.x;
		dr[1] = it->second.y;
		dr[2] = it->second.z;
		extent_sq = dr[0]*dr[0] + dr[1]*dr[1] + dr[2]*dr[2];
		if( extent_sq > max_extent_from_cog_sq )
		{
			max_extent_from_cog_sq = extent_sq;
		}
	}
	COG2[0] /= sd.n_atoms;
	COG2[1] /= sd.n_atoms;
	COG2[2] /= sd.n_atoms;
	printf( "COM now: %g, %g, %g\n", COG2[0], COG2[1], COG2[2] );
	
	return sqrt( max_extent_from_cog_sq );
}

void add_fragment( struct sim_data &sd, struct sim_data &frag )
{
	int start_mol_id, start_atom_serial;
	
	std::map<int, struct atom_data>::iterator a_it;
	std::map<int, struct bond_data>::iterator b_it;
	std::map<int, struct improper_data>::iterator i_it;

	start_mol_id = get_largest_molecule_id( sd );
	start_atom_serial = sd.n_atoms;
	
	for( a_it = frag.atoms.begin(); a_it != frag.atoms.end(); a_it++ )
	{
		add_atom( sd,
			a_it->second.type,
			a_it->second.mol+start_mol_id,
			a_it->second.x,
			a_it->second.y,
			a_it->second.z,
			a_it->second.vx,
			a_it->second.vy,
			a_it->second.vz );
	}
	for( b_it = frag.bonds.begin(); b_it != frag.bonds.end(); b_it++ )
	{
		add_bond( sd, b_it->second.type, b_it->second.i+start_atom_serial, b_it->second.j+start_atom_serial );
	}
	for( i_it = frag.impropers.begin(); i_it != frag.impropers.end(); i_it++ )
	{
		add_improper( sd, i_it->second.type, i_it->second.i+start_atom_serial, i_it->second.j+start_atom_serial, i_it->second.k+start_atom_serial, i_it->second.l+start_atom_serial );
	}
}

int check_collisions( struct sim_data &sd, struct sim_data &frag, float min_separation )
{
	std::map<int, struct atom_data>::iterator i_it;
	std::map<int, struct atom_data>::iterator j_it;

	for( i_it = frag.atoms.begin(); i_it != frag.atoms.end(); i_it++ )
	{
		for( j_it = sd.atoms.begin(); j_it != sd.atoms.end(); j_it++ )
		{
			double dr[3], r_sq;
			
			dr[0] = i_it->second.x - j_it->second.x;
			dr[1] = i_it->second.y - j_it->second.y;
			dr[2] = i_it->second.z - j_it->second.z;
			r_sq = dr[0]*dr[0] + dr[1]*dr[1] + dr[2]*dr[2];
			
			if( r_sq < min_separation*min_separation ) return 1;
		}
	}
	
	return 0;
}

void sim_coords_to_xyz( struct sim_data &sd, FLOAT_TYPE *xyz )
{
	std::map<int, struct atom_data>::iterator it;
	int i;
	
	i = 0;
	for( it = sd.atoms.begin(); it != sd.atoms.end(); it++ )
	{
		xyz[i*3 +0] = it->second.x;
		xyz[i*3 +1] = it->second.y;
		xyz[i*3 +2] = it->second.z;
		i++;
	}
}
void xyz_to_sim_coords( FLOAT_TYPE *xyz, struct sim_data &sd )
{
	std::map<int, struct atom_data>::iterator it;
	int i;
	
	i = 0;
	for( it = sd.atoms.begin(); it != sd.atoms.end(); it++ )
	{
		it->second.x = xyz[i*3+0];
		it->second.y = xyz[i*3+1];
		it->second.z = xyz[i*3+2];
		i++;
	}
}
void save_xyz( const char *fpath, int N, FLOAT_TYPE *xyz )
{
	FILE * f;
	
	f = fopen( fpath, "w" );
	fprintf( f, "%d\n", N );
	fprintf( f, "Comment line\n" );
	for( int i=0; i<N; i++ )
	{
		fprintf( f, "P %g %g %g\n", xyz[i*3+0], xyz[i*3+1], xyz[i*3+2] );
	}
	fclose( f );
}
void save_sim_as_xyz( const char *fpath, struct sim_data &sd )
{
	std::map<int, struct atom_data>::iterator it;
	FILE * f;
	
	f = fopen( fpath, "w" );
	fprintf( f, "%d\n", (int)sd.atoms.size() );
	fprintf( f, "Comment line\n" );

	for( it = sd.atoms.begin(); it != sd.atoms.end(); it++ )
	{
		fprintf( f, "P %g %g %g\n", it->second.x, it->second.y, it->second.z );
	}

	fclose( f );
}

int main( int argc, char **argv )
{
	struct sim_data sim, fragment, frag;
	int ncopies, insert_on_surface;
	double radius, min_sep, max_extent_from_cog;
	long ran1_seed;
	FLOAT_TYPE *original_xyz, *xyz;
	
	FILE *f;
	const char *sim_fpath, *fragment_fpath, *output_fpath;
	
	if( argc < 8 )
	{
		printf( "Usage: %s <sim data> <fragment data> <ncopies of fragment> <sphere radius> <insert_on_surface> <min_separation> <output file>\n", argv[0] );
		exit( -1 );
	}
	
	sim_fpath = argv[1];
	fragment_fpath = argv[2];
	ncopies = atoi( argv[3] );
	radius = atof( argv[4] );
	insert_on_surface = atoi( argv[5] );
	min_sep = atof( argv[6] );
	output_fpath = argv[7];
	
	// load target sim
	if( (f = fopen( sim_fpath, "r" )) == NULL )
	{
		printf( "Unable to open file '%s'\n", sim_fpath );
		exit( EXIT_FAILURE );
	}
	load_data( f, sim );
	fclose( f );

	// load fragment to add to target sim
	if( (f = fopen( fragment_fpath, "r" )) == NULL )
	{
		printf( "Unable to open file '%s'\n", fragment_fpath );
		exit( -1 );
	}
	load_data( f, fragment );
	fclose( f );

	max_extent_from_cog = reset_cog( fragment );
	
	printf( "Target sim has %d atoms (%d molecules)\n", sim.n_atoms, get_largest_molecule_id( sim ) );
	printf( "Fragment sim has %d atoms (%d molecules), max extent from COG is %g\n", fragment.n_atoms, get_largest_molecule_id( fragment ), max_extent_from_cog );
	printf( "%d copies of fragment data to insert.\n", ncopies );
	printf( "sphere radius is %g.\n", radius );
	
	original_xyz = new FLOAT_TYPE[ fragment.atoms.size()*3 ];
	xyz = new FLOAT_TYPE[ fragment.atoms.size()*3 ];

	sim_coords_to_xyz( fragment, original_xyz );
	frag = fragment;

	for( int i=0; i<ncopies; i++ )
	{
		int collided;
		
		if( ncopies >= 10 && i % (ncopies/10) == 0 ) { printf( "Adding %d...\n", i+1 ); save_sim_as_xyz( "temp.xyz", sim ); }
		
		collided = 1;
		for( int j=0; j<1e6; j++ )
		{
			FLOAT_TYPE origin[3], COG[3], axis[3], theta;
			FLOAT_TYPE r1, r2, distance_from_origin;
			
			memcpy( xyz, original_xyz, sizeof(FLOAT_TYPE)*frag.atoms.size()*3 );

			// generate random axis of rotation
			r1 = ran1( &ran1_seed );
			r2 = ran1( &ran1_seed );
			UniformSpherePoint( r1, r2, axis );

			// generate random rotation about this axis
			theta = (ran1( &ran1_seed )-0.5) * 2.0*M_PI;

			// generate random COG offset onto surface of sphere
			r1 = ran1( &ran1_seed );
			r2 = ran1( &ran1_seed );
			UniformSpherePoint( r1, r2, COG );
			if( insert_on_surface == 1 )
			{
				distance_from_origin = radius - max_extent_from_cog;
			}
			else
			{
				distance_from_origin = ran1( &ran1_seed ) * (radius - max_extent_from_cog);
			}
			COG[0] *= distance_from_origin;
			COG[1] *= distance_from_origin;
			COG[2] *= distance_from_origin;
			
			origin[0] = origin[1] = origin[2] = 0.0;
			
			RotateAboutPoint( (int)frag.atoms.size(), xyz, origin, axis, theta );
			for( size_t i=0; i<frag.atoms.size(); i++ )
			{
				xyz[i*3+0] += COG[0];
				xyz[i*3+1] += COG[1];
				xyz[i*3+2] += COG[2];
			}
			xyz_to_sim_coords( xyz, frag );
			
			// test for collisions
			if( check_collisions( sim, frag, min_sep ) == 0 )
			{
				collided = 0;
				break;
			}
		}
		if( collided == 1 )
		{
			printf( "Unable to insert fragment.\n" );
			exit( -1 );
		}
		
		add_fragment( sim, frag );
	}
	
	delete[] original_xyz;
	delete[] xyz;
	
	// save target sim
	if( (f = fopen( output_fpath, "w" )) == NULL )
	{
		printf( "Unable to open file '%s'\n", output_fpath );
		exit( EXIT_FAILURE );
	}
	save_data( f, sim );
	fclose( f );

	exit( EXIT_SUCCESS );
}
