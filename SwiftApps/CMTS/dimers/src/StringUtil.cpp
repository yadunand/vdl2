#include "StringUtil.h"

#define STRINGERROR( msg ) { printf( "ERROR: %s(): %s line %d: \"%s\"\n", __func__, __FILE__, __LINE__, msg ); exit(-1); }

/*
	Convert null terminated character array into an integer. Checks for bad input string, and produces error where failed.
*/
int StringToInt( const char * str, int base, const char * file, int lineno )
{
	char *endptr;
	char message[1024];
	int returned;
	
	#ifdef DEBUG
		if( str == NULL ) STRINGERROR( "NULL string passed as input" );
		if( file == NULL ) STRINGERROR( "NULL file name passed as input" );
	#endif
	
	returned = strtol( str, &endptr, 10 );
	if( *endptr != '\0' )
	{
		sprintf( message, "Unable to convert '%s' (%s, line %d) into an integer of base %d", str, file, lineno, base );
		STRINGERROR( message );
	}
	return returned;
}
/*
	Convert null terminated character array into a double precision floating point. Checks for bad input string, and produces error where failed.
*/
double StringToDouble( const char * str, const char * file, int lineno )
{
	char *endptr;
	char message[1024];
	double returned;

	#ifdef DEBUG
		if( str == NULL ) STRINGERROR( "NULL string passed" );
		if( file == NULL ) STRINGERROR( "NULL file name passed" );
	#endif

	returned = strtod( str, &endptr );
	if( *endptr != '\0' )
	{
		sprintf( message, "Unable to convert '%s' (%s, line %d) into a double", str, file, lineno );
		STRINGERROR( message );
	}
	return returned;
}
/*
	Check if character 'test' is a member of "delimiters"
*/
int IsDelim( char test, const char * delimiters, int ndelim )
{
	int i;

	#ifdef DEBUG
		if( delimiters == NULL ) STRINGERROR( "NULL delimiter string passed" );
	#endif

	for( i=0; i<ndelim; i++ )
		if( test == delimiters[i] ) return 1;
	return 0;
}
/*
	Strip whitespace from start and end of string, with user-defined whitespace.
	Where whitespace pointer is NULL, uses default whitespace of space, tab and newline characters.
*/
int StripString( char * str, const char *whitespace )
{
	int str_length, whitespace_length, i, j, start, end;
	const char *default_whitespace = " \t\n";
	const char *whitespace_ptr;

	#ifdef DEBUG
		if( str == NULL ) STRINGERROR( "NULL delimiter string passed" );
	#endif
	
	if( whitespace != NULL ) whitespace_ptr = whitespace;
	else whitespace_ptr = default_whitespace;
	str_length = strlen( str );
	whitespace_length = strlen( whitespace_ptr );

	/* get index of first non-whitespace character in source string */
	start = -1;
	for( i=0; i<str_length && start == -1; i++ )
	{
		if( IsDelim( str[i], whitespace_ptr, whitespace_length ) == 0 ) start = i;
	}

	/* get index of last non-whitespace character in source string */
	end = -1;
	for( i=str_length-1; i>=0 && end == -1; i-- )
	{
		if( IsDelim( str[i], whitespace_ptr, whitespace_length ) == 0 ) end = i;
	}
	
	/* note that we could have an empty string, so check for that. */
	j = 0;
	if( start == -1 || end == -1 || end < start )
	{
	}
	else
	{
		for( i=start; i<=end; i++ )
		{
			str[j] = str[i];
			j++;
		}
	}
	str[j] = '\0';
	
	return strlen( str );
}
/*
	Chops up "str", and stores the substrings in the array "pointers". If too many substrings are present to store in
	the "pointers" array, this routine will just do as many as possible.
*/
int TokenizeString( char * str, const char * delimiters, char ** pointers, int maxptrs )
{
	int ntoks, len, ndelim, i, j;
	
	#ifdef DEBUG
		if( str == NULL ) STRINGERROR( "NULL string passed" );
		if( delimiters == NULL ) STRINGERROR( "NULL delimiters string passed" );
		if( pointers == NULL ) STRINGERROR( "NULL pointers array passed" );
		if( maxptrs < 1 ) STRINGERROR( "maxptrs < 1" );
	#endif

	len = strlen( str );
	ndelim = strlen( delimiters );
	ntoks = 0;
	
	j = 0; /* index into current token. */
	for( i=0; i<len; i++ )
	{
		if( IsDelim( str[i], delimiters, ndelim ) == 1 )
		{
			/* If we've been generating a valid token and hit whitespace, increase ntokens and reset token character index */
			if( j > 0 ) ntoks++;
			j = 0;
			if( ntoks >= maxptrs ) break;
		}
		else
		{
			pointers[ntoks][j] = str[i];
			pointers[ntoks][j+1] = '\0';
			j++;
		}
	}
	/* We may have a trailing token at the end of the string */
	if( j > 0 ) ntoks++;
	return ntoks;
}

int TokenizeString( const char * source, const char * delimiters, std::vector< std::string > &results )
{
	int ntoks;
	char buffer[1024], tokens[100][1024], *token_ptrs[100];
	std::string str;
	
	for( int i=0; i<100; i++ ) token_ptrs[i] = tokens[i];
	
	strcpy( buffer, source );
	
	results.clear();

	ntoks = TokenizeString( buffer, delimiters, token_ptrs, 100 );
	for( int i=0; i<ntoks; i++ )
	{
		str = tokens[i];
		results.push_back( str );
	}

	return ntoks;
}
int TokenizeLine( FILE * f, const char *delimiters, char *line_buffer, std::vector<std::string> &results )
{
	#ifdef DEBUG
		if( f == NULL ) STRINGERROR( "NULL FILE* passed" );
		if( delimiters == NULL ) STRINGERROR( "NULL delimiters string passed" );
		if( line_buffer == NULL ) STRINGERROR( "NULL line_buffer pointer passed" );
	#endif

	line_buffer[0] = '\0';
	if( fgets( line_buffer, 1024, f) == NULL ) return -1;
	return TokenizeString( line_buffer, delimiters, results );
}


#undef STRINGERROR

