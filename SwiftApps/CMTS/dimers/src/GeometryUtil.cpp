#include "GeometryUtil.h"

/*
	Here theta is the "horizontal" resolution, and phi is the "vertical resolution"
	We add single point "caps" on the north and south poles of the sphere - these are included
	in the phi resolution, so the number of vertical circles which for the sphere is phi_res-2.
*/
FLOAT_TYPE * GenerateSphere( int theta_res, int phi_res, FLOAT_TYPE sphere_radius, int *npoints )
{
	double theta, phi, delta_phi;
	double circle_radius, pi, twopi, x, y, z;
	FLOAT_TYPE *points; // FLOAT_TYPE, as we pass it back.
	int i, j, k;
	
	assert( phi_res > 2 ); // ensure we have at least one genuine circle on the vertical axis!
	
	pi = M_PI;
	twopi = 2.0*M_PI;
	
	phi_res -= 2;
	
	*npoints = 2 + (phi_res*theta_res);
	points = (FLOAT_TYPE *) malloc( sizeof(FLOAT_TYPE)*(*npoints)*3 );
	
	k = 0;
	
	// add north pole - single point
	points[k*3 +0] = 0.0;
	points[k*3 +1] = 0.0;
	points[k*3 +2] = sphere_radius;
	k++;
	
	// add list of circles on vertical axis
	delta_phi = pi/(phi_res+1);
	for( i=0; i<phi_res; i++ )
	{
		phi = delta_phi*(i+1);
		z = sphere_radius*cos( phi );
		circle_radius = sphere_radius*sin( phi );
		
		for( j=0; j<theta_res; j++ )
		{
			theta = (twopi/theta_res)*j;
			x = circle_radius*sin(theta);
			y = circle_radius*cos(theta);

			points[k*3 +0] = x;
			points[k*3 +1] = y;
			points[k*3 +2] = z;
			k++;
		}
	}
	
	// add south pole - single point
	points[k*3 +0] = 0.0;
	points[k*3 +1] = 0.0;
	points[k*3 +2] = -sphere_radius;
	k++;
	
	assert( k == *npoints );
	
	return points;
}


void CentreOfMass( int N, FLOAT_TYPE *xyz, FLOAT_TYPE *mass, FLOAT_TYPE *COM )
{
	int i;
	double m, total_mass;
	
	assert( N > 0 );
	assert( xyz != NULL );
	assert( COM != NULL );
	
	COM[0] = 0.0;
	COM[1] = 0.0;
	COM[2] = 0.0;
	total_mass = 0.0;
	
	if( mass == NULL )
	{
		// assume mass of 1.0
		m = 1.0;
		for( i=0; i<N; i++ )
		{
			COM[0] += m * xyz[i*3 +0];
			COM[1] += m * xyz[i*3 +1];
			COM[2] += m * xyz[i*3 +2];
			total_mass += m;
		}
	}
	else
	{
		for( i=0; i<N; i++ )
		{
			m = mass[i];
			COM[0] += m * xyz[i*3 +0];
			COM[1] += m * xyz[i*3 +1];
			COM[2] += m * xyz[i*3 +2];
			total_mass += m;
		}
	}
	
	COM[0] /= total_mass;
	COM[1] /= total_mass;
	COM[2] /= total_mass;
}

/*
	I want to maintain the full range of coords for theta and phi; is we simply use the common vector angle difference
	method, we only get values of 0->pi, ie the minimum angle. For the case of theta, this gives us a circle around the zenith axis
	but to properly define the phi we need the full range of 0->2pi so we don't have uncertainty as to which "side" of the circle
	phi actually lies on.
	
	This is an issue, as which "side" of the circle phi is on dependes on whether we're looking from above or below the origin of
	the coordinate system, as that reflects phi! Therefore, we make an assumption here; specifically, that the zenith axis defines
	a normal to the plane formed by the remaining two cartesian axes, and we use this as "up". If we then project the 3D vector
	onto a 2D plane defined by the two potential azimuthal axes (by killing the zenith axis component), we can actually get the
	full range of 0->2pi given that "up" is the zenith axis, and IT IS SIGNED!
	
	Here we use zenith axis = z and azimuthal axis = x for convenience and easy interpretation.
	
	"Conventional" method (phi only on [0,pi]):
	
	x,y,z = v2-v1
	r = sqrt( dot(v1,v2) )
	theta = acos( z / r )
	phi = atan( y, x )
*/
void CartesianToSpherical( FLOAT_TYPE *p1, FLOAT_TYPE *p2, FLOAT_TYPE *r, FLOAT_TYPE *theta, FLOAT_TYPE *phi )
{
	double vec[3], axis[3], r2, x, y;
	double min_r = 1e-20;
	
	assert( p1 != NULL );
	assert( p2 != NULL );
	assert( r != NULL );
	assert( theta != NULL );
	assert( phi != NULL );
	
	vec[0] = p2[0] - p1[0];
	vec[1] = p2[1] - p1[1];
	vec[2] = p2[2] - p1[2];
	
	*r = sqrt( DOT_PRODUCT(vec,vec) );
	// is the 3D point too close to the origin?
	if( *r < min_r )
	{
		*theta = 0.0;
		*phi = 0.0;
	}
	else
	{
		vec[0] /= *r;
		vec[1] /= *r;
		vec[2] /= *r;
	
		// theta is the angle between the vector and the zenith axis - this is easy.
		axis[0] = axis[1] = axis[2] = 0.0;
		axis[2] = 1.0; // zenith axis is z
		*theta = acos( DOT_PRODUCT(axis,vec) );

		// now we get clever; we project the vector onto the 2D plane by killing the zenith component. Obviously,
		// this vector may no longer be a unit vector, so we renormalise. We can then use the atan2 function to get
		// the angle in the range -pi->pi (atan2 is also more accurate than other methods)
		axis[0] = axis[1] = axis[2] = 0.0;
		axis[0] = 1.0; // azimuthal axis is x
	
		vec[2] = 0.0; // kill the zenith component of the vector to project onto azimuthal plane
		r2 = sqrt( DOT_PRODUCT(vec,vec) ); // renomalise value
		// is the projected point too close to the 2D origin?
		if( r2 < min_r ) *phi = 0.0;
		else
		{
			// use the atan2 function, with x and y components!
			vec[0] /= r2;
			vec[1] /= r2;
			vec[2] /= r2;
			//
			x = vec[0];
			y = vec[1];
			*phi = atan2( x, y );
			//*phi = acos( axis[0]*vec[0] + axis[1]*vec[1] + axis[2]*vec[2] ); // old version; only 0 -> pi, so merges data symmetrically.
		}
	}
}
void SphericalToCartesian( FLOAT_TYPE r, FLOAT_TYPE theta, FLOAT_TYPE phi, FLOAT_TYPE *x, FLOAT_TYPE *y, FLOAT_TYPE *z )
{
	assert( x != NULL );
	assert( y != NULL );
	assert( z != NULL );
	
	// deffo right! unaffected by phi calculations.
	*z = r * cos(theta);
	
	// but it fucking beats me why the x and y components have switched here vs "standard" spherical. :/
	*x = r * sin(theta)*sin( phi );
	*y = r * sin(theta)*cos( phi );
	
	// the above actually avoids singularities that catch out the standard spherical coords method,
	// so we don't need to check for any weirdness as we have phi defined with no weirdness.
	
	// alternatively ...
	//FLOAT_TYPE u;
	//u = sqrt( r*r - (*z)*(*z) );
	//*x = u * sin( phi );
	//*y = u * cos( phi );
}

/*
// conventional Cartesian => spherical coords conversion
void _CartesianToSpherical( FLOAT_TYPE *p1, FLOAT_TYPE *p2, FLOAT_TYPE *r, FLOAT_TYPE *theta, FLOAT_TYPE *phi )
{
	double x, y, z;
	
	x = p2[0] - p1[0];
	y = p2[1] - p1[1];
	z = p2[2] - p1[2];

	*r = sqrt( x*x + y*y + z*z );
	*theta = acos( z / *r );
	*phi = atan( y/x );
}
// conventional spherical coords => Cartesian conversion
void _SphericalToCartesian( FLOAT_TYPE r, FLOAT_TYPE theta, FLOAT_TYPE phi, FLOAT_TYPE *x, FLOAT_TYPE *y, FLOAT_TYPE *z )
{
	*x = r * sin(theta)*cos(phi);
	*y = r * sin(theta)*sin(phi);
	*z = r * cos(theta);
}
// check we recover correct coordinates vs original, and against standard spherical methods marked with underscore.
// note the problems in the original method that we avoid around the origin!
void test_spherical( int dim )
{
	int x, y, z;
	FLOAT_TYPE s_r, s_t, s_p; // need to be FLOAT_TYPE, as we're passing into and out of the FLOAT_TYPE routines.
	FLOAT_TYPE v1[3], v2[3]; // as above.
	
	v1[0] = v1[1] = v1[2] = 0.0;
	
	for( x=-dim; x<=dim; x++ )
		for( y=-dim; y<=dim; y++ )
			for( z=-dim; z<=dim; z++ )
			{
				v2[0] = x;
				v2[1] = y;
				v2[2] = z;
				printf( "%+.12f %+.12f %+.12f\n", v2[0], v2[1], v2[2] );
				
				_CartesianToSpherical( v1, v2, &s_r, &s_t, &s_p );
				printf( "\t %+.6f %+.6f %+.6f => ", s_r, s_t, s_p );
				_SphericalToCartesian( s_r, s_t, s_p, &v2[0], &v2[1], &v2[2] );
				printf( "%+.12f %+.12f %+.12f\n", v2[0], v2[1], v2[2] );
				
				v2[0] = x;
				v2[1] = y;
				v2[2] = z;
				CartesianToSpherical( v1, v2, &s_r, &s_t, &s_p );
				printf( "\t %+.6f %+.6f %+.6f => ", s_r, s_t, s_p );
				SphericalToCartesian( s_r, s_t, s_p, &v2[0], &v2[1], &v2[2] );
				printf( "%+.12f %+.12f %+.12f\n", v2[0], v2[1], v2[2] );
			}
}
*/

void Translate( int N, FLOAT_TYPE *xyz, FLOAT_TYPE *offset )
{
	int i;
	
	assert( N > 0 );
	assert( xyz != NULL );
	assert( offset != NULL );
	
	for( i=0; i<N; i++ )
	{
		xyz[i*3 +0] += offset[0];
		xyz[i*3 +1] += offset[1];
		xyz[i*3 +2] += offset[2];
	}
}
// pass point = NULL for rotation about system origin, else rotation axis will pass through that point.
void RotateAboutPoint( int N, FLOAT_TYPE *xyz, FLOAT_TYPE *point, FLOAT_TYPE *axis, FLOAT_TYPE theta )
{
	int i;
	double a, b, c, x, y, z, rx, ry, rz, u, v, w, u2, v2, w2, K, L, ct, st;

	assert( N > 0 );
	assert( xyz != NULL );
	assert( axis != NULL );

	a = 0.0;
	b = 0.0;
	c = 0.0;
	
	if( point != NULL )
	{
		a = point[0];
		b = point[1];
		c = point[2];
	}
	
	ct = cos( theta );
	st = sin( theta );

	u = axis[0];
	v = axis[1];
	w = axis[2];
	u2 = u*u;
	v2 = v*v;
	w2 = w*w;
	K = u2 + v2 + w2;
	L = sqrt( K );
	
	for( i=0; i<N; i++ )
	{
		x = xyz[i*3 +0] - a;
		y = xyz[i*3 +1] - b;
		z = xyz[i*3 +2] - c;

		rx = a*(v2+w2) + u*(-b*v -c*w + u*x + v*y + w*z) + ( (x-a)*(v2+w2) + u*(b*v + c*w - v*y - w*z) ) * ct + L*(b*w - c*v - w*y + v*z)*st;
		rx = rx / K;

		ry = b*(u2+w2) + v*(-a*u -c*w + u*x + v*y + w*z) + ( (y-b)*(u2+w2) + v*(a*u + c*w - u*x - w*z) ) * ct + L*(-a*w + c*u + w*x - u*z)*st;
		ry = ry / K;

		rz = c*(u2+v2) + w*(-a*u -b*v + u*x + v*y + w*z) + ( (z-c)*(u2+v2) + w*(a*u + b*v - u*x - v*y) ) * ct + L*(a*v - b*u - v*x + u*y)*st;
		rz = rz / K;

		xyz[i*3 +0] = rx + a;
		xyz[i*3 +1] = ry + b;
		xyz[i*3 +2] = rz + c;
	}
}



FLOAT_TYPE l2_norm_difference( int len, FLOAT_TYPE *v1, FLOAT_TYPE *v2 )
{
	int i;
	double d, acc;
	
	acc = 0.0;
	for( i=0; i<len; i++ )
	{
		d = v1[i] - v2[i];
		acc += d*d;
	}
	return sqrt( acc );
}


void Subdivider::InitTetrahedron()
{
	float sqrt3 = 1 / sqrt(3.0);
	float tetrahedron_vertices[] = {
		sqrt3, sqrt3, sqrt3,
		-sqrt3, -sqrt3, sqrt3,
		-sqrt3, sqrt3, -sqrt3,
		sqrt3, -sqrt3, -sqrt3 }; 
	int tetrahedron_faces[] = {0, 2, 1, 0, 1, 3, 2, 3, 1, 3, 2, 0};

	n_vertices = 4; 
	n_faces = 4; 
	n_edges = 6; 
	vertices = (float*) malloc( 3*n_vertices*sizeof(float) );
	faces = (int*) malloc( 3*n_faces*sizeof(int) );
	memcpy( (void*) vertices, (void*) tetrahedron_vertices, 3*n_vertices*sizeof(float) );
	memcpy( (void*) faces, (void*) tetrahedron_faces, 3*n_faces*sizeof(int) );
}
void Subdivider::InitOctahedron()
{
	float octahedron_vertices[] = {
		0.0, 0.0, -1.0,
		1.0, 0.0, 0.0,
		0.0, -1.0, 0.0,
		-1.0, 0.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 0.0, 1.0 };
	int octahedron_faces[] = {0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 1, 5, 2, 1, 5, 3, 2, 5, 4, 3, 5, 1, 4}; 

	n_vertices = 6; 
	n_faces = 8;
	n_edges = 12; 
	vertices = (float*) malloc( 3*n_vertices*sizeof(float) );
	faces = (int*) malloc( 3*n_faces*sizeof(int) );
	memcpy( (void*) vertices, (void*) octahedron_vertices, 3*n_vertices*sizeof(float) );
	memcpy( (void*) faces, (void*) octahedron_faces, 3*n_faces*sizeof(int) );
}
void Subdivider::InitIcosahedron()
{
	float t = (1+sqrt(5))/2;
	float tau = t/sqrt(1+t*t);
	float one = 1/sqrt(1+t*t);

	float icosahedron_vertices[] = {
		tau, one, 0.0,
		-tau, one, 0.0,
		-tau, -one, 0.0,
		tau, -one, 0.0,
		one, 0.0 ,  tau,
		one, 0.0 , -tau,
		-one, 0.0 , -tau,
		-one, 0.0 , tau,
		0.0 , tau, one,
		0.0 , -tau, one,
		0.0 , -tau, -one,
		0.0 , tau, -one };
		
	int icosahedron_faces[] = {
		4, 8, 7,
		4, 7, 9,
		5, 6, 11,
		5, 10, 6,
		0, 4, 3,
		0, 3, 5,
		2, 7, 1,
		2, 1, 6,
		8, 0, 11,
		8, 11, 1,
		9, 10, 3,
		9, 2, 10,
		8, 4, 0,
		11, 0, 5,
		4, 9, 3,
		5, 3, 10,
		7, 8, 1,
		6, 1, 11,
		7, 2, 9,
		6, 10, 2 };

	n_vertices = 12; 
	n_faces = 20;
	n_edges = 30;
	vertices = (float*) malloc( 3*n_vertices*sizeof(float) );
	faces = (int*) malloc( 3*n_faces*sizeof(int) );
	memcpy( (void*) vertices, (void*) icosahedron_vertices, 3*n_vertices*sizeof(float) );
	memcpy( (void*) faces, (void*) icosahedron_faces, 3*n_faces*sizeof(int) );
}
int Subdivider::SearchMidpoint( int index_start, int index_end )
{ 
	int i, res;
	float length;
	
	for (i=0; i<edge_walk; i++)
	{
		if( (start[i] == index_start && end[i] == index_end) || (start[i] == index_end && end[i] == index_start) )
		{
			res = midpoint[i];

			/* update the arrays */
			start[i]    = start[edge_walk-1];
			end[i]      = end[edge_walk-1];
			midpoint[i] = midpoint[edge_walk-1];
			edge_walk--;

			return res; 
		}
	}

	/* vertex not in the list, so we add it */
	start[edge_walk] = index_start;
	end[edge_walk] = index_end; 
	midpoint[edge_walk] = n_vertices; 

	/* create new vertex */ 
	vertices[3*n_vertices+0] = (vertices[3*index_start+0] + vertices[3*index_end+0]) / 2.0;
	vertices[3*n_vertices+1] = (vertices[3*index_start+1] + vertices[3*index_end+1]) / 2.0;
	vertices[3*n_vertices+2] = (vertices[3*index_start+2] + vertices[3*index_end+2]) / 2.0;

	/* normalize the new vertex */ 
	length = sqrt(
		vertices[3*n_vertices+0] * vertices[3*n_vertices+0] +
		vertices[3*n_vertices+1] * vertices[3*n_vertices+1] +
		vertices[3*n_vertices+2] * vertices[3*n_vertices+2] );
	length = 1.0/length;
	vertices[3*n_vertices+0] *= length;
	vertices[3*n_vertices+1] *= length;
	vertices[3*n_vertices+2] *= length;

	n_vertices++;
	edge_walk++;
	return midpoint[edge_walk-1];
} 
void Subdivider::Subdivide()
{ 
	int n_vertices_new = n_vertices+2*n_edges; 
	int n_faces_new = 4*n_faces; 
	int i;
	
	int *faces_old;
	int a, b, c, ab_midpoint, bc_midpoint, ca_midpoint;

	edge_walk = 0; 
	n_edges = 2*n_vertices + 3*n_faces; 
	start = (int*) malloc( n_edges*sizeof(int) );
	end = (int*) malloc( n_edges*sizeof(int) );
	midpoint = (int*) malloc( n_edges*sizeof(int) );

	faces_old = (int*) malloc( 3*n_faces*sizeof(int) ); 
	faces_old = (int*) memcpy( (void*)faces_old, (void*)faces, 3*n_faces*sizeof(int) ); 
	vertices = (float*) realloc( (void*)vertices, 3*n_vertices_new*sizeof(float) ); 
	faces = (int*) realloc( (void*)faces, 3*n_faces_new*sizeof(int) ); 
	n_faces_new = 0; 

	for (i=0; i<n_faces; i++) 
	{ 
		a = faces_old[3*i+0]; 
		b = faces_old[3*i+1]; 
		c = faces_old[3*i+2]; 
        
		ab_midpoint = SearchMidpoint( b, a ); 
		bc_midpoint = SearchMidpoint( c, b ); 
		ca_midpoint = SearchMidpoint( a, c ); 

		faces[3*n_faces_new+0] = a; 
		faces[3*n_faces_new+1] = ab_midpoint; 
		faces[3*n_faces_new+2] = ca_midpoint; 
		n_faces_new++; 
		faces[3*n_faces_new+0] = ca_midpoint; 
		faces[3*n_faces_new+1] = ab_midpoint; 
		faces[3*n_faces_new+2] = bc_midpoint; 
		n_faces_new++; 
		faces[3*n_faces_new+0] = ca_midpoint; 
		faces[3*n_faces_new+1] = bc_midpoint; 
		faces[3*n_faces_new+2] = c; 
		n_faces_new++; 
		faces[3*n_faces_new+0] = ab_midpoint; 
		faces[3*n_faces_new+1] = b; 
		faces[3*n_faces_new+2] = bc_midpoint; 
		n_faces_new++; 
	} 
	n_faces = n_faces_new; 
	free( start );
	free( end );
	free( midpoint );
	free( faces_old );
} 
Subdivider::Subdivider( int init_as, int n_subdivisions )
{
	int i;
	
	n_vertices = 0;
	n_faces = 0;
	n_edges = 0;
	vertices = NULL;
	faces = NULL;
	
	edge_walk = 0;
	start = NULL;
	end = NULL;
	midpoint = NULL;
	
	switch( init_as )
	{
		case INIT_TETRAHEDRON:
			InitTetrahedron();
		break;

		case INIT_OCTAHEDRON:
			InitOctahedron();
		break;

		case INIT_ICOSAHEDRON:
			InitIcosahedron();
		break;

		default:
			printf( "Subdivider::%s(): unknown init type.\n", __func__ );
			exit( -1 );
		break;
	}

	double rv[3], r;
	rv[0] = 0.0;
	rv[1] = 0.0;
	rv[2] = 0.0;
	for( i=0; i<n_vertices; i++ )
	{
		rv[0] += vertices[i*3 +0];
		rv[1] += vertices[i*3 +1];
		rv[2] += vertices[i*3 +2];
	}
	rv[0] /= n_vertices;
	rv[1] /= n_vertices;
	rv[2] /= n_vertices;

	//printf( "Subdivider::%s(): starting vertex count is %d\n", __func__, n_vertices );
	for( i=0; i<n_subdivisions; i++ )
	{
		Subdivide(); 
		//printf( "Subdivider::%s(): subdivision %d resulted in %d vertices\n", __func__, i+1, n_vertices );
	}

	// renormalise - redundant, but never mind.
	for( i=0; i<n_vertices; i++ )
	{
		rv[0] = vertices[i*3 +0];
		rv[1] = vertices[i*3 +1];
		rv[2] = vertices[i*3 +2];
		r = sqrt( rv[0]*rv[0] + rv[1]*rv[1] + rv[2]*rv[2] );
		vertices[i*3+0] /= r;
		vertices[i*3+1] /= r;
		vertices[i*3+2] /= r;
	}
}
Subdivider::~Subdivider()
{
  if( vertices != NULL ) free( vertices ); 
  if( faces != NULL ) free( faces ); 
}
FLOAT_TYPE * Subdivider::BuildShell( int n_atoms, FLOAT_TYPE *atom_pos, FLOAT_TYPE r, int *n_shell_points )
{
	FLOAT_TYPE * shell_points;
	double pos[3], rv[3], r2;
	int i, j, k, good_point;
	
	std::vector<FLOAT_TYPE> x, y, z;
	
	// build a list of "good" vertices in the shell
	for( i=0; i<n_atoms; i++ )
	{
		for( j=0; j<n_vertices; j++ )
		{
			pos[0] = atom_pos[i*3 +0] + vertices[j*3 +0]*r;
			pos[1] = atom_pos[i*3 +1] + vertices[j*3 +1]*r;
			pos[2] = atom_pos[i*3 +2] + vertices[j*3 +2]*r;
			
			// check whether vertex collides with another atom
			good_point = 1;
			for( k=0; k<n_atoms; k++ )
			{
				if( i == k ) continue;
				
				rv[0] = pos[0] - atom_pos[k*3 +0];
				rv[1] = pos[1] - atom_pos[k*3 +1];
				rv[2] = pos[2] - atom_pos[k*3 +2];
				r2 = rv[0]*rv[0] + rv[1]*rv[1] + rv[2]*rv[2];
				
				if( r2 < r*r )
				{
					good_point = 0;
					break;
				}
			}
			
			if( good_point == 1 )
			{
				x.push_back( pos[0] );
				y.push_back( pos[1] );
				z.push_back( pos[2] );
			}
		}
	}
	
	// add good vertices to array
	*n_shell_points = (int)x.size();
	shell_points = (FLOAT_TYPE *)malloc( sizeof(FLOAT_TYPE)*3*(*n_shell_points) );
	for( i=0; i<(*n_shell_points); i++ )
	{
		shell_points[i*3 +0] = x[i];
		shell_points[i*3 +1] = y[i];
		shell_points[i*3 +2] = z[i];
	}

	// final check; no points should be closer than r from any atom.
	for( i=0; i<n_atoms; i++ )
	{
		//printf( "%f, %f, %f\n", atom_pos[i*3 +0], atom_pos[i*3 +1], atom_pos[i*3 +2] );
		for( j=0; j<(*n_shell_points); j++ )
		{
			//printf( "\t %f, %f, %f\n", shell_points[j*3 +0], shell_points[j*3 +1], shell_points[j*3 +2] );
			rv[0] = atom_pos[i*3 +0] - shell_points[j*3 +0];
			rv[1] = atom_pos[i*3 +1] - shell_points[j*3 +1];
			rv[2] = atom_pos[i*3 +2] - shell_points[j*3 +2];
			r2 = rv[0]*rv[0] + rv[1]*rv[1] + rv[2]*rv[2];
			
			if( r2 < (r*r)*0.999 )
			{
				printf( "Subdivider::%s(): AAARGH!\n", __func__ );
				printf( "Point %d is %e from atom %d; minimum is specified as %e\n", j, sqrt(r2), i, r );
				exit( -1 );
			}
		}
	}

	return shell_points;
}

// pass 2 random numbers on the range 0 -> 1!
void UniformSpherePoint( FLOAT_TYPE random_number1, FLOAT_TYPE random_number2, FLOAT_TYPE *vec )
{
	/*
	// original version; this had issues
	
	double u, theta;
	
	assert( vec != NULL );
	
	u = (random_number1-0.5) * 2.0;		// u \elem [-1,1]
	u = sqrt( 1 - u*u );
	theta = random_number2 * 2.0*M_PI;	// theta \elem [0,2pi)
	vec[0] = u * cos( theta );
	vec[1] = u * sin( theta );
	vec[2] = u;
	*/

	// new; tested ok! previous version was wrong.
	double z, phi, theta, ctheta;
	
	assert( vec != NULL );

	z = (random_number1 - 0.5) * 2.0;
	phi = random_number2 * 2.0*M_PI;
	theta = asin( z );
	ctheta = cos( theta );

	vec[0] = ctheta * cos(phi);
	vec[1] = ctheta * sin(phi);
	vec[2] = z;

}
