#!/bin/bash

prev_ndimers_file=$1
dimer_file=$2
n_dimers_to_add=$3
virion_radius=$4
ndimers=$5

lmp_john=`which lmp_john`

insert_molecules ${prev_ndimers_file} ${dimer_file} ${n_dimers_to_add} ${virion_radius} 0 25.0 start_${ndimers}.lammps_config

mpiexec -machinefile $PBS_NODEFILE ${lmp_john} -in lammps_new_spherical.in -var CONFIG start_${ndimers}.lammps_config -var NTDNTD_LJEPS 5.0 -var NTDCTD_LJEPS 5.0 -var RANDOMSEED 666 -var SPHERE_RADIUS ${virion_radius} -var NDIMERS ${ndimers}

restart2data end_${ndimers}.restart end_${ndimers}.lammps_config

