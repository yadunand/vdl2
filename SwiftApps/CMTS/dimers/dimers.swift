type file;

# Dimers wrapper script
app (file config_out, file restart_out, file trajectory_out) dimers (file previous_file, file lampps_input_file, file dimer, int dimers_to_add, int v_radius, int step)
{
   dimers @previous_file @dimer dimers_to_add v_radius step;   
}

# Parameters
int start_ndimers=5;
int max_ndimers=30;
int delta_ndimers=5;
int virion_radius=300;

# Files
file dimer_file <"dimer_1232.lammps_config">;
file initial_output <"empty.lammps_config">;
file lampps_input <"lammps_new_spherical.in">;
file lampps_configs[];

lampps_configs[start_ndimers-delta_ndimers] = initial_output;

# Main loop - example of a serial dependency
foreach i in [start_ndimers:max_ndimers:delta_ndimers] {
   file config <single_file_mapper; file=@strcat("end_", i, ".lammps_config")>;
   file restart <single_file_mapper; file=@strcat("end_", i, ".restart")>;
   file trajectory <single_file_mapper; file=@strcat("traj_", i, ".lammpstrj")>;
   (config, restart, trajectory) = dimers(lampps_configs[i-delta_ndimers], lampps_input, dimer_file, delta_ndimers, virion_radius, i);
   lampps_configs[i] = config;
}
