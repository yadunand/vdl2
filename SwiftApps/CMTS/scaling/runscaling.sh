#!/bin/bash

# Usage: swiftopt.sh [-s sitename] [-p paramfile] [-w] 
#
# NOTE: this command expects symlink "swift" in the cur dir to point
# to relese installed by setup.sh If you want to run with a different
# swift release, replace symlink "swift" with a link to your swift
# release dir.

usage="$0 [-s sitename] [-p paramfile]"

# Function to run Swift
runswift() {
   swift -tc.file tc.data -sites.file $1 -config cf scaling.swift 2>&1 | tee swift.out
}

# Default settings
execsite=local
paramfile=local
ram=2048M

# Process command line arguments
while [ $# -gt 0 ]; do
  case $1 in
    -s) execsite=$2; shift 2;;
    *) echo $usage 1>&2
       exit 1;;
  esac
done

# Create next unique run id and run directory
rundir=$( echo run??? | sed -e 's/^.*run//' | awk '{ printf("run%03d\n", $1+1)}' )

# Exit if rundir already exits. Something is funky
if [ -d $rundir ];
then
    echo "$rundir already exists! exiting." >&2
    exit 2
else
    mkdir $rundir
fi

# Copy input files
cp input_files/* $rundir
echo Run directory $rundir: site=$execsite paramfile=$paramfile

# Report an error if configuration files are missing
if [ ! -f "conf/$execsite.xml" ] && [ ! -f "conf/$execsite.conf" ]; then
   echo Unable to find requested configuration file for site $execsite
   exit 1
fi

# Use start-coaster-service if site is a .conf file
if [ -f "conf/$execsite.conf" ]; then
   USE_SCS=1
fi

# Check for missing .cf files
if [ -f "conf/$execsite.xml" ] && [ ! -f "conf/$execsite.cf" ]; then
   echo Missing configuration file $execsite.cf
fi

# Do the run
cd $rundir
cp ../scaling.swift .
export WORK=$PWD/swiftwork
mkdir -p $PWD/swiftwork/workers

# Create CDM config
# echo "rule .* DIRECT $PWD" >> fs.data

# Use start-coaster-service if the site uses a .conf file
if [ "$USE_SCS" == "1" ]; then
   cp ../conf/$execsite.conf coaster-service.conf
   cp ../conf/$execsite.cf cf
   sed -i -e "s@_RUNDIR_@$rundir@" coaster-service.conf
   start-coaster-service
fi

# Run gensites
if [ ! "$USE_SCS" == 1 ]; then
   cp ../conf/$execsite.cf cf
   gensites -p ../conf/$execsite.cf ../conf/$execsite.xml > $execsite.xml
fi

echo "Run dir=$rundir" >> ABOUT
echo "Work dir=$WORK" >> ABOUT
echo "Total jobs=$total_jobs" >> ABOUT

if [ "$USE_SCS" == "1" ]; then
   runswift "sites.xml"
   stop-coaster-service
else
   runswift "$execsite.xml"
fi

exit
