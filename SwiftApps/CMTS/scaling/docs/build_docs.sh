#!/bin/bash

echo Converting README to HTML
      asciidoc -a toc -a toclevels=2                            \
               -a max-width=750px                               \
               -a textwidth=80                                  \
               -a stylesheet=$PWD/stylesheets/asciidoc.css	\
               README

echo Converting README to PDF
        a2x --format=pdf --no-xmllint README
