type file;

// Namd wrapper
app (file out, file err) namd_wrapper (int numnodes, file psf_file, file pdb_file, 
                                       file coord_restart_file, file velocity_restart_file, 
                                       file system_restart_file, file namd_conf, file charmm_params)
{
   profile "count" = numnodes;
   namd_wrapper numnodes @namd_conf stdout=@out stderr=@err; 
}

// Plotting script
app (file png) plot (file[] namdlogs)
{
    plot @png @namdlogs;
}

# Range of nodes to test on
int minNodes=1;
int maxNodes=5;
int delta=1;

# Files
file psf <"h0_solvion.psf">;
file pdb <"h0_solvion.pdb">;
file coord_restart <"h0_eq.0.restart.coor">;
file velocity_restart <"h0_eq.0.restart.vel">;
file system_restart <"h0_eq.0.restart.xsc">;
file namd_config <"h0_eq.conf">;
file charmm_parameters <"par_all22_prot.inp">;
file logs[];

// Run scaling tests
foreach nodes in [minNodes:maxNodes:delta] {
   file namd_output <single_file_mapper; file=@strcat("logs/h0scale.", nodes, ".out")>;
   file namd_error <single_file_mapper; file=@strcat("logs/h0scale.", nodes, ".err")>;
   (namd_output, namd_error) = namd_wrapper(nodes, psf, pdb, coord_restart, velocity_restart, 
                                            system_restart, namd_config, charmm_parameters);
   logs[nodes] = namd_output;
}

// Plot output
file plot_output <"scale.dat">;
plot_output = plot(logs);

