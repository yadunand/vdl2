#!/bin/bash -x

numNodes=$1
namdConf=h0_eq.$numNodes.conf

echo Arguments are: $@
sed -e "s/_JOB_/$numNodes/g" h0_eq.conf > $namdConf
mpiexec namd2 $namdConf
