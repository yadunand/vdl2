#!/bin/bash -x

namdConf=h0_eq.$numJobs.conf

echo Arguments are: $@
sed -e "s/_JOB_/$numJobs/g" h0_eq.conf > $namdConf
mpiexec namd2 $namdConf
