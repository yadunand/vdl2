############################################################
## JOB DESCRIPTION                                         ##
#############################################################

# Minimization and equilibration of waters with 
# protein held fixed to reduce artifacts

set WORK	  /home/fxv/CGMEMB/SOLV
set SCRATCH	  /home/fxv/scratch-midway/CGMEMB/SOLV

#############################################################
## ADJUSTABLE PARAMETERS                                   ##
#############################################################

structure          $WORK/h0_solvion.psf
coordinates        $WORK/h0_solvion.pdb

#############################################################
# Output Names                                             ##
#############################################################
set basename       h0_eq.JOB
outputName         $WORK/OUT/$basename
DCDfile            $SCRATCH/$basename.dcd
forceDCDfile	   $SCRATCH/$basename.forces.dcd
restartname        $WORK/RESTART/$basename.restart

# Initial temperature
set temperature    310

# Continuing a job from the restart files
if {1} {
set inputname      h0_eq.OLDJOB 
binCoordinates     $WORK/RESTART/$inputname.restart.coor
binVelocities      $WORK/RESTART/$inputname.restart.vel  
extendedSystem     $WORK/RESTART/$inputname.restart.xsc
}

firsttimestep 	  0  		      

#############################################################
## SIMULATION PARAMETERS                                   ##
#############################################################

# Input
paraTypeCharmm      on
parameters          $WORK/par_all22_prot.inp

# Periodic Boundary Conditions from extended system cell (.xsc) 
wrapWater           on
wrapAll             on


# Force-Field Parameters
exclude             scaled1-4
1-4scaling          1.0
cutoff              12.0
switching           on
switchdist          10.0
pairlistdist        13.5

# Integrator Parameters
timestep            2.0  ;# 2fs/step
rigidBonds          all  ;# needed for 2fs steps
nonbondedFreq       2
fullElectFrequency  2
stepspercycle       20

PME                 yes
PMEPencils          16
PMETolerance     1.0e-6
PMEInterpOrder   4
PMEGridSpacing   1.0

# Constant Temperature Control
langevin            on    ;# do langevin dynamics
langevinDamping     .5     ;# damping coefficient (gamma) of 5/ps
langevinTemp        $temperature
langevinHydrogen    off

# Constant Pressure Control (variable volume)
if {0} {
useGroupPressure      yes ;# needed for 2fs steps
useFlexibleCell       no  ;# no for water box, yes for membrane
useConstantArea       no  ;# no for water box, yes for membrane

langevinPiston        on
langevinPistonTarget  1.01325 ;#  in bar -> 1 atm
langevinPistonPeriod  2000.0
langevinPistonDecay   2000.0
langevinPistonTemp    $temperature
}

restartfreq        1000     ;# 1000steps =  every 2ps
dcdfreq            1000
forcedcdfreq	   1000
xstFreq            1000
outputEnergies     1000
outputPressure     1000


#############################################################
## EXTRA PARAMETERS                                        ##
#############################################################

# Put here any custom parameters that are specific to 
# this job (e.g., SMD, TclForces, etc...)

#############################################################
## EXECUTION SCRIPT                                        ##
#############################################################

run 100000000
