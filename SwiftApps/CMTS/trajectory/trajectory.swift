type file;

// Namd wrapper
app (file out, file err, file velocity_file, file system_file, file config_file) namd_wrapper 
                 (file psf_file, file pdb_file, file coord_restart_file, 
                 file velocity_restart_file, file system_restart_file, 
                 file namd_conf, file charmm_params)
{
   namd_wrapper @namd_conf stdout=@out stderr=@err; 
}

# Range of nodes to test on
int jobStart=1;
int jobStop=5;
int delta=1;

# Input files
file psf <"h0_solvion.psf">;
file pdb <"h0_solvion.pdb">;
file charmm_parameters <"par_all22_prot.inp">;
file initial_coords <"h0_eq.0.restart.coor">;
file initial_velocity <"h0_eq.0.restart.vel">;
file initial_system <"h0_eq.0.restart.xsc">;
file initial_config <"h0_eq.conf">;

# File arrays to hold outputs
file namd_configs[];
file namd_coordinates[];
file namd_velocities[];
file namd_systems[];

# Set initial values
namd_configs[jobStart - delta]     = initial_config;
namd_coordinates[jobStart - delta] = initial_coords;
namd_velocities[jobStart - delta]  = initial_velocity;
namd_systems[jobStart - delta]     = initial_system;

# Run NAMD
foreach jobNumber in [jobStart:jobStop:delta] {
   file namd_output <single_file_mapper; file=@strcat("logs/namd.", jobNumber, ".out")>;
   file namd_error <single_file_mapper; file=@strcat("logs/namd.", jobNumber, ".err")>;
   file velocity <single_file_mapper; file=@strcat("h0_eq.", jobNumber, ".restart.vel")>;
   file system <single_file_mapper; file=@strcat("h0_eq.", jobNumber, ".restart.xsc")>;
   file config <single_file_mapper; file=@strcat("h0_eq.", jobNumber, ".conf")>;
   file coords <single_file_mapper; file=@strcat("h0_eq.", jobNumber, "restart.coord")>;

   (namd_output, namd_error, velocity, system, config) = namd_wrapper(psf, pdb, namd_coordinates[jobNumber-delta], 
                                            namd_velocities[jobNumber-delta], 
                                            namd_systems[jobNumber-delta], 
                                            namd_configs[jobNumber-delta], 
                                            charmm_parameters);
   namd_velocities[jobNumber] = velocity;
   namd_systems[jobNumber] = system;
   namd_configs[jobNumber] = config;
   namd_coordinates[jobNumber] = coords;
}

