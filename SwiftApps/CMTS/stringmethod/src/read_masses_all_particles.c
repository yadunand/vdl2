/* Last updated on August 29, 2011*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

/* Read the masses of the particles from the file 
 * 'MASSES_PARTICLES'
 * Format:
 * NUM_PARTICLES (number of particles %d)
 * Masses of particles one per line.
 * */

#include "read_masses_all_particles.h"

/* Memory allocation must be done before calling this function*/
void Read_masses_all_particles(FR_DAT *fr)
{
  char string[1000];
  FILE* indata1;
  int Num_particles_for_check;
  int i;

  indata1 = fopen("MASSES_PARTICLES", "r");
  if (indata1 == NULL)
    {
      printf ("Input file named 'MASSES_PARTICLES' not found. Exiting program\n");
      exit(1);
    }

  fscanf(indata1, "%s %d", string, &Num_particles_for_check);
  if(Num_particles_for_check != fr->natoms)
    {
      printf("Number of particles read from 'MASSES_PARTICLES' is different from other inputs.\n");
      exit(1);
    }

  for(i = 0 ; i < fr->natoms ; i++)
    {
      fscanf(indata1, "%lf ", &fr->m[i]);
    }

  fclose(indata1);
  
}

void Read_masses_all_particles2(FR_DAT *fr)
{
  char string[1000];
  FILE* indata1;
  int Num_particles_for_check;
  int i;

  indata1 = fopen("MASSES_PARTICLES_2", "r");
  if (indata1 == NULL)
    {
      printf ("Input file named 'MASSES_PARTICLES' not found. Exiting program\n");
      exit(1);
    }

  fscanf(indata1, "%s %d", string, &Num_particles_for_check);
  if(Num_particles_for_check != fr->natoms)
    {
      printf("Number of particles read from 'MASSES_PARTICLES' is different from other inputs.\n");
      exit(1);
    }

  for(i = 0 ; i < fr->natoms ; i++)
    {
      fscanf(indata1, "%lf ", &fr->m[i]);
    }

  fclose(indata1);
  
}

