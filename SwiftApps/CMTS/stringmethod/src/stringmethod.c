/* Code for Langevin Dynamics simulation for a tw ostate elastic network 
 * model. This is meant for models for protein backbone where only C-alpha 
 * atoms are considered.
 *
 * For integration the integrator described in the follwoing paper is
 * is used:
 *  
 * Vanden-Eijnden and Ciccotti Chem. Phys. Lett. vol 429, pp 310-316, 2006*/

/* This code does not use periodic boundary condition.*/

/* The main dynamics loop resides in a subroutine whcih is called from the
 * the main program.*/

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <omp.h>

#include "include_all.h"
#define DIMM 32 //number of images along the string
#define NSTP1 70000 //steps for constrained dynamics
#define NSTP2A 5000 //steps for free dynamics
#define NCYC 100 // number of free dynamics swarms
#define OUTCYC 1000 //number of iterations of the entire string method 
#define Num_cv 190  /*  \  */  
#define Num_cv2 513 // --number of collective variables
#define Num_cv3 18 //   /
#define TSTP1 0.01 //timestep for free/constrained dynamics 
#define Barrier 0.005 //energy barrier related parameter

void Copy_fr(FR_DAT fr, FR_DAT *copy_of_fr);
void Save_collective_variables(FR_DAT copy_of_fr, collective_variables  *sum_distances);
void Repar(collective_variables **update_distances);
void Repar2(collective_variables **update_distances);
void Init_fr(FR_DAT fr, FR_DAT *copy_of_fr);
void Clear_positions(FR_DAT *sum_of_fr);
void Sum_positions(FR_DAT fr, FR_DAT *sum_of_fr);
void Average_positions(FR_DAT *sum_of_fr);
void Fix_distances( collective_variables **update_distances, collective_variables **charmm_distances, collective_variables **diff_distances);
void  Add_fix_distances(collective_variables **update_distances, collective_variables **diff_distances);
void Read_from_fine_grain(FR_DAT *fr, FR_DAT fr2, int coarse_grain_id[]);
void Save_extra_constraint(FR_DAT copy_of_fr, collective_variables *sample_distances);
void Save_extra_constraint2(FR_DAT copy_of_fr, collective_variables *sample_distances);


int main(int argc, char *argv[])
{

  int th_id, nthreads;
  
  struct drand48_data buffer;

  #pragma omp parallel private(th_id)
  {
    th_id = omp_get_thread_num();
  #pragma omp barrier
    if(th_id==0){
      nthreads = omp_get_num_threads();
      if(nthreads < DIMM){
        printf("WARNING: Number of threads fewer than DIMM\n");
      
      }
    
    }
  }
 

  int coarse_grain_id[]= {0, 2, 4, 6, 8, 10, 11, 13, 15, 17, 19, 21, 23, 25, 26, 28, 30, 32, 34, 36}; 

  double KK=200.0, KK2=0.0;
  double Kharm=atof(argv[2]);

  char tempname[1000];
  char file_ad[100];
  strcpy(file_ad, argv[1]);

  time_t start, end;
  time(&start);

  int dim;

  double temperature;
  double Boltzmann_constant;

  /*Structure for storing position, velocity, force mass etc. main data structure seen by the dynamics routines*/
  FR_DAT fr[DIMM], copy_of_fr_omp[DIMM], sum_of_fr;
  //FR_DAT diff_of_fr, diff_of_fr2;
  FR_DAT fr2[DIMM], copy_of_fr_omp2[DIMM], sum_of_fr2;
  /*Structure for storing simulation parameters like time-step. length of run etc.*/
  struct simudetails SimulParam;
  /*Structure for storing Langevin friction coefficients.*/
  struct langevin Langevin_info;
  struct langevin Langevin_info2;
  /*Structure for implementing the integrator*/
  struct integrator_langenvin_VE_C Integrator_struct;
  struct integrator_langenvin_VE_C Integrator_struct2;
  /* Arrays for storing the accelerations. These are used for integrations*/
  rvec *accelerations_old[DIMM];
  rvec *accelerations_new[DIMM];
  rvec *accelerations_old2[DIMM];
  rvec *accelerations_new2[DIMM];

  /*Input parameters*/
  int Num_atoms, Num_atoms2;         /*Number of C-alpha atoms*/
  int Num_input_structures, Num_input_structures2;    /*Number of input structures, for two state model Num_input_structures = 2*/
  double *aray_of_cutoff_distances, *aray_of_cutoff_distances2;     /*One value per structure. For each structure the same value is used
                                          for all sites.*/
  double *array_of_mixing_parameters, *array_of_mixing_parameters2;   /*\beta_m s for two consecutive structures*/
  double repulsive_sigma, repulsive_sigma2;               /*one value of sigma is used for the repulsive parts of the all pairs*/
  double repulsive_epsilon, repulsive_epsilon2;             /*one value of epsilon is used for the repulsive parts of all pairs*/
  double fixed_value_of_force_constant, fixed_value_of_force_constant2;  /*In Albert Pan's paper this is set to 0.2 kcal/(mol Angstrom^2)*/
  double energy_parameter_for_force_constant, energy_parameter_for_force_constant2;     /*kcal/mol. \epsilon_k in Pan's paper and the value was set to 0.5*/
  double energy_difference_from_first_structure, energy_difference_from_first_structure2; /*Energy difference of the second structure from the first one in kcal/mol*/


  /*Data structures for storing the force filed information*/
  double ***array_of_pair_distance_matrices, ***array_of_pair_distance_matrices2;  /*Array of matrices where pair distances for input strcuturesare stored. 
                                                For a two state model we will have 2 matrices for two end states.
                                                Each matrix is symmetric with meanlingless diagonal elements (that are set to -1)*/
  double ***array_of_cutoff_matrices, ***array_of_cutoff_matrices2;     /*Each member of the array stores the cutoffs for all pairs for that structure
                                            Each matrix is symmetric with meanlingless diagonal elements (that are set to -1)*/
  double **force_constants_all_pairs, **force_constants_all_pairs2;     /*This is a matrix that stores the force constants of all pairs. 
                                            The matrix is symmetric with meanlingless diagonal elements (
                                            that are set to -1)*/
  double **sigmas_for_repulsive_part, **sigmas_for_repulsive_part2;          /*Stores the sigmas for repulsive part for all pairs. 
                                                 Matrix is symmetric with meanlingless diagonal elements (that are set to -1)*/
  double **epsilons_for_repulsive_part, **epsilons_for_repulsive_part2;       /*Stores the epsilons for repulsive part for all pairs. 
                                                 Matrix is symmetric with meanlingless diagonal elements (that are set to -1)*/
  
 FR_DAT *coordinates_all_structures, *coordinates_all_structures2;     /*Array for storing the positions of all the input structures.
                                            This is also used for storing some force information in the 
                                            intermediate states of force calcualtion. The reason is that we
                                            have to calcualte forces from elastic network of each structure.*/

  
  
  /* File name for the file with input configuration*/
  char Filename_input_config[1000]; 
  /* File name for the file with final configuration at the end of the run*/
  char Filename_output_config[1000];
  /* File name for the trajector file.*/
  char Filename_trajectory[1000];
  /* File name for input velocities*/
//  char Filename_input_velocities[1000];
  /* File name for output velocities*/
 // char Filename_output_velocities[1000];

  /*file pointers for recording various quantities*/
  FILE *energy, *progress, *KE, *PE, *pressure, *trajectory; 
  long int Num_frames_in_trajectory;  /* Number of frames in the output trajectory*/
  long int Num_data_other_output;     /* Number of data points in output files. This is 
                                    determined by 'WRITE_FREQUENCY_OTHER' input in the SIMULPARAMS file*/

  double Average_kinetic_enenrgy;  /*Average kinetic energy of the run. This is use to calcualte the temperature*/
  double Average_kinetic_enenrgy2;  /*Average kinetic energy of the run. This is use to calcualte the temperature*/
  long int Num_data_rmsd;        /*Number of data points in the RMSD file*/
  long int Num_frames_PDB_trajectory; /*Num_frames in the PDB trajectory file*/
  char **Residue_names;      /*Three letter codes for all the residues. Given in the 'RESIDUE_NAMES'
                               file. This is used for writing the PDB files.*/
  char **Atom_names;    
  char **Residue_names2;   
  int i;
  /********************************************************************************************/

  /* Spatial dimension*/
  dim = DIMENSION;
  if(dim != 3)
    {
      printf("This is meant for three dimensional systems\n");
      exit(1);
    }

  /* Read system information, i.e number of C-alpha atoms, number of input structures, mixing coefficients etc.*/
  Read_en_info(&Num_atoms, &Num_input_structures, &aray_of_cutoff_distances, &array_of_mixing_parameters, &repulsive_sigma, &repulsive_epsilon, &fixed_value_of_force_constant, &energy_parameter_for_force_constant, &energy_difference_from_first_structure);
  Read_en_info2(&Num_atoms2, &Num_input_structures2, &aray_of_cutoff_distances2, &array_of_mixing_parameters2, &repulsive_sigma2, &repulsive_epsilon2, &fixed_value_of_force_constant2, &energy_parameter_for_force_constant2, &energy_difference_from_first_structure2);
  if(Num_input_structures != 2)
    {
      printf("This is meant for a two state system. Only two input structures are allowed.\n");
      exit(1);
    }
  Print_en_input(Num_atoms, Num_input_structures, aray_of_cutoff_distances, array_of_mixing_parameters, repulsive_sigma, repulsive_epsilon, fixed_value_of_force_constant, energy_parameter_for_force_constant, energy_difference_from_first_structure);
  Print_en_input2(Num_atoms2, Num_input_structures2, aray_of_cutoff_distances2, array_of_mixing_parameters2, repulsive_sigma2, repulsive_epsilon2, fixed_value_of_force_constant2, energy_parameter_for_force_constant2, energy_difference_from_first_structure2);
////////////////////////////
//change energy barrier
array_of_mixing_parameters2[0]=Barrier;

////////////////////

  /* Allocate memeory*/
  
for(i=0;i<DIMM;i++){
  Alloc_main_data_structure(Num_atoms, &fr[i]);
  accelerations_old[i] = (rvec *)malloc(fr[i].natoms * sizeof(rvec));
  accelerations_new[i] = (rvec *)malloc(fr[i].natoms * sizeof(rvec));
}

for(i=0;i<DIMM;i++){
  Alloc_main_data_structure(Num_atoms2, &fr2[i]);
  accelerations_old2[i] = (rvec *)malloc(fr2[i].natoms * sizeof(rvec));
  accelerations_new2[i] = (rvec *)malloc(fr2[i].natoms * sizeof(rvec));
}

/***********************/
/* Allocate memory for copy_of_fr, sum_of_fr, diff_of_fr */
  for(i=0; i<DIMM; i++){
        Alloc_main_data_structure(Num_atoms, &copy_of_fr_omp[i]);
  }
  Alloc_main_data_structure(Num_atoms, &sum_of_fr);
 // Alloc_main_data_structure(Num_atoms, &diff_of_fr);
  
  for(i=0; i<DIMM; i++){
        Alloc_main_data_structure(Num_atoms2, &copy_of_fr_omp2[i]);
  }
  Alloc_main_data_structure(Num_atoms2, &sum_of_fr2);
//  Alloc_main_data_structure(Num_atoms2, &diff_of_fr2);
/**********************/


  Alloc_en(Num_atoms, Num_input_structures, &array_of_pair_distance_matrices, &array_of_cutoff_matrices, &force_constants_all_pairs, &sigmas_for_repulsive_part, &epsilons_for_repulsive_part, &coordinates_all_structures);
  Alloc_en(Num_atoms2, Num_input_structures2, &array_of_pair_distance_matrices2, &array_of_cutoff_matrices2, &force_constants_all_pairs2, &sigmas_for_repulsive_part2, &epsilons_for_repulsive_part2, &coordinates_all_structures2);

  /* Read all the input structures*/
  Read_input_structures(Num_input_structures, &coordinates_all_structures);
  Read_input_structures2(Num_input_structures2, &coordinates_all_structures2);

  /* Calculate all the pair distances for all the structures*/
  Calc_pair_distances_all_structures(Num_atoms, Num_input_structures, coordinates_all_structures, array_of_pair_distance_matrices); 
  Print_pair_distances_all_structures(Num_atoms, Num_input_structures, array_of_pair_distance_matrices);
  
  Calc_pair_distances_all_structures(Num_atoms2, Num_input_structures2, coordinates_all_structures2, array_of_pair_distance_matrices2); 
  Print_pair_distances_all_structures2(Num_atoms2, Num_input_structures2, array_of_pair_distance_matrices2);

  /* Set various quantities e.g. the cutoff distances, the sigmas and epsilons*/
  Set_various_quantities(Num_atoms, Num_input_structures, array_of_cutoff_matrices, sigmas_for_repulsive_part, epsilons_for_repulsive_part, aray_of_cutoff_distances, repulsive_sigma, repulsive_epsilon);  
  
  Set_various_quantities(Num_atoms2, Num_input_structures2, array_of_cutoff_matrices2, sigmas_for_repulsive_part2, epsilons_for_repulsive_part2, aray_of_cutoff_distances2, repulsive_sigma2, repulsive_epsilon2);  

  /* Calculate the force constants*/
  Set_site_dependent_force_constant_2_state(Num_input_structures, Num_atoms, array_of_pair_distance_matrices, energy_parameter_for_force_constant, fixed_value_of_force_constant, force_constants_all_pairs);
  Print_force_constants(Num_atoms, force_constants_all_pairs);
  
  Set_site_dependent_force_constant_2_state(Num_input_structures2, Num_atoms2, array_of_pair_distance_matrices2, energy_parameter_for_force_constant2, fixed_value_of_force_constant2, force_constants_all_pairs2);
  Print_force_constants2(Num_atoms2, force_constants_all_pairs2);
  
  /* Read simulation parameters*/
  ReadSimuDetails(&SimulParam, &temperature);  //SimulParam.timestep may change!!!

  /*Read the residue names*/
  Read_residue_names(Num_atoms, &Residue_names);
  Read_residue_names2(Num_atoms2, &Atom_names, &Residue_names2);

  /* Read input configuration*/
for(i=0;i<DIMM;i++){
  sprintf(Filename_input_config, "./%s/INITIAL_CONFIG_C_ALPHA_%d", file_ad, i);
  Read_config_simple_3d(Num_atoms, &fr[i], Filename_input_config);
  /* Write a PDB file with the initial configuration*/
  sprintf(Filename_output_config, "./%s/initial_config_c_alpha_%d.pdb", file_ad, i);
  Write_CA_PDB_file(&fr[i], Residue_names, Filename_output_config); 

  /* Read mass information*/
  Read_masses_all_particles(&fr[i]);
  fr[i].t = 0.0;
}
  
for(i=0;i<DIMM;i++){
  sprintf(Filename_input_config, "./%s/INITIAL_CONFIG_C_ALPHA_2_%d", file_ad, i);
  Read_config_simple_3d(Num_atoms2, &fr2[i], Filename_input_config);
  /* Write a PDB file with the initial configuration*/
  sprintf(Filename_output_config, "./%s/initial_config_c_alpha_2_%d.pdb", file_ad, i);
  Write_CA_PDB_file2(&fr2[i], Residue_names, Filename_output_config); 

  /* Read mass information*/
  Read_masses_all_particles2(&fr2[i]);
  fr2[i].t = 0.0;
}
  
  /*Initialize copy_of_fr and sum_of_fr*/
  for(i=0; i<DIMM; i++){  
        Init_fr(fr[0], &copy_of_fr_omp[i]);
  }
  Init_fr(fr[0], &sum_of_fr);
 // Init_fr(fr[0], &diff_of_fr);

  for(i=0; i<DIMM; i++){  
        Init_fr(fr2[0], &copy_of_fr_omp2[i]);
  }
  Init_fr(fr2[0], &sum_of_fr2);
  
  /* Set boltzmann constant.*/
  Boltzmann_constant = BOLTZMANN_CONSTANT;
  
  /* Reads langevin friction coefficients*/
  ReadFrictionCoefficients(&Langevin_info, temperature, Boltzmann_constant, fr[0]);
  ReadFrictionCoefficients2(&Langevin_info2, temperature, Boltzmann_constant, fr2[0]);

  /* Allocates and initializes data structure used for integration*/
  Aolloc_initialize_integration_structure(&Integrator_struct, fr[0], SimulParam);
  Aolloc_initialize_integration_structure(&Integrator_struct2, fr2[0], SimulParam);


  /********************************************************************************************/

  /*Start ouput files*/
  StartFiles(&SimulParam, &energy, &KE, &PE, &pressure);
  sprintf(tempname, "./%s/track-progress", file_ad);
  progress = fopen(tempname, "w");
  fprintf (progress, "Time (fs)         Timestep   Potential energy     Kinetic energy     Total energy     Pressure\n");

  FILE *pdb_traj;
  FILE *pdb_traj2;
  FILE *rmsd_1, *rmsd_2;
  FILE *rmsd_1b, *rmsd_2b;
  FILE *cons_file;
  FILE *out_r, *out_en;
  collective_variables cons_distances_omp[DIMM][Num_cv], sample_distances_omp[DIMM][Num_cv], **update_distances, **diff_distances;
  collective_variables cons_distances_omp2[DIMM][Num_cv+Num_cv2], sample_distances_omp2[DIMM][Num_cv+Num_cv2], **update_distances2;
  collective_variables cons_distances_fine[DIMM][Num_cv3];
  int j, k, kk, iter;
  double sum_distances_omp[DIMM][Num_cv];
  double sum_distances_omp2[DIMM][Num_cv+Num_cv2];
  FILE *cv_file;
  
  update_distances = (collective_variables **)malloc(DIMM*sizeof(collective_variables *));
  update_distances2 = (collective_variables **)malloc(DIMM*sizeof(collective_variables *));
  diff_distances = (collective_variables **)malloc(DIMM*sizeof(collective_variables *));
  for(i=0;i<DIMM;i++){
    update_distances[i] = (collective_variables *)malloc(Num_cv*sizeof(collective_variables));
    update_distances2[i] = (collective_variables *)malloc((Num_cv+Num_cv2)*sizeof(collective_variables));
    diff_distances[i] = (collective_variables *)malloc(Num_cv*sizeof(collective_variables));
  }

  sprintf(tempname, "./%s/CONSTRAINS-EXT-EXT", file_ad);
  cons_file=fopen(tempname,"r");
  if(cons_file==NULL){
    printf("No constrain file!\n");
    exit(1);  
  }
  
  for(j=0;j<Num_cv;j++){
    fscanf(cons_file, "%d  %d", &(cons_distances_omp[0][j].atomi), &(cons_distances_omp[0][j].atomj));
    cons_distances_omp[0][j].atomi--;
    cons_distances_omp[0][j].atomj--;    
    for(i=0;i<DIMM;i++){
      sample_distances_omp[i][j].atomi = cons_distances_omp[0][j].atomi;
      sample_distances_omp[i][j].atomj = cons_distances_omp[0][j].atomj;
      
      if(i!=0){
         cons_distances_omp[i][j].atomi = cons_distances_omp[0][j].atomi;
         cons_distances_omp[i][j].atomj = cons_distances_omp[0][j].atomj;
      }

      update_distances[i][j].atomi = cons_distances_omp[0][j].atomi;
      update_distances[i][j].atomj = cons_distances_omp[0][j].atomj;
      diff_distances[i][j].atomi = cons_distances_omp[0][j].atomi; 
      diff_distances[i][j].atomj = cons_distances_omp[0][j].atomj; 
    }
  }
  fclose(cons_file);

  sprintf(tempname, "./%s/CONSTRAINS-EXT-EXT_2", file_ad);
  cons_file=fopen(tempname,"r");
  if(cons_file==NULL){
    printf("No constrain file2!\n");
    exit(1);  
  }
  
  for(j=0;j<Num_cv;j++){
    fscanf(cons_file, "%d  %d", &(cons_distances_omp2[0][j].atomi), &(cons_distances_omp2[0][j].atomj));
    cons_distances_omp2[0][j].atomi--;
    cons_distances_omp2[0][j].atomj--;    
    for(i=0;i<DIMM;i++){
      sample_distances_omp2[i][j].atomi = cons_distances_omp2[0][j].atomi;
      sample_distances_omp2[i][j].atomj = cons_distances_omp2[0][j].atomj;
      
      if(i!=0){
         cons_distances_omp2[i][j].atomi = cons_distances_omp2[0][j].atomi;
         cons_distances_omp2[i][j].atomj = cons_distances_omp2[0][j].atomj;
      }

      update_distances2[i][j].atomi = cons_distances_omp2[0][j].atomi;
      update_distances2[i][j].atomj = cons_distances_omp2[0][j].atomj;
    }
  }
  fclose(cons_file);

  sprintf(tempname, "./%s/CONSTRAINS-EXT-EXT_2b", file_ad);
  cons_file=fopen(tempname,"r");
  if(cons_file==NULL){
    printf("No constrain file2!\n");
    exit(1);  
  }
  
  for(j=Num_cv;j<Num_cv+Num_cv2;j++){
    fscanf(cons_file, "%d  %d", &(cons_distances_omp2[0][j].atomi), &(cons_distances_omp2[0][j].atomj));
    cons_distances_omp2[0][j].atomi--;
    cons_distances_omp2[0][j].atomj--;    
    for(i=0;i<DIMM;i++){
      
      sample_distances_omp2[i][j].atomi = cons_distances_omp2[0][j].atomi;
      sample_distances_omp2[i][j].atomj = cons_distances_omp2[0][j].atomj;
      
      if(i!=0){
         cons_distances_omp2[i][j].atomi = cons_distances_omp2[0][j].atomi;
         cons_distances_omp2[i][j].atomj = cons_distances_omp2[0][j].atomj;
      }
      update_distances2[i][j].atomi = cons_distances_omp2[0][j].atomi;
      update_distances2[i][j].atomj = cons_distances_omp2[0][j].atomj;

    }
  }
  fclose(cons_file);

  sprintf(tempname, "./%s/CONSTRAINS-FINE", file_ad);
  cons_file=fopen(tempname,"r");
  if(cons_file==NULL){
    printf("No constrain file2!\n");
    exit(1);  
  }
  
  for(j=0;j<Num_cv3;j++){
    fscanf(cons_file, "%d  %d", &(cons_distances_fine[0][j].atomi), &(cons_distances_fine[0][j].atomj));
    cons_distances_fine[0][j].atomi--;
    cons_distances_fine[0][j].atomj--;    
    for(i=1;i<DIMM;i++){
         cons_distances_fine[i][j].atomi = cons_distances_fine[0][j].atomi;
         cons_distances_fine[i][j].atomj = cons_distances_fine[0][j].atomj;
    }
  }
  fclose(cons_file);
  
  for(i=0;i<DIMM;i++)
    Save_collective_variables(fr2[i], update_distances2[i]);
    
  for(i=0;i<DIMM;i++)
    Save_extra_constraint(fr2[i], update_distances2[i]);

  for(i=0;i<DIMM;i++)
    Save_extra_constraint2(fr2[i], cons_distances_fine[i]);//record all the bond length

  sprintf(tempname, "./%s/Collective_variables", file_ad);
  cv_file=fopen(tempname,"w");
  sprintf(tempname, "./%s/rmsd_from_structure_1_with_time", file_ad);
  rmsd_1 = fopen(tempname, "w");
  sprintf(tempname, "./%s/rmsd_from_structure_2_with_time", file_ad);
  rmsd_2 = fopen(tempname, "w");
  sprintf(tempname, "./%s/rmsd_from_structure_1b_with_time", file_ad);
  rmsd_1b = fopen(tempname, "w");
  sprintf(tempname, "./%s/rmsd_from_structure_2b_with_time", file_ad);
  rmsd_2b = fopen(tempname, "w");
  sprintf(tempname, "./%s/repulsive-potential-energy", file_ad);
  out_r = fopen(tempname, "w");
  sprintf(tempname, "./%s/elastic-network-potential-energy", file_ad);
  out_en = fopen(tempname, "w");
  
  /***********************/
/******Start Main loop********/

for(iter=0;iter<OUTCYC;iter++){  

  //Record collective variables and RMSDs
  for(i=0;i<DIMM;i++){ 
    
    fprintf(rmsd_1, "%d   %.8e   %.15e\n", i, fr->t * INTERNAL_TIME_TO_FS, Calc_rmsd(&fr2[i], &coordinates_all_structures2[0]));
    fprintf(rmsd_2, "%d   %.8e   %.15e\n", i, fr->t * INTERNAL_TIME_TO_FS, Calc_rmsd(&fr2[i], &coordinates_all_structures2[1]));
    
    Read_from_fine_grain(&fr[i], fr2[i], coarse_grain_id);
    
    fprintf(rmsd_1b, "%d   %.8e   %.15e\n", i, fr->t * INTERNAL_TIME_TO_FS, Calc_rmsd(&fr[i], &coordinates_all_structures[0]));
    fprintf(rmsd_2b, "%d   %.8e   %.15e\n", i, fr->t * INTERNAL_TIME_TO_FS, Calc_rmsd(&fr[i], &coordinates_all_structures[1]));
    
    fprintf(cv_file, "%d  %d  ", iter, i);
    for(j=0;j<Num_cv;j+=19)
      fprintf(cv_file, "%lf  ", update_distances2[i][j].dis); 
    fprintf(cv_file, "\n");

  }
  fflush(rmsd_1);
  fflush(rmsd_2);
  fflush(rmsd_1b);
  fflush(rmsd_2b);
  fflush(cv_file);

  //RUN CONSTRAINT DYNAMICS
  SimulParam.RUNLENGTH = NSTP1;
  SimulParam.timestep = TSTP1; 
  #pragma omp parallel for private( j, k, buffer, tempname, pdb_traj2)
  for(i=0;i<DIMM;i++){
                sprintf(tempname, "./%s/trajectory_all_%d.pdb", file_ad, i);
                if(iter==0)
                  pdb_traj2 = fopen(tempname, "w");
                else
                  pdb_traj2 = fopen(tempname, "a");
    
                Write_config_to_a_PDB_trajectory_file_2(pdb_traj2, &fr2[i], Atom_names, Residue_names2, iter);
                fclose(pdb_traj2);
               
                seed_rand(omp_get_thread_num(), &buffer);
                
                for(j=0;j<Num_cv+Num_cv2;j++){
                        cons_distances_omp2[i][j].dis=update_distances2[i][j].dis; 
                }  
               
                // get velocities to start the run 
                getvelocities(&fr2[i], temperature);
                // removes drift. Drift correction is not necessary for a one particle system.
                velocenter(&fr2[i]);
  
                Copy_fr(fr2[i], &copy_of_fr_omp2[i]);


                getforces_cons_harm_omp(&fr2[i], &coordinates_all_structures2, Num_input_structures2, array_of_pair_distance_matrices2, force_constants_all_pairs2, sigmas_for_repulsive_part2, epsilons_for_repulsive_part2, array_of_mixing_parameters2, aray_of_cutoff_distances2, energy_difference_from_first_structure2, cons_distances_omp2[i], Num_cv+Num_cv2, copy_of_fr_omp2[i], KK, Kharm, Num_atoms2);  

                // Calcualte kinetic energy
                get_kinetic_energy(&fr2[i]);

                Average_kinetic_enenrgy2 = 0.0;
                
                Perform_langevin_dynamics_cons_harm_omp(&fr2[i], &SimulParam, &Langevin_info2, accelerations_old2[i], accelerations_new2[i], &Integrator_struct2, &coordinates_all_structures2, Num_input_structures2, array_of_pair_distance_matrices2, force_constants_all_pairs2, sigmas_for_repulsive_part2, epsilons_for_repulsive_part2, array_of_mixing_parameters2, aray_of_cutoff_distances2, energy_difference_from_first_structure2, &Average_kinetic_enenrgy2, cons_distances_omp2[i], Num_cv+Num_cv2, copy_of_fr_omp2[i], KK, Kharm, Num_atoms2, &buffer);
  }
        
  //RELEASE SWARM, RUN FREE DYNAMICS
  SimulParam.RUNLENGTH = NSTP2A;
  SimulParam.timestep = TSTP1;
  #pragma omp parallel for private( j, k, buffer)
  for(i=0;i<DIMM;i++){
                
                for(j=0;j<Num_cv+Num_cv2;j++){
                  sample_distances_omp2[i][j].dis=0;
                  sum_distances_omp2[i][j]=0;
                }
                
                seed_rand(omp_get_thread_num(), &buffer);
                
                for(j=0;j<NCYC;j++){
                 // can i declare copy_of_fr to be private instead of defining an copy_fr array??? 
                 Copy_fr(fr2[i], &copy_of_fr_omp2[i]);
      
                 getvelocities(&copy_of_fr_omp2[i], temperature);
                 // removes drift. Drift correction is not necessary for a one particle system.*
                 velocenter(&copy_of_fr_omp2[i]);

                 // Calculate forces and potential energy*
                 getforces_cons_harm_omp(&copy_of_fr_omp2[i], &coordinates_all_structures2, Num_input_structures2, array_of_pair_distance_matrices2, force_constants_all_pairs2, sigmas_for_repulsive_part2, epsilons_for_repulsive_part2, array_of_mixing_parameters2, aray_of_cutoff_distances2, energy_difference_from_first_structure2, cons_distances_fine[i], Num_cv3, fr2[i], KK, Kharm, Num_atoms2);  

                 // Calcualte kinetic energy*
                 get_kinetic_energy(&copy_of_fr_omp2[i]);

                 Average_kinetic_enenrgy2 = 0.0;

                 Perform_langevin_dynamics_cons_harm_omp(&copy_of_fr_omp2[i], &SimulParam, &Langevin_info2, accelerations_old2[i], accelerations_new2[i], &Integrator_struct2, &coordinates_all_structures2, Num_input_structures2, array_of_pair_distance_matrices2, force_constants_all_pairs2, sigmas_for_repulsive_part2, epsilons_for_repulsive_part2, array_of_mixing_parameters2, aray_of_cutoff_distances2, energy_difference_from_first_structure2, &Average_kinetic_enenrgy2, cons_distances_fine[i], Num_cv3, fr2[i], KK, Kharm, Num_atoms2, &buffer);

                 Save_collective_variables(copy_of_fr_omp2[i], sample_distances_omp2[i]); 
                 Save_extra_constraint(copy_of_fr_omp2[i], sample_distances_omp2[i]);
                 
                 for(k=0;k<Num_cv+Num_cv2;k++)
                 sum_distances_omp2[i][k] +=sample_distances_omp2[i][k].dis;
               }
               
               for(j=0;j<Num_cv+Num_cv2;j++){
                 update_distances2[i][j].dis=sum_distances_omp2[i][j]/NCYC;
               }
  }

  //Record Structural information
  #pragma omp parallel for private( j, k, buffer, tempname, pdb_traj)
  for(i=0;i<DIMM;i++){
        //from all atom to coarse grain
         Read_from_fine_grain(&fr[i], fr2[i], coarse_grain_id);

         //Swarm in coarse grained potential
         sprintf(tempname, "./%s/trajectory_%d.pdb", file_ad, i);
         if(iter==0)
             pdb_traj = fopen(tempname, "w");
         else
             pdb_traj = fopen(tempname, "a");
         Write_config_to_a_PDB_trajectory_file_1(pdb_traj, &fr[i], Residue_names, iter);

        fclose(pdb_traj);
        
  } 

  //UPDATE COLLECTIVE VARIABLES, CONSTRAINTS FOR NEXT ITERATION***********
  Repar(update_distances2);
  Repar2(update_distances2);

}
//End of Main loop**********

  fclose(out_r);
  fclose(out_en);
  fclose(rmsd_1);
  fclose(rmsd_2); 
  fclose(rmsd_1b);
  fclose(rmsd_2b); 
  fclose(energy);
  fclose(KE);
  fclose(PE);
  fclose(progress);
  fclose(cv_file);
  //Calculate average kinetic eenrgy of the run*
  Average_kinetic_enenrgy = Average_kinetic_enenrgy / ((double)SimulParam.RUNLENGTH);
  //Writes the 'summary' file and bunch of other stuff*
  WriteSummary(&SimulParam, &fr[0], temperature, Average_kinetic_enenrgy);

  // Writes the final configuration *
for(i=0;i<DIMM;i++){ 
  sprintf(Filename_output_config, "./%s/final_config_c_alpha_%d", file_ad, i);
  Write_config_simple(&fr[i], Filename_output_config);
  sprintf(Filename_output_config, "./%s/final_config_c_alpha_%d.pdb", file_ad, i);
  Write_CA_PDB_file(&fr[i], Residue_names, Filename_output_config);
  }
  
  /********************************************************************************************/ 

  time(&end);
  double diff = difftime(end, start);
  FILE *t;
  sprintf(tempname, "./%s/time_elapsed", file_ad);
  t = fopen(tempname, "w");
  fprintf(t," %.15e\n", diff);
  fclose(t);

  printf("Done !!!!\n");

  return 0;
}

/**************END OF MAIN ROUTINE****************/


void Init_fr(FR_DAT fr, FR_DAT *copy_of_fr){
  
  int i,j;
  
  copy_of_fr->natoms = fr.natoms;
  
  for(i=0; i<fr.natoms; i++){
    copy_of_fr->m[i]=fr.m[i];    
    for(j=0; j<DIMENSION; j++){
      copy_of_fr->x[i][j]=0;
    }
  }
  
  copy_of_fr->temperature=fr.temperature;
  copy_of_fr->t=fr.t;

}


void Copy_fr(FR_DAT fr, FR_DAT *copy_of_fr){
  
  int i,j;
  
  for(i=0; i<fr.natoms; i++){
    for(j=0; j<DIMENSION; j++){
      copy_of_fr->x[i][j]=fr.x[i][j];
    }
  }
}

void Clear_positions(FR_DAT *sum_of_fr){
  
  int i,j;
  
  for(i=0; i<sum_of_fr->natoms; i++)
    for(j=0; j<DIMENSION; j++)
      sum_of_fr->x[i][j]=0;
    
}

void Sum_positions(FR_DAT fr, FR_DAT *sum_of_fr){
  
  int i,j;
  
  for(i=0; i<sum_of_fr->natoms; i++)
    for(j=0; j<DIMENSION; j++)
      sum_of_fr->x[i][j] += fr.x[i][j];
    
}


void Average_positions(FR_DAT *sum_of_fr){

  int i,j;
  
  for(i=0; i<sum_of_fr->natoms; i++)
    for(j=0; j<DIMENSION; j++)
      sum_of_fr->x[i][j] /= NCYC;

}


void Fix_distances( collective_variables **update_distances, collective_variables **charmm_distances, collective_variables **diff_distances){
  
  int i, j; 

  for(i=0;i<DIMM;i++)
    for(j=0;j<Num_cv;j++){
      diff_distances[i][j].dis=charmm_distances[i][j].dis-update_distances[i][j].dis; 
    }

}


void  Add_fix_distances(collective_variables **update_distances, collective_variables **diff_distances){

  int i, j; 

  for(i=0;i<DIMM;i++)
    for(j=0;j<Num_cv;j++)
      update_distances[i][j].dis += diff_distances[i][j].dis; 


}


void Save_collective_variables(FR_DAT copy_of_fr, collective_variables *sample_distances){
  
  int i,j,a,b;
  double dist, sq_dist;
  
  for(i=0;i<Num_cv;i++){
    sq_dist=0; 
    a=sample_distances[i].atomi;
    b=sample_distances[i].atomj;
    for(j=0;j<DIMENSION;j++){
      dist=copy_of_fr.x[a][j]-copy_of_fr.x[b][j];
      sq_dist += dist*dist;
    }  
  
    sample_distances[i].dis = sqrt(sq_dist);
  }

}

void Repar(collective_variables **update_distances){

  double c[DIMM][Num_cv], cr[DIMM][Num_cv], LL[DIMM], s[DIMM];
  double scd, cd[Num_cv], L, d;
  int i, j, k;
  
  for(i=0;i<DIMM;i++)
    for(j=0;j<Num_cv;j++)
      c[i][j]=update_distances[i][j].dis; 
  

  for(i=0; i<DIMM; i++)
    LL[i]=0;

  for(i=1; i<DIMM; i++){
    scd=0;
    for(k=0; k<Num_cv; k++){
      cd[k]=c[i][k]-c[i-1][k];
      scd +=cd[k]*cd[k];
    }
    
    LL[i]=LL[i-1]+sqrt(scd);
  
  }

  L=LL[DIMM-1];


  for(k=0; k<DIMM; k++){
    s[k]=k*L/(DIMM-1);
  }

  for(k=0; k<Num_cv; k++){
    cr[0][k]=c[0][k];
    cr[DIMM-1][k]=c[DIMM-1][k];
  }

  for(i=1; i<DIMM-1; i++)
    for(j=1; j<DIMM; j++)
      if(LL[j]>=s[i]){
        scd=0;
        for(k=0; k<Num_cv; k++){
          cd[k]=c[j][k]-c[j-1][k];
          scd += cd[k]*cd[k];
        }
        
        d=sqrt(scd);

        for(k=0; k<Num_cv; k++)
          cr[i][k]=(s[i]-LL[j-1])*cd[k]/d + c[j-1][k];

        break;

      }

  for(i=0; i<DIMM; i++){
      for(k=0;k<Num_cv;k++){
        update_distances[i][k].dis=cr[i][k];
      }
  }

}



void Repar2(collective_variables **update_distances){

  double c[DIMM][Num_cv2], cr[DIMM][Num_cv2], LL[DIMM], s[DIMM];
  double scd, cd[Num_cv2], L, d;
  int i, j, k;
  
  for(i=0;i<DIMM;i++)
    for(j=0;j<Num_cv2;j++)
      c[i][j]=update_distances[i][j+Num_cv].dis; 
  

  for(i=0; i<DIMM; i++)
    LL[i]=0;

  for(i=1; i<DIMM; i++){
    scd=0;
    for(k=0; k<Num_cv2; k++){
      cd[k]=c[i][k]-c[i-1][k];
      scd +=cd[k]*cd[k];
    }
    
    LL[i]=LL[i-1]+sqrt(scd);
  
  }

  L=LL[DIMM-1];


  for(k=0; k<DIMM; k++){
    s[k]=k*L/(DIMM-1);
  }

  for(k=0; k<Num_cv2; k++){
    cr[0][k]=c[0][k];
    cr[DIMM-1][k]=c[DIMM-1][k];
  }

  for(i=1; i<DIMM-1; i++)
    for(j=1; j<DIMM; j++)
      if(LL[j]>=s[i]){
        scd=0;
        for(k=0; k<Num_cv2; k++){
          cd[k]=c[j][k]-c[j-1][k];
          scd += cd[k]*cd[k];
        }
        
        d=sqrt(scd);

        for(k=0; k<Num_cv2; k++)
          cr[i][k]=(s[i]-LL[j-1])*cd[k]/d + c[j-1][k];

        break;

      }

  for(i=0; i<DIMM; i++){
      for(k=0;k<Num_cv2;k++){
        update_distances[i][k+Num_cv].dis=cr[i][k];
      }
  }

}

void Read_from_fine_grain(FR_DAT *fr, FR_DAT fr2, int coarse_grain_id[]){
  int i,j;
  for(i=0; i<fr->natoms; i++){
    for(j=0; j<DIMENSION; j++){
      fr->x[i][j]=fr2.x[coarse_grain_id[i]][j];
    }
  }

}

void Save_extra_constraint(FR_DAT copy_of_fr, collective_variables *sample_distances){
  
  int i,j,a,b;
  double dist, sq_dist;
  
  for(i=Num_cv;i<Num_cv+Num_cv2;i++){
    sq_dist=0; 
    a=sample_distances[i].atomi;
    b=sample_distances[i].atomj;
    for(j=0;j<DIMENSION;j++){
      dist=copy_of_fr.x[a][j]-copy_of_fr.x[b][j];
      sq_dist += dist*dist;
    }  
  
    sample_distances[i].dis = sqrt(sq_dist);
  }

}

void Save_extra_constraint2(FR_DAT copy_of_fr, collective_variables *sample_distances){
  
  int i,j,a,b;
  double dist, sq_dist;
  
  for(i=0;i<Num_cv3;i++){
    sq_dist=0; 
    a=sample_distances[i].atomi;
    b=sample_distances[i].atomj;
    for(j=0;j<DIMENSION;j++){
      dist=copy_of_fr.x[a][j]-copy_of_fr.x[b][j];
      sq_dist += dist*dist;
    }  
  
    sample_distances[i].dis = sqrt(sq_dist);
  }

}

