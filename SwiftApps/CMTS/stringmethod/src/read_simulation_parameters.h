#ifndef _read_simulation_parameters_h
#define _read_simulation_parameters_h

#include "md_ld_common_d_dimension_namd.h"


void ReadSimuDetails(struct simudetails *SimulParam, double *temperature);

#endif
