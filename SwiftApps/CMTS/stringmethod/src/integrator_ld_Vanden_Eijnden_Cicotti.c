/*Written by Avisek Das*/
/*Last modified on December 19, 2011*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include "integrator_ld_Vanden_Eijnden_Cicotti.h"

/*Allocates a strcuture of type 'integrator_langenvin_VE_C'.*/
void Aolloc_initialize_integration_structure(struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT fr, struct simudetails SimulParam)
{
  Integrator_struct->xi = (rvec*)malloc(fr.natoms * sizeof(rvec));
  Integrator_struct->eta = (rvec*)malloc(fr.natoms * sizeof(rvec));
  Integrator_struct->A = (rvec*)malloc(fr.natoms * sizeof(rvec));

  double h = SimulParam.timestep;
  Integrator_struct->h = h;
  Integrator_struct->h_squared = h * h;
  Integrator_struct->h_to_three_by_two = sqrt(h * h * h);
  Integrator_struct->square_root_of_h = sqrt(h);

  Integrator_struct->one_over_two_root_three = 1.0 / (2.0 * sqrt(3));
}



/*Generate and store gaussian random numbers*/
void Generate_and_store_random_numbers(FR_DAT *fr, struct integrator_langenvin_VE_C *Integrator_struct)
{
  int i, d, dim;

  dim = DIMENSION;

  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < dim ; d++)
        {
          Integrator_struct->xi[i][d] = getnorm();
          Integrator_struct->eta[i][d] = getnorm();
        }
    }
}



void Generate_and_store_random_numbers_omp(FR_DAT *fr, struct integrator_langenvin_VE_C *Integrator_struct, struct drand48_data *buffer)
{
  int i, d, dim;

  dim = DIMENSION;

  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < dim ; d++)
        {
          Integrator_struct->xi[i][d] = getnorm_omp(buffer);
          Integrator_struct->eta[i][d] = getnorm_omp(buffer);

          //printf("random number %f", Integrator_struct->xi[i][d]);
          //printf("random number %f", Integrator_struct->eta[i][d]);
          //printf("\n");
        }
    }
}




/*Calculate accelerations with present forces*/
void Calc_old_accelerations(FR_DAT *fr, rvec *accelerations_old)
{
  int i, d, dim;

  dim = DIMENSION;

  /* calculate current accelerations using current forces*/
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < dim ; d++)
        {
          accelerations_old[i][d] = fr->f[i][d] / fr->m[i];
        }
    }
}



/* Calculate A_i 's */
void Calc_A(FR_DAT *fr, rvec *accelerations_old, struct langevin *Langevin_info, struct integrator_langenvin_VE_C *Integrator_struct)
{
  int i, d, dim;
  double h, h_squared, h_to_three_by_two, one_over_two_root_three;

  dim = DIMENSION;

  h = Integrator_struct->h;
  h_squared = Integrator_struct->h_squared;
  h_to_three_by_two = Integrator_struct->h_to_three_by_two;
  one_over_two_root_three = Integrator_struct->one_over_two_root_three;

  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < dim ; d++)
        {
          Integrator_struct->A[i][d] = ( 0.5 * h_squared * ( accelerations_old[i][d] 
                                                           -  (Langevin_info->gamma[i] * fr->v[i][d]) ) ) 
                                       + ( Langevin_info->sigma_langevin[i] * h_to_three_by_two * ( (0.5 * Integrator_struct->xi[i][d]) 
                                                                                                 + (one_over_two_root_three * Integrator_struct->eta[i][d]) ) );
        }
    }
}



/*Calculate new positions*/
void Calc_new_positions(FR_DAT *const fr, struct integrator_langenvin_VE_C *Integrator_struct)
{
  int i, d, dim;
  double h = Integrator_struct->h;

  dim = DIMENSION;

  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < dim ; d++)
        {
          fr->x[i][d] = fr->x[i][d] + (h * fr->v[i][d]) + Integrator_struct->A[i][d];
        }
    }
}



/*Calculate new velocity*/
void Calc_new_velocities(FR_DAT *const fr, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, struct langevin *Langevin_info)
{
  int i, d, dim;
  double h, square_root_of_h;

  dim = DIMENSION;
  h = Integrator_struct->h;
  square_root_of_h = Integrator_struct->square_root_of_h;

  /*calculate new accelerations from new forces and then calculate new velocities*/
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < dim ; d++)
        {
          accelerations_new[i][d] = fr->f[i][d] / fr->m[i];
          fr->v[i][d] = fr->v[i][d] + (0.5 * h * (accelerations_old[i][d] + accelerations_new[i][d])) 
                        - (h * Langevin_info->gamma[i] * fr->v[i][d]) 
                        + (square_root_of_h * Langevin_info->sigma_langevin[i] * Integrator_struct->xi[i][d])
                        - (Langevin_info->gamma[i] * Integrator_struct->A[i][d]);
        }
    }

}






