/*
  Reads details of an MD run
*/

/* Last modified on December 05, 2011*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>


#include "read_simulation_parameters.h"


/*reads parameters for the MD simulation
  The filename is : 'SIMULPARAMS_NAMD_UNIT'
  Format:
  RUN_LENGTH_TIME_STEPS (run length in time steps %d)
  TIMESTEP (timestep in femto second %lf)
  COLLISION_FREQUENCY_TIMESTEP (frequency of thermalizing collisions in timestep %d)
  TEMPERATURE (temperature in kelvin %lf)
  CONFIG_SAVE_FREQUENCY (frequency of saving configuration to disk in timestep unit %d)
  PROGRESS_FREQUENCY (frequency for writing the information about the progress of the calculation in timestep %d)
  WRITE_FREQUENCY_OTHER (frequency for writing other stuff like kinetic energy, potential energy etc. in timestep unit %d)
  WRITE_FREQUENCY_RMSD (frequency for writing rmsd from initial structure or two end structures in 
                        timestep unit. %d)
  WRITE_FREQUENCY_PDB_TRAJ (frequency for writing configuration to the PDB trajectory file %d. This is
                            mainly for visualization purpose. This is also in the unit of time step)
*/
void ReadSimuDetails(struct simudetails *SimulParam, double *temperature)
{
  double value;
  char string[1000];
  FILE* indata1;

  indata1 = fopen("SIMULPARAMS_NAMD_UNIT", "r");
  if (indata1 == NULL)
    {
      printf ("Input file named 'SIMULPARAMS_NAMD_UNIT' not found. Exiting program\n");
      exit(1);
    }

  fscanf(indata1, "%s %lu", string, &(SimulParam->RUNLENGTH));
  if(SimulParam->RUNLENGTH == 0)
    {
      printf("Invalid entry for RUNLENGTH. Exiting program.\n");
      exit(1);
    }

  fscanf(indata1,"%s %lf",string, &(SimulParam->timestep));
  if(SimulParam->timestep <= 0.000000)
    {
      printf("Invalid entry for timestep. Exiting program.\n");
       exit(1);
    }

  fscanf(indata1,"%s %d", string, &(SimulParam->ColliFreqInTimestep));

  fscanf(indata1,"%s %lf", string, &value);
  *temperature = value;
  
  /*
  fscanf(indata1, "%d", &(SimulParam->NumFrames));
  if((SimulParam->NumFrames <= 0))
    {
      printf("NumFrames should be positive. Please check input\n");
      exit(1);
    }
  fscanf(indata1, "%lf", &(SimulParam->MaxDistForRDF));
  fscanf(indata1, "%lf", &(SimulParam->deltar));*/

  fscanf(indata1, "%s %lu", string, &(SimulParam->ConfigSaveFreqTimeStep));

  fscanf(indata1, "%s %lu", string, &(SimulParam->ProgressFreqTimeStep));

  fscanf(indata1, "%s %lu", string, &(SimulParam->WriteFreqTimestep)); 

  fscanf(indata1, "%s %lu", string, &(SimulParam->WriteFreqRMSD)); 

  fscanf(indata1, "%s %lu", string, &(SimulParam->WriteFreqPDBtraj));
  
  fclose(indata1);

  /*Ceonvert timestep from femotosecond to internal time unit*/
  SimulParam->timestep = SimulParam->timestep * FS_TO_INTERNAL_TIME;

}


