#ifndef _output_h
#define _output_h

#include "md_ld_common_d_dimension_namd.h"


int StartFiles(struct simudetails *SimulParam, FILE **energy, FILE **KE, FILE **PE, FILE **pressure);


int WriteFiles(FR_DAT *fr, FILE *energy, FILE *KE, FILE *PE, FILE *pressure);


int CalculateAveStdDev(char *Filename, double *Average, double *StandardDeviation);

int WriteSummary(struct simudetails *SimulParam, FR_DAT *fr, double temperature, double Average_kinetic_enenrgy);

void Write_masses(FR_DAT *fr);

void Write_friction_coefficients(FR_DAT *fr, struct langevin Langevin_info);


#endif
