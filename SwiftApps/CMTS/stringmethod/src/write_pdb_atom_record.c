/*Read and write one PDB style ATOM record */

/*Last modified on December 01, 2011*/


/* FORTRAN format for writing PDB records
 * ATOM
 * HETATM Format(A6,I5,1X,A4,A1,A3,1X,A1,I4,A1,3X,3F8.3,2F6.2,10X,A2,A2)
 *
 * HELIX  Format(A6,1X,I3,1X,A3,2(1X,A3,1X,A1,1X,I4,A1),I2,A30,1X,I5)
 *
 * SHEET Format(A6,1X,I3,1X,A3,I2,2(1X,A3,1X,A1,I4,A1),I2,2(1X,A4,A3,1X,A1,I4,A1))
 *
 * SSBOND Format(A6,1X,I3,1X,A3,1X,A1,1X,I4,A1,3X,A3,1X,A1,1X,I4,A1,23X,2(2I3,1X),F5.2)
 * */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "write_pdb_atom_record.h"


/* Write one ATOM record for a CA atom to a file. This is suitable for writing output of our
 * programs that work with C-alpha s only. Relevant fields are supplied as arguments other
 * fields are populated with generic values.*/
void Write_one_PDB_ATOM_CA(FILE *out, char residue_name[4], int residue_sequence_number, float x, float y, float z)
{
  PDB_ATOM one_PDB_ATOM;
  
  one_PDB_ATOM.atom_serial_number = residue_sequence_number;  /*here atom and residue sare same*/
  sprintf(one_PDB_ATOM.atom_name, "CA");
  one_PDB_ATOM.atom_alt_loc = ' ';
  sprintf(one_PDB_ATOM.residue_name, residue_name);
  one_PDB_ATOM.chain_id = 'A';
  one_PDB_ATOM.residue_sequence_number = residue_sequence_number;
  one_PDB_ATOM.insert_code = ' ';
  one_PDB_ATOM.x = x;
  one_PDB_ATOM.y = y;
  one_PDB_ATOM.z = z;
  one_PDB_ATOM.occupancy = 1.00;
  one_PDB_ATOM.temperature_factor = 1.00;
  sprintf(one_PDB_ATOM.element_symbol, "A");
  sprintf(one_PDB_ATOM.charge_on_atom, "C");
  
  Write_one_PDB_ATOM(out, &one_PDB_ATOM);
}

void Write_one_PDB_ATOM_CA2(FILE *out, char atom_name[3], char residue_name[4], int residue_sequence_number, float x, float y, float z)
{
  PDB_ATOM one_PDB_ATOM;
  
  one_PDB_ATOM.atom_serial_number = residue_sequence_number;  /*here atom and residue sare same*/
  sprintf(one_PDB_ATOM.atom_name, atom_name);
  one_PDB_ATOM.atom_alt_loc = ' ';
  sprintf(one_PDB_ATOM.residue_name, residue_name);
  one_PDB_ATOM.chain_id = 'A';
  one_PDB_ATOM.residue_sequence_number = residue_sequence_number;
  one_PDB_ATOM.insert_code = ' ';
  one_PDB_ATOM.x = x;
  one_PDB_ATOM.y = y;
  one_PDB_ATOM.z = z;
  one_PDB_ATOM.occupancy = 1.00;
  one_PDB_ATOM.temperature_factor = 1.00;
  sprintf(one_PDB_ATOM.element_symbol, "A");
  sprintf(one_PDB_ATOM.charge_on_atom, "C");
  
  Write_one_PDB_ATOM(out, &one_PDB_ATOM);
}

/* THIS HAS ALL THE FIELDS. Write one ATOM record to a file.
 * FORTRAM Format(A6,I5,1X,A4,A1,A3,1X,A1,I4,A1,3X,3F8.3,2F6.2,10X,A2,A2)
 * The code follows this FORTRAN format which is consistent with the official format.
 * */
void Write_one_PDB_ATOM(FILE *out, PDB_ATOM *one_PDB_ATOM)
{
  fprintf(out, "ATOM  %5d %-4s%c%-3s %c%4d%c   %8.3f%8.3f%8.3f%6.2f%6.2f          %-2s%-2s\n", 
               one_PDB_ATOM->atom_serial_number, 
               one_PDB_ATOM->atom_name, 
               one_PDB_ATOM->atom_alt_loc, 
               one_PDB_ATOM->residue_name, 
               one_PDB_ATOM->chain_id, 
               one_PDB_ATOM->residue_sequence_number, 
               one_PDB_ATOM->insert_code, 
               one_PDB_ATOM->x, 
               one_PDB_ATOM->y, 
               one_PDB_ATOM->z, 
               one_PDB_ATOM->occupancy, 
               one_PDB_ATOM->temperature_factor, 
               one_PDB_ATOM->element_symbol, 
               one_PDB_ATOM->charge_on_atom);
}




