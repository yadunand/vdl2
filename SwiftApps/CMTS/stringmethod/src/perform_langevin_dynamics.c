/* Main loop for Langevin dynamics simulation*/

/* Last modified on December 19, 2011*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

#include "perform_langevin_dynamics.h"

void Perform_langevin_dynamics(FR_DAT *fr, struct simudetails *SimulParam, struct langevin *Langevin_info, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, double *Average_kinetic_enenrgy)
{
  long int q;
  long int config_index;

  double rmsd_from_structure_1, rmsd_from_structure_2, time_in_fs;
  
  /* main MD/LD loop*/
  for(q = 0; q < SimulParam->RUNLENGTH; q++)
    {
      /** Writes structure related output files **/
      /* saves configuration to trajectory file*/
     /* if(q % SimulParam->ConfigSaveFreqTimeStep == 0)
        {
          Write_config_to_trajectory_file_only_coordinates(fr, trajectory);
        } */
      /* Write RMSD*/
     /* if((q % SimulParam->WriteFreqRMSD) == 0)
        {
          time_in_fs = fr->t * INTERNAL_TIME_TO_FS;
          rmsd_from_structure_1 = Calc_rmsd(fr, &(*coordinates_all_structures)[0]);
          rmsd_from_structure_2 = Calc_rmsd(fr, &(*coordinates_all_structures)[1]);
          fprintf(rmsd_1, "%.8e   %.15e\n", time_in_fs, rmsd_from_structure_1);
          fprintf(rmsd_2, "%.8e   %.15e\n", time_in_fs, rmsd_from_structure_2);
        } */
      /* Write config to PDB trajectory file*/
     /* if((q % SimulParam->WriteFreqPDBtraj) == 0)
        {
          config_index = (long int)(q / SimulParam->WriteFreqPDBtraj) + 1;
          Write_config_to_a_PDB_trajectory_file_1(pdb_traj, fr, Residue_names, config_index);
        } */
      /** End of writing structure related output files **/

      /* Calcualte total energy*/
      get_kinetic_energy(fr);
      fr->E = fr->U + fr->K;

      /* Writes various quantites to files*/
     // if((q % SimulParam->WriteFreqTimestep) == 0)
       // {
         // WriteFiles(fr, energy, KE, PE, pressure);
          /*Wrtie two contribution to potential energy*/
         // fprintf(out_r, "%.15e\n", fr->U_r);
         // fprintf(out_en, "%.15e\n", fr->U_en);
       // }
      /* Prints updates at regular interval*/
    /*  if ((q % SimulParam->ProgressFreqTimeStep) == 0)
        {
          fprintf (progress, "%g       %lu          %g              %g             %g\n", (fr->t * INTERNAL_TIME_TO_FS), q, fr->U, fr->K, fr->E);
          fflush(progress);
        }
*/
      /*Sum total kinetic eenrgy*/
      *Average_kinetic_enenrgy = *Average_kinetic_enenrgy + fr->K;
      
      /** Perform integration using Vanden-Eijnden Cicotti integrator **/
      /* Generate random numbers*/
      Generate_and_store_random_numbers(fr, Integrator_struct);
      /* Calculate old accelerations*/
      Calc_old_accelerations(fr, accelerations_old);
      /*Calculate the A vectors*/
      Calc_A(fr, accelerations_old, Langevin_info, Integrator_struct);
      /* Calculate new positions */
      Calc_new_positions(fr, Integrator_struct);
      /* Calculates new forces using new positions. Also calculate the new potential energy*/ 
      getforces(fr, coordinates_all_structures, Num_input_structures, array_of_pair_distance_matrices, force_constants_all_pairs, sigmas_for_repulsive_part, epsilons_for_repulsive_part, array_of_mixing_parameters, aray_of_cutoff_distances, energy_difference_from_first_structure);
      /* Calculate new velocities*/
      Calc_new_velocities(fr, accelerations_old, accelerations_new, Integrator_struct, Langevin_info);
      /*removes the drift*/
      velocenter(fr);

      /* Increase the time*/
      fr->t = fr->t + SimulParam->timestep;
    }
}


void Perform_langevin_dynamics_cons(FR_DAT *fr, struct simudetails *SimulParam, struct langevin *Langevin_info, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, double *Average_kinetic_enenrgy, collective_variables *cons_distances, int Num_cv, double KK)
{
  long int q;
  long int config_index;

  double rmsd_from_structure_1, rmsd_from_structure_2, time_in_fs;
  
  /* main MD/LD loop*/
  for(q = 0; q < SimulParam->RUNLENGTH; q++)
    {
      /** Writes structure related output files **/
      /* saves configuration to trajectory file*/
     /* if(q % SimulParam->ConfigSaveFreqTimeStep == 0)
        {
          Write_config_to_trajectory_file_only_coordinates(fr, trajectory);
        } */
      /* Write RMSD*/
     /* if((q % SimulParam->WriteFreqRMSD) == 0)
        {
          time_in_fs = fr->t * INTERNAL_TIME_TO_FS;
          rmsd_from_structure_1 = Calc_rmsd(fr, &(*coordinates_all_structures)[0]);
          rmsd_from_structure_2 = Calc_rmsd(fr, &(*coordinates_all_structures)[1]);
          fprintf(rmsd_1, "%.8e   %.15e\n", time_in_fs, rmsd_from_structure_1);
          fprintf(rmsd_2, "%.8e   %.15e\n", time_in_fs, rmsd_from_structure_2);
        } */
      /* Write config to PDB trajectory file*/
     /* if((q % SimulParam->WriteFreqPDBtraj) == 0)
        {
          config_index = (long int)(q / SimulParam->WriteFreqPDBtraj) + 1;
          Write_config_to_a_PDB_trajectory_file_1(pdb_traj, fr, Residue_names, config_index);
        } */
      /** End of writing structure related output files **/

      /* Calcualte total energy*/
      get_kinetic_energy(fr);
      fr->E = fr->U + fr->K;

      /* Writes various quantites to files*/
     // if((q % SimulParam->WriteFreqTimestep) == 0)
       // {
         // WriteFiles(fr, energy, KE, PE, pressure);
          /*Wrtie two contribution to potential energy*/
         // fprintf(out_r, "%.15e\n", fr->U_r);
         // fprintf(out_en, "%.15e\n", fr->U_en);
       // }
      /* Prints updates at regular interval*/
    /*  if ((q % SimulParam->ProgressFreqTimeStep) == 0)
        {
          fprintf (progress, "%g       %lu          %g              %g             %g\n", (fr->t * INTERNAL_TIME_TO_FS), q, fr->U, fr->K, fr->E);
          fflush(progress);
        }
*/
      /*Sum total kinetic eenrgy*/
      *Average_kinetic_enenrgy = *Average_kinetic_enenrgy + fr->K;
      
      /** Perform integration using Vanden-Eijnden Cicotti integrator **/
      /* Generate random numbers*/
      Generate_and_store_random_numbers(fr, Integrator_struct);
      /* Calculate old accelerations*/
      Calc_old_accelerations(fr, accelerations_old);
      /*Calculate the A vectors*/
      Calc_A(fr, accelerations_old, Langevin_info, Integrator_struct);
      /* Calculate new positions */
      Calc_new_positions(fr, Integrator_struct);
      /* Calculates new forces using new positions. Also calculate the new potential energy*/ 
      getforces_cons(fr, coordinates_all_structures, Num_input_structures, array_of_pair_distance_matrices, force_constants_all_pairs, sigmas_for_repulsive_part, epsilons_for_repulsive_part, array_of_mixing_parameters, aray_of_cutoff_distances, energy_difference_from_first_structure, cons_distances, Num_cv, KK);
      /* Calculate new velocities*/
      Calc_new_velocities(fr, accelerations_old, accelerations_new, Integrator_struct, Langevin_info);
      /*removes the drift*/
      velocenter(fr);

      /* Increase the time*/
      fr->t = fr->t + SimulParam->timestep;
    }
}



void Perform_langevin_dynamics_cons_harm(FR_DAT *fr, struct simudetails *SimulParam, struct langevin *Langevin_info, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, double *Average_kinetic_enenrgy, collective_variables *cons_distances, int Num_cv, FR_DAT fix_position, double KK, double Kharm)
{
  long int q;
  long int config_index;

  double rmsd_from_structure_1, rmsd_from_structure_2, time_in_fs;
  
  /* main MD/LD loop*/
  for(q = 0; q < SimulParam->RUNLENGTH; q++)
    {
      /** Writes structure related output files **/
      /* saves configuration to trajectory file*/
     /* if(q % SimulParam->ConfigSaveFreqTimeStep == 0)
        {
          Write_config_to_trajectory_file_only_coordinates(fr, trajectory);
        } */
      /* Write RMSD*/
     /* if((q % SimulParam->WriteFreqRMSD) == 0)
        {
          time_in_fs = fr->t * INTERNAL_TIME_TO_FS;
          rmsd_from_structure_1 = Calc_rmsd(fr, &(*coordinates_all_structures)[0]);
          rmsd_from_structure_2 = Calc_rmsd(fr, &(*coordinates_all_structures)[1]);
          fprintf(rmsd_1, "%.8e   %.15e\n", time_in_fs, rmsd_from_structure_1);
          fprintf(rmsd_2, "%.8e   %.15e\n", time_in_fs, rmsd_from_structure_2);
        } */
      /* Write config to PDB trajectory file*/
     /* if((q % SimulParam->WriteFreqPDBtraj) == 0)
        {
          config_index = (long int)(q / SimulParam->WriteFreqPDBtraj) + 1;
          Write_config_to_a_PDB_trajectory_file_1(pdb_traj, fr, Residue_names, config_index);
        } */
      /** End of writing structure related output files **/

      /* Calcualte total energy*/
      get_kinetic_energy(fr);
      fr->E = fr->U + fr->K;

      /* Writes various quantites to files*/
     // if((q % SimulParam->WriteFreqTimestep) == 0)
       // {
         // WriteFiles(fr, energy, KE, PE, pressure);
          /*Wrtie two contribution to potential energy*/
         // fprintf(out_r, "%.15e\n", fr->U_r);
         // fprintf(out_en, "%.15e\n", fr->U_en);
       // }
      /* Prints updates at regular interval*/
    /*  if ((q % SimulParam->ProgressFreqTimeStep) == 0)
        {
          fprintf (progress, "%g       %lu          %g              %g             %g\n", (fr->t * INTERNAL_TIME_TO_FS), q, fr->U, fr->K, fr->E);
          fflush(progress);
        }
*/
      /*Sum total kinetic eenrgy*/
      *Average_kinetic_enenrgy = *Average_kinetic_enenrgy + fr->K;
      
      /** Perform integration using Vanden-Eijnden Cicotti integrator **/
      /* Generate random numbers*/
      Generate_and_store_random_numbers(fr, Integrator_struct);
      /* Calculate old accelerations*/
      Calc_old_accelerations(fr, accelerations_old);
      /*Calculate the A vectors*/
      Calc_A(fr, accelerations_old, Langevin_info, Integrator_struct);
      /* Calculate new positions */
      Calc_new_positions(fr, Integrator_struct);
      /* Calculates new forces using new positions. Also calculate the new potential energy*/ 
      getforces_cons_harm(fr, coordinates_all_structures, Num_input_structures, array_of_pair_distance_matrices, force_constants_all_pairs, sigmas_for_repulsive_part, epsilons_for_repulsive_part, array_of_mixing_parameters, aray_of_cutoff_distances, energy_difference_from_first_structure, cons_distances, Num_cv, fix_position, KK, Kharm);
      /* Calculate new velocities*/
      Calc_new_velocities(fr, accelerations_old, accelerations_new, Integrator_struct, Langevin_info);
      /*removes the drift*/
      velocenter(fr);

      /* Increase the time*/
      fr->t = fr->t + SimulParam->timestep;
    }
}



void Perform_langevin_dynamics_harm(FR_DAT *fr, struct simudetails *SimulParam, struct langevin *Langevin_info, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, double *Average_kinetic_enenrgy, FR_DAT fix_position, double Kharm)
{
  long int q;
  long int config_index;

  double rmsd_from_structure_1, rmsd_from_structure_2, time_in_fs;
  
  /* main MD/LD loop*/
  for(q = 0; q < SimulParam->RUNLENGTH; q++)
    {
      /** Writes structure related output files **/
      /* saves configuration to trajectory file*/
     /* if(q % SimulParam->ConfigSaveFreqTimeStep == 0)
        {
          Write_config_to_trajectory_file_only_coordinates(fr, trajectory);
        } */
      /* Write RMSD*/
     /* if((q % SimulParam->WriteFreqRMSD) == 0)
        {
          time_in_fs = fr->t * INTERNAL_TIME_TO_FS;
          rmsd_from_structure_1 = Calc_rmsd(fr, &(*coordinates_all_structures)[0]);
          rmsd_from_structure_2 = Calc_rmsd(fr, &(*coordinates_all_structures)[1]);
          fprintf(rmsd_1, "%.8e   %.15e\n", time_in_fs, rmsd_from_structure_1);
          fprintf(rmsd_2, "%.8e   %.15e\n", time_in_fs, rmsd_from_structure_2);
        } */
      /* Write config to PDB trajectory file*/
     /* if((q % SimulParam->WriteFreqPDBtraj) == 0)
        {
          config_index = (long int)(q / SimulParam->WriteFreqPDBtraj) + 1;
          Write_config_to_a_PDB_trajectory_file_1(pdb_traj, fr, Residue_names, config_index);
        } */
      /** End of writing structure related output files **/

      /* Calcualte total energy*/
      get_kinetic_energy(fr);
      fr->E = fr->U + fr->K;

      /* Writes various quantites to files*/
     // if((q % SimulParam->WriteFreqTimestep) == 0)
       // {
         // WriteFiles(fr, energy, KE, PE, pressure);
          /*Wrtie two contribution to potential energy*/
         // fprintf(out_r, "%.15e\n", fr->U_r);
         // fprintf(out_en, "%.15e\n", fr->U_en);
       // }
      /* Prints updates at regular interval*/
    /*  if ((q % SimulParam->ProgressFreqTimeStep) == 0)
        {
          fprintf (progress, "%g       %lu          %g              %g             %g\n", (fr->t * INTERNAL_TIME_TO_FS), q, fr->U, fr->K, fr->E);
          fflush(progress);
        }
*/
      /*Sum total kinetic eenrgy*/
      *Average_kinetic_enenrgy = *Average_kinetic_enenrgy + fr->K;
      
      /** Perform integration using Vanden-Eijnden Cicotti integrator **/
      /* Generate random numbers*/
      Generate_and_store_random_numbers(fr, Integrator_struct);
      /* Calculate old accelerations*/
      Calc_old_accelerations(fr, accelerations_old);
      /*Calculate the A vectors*/
      Calc_A(fr, accelerations_old, Langevin_info, Integrator_struct);
      /* Calculate new positions */
      Calc_new_positions(fr, Integrator_struct);
      /* Calculates new forces using new positions. Also calculate the new potential energy*/ 
      getforces_harm(fr, coordinates_all_structures, Num_input_structures, array_of_pair_distance_matrices, force_constants_all_pairs, sigmas_for_repulsive_part, epsilons_for_repulsive_part, array_of_mixing_parameters, aray_of_cutoff_distances, energy_difference_from_first_structure, fix_position, Kharm);
      /* Calculate new velocities*/
      Calc_new_velocities(fr, accelerations_old, accelerations_new, Integrator_struct, Langevin_info);
      /*removes the drift*/
      velocenter(fr);

      /* Increase the time*/
      fr->t = fr->t + SimulParam->timestep;
    }
}


void Perform_langevin_dynamics_harm_omp(FR_DAT *fr, struct simudetails *SimulParam, struct langevin *Langevin_info, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, double *Average_kinetic_enenrgy, FR_DAT fix_position, double Kharm, int Num_atoms, struct drand48_data *buffer)
{
  long int q;
  long int config_index;

  double rmsd_from_structure_1, rmsd_from_structure_2, time_in_fs;
  
  /* main MD/LD loop*/
  for(q = 0; q < SimulParam->RUNLENGTH; q++)
    {

      /* Calcualte total energy*/
      get_kinetic_energy(fr);
      fr->E = fr->U + fr->K;

      /*Sum total kinetic eenrgy*/
      *Average_kinetic_enenrgy = *Average_kinetic_enenrgy + fr->K;
      
      /** Perform integration using Vanden-Eijnden Cicotti integrator **/
      /* Generate random numbers*/
      Generate_and_store_random_numbers_omp(fr, Integrator_struct, buffer);
      /* Calculate old accelerations*/
      Calc_old_accelerations(fr, accelerations_old);
      /*Calculate the A vectors*/
      Calc_A(fr, accelerations_old, Langevin_info, Integrator_struct);
      /* Calculate new positions */
      Calc_new_positions(fr, Integrator_struct);
      /* Calculates new forces using new positions. Also calculate the new potential energy*/ 
      getforces_harm_omp(fr, coordinates_all_structures, Num_input_structures, array_of_pair_distance_matrices, force_constants_all_pairs, sigmas_for_repulsive_part, epsilons_for_repulsive_part, array_of_mixing_parameters, aray_of_cutoff_distances, energy_difference_from_first_structure, fix_position, Kharm, Num_atoms);
      /* Calculate new velocities*/
      Calc_new_velocities(fr, accelerations_old, accelerations_new, Integrator_struct, Langevin_info);
      /*removes the drift*/
      velocenter(fr);

      /* Increase the time*/
      fr->t = fr->t + SimulParam->timestep;
    }
}


void Perform_langevin_dynamics_cons_harm_omp(FR_DAT *fr, struct simudetails *SimulParam, struct langevin *Langevin_info, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, double *Average_kinetic_enenrgy, collective_variables *cons_distances, int Num_cv, FR_DAT fix_position, double KK, double Kharm, int Num_atoms, struct drand48_data *buffer)
{
  long int q;
  long int config_index;

  double rmsd_from_structure_1, rmsd_from_structure_2, time_in_fs;
  
  /* main MD/LD loop*/
  for(q = 0; q < SimulParam->RUNLENGTH; q++)
    {

      /* Calcualte total energy*/
      get_kinetic_energy(fr);
      fr->E = fr->U + fr->K;

      /*Sum total kinetic eenrgy*/
      *Average_kinetic_enenrgy = *Average_kinetic_enenrgy + fr->K;
      
      /** Perform integration using Vanden-Eijnden Cicotti integrator **/
      /* Generate random numbers*/
      Generate_and_store_random_numbers_omp(fr, Integrator_struct, buffer);
      /* Calculate old accelerations*/
      Calc_old_accelerations(fr, accelerations_old);
      /*Calculate the A vectors*/
      Calc_A(fr, accelerations_old, Langevin_info, Integrator_struct);
      /* Calculate new positions */
      Calc_new_positions(fr, Integrator_struct);
      /* Calculates new forces using new positions. Also calculate the new potential energy*/ 
      getforces_cons_harm_omp(fr, coordinates_all_structures, Num_input_structures, array_of_pair_distance_matrices, force_constants_all_pairs, sigmas_for_repulsive_part, epsilons_for_repulsive_part, array_of_mixing_parameters, aray_of_cutoff_distances, energy_difference_from_first_structure, cons_distances, Num_cv, fix_position, KK, Kharm, Num_atoms);
      /* Calculate new velocities*/
      Calc_new_velocities(fr, accelerations_old, accelerations_new, Integrator_struct, Langevin_info);
      /*removes the drift*/
      velocenter(fr);

      /* Increase the time*/
      fr->t = fr->t + SimulParam->timestep;
    }
}



