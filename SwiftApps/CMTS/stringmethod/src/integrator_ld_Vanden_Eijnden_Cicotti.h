/*Written by Avisek Das*/
/*Last modified on September 06, 2011*/


#ifndef _integrator_ld_Vanden_Eijnden_Cicotti_h
#define _integrator_ld_Vanden_Eijnden_Cicotti_h

#include "md_ld_common_d_dimension_namd.h"
#include "common_functions.h"


/* Structure for implementing Langevin dynamics integrator due to Van-Eijnden and Cicotti
 * for detailed meaning of the symbols see the document 'integrator-langevin.pdf' by Avisek
 * */
struct integrator_langenvin_VE_C
{
  rvec *xi;         /*Array for storing the Gaussian random numbers. xi[i][0] 
                      random number of the x component of the i'th particle*/
  rvec *eta;        /*Array of random numbers*/
  rvec *A;          /*This quantity needs to be calcualted for all the particle at 
                      every timestep.*/
  double h;         /*timestep*/
  double h_squared; 
  double h_to_three_by_two; /*h^{3/2}*/
  double square_root_of_h;  /*\sqrt(h)*/ 
  double one_over_two_root_three;  /*1 / (2 \sqrt(3))*/
};



void Aolloc_initialize_integration_structure(struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT fr, struct simudetails SimulParam);

void Generate_and_store_random_numbers(FR_DAT *fr, struct integrator_langenvin_VE_C *Integrator_struct);

void Generate_and_store_random_numbers_omp(FR_DAT *fr, struct integrator_langenvin_VE_C *Integrator_struct, struct drand48_data *buffer);

void Calc_old_accelerations(FR_DAT *fr, rvec *accelerations_old);

void Calc_A(FR_DAT *fr, rvec *accelerations_old, struct langevin *Langevin_info, struct integrator_langenvin_VE_C *Integrator_struct);

void Calc_new_positions(FR_DAT *const fr, struct integrator_langenvin_VE_C *Integrator_struct);

void Calc_new_velocities(FR_DAT *const fr, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, struct langevin *Langevin_info);


#endif
