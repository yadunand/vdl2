#ifndef _write_velocities_h 
#define _write_velocities_h

#include "md_ld_common_d_dimension_namd.h"

void Write_velocities(FR_DAT *fr, char *output_filename);

void Write_velocity_to_files(FR_DAT *fr, FILE *output_file_pointer_x, FILE *output_file_pointer_y);

#endif
