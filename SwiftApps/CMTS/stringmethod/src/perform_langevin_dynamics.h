#ifndef _perform_langevin_dynamics_h
#define _perform_langevin_dynamics_h


#include "calc_rmsd_from_one_structure.h"
#include "en_boltzmann_mixing_stuff.h"
#include "integrator_ld_Vanden_Eijnden_Cicotti.h"
#include "md_ld_common_d_dimension_namd.h"
#include "output.h"
#include "write_CA_PDB_files.h"
#include "write_pdb_atom_record.h"
#include "wrtie_configuration.h"


void Perform_langevin_dynamics(FR_DAT *fr, struct simudetails *SimulParam, struct langevin *Langevin_info, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, double *Average_kinetic_enenrgy);

void Perform_langevin_dynamics_cons(FR_DAT *fr, struct simudetails *SimulParam, struct langevin *Langevin_info, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, double *Average_kinetic_enenrgy, collective_variables *cons_distances, int Num_cv, double KK);

void Perform_langevin_dynamics_harm(FR_DAT *fr, struct simudetails *SimulParam, struct langevin *Langevin_info, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, double *Average_kinetic_enenrgy, FR_DAT fix_position, double Kharm);

void Perform_langevin_dynamics_cons_harm(FR_DAT *fr, struct simudetails *SimulParam, struct langevin *Langevin_info, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, double *Average_kinetic_enenrgy, collective_variables *cons_distances, int Num_cv, FR_DAT fix_position, double KK, double Kharm);

void Perform_langevin_dynamics_harm_omp(FR_DAT *fr, struct simudetails *SimulParam, struct langevin *Langevin_info, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, double *Average_kinetic_enenrgy, FR_DAT fix_position, double Kharm, int Num_atoms, struct drand48_data *buffer);

void Perform_langevin_dynamics_cons_harm_omp(FR_DAT *fr, struct simudetails *SimulParam, struct langevin *Langevin_info, rvec *accelerations_old, rvec *accelerations_new, struct integrator_langenvin_VE_C *Integrator_struct, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, double *Average_kinetic_enenrgy, collective_variables *cons_distances, int Num_cv, FR_DAT fix_position, double KK, double Kharm, int Num_atoms, struct drand48_data *buffer);
#endif
