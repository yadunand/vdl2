/* Reads a text file with velocities of the particles.*/

/* last modified on September 06, 2011*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include "write_velocities.h"


/* Format of the input file:
---------------------------------------------------------------
Number of atoms
<blank line>
x_velocity_of_particle_1 y_velocity_of_particle_1 z_velocity_of_particle_1
x_velocity_of_particle_2 y_velocity_of_particle_2 z_velocity_of_particle_2
etc. total no. of lines should be equal to total number of atoms
*/

void Write_velocities(FR_DAT *fr, char *output_filename)
{
  int i, d;
  int dim = DIMENSION;
  FILE *out;

  out = fopen(output_filename, "w");
  
  fprintf(out, "%d\n\n", fr->natoms);

  /* write particle velocities*/
  for(i = 0 ; i < fr->natoms ; i++)
    {
      //fprintf(out, "%.15e %.15e %.15e\n", fr->v[i][0], fr->v[i][1], fr->v[i][2]);
      for(d = 0 ; d < dim ; d++)
        {
          fprintf(out, "%.15e ", fr->v[i][d]);
        }
      fprintf(out, "\n");
    }

  fclose(out);

}



/*Writes x and y components of the velocity to two different files at regular interval*/
void Write_velocity_to_files(FR_DAT *fr, FILE *output_file_pointer_x, FILE *output_file_pointer_y)
{

  fprintf(output_file_pointer_x, "%.15e\n", fr->v[0][0]);
  fprintf(output_file_pointer_y, "%.15e\n", fr->v[0][1]);
    
}




