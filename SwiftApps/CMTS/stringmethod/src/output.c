/*Last modified on November 14, 2011*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>


#include "output.h"




/*This function opens files for writing energy,KE,PE,pressure at each MD time-step and for tracking progress*/
int StartFiles(struct simudetails *SimulParam, FILE **energy, FILE **KE, FILE **PE, FILE **pressure)
{
  //long int totalsteps = SimulParam->RUNLENGTH;
  int Num_data_other_output;     /* Number of data points in output files. This is 
                                    determined by 'WRITE_FREQUENCY_OTHER' input in the SIMULPARAMS file*/

  Num_data_other_output = (int)(SimulParam->RUNLENGTH / SimulParam->WriteFreqTimestep);
  
  *energy = fopen("total-energies", "w");// energy values are stored in a file called "energies.dat"
  fprintf (*energy, "%d\n", Num_data_other_output);
  *KE = fopen ("kinetic-energies", "w"); /* kinetic energy values are stored in a file called "kinetic-energies"*/
  fprintf (*KE, "%d\n", Num_data_other_output);
  *PE = fopen("potential-energies", "w");/*potential enrgy values are stored in file called "potential-energies"*/
  fprintf (*PE, "%d\n", Num_data_other_output);
  //*pressure = fopen("pressures", "w"); /*pressures at each time step are stored in "pressures"*/
  //fprintf (*pressure, "%lu\n", totalsteps);
  return 0;
}

/*This function writes energy, KE, PE, pressure at each time-step to the respective files*/
int WriteFiles(FR_DAT *fr, FILE *energy, FILE *KE, FILE *PE, FILE *pressure)
{
  fprintf (energy, "%.15e\n", fr->E);
  fprintf (KE, "%.15e\n", fr->K);
  fprintf (PE, "%.15e\n", fr->U);
  //fprintf (pressure, "%.15e\n", fr->P);
  return 0;
}


/*this function is used to calculate the averages and standard deviations of energy, KE, pressure etc at the end of the run. The function takes filename as input 
and open that file to calcualte average and standard deviation. The file should be in the usual format, i.e. first line should have the number of 
entries and then each line should have single entry.*/
int CalculateAveStdDev(char *Filename, double *Average, double *StandardDeviation)
{
  long int NumDataPoints;
  int i;
  double FirstDataPoint, DataPoint, SumOfSquares = 0.0, Sum = 0.0, Temp, Mean, MSD;
  FILE *input;
  input = fopen(Filename, "r");
  fscanf (input, " %lu", &NumDataPoints);
  
  fscanf(input, "%lf", &FirstDataPoint);
  for(i = 1 ; i < NumDataPoints ; i++)
    {
      fscanf(input, "%lf", &DataPoint);
      Temp = DataPoint - FirstDataPoint;
      Sum = Sum + Temp;
      SumOfSquares = SumOfSquares + Temp*Temp;
    }
  fclose(input);
  Mean = (double)(Sum/NumDataPoints);
  MSD = (double)(SumOfSquares/NumDataPoints) - Mean*Mean;
  *StandardDeviation = sqrt(MSD);
  *Average = Mean + FirstDataPoint;
  return 0;
}



/*wirites the summary of the calculation performed in a file called 'summary'*/
int WriteSummary(struct simudetails *SimulParam, FR_DAT *fr, double temperature, double Average_kinetic_enenrgy)
{
  //int d;
  int dim = DIMENSION;
  FILE *rite;
  rite = fopen("summary", "w");
  fprintf(rite, "SUMMARY OF MOLECULAR DYNAMICS / LANGEVIN DYNAMICS SIMULATION PERFORMED\n");
  fprintf(rite, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
  fprintf(rite, "Number of particles: %d\n", fr->natoms);
  /*fprintf(rite, "Box Length: ");
  for(d = 0 ; d < dim ; d++)
    {
      fprintf(rite, "%lf ", fr->box[d][d]);
    }
  fprintf(rite, "\n");*/
  fprintf(rite, "Temperature: %lf K\n", temperature);

  fprintf(rite, "\nDetails of Simulation Parameters\n");
  fprintf(rite, "--------------------------------\n");
  fprintf(rite, "Runlength in the unit of time-steps: %lu\n", SimulParam->RUNLENGTH);
  fprintf(rite, "Runlength in the unit of time: %.15e fs\n", ( ((double)(SimulParam->RUNLENGTH))*(SimulParam->timestep) * INTERNAL_TIME_TO_FS));
  fprintf(rite, "Timestep: %.15e internal unit\n", SimulParam->timestep);
  fprintf(rite, "Timestep: %.15e fs\n", (SimulParam->timestep * INTERNAL_TIME_TO_FS));
  //fprintf(rite, "Collision frequency in unit of time-steps: %d\n", SimulParam->ColliFreqInTimestep);

 

  /*calculates some average quantities*/
  //double avePressure, aveTotalEnergy, aveKE, avePE;
  //double stddevPressure, stddevTotalEnergy, stddevKE, stddevPE;
  double aveTotalEnergy, aveKE, avePE;
  double stddevTotalEnergy, stddevKE, stddevPE;
  char filename[256];
  
  //sprintf(filename, "pressures");
  //CalculateAveStdDev(filename, &avePressure, &stddevPressure);
  
  sprintf(filename, "total-energies");
  CalculateAveStdDev(filename, &aveTotalEnergy, &stddevTotalEnergy);
  
  sprintf(filename, "kinetic-energies");
  CalculateAveStdDev(filename, &aveKE, &stddevKE);
  
  sprintf(filename, "potential-energies");
  CalculateAveStdDev(filename, &avePE, &stddevPE);

  fprintf(rite, "\nAverages and Standard Deviations of various quantities from output files\n");
  fprintf(rite, "-------------------------------------------------------\n");
  
  //fprintf(rite, "Average Pressure: %lf          Standard Deviation of Pressure: %lf\n", avePressure, stddevPressure);
  fprintf(rite, "Average Total Energy: %.15e           Standard Deviation of Total Energy: %.15e\n", aveTotalEnergy, stddevTotalEnergy);
  fprintf(rite, "Average Kinetic Energy: %lf          Standard Deviation of Kinetic Energy: %lf\n", aveKE, stddevKE);
  fprintf(rite, "Average Potential Energy Particles: %lf           Standard Deviation of Potential Energy: %lf\n", avePE, stddevPE);
  
  
  fprintf(rite, "\nTemperature control\n");
  fprintf(rite, "-----------------------\n");
  double ParticleTemperature;
  double k_b = BOLTZMANN_CONSTANT;  /*Boltzmann constant*/
  //ParticleTemperature = (2.00 * aveKE) / ((3.00 * fr->natoms - 3) * k_b);  /*we subtract the center of mass motion. Hence the actual no. of degrees of freedom = 3N-3 in 3 dimension.*/
  aveKE = Average_kinetic_enenrgy; //This is the actual value for the entire run
  /*Special case for one particle system*/
  if(fr->natoms == 1)
    {
      ParticleTemperature = (2.00 * aveKE) / (dim * k_b);
    }
  else
    {
      ParticleTemperature = (2.00 * aveKE) / (dim * (fr->natoms - 1) * k_b);
    }
  fprintf(rite, "Actual value of average kinetic energy: %.15e\n", aveKE);
  fprintf(rite, "Observed temperature for particles: %lf K\n", ParticleTemperature);
  fprintf(rite, "Value of Boltzmann constant used: %.15e\n", k_b);
  

  fclose(rite);
  return 0;
}



void Write_masses(FR_DAT *fr)
{
  int i;
  FILE *rite1;

  rite1 = fopen("masses_out", "w");
  for(i = 0 ; i < fr->natoms ; i++)
    {
      fprintf(rite1, "%lf\n", fr->m[i]);
    }
  fclose(rite1);
}


void Write_friction_coefficients(FR_DAT *fr, struct langevin Langevin_info)
{
  int i;
  FILE *rite1;
  rite1 = fopen("friction_out", "w");
  fprintf(rite1, "Friction coefficients are in the unit of (internal time unit)^{-1}\n");
  for(i = 0 ; i < fr->natoms ; i++)
    {
      fprintf(rite1, "%lf\n", Langevin_info.gamma[i]);
    }
  fclose(rite1);
}

