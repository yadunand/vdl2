/* Codes for writing PDB files for C-alpha CG model of a protein.*/

/*Last modified on December 05, 2011*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "md_ld_common_d_dimension_namd.h"
#include "write_pdb_atom_record.h"


/* Write one configuration of the system to a PDB file. The array of residue
 * names is supplied as an argument.*/
void Write_one_CA_config_in_PDB_format(FILE *out, FR_DAT *fr, char **Residue_names)
{
  int i, residue_sequence_number;
  float x, y, z;

  for(i = 0 ; i < fr->natoms ; i++)
    {
      /*need to typecast positions values from double to float*/
      x = (float)fr->x[i][0];
      y = (float)fr->x[i][1];
      z = (float)fr->x[i][2];
      residue_sequence_number = i + 1;
      /*write to the file*/
      Write_one_PDB_ATOM_CA(out, Residue_names[i], residue_sequence_number, x, y, z);
    }

}

void Write_one_CA_config_in_PDB_format2(FILE *out, FR_DAT *fr, char **Residue_names)
{
  int i, residue_sequence_number;
  float x, y, z;
  int ca_num[]= {0, 2, 4, 6, 8, 10, 11, 13, 15, 17, 19, 21, 23, 25, 26, 28, 30, 32, 34, 36}; 

  for(i = 0 ; i < 20 ; i++)
    {
      /*need to typecast positions values from double to float*/
      x = (float)fr->x[ca_num[i]][0];
      y = (float)fr->x[ca_num[i]][1];
      z = (float)fr->x[ca_num[i]][2];
      residue_sequence_number = i + 1;
      /*write to the file*/
      Write_one_PDB_ATOM_CA(out, Residue_names[i], residue_sequence_number, x, y, z);
    }

}

void Write_one_CA_config_in_PDB_format3(FILE *out, FR_DAT *fr, char **Atom_names, char **Residue_names)
{
  int i, residue_sequence_number;
  float x, y, z;

  for(i = 0 ; i < fr->natoms ; i++)
    {
      /*need to typecast positions values from double to float*/
      x = (float)fr->x[i][0];
      y = (float)fr->x[i][1];
      z = (float)fr->x[i][2];
      residue_sequence_number = i + 1;
      /*write to the file*/
      Write_one_PDB_ATOM_CA2(out, Atom_names[i], Residue_names[i], residue_sequence_number, x, y, z);
    }

}

/* Read residue names from the file 'RESIDUE_NAMES'. Number of lines should be equal to the number of C-alpha atoms.
 * This code also allocates the necessary memory.*/
void Read_residue_names(int Num_CA_atoms, char ***Residue_names)
{
  int i;
  FILE *in;
  char line[1000000], name[4];
  int Num_lines;

  *Residue_names = (char**)malloc(Num_CA_atoms * sizeof(char*));
  for(i = 0 ; i < Num_CA_atoms ; i++)
    {
      (*Residue_names)[i] = (char*)malloc(4 * sizeof(char));
    }

  in = fopen("RESIDUE_NAMES", "r");
  if(in == NULL)
    {
      printf("File 'RESIDUE_NAMES' not found\n");
      exit(1);
    }
  Num_lines = 0;
  while(fgets(line, 1000000, in) != NULL)
    {
      if((Num_lines + 1) > Num_CA_atoms)
        {
          printf("Number of lines in file 'RESIDUE_NAMES' is more than the number of CA atoms (%d). Something is wrong! \n", Num_CA_atoms);                                                                                 
	}
      
      sscanf(line, "%s", name);
      strcpy((*Residue_names)[Num_lines], name);
      Num_lines = Num_lines + 1;
    }
  fclose(in);
  /*Check if number of lines is less than number of atoms*/
  if(Num_lines < Num_CA_atoms)
    {
      printf("Number of lines in file 'RESIDUE_NAMES' (%d) is less the number of atoms (%d).\n", Num_lines, Num_CA_atoms);
      exit(1);
    } 
}

void Read_residue_names2(int Num_CA_atoms, char *** Atom_names, char ***Residue_names)
{
  int i;
  FILE *in;
  char line[1000000], name[4], name2[3];
  int Num_lines;

  *Residue_names = (char**)malloc(Num_CA_atoms * sizeof(char*));
  *Atom_names = (char**)malloc(Num_CA_atoms * sizeof(char*));
  for(i = 0 ; i < Num_CA_atoms ; i++)
    {
      (*Residue_names)[i] = (char*)malloc(4 * sizeof(char));
      (*Atom_names)[i] = (char*)malloc(3 * sizeof(char));
    }

  in = fopen("RESIDUE_NAMES_2", "r");
  if(in == NULL)
    {
      printf("File 'RESIDUE_NAMES' not found\n");
      exit(1);
    }
  Num_lines = 0;
  while(fgets(line, 1000000, in) != NULL)
    {
      if((Num_lines + 1) > Num_CA_atoms)
        {
          printf("Number of lines in file 'RESIDUE_NAMES' is more than the number of CA atoms (%d). Something is wrong! \n", Num_CA_atoms);                                                                                 
	}
      
      sscanf(line, "%s %s", name2, name);
      strcpy((*Residue_names)[Num_lines], name);
      strcpy((*Atom_names)[Num_lines], name2);
      Num_lines = Num_lines + 1;
    }
  fclose(in);
  /*Check if number of lines is less than number of atoms*/
  if(Num_lines < Num_CA_atoms)
    {
      printf("Number of lines in file 'RESIDUE_NAMES' (%d) is less the number of atoms (%d).\n", Num_lines, Num_CA_atoms);
      exit(1);
    } 
}


/* Write a PDB file with one configuration of the system. This is a wrapper function*/
void Write_CA_PDB_file(FR_DAT *fr, char **Residue_names, char File_name_CA_PDB[1000])
{
  FILE *out;
  out = fopen(File_name_CA_PDB, "w");
  Write_one_CA_config_in_PDB_format(out, fr, Residue_names);
  fclose(out);
}

void Write_CA_PDB_file2(FR_DAT *fr, char **Residue_names, char File_name_CA_PDB[1000])
{
  FILE *out;
  out = fopen(File_name_CA_PDB, "w");
  Write_one_CA_config_in_PDB_format2(out, fr, Residue_names);
  fclose(out);
}

/* This function writes a configuration to a PDB trajectory file. 
 * 'config_index' gives the index of the configuration written. It starts from 1 and takes values upto total Number of 
 * trajectories in the file.
 *
 * The format of the file. Each config is written in the following way.
 * MODEL        config_index
 * Info about config in PDB format
 * ENDMDL
 *
 * */
void Write_config_to_a_PDB_trajectory_file(FILE *out, FR_DAT *fr, char **Residue_names, int config_index)
{
  fprintf(out, "MODEL        %d\n", config_index);
  Write_one_CA_config_in_PDB_format(out, fr, Residue_names);
  fprintf(out, "ENDMDL\n");
}



/* Same stuff as above only 'config_index' is long int*/
void Write_config_to_a_PDB_trajectory_file_1(FILE *out, FR_DAT *fr, char **Residue_names, long int config_index)
{
  fprintf(out, "MODEL        %lu\n", config_index);
  Write_one_CA_config_in_PDB_format(out, fr, Residue_names);
  fprintf(out, "ENDMDL\n");
}

void Write_config_to_a_PDB_trajectory_file_2(FILE *out, FR_DAT *fr, char **Atom_names, char **Residue_names, long int config_index)
{
  fprintf(out, "MODEL        %lu\n", config_index);
  Write_one_CA_config_in_PDB_format3(out, fr, Atom_names, Residue_names);
  fprintf(out, "ENDMDL\n");
}
