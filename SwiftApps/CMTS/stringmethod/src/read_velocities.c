/* Reads a text file with velocities of the particles.*/

/* last modified on August 30, 2011*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include "read_velocities.h"


/* Format of the input file:
---------------------------------------------------------------
Number of atoms
<blank line>
x_velocity_of_particle_1 y_velocity_of_particle_1 z_velocity_of_particle_1
x_velocity_of_particle_2 y_velocity_of_particle_2 z_velocity_of_particle_2
etc. total no. of lines should be equal to total number of atoms
*/
/* Assumes that memory for velocity array has already been allocated. Returns 1 if file 
 * read is successful otherwise returns 0.*/
int Read_velocities(FR_DAT *fr, char *input_filename)
{
  int i, d;
  int dim = DIMENSION;
  FILE *in;
  int Num_atoms;

  in = fopen(input_filename, "r");
  if(in == NULL)
    {
      return 0;
    }
  else
    {
      fscanf(in, "%d ", &Num_atoms);

      if(Num_atoms != fr->natoms)
	{
	  printf("Number of atoms specified in the velocity file is not equal to that in the configuration file\n");
	  exit(1);
	}
      
      
      /* read particle velocities*/
      for(i = 0 ; i < fr->natoms ; i++)
	{
	  //fscanf(in, "%lf %lf %lf ", &fr->v[i][0], &fr->v[i][1], &fr->v[i][2]);
	  for(d = 0 ; d < dim ; d++)
	    {
	      fscanf(in, "%lf ", &fr->v[i][d]);
	    }
	}
      
      fclose(in);
      return 1;
    }
}


