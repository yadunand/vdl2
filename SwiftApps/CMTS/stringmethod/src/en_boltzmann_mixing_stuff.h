/*All routines needed for two state or multistate elastic network based Hamiltonian.*/

#ifndef _en_boltzmann_mixing_stuff_h 
#define _en_boltzmann_mixing_stuff_h

#include "md_ld_common_d_dimension_namd.h"

void Read_en_info(int *Num_atoms, int *Num_input_structures, double **aray_of_cutoff_distance, double **array_of_mixing_parameters, double *repulsive_sigma, double *repulsive_epsilon, double *fixed_value_of_force_constant, double *energy_parameter_for_force_constant, double *energy_difference_from_first_structure);
void Read_en_info2(int *Num_atoms, int *Num_input_structures, double **aray_of_cutoff_distance, double **array_of_mixing_parameters, double *repulsive_sigma, double *repulsive_epsilon, double *fixed_value_of_force_constant, double *energy_parameter_for_force_constant, double *energy_difference_from_first_structure);

void Print_en_input(int Num_atoms, int Num_input_structures, double *aray_of_cutoff_distance, double *array_of_mixing_parameters, double repulsive_sigma, double repulsive_epsilon, double fixed_value_of_force_constant, double energy_parameter_for_force_constant, double energy_difference_from_first_structure);
void Print_en_input2(int Num_atoms, int Num_input_structures, double *aray_of_cutoff_distance, double *array_of_mixing_parameters, double repulsive_sigma, double repulsive_epsilon, double fixed_value_of_force_constant, double energy_parameter_for_force_constant, double energy_difference_from_first_structure);

void Alloc_en(int Num_atoms, int Num_input_structures, double ****array_of_pair_distance_matrices, double ****array_of_cutoff_matrices, double ***force_constants_all_pairs, double ***sigmas_for_repulsive_part, double ***epsilons_for_repulsive_part, FR_DAT **coordinates_all_structures);

int Read_one_structure(FR_DAT *one_coordinate_set, char filename[1000]);

void Write_one_structure(FR_DAT one_coordinate_set, char filename[1000]);

void Read_input_structures(int Num_input_structures, FR_DAT **coordinates_all_structures);
void Read_input_structures2(int Num_input_structures, FR_DAT **coordinates_all_structures);

void Calc_simple_distance(int dimension, double *point_1, double *point_2, double *distance, double *distance_squared, double *distance_vector);

void Calc_pair_distances_all_structures(int Num_atoms, int Num_input_structures, FR_DAT *coordinates_all_structures, double ***array_of_pair_distance_matrices);

void Print_pair_distances_all_structures(int Num_atoms, int Num_input_structures, double ***array_of_pair_distance_matrices);
void Print_pair_distances_all_structures2(int Num_atoms, int Num_input_structures, double ***array_of_pair_distance_matrices);

void Set_various_quantities(int Num_atoms, int Num_input_structures, double ***array_of_cutoff_matrices, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *aray_of_cutoff_distances, double repulsive_sigma, double repulsive_epsilon);

double Find_minimum(double number_1, double number_2);

void Set_site_dependent_force_constant_2_state(int Num_input_structures, int Num_atoms, double ***array_of_pair_distance_matrices, double energy_parameter, double fixed_value_of_force_constant, double **force_constants_all_pairs);

void Print_force_constants(int Num_atoms, double **force_constants_all_pairs);
void Print_force_constants2(int Num_atoms, double **force_constants_all_pairs);

void Cal_repulsive_force_pot_one_pair(double distance, int index_of_site_1, int index_of_site_2, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, int index_of_the_repulsive_function, double *scalar_force, double *potential);

void get_force_one_pair_one_structure(double distance, int index_of_site_1, int index_of_site_2, int index_of_input_structure, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double *scalar_force, double *potential);

void getforces(FR_DAT *fr, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure);

void getforces_cons(FR_DAT *fr, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, collective_variables *cons_distances, int Num_cv, double KK);

void getforces_cons_harm(FR_DAT *fr, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, collective_variables *cons_distances, int Num_cv, FR_DAT fix_position, double KK, double Kharm);

void getforces_harm(FR_DAT *fr, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, FR_DAT fix_position, double Kharm);

void getforces_cons_harm_omp(FR_DAT *fr, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, collective_variables *cons_distances, int Num_cv, FR_DAT fix_position, double KK, double Kharm, int Num_atoms);

void getforces_harm_omp(FR_DAT *fr, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, FR_DAT fix_position, double Kharm, int Num_atoms);
#endif
