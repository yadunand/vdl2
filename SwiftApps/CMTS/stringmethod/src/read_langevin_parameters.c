/*
  Reads friction coefficients for Langevin dynamics.
  Same friction coefficient for all componenets of a single particle.
  But different particle can have different friction coefficients.
*/

/* Last modified on November 01, 2011*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>


#include "read_langevin_parameters.h"


/*reads friction coefficients for Langevin dynamics simulation.
 * It also populates the array called 'sigma_langevin'. 
 * (See 'md_ld_common_d_dimension.h').
  The filename is : LANGEVIN_FRICTION
  Format:
  NUM_PARTICLES (number of particles %d)
  Value of friction coefficients. One value per line (in the unit of fs^{-1})
*/
void ReadFrictionCoefficients(struct langevin *Langevin_info, double temperature, double Boltzmann_constant, FR_DAT fr)
{
  char string[1000];
  FILE* indata1;
  int Num_particles_for_check;
  int i;

  indata1 = fopen("LANGEVIN_FRICTION", "r");
  if (indata1 == NULL)
    {
      printf ("Input file named 'LANGEVIN_FRICTION' not found. Exiting program\n");
      exit(1);
    }

  fscanf(indata1, "%s %d", string, &Num_particles_for_check);
  if(Num_particles_for_check != fr.natoms)
    {
      printf("Number of particles read from 'LANGEVIN_FRICTION' is different from other inputs.\n");
      exit(1);
    }

  /*allocate memory*/
  Langevin_info->gamma = (double*)malloc(fr.natoms * sizeof(double));
  Langevin_info->sigma_langevin = (double*)malloc(fr.natoms * sizeof(double));

  for(i = 0 ; i < fr.natoms ; i++)
    {
      fscanf(indata1, "%lf ", &Langevin_info->gamma[i]);
      /*convert from fs^{-1} to inverse of internal time unit*/
      Langevin_info->gamma[i] = Langevin_info->gamma[i] * INVERSE_FS_TO_INVERSE_INTERNAL_TIME;
      Langevin_info->sigma_langevin[i] = sqrt((2.0 * Boltzmann_constant * temperature * Langevin_info->gamma[i]) / fr.m[i]);
    } 
  
  fclose(indata1);
}


void ReadFrictionCoefficients2(struct langevin *Langevin_info, double temperature, double Boltzmann_constant, FR_DAT fr)
{
  char string[1000];
  FILE* indata1;
  int Num_particles_for_check;
  int i;

  indata1 = fopen("LANGEVIN_FRICTION_2", "r");
  if (indata1 == NULL)
    {
      printf ("Input file named 'LANGEVIN_FRICTION' not found. Exiting program\n");
      exit(1);
    }

  fscanf(indata1, "%s %d", string, &Num_particles_for_check);
  if(Num_particles_for_check != fr.natoms)
    {
      printf("Number of particles read from 'LANGEVIN_FRICTION' is different from other inputs.\n");
      exit(1);
    }

  /*allocate memory*/
  Langevin_info->gamma = (double*)malloc(fr.natoms * sizeof(double));
  Langevin_info->sigma_langevin = (double*)malloc(fr.natoms * sizeof(double));

  for(i = 0 ; i < fr.natoms ; i++)
    {
      fscanf(indata1, "%lf ", &Langevin_info->gamma[i]);
      /*convert from fs^{-1} to inverse of internal time unit*/
      Langevin_info->gamma[i] = Langevin_info->gamma[i] * INVERSE_FS_TO_INVERSE_INTERNAL_TIME;
      Langevin_info->sigma_langevin[i] = sqrt((2.0 * Boltzmann_constant * temperature * Langevin_info->gamma[i]) / fr.m[i]);
    } 
  
  fclose(indata1);
}

