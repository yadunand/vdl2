/*Input/Output and other routines related to two state or multistate elastic network based Hamiltonian*/

/*Last modified on December 19, 2011*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

#include "en_boltzmann_mixing_stuff.h"

/*
 * Input file name: EN_BOLTZMANN_MIXING_INFO
 * Format:
 * NUM_C_ALPHA_ATOMS (%d number of C-alpha atoms)
 * NUM_INPUT_STRUCTURES (%d number of input structures i.e. number of states)
 * CUTOFF_DISTANCES (cutoff distances for each structure, separated by white space. Number of cutoffs should be equal
 *                   to number of strcutures)
 * MIXING_COEFFICIENTS (Mixing coeficients for mixing potentials for various structures. Number of coefficients 
 *                      should be one less than the number of structures. Values should be separated by white space) 
 * SIGMA_REPULSIVE (%lf sigma for the repulsive part of the potential. Same sigma is used for all pairs)
 * EPSILON_REPULSIVE (%lf epsilon for the repulsive part of the potential)
 * FIXED_VALUE_OF_FORCE_CONSANT (%lf. Fixed value of force constnat. This value is used unless a different 
 *                               site dependent value is required to minimize strain. See Albert pan's
 *                               string apper JPCB 112, 3432 (2008).)
 * ENERGY_PARAM_FOR_FORCE_CONSTANT (%lf Energy parameter for site dependent force constant. See the above paper.)
 * ENERGY_DIFFERENCE_FROM_FIRST_STRUCTURE (%lf Energy difference between the two input structures)
 * */
void Read_en_info(int *Num_atoms, int *Num_input_structures, double **aray_of_cutoff_distance, double **array_of_mixing_parameters, double *repulsive_sigma, double *repulsive_epsilon, double *fixed_value_of_force_constant, double *energy_parameter_for_force_constant, double *energy_difference_from_first_structure)
{
  FILE *in;
  char key[1000];
  int i;
  in = fopen("EN_BOLTZMANN_MIXING_INFO", "r");
  if(in == NULL)
    {
      printf("File 'EN_BOLTZMANN_MIXING_INFO' not found\n");
      exit(1);
    }
  
  fscanf(in, "%s %d ", key, Num_atoms);
  fscanf(in, "%s %d ", key, Num_input_structures);
  
  *aray_of_cutoff_distance = (double*)malloc((*Num_input_structures) * sizeof(double));
  *array_of_mixing_parameters = (double*)malloc(((*Num_input_structures) - 1) * sizeof(double));

  fscanf(in, "%s ", key);
  for(i = 0 ; i < *Num_input_structures ; i++)
    {
      fscanf(in, " %lf ", &(*aray_of_cutoff_distance)[i]);
    }
  fscanf(in, "%s ", key);
  for(i = 0 ; i < ((*Num_input_structures) - 1) ; i++)
    {
      fscanf(in, " %lf ", &(*array_of_mixing_parameters)[i]);
    }

  fscanf(in, "%s %lf ", key, repulsive_sigma);
  fscanf(in, "%s %lf ", key, repulsive_epsilon);
  fscanf(in, "%s %lf ", key, fixed_value_of_force_constant);
  fscanf(in, "%s %lf ", key, energy_parameter_for_force_constant);
  fscanf(in, "%s %lf ", key, energy_difference_from_first_structure);
  

  fclose(in);

}

void Read_en_info2(int *Num_atoms, int *Num_input_structures, double **aray_of_cutoff_distance, double **array_of_mixing_parameters, double *repulsive_sigma, double *repulsive_epsilon, double *fixed_value_of_force_constant, double *energy_parameter_for_force_constant, double *energy_difference_from_first_structure)
{
  FILE *in;
  char key[1000];
  int i;
  in = fopen("EN_BOLTZMANN_MIXING_INFO_2", "r");
  if(in == NULL)
    {
      printf("File 'EN_BOLTZMANN_MIXING_INFO_2' not found\n");
      exit(1);
    }
  
  fscanf(in, "%s %d ", key, Num_atoms);
  fscanf(in, "%s %d ", key, Num_input_structures);
  
  *aray_of_cutoff_distance = (double*)malloc((*Num_input_structures) * sizeof(double));
  *array_of_mixing_parameters = (double*)malloc(((*Num_input_structures) - 1) * sizeof(double));

  fscanf(in, "%s ", key);
  for(i = 0 ; i < *Num_input_structures ; i++)
    {
      fscanf(in, " %lf ", &(*aray_of_cutoff_distance)[i]);
    }
  fscanf(in, "%s ", key);
  for(i = 0 ; i < ((*Num_input_structures) - 1) ; i++)
    {
      fscanf(in, " %lf ", &(*array_of_mixing_parameters)[i]);
    }

  fscanf(in, "%s %lf ", key, repulsive_sigma);
  fscanf(in, "%s %lf ", key, repulsive_epsilon);
  fscanf(in, "%s %lf ", key, fixed_value_of_force_constant);
  fscanf(in, "%s %lf ", key, energy_parameter_for_force_constant);
  fscanf(in, "%s %lf ", key, energy_difference_from_first_structure);
  

  fclose(in);

}


/*for testing*/
void Print_en_input(int Num_atoms, int Num_input_structures, double *aray_of_cutoff_distance, double *array_of_mixing_parameters, double repulsive_sigma, double repulsive_epsilon, double fixed_value_of_force_constant, double energy_parameter_for_force_constant, double energy_difference_from_first_structure)
{
  FILE *out;
  int i;

  out = fopen("out_en_boltzmann_mixing_info", "w");
  fprintf(out, "%d\n", Num_atoms);
  fprintf(out, "%d\n", Num_input_structures);
  for(i = 0 ; i < Num_input_structures ; i++)
    {
      fprintf(out, "%lf ", aray_of_cutoff_distance[i]);
    }
  fprintf(out, "\n");
  for(i = 0 ; i < (Num_input_structures - 1) ; i++)
    {
      fprintf(out, "%lf ", array_of_mixing_parameters[i]);
    }
  fprintf(out, "\n");
  fprintf(out, "%lf\n", repulsive_sigma);
  fprintf(out, "%lf\n", repulsive_epsilon);
  fprintf(out, "%lf\n", fixed_value_of_force_constant);
  fprintf(out, "%lf\n", energy_parameter_for_force_constant);
  fprintf(out, "%lf\n", energy_difference_from_first_structure);
  fclose(out);
  
}

void Print_en_input2(int Num_atoms, int Num_input_structures, double *aray_of_cutoff_distance, double *array_of_mixing_parameters, double repulsive_sigma, double repulsive_epsilon, double fixed_value_of_force_constant, double energy_parameter_for_force_constant, double energy_difference_from_first_structure)
{
  FILE *out;
  int i;

  out = fopen("out_en_boltzmann_mixing_info_2", "w");
  fprintf(out, "%d\n", Num_atoms);
  fprintf(out, "%d\n", Num_input_structures);
  for(i = 0 ; i < Num_input_structures ; i++)
    {
      fprintf(out, "%lf ", aray_of_cutoff_distance[i]);
    }
  fprintf(out, "\n");
  for(i = 0 ; i < (Num_input_structures - 1) ; i++)
    {
      fprintf(out, "%lf ", array_of_mixing_parameters[i]);
    }
  fprintf(out, "\n");
  fprintf(out, "%lf\n", repulsive_sigma);
  fprintf(out, "%lf\n", repulsive_epsilon);
  fprintf(out, "%lf\n", fixed_value_of_force_constant);
  fprintf(out, "%lf\n", energy_parameter_for_force_constant);
  fprintf(out, "%lf\n", energy_difference_from_first_structure);
  fclose(out);
  
}


/* Allocate memory for all the data structures needed for elastic network Hamiltonian*/
void Alloc_en(int Num_atoms, int Num_input_structures, double ****array_of_pair_distance_matrices, double ****array_of_cutoff_matrices, double ***force_constants_all_pairs, double ***sigmas_for_repulsive_part, double ***epsilons_for_repulsive_part, FR_DAT **coordinates_all_structures)
{
  int a, i;
  
  *array_of_pair_distance_matrices = (double ***)malloc(Num_input_structures * sizeof(double**));
  *array_of_cutoff_matrices = (double ***)malloc(Num_input_structures * sizeof(double**)); 
  *coordinates_all_structures = (FR_DAT *)malloc(Num_input_structures * sizeof(FR_DAT));
  
  
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      (*array_of_pair_distance_matrices)[a] = (double **)malloc(Num_atoms * sizeof(double*));
      (*array_of_cutoff_matrices)[a] = (double **)malloc(Num_atoms * sizeof(double*));
      (*coordinates_all_structures)[a].x = (rvec*)malloc(Num_atoms * sizeof(rvec));
      (*coordinates_all_structures)[a].f = (rvec*)malloc(Num_atoms * sizeof(rvec));

      (*coordinates_all_structures)[a].natoms = Num_atoms;
      
      for(i = 0 ; i < Num_atoms ; i++)
	{
	  (*array_of_pair_distance_matrices)[a][i] = (double *)malloc(Num_atoms * sizeof(double));
	  (*array_of_cutoff_matrices)[a][i] = (double *)malloc(Num_atoms * sizeof(double));
	} 
    }
  
  *sigmas_for_repulsive_part = (double **)malloc(Num_atoms * sizeof(double*));
  *epsilons_for_repulsive_part = (double **)malloc(Num_atoms * sizeof(double*));
  *force_constants_all_pairs = (double **)malloc(Num_atoms * sizeof(double*));
  for(i = 0 ; i < Num_atoms ; i++)
    {
      (*sigmas_for_repulsive_part)[i] = (double *)malloc(Num_atoms * sizeof(double));
      (*epsilons_for_repulsive_part)[i] = (double *)malloc(Num_atoms * sizeof(double));
      (*force_constants_all_pairs)[i] = (double *)malloc(Num_atoms * sizeof(double));
    }
  
}


/* Read one structure
 * Format:
 * Each line should have x, y, and z cooridnates of one particle (C_ALPHA) separated by white space.
 * Number of lines should be equal to the number of particles. 
 * Returns the number of lines read. If file is in proper format this should be equal to the number of atoms.
 * If more lines are present reads first n lines where n is the number of atoms. If less than required number of atoms 
 * found gives error and quits..*/
int Read_one_structure(FR_DAT *one_coordinate_set, char filename[1000])
{
  char line[1000000];
  int Num_lines;
  double x, y, z;
  FILE *in;
  in = fopen(filename, "r");
  if(in == NULL)
    {
      printf("File '%s' not found\n", filename);
      exit(1);
    }
  Num_lines = 0;
  while(fgets(line, 1000000, in) != NULL)
    {
      if((Num_lines + 1) > one_coordinate_set->natoms)
        {
          printf("Number of atoms in file '%s' is more than the expected number of %d. Something is wrong.\n", filename, one_coordinate_set->natoms);
          exit(1);
        }
      sscanf(line, "%lf  %lf  %lf", &x, &y, &z);
      one_coordinate_set->x[Num_lines][0] = x;
      one_coordinate_set->x[Num_lines][1] = y;
      one_coordinate_set->x[Num_lines][2] = z;
      Num_lines = Num_lines + 1;
    }
  fclose(in);
  /*Check if number of lines is less than number of atoms*/
  if(Num_lines < one_coordinate_set->natoms)
    {
      printf("Number of lines in file '%s' (%d) is less than the expected number of atoms (%d).\n", filename, Num_lines, one_coordinate_set->natoms);
      exit(1);
    }
  else
    {
      return Num_lines;
    }
}


void Write_one_structure(FR_DAT one_coordinate_set, char filename[1000])
{
  FILE *out;
  int i;

  out = fopen(filename, "w");
  for(i = 0 ; i < one_coordinate_set.natoms; i++)
    {
      fprintf(out, "%lf   %lf   %lf\n", one_coordinate_set.x[i][0], one_coordinate_set.x[i][1], one_coordinate_set.x[i][2]);
    }
  fclose(out);
}


/* Read all input structures.
 * Input structures should be named as 'input_structures_1', 'input_structures_2',...*/
void Read_input_structures(int Num_input_structures, FR_DAT **coordinates_all_structures)
{
  int i;
  char filename[1000];
  int num_atoms;
  for(i = 0 ; i < Num_input_structures ; i++)
    {
      sprintf(filename, "INPUT_STRUCTURE_%d", (i+1));
      num_atoms = Read_one_structure(&(*coordinates_all_structures)[i], filename);
    }
}

void Read_input_structures2(int Num_input_structures, FR_DAT **coordinates_all_structures)
{
  int i;
  char filename[1000];
  int num_atoms;
  for(i = 0 ; i < Num_input_structures ; i++)
    {
      sprintf(filename, "INPUT_STRUCTURE_2_%d", (i+1));
      num_atoms = Read_one_structure(&(*coordinates_all_structures)[i], filename);
    }
}


/* Calculaes simple distance, square of distance and the distance vector 
 * between two points in d-dimension. *point_1 and *point_2 are 
 * d-diemnsional arrays (or d-dimensional vectors). This is meant for non-periodic systems
 * The distance_vector = point_2 - point_1*/
void Calc_simple_distance(int dimension, double *point_1, double *point_2, double *distance, double *distance_squared, double *distance_vector)
{
  int i;
  for(i=0;i<dimension;i++)
    {
      distance_vector[i] = point_2[i] - point_1[i];
    }
  *distance_squared = 0.0;
  for(i=0;i<dimension;i++)
    {
      *distance_squared = *distance_squared + (distance_vector[i] * distance_vector[i]);
    }
  *distance = sqrt(*distance_squared);
}



/* Calculate the pair distances in all the structures or states.*/
void Calc_pair_distances_all_structures(int Num_atoms, int Num_input_structures, FR_DAT *coordinates_all_structures, double ***array_of_pair_distance_matrices)
{
  int a, i, j;
  int dimension = DIMENSION;
  double distance;
  double distance_squared; 
  double distance_vector[DIMENSION];
  /*set diagonal elemnts to -1*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      for(i = 0 ; i < Num_atoms ; i++)
        {
          array_of_pair_distance_matrices[a][i][i] = -1.0;
        }
    } 
  /* calcualte pair distances*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      for(i = 0 ; i < (Num_atoms - 1) ; i++)
        {
          for(j = (i + 1) ; j < Num_atoms ; j++)
            {
              /*calculate distance*/
              Calc_simple_distance(dimension, coordinates_all_structures[a].x[i], coordinates_all_structures[a].x[j], &distance, &distance_squared, distance_vector);
              array_of_pair_distance_matrices[a][i][j] = distance;
              array_of_pair_distance_matrices[a][j][i] = distance;
            }
        }
    }

}


/* Print distances*/
void Print_pair_distances_all_structures(int Num_atoms, int Num_input_structures, double ***array_of_pair_distance_matrices)
{
  FILE *out;
  int a, i, j;
  char filename[1000];
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      sprintf(filename, "pair_distances_%d", (a+1));
      out = fopen(filename, "w");
      for(i = 0 ; i < Num_atoms ; i++)
        {
          for(j = 0 ; j < Num_atoms ; j++)
            {
              fprintf(out, "%lf ", array_of_pair_distance_matrices[a][i][j]);
            }
          fprintf(out, "\n");
        }
      fclose(out);
    }

}

void Print_pair_distances_all_structures2(int Num_atoms, int Num_input_structures, double ***array_of_pair_distance_matrices)
{
  FILE *out;
  int a, i, j;
  char filename[1000];
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      sprintf(filename, "pair_distances_2_%d", (a+1));
      out = fopen(filename, "w");
      for(i = 0 ; i < Num_atoms ; i++)
        {
          for(j = 0 ; j < Num_atoms ; j++)
            {
              fprintf(out, "%lf ", array_of_pair_distance_matrices[a][i][j]);
            }
          fprintf(out, "\n");
        }
      fclose(out);
    }

}


/* Set various quantities like cutoffs, sigma, epsilons etc.*/
void Set_various_quantities(int Num_atoms, int Num_input_structures, double ***array_of_cutoff_matrices, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *aray_of_cutoff_distances, double repulsive_sigma, double repulsive_epsilon)
{
  int a, i, j;

  for(a = 0 ; a < Num_input_structures ; a++)
    {
      for(i = 0 ; i < (Num_atoms - 1) ; i++)
        {
          array_of_cutoff_matrices[a][i][i] = -1.0;
        }
    }

  for(a = 0 ; a < Num_input_structures ; a++)
    {
      for(i = 0 ; i < (Num_atoms - 1) ; i++)
        {
          for(j = (i + 1) ; j < Num_atoms ; j++)
            {
              array_of_cutoff_matrices[a][i][j] = aray_of_cutoff_distances[a];
              array_of_cutoff_matrices[a][j][i] = aray_of_cutoff_distances[a]; 
            }
        }
    }


  for(i = 0 ; i < Num_atoms ; i++)
    {
      sigmas_for_repulsive_part[i][i] = -1.0;
      epsilons_for_repulsive_part[i][i] = -1.0;
    }
  for(i = 0 ; i < (Num_atoms - 1) ; i++)
    {
      for(j = (i + 1) ; j < Num_atoms ; j++)
        {
          sigmas_for_repulsive_part[i][j] = repulsive_sigma;
          sigmas_for_repulsive_part[j][i] = repulsive_sigma;

          epsilons_for_repulsive_part[i][j] = repulsive_epsilon;
          epsilons_for_repulsive_part[j][i] = repulsive_epsilon;
        }
    }


}


/* Find minimum of two numbers.*/
double Find_minimum(double number_1, double number_2)
{
  double min_value;
  if(number_1 <= number_2)
    {
      min_value = number_1;
    }
  else
    {
      min_value = number_2;
    }

  return min_value;
}

/* Set force constants. Site dependent force constants. This is taken from Pan et al. J. Phys. Chem. B. 112, 3432 
 * (2008) (the swarms paper). They took it from some other paper.
 *
 * This is for tw ostate model. Generalizations probably exist.
 * k_{ij} = \min{k^{\hat}_{ij}, 0.2 kcal mol^{-1} \AA^{-2}} where
 * k^{\hat}_{ij} = \epsilon_k / (r_{ij}^A - r_{ij}^B)^2 . r_{ij}^A and r_{ij}^B are the distances between sites i
 * and j in structres A and B respectively.
 * \epsilon_k = 0.5 kcal/mol in Pan's paper.
 * 'energy_parameter' is \epsilon_k
 * 'fixed_value_of_force_constant' = 0.2 kcal mol^{-1} \AA^{-2}} in Pan's paper.
 * */
void Set_site_dependent_force_constant_2_state(int Num_input_structures, int Num_atoms, double ***array_of_pair_distance_matrices, double energy_parameter, double fixed_value_of_force_constant, double **force_constants_all_pairs)
{
  int i, j; 
  double force_constant_from_data, final_force_constant;
  double distance_structure_1, distance_structure_2;
  double temp, temp1;
  
  for(i = 0 ; i < Num_atoms ; i++)
    {
      force_constants_all_pairs[i][i] = -1.0;
    }
  for(i = 0 ; i < (Num_atoms - 1) ; i++)
    {
      for(j = (i + 1) ; j < Num_atoms ; j++)
        {
          distance_structure_1 = array_of_pair_distance_matrices[0][i][j];
          distance_structure_2 = array_of_pair_distance_matrices[1][i][j];

          temp = distance_structure_1 - distance_structure_2;
          temp1 = pow(temp, 2);
          force_constant_from_data = energy_parameter / temp1;
          final_force_constant = Find_minimum(force_constant_from_data, fixed_value_of_force_constant);

          force_constants_all_pairs[i][j] = final_force_constant;
          force_constants_all_pairs[j][i] = final_force_constant;
        }
    }
}


void Print_force_constants(int Num_atoms, double **force_constants_all_pairs)
{
  int i, j;
  FILE *out;
  out = fopen("force_constants_out", "w");
  for(i = 0 ; i < Num_atoms ; i++)
    {
      for(j = 0 ; j < Num_atoms ; j++)
        {
          fprintf(out, "%lf ", force_constants_all_pairs[i][j]);
        }
      fprintf(out, "\n");
    }
  fclose(out);

}

void Print_force_constants2(int Num_atoms, double **force_constants_all_pairs)
{
  int i, j;
  FILE *out;
  out = fopen("force_constants_out_2", "w");
  for(i = 0 ; i < Num_atoms ; i++)
    {
      for(j = 0 ; j < Num_atoms ; j++)
        {
          fprintf(out, "%lf ", force_constants_all_pairs[i][j]);
        }
      fprintf(out, "\n");
    }
  fclose(out);

}


/** Codes for force and energy calcualtion**/

/* Calculates scalar force and energy for one pair of sites. 
 * The functional form:
 * U_R(r_{ij}) = \epsilon (\sigma / r_{ij})^n
 * n = 12*/
void Cal_repulsive_force_pot_one_pair(double distance, int index_of_site_1, int index_of_site_2, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, int index_of_the_repulsive_function, double *scalar_force, double *potential)
{
  int i, j, n;
  double sigma, epsilon, sigma_over_distance;
  i = index_of_site_1;
  j = index_of_site_2;
  n = index_of_the_repulsive_function;
  sigma = sigmas_for_repulsive_part[i][j];
  epsilon = epsilons_for_repulsive_part[i][j];
  sigma_over_distance = sigma / distance;

  *potential = epsilon * pow(sigma_over_distance, n);
  *scalar_force = (*potential) * ( ((double)n) / distance );

}

/* Calcualte the force and potential for one pair for one structure. This function is only called
 * when two sites are within interacting distance in the native structure.I.e. if the distance between
 * two sites in the structure is less than the cutoff for that struture.*/
void get_force_one_pair_one_structure(double distance, int index_of_site_1, int index_of_site_2, int index_of_input_structure, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double *scalar_force, double *potential)
{
  double k, diff, distance_in_static_structure;

  k = force_constants_all_pairs[index_of_site_1][index_of_site_2];
  distance_in_static_structure = array_of_pair_distance_matrices[index_of_input_structure][index_of_site_1][index_of_site_2];

  diff = distance - distance_in_static_structure;

  *potential = 0.5 * k * pow(diff, 2);
  *scalar_force = -k * diff;
}




/* Calculate the total forces on all the particles as well as the total potential energy*/
void getforces(FR_DAT *fr, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure)
{
  int a, i, j, d;
  double distance, distance_squared, scalar_force_repulsive, potential_repulsive;
  double scalar_force_one_structure, potential_one_structure;
  double distance_vector[3];
  double unit_vector[3];
  double temp_force[3];  /*this is used for various stages of force calculation*/
  double beta_m;        /*Mixing parameter*/
  double *Boltzmann_factor_like_terms;     /* one for each structure*/
  double temp;          /*for miscellenious purposes*/
  double sum_of_Boltzmann_factors;
  double *factor_for_force_for_each_structure;

  beta_m = array_of_mixing_parameters[0];       /*two input structures. Only one mixing parameter*/

  Boltzmann_factor_like_terms = (double*)malloc(Num_input_structures * sizeof(double));
  factor_for_force_for_each_structure = (double*)malloc(Num_input_structures * sizeof(double));
  
  /* zero froces for the current configuration (or frame) */
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < 3 ; d++)
        {
          fr->f[i][d] = 0.0;
        }
    }  
  
  /* zero forces for data structure that stores forces from each individual structure*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      for(i = 0 ; i < fr->natoms ; i++)
	{
	  for(d = 0 ; d < 3 ; d++)
	    {
	      (*coordinates_all_structures)[a].f[i][d] = 0.0;
	    }
	}
      (*coordinates_all_structures)[a].U = 0.0;
    }
  
  /* zero total energy*/
  fr->U = 0.0;
  fr->U_r = 0.0;
  fr->U_en = 0.0;

  /* double loop over all possible pairs */
  for(i = 0 ; i < (fr->natoms - 1) ; i++)
    {
      for(j = (i+1) ; j < fr->natoms ; j++)
        {
          /* calculate distance and unit vector*/
          Calc_simple_distance(3, fr->x[i], fr->x[j], &distance, &distance_squared, distance_vector);
          for(d = 0 ; d < 3 ; d++)
            {
              unit_vector[d] = distance_vector[d] / distance;
            }
          /* Calculate force and potential from the repulsive part and sum 
             these forces and potential for both the sites into the main data structure*/
          Cal_repulsive_force_pot_one_pair(distance, i, j, sigmas_for_repulsive_part, epsilons_for_repulsive_part, 12, &scalar_force_repulsive, &potential_repulsive);
          fr->U = fr->U + potential_repulsive;
          fr->U_r = fr->U_r + potential_repulsive;
          for(d = 0 ; d < 3 ; d++)
            {
              temp_force[d] = scalar_force_repulsive * unit_vector[d];
              fr->f[i][d] = fr->f[i][d] + (-temp_force[d]);
              fr->f[j][d] = fr->f[j][d] + temp_force[d];
            }

          /* loop over all the structure and calculate and sum force and potential for each 
             structure in separate data structures.*/
          for(a = 0 ; a < Num_input_structures ; a++)
            {
              /*first check for cutoff. I.e. calculate interaction only if the pair distance
                between two sites in question in the static structure is less than the cutoff 
		for that structure.*/
	      if(array_of_pair_distance_matrices[a][i][j] < aray_of_cutoff_distances[a])
		{
		  get_force_one_pair_one_structure(distance, i, j, a, array_of_pair_distance_matrices, force_constants_all_pairs, &scalar_force_one_structure, &potential_one_structure);
		  (*coordinates_all_structures)[a].U = (*coordinates_all_structures)[a].U + potential_one_structure;
		  for(d = 0 ; d < 3 ; d++)
		    {
		      temp_force[d] = scalar_force_one_structure * unit_vector[d];
		      (*coordinates_all_structures)[a].f[i][d] = (*coordinates_all_structures)[a].f[i][d] +
			(-temp_force[d]);
		      (*coordinates_all_structures)[a].f[j][d] = (*coordinates_all_structures)[a].f[j][d] +
			temp_force[d];
		    }
		}
            }     /*end of loop over a*/
        }      /*end of j loop*/
    }       /*end of i loop*/  
  
  /* Now we have calcualted the potentials for both the structures, use them to calculate
     constant terms. Constant terms are like Boltzmann factors where each term involves 
     the potential energy of that structure.*/
  /* Calculate Boltzmann term*/
  sum_of_Boltzmann_factors = 0.0;
  /* First state*/
  temp = -beta_m * ((*coordinates_all_structures)[0].U);
  Boltzmann_factor_like_terms[0] = exp(temp);
  /* Second state*/
  temp = -beta_m * ( ((*coordinates_all_structures)[1].U) + energy_difference_from_first_structure );
  Boltzmann_factor_like_terms[1] = exp(temp);
  /* Sum of Boltzmann terms*/
  sum_of_Boltzmann_factors = Boltzmann_factor_like_terms[0] + Boltzmann_factor_like_terms[1];
  /* Calculate the constant factor for each structure that forces need to be multiplied by.*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      factor_for_force_for_each_structure[a] = Boltzmann_factor_like_terms[a] / sum_of_Boltzmann_factors;
    } 

  /* Update the total forces and total forces on all the particles. It will involve a sum over
     all the structures.*/
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < 3 ; d++)
        {
          for(a = 0 ; a < Num_input_structures ; a++)
            {
              temp = factor_for_force_for_each_structure[a];
              fr->f[i][d] = fr->f[i][d] + (temp * (*coordinates_all_structures)[a].f[i][d]);
            }
        }
    }

  fr->U_en = (-(1.0 / beta_m) * log(sum_of_Boltzmann_factors));
  /* Calculate the total energy*/
  fr->U = fr->U + (-(1.0 / beta_m) * log(sum_of_Boltzmann_factors));

  free(Boltzmann_factor_like_terms);
  free(factor_for_force_for_each_structure);
}



void getforces_cons(FR_DAT *fr, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, collective_variables *cons_distances, int Num_cv, double KK)
{
  int a, b, i, j, d;
  double distance, distance_squared, scalar_force_repulsive, potential_repulsive;
  double scalar_force_one_structure, potential_one_structure;
  double distance_vector[3];
  double unit_vector[3];
  double temp_force[3];  /*this is used for various stages of force calculation*/
  double beta_m;        /*Mixing parameter*/
  double *Boltzmann_factor_like_terms;     /* one for each structure*/
  double temp;          /*for miscellenious purposes*/
  double sum_of_Boltzmann_factors;
  double *factor_for_force_for_each_structure;

  beta_m = array_of_mixing_parameters[0];       /*two input structures. Only one mixing parameter*/

  Boltzmann_factor_like_terms = (double*)malloc(Num_input_structures * sizeof(double));
  factor_for_force_for_each_structure = (double*)malloc(Num_input_structures * sizeof(double));
  
  /* zero froces for the current configuration (or frame) */
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < 3 ; d++)
        {
          fr->f[i][d] = 0.0;
        }
    }  
  
  /* zero forces for data structure that stores forces from each individual structure*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      for(i = 0 ; i < fr->natoms ; i++)
	{
	  for(d = 0 ; d < 3 ; d++)
	    {
	      (*coordinates_all_structures)[a].f[i][d] = 0.0;
	    }
	}
      (*coordinates_all_structures)[a].U = 0.0;
    }
  
  /* zero total energy*/
  fr->U = 0.0;
  fr->U_r = 0.0;
  fr->U_en = 0.0;

  /* double loop over all possible pairs */
  for(i = 0 ; i < (fr->natoms - 1) ; i++)
    {
      for(j = (i+1) ; j < fr->natoms ; j++)
        {
          /* calculate distance and unit vector*/
          Calc_simple_distance(3, fr->x[i], fr->x[j], &distance, &distance_squared, distance_vector);
          for(d = 0 ; d < 3 ; d++)
            {
              unit_vector[d] = distance_vector[d] / distance;
            }
          /* Calculate force and potential from the repulsive part and sum 
             these forces and potential for both the sites into the main data structure*/
          Cal_repulsive_force_pot_one_pair(distance, i, j, sigmas_for_repulsive_part, epsilons_for_repulsive_part, 12, &scalar_force_repulsive, &potential_repulsive);
          fr->U = fr->U + potential_repulsive;
          fr->U_r = fr->U_r + potential_repulsive;
          for(d = 0 ; d < 3 ; d++)
            {
              temp_force[d] = scalar_force_repulsive * unit_vector[d];
              fr->f[i][d] = fr->f[i][d] + (-temp_force[d]);
              fr->f[j][d] = fr->f[j][d] + temp_force[d];
            }

          /* loop over all the structure and calculate and sum force and potential for each 
             structure in separate data structures.*/
          for(a = 0 ; a < Num_input_structures ; a++)
            {
              /*first check for cutoff. I.e. calculate interaction only if the pair distance
                between two sites in question in the static structure is less than the cutoff 
		for that structure.*/
	      if(array_of_pair_distance_matrices[a][i][j] < aray_of_cutoff_distances[a])
		{
		  get_force_one_pair_one_structure(distance, i, j, a, array_of_pair_distance_matrices, force_constants_all_pairs, &scalar_force_one_structure, &potential_one_structure);
		  (*coordinates_all_structures)[a].U = (*coordinates_all_structures)[a].U + potential_one_structure;
		  for(d = 0 ; d < 3 ; d++)
		    {
		      temp_force[d] = scalar_force_one_structure * unit_vector[d];
		      (*coordinates_all_structures)[a].f[i][d] = (*coordinates_all_structures)[a].f[i][d] +
			(-temp_force[d]);
		      (*coordinates_all_structures)[a].f[j][d] = (*coordinates_all_structures)[a].f[j][d] +
			temp_force[d];
		    }
		}
            }     /*end of loop over a*/
        }      /*end of j loop*/
    }       /*end of i loop*/  
  
  /* Now we have calcualted the potentials for both the structures, use them to calculate
     constant terms. Constant terms are like Boltzmann factors where each term involves 
     the potential energy of that structure.*/
  /* Calculate Boltzmann term*/
  sum_of_Boltzmann_factors = 0.0;
  /* First state*/
  temp = -beta_m * ((*coordinates_all_structures)[0].U);
  Boltzmann_factor_like_terms[0] = exp(temp);
  /* Second state*/
  temp = -beta_m * ( ((*coordinates_all_structures)[1].U) + energy_difference_from_first_structure );
  Boltzmann_factor_like_terms[1] = exp(temp);
  /* Sum of Boltzmann terms*/
  sum_of_Boltzmann_factors = Boltzmann_factor_like_terms[0] + Boltzmann_factor_like_terms[1];
  /* Calculate the constant factor for each structure that forces need to be multiplied by.*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      factor_for_force_for_each_structure[a] = Boltzmann_factor_like_terms[a] / sum_of_Boltzmann_factors;
    } 

  /* Update the total forces and total forces on all the particles. It will involve a sum over
     all the structures.*/
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < 3 ; d++)
        {
          for(a = 0 ; a < Num_input_structures ; a++)
            {
              temp = factor_for_force_for_each_structure[a];
              fr->f[i][d] = fr->f[i][d] + (temp * (*coordinates_all_structures)[a].f[i][d]);
            }
        }
    }

  /*Add constraining force*/
  for(i=0;i<Num_cv;i++){
    
      a = cons_distances[i].atomi;
      b = cons_distances[i].atomj;
      Calc_simple_distance(3, fr->x[a], fr->x[b], &distance, &distance_squared, distance_vector);
       //   printf("dis[%d][%d]=%lf\n",a+1,b+1,distance); 
      for(d = 0 ; d < 3 ; d++)
        {
          unit_vector[d] = distance_vector[d] / distance;
          fr->f[a][d] += KK*unit_vector[d]*( distance - cons_distances[i].dis);
          fr->f[b][d] += -KK*unit_vector[d]*( distance - cons_distances[i].dis);
        }
  }


  fr->U_en = (-(1.0 / beta_m) * log(sum_of_Boltzmann_factors));
  /* Calculate the total energy*/
  fr->U = fr->U + (-(1.0 / beta_m) * log(sum_of_Boltzmann_factors));

  free(Boltzmann_factor_like_terms);
  free(factor_for_force_for_each_structure);
}



void getforces_cons_harm(FR_DAT *fr, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, collective_variables *cons_distances, int Num_cv, FR_DAT fix_position, double KK, double Kharm)
{
  int a, b, i, j, d;
  double distance, distance_squared, scalar_force_repulsive, potential_repulsive;
  double scalar_force_one_structure, potential_one_structure;
  double distance_vector[3];
  double unit_vector[3];
  double temp_force[3];  /*this is used for various stages of force calculation*/
  double beta_m;        /*Mixing parameter*/
  double *Boltzmann_factor_like_terms;     /* one for each structure*/
  double temp;          /*for miscellenious purposes*/
  double sum_of_Boltzmann_factors;
  double *factor_for_force_for_each_structure;

  beta_m = array_of_mixing_parameters[0];       /*two input structures. Only one mixing parameter*/

  Boltzmann_factor_like_terms = (double*)malloc(Num_input_structures * sizeof(double));
  factor_for_force_for_each_structure = (double*)malloc(Num_input_structures * sizeof(double));
  
  /* zero froces for the current configuration (or frame) */
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < 3 ; d++)
        {
          fr->f[i][d] = 0.0;
        }
    }  
  
  /* zero forces for data structure that stores forces from each individual structure*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      for(i = 0 ; i < fr->natoms ; i++)
	{
	  for(d = 0 ; d < 3 ; d++)
	    {
	      (*coordinates_all_structures)[a].f[i][d] = 0.0;
	    }
	}
      (*coordinates_all_structures)[a].U = 0.0;
    }
  
  /* zero total energy*/
  fr->U = 0.0;
  fr->U_r = 0.0;
  fr->U_en = 0.0;

  /* double loop over all possible pairs */
  for(i = 0 ; i < (fr->natoms - 1) ; i++)
    {
      for(j = (i+1) ; j < fr->natoms ; j++)
        {
          /* calculate distance and unit vector*/
          Calc_simple_distance(3, fr->x[i], fr->x[j], &distance, &distance_squared, distance_vector);
          for(d = 0 ; d < 3 ; d++)
            {
              unit_vector[d] = distance_vector[d] / distance;
            }
          /* Calculate force and potential from the repulsive part and sum 
             these forces and potential for both the sites into the main data structure*/
          Cal_repulsive_force_pot_one_pair(distance, i, j, sigmas_for_repulsive_part, epsilons_for_repulsive_part, 12, &scalar_force_repulsive, &potential_repulsive);
          fr->U = fr->U + potential_repulsive;
          fr->U_r = fr->U_r + potential_repulsive;
          for(d = 0 ; d < 3 ; d++)
            {
              temp_force[d] = scalar_force_repulsive * unit_vector[d];
              fr->f[i][d] = fr->f[i][d] + (-temp_force[d]);
              fr->f[j][d] = fr->f[j][d] + temp_force[d];
            }

          /* loop over all the structure and calculate and sum force and potential for each 
             structure in separate data structures.*/
          for(a = 0 ; a < Num_input_structures ; a++)
            {
              /*first check for cutoff. I.e. calculate interaction only if the pair distance
                between two sites in question in the static structure is less than the cutoff 
		for that structure.*/
	      if(array_of_pair_distance_matrices[a][i][j] < aray_of_cutoff_distances[a])
		{
		  get_force_one_pair_one_structure(distance, i, j, a, array_of_pair_distance_matrices, force_constants_all_pairs, &scalar_force_one_structure, &potential_one_structure);
		  (*coordinates_all_structures)[a].U = (*coordinates_all_structures)[a].U + potential_one_structure;
		  for(d = 0 ; d < 3 ; d++)
		    {
		      temp_force[d] = scalar_force_one_structure * unit_vector[d];
		      (*coordinates_all_structures)[a].f[i][d] = (*coordinates_all_structures)[a].f[i][d] +
			(-temp_force[d]);
		      (*coordinates_all_structures)[a].f[j][d] = (*coordinates_all_structures)[a].f[j][d] +
			temp_force[d];
		    }
		}
            }     /*end of loop over a*/
        }      /*end of j loop*/
    }       /*end of i loop*/  
  
  /* Now we have calcualted the potentials for both the structures, use them to calculate
     constant terms. Constant terms are like Boltzmann factors where each term involves 
     the potential energy of that structure.*/
  /* Calculate Boltzmann term*/
  sum_of_Boltzmann_factors = 0.0;
  /* First state*/
  temp = -beta_m * ((*coordinates_all_structures)[0].U);
  Boltzmann_factor_like_terms[0] = exp(temp);
  /* Second state*/
  temp = -beta_m * ( ((*coordinates_all_structures)[1].U) + energy_difference_from_first_structure );
  Boltzmann_factor_like_terms[1] = exp(temp);
  /* Sum of Boltzmann terms*/
  sum_of_Boltzmann_factors = Boltzmann_factor_like_terms[0] + Boltzmann_factor_like_terms[1];
  /* Calculate the constant factor for each structure that forces need to be multiplied by.*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      factor_for_force_for_each_structure[a] = Boltzmann_factor_like_terms[a] / sum_of_Boltzmann_factors;
    } 

  /* Update the total forces and total forces on all the particles. It will involve a sum over
     all the structures.*/
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < 3 ; d++)
        {
          for(a = 0 ; a < Num_input_structures ; a++)
            {
              temp = factor_for_force_for_each_structure[a];
              fr->f[i][d] = fr->f[i][d] + (temp * (*coordinates_all_structures)[a].f[i][d]);
            }
            //Add harmonic constraining force
            fr->f[i][d] += -2*Kharm*(fr->x[i][d] - fix_position.x[i][d]);

        }
    }

  /*Add constraining force*/
  for(i=0;i<Num_cv;i++){
    
      a = cons_distances[i].atomi;
      b = cons_distances[i].atomj;
      Calc_simple_distance(3, fr->x[a], fr->x[b], &distance, &distance_squared, distance_vector);
       //   printf("dis[%d][%d]=%lf\n",a+1,b+1,distance); 
      for(d = 0 ; d < 3 ; d++)
        {
          unit_vector[d] = distance_vector[d] / distance;
          fr->f[a][d] += KK*unit_vector[d]*( distance - cons_distances[i].dis);
          fr->f[b][d] += -KK*unit_vector[d]*( distance - cons_distances[i].dis);
        }
  }

 // printf("%lf %lf\n", KK, Kharm);

  fr->U_en = (-(1.0 / beta_m) * log(sum_of_Boltzmann_factors));
  /* Calculate the total energy*/
  fr->U = fr->U + (-(1.0 / beta_m) * log(sum_of_Boltzmann_factors));

  free(Boltzmann_factor_like_terms);
  free(factor_for_force_for_each_structure);
}


void getforces_harm(FR_DAT *fr, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, FR_DAT fix_position, double Kharm)
{
  int a, i, j, d;
  double distance, distance_squared, scalar_force_repulsive, potential_repulsive;
  double scalar_force_one_structure, potential_one_structure;
  double distance_vector[3];
  double unit_vector[3];
  double temp_force[3];  /*this is used for various stages of force calculation*/
  double beta_m;        /*Mixing parameter*/
  double *Boltzmann_factor_like_terms;     /* one for each structure*/
  double temp;          /*for miscellenious purposes*/
  double sum_of_Boltzmann_factors;
  double *factor_for_force_for_each_structure;

  beta_m = array_of_mixing_parameters[0];       /*two input structures. Only one mixing parameter*/

  Boltzmann_factor_like_terms = (double*)malloc(Num_input_structures * sizeof(double));
  factor_for_force_for_each_structure = (double*)malloc(Num_input_structures * sizeof(double));
  
  /* zero froces for the current configuration (or frame) */
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < 3 ; d++)
        {
          fr->f[i][d] = 0.0;
        }
    }  
  
  /* zero forces for data structure that stores forces from each individual structure*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      for(i = 0 ; i < fr->natoms ; i++)
	{
	  for(d = 0 ; d < 3 ; d++)
	    {
	      (*coordinates_all_structures)[a].f[i][d] = 0.0;
	    }
	}
      (*coordinates_all_structures)[a].U = 0.0;
    }
  
  /* zero total energy*/
  fr->U = 0.0;
  fr->U_r = 0.0;
  fr->U_en = 0.0;

  /* double loop over all possible pairs */
  for(i = 0 ; i < (fr->natoms - 1) ; i++)
    {
      for(j = (i+1) ; j < fr->natoms ; j++)
        {
          /* calculate distance and unit vector*/
          Calc_simple_distance(3, fr->x[i], fr->x[j], &distance, &distance_squared, distance_vector);
          for(d = 0 ; d < 3 ; d++)
            {
              unit_vector[d] = distance_vector[d] / distance;
            }
          /* Calculate force and potential from the repulsive part and sum 
             these forces and potential for both the sites into the main data structure*/
          Cal_repulsive_force_pot_one_pair(distance, i, j, sigmas_for_repulsive_part, epsilons_for_repulsive_part, 12, &scalar_force_repulsive, &potential_repulsive);
          fr->U = fr->U + potential_repulsive;
          fr->U_r = fr->U_r + potential_repulsive;
          for(d = 0 ; d < 3 ; d++)
            {
              temp_force[d] = scalar_force_repulsive * unit_vector[d];
              fr->f[i][d] = fr->f[i][d] + (-temp_force[d]);
              fr->f[j][d] = fr->f[j][d] + temp_force[d];
            }

          /* loop over all the structure and calculate and sum force and potential for each 
             structure in separate data structures.*/
          for(a = 0 ; a < Num_input_structures ; a++)
            {
              /*first check for cutoff. I.e. calculate interaction only if the pair distance
                between two sites in question in the static structure is less than the cutoff 
		for that structure.*/
	      if(array_of_pair_distance_matrices[a][i][j] < aray_of_cutoff_distances[a])
		{
		  get_force_one_pair_one_structure(distance, i, j, a, array_of_pair_distance_matrices, force_constants_all_pairs, &scalar_force_one_structure, &potential_one_structure);
		  (*coordinates_all_structures)[a].U = (*coordinates_all_structures)[a].U + potential_one_structure;
		  for(d = 0 ; d < 3 ; d++)
		    {
		      temp_force[d] = scalar_force_one_structure * unit_vector[d];
		      (*coordinates_all_structures)[a].f[i][d] = (*coordinates_all_structures)[a].f[i][d] +
			(-temp_force[d]);
		      (*coordinates_all_structures)[a].f[j][d] = (*coordinates_all_structures)[a].f[j][d] +
			temp_force[d];
		    }
		}
            }     /*end of loop over a*/
        }      /*end of j loop*/
    }       /*end of i loop*/  
  
  /* Now we have calcualted the potentials for both the structures, use them to calculate
     constant terms. Constant terms are like Boltzmann factors where each term involves 
     the potential energy of that structure.*/
  /* Calculate Boltzmann term*/
  sum_of_Boltzmann_factors = 0.0;
  /* First state*/
  temp = -beta_m * ((*coordinates_all_structures)[0].U);
  Boltzmann_factor_like_terms[0] = exp(temp);
  /* Second state*/
  temp = -beta_m * ( ((*coordinates_all_structures)[1].U) + energy_difference_from_first_structure );
  Boltzmann_factor_like_terms[1] = exp(temp);
  /* Sum of Boltzmann terms*/
  sum_of_Boltzmann_factors = Boltzmann_factor_like_terms[0] + Boltzmann_factor_like_terms[1];
  /* Calculate the constant factor for each structure that forces need to be multiplied by.*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      factor_for_force_for_each_structure[a] = Boltzmann_factor_like_terms[a] / sum_of_Boltzmann_factors;
    } 

  /* Update the total forces and total forces on all the particles. It will involve a sum over
     all the structures.*/
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < 3 ; d++)
        {
          for(a = 0 ; a < Num_input_structures ; a++)
            {
              temp = factor_for_force_for_each_structure[a];
              fr->f[i][d] = fr->f[i][d] + (temp * (*coordinates_all_structures)[a].f[i][d]);
            }
            //Add harmonic constraining force
            fr->f[i][d] += -2*Kharm*(fr->x[i][d] - fix_position.x[i][d]);
        }
    }

  fr->U_en = (-(1.0 / beta_m) * log(sum_of_Boltzmann_factors));
  /* Calculate the total energy*/
  fr->U = fr->U + (-(1.0 / beta_m) * log(sum_of_Boltzmann_factors));

  free(Boltzmann_factor_like_terms);
  free(factor_for_force_for_each_structure);
}


void getforces_cons_harm_omp(FR_DAT *fr, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, collective_variables *cons_distances, int Num_cv, FR_DAT fix_position, double KK, double Kharm, int Num_atoms)
{
  int a, b, i, j, d;
  double distance, distance_squared, scalar_force_repulsive, potential_repulsive;
  double scalar_force_one_structure, potential_one_structure;
  double distance_vector[3];
  double unit_vector[3];
  double temp_force[3];  /*this is used for various stages of force calculation*/
  double beta_m;        /*Mixing parameter*/
  double *Boltzmann_factor_like_terms;     /* one for each structure*/
  double temp;          /*for miscellenious purposes*/
  double sum_of_Boltzmann_factors;
  double *factor_for_force_for_each_structure;

  beta_m = array_of_mixing_parameters[0];       /*two input structures. Only one mixing parameter*/

  Boltzmann_factor_like_terms = (double*)malloc(Num_input_structures * sizeof(double));
  factor_for_force_for_each_structure = (double*)malloc(Num_input_structures * sizeof(double));
  
  /* zero froces for the current configuration (or frame) */
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < 3 ; d++)
        {
          fr->f[i][d] = 0.0;
        }
    }  
  
  FR_DAT_MINI *coordinates_all_structures_mini;

  coordinates_all_structures_mini = (FR_DAT_MINI *)malloc(Num_input_structures * sizeof(FR_DAT_MINI));
 
  for(a=0; a< Num_input_structures; a++){
        coordinates_all_structures_mini[a].f = (rvec*)malloc(Num_atoms * sizeof(rvec));
  }
 
 
  /* zero forces for data structure that stores forces from each individual structure*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      for(i = 0 ; i < fr->natoms ; i++)
	{
	  for(d = 0 ; d < 3 ; d++)
	    {
	      coordinates_all_structures_mini[a].f[i][d] = 0.0;
	    }
	}
      coordinates_all_structures_mini[a].U = 0.0;
    }
  
  /* zero total energy*/
  fr->U = 0.0;
  fr->U_r = 0.0;
  fr->U_en = 0.0;

  /* double loop over all possible pairs */
  for(i = 0 ; i < (fr->natoms - 1) ; i++)
    {
      for(j = (i+1) ; j < fr->natoms ; j++)
        {
          /* calculate distance and unit vector*/
          Calc_simple_distance(3, fr->x[i], fr->x[j], &distance, &distance_squared, distance_vector);
          for(d = 0 ; d < 3 ; d++)
            {
              unit_vector[d] = distance_vector[d] / distance;
            }
          /* Calculate force and potential from the repulsive part and sum 
             these forces and potential for both the sites into the main data structure*/
          //ADD THRESHOLD
          //
          //CHANGE POWER
          //if(distance > 1.8){
          Cal_repulsive_force_pot_one_pair(distance, i, j, sigmas_for_repulsive_part, epsilons_for_repulsive_part, 2, &scalar_force_repulsive, &potential_repulsive);
          //}
          //else {
          //Cal_repulsive_force_pot_one_pair(distance, i, j, sigmas_for_repulsive_part, epsilons_for_repulsive_part, 2, &scalar_force_repulsive, &potential_repulsive);
          //scalar_force_repulsive=0;  
          //potential_repulsive=0;
          //}
            //printf("test scalar force %f  atom: %d  %d\n", scalar_force_repulsive, i, j);
            //fflush(stdout);
          
          fr->U = fr->U + potential_repulsive;
          fr->U_r = fr->U_r + potential_repulsive;
          for(d = 0 ; d < 3 ; d++)
            {
              temp_force[d] = scalar_force_repulsive * unit_vector[d];
              fr->f[i][d] = fr->f[i][d] + (-temp_force[d]);
              fr->f[j][d] = fr->f[j][d] + temp_force[d];
            }

          /* loop over all the structure and calculate and sum force and potential for each 
             structure in separate data structures.*/
          for(a = 0 ; a < Num_input_structures ; a++)
            {
              /*first check for cutoff. I.e. calculate interaction only if the pair distance
                between two sites in question in the static structure is less than the cutoff 
		for that structure.*/
	      if(array_of_pair_distance_matrices[a][i][j] < aray_of_cutoff_distances[a])
		{

                 // if(array_of_pair_distance_matrices[a][i][j]>3.6){
		  get_force_one_pair_one_structure(distance, i, j, a, array_of_pair_distance_matrices, force_constants_all_pairs, &scalar_force_one_structure, &potential_one_structure);
                 // }
                 // else{
                   // scalar_force_one_structure=0;
                   // potential_one_structure=0;
                  
                 // }
            
                  //printf("test scalar force %f  atom: %d  %d\n", scalar_force_one_structure, i, j);
                  //fflush(stdout);
		  
            
                  coordinates_all_structures_mini[a].U = coordinates_all_structures_mini[a].U + potential_one_structure;
		  for(d = 0 ; d < 3 ; d++)
		    {
		      temp_force[d] = scalar_force_one_structure * unit_vector[d];
		      coordinates_all_structures_mini[a].f[i][d] = coordinates_all_structures_mini[a].f[i][d] +
			(-temp_force[d]);
		      coordinates_all_structures_mini[a].f[j][d] = coordinates_all_structures_mini[a].f[j][d] +
			temp_force[d];
		    }
		}
            }     /*end of loop over a*/
        }      /*end of j loop*/
    }       /*end of i loop*/  
  
  /* Now we have calcualted the potentials for both the structures, use them to calculate
     constant terms. Constant terms are like Boltzmann factors where each term involves 
     the potential energy of that structure.*/
  /* Calculate Boltzmann term*/
  sum_of_Boltzmann_factors = 0.0;
  /* First state*/
  temp = -beta_m * (coordinates_all_structures_mini[0].U);
             //printf("test temp %f  atom: %d\n", temp, i);
             //fflush(stdout);
  Boltzmann_factor_like_terms[0] = exp(temp);
  /* Second state*/
  temp = -beta_m * ( (coordinates_all_structures_mini[1].U) + energy_difference_from_first_structure );
  Boltzmann_factor_like_terms[1] = exp(temp);
  /* Sum of Boltzmann terms*/
  sum_of_Boltzmann_factors = Boltzmann_factor_like_terms[0] + Boltzmann_factor_like_terms[1];
  /* Calculate the constant factor for each structure that forces need to be multiplied by.*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      factor_for_force_for_each_structure[a] = Boltzmann_factor_like_terms[a] / sum_of_Boltzmann_factors;
    } 

  /* Update the total forces and total forces on all the particles. It will involve a sum over
     all the structures.*/
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < 3 ; d++)
        {
          for(a = 0 ; a < Num_input_structures ; a++)
            {
              temp = factor_for_force_for_each_structure[a];
              fr->f[i][d] = fr->f[i][d] + (temp * coordinates_all_structures_mini[a].f[i][d]);
                  
            }
            //Add harmonic constraining force
            fr->f[i][d] += -2*Kharm*(fr->x[i][d] - fix_position.x[i][d]);
           // printf("force test %f\n", fr->f[i][d]);
           // fflush(stdout);

        }
    }

           // printf("force test %f\n", fr->f[13][1]);
           // fflush(stdout);
  /*Add constraining force*/
  for(i=0;i<Num_cv;i++){
    
      a = cons_distances[i].atomi;
      b = cons_distances[i].atomj;
      Calc_simple_distance(3, fr->x[a], fr->x[b], &distance, &distance_squared, distance_vector);
      for(d = 0 ; d < 3 ; d++)
        {
          unit_vector[d] = distance_vector[d] / distance;
          fr->f[a][d] += KK*unit_vector[d]*( distance - cons_distances[i].dis);
          fr->f[b][d] += -KK*unit_vector[d]*( distance - cons_distances[i].dis);
        }
         //if(i%500==0) printf("force test2 %lf\n", fr->f[a][1]); 
         //fflush(stdout);
  }

 // printf("%lf %lf\n", KK, Kharm);

  fr->U_en = (-(1.0 / beta_m) * log(sum_of_Boltzmann_factors));
  /* Calculate the total energy*/
  fr->U = fr->U + (-(1.0 / beta_m) * log(sum_of_Boltzmann_factors));

  free(Boltzmann_factor_like_terms);
  free(factor_for_force_for_each_structure);

  for(a=0; a<Num_input_structures;a++)
    free(coordinates_all_structures_mini[a].f);
  free(coordinates_all_structures_mini);
}


void getforces_harm_omp(FR_DAT *fr, FR_DAT **coordinates_all_structures, int Num_input_structures, double ***array_of_pair_distance_matrices, double **force_constants_all_pairs, double **sigmas_for_repulsive_part, double **epsilons_for_repulsive_part, double *array_of_mixing_parameters, double *aray_of_cutoff_distances, double energy_difference_from_first_structure, FR_DAT fix_position, double Kharm, int Num_atoms)
{
  int a, i, j, d;
  double distance, distance_squared, scalar_force_repulsive, potential_repulsive;
  double scalar_force_one_structure, potential_one_structure;
  double distance_vector[3];
  double unit_vector[3];
  double temp_force[3];  /*this is used for various stages of force calculation*/
  double beta_m;        /*Mixing parameter*/
  double *Boltzmann_factor_like_terms;     /* one for each structure*/
  double temp;          /*for miscellenious purposes*/
  double sum_of_Boltzmann_factors;
  double *factor_for_force_for_each_structure;

  beta_m = array_of_mixing_parameters[0];       /*two input structures. Only one mixing parameter*/

  Boltzmann_factor_like_terms = (double*)malloc(Num_input_structures * sizeof(double));
  factor_for_force_for_each_structure = (double*)malloc(Num_input_structures * sizeof(double));
  
  FR_DAT_MINI *coordinates_all_structures_mini;

  coordinates_all_structures_mini = (FR_DAT_MINI *)malloc(Num_input_structures * sizeof(FR_DAT_MINI));
 
  for(a=0; a< Num_input_structures; a++){
        coordinates_all_structures_mini[a].f = (rvec*)malloc(Num_atoms * sizeof(rvec));
  }
 
  /* zero froces for the current configuration (or frame) */
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < 3 ; d++)
        {
          fr->f[i][d] = 0.0;
        }
    }  
  
  /* zero forces for data structure that stores forces from each individual structure*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      for(i = 0 ; i < fr->natoms ; i++)
	{
	  for(d = 0 ; d < 3 ; d++)
	    {
	      coordinates_all_structures_mini[a].f[i][d] = 0.0;
	    }
	}
      coordinates_all_structures_mini[a].U = 0.0;
    }
  
  /* zero total energy*/
  fr->U = 0.0;
  fr->U_r = 0.0;
  fr->U_en = 0.0;

  /* double loop over all possible pairs */
  for(i = 0 ; i < (fr->natoms - 1) ; i++)
    {
      for(j = (i+1) ; j < fr->natoms ; j++)
        {
          /* calculate distance and unit vector*/
          Calc_simple_distance(3, fr->x[i], fr->x[j], &distance, &distance_squared, distance_vector);
          for(d = 0 ; d < 3 ; d++)
            {
              unit_vector[d] = distance_vector[d] / distance;
            }
          /* Calculate force and potential from the repulsive part and sum 
             these forces and potential for both the sites into the main data structure*/
          //ADD THRESHOLD
          //
          //CHANGE POWER
          //if(distance > 1.8){
          Cal_repulsive_force_pot_one_pair(distance, i, j, sigmas_for_repulsive_part, epsilons_for_repulsive_part, 2, &scalar_force_repulsive, &potential_repulsive);
          //}
          //else {
          //Cal_repulsive_force_pot_one_pair(distance, i, j, sigmas_for_repulsive_part, epsilons_for_repulsive_part, 2, &scalar_force_repulsive, &potential_repulsive);
          //scalar_force_repulsive=0;  
          //potential_repulsive=0;
          //}
           // printf("test scalar force2 %f  atom: %d  %d\n", scalar_force_repulsive, i, j);
           // fflush(stdout);
          fr->U = fr->U + potential_repulsive;
          fr->U_r = fr->U_r + potential_repulsive;
          for(d = 0 ; d < 3 ; d++)
            {
              temp_force[d] = scalar_force_repulsive * unit_vector[d];
              fr->f[i][d] = fr->f[i][d] + (-temp_force[d]);
              fr->f[j][d] = fr->f[j][d] + temp_force[d];
            }

          /* loop over all the structure and calculate and sum force and potential for each 
             structure in separate data structures.*/
          for(a = 0 ; a < Num_input_structures ; a++)
            {
              /*first check for cutoff. I.e. calculate interaction only if the pair distance
                between two sites in question in the static structure is less than the cutoff 
		for that structure.*/
	      if(array_of_pair_distance_matrices[a][i][j] < aray_of_cutoff_distances[a])
		{
		  get_force_one_pair_one_structure(distance, i, j, a, array_of_pair_distance_matrices, force_constants_all_pairs, &scalar_force_one_structure, &potential_one_structure);
           // printf("test potential %f  atom: %d  %d\n", potential_one_structure, i, j);
           // fflush(stdout);
		  coordinates_all_structures_mini[a].U = coordinates_all_structures_mini[a].U + potential_one_structure;
		  for(d = 0 ; d < 3 ; d++)
		    {
		      temp_force[d] = scalar_force_one_structure * unit_vector[d];
		      coordinates_all_structures_mini[a].f[i][d] = coordinates_all_structures_mini[a].f[i][d] +
			(-temp_force[d]);
		      coordinates_all_structures_mini[a].f[j][d] = coordinates_all_structures_mini[a].f[j][d] +
			temp_force[d];
		    }
		}
            }     /*end of loop over a*/
        }      /*end of j loop*/
    }       /*end of i loop*/  
  
  /* Now we have calcualted the potentials for both the structures, use them to calculate
     constant terms. Constant terms are like Boltzmann factors where each term involves 
     the potential energy of that structure.*/
  /* Calculate Boltzmann term*/
  sum_of_Boltzmann_factors = 0.0;
  /* First state*/
  temp = -beta_m * (coordinates_all_structures_mini[0].U);
             //printf("test temp2 %f  atom: %d\n", temp, i);
             //fflush(stdout);
  Boltzmann_factor_like_terms[0] = exp(temp);
  /* Second state*/
  temp = -beta_m * ( (coordinates_all_structures_mini[1].U) + energy_difference_from_first_structure );
  Boltzmann_factor_like_terms[1] = exp(temp);
  /* Sum of Boltzmann terms*/
  sum_of_Boltzmann_factors = Boltzmann_factor_like_terms[0] + Boltzmann_factor_like_terms[1];
  /* Calculate the constant factor for each structure that forces need to be multiplied by.*/
  for(a = 0 ; a < Num_input_structures ; a++)
    {
      factor_for_force_for_each_structure[a] = Boltzmann_factor_like_terms[a] / sum_of_Boltzmann_factors;
    } 

  /* Update the total forces and total forces on all the particles. It will involve a sum over
     all the structures.*/
  for(i = 0 ; i < fr->natoms ; i++)
    {
      for(d = 0 ; d < 3 ; d++)
        {
          for(a = 0 ; a < Num_input_structures ; a++)
            {
              temp = factor_for_force_for_each_structure[a];
              fr->f[i][d] = fr->f[i][d] + (temp * coordinates_all_structures_mini[a].f[i][d]);
            }
            //Add harmonic constraining force
            fr->f[i][d] += -2*Kharm*(fr->x[i][d] - fix_position.x[i][d]);
        }
    }

  fr->U_en = (-(1.0 / beta_m) * log(sum_of_Boltzmann_factors));
  /* Calculate the total energy*/
  fr->U = fr->U + (-(1.0 / beta_m) * log(sum_of_Boltzmann_factors));

  free(Boltzmann_factor_like_terms);
  free(factor_for_force_for_each_structure);
  for(a=0; a<Num_input_structures;a++)
    free(coordinates_all_structures_mini[a].f);
  free(coordinates_all_structures_mini);
}




