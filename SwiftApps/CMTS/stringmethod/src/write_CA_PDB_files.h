#ifndef _write_CA_PDB_files_h
#define _write_CA_PDB_files_h

#include "md_ld_common_d_dimension_namd.h"
#include "write_pdb_atom_record.h"


void Write_one_CA_config_in_PDB_format(FILE *out, FR_DAT *fr, char **Residue_names);
void Write_one_CA_config_in_PDB_format2(FILE *out, FR_DAT *fr, char **Residue_names);
void Write_one_CA_config_in_PDB_format3(FILE *out, FR_DAT *fr, char **Atom_names, char **Residue_names);

void Read_residue_names(int Num_CA_atoms, char ***Residue_names);

void Write_CA_PDB_file(FR_DAT *fr, char **Residue_names, char File_name_CA_PDB[1000]);
void Write_CA_PDB_file2(FR_DAT *fr, char **Residue_names, char File_name_CA_PDB[1000]);

void Write_config_to_a_PDB_trajectory_file(FILE *out, FR_DAT *fr, char **Residue_names, int config_index);

void Write_config_to_a_PDB_trajectory_file_1(FILE *out, FR_DAT *fr, char **Residue_names, long int config_index);

void Read_residue_names2(int Num_CA_atoms, char *** Atom_names, char ***Residue_names);

void Write_config_to_a_PDB_trajectory_file_2(FILE *out, FR_DAT *fr, char **Atom_names, char **Residue_names, long int config_index);
#endif
