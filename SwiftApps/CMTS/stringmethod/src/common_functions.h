#ifndef _commmon_functions_h
#define _commmon_functions_h

#include "md_ld_common_d_dimension_namd.h"


double celladjustOneComp(double OneCompOfVector, int index_of_component,  my_matrix box);

void celladjust_one_config(FR_DAT *const fr);

double celladjustOneComp_origin_box_center(double OneCompOfVector, int index_of_component,  my_matrix box);

void celladjust_one_config_origin_box_center(FR_DAT *const fr);

int velocenter(FR_DAT *const fr);

double getnorm();
double getnorm_omp(struct drand48_data *buffer);
void seed_rand(int thread_n, struct drand48_data *buffer);

void getvelocities(FR_DAT *const fr, double temperature);

void Randomize(void);

void get_kinetic_energy(FR_DAT *const fr);

void Alloc_main_data_structure(int Num_atoms, FR_DAT *const fr);

#endif
