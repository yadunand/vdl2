
/* Last modified on December 29, 2011*/

#ifndef _read_langevin_parameters_h
#define _read_langevin_parameters_h

#include "md_ld_common_d_dimension_namd.h"


/*reads friction coefficients for Langevin dynamics simulation.
 * It also populates the array of type called 'sigma_langevin'. 
 * (See 'md_ld_common_d_dimension.h').
  The filename is : LANGEVIN_FRICTION
  Format:
  NUM_PARTICLES (number of particles %d)
  Value of friction coefficients. One value per line
*/
void ReadFrictionCoefficients(struct langevin *Langevin_info, double temperature, double Boltzmann_constant, FR_DAT fr);
void ReadFrictionCoefficients2(struct langevin *Langevin_info, double temperature, double Boltzmann_constant, FR_DAT fr);


#endif
