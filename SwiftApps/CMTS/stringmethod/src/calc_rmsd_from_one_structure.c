/* Calculate rmsd of the current configuration from a fixed state*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

#include "calc_rmsd_from_one_structure.h"

double Calc_rmsd(FR_DAT *fr, FR_DAT *fr_fixed_state)
{
  double msd, rmsd, dis1, dis2, dif, DEL_X, DEL_Y, DEL_Z;
  int i,j;
  msd = 0.0;
//  for(i = 0 ; i < fr->natoms; i++)
  for(i = 0 ; i < fr->natoms-1; i++)
    {
      for(j=i+1; j<fr->natoms; j++){
      DEL_X = (fr->x[i][0]-fr->x[j][0]);
      DEL_Y = (fr->x[i][1]-fr->x[j][1]);
      DEL_Z = (fr->x[i][2]-fr->x[j][2]);
      dis1 = pow(DEL_X,2)+pow(DEL_Y,2)+pow(DEL_Z,2);
      dis1 = sqrt(dis1);

      DEL_X = (fr_fixed_state->x[i][0]-fr_fixed_state->x[j][0]); 
      DEL_Y = (fr_fixed_state->x[i][1]-fr_fixed_state->x[j][1]); 
      DEL_Z = (fr_fixed_state->x[i][2]-fr_fixed_state->x[j][2]); 
      dis2 = pow(DEL_X,2)+pow(DEL_Y,2)+pow(DEL_Z,2);
      dis2 = sqrt(dis2);
      
      dif = dis1-dis2;

      msd += pow(dif,2);
      
      }

//      DEL_X = fr->x[i][0] - fr_fixed_state->x[i][0];
  //    DEL_Y = fr->x[i][1] - fr_fixed_state->x[i][1];
    //  DEL_Z = fr->x[i][2] - fr_fixed_state->x[i][2];
    //  msd = msd + pow(DEL_X, 2) + pow(DEL_Y, 2) + pow(DEL_Z, 2);
    }
//  msd = msd / ((double)fr->natoms);
  msd = msd / (fr->natoms * (fr->natoms-1)*0.5);
  rmsd = sqrt(msd);
  return rmsd;
}


