/* Last modified on Septemeber 29, 2011. */

/* Modifications are made to make the functions work for 
 * a d-dimensional system.
 * */

/* Data type 'matrix' is remaned to 'my_matrix' to avoid conflict with 
 * Numerical Recipes data type with the same name.*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include "common_functions.h"

/*each component of a position vector is within [0.0, box_length]
  index_for_coponent = 0 for x
  index_for_coponent = 1 for y
  index_for_coponent = 2 for z
*/
double celladjustOneComp(double OneCompOfVector, int index_of_component,  my_matrix box)
{
  double BoxLength;
  double CorrectedComponent;
  CorrectedComponent = OneCompOfVector;
  BoxLength = box[index_of_component][index_of_component];
  if((CorrectedComponent > BoxLength) || (CorrectedComponent < 0))
    {
      if(CorrectedComponent > BoxLength)
	{
	  while(CorrectedComponent > BoxLength)
	    {
	      CorrectedComponent = CorrectedComponent - BoxLength;
	    }
	}
      else
	{
	  while(CorrectedComponent < 0)
	    {
	      CorrectedComponent = CorrectedComponent + BoxLength;
	    }
	}
    }
  
  return CorrectedComponent;
}

/* adjusts the position vector of all the particles to keep them inside the
   central box for one frame (or config)*/
void celladjust_one_config(FR_DAT *const fr)
{
  int dim = DIMENSION;
  int d;
  int NumParticles = fr->natoms;
  double current_value;
  /*
  double xComponent;
  double yComponent;
  double zComponent;
  */

  int j;
    
  for (j = 0 ; j < NumParticles ; j++)
    {
      /*
      xComponent = fr->x[j][0];
      yComponent = fr->x[j][1];
      zComponent = fr->x[j][2];
      
      fr->x[j][0] = celladjustOneComp(xComponent, 0, fr->box);
      fr->x[j][1] = celladjustOneComp(yComponent, 1, fr->box);
      fr->x[j][2] = celladjustOneComp(zComponent, 2, fr->box);
      */

      for(d = 0 ; d < dim ; d++)
        {
          current_value = fr->x[j][d];
          fr->x[j][d] = celladjustOneComp(current_value, d, fr->box);
        }
    }
}


/*each component of a position vector is within [-box_length/2, box_length/2]
  index_for_coponent = 0 for x
  index_for_coponent = 1 for y
  index_for_coponent = 2 for z
*/
double celladjustOneComp_origin_box_center(double OneCompOfVector, int index_of_component,  my_matrix box)
{
  double BoxLength, Half_BoxLength;
  double CorrectedComponent;
  CorrectedComponent = OneCompOfVector;
  BoxLength = box[index_of_component][index_of_component];
  Half_BoxLength = box[index_of_component][index_of_component] / 2;
  if((CorrectedComponent > Half_BoxLength) || (CorrectedComponent < (-Half_BoxLength)))
    {
      if(CorrectedComponent > Half_BoxLength)
        {
          while(CorrectedComponent > Half_BoxLength)
            {
              CorrectedComponent = CorrectedComponent - BoxLength;
            }
        }
      if(CorrectedComponent < (-Half_BoxLength))
        {
          while(CorrectedComponent < (-Half_BoxLength))
            {
              CorrectedComponent = CorrectedComponent + BoxLength;
            }
        }
    }

  return CorrectedComponent;
}



/* adjusts the position vector of all the particles to keep them inside the
   central box for one frame (or config)*/
void celladjust_one_config_origin_box_center(FR_DAT *const fr)
{
  int dim = DIMENSION;
  int d;
  int NumParticles = fr->natoms;
  double current_value;
  /*
  double xComponent;
  double yComponent;
  double zComponent;
  */

  int j;
    
  for (j = 0 ; j < NumParticles ; j++)
    {
      /*
      xComponent = fr->x[j][0];
      yComponent = fr->x[j][1];
      zComponent = fr->x[j][2];
      
      fr->x[j][0] = celladjustOneComp(xComponent, 0, fr->box);
      fr->x[j][1] = celladjustOneComp(yComponent, 1, fr->box);
      fr->x[j][2] = celladjustOneComp(zComponent, 2, fr->box);
      */

      for(d = 0 ; d < dim ; d++)
        {
          current_value = fr->x[j][d];
          fr->x[j][d] = celladjustOneComp_origin_box_center(current_value, d, fr->box);
        }
    }
}




/*Corrects the drift of the system. Total momentun of the system should be zero*/
int velocenter(FR_DAT *const fr)
{
   int i,d;
   int dim;
   double totmass;
   /*
   double netvelx, netvely, netvelz;
   double driftx, drifty, driftz;
   driftx = 0;
   drifty = 0;
   driftz = 0;
   */
   double drift[DIMENSION], net_velocity_correction[DIMENSION];

   dim = DIMENSION;
   totmass = 0;
   for(d = 0 ; d < dim ; d++)
     {
       drift[d] = 0;
     }

   /* calculate the mean "drift" of the system and correct */
   for (i = 0; i < fr->natoms; i++) 
     {
       /*
       driftx = driftx + (fr->v[i][0] * fr->m[i]);
       drifty = drifty + (fr->v[i][1] * fr->m[i]);
       driftz = driftz + (fr->v[i][2] * fr->m[i]);
       */
       for(d = 0 ; d < dim ; d++)
         {
           drift[d] = drift[d] + (fr->v[i][d] * fr->m[i]); 
         }
       totmass = totmass + fr->m[i];              /* Find the total mass of the system */
     }
   /*
   netvelx = driftx / totmass;
   netvely = drifty / totmass; 
   netvelz = driftz / totmass;
   */
   /* Find the amount by which to correct the velocities.*/
   for(d = 0 ; d < dim ; d++)
     {
       net_velocity_correction[d] = drift[d] / totmass; 
     }
   /* Correct velocities  */
   for (i = 0; i < fr->natoms; i++) 
     {
       /*
       fr->v[i][0] = fr->v[i][0] - netvelx;
       fr->v[i][1] = fr->v[i][1] - netvely;
       fr->v[i][2] = fr->v[i][2] - netvelz;
       */
       for(d = 0 ; d < dim ; d++)
         {
           fr->v[i][d] = fr->v[i][d] - net_velocity_correction[d];
         }
     }
   return 0;
}



/*Generates random numbers from standard normal distributions*/
double getnorm()
{
  double normx, normy, w;
  w = 1.1;
  while (w > 1.0) 
    {
      normx = (double) rand() / ((double) RAND_MAX + 1);
      normy = (double) rand() / ((double) RAND_MAX + 1);
      normx = (2.0 * normx) - 1.0;
      normy = (2.0 * normy) - 1.0;
      w = (normx * normx) + (normy * normy);
    }
  w = (-2.0 * log(w)) / w;
  w = sqrt(w);
  return (normx * w);
}

void seed_rand(int thread_n, struct drand48_data *buffer)
{
  struct timeval tv;

  gettimeofday(&tv, NULL);
  srand48_r(tv.tv_sec*thread_n + tv.tv_usec, buffer);
}


double getnorm_omp(struct drand48_data *drand_buffer)
{
  double normx, normy, w;
  w = 1.1;
  while (w > 1.0) 
    {
      drand48_r(drand_buffer, &normx);
      drand48_r(drand_buffer, &normy);
      normx = (2.0 * normx) - 1.0;
      normy = (2.0 * normy) - 1.0;
      w = (normx * normx) + (normy * normy);
    }
  w = (-2.0 * log(w)) / w;
  w = sqrt(w);
  return (normx * w);
}


/*randomizes the velocity of each particle. 
  The Maxwell-Boltzmann distribution is  exp(-mv^2 / 2k_bT). Where m is mass 
  and k_b is the Boltzmann constant. In LJ reduced unit the Boltzmann constant
  is 1. BUT IN ANY OTHER UNIT IT IS NOT 1.

  BOLTZMANN CONSTANT IS #defined IN THE FILE 'md_common_structures.h'
*/
void getvelocities(FR_DAT *const fr, double temperature)
{
  int i, d;
  int dim = DIMENSION;
  double normfoo;
  double factor, factor_temp;
  double k_b = BOLTZMANN_CONSTANT;   /*This is the Boltzmann constant */
  double k_bT = k_b * temperature;
  for (i = 0; i < fr->natoms; i++) 
    {
      factor_temp = k_bT / fr->m[i];
      factor = sqrt(factor_temp);

      /*
      normfoo = getnorm();
      fr->v[i][0] = normfoo * factor;
      normfoo = getnorm();
      fr->v[i][1] = normfoo * factor;
      normfoo = getnorm();
      fr->v[i][2] = normfoo * factor;
      */

      for(d = 0 ; d < dim ; d++)
        {
          normfoo = getnorm();
          fr->v[i][d] = normfoo * factor;
        }
    }
}



/*Randomize the random number generator*/
void Randomize(void)
{
   srand((int) time(NULL));
}



/* Calculates kinetic energy of the system*/
void get_kinetic_energy(FR_DAT *const fr)
{
  int i, j;
  int dim = DIMENSION;
  double total_KE, velocity_squared;

  total_KE = 0.0;

  for(i = 0 ; i < fr->natoms ; i++)
    {
      velocity_squared = 0.0;
      for(j = 0 ; j < dim ; j++)
        {
          velocity_squared = velocity_squared + (fr->v[i][j] * fr->v[i][j]);
        }
      total_KE = total_KE + ((fr->m[i] * velocity_squared) / 2);
    }

  fr->K = total_KE;
}



/*allocate configuration data structure. Num_atoms is the total number of atoms in the
  topology file.*/
void Alloc_main_data_structure(int Num_atoms, FR_DAT *const fr)
{
  fr->natoms = Num_atoms;
  fr->x = (rvec *)malloc(Num_atoms * sizeof(rvec));
  fr->f = (rvec *)malloc(Num_atoms * sizeof(rvec));
  fr->v = (rvec *)malloc(Num_atoms * sizeof(rvec));
  fr->m = (double *)malloc(Num_atoms * sizeof(double));
}
