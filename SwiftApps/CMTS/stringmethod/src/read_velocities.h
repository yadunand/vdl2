#ifndef _read_velocities_h
#define _read_velocities_h

#include "md_ld_common_d_dimension_namd.h"

int Read_velocities(FR_DAT *fr, char *input_filename);

#endif
