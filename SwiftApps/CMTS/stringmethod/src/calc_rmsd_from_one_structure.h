#ifndef _calc_rmsd_from_one_structure_h
#define _calc_rmsd_from_one_structure_h

#include "md_ld_common_d_dimension_namd.h"

double Calc_rmsd(FR_DAT *fr, FR_DAT *fr_fixed_state);

#endif
