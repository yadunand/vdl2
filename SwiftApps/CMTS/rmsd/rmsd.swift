type file;

app (file o, file e, file dcdf, file datf) rmsd (float b1_val, float b2_val, string b1_name, string b2_name, file namd_input_file, file namd_params_file, file vmd_script_file, file psf_file, file pdb_file)
{
  rmsd b1_val b2_val b1_name b2_name @namd_input_file @namd_params_file @vmd_script_file stdout=@o stderr=@e;
}

app (file plot_pd_stdout, file plot_pd_output) plot_pd (file[float][float] datfs)
{
   plot_pd stdout=@plot_pd_stdout;
}

# Bead names
string b1name = "PX";
string b2name = "DT";

# Bead ranges
float b1min=0.0;
float b1max=1.0;
float b2min=0.0;
float b2max=1.0;

# Delta
float delta=0.5;

# Files
file namd_params <"par_cg_clath.inp">;
file namd_input <"cg_clath_cage.conf">;
file vmd_script <"rmsd.temp.tcl">;
file psf <"cg_clath_cage.psf">;
file pdb <"cg_clath_cage.pdb">;
file[float][float] dats;

foreach b1 in [b1min:b1max:delta] {
   foreach b2 in [b2min:b2max:delta] {
      file output <single_file_mapper; file=@strcat("logs/rmsd-", b1, "-", b2, ".out.txt")>;
      file error <single_file_mapper; file=@strcat("logs/rmsd-", b1, "-", b2, ".err.txt")>;
      file dcd <single_file_mapper; file=@strcat("output/cg_clath_cage-", b1, "-", b2, ".dcd")>;
      file dat <single_file_mapper; file=@strcat("output/rmsd.", b1name, b1, "_", b2name, b2, ".dat")>;
      dats[b1][b2] = dat;
      (output, error, dcd, dat) = rmsd(b1, b2, b1name, b2name, namd_input, namd_params, vmd_script, psf, pdb);
   }
}

# Post processing analysis
file plot_pd_stdout <"output/plot_pd.txt">;
file plot_pd_output <"output/rmsd_pd.dat">;
(plot_pd_stdout, plot_pd_output) = plot_pd(dats);
