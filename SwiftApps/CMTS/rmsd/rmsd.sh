#!/bin/bash 

b1=$1
b2=$2
b1name=$3
b2name=$4
namd_input=$5
namd_parameters=$6
vmd_script=$7

echo Command line arguments: $@
echo

echo Hostname: $( hostname )
echo

echo File listing
echo ------------
find
echo

echo Running the following sed commands
echo sed -i -e s/BLAB1/$b1name$b1/ -e s/BLAB2/$b2name$b2/ -e s@__DIR__@$PWD@ $namd_input
echo sed -i -e s/BLAB1/$b1name$b1/ -e s/BLAB2/$b2name$b2/ -e s@__DIR__@$PWD@ $namd_input
echo sed -i -e s/HBEPS/-0.01/g -e s/PXEPS/-$b1/g -e s/DTEPS/-$b2/g -e s/AKEPS/-0.01/g -e s/LTEPS/-0.01/g $namd_parameters
sed -i -e s/BLAB1/$b1name$b1/g -e s/BLAB2/$b2name$b2/g -e s/B1/$b1/ -e s/B2/$b2/ $vmd_script
sed -i -e s/HBEPS/-0.01/g -e s/PXEPS/-$b1/g -e s/DTEPS/-$b2/g -e s/AKEPS/-0.01/g -e s/LTEPS/-0.01/g $namd_parameters
sed -i -e s/BLAB1/$b1name$b1/g -e s/BLAB2/$b2name$b2/g -e s/B1/$b1/ -e s/B2/$b2/ $vmd_script
echo Done with sed
echo

namd2=$( which namd2 )
echo Found namd2 at $namd2
echo About to run mpiexec -machinefile $PBS_NODEFILE $namd2 $namd_input 
mpiexec -machinefile $PBS_NODEFILE $namd2 $namd_input 

echo
echo About to run vmd -dispdev text -e $vmd_script 
vmd -dispdev text -e $vmd_script 

mv output/cg_clath_cage.dcd output/cg_clath_cage-$b1-$b2.dcd
mv rmsd.dat output/rmsd.$b1name${b1}_$b2name${b2}.dat

echo
echo Files at end of run
find
echo
