count=$(head -1 counter.txt);
expr $count + 1 > counter.txt
#echo $count
mkdir run$count
cp RunDSSAT.swift run$count/
cp gridList.txt run$count/
cp sites.xml run$count/
cp beagle-coaster-trunk.xml run$count/
cp tc run$count/
cp cf run$count/
cd run$count
#time swift -tc.file tc -sites.file sites.xml -config cf  RunDSSAT.swift >& swift.out
SWIFT_HEAP_MAX=7000M time swift -tc.file tc -sites.file beagle-coaster-trunk.xml -config cf  RunDSSAT.swift >& swift.out

