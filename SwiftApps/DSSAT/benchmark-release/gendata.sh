#!/bin/bash

cd campaigns/dummy100000
echo Generating data, please wait..

for count in $( seq -w 0000001 0099999 )
do
    ln -s 0000000 $count
done

echo Done
