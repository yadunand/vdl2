#!/usr/bin/perl

use strict;

my $count=1;
chdir("campaigns/dummy100000") || die "Unable to chdir to campaigns/dummy100000";

while($count < 100000)
{
   my $pcount = sprintf("%07d", $count);
   system("ln -s 0000000 $pcount");   
   $count++;
}
