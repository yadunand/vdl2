#!/bin/bash -x

crash()
{
    MSG=$1
    echo ${MSG}  >&2
    exit 1
}

launchDirectory=$PWD
echo Viewing first input
echo Arguments are $@

# Verify WN_TMP is defined
if [ -z "$OSG_WN_TMP" ]; then
   crash "OSG_WN_TMP is undefined"
fi

# Verify engage subdirectory of OSG_WN_TMP
mkdir -p $OSG_WN_TMP/engage
if [ ! -d "$OSG_WN_TMP/engage" ]; then
   crash "OSG_WN_TMP/engage does not exist"
fi

tmpDirectory=`mktemp -d $OSG_WN_TMP/engage/SciColSim.XXXXXX`
cd $tmpDirectory

# Retrieve software
wget http://www.ci.uchicago.edu/~davidk/DSSAT/DSSAT.tgz
tar xvfz ./DSSAT.tgz

# Run evolve
cd DSSAT/data
./DSSAT/bin/RunDSSAT.sh "$@"

# Finish
cd $launchDirectory
rm -rf $tmpDirectory
