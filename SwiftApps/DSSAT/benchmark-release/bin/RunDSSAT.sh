#!/bin/bash

dssatdir=/dev/shm/dssat
dssattmp=/dev/shm/dssat.tmp

out=$1
indata=$2
refdata=$3
campaign=$4
binpath=$5

if [ ! -d $dssatdir ]; then
  if mkdir $dssattmp 2>/dev/null; then
    echo $$ copying
    savewd=$PWD
    cd $dssattmp

    mkdir -p refdata
    cp $refdata/* refdata/

    mkdir -p campaign
    cp $campaign/*.MZX campaign/
    
    mkdir -p binpath
    cp $binpath/*.EXE binpath/

    cd $savewd
    mv $dssattmp $dssatdir
  else
    while [ ! -d $dssatdir ]; do
      echo $$ sleeping
      sleep 1;
    done
  fi
fi

mkdir -p data

ln -s $indata/* data
ln -s $dssatdir/refdata/* data
ln -s $dssatdir/campaign/*.MZX data
ln -s $dssatdir/binpath/*.EXE .

# Run 
cd data
../DSSAT040.EXE A X1234567.MZX > RESULT.OUT 2>&1
exit_status=$?
cd ..

# Tar
mkdir -p output
cp data/*.OUT output
tar cf $out output

if [ "$exit_status" -ne 0 ]; then
    echo $2 | awk '{ print $1 }' >> FailedList.txt
fi

exit

