#!/bin/bash

rm -rf run
mkdir run
cd run
mkdir data

out=$1
indata=$2
refdata=$3
campaign=$4
binpath=$5

#copy files of grid cell into data directory
for i in $2
do
  cp ../$i data
#echo "commented"
done

#cp -v $2/* data

cp  $refdata/* data
cp  $campaign/H1234567.MZX data
cp  $binpath/*.EXE .

cd data

../DSSAT040.EXE A H1234567.MZX > RESULT.OUT
exit_status=$?

cd ..
mkdir output

cp data/*.OUT output

tar cf ../$out output

if [ "$exit_status" -ne 0 ]; then
    echo $2 | awk '{ print $1 }' >> FailedList.txt
fi

exit

