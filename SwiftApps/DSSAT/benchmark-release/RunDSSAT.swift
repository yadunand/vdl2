type file;

app (file output) RunDSSAT (string input_directory, string refdatapath, string campaignpath, string binpath)
{
  RunDSSAT @output input_directory refdatapath campaignpath binpath;
}

//This is unused at the moment since it was used to check bad cells (bad cell means that the cell doesn't have soil files)
app (file output) gridCheck (string path)
{
  gridCheck @output path;
}

string gridLists[] = readData("gridList.txt");

foreach g,i in gridLists{
 file out <single_file_mapper; file=@strcat("output/", gridLists[i], "output.tar")>;
 string input_directory = @strcat(@arg("cmppath"), "/", gridLists[i]);
 out = RunDSSAT(input_directory, @arg("refdata"), @arg("campaign"), @arg("bindata"));
}

