type file;

app (file output) RunDSSAT (file input[], string _refdatapath, string _campaignpath, string _binpath)
{
  RunDSSAT @output @input _refdatapath _campaignpath _binpath;
}

//This is unused at the moment since it was used to check bad cells (bad cell means that the cell doesn't have soil files)
app (file output) gridCheck (string path)
{
  gridCheck @output path;
}

string gridLists[] = readData("gridList.txt");

//string campaign="grid_ccsm3_a2_bcsd";

foreach g,i in gridLists{
 file in1[] <filesys_mapper; location=@strcat(@arg("cmppath","/scratch/ketan/dssat/data/grid_ccsm3_a2_bcsd"), "/", gridLists[i]), pattern="*">;
 file out<single_file_mapper; file=@strcat("output/", gridLists[i], "output.tar")>;
 out = RunDSSAT(in1, @arg("refdata","/home/ketan/see/DSSAT/campaigns/common"), @arg("campaign", "/home/ketan/see/DSSAT/campaigns/GCM/ccsm3_a2_bcsd"), @arg("bindata","/home/ketan/see/DSSAT/bin"));

}


