type file;

app (file output) RunDSSAT (file input[])
{
  RunDSSAT @output @input ;
}

//This is unused at the moment since it was used to check bad cells (bad cell means that the cell doesn't have soil files)
app (file output) gridCheck (string path)
{
  gridCheck @output path;
}

//gridList.txt contains the list of good grid cells
//file gl <"gridList.txt">;

//gridCheck app discards empty grid cells or those containing unusable files 
//gl = gridCheck(@arg("path","/gpfs/pads/projects/CI-SES000031/data/dssat/grid/"));

string gridLists[] = readData("gridList.txt");

foreach g,i in gridLists
{
//file out<single_file_mapper;file=@strcat("output/",gridLists[i],"output.tar")>;
file out<concurrent_mapper;prefix="output", suffix=".tar">;

file in1[] <filesys_mapper;location=@strcat(@arg("path","/gpfs/pads/projects/CI-SES000031/data/dssat/grid/"), gridLists[i]), pattern="*">;

out = RunDSSAT(in1);
}

