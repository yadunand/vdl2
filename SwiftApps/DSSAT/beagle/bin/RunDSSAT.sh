#!/bin/bash

rm -rf run
mkdir run
cd run
mkdir data
out=$1

#copy files of grid cell into data directory
for i in $2
do
  cp ../$i data;
done
                                              
cp /gpfs/pads/projects/CI-SES000031/csm/sim/* data          
cp /gpfs/pads/projects/CI-SES000031/csm/*.CDE data

cp /gpfs/pads/projects/CI-SES000031/beagle/*.EXE .
cd data                                                             


#../DSSAT040.EXE A H1234567.MZX > RESULT.OUT
#strace -o strace.txt ../DSSAT040.EXE A X1234567.MZX > RESULT.OUT
../DSSAT040.EXE A X1234567.MZX > RESULT.OUT
#../DSSAT040.EXE A $3 > RESULT.OUT

cd ..
mkdir output

cp data/*.OUT output
cp data/strace.txt output
cp data/RESULT.OUT output

tar cf ../$out output

exit

