type file;

app (file fig, file sig, file log) episnp (file wrapper, file exec, file trait, file chroms, int nSingles, int nPairs)
{
  sh @wrapper @exec @trait @chroms nSingles nPairs @log @fig @sig;
}

file epiwrapper<"epiwrap.sh">;
file epiexec<"EPISNP1v41.gz">;
file epitrait <"trait.dat">;
file epichroms<"chroms.tgz">;

file logout[]<simple_mapper; location="output", prefix="episnp.",suffix=".out">;
file figout[]<simple_mapper; location="output", prefix="single_locus_fig.",suffix=".out">;
file sigout[]<simple_mapper; location="output", prefix="single_locus_sig.",suffix=".out">;

int nRuns =       @toInt(@arg("nRuns","1"));
int initSingles = @toInt(@arg("initSingles","1000"));
int initPairs =   @toInt(@arg("initPairs","1000"));

foreach incr, i in [0:nRuns-1] {
  (figout[i], sigout[i], logout[i]) = episnp(epiwrapper, epiexec, epitrait, epichroms, initSingles+i, initPairs+i);
}
