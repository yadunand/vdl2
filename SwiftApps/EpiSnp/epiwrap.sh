#! /bin/sh

# called from swift app cmd: sh @wrapper @exec @trait @chroms nSingles nPairs @epiout @fig @sig;

exec=$1
trait=$2
chroms=$3
nSingles=$4
nPairs=$5
epiout=$6
figout=$7
sigout=$8

echo $0: exec=$exec trait=$trait chroms=$chroms nSingles=$nSingles nPairs=$nPairs figout=$figout sigout=$sigout

cp $exec episnp.gz
gunzip episnp
chmod +x episnp

tar zxf $chroms

# FIXME: make # chrNN.dat files below depend on # files in input tarball

cat >parameter.dat <<END
0
1  # number of traits
6  # starting position of traits in the trait file
4  1  2  # number of column for gender information and codes for male and female
9999  # code for missing traits, non-genetic factors and covariables
1654 # number of individuals
0  # number of non-genetic factors
0  # positions of the non-genetic factors
0  # number of covariables
0  # positions of covariables in the trait file
6  # number of chromosomes
30  # sex chromosome number
1  # 1: 0=A1A1, 1=A1A2, 2=A2A2 and others=missing used for coding; 2: A1/A1, A1/A2 and A2/A2 used for coding, 0/0 used for missing
2  # starting position of SNPs in the SNP data file
1500 chr01.dat
1200 chr02.dat
1300 chr03.dat
1400 chr04.dat
1500 chr05.dat
1600 chr06.dat
trait.dat  # file name of the phenotype data
$nSingles  # number of most significant results for single SNP tests to be printed in the output file
$nPairs  # number of most significant results for pairwise tests to be printed in the output file
END

./episnp >& episnp.out

mv episnp.out $epiout
mv single_locus_fig.out $figout
mv single_locus_sig.out $sigout


