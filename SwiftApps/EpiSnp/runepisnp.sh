#! /bin/sh

site=${1:-local}

swift -config cf.$site -tc.file tc -sites.file $site.xml episnp.swift -nRuns=${2:-1} -initSingles=${3:-1000} -initPairs=${4:-10000}
