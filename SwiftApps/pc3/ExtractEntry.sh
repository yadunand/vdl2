#!/bin/bash
INPUT=$1
INTEGER=$2
OUTPUT=$3
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" > $OUTPUT 
echo "<java version=\"1.6.0_12\" class=\"java.beans.XMLDecoder\">" >> $OUTPUT 
 
xpath -q -e "/java/object/void[$INTEGER]/object" $INPUT >> $OUTPUT

echo "</java>" >> $OUTPUT