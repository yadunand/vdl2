#!/bin/bash

rm -rfv /var/tmp/derby-pc3/

for ID in J062941 J062942 J062943 J062944 J062945 ;  do

  echo Swift run for $ID

cat > oc5CSVRootPathInput.xml << UUUUUUU
<?xml version="1.0" encoding="UTF-8"?> 
<java version="1.6.0_03" class="java.beans.XMLDecoder"> 
 <string>/Users/benc/work/swift-svn.git/SwiftApps/pc3/PC3/SampleData/$ID</string> 
</java> 
UUUUUUU

cat > oc5JobIDInput.xml << UUUUUU
<?xml version="1.0" encoding="UTF-8"?> 
<java version="1.6.0_03" class="java.beans.XMLDecoder"> 
 <string>$ID</string> 
</java>
UUUUUU

  swift -tc.file tc.data.append pc3.swift -csvpath=oc5CSVRootPathInput.xml -jobid=oc5JobIDInput.xml


done

