type xmlfile;
type textfile;

(external out) checkvalid(boolean b) {
  if(!b) { stop(); }
}

(xmlfile output) ps_load_executable(xmlfile input, string s) {
  app {
    ps_load_executable_app @input s @output;
  }
}

(xmlfile output) ps_load_executable_threaded(xmlfile input, string s, external thread) {
  app {
    ps_load_executable_app @input s @output;
  }
}


(textfile output) parse_xml_boolean_value(xmlfile input) {
  app {
    parse_xml_boolean_value_app @input @output;
  }
}

(boolean output) extract_boolean(xmlfile input) {
  textfile text_out = parse_xml_boolean_value(input);
  output = readData(text_out);
}

(boolean output) ps_load_executable_boolean(xmlfile input, string s) {
  xmlfile xml_out = ps_load_executable(input, s);
  output = extract_boolean(xml_out);
}

(boolean output) is_csv_ready_file_exists(xmlfile input) {
  output = ps_load_executable_boolean(input, "IsCSVReadyFileExists");
}

(xmlfile output) read_csv_ready_file(xmlfile input) {
  output = ps_load_executable(input, "ReadCSVReadyFile");
}

(boolean output) is_match_csv_file_tables(xmlfile input) {
  string fn = readData(input);
  output = ps_load_executable_boolean(input, "IsMatchCSVFileTables");
}

// TODO I think this output never gets used?
(xmlfile output, external outthread) create_empty_load_db(xmlfile input) {
  output = ps_load_executable(input, "CreateEmptyLoadDB");
}

(textfile output) count_entries(xmlfile input) {
  app {
    count_entries_app @input @output;
  }
}

(xmlfile output) extract_entry(xmlfile input, int i) {
  app {
    extract_entry_app @input i @output;
  }
}

(boolean output) is_exists_csv_file(xmlfile input) {
  output = ps_load_executable_boolean(input, "IsExistsCSVFile");  
}

(xmlfile output) read_csv_file_column_names(xmlfile input, external thread) {
  output = ps_load_executable_threaded(input, "ReadCSVFileColumnNames", thread);
}

(boolean output) is_match_csv_file_column_names(xmlfile input) {
  output =  ps_load_executable_boolean(input, "IsMatchCSVFileColumnNames");
}

(xmlfile output) ps_load_executable_db (xmlfile db, xmlfile input, string s, external thread) {
  app {
    ps_load_executable_db_app @db @input s @output;
  }
}

(boolean output) ps_load_executable_db_boolean(xmlfile db, xmlfile input, string s, external thread) {
  xmlfile xml_out = ps_load_executable_db(db, input, s, thread);
  output = extract_boolean(xml_out);
}

(boolean output) load_csv_file_into_table(xmlfile db, xmlfile input, external thread) {
  output = ps_load_executable_db_boolean(db, input, "LoadCSVFileIntoTable", thread);
}

(boolean output) update_computed_columns(xmlfile db, xmlfile input, external thread) {
  output = ps_load_executable_db_boolean(db, input, "UpdateComputedColumns", thread);
}

(boolean output) is_match_table_row_count(xmlfile db, xmlfile input, external thread) {
  output = ps_load_executable_db_boolean(db, input, "IsMatchTableRowCount", thread);
}

(boolean output) is_match_table_column_ranges(xmlfile db, xmlfile input, external thread) {
  string dbcontent = readData(db);
  string inputcontent = readData(input);
  output = ps_load_executable_db_boolean(db, input, "IsMatchTableColumnRanges", thread);
}

// TODO what is the input used for here?
compact_database (xmlfile input, external thread) {
  app {
    compact_database_app @input;
  }
}

stop() {
  app {
    stop_app;
  }
}
string csv_root_path_input_arg = @arg("csvpath");
string job_id_arg = @arg("jobid");
string job_id_content = readData(job_id_arg);
xmlfile csv_root_path_input <single_file_mapper;file=csv_root_path_input_arg>; 
xmlfile job_id <single_file_mapper;file=job_id_arg>; 
boolean  is_csv_ready_file_exists_output;
xmlfile  read_csv_ready_file_output;
boolean is_match_csv_file_tables_output;
xmlfile create_empty_load_db_output;
textfile count_entries_output;
int entries;
xmlfile split_list_output[];

// check that the batch directory and manifest are ready
is_csv_ready_file_exists_output = is_csv_ready_file_exists(csv_root_path_input);

// read_csv_ready_file_output will contain a list of csv file entries,
// with each containing the path of a csv, a path to a header file,
// the rowcount for that file, the target table name, and a checksum
read_csv_ready_file_output = read_csv_ready_file(csv_root_path_input);

// check that for each csv file entry, we have a corresponding csv file
is_match_csv_file_tables_output = is_match_csv_file_tables(read_csv_ready_file_output);
if(is_match_csv_file_tables_output) {

    // These variables are used to sequence database accesses in a dataflow
    // style, rather than using imperative sequencing operations. With a
    // database that allowed parallel accesses, we would not need to have
    // such tight sequencing, though we would still need some.
    external db_over_time[];    
    external dbinit; // some bug in Swift static analysis means can't use db_over_time for initial one


    // create_empty_load_db_output contains a reference to the
    // newly created database
    (create_empty_load_db_output, dbinit) = create_empty_load_db(job_id);

    // count the entries in read_csv_ready_file_output
    // TODO could this table be read into a Swift struct array?
    count_entries_output = count_entries(read_csv_ready_file_output);
    entries = readData(count_entries_output);
    int entries_seq[] = [1:entries];
   
    // splitlistoutput[] will contain one element for each entry in
    // read_csv_ready_file_output 
    foreach i in entries_seq {
      split_list_output[i] = extract_entry(read_csv_ready_file_output, i);
    }    

// TODO this can merge with merge with above foreach, and split_list_output
// does not need to be an array then
 
    foreach i in entries_seq {
      boolean is_exists_csv_file_output;
      xmlfile read_csv_file_column_names_output;
      boolean is_match_csv_file_column_names_output;
      boolean load_csv_file_into_table_output;
      boolean update_computed_columns_output;
      boolean is_match_table_row_count_output;
      boolean is_match_table_column_ranges_output;

      // check that the data files exist, and use an external to
      // sequence this before read_csv_file_column_names
      is_exists_csv_file_output = is_exists_csv_file(split_list_output[i]);
      external thread6 = checkvalid(is_exists_csv_file_output);

      // update the CSVFileEntry with column names read from the header file
      // (which is itself listed in the CSVFileEntry)
      read_csv_file_column_names_output = read_csv_file_column_names(split_list_output[i], thread6);

      // check that the table column names match the csv column names
      // TODO where is the database reference passed in? is there enough
      // information in the CSVFileEntry to determine that? there is a
      // table name at least... I guess I should look inside
      is_match_csv_file_column_names_output = is_match_csv_file_column_names(read_csv_file_column_names_output);
      external thread2 = checkvalid(is_match_csv_file_column_names_output);

      // explicit external-based sequencing between checking table validity and
      // loading into the DB

      if(i==1) { // first element... see above note about not being
                 // able to use db_over_time[1] for this.
        load_csv_file_into_table_output = load_csv_file_into_table(create_empty_load_db_output, read_csv_file_column_names_output, dbinit);
      } else {
	     load_csv_file_into_table_output = load_csv_file_into_table(create_empty_load_db_output, read_csv_file_column_names_output, db_over_time[i]);
      }
      external thread3=checkvalid(load_csv_file_into_table_output);

      // explicitly sequence the database load and the computed-column-update

      // update the computed columns, and fail the workflow if this fails
      update_computed_columns_output = update_computed_columns(create_empty_load_db_output, read_csv_file_column_names_output, thread3);
      external thread4 = checkvalid(update_computed_columns_output);


      // now check that we loaded in the right number of rows, and ranges
      // TODO With a parallelised database, we could do these two checks in
      // parallel, I think. But for now, we use 'thread1' to sequence them
      // explicitly

      is_match_table_row_count_output = is_match_table_row_count(create_empty_load_db_output, read_csv_file_column_names_output, thread4);
      external thread1 = checkvalid(is_match_table_row_count_output);
      is_match_table_column_ranges_output = is_match_table_column_ranges(create_empty_load_db_output, read_csv_file_column_names_output, thread1);
      db_over_time[i+1] = checkvalid(is_match_table_column_ranges_output);

    }

    // TODO if we did this in a more parallel fashion, could wait for the
    // whole db_over_time array to be closed...
    compact_database(create_empty_load_db_output, db_over_time[entries+1]);
}
else {
    stop();
}
