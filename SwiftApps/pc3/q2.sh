#!/bin/bash

# this will get all the process IDs for IsMatchTableColumnRanges calls...
# select execute_id from invocation_procedure_names where procedure_name='is_match_table_column_ranges';

# need to do two things:

# augment with a success/failure value

#  select * from processes where id like 'tag:benc@ci.uchicago.edu,2008:swiftlogs:execute:pc3-20090429-1456-ssftbvsa:0-5-5-3-11-%';
# this will get the compound and execute (but not operator) subprocesses
# of a compound process
# we can probably ask if there is a sub-process with 'stop' in it...
# although a more positive measure of success might be better
# should generalise process containment...


# augment with the table name
# perhaps the workflow needs to bring more of the parameter structure into
# swiftscript rather than dealing with opaque files

echo " select processes.id, dataset_usage.dataset_id, dataset_usage.param_name, dataset_values.value from processes,invocation_procedure_names,dataset_usage, dataset_values where type='compound' and processes.id = invocation_procedure_names.execute_id and procedure_name='is_match_table_column_ranges' and dataset_usage.process_id = processes.id and dataset_usage.direction='O' and dataset_usage.param_name='inputcontent' and dataset_usage.dataset_id = dataset_values.dataset_id;" | sqlite3 provdb

