type file;
type exe;

app (file _o) compute_pi (exe _pi)
{
  runpi @_pi stdout=@_o;
}

app (file _o) avg_pi (exe _avg, file[] _i)
{
  bash @_avg @_i stdout=@_o; 
}

exe avg<"avg">;
exe pi<"pi">;

file pival<"ans.txt">;
file out[]<simple_mapper; location="outdir", prefix="pi.",suffix=".out">;
foreach i in [0:9] {
    out[i] = compute_pi(pi);
}

pival=avg_pi(avg, out);

