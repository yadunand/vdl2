Running Swift on Cetus and Tukey
=================================

Introduction
------------

This tutorial will walk you through running Swift applications over ALCF Cetus and Tukey systems. The motivation behind this tutorial is to enable use-cases on ALCF systems which need systems such as Mira and Cetus for the analysis and processing phase of their application and systems such as Tukey for postprocessing and visualization phase. The tutorial describes a technique to run both these phases across the two (or more) systems in a seamless manner (that is, without user intervention for authentication purposes). 

Copy Tarball
-------------

Copy the tarball to your home area on Cetus:

----
cp ~ketan/public/cetustukeytut.tgz $HOME
----

followed by:

----
tar -zxf cetustukeytut.tgz
----

Create Tunnel
-------------

Open a new terminal and log into Cetus. Create an ssh tunnel between Cetus and Tukey:

----
cetus$ ssh -L 52000:127.0.0.1:52000 tukey.alcf.anl.gov -N -f
----

Start Service
--------------

Open a new terminal and log into Tukey. Start the coaster service on Tukey:

----
tukey$ /home/ketan/swift-k/dist/swift-svn/bin/coaster-service -p 52000 -nosec
----

Systems Interaction
--------------------
The following figure gives an overview of systems interactions in the setup:

image:cetustukey.png[]

Swift Application
------------------
The Swift application included in this package computes the value of pi using montecarlo method. An MPI based C code computes the value of pi. This code is invoked multiple times in parallel. Each invocation writes the value it computed in an output file. A next application, implemented as a shell script that computes the average of the values found in the collection of output files generated in previous runs. The Swift source code for the applications is as below:

[source,C]
----
include::pi.swift[]
----

Configuration
--------------

The run is controlled by a configuration file called ct.conf which is as follows:

[source,json]
----
include::ct.conf[]
----

Run Swift
----------

Run Swift on Cetus:

----
cetus$ cd $HOME/cetustukeytut
cetus$ ./runct
----

Results
-------

On successful completion, the answer, an approximation of pi should be in the produced file +ans.txt+:

----
cetus$ cat ans.txt
----

Upcoming Features
------------------
. Extend to other systems.
. Show use of gridftp and/or Globus.org for file transfer.
