#include <fstream>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <ctime>
#include <algorithm>
#include <string>

#include <math.h>

#include <stdio.h>
#include <sys/param.h>
#include <sys/time.h>
#include <sys/types.h>
#include "unistd.h"

#include <fstream>
#include <stdlib.h>

//#define N 23000
#define N 10000

float Prob[N][N];
float Rank[N];
float Dist[N][N];

float alpha_i = 1.0;
float alpha_m = 1.0;
float beta = 1.0;
float gam = 1.0;
float delta = 1.0;
float k_max = 1.0;

float min(float a, float b)
{
  if(a<b) return(a); else return(b);
}

float max(float a, float b)
{
  if(a>b) return(a); else return(b);
}

void update(void){

    int i, j;
    float Summa = 0.;

printf("Loop 1\n");
    //#pragma omp parallel for private (j)
#pragma omp parallel for default(none) shared( Prob, alpha_i, alpha_m, beta, k_max, gam, delta, Rank, Dist) private(i,j)
    for( i=0; i<N-1; i++){
      for(  j=i+1; j<N; j++){

	float bg = 0.;

	Prob[i][j] = alpha_i*log(min(Rank[i]+1.,Rank[j]+1.)) +
	  alpha_m*log(max(Rank[i]+1.,Rank[j]+1.));
	if (Dist[i][j] > 0.){
	  float k = Dist[i][j];
	  if (k >= k_max){
	    k = k_max-1;
	  }
	  bg = beta * log(k/k_max) + gam * log(1. - k/k_max);
	} else {
	  bg = delta;
	}
	Prob[i][j] = exp(Prob[i][j] + bg);
      }
    }

printf("Loop 2\n");
    #pragma omp parallel for private(i)
    for(i=0; i<N-1; i++){
      for(j=i+1; j<N; j++){
	Summa += Prob[i][j];
      }
    }

printf("Loop 3\n");
    #pragma omp parallel for private(i)
    for( i=0; i<N-1; i++){
      for(  j=i+1; j<N; j++){
	Prob[i][j] /= Summa;
      }
    }
}

main()
{
  int i, j;
  for(i=0;i<N;i++) {
    Rank[i] = 1.0;
    for(j=0;j<N;j++) {
      Prob[i][j] = Dist[i][j] = 1.0;
    }
  }

  for(;;) {
   printf("update\n");
   update();	
  }
}
