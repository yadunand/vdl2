#! /bin/sh

fraction=${1:-1.0}

tmp=$(mktemp -t samplegraph.XXXX)

awk '

function crand()
{
    _cliff_seed = (100 * log(_cliff_seed)) % 1
    if (_cliff_seed < 0)
        _cliff_seed = - _cliff_seed
    return _cliff_seed
}
     
BEGIN { 
  getmsec="perl -MTime::HiRes=gettimeofday -e \"print int(1000*gettimeofday()).qq(\\n);\""
  getmsec="date +%N"
  getmsec | getline msec
  close(getmsec)
  _cliff_seed = (msec % 10000) / 10000
 nv=0
}

NR == 1 { }

(NR > 1) && (crand() < fraction) {
  if ( $1 in vertices ) {
     v1 = vertices[$1]
  }
  else {
     vertices[$1] = v1 = nv++;
  }
  if ( $2 in vertices ) {
     v2 = vertices[$2]
  }
  else {
     vertices[$2] = v2 = nv++;
  }
  print v1, v2  
}

END {print nv}

' fraction=$fraction < big_graph.v01.txt >$tmp

edges=$(wc -l <$tmp)
vertices=$(tail -1 $tmp)

echo $vertices $edges
sed -e '$d' <$tmp
rm $tmp
