#! /bin/sh

# Calculates the average loss, loss standard deviation, average running time, and average running time standard deviation
# of a set of loss files all produced by evolve.sh

# loss = average loss
# sdev = standard deviation of loss
# tavg = average timing data(name should be changed)
# tsdev = standard deviation of timing data

# process each file individually and treat each number as a field


logfile=$1; shift

outfile=$(mktemp sumloss.outfile.XXXXXX)

awk '

BEGIN { RS = ""; FS="\n";  loss = 0; loss_sq = 0; tavg = 0; tavg_sq = 0; n = 0; tn = 0; }

{
  for(i=1;i<NF;i++)
  {
#printf "%12.5f\n", $i
      sum_loss += $i
      sum_lossSQ += ($i*$i)
      nloss++
  }
  sum_time += $NF
  sum_timeSQ += ($NF*$NF)
  ntime++
}

END {

  avg_loss = sum_loss / nloss;
  avg_time = sum_time / ntime;

  var_loss = (sum_lossSQ - ((sum_loss*sum_loss)/nloss))/nloss
  var_time = (sum_timeSQ - ((sum_time*sum_time)/ntime))/ntime

  if (var_loss<0.0) var_loss=0.0; # handle floating point roundoff
  if (var_time<0.0) var_time=0.0; # handle floating point roundoff

  sdev_loss = sqrt(var_loss)
  sdev_time = sqrt(var_time)

  serr_loss = 2.0 * sdev_loss;
  serr_time = 2.0 * sdev_time;

#printf "sum_loss=%f sum_lossSQ=%f\navg_loss=%f var_loss=%f\nsdev_loss=%f serr_loss=%f\n", sum_loss, sum_lossSQ, avg_loss, var_loss, sdev_loss, serr_loss

  printf "loss sdev tavg tsdev\n" # header line must match Swift structure fields
  printf "%f %f %f %f\n", avg_loss, serr_loss, avg_time, serr_time

  if ( sdev_loss >50000 ) {
    printf("sumloss warning: high sdev_loss=%f\n", sdev_loss) > sdev_warning
  }
} ' $* 2>&1 | tee $outfile


if [ _$logfile != _ -a _$logfile != "_none" ]; then
( echo sumloss: $0 $*
  if [ -f sdev_warning ]; then
    cat sdve_warning
  fi
  echo
  for f in $*; do
    echo -e "\ninput file $f:\n"
    cat $f
  done
  echo -e "\nsumloss output: \n"
  cat $outfile

) >$logfile
fi

rm -f $outfile
