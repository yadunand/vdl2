#! /bin/sh

tarfile=boost_1_47_0.tar.gz
release=$(basename $tarfile .tar.gz)
boostrel=http://www.ci.uchicago.edu/~jonmon/SciColSim/$tarfile

if [ -d boost_1_47_0 ];
then
    echo "Boost directory already created!"
    exit 0
fi

echo
echo Downloading $release from $boostrel
echo

rm -rf $tarfile $release

if [ $(uname) = Linux ]; then
  wget  $boostrel
else
  curl $boostrel > $(basename $boostrel)
fi

echo
echo Extracting $release from $tarfile
echo

tar zxf $tarfile

echo
echo Boost installed at $(pwd)/$release
echo



