#! /usr/bin/env python
import os

# FULL for target in range(58,1009,50):
# FAST for target in range(58,209,50):
for target in range(58,59,50):
	s = ("%d" % target)
	print s

# FULL	for i in range(15):
# FAST	for i in range(2):
	for i in range(1):
		args="./toptimizer 0 0 4 50 -1 "+s+" 40000 20 1000 2 1 2. 0.01 100 0.3 2.3 1 1 0 0 0 # > out.T"+str(target)+".i"+str(i)
		args="OMP_NUM_THREADS=24 ./toptimizer 0 0 4 50 -1 "+s+" 40000 20 100 2 1 2. 0.01 2 0.3 2.3 1 1 0 0 0 # > out.T"+str(target)+".i"+str(i)
		args="./toptimizer 0 0 4 50 -1 "+s+" 40000 20 75    2 1 2. 0.01 2 0.3 2.3 1 1 1 0 0 m # > out.T"+str(target)+".i"+str(i)
		args="OMP_NUM_THREADS=24 ./toptimizer 0 0 4 50 -1 "+s+" 40000 20 96    2 1 2. 0.01 2 0.3 2.3 1 1 0 0 0 m 24 # > out.T"+str(target)+".i"+str(i)
		print("\n\n **** CALLING APP: "+args+"\n\n\n")
	        os.system(args);
#		print("\n\n **** CALLING APP: ./optimizer 0 0 4 50 -1 "+s+" 40000 20 10 2 1 2. 0.01 100 0.3 2.3 1 1 0 0 0\n\n\n")
#		FAST: os.system("./optimizer 0 0 4 50 -1 "+s+" 40000 20 10 2 1 2. 0.01 2 0.3 2.3 1 1 0 0 0")


print "Done!"
