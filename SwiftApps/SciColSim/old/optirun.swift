type file;

app (file outfile, file best, file max) optimize ( string args[], file graph )
{
  optimizersh @best @max args stdout=@outfile ;
}

int minrange=58;
int maxrange=1009;
//int maxrange=209;
int rangeinc=50;

int nreps=2; # 15

//      [alpha_i alpha_m beta gamma delta target_innov
//      [n_epochs n_steps n_reruns] [range]
//      [verbose_level]
//      [T_start T_end Annealing_steps Target_rejection Starting_jump]
//      [FREEZE_alpha_i FREEZE_alpha_m FREEZE_beta FREEZE_gamma FREEZE_delta]

file graph <"movie_graph.txt">;

foreach target in [minrange:maxrange:rangeinc] {
  foreach rep in [1:nreps] {
    file outfile <single_file_mapper; file=@strcat("output/T",target,".R",rep,".out")>;
    // file errfile <single_file_mapper; file=@strcat("output/T",target,".R",rep,".err")>;
    file bestfile <single_file_mapper; file=@strcat("output/T",target,".R",rep,".best_opt_some")>;
    file maxfile <single_file_mapper; file=@strcat("output/T",target,".R",rep,".max_dist")>;

    // string longargs[] = @strcat("0 0 4 50 -1 ",target," 40000 20 1000 2 1 2. 0.01 100 0.3 2.3 1 1 0 0 0");

    string fastargs1[] = ["0", "0", "4", "50", "-1", @strcat(target), "40000", "20", "1000", "2", "1", "2.", "0.01", "100", "0.3", "2.3", "1", "1", "0", "0", "0"];
    string fastargs2[] = [
       "0", "0", "4", "50", "-1", @strcat(target),
       "40000", "20", "1000", "2",
       "1",
       "2.", "0.01",  "5", "0.3", "2.3",
       "1", "1", "0", "0", "0"];
    (outfile, bestfile, maxfile) = optimize(fastargs2,graph);
  }
}
