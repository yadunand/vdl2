#!/bin/bash

# ./Runswift local to run on sandbox
# ./Runswift clustersmall to run on beagle pbs cluster at small scale
# ./Runswift clusterquick to run on beagle pbs cluster at large scale

escapecode=$(echo -n -e '\033')

count=$(head -1 counter.txt);
expr $count + 1 > counter.txt
mkdir run$count
cp /home/ketan/SciColSim/*.swift run$count/
cp /home/ketan/SciColSim/sites.beagle.xml run$count/
cp /home/ketan/SciColSim/sites.beagle.quick.xml run$count/
cp local.xml run$count/
cp /home/ketan/SciColSim/tc run$count/
cp /home/ketan/SciColSim/movie_graph.txt run$count/
cp /home/ketan/SciColSim/cf run$count/
cd run$count

if [ $1 = "local" ]
then
 #SWIFT_HEAP_MAX=7000M swift -tc.file tc -sites.file local.xml -config cf annealing.swift -e33="$escapecode" -nworkers=36 >& swift.out
 #Total jobs = 6 * 1 * 120/20 * 3 * 100 = 10,800
 SWIFT_HEAP_MAX=7000M swift -tc.file tc -sites.file local.xml -config cf annealing.swift -e33="$escapecode" -nworkers=6 -minrange=58 -maxrange=64 -rangeinc=1 -evoreruns=120 -nreps=1 -alphai=0 -alpham=0 -beta=4.0 -gamma=50.0 -delta=-1 -annealingcycles=100 -rerunsperapp=20  >& swift.out

elif [ $1 = "clusterbig" ]
then
 SWIFT_HEAP_MAX=7000M swift -tc.file tc -sites.file sites.beagle.xml -config cf annealing.swift -e33="$escapecode" -nworkers=24 -rangeinc=50 -evoreruns=960 -startingjump=2.3 -alphai=0 -alpham=0 -beta=4.0 -gamma=50.0 -delta=-1 -annealingcycles=100 -rerunsperapp=192  >& swift.out

elif [ $1 = "clustersmall" ]
then
 SWIFT_HEAP_MAX=7000M swift -tc.file tc -sites.file sites.beagle.xml -config cf annealing.swift \-e33="$escapecode" \
      >& swift.out

elif [ $1 = "clusterquick" ]
then
#target_innovation=(1009-58)/50=~20
#repeats=nreps=1
# 3 repeats constant (serial)
#annealing_cycles=100 (serial)
#rerunsperapp=192
#evoreruns=960
#J=evoreruns/rerunsperapp=960/192=5

#Total parallel jobs = (maxrange-minrange)/rangeinc * nreps * (evoreruns/rerunsperapp) = (1009-58)/50 * 1 * 960/192 = 20*5 = 100 Jobs = 2400 openmp jobs in parallel
 SWIFT_HEAP_MAX=7000M swift -tc.file tc -sites.file sites.beagle.quick.xml -config cf annealing.swift -e33="$escapecode" -nworkers=24 -minrange=58 -maxrange=1009 -rangeinc=50 -evoreruns=960 -nreps=1 -alphai=0 -alpham=0 -beta=4.0 -gamma=50.0 -delta=-1 -annealingcycles=100 -rerunsperapp=192  >& swift.out
fi

