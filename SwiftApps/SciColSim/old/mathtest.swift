import "math";

float a = 0.5;
tracef("random(): %f\n", random());
tracef("sin(%f): %f\n", a, sin(a));
tracef("jlog(%f): %f\n", a, jlog(a));
tracef("log10(%f): %f\n", a, log10(a));
tracef("exp(%f): %f\n", a, exp(a));
tracef("ceil(%f): %f\n", 1.23, ceil(1.23));
tracef("floor(%f): %f\n", 1.23, floor(1.23));
tracef("pow(%f,%f): %f\n", 2.0, 4.0, pow(2.0,4.0));
tracef("min(%f,%f): %f\n", 2.0, 4.0, min(2.0,4.0));
tracef("min(%f,%f): %f\n", 2.0, 1.5, min(2.0,1.5));





