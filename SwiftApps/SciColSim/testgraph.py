#! /usr/bin/env python 

import os, sys

unbuffered = os.fdopen(sys.stdout.fileno(), 'w', 0)
sys.stdout = unbuffered

app              = "./orig-optimizer";     # For Mac only: original code (+1 loop fix) using Grnd Central Dispatch
app              = "./dispatch-optimizer"; # For Mac only: sing Grand Central Dispatch
app              = "./openmp-optimizer";   # For Mac or Linux: Using OpenMP (Default)
app              = "./graphsim";           # For Mac or Linux: does graph simulation only - Using OpenMP (Default)

#paramset="default"
paramset="mw3"
#paramset="VERYFASTTEST"
#paramset="VERYFASTTEST"

startTarget = int(sys.argv[1])
print("Running with startTarget " + str(startTarget) + "\n\n");

print("Running with parameter set " + paramset + "\n\n");

if paramset == "beta":
  startTarget = 58
  endTarget = 59
  incrTarget = 50
  optimizerRepeats = 100
  evolveReruns = 32
  annealingSteps = 100
  NWorkers = "8"
  seed = ""
  openmp = "OMP_NUM_THREADS=" + NWorkers
  operation = "n"
  

elif paramset == "default":
  
  # Parameter names and optimizer command line positions
  
  startTarget      = 58    # target (target innovation)
  endTarget        = 1009  # "
  incrTarget       = 50    # "
  optimizerRepeats = 15    # wrapper control var (# of repeats of optimizer to do for each target loss)
  evolveReruns     = 1000  # n_reruns = 
  annealingSteps   = 100

# Following params are for modified optimizer openmp-optimizer

# NWorkers         = "2"
# operation        = "n"  # n=normal, m=manual (runs 1 multi_loss call)
# seed             = "1234567"
# openmp           = "OMP_NUM_THREADS=24"

  NWorkers         = ""
  operation        = ""
  seed             = ""
  openmp           = ""

elif paramset == "FULLSCALE":  # FULL INITIAL TEST PARAMETERS from Andrey
  
  startTarget      = 58
  endTarget        = 1009
  incrTarget       = 50
  optimizerRepeats = 15
  evolveReruns     = 1000
  annealingSteps   = 100

elif paramset == "FASTTEST":  # FAST TEST PARAMETERS by Mike
  
  startTarget      = 58
  endTarget        = 209
  incrTarget       = 50
  optimizerRepeats = 2
  evolveReruns     = 10
  annealingSteps   = 2
  NWorkers         = "2"
  openmp           = "OMP_NUM_THREADS=" + NWorkers
  operation        = "n"  # n=normal, m=manual (runs 1 multi_loss call)
  seed             = "1234567"

elif paramset == "VERYFASTTEST":  #VERY FAST TEST PARAMETERS by Ketan
  
  startTarget      = 58
  endTarget        = 209
  incrTarget       = 50
  optimizerRepeats = 2
  evolveReruns     = 1
  annealingSteps   = 2
  NWorkers         = "1"
  openmp           = "OMP_NUM_THREADS=" + NWorkers
  operation        = "n"  # n=normal, m=manual (runs 1 multi_loss call)
  seed             = "1234567"

elif paramset == "MANUALTEST":  # for manual (one multi_loss call) mode
  
  startTarget      = 258  # runtime increases with greater target
  endTarget        = 259
  incrTarget       = 50
  optimizerRepeats = 1
  evolveReruns     = 60
  annealingSteps   = 5
  NWorkers         = "4"
  openmp           = "OMP_NUM_THREADS=" + NWorkers
  operation        = "m"  # n=normal, m=manual (runs 1 multi_loss call)
  seed             = "1234567"

elif paramset == "mw":  # FAST TEST PARAMETERS by Mike
  
  startTarget      = 58
  endTarget        = 59
  incrTarget       = 50
  optimizerRepeats = 1
  evolveReruns     = 100
  annealingSteps   = 5
  NWorkers         = "4"
  openmp           = "OMP_NUM_THREADS=" + NWorkers
  operation        = "n"  # n=normal, m=manual (runs 1 multi_loss call)
  seed             = "1234567"
  app              = "./openmp-optimizer";

elif paramset == "mw2":  # Timing TEST PARAMETERS by Mike
  
  startTarget      = 58
  endTarget        = 209
  incrTarget       = 50
  optimizerRepeats = 1
  evolveReruns     = 240
  annealingSteps   = 1
  NWorkers         = "8"
  openmp           = "OMP_NUM_THREADS=" + NWorkers
  operation        = "m"  # n=normal, m=manual (runs 1 multi_loss call)
  seed             = "123456"
  app              = "./openmp-optimizer";

elif paramset == "mw3":  # Timing TEST PARAMETERS by Mike
  
  #  startTarget      = 9800
  endTarget        = startTarget+1
  incrTarget       = 50
  optimizerRepeats = 1
  # evolveReruns     = 1
  evolveReruns     = 1
  annealingSteps   = 1
  NWorkers         = "8"
  openmp           = "OMP_NUM_THREADS=" + NWorkers
  operation        = "m"  # n=normal, m=manual (runs 1 multi_loss call)
  seed             = "" # "123456"
  app              = "./openmp-optimizer";
  app              = "./graphsim";

# Ensure we dont pass new parameters to original optimizer versions
# (but they would be ignored)

if app == "./orig-optimizer":
  NWorkers = ""
  operation = ""
  seed = ""

# Run the test

for target in range(startTarget,endTarget,incrTarget):
  for i in range(optimizerRepeats):
    args = "rm -f bestdb.txt;" +  openmp +" " + "/usr/bin/time --verbose " + app + " 0 0 0 0 0 " + str(target) + " 40000 20 " + str(evolveReruns) + \
           " 2 2 2. 0.01 " + str(annealingSteps) + " 0.3 2.3 0 0 0 0 0 " + operation + " " + NWorkers + " " + seed + \
           " # >& out.T"+str(target)+".i"+str(i) + "; #mv bestdb.txt best.T" + str(target) + ".i" + str(i) 
    print("\n**** Calling optimizer: "+args+"\n")
    os.system(args);

print sys.argv[0] + " Done!"

#            openmp + " " + app + " 0 0 4 50 -1 " + str(target) + " 40000 20 " + str(evolveReruns) + \

###   Argument worksheet:

#       string fastargs1[] = [
#         "0", "0", "4", "50", "-1", @strcat(target),
#         "40000", "20", "1000", "2",
#         "1",
#         "2.", "0.01", "100", "0.3", "2.3",
#         "1", "1", "0", "0", "0"];
#       string fastargs2[] = [
#         "0", "0", "4", "50", "-1", @strcat(target),
#         "40000", "20", "1000", "2",
#         "1",
#         "2.", "0.01",  "5", "0.3", "2.3",
#         "1", "1", "0", "0", "0", "m"];
#       string fastargs3[] = [
#         "0", "0", "4", "50", "-1", @strcat(target),
#         "40000", "20", @strcat(repeats), "2",
#         "1",
#         "2.", "0.01",  "2", "0.3", "2.3",
#         "1", "1", "0", "0", "0", "m"];
# 
#
#   long running args = "0 0 4 50 -1 target | 40000 20 1000 | 2 | 1 | 2. 0.01 100 0.3 2.3 | 1 1 0 0 0 | m 24? 12345
# 
#   [ alpha_i alpha_m beta gamma delta target_innov ]
#     0       0       4    50    -1    target
#         
#   [ n_epochs n_steps n_reruns ]  [ range ] [ verbose_level]
#     40000    20      1000          2         1
#         
#   [ T_start T_end Annealing_steps Target_rejection Starting_jump ]
#     2.      0.01  100             0.3              2.3 
# 
#   [ FREEZE_alpha_i FREEZE_alpha_m FREEZE_beta FREEZE_gamma FREEZE_delta ]
#     1              1              0           0            0
# 
#   [ operation-code:(n,m) Nworkers seed ]
#     n                    24       12345 
# 

# From older test, using separate file per run:
#    args = app + " 0 0 4 50 -1 " + str(target) + " 40000 20 " + str(evolveReruns) + \
#      " 2 1 2. 0.01 " + str(annealingSteps) + " 0.3 2.3 1 1 0 0 0 > out.T"+str(target)+".i"+str(i)

### Optimization loop logic:

# a) 20 targets (parallel)
# b)   15 repeats (parallel) of optimizer (optimizer == multi_annealing):
#        1 multi_loss to initialize
# c)     100 Annealing_cycles (serial)
# d)        3 repeats (1 per non-fixed param, serial) of multi_loss:
# e)          1000 to 10000 evolve_reruns (parallel) == multi_loss
# f)            evolve()
