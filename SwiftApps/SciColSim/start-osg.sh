#!/bin/bash

#voms-proxy-init
mkdir -p condor
swift-0.93/bin/start-grid-service --loglevel DEBUG --throttle 9.99 --jobspernode 1 > start-grid-service.log 2>&1
sleep 15
#run-gwms-workers http://communicado.ci.uchicago.edu:$1 $2 > gwms-workers.log 2>&1 &
swift-0.93/bin/run-gwms-workers http://$( hostname -f ):$( cat service-0.wport ) $1 > gwms-workers.log 2>&1 &
mv sites.grid-ps.xml conf/grid.xml
rm service*
