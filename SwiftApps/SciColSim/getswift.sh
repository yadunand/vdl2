#!/bin/bash

swiftrel=http://www.ci.uchicago.edu/swift/packages/swift-0.93.tar.gz
tarfile=$(basename $swiftrel)
release=$(basename $swiftrel .tar.gz)

echo
echo Downloading $release from $swiftrel
echo

rm -rf $tarfile $release

if [ $(uname) = Linux ]; then
  wget $swiftrel
else
  curl $swiftrel > $(basename $swiftrel)
fi

echo
echo Extracting $release from $swiftrel
echo

tar zxf $tarfile

echo "Retrieving updated gensites command for mac compatability"
svn cat --revision 5636 https://svn.ci.uchicago.edu/svn/vdl2/trunk/bin/gensites > swift-0.93/bin/gensites
if [ "$?" != 0 ]; then
    echo "Failed to retrieve mac compatable gensites command, please update with the command"
    echo "svn cat --revision 5636 https://svn.ci.uchicago.edu/svn/vdl2/trunk/bin/gensites > swift-0.93/bin/gensites"
fi
echo "Retrieving updated swift command for mac compatability"
svn cat --revision 5690 https://svn.ci.uchicago.edu/svn/vdl2/trunk/bin/swift > swift-0.93/bin/swift
if [ "$?" != 0 ]; then
    echo "Failed to retrieve mac compatable swift command, please update with the command"
    echo "svn cat --revision 5636 https://svn.ci.uchicago.edu/svn/vdl2/trunk/bin/swift > swift-0.93/bin/swift"
fi

echo "Retrieving gridutils"
cd swift-0.93/bin
wget http://www.ci.uchicago.edu/~davidk/SciColSim/gridutils.tar.gz
tar xvfz gridutils.tar.gz
cd ../..

echo
echo Swift installed at $PWD/$release
echo

if [ -e "swift" ]; then
   rm -f swift # We expect this to be a symlink or non-existant
fi

ln -s $release swift

PATH=$PWD/swift/bin:$PATH
$PWD/swift/bin/swift -version
rm -f swift.log

echo
echo Swift installation complete.
echo

