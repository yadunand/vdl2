#! /bin/sh

vertices=${1:-100}
fraction=${2:-1.0}

tmp=$(mktemp -t trimgraph)

awk '
NR == 1 { }

(NR > 1) && ($1 < n) && ($2 < n) && (rand() < fraction) { print $1, $2 }
' n=$vertices fraction=$fraction < big_graph.v01.txt >$tmp

edges=$(wc -l <$tmp)

echo $vertices $edges
cat $tmp
rm $tmp




