#! /bin/sh

interval=120 # seconds between snapshots
statusdir=$HOME/public_html/scicol/status

mkdir -p $statusdir

if [ _$(basename $PWD | sed -e 's/run...$/run/') = _run ]; then
  runid=$(basename $PWD)
else
  runid=$(/bin/ls -1td run??? | head -1)
  cd $runid
fi

echo monitoring run id $runid

source ./params.annealing

min=$min_target_innovation
inc=$target_innovation_increment
max=$max_target_innovation

while true; do
  
  ../bin/convertbest.sh

  (echo status of run $PWD at $(date);
   echo -e "\nParameters:\n"
   cat paramfile
   echo -e "\nLatest status of all targets:\n"
   for s in $(seq $min $inc $max); do tail -1 best.T$s.R1.fmt; done
   echo -e "\nLatest results from each target:\n"
   more $(for s in $(seq $min $inc $max); do echo best.T$s.R1.fmt; done)
   echo -e "\nResults above are from files last changed at these times:\n"
   ls -lt best.*.txt
   echo -e "\nBeagle job status:\n"
   xtnodestat | grep $USER
  ) >$statusdir/$runid

  sleep $interval

done
