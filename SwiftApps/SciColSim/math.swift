
(float result) sin(float x)
{
   result = @java("java.lang.Math", "sin", x);
}

(float result) exp(float x)
{
   result = @java("java.lang.Math", "exp", x);
}

(float result) jlog(float x)
{
   result = @java("java.lang.Math", "log", x);
}

(float result) log10(float x)
{
   result = @java("java.lang.Math", "log10", x);
}

(float result) ceil(float x)
{
   result = @java("java.lang.Math", "ceil", x);
}

(float result) floor (float x)
{
   result = @java("java.lang.Math", "floor", x);
}

(float result) min (float a, float b)
{
   //result = @java("java.lang.Math", "min", a, b);
   if ( a < b ) {
       result = a;
   }
   else {
       result = b;
   }
   tracef("math/min: result=%f\n",result);
}

(float result) pow (float x, float y)
{
   result = @java("java.lang.Math", "pow", x, y);
}

(float result) random ()
{
  result = @java("java.lang.Math","random");
}

// Functions below are exprimental and do not work
// FIXME: fix 0.93 problems casting ints as doubles internally.
//  This breaks the @java interface for Swift ints.

(string result) itos (int i)
{
  result = @java("java.lang.Integer","toString",i);
}

(string result) itos2 (int i)
{
  result = @java("java.lang.String","valueOf",i);
}

(string result) ctos (int i)
{
  result = @java("java.lang.Character","valueOf",i);
}

(string result) format (string f, int c)
{
  result = @java("java.lang.String", "format", f, c );
}
