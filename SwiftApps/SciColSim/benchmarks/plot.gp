set terminal png enhanced size 1000,1000
#set style line 1 linecolor rgb "blue"
set output "`echo $PLOT_OUTPUT`"
set nokey
set xlabel "Target Innovation"
set ylabel "Time in Seconds"
set title "`basename $PLOT_OUTPUT .png`"
plot "results.txt" using 1:2 with linespoints
