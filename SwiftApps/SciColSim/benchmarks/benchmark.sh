#!/bin/bash -x

# Create run directory
ORIGDIR=$PWD
TIME_OUTPUT=$ORIGDIR/$3
MEM_OUTPUT=$ORIGDIR/$4

cd $5

# Clean any previous run
if [ -f "bestdb.txt" ]; then
   rm -f bestdb.txt
fi

# Run programs
export OMP_NUM_THREADS=1 
export WORK=$ORIGDIR
tmpfile=$( mktemp -p . )
tmpfile2=$( mktemp -p . )
/usr/bin/time --format='Memory: %R' -a -o $tmpfile bin/timeout $2 ./graphsim 0 0 0 0 0 $1 40000 20 1 2 1 2. 0.01 1 0.3 2.3 0 0 0 0 0 m 1 > $tmpfile2
memusage=$( grep Memory $tmpfile | awk '{print $2}' )
timing=$( grep time $tmpfile2 | awk '{print $5}' )
rm $tmpfile
rm $tmpfile2

# Write results
echo $1 $memusage > $MEM_OUTPUT
if [ -n "$timing" ]; then
   echo $1 $timing > $TIME_OUTPUT
else
   echo "$1 -1" > $TIME_OUTPUT
fi
