#!/bin/bash

# Report error and exit
crash()
{
   MSG=$1
   echo ${MSG} >&2
   exit 1
}

# Provide usage info
usage="Usage: $0 -site sitename -start startval -stop stopval -step stepval [-limit limitval]"

# Process command line arguments
while [ $# -gt 0 ]; do
  case $1 in
    -site) execsite=$2; shift 2;;
    -start) start=$2; shift 2;;
    -stop) stop=$2; shift 2;;
    -step) step=$2; shift 2;;
    -limit) limit=$2; shift 2;;
    *) echo $usage 1>&2
       exit 1;;
  esac
done

if [ -z "$execsite" ] || [ -z "$start" ] || [ -z "$stop" ] || [ -z "$step" ]; then
   crash "$usage"
fi


# Create run directory
rundir=$( echo run??? | sed -e 's/^.*run//' | awk '{ printf("run%03d\n", $1+1)}' )

if [ -d $rundir ];
then
    crash "$rundir already exists! exiting." >&2
else
    mkdir $rundir
fi

cd $rundir

# Copy needed files
cp ../benchmark.swift .
cp ../benchmark.sh .
cp ../../movie_graph.txt .
cp ../plot.sh .
cp ../plot.gp .

# Run gensites
export WORK=$PWD/swiftwork
if [ ! "$USE_SCS" == 1 ]; then
   cp ../conf/$execsite.cf cf
   SWIFT_HOME=../../swift/bin ../../swift/bin/gensites -p ../conf/$execsite.cf ../conf/$execsite.xml > sites.xml
fi

# Run swift 
scicoldir=$( dirname $( dirname $PWD ) )
command="swift -sites.file sites.xml -tc.file tc.data benchmark.swift -scicoldir=$scicoldir -start=$start -stop=$stop -step=$step"
if [ -n "$limit" ]; then
   command="$command -limit=$limit"
fi
$( echo $command )
