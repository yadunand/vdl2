type file;

# Perform a single benchmark
app (file timef, file memf, file errorf) benchmark (int target_innovation, int tl, string scidir)
{
  benchmark target_innovation tl @filename(timef) @filename(memf) scidir stderr=@errorf;
}

# Create a plot from an array of result files
app (file png) plot (file in1[], file in2[], file gpl)
{
   plot @filename(png);
}

int target_innovation_start=@toInt(@arg("start"));
int target_innovation_stop=@toInt(@arg("stop"));
int step=@toInt(@arg("step"));
int time_limit=@toInt(@arg("limit", "86300"));
string scicoldir=@arg("scicoldir");

# Run benchmark
file graph <"movie-graph.txt">;
file timeresults[];
file memresults[];

foreach ti,tidx in [target_innovation_start:target_innovation_stop:step] {
   file times <single_file_mapper; file=@strcat("times/times-", ti, ".out")>;
   file mem <single_file_mapper; file=@strcat("mem/mem-", ti, ".out")>;
   file error <single_file_mapper; file=@strcat("errors/errors-", ti, ".err")>;
   (times, mem, error) = benchmark(ti, time_limit, scicoldir);
   timeresults[tidx] = times;
   memresults[tidx] = mem;
}

file png <"results/plot.png">;
file gp <"plot.gp">;
png = plot(memresults, timeresults, gp);
