#!/bin/bash

export PLOT_OUTPUT=$1
export PLOT_TITLE=$( basename $( dirname $1 ))
find . -name *.out -exec cat {} \; | sort -n | grep -v -- "-1" > results.txt
gnuplot plot.gp
