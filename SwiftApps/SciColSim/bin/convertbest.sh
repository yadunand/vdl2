#! /bin/sh

dir=$(dirname $0)
for f in best*.txt; do
  out=$(basename $f .txt).fmt
  $dir/showbest.sh <$f >$out
done
