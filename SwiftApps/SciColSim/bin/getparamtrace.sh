#! /bin/sh

targetinno=${1:-58}
grep 'calling evolve' swift.out | grep ,$targetinno, | 
  sed -e 's/^.*\[//' \
      -e 's/\]$//' \
      -e 's/,/ /g' \
      -e 's/\(\......\)[0-9]* /\1 /g' |
  awk '{print $3, $4, $5}' | uniq 
