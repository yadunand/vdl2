#1 /bin/sh
bestfile=$1
maxfile=$2
datafile=$3
touch best_opt_some.txt max_dist.txt multi_loss.data
shift 3
$(dirname $0)/Optimizer $* 2>&1
mv best_opt_some.txt $bestfile
mv max_dist.txt $maxfile
mv multi_loss.data $datafile
