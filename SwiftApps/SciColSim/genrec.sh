#! /bin/sh

for f in $1/best.T*.R1.txt; do
  ti=$(echo $f | sed -e 's/^.*T//' -e 's/\..*//')
  (
    echo  "i j dx0 dx1 dx2 dx3 dx4 rej0 rej1 rej2 rej3 rej4 target_innov loss alpha_i alpha_m beta gamma delta sdev"
    cycle=$(sed -e 's/\]//g' -e 's/[,|\[]//g' $f | awk '$4 == 4 {print $3}' | tail -1)
    echo 1>&2 ti=$ti will restart at cycle $cycle
    sed -e 's/\]//g' -e 's/[,|\[]//g' $f |
      awk '
        $3 == cycle { dx[$4] = $5; rej[$4] = $6 } 

        $3 == cycle && $4 == 4 {
          print $3, $4,
                dx[0], dx[1], dx[2], dx[3], dx[4],
                rej[0], rej[1], rej[2], rej[3], rej[4],
                $7, $8, $9, $10, $11, $12, $13, $14
        }
      ' cycle=$cycle |
      tail -1
  ) > ckpt.$ti
  if [ $(wc -l <ckpt.$ti) != 2 ]; then
    echo $0: WARNING: ckpt.$ti was not generated: input does not contain at least one complete annealing cycle.
    rm -f ckpt.$ti
  fi
done

# N AvgTime  i  j     dx      Rej    TgtInno    AvgLoss     alpha_i   alpha_m      beta     gamma     delta     LossSdev
