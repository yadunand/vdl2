#include <fstream>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <ctime>
#include <algorithm>
#include <string>

#include <sys/param.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

#include <math.h>

using namespace std;

void set_max_loss(long long v, long long e, long long t) {

  // Based on formulas from random_analytical.pdf

  // t-=100;
  double E = e;
  double V = v;
  double T = t;

  double max_loss;
  double sum, ERLt, VarRLt, absVarRLt;

  // (9): E[RLt] = (1/t) * SUM(i=0 to T of: (V*(V-1) / (2 * (E-i))

  sum = 0;
  for(int i=0;i<t;i++) {
    sum += ((double)(v*(v-1))) / ((double)(2*(e-i)));
  }
  ERLt = (1.0/(double) t) * sum;

  // (12): Var[RLt] = 1 / (T**2) * SUM( i=0 to T of: ((V*(V-1))/2) - E + 1 ) / ((E-i)**2)

  sum = 0;
  for(int i=0;i<t;i++) {
    sum +=  
      ( (((double)(v*(v-1)))/ 2.0) - (double)e + (double)i )
      /  
      (double)((e-i)*(e-i))
      ;
    if ( (double)((e-i)*(e-i)) == 0.0 ) {
      printf( "zero denom: %d %d\n", e, i);
    }
  }
  int tmp = t*t;
  VarRLt = (1.0/(double)((t) * t)) * sum;
  if (VarRLt < 0.0) absVarRLt=0.0;
  else absVarRLt = VarRLt;

  // (9) + 3sigma = E[RLt] + 3 * SQRT(Var)
  max_loss = ERLt + (3.0 * sqrt(absVarRLt));
  printf("%12d %12.3f %12.8e %12.3f\n", t, ERLt, VarRLt, max_loss);
}

struct Graph {
  const char *n;
  int v;
  int e;
} G[] =

{
  "movie",    500,    1008,
  "big.001", 1857,    1312,
  "big.002", 3094,    2683,
  "big.005", 5405,    6776,
  "big.01",  7765,   13421,
  "big.10", 19281,  134594,
  "big.20", 23258,  269133,
  "big.30", 25484,  403401,
  "big.40", 26821,  537106,
  "big.50", 27774,  671179,
  "big.60", 28482,  804774,
  "big.70", 29055,  938061,
  "big.80", 29487, 1072415,
  "big.90", 29779, 1205741,
  "big",    30060, 1338753,   
  "",        0,     0
};

int main()
{
  int E, V, T;
  int i=0;
  while (G[i].v > 0) {
    cout << "\nGraph " << G[i].n << " V=" << G[i].v << " E=" << G[i].e << endl;
    printf("Pct          T          ERLt       VarRLt       max_loss\n");
    for(double p = 0.1; p<=1.0; p+=0.1) {
      printf("%.2f",p);
      set_max_loss(G[i].v, G[i].e, (int)((G[i].e * p)+0.5));
    }
    i++;
  }
}

