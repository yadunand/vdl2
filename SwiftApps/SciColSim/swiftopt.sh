#!/bin/bash

# Usage: swiftopt.sh [-s sitename] [-p paramfile] [-w] [-n # for dryrun ]
#
# NOTE: this command expects symlink "swift" in the cur dir to point
# to relese installed by setup.sh If you want to run with a different
# swift release, replace symlink "swift" with a link to your swift
# release dir.

usage="$0 [-s sitename] [-p paramfile] [-n] [-w] [-l] # -n for dryrun: just print params and time estimates"

# Function to run Swift
runswift() {
   SWIFT_HEAP_MAX=$ram SWIFT_LIB=.. swift >> swift.out 2>&1 -tc.file tc.data -sites.file $1 -config cf annealing.swift -e33="$escapecode" \
                                            \
   -minrange=$min_target_innovation         \
   -maxrange=$max_target_innovation         \
   -rangeinc=$target_innovation_increment   \
                                            \
   -nreps=$annealing_repeats                \
   -annealingcycles=$annealing_cycles       \
   -evoreruns=$evolve_reruns                \
                                            \
   -alphai=$alpha_i                         \
   -alpham=$alpha_m                         \
   -beta=$beta                              \
   -gamma=$gamma                            \
   -delta=$delta                            \
                                            \
   -startingjump=$starting_jump             \
                                            \
   -nworkers=$nworkers                      \
   -rerunsperapp=$reruns_per_opt_invocation \
   -wget=$wget				    \
   -longruns=$longruns                      \
   -restart=$restart
}


# Default settings
execsite=local
paramfile=Fast01
ram=6000M
dryrun=
escapecode=$(printf '\033')
variable_params=5     # Currently CONSTANT, will want to have this vary
wget=false
longruns=false

# Process command line arguments
while [ $# -gt 0 ]; do
  case $1 in
    -l) longruns=true; shift 1;; # FIXME
    -n) dryrun=true; shift 1;;
    -p) paramfile=$2; shift 2;;
    -s) execsite=$2; shift 2;;
    -w) wget=true; shift 1;;     # FIXME
    -r) resdir=$2; shift 2;;
    *) echo $usage 1>&2
       exit 1;;
  esac
done

# Create next unique run id and run directory
rundir=$( echo run??? | sed -e 's/^.*run//' | awk '{ printf("run%03d\n", $1+1)}' )

#Exit if rundir already exits. Something is funky
if [ -d $rundir ];
then
    echo "$rundir already exists! exiting." >&2
    exit 2
else
    mkdir $rundir
fi

if [ _$resdir != _ ]; then
  if [ -d resdir ]; then
    restart=true
    cp $resdir/ckpt.* $rundir
  else
    echo $0: no restart directory $resdir found.
    exit 1
  fi
else
  restart=false
fi

# Get optimization parameters
if [ ! -f params/$paramfile ];
then
    echo "Could not find parameter file $paramfile in params!"
    exit 1
fi
cp params/$paramfile $rundir/paramfile
sed -e '/^[[:space:]]*\(#.*\)*$/d' -e 's/#.*//' -e 's/  */=/' -e 's/^/export /' <params/$paramfile >$rundir/params.annealing
source $rundir/params.annealing
#swift=../swift/bin/swift # relative to runNNN/ dirs
echo Optimization $rundir: site=$execsite paramfile=$paramfile

# Report an error if configuration files are missing
if [ ! -f "conf/$execsite.xml" ] && [ ! -f "conf/$execsite.conf" ]; then
   echo Unable to find requested configuration file for site $execsite
   exit 1
fi

# Use start-coaster-service if site is a .conf file
if [ -f "conf/$execsite.conf" ]; then
   USE_SCS=1
fi

# Check for missing .cf files
if [ -f "conf/$execsite.xml" ] && [ ! -f "conf/$execsite.cf" ]; then
   echo Missing configuration file $execsite.cf
fi

cp movie_graph.txt $rundir
cp annealing.swift $rundir
cp colortext.swift $rundir
cp math.swift $rundir

# Echo parameters
echo Annealing parameters:
echo
cat $rundir/params.annealing
echo

# Echo runtime estimates
total_jobs=`python -c "from math import ceil; print int(ceil(($max_target_innovation.00 - $min_target_innovation.00)/$target_innovation_increment.00) * $annealing_repeats * $variable_params * $annealing_cycles * $evolve_reruns/$reruns_per_opt_invocation)"`
echo Total jobs = $total_jobs

cd $rundir

# Do the run
export WORK=$PWD/swiftwork
mkdir -p $PWD/swiftwork/workers

# Use start-coaster-service if the site uses a .conf file
if [ "$USE_SCS" == "1" ]; then
   cp ../conf/$execsite.conf coaster-service.conf
   cp ../conf/$execsite.cf cf
   sed -i -e "s@_RUNDIR_@$rundir@" coaster-service.conf
   start-coaster-service
fi

# Run gensites
if [ ! "$USE_SCS" == 1 ]; then
   cp ../conf/$execsite.cf cf
   # SWIFT_HOME=../swift/bin ../swift/bin/gensites -p ../conf/$execsite.cf ../conf/$execsite.xml > $execsite.xml
   gensites -p ../conf/$execsite.cf ../conf/$execsite.xml > $execsite.xml
fi

echo "Run dir=$rundir" >> ABOUT
echo "Work dir=$WORK" >> ABOUT
echo "Total jobs=$total_jobs" >> ABOUT
echo "Run Command: SWIFT_HEAP_MAX=$ram SWIFT_LIB=.. swift >> swift.out 2>&1 -tc.file tc.data -sites.file $execsite.xml -config cf annealing.swift -minrange=$min_target_innovation -maxrange=$max_target_innovation-rangeinc=$target_innovation_increment -nreps=$annealing_repeats -annealingcycles=$annealing_cycles -evoreruns=$evolve_reruns -alphai=$alpha_i -alpham=$alpha_m -beta=$beta -gamma=$gamma -delta=$delta -nworkers=$nworkers -rerunsperapp=$reruns_per_opt_invocation -e33="$escapecode"" >> ABOUT

if [ _$dryrun != _ ]; then
  exit 0
fi

if [ "$USE_SCS" == "1" ]; then
   runswift "sites.xml"
   stop-coaster-service
else
   runswift "$execsite.xml"
fi

# Run the convertbest.sh script on the generated txt files from within the rundir
../bin/convertbest.sh *.txt

exit

# @arg("nworkers","4")
# @arg("rerunsperapp", "100")
# @arg("seed", "0" )          FIXME
# @arg("minrange", "58")
# @arg("maxrange", "59")
# @arg("rangeinc", "50")
# @arg("nreps", "1")
# @arg("tstart", "2.0")       FIXME
# @arg("tend", "0.01")        FIXME
# @arg("trejection", "0.3")   FIXME
# @arg("evoreruns", "100")
# @arg("startingjump", "2.3")
# @arg("alphai", "0.0")
# @arg("alpham", "0.0")
# @arg("beta", "4.0")
# @arg("gamma", "50.0")
# @arg("delta", "-1.0")
# @arg("annealingcycles", "50")

