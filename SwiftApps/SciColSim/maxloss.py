#! /usr/bin/env python

n = 0
v = 1
e = 2

graphs = [ 
     [ "movie",    500,    1008 ],
     [ "big.001", 1857,    1312 ],
     [ "big.002", 3094,    2683 ],
     [ "big.005", 5405,    6776 ],
     [ "big.01",  7765,   13421 ],
     [ "big.10", 19281,  134594 ],
     [ "big.20", 23258,  269133 ],
     [ "big.30", 25484,  403401 ],
     [ "big.40", 26821,  537106 ],
     [ "big.50", 27774,  671179 ],
     [ "big.60", 28482,  804774 ],
     [ "big.70", 29055,  938061 ],
     [ "big.80", 29487, 1072415 ],
     [ "big.90", 29779, 1205741 ],
     [ "big",    30060, 1338753 ], 
     ]

for G in graphs:
	N = G[n]
	V = G[v]
	E = G[e]
	print "\nGraph %s: V=%d E=%d\n" % (N,V,E)
	for pct in range(1,11):
		Tmax = int(E * (pct/10.0))
		tl = 0.
		vl = 0.
		for t in range(Tmax):
			tl = tl + float(V*(V-1.))/float(2.*(E-t))
			vl = vl + float(V*(V-1.)/2. - E + t)/float((E-t)*(E-t))
		tl /= float(Tmax)
		vl /= float(Tmax)*float(Tmax)
		print pct, Tmax, tl, vl
