#! /bin/bash
datafile=$1
touch multi_loss.data
shift 1
NWORKERS=${23}
START=$(date +%s)
OMP_NUM_THREADS=$NWORKERS $(dirname $0)/openmp-optimizer $* >/dev/null 2>&1
RC=$?
END=$(date +%s)
if [ $RC == 127 ];
then
    echo "Missing openmp-optimizer binary" >&2
    exit 1
fi

if [ $RC != 0 ];
then
    echo "Problem running the openmp-optimizer" >&2
    exit 1
fi

mv multi_loss.data $datafile
DIFF=$(($END-$START))
echo $DIFF >> $datafile

exit $RC
