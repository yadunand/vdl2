#! /bin/sh

mkdir -p graph

for p in .0005 ; do # .001 .002 .003 .004 .005 .006 .007 .008 .009 .010; do
  for ((i=0;i<10;i++)); do
    ./samplegraph.sh $p > g/bg${p}.${i}
  done
done
