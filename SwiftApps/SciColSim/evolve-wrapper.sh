#!/bin/bash -x

crash()
{
    MSG=$1
    echo ${MSG}  >&2
    exit 1
}

launchDirectory=$PWD
echo Viewing first input
echo Arguments are $@

# Verify WN_TMP is defined
if [ -z "$OSG_WN_TMP" ]; then
   crash "OSG_WN_TMP is undefined"
fi

# Verify engage subdirectory of OSG_WN_TMP
mkdir -p $OSG_WN_TMP/engage
if [ ! -d "$OSG_WN_TMP/engage" ]; then
   crash "OSG_WN_TMP/engage does not exist"
fi

tmpDirectory=`mktemp -d $OSG_WN_TMP/engage/SciColSim.XXXXXX`
cd $tmpDirectory

# Retrieve software
wget http://www.ci.uchicago.edu/~davidk/SciColSim/evolve.sh 
chmod +x evolve.sh
wget http://www.ci.uchicago.edu/~davidk/SciColSim/openmp-optimizer 
chmod +x openmp-optimizer
wget http://www.ci.uchicago.edu/~davidk/SciColSim/movie_graph.txt

# Run evolve
mkdir -p $( dirname $1)
./evolve.sh "$@"
cp $1 $launchDirectory/$( dirname $1 )

# Finish
cd $launchDirectory
rm -rf $tmpDirectory
