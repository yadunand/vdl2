import "math";
import "colortext";

/*
 *TODO:
 */

type file;

type Res
{
    float loss;
    float sdev;
    float tavg;
    float tsdev;
}

type Checkpoint
{
  int i;
  int j;
  float dx0, dx1, dx2, dx3, dx4;
  float rej0, rej1, rej2, rej3, rej4;
  float target_innov;
  float loss;
  float alpha_i;
  float alpha_m;
  float beta;
  float gamma;
  float delta;
  float sdev;
}

global boolean restart = (@arg("restart","false") == "true");
tracef("Restart flag is %b\n", restart);

global int iterate_adjust = @toint(@arg("iterate_adjust","1")); // FIXME!!! : 1 for 0.93, 0 for 0.94

global boolean FIX_VARIABLES = true;
global int var_fixed[] = [0,0,0,0,0];
global int Nworkers = @toint(@arg("nworkers","4"));
global int rerunsPerApp= @toint(@arg("rerunsperapp", "100"));

( float nx ) newx( float x, float dx )
{
  float r = (random());

  if (r > 0.5) {
    nx = x + (random())*dx; //Java already returns a float between [0-1]
  }
  else {
    nx = x - (random())*dx;
  }
  // tracef("newx(%f,%f)=%f\n",x,dx,nx);
}

app  (/*file outfile,*/  file loss ) evolve ( string args[], file graph )
{
  evolve @loss  args /*stdout=@outfile*/;  // FIXME: graph is passed implicitly
}

app (file outfile, file logfile ) sumloss( file loss[] )
{
  sumloss @filename(logfile) @filenames(loss) stdout=@filename(outfile);
}

/*

  Program structure:

  main
    optimizer_sweep() - formerly, python script
      multi_annealing()
        multi_loss()
         evolve()
       sumloss()
*/

(file bestfile, file maxfile) multi_annealing (string best_filename,
                                               float T_start,
                                               float T_end,
                                               float Target_rejection,
                                               int evolve_reruns,
                                               float starting_jump,
                                               float params0[],
                                               float target_innov,
                                               int annealing_cycles)
{
  int cycle      = 10;     // const
  int NEVOPARAMS = 5;      // const - 5 params: alpha_i, alpha_m, beta, gamma, delta

  float rejection[][];  // [i][j] where i is cycle and j is evolve-parameter (alpha_i, alpha_m, beta, gamma, delta)
  float trejection[][];

  float x[][], dx[][], curr_loss[], curr_sdev[];

  Res mlres[][];

  file ckptFile <single_file_mapper; file=@strcat("ckpt.",target_innov)>;
  Checkpoint ckpt;
  int restartIndex;

  if (restart) {
    ckpt = readData(ckptFile);
    restartIndex = ckpt.i;
  }
  else {
    restartIndex = -1;
    mlres[0][0] = multi_loss(T_start, T_end, annealing_cycles, Target_rejection,
                            starting_jump, 0, 0, params0, target_innov, evolve_reruns ); // FIXME: serves for all evolve-params ???
    tracef( "multi_annealing: AR: initial: %f +- %f\n", mlres[0][0].loss, mlres[0][0].sdev );
  
    foreach j in [0:NEVOPARAMS-1] {
      x[0][j]         = params0[j];
      dx[0][j]        = starting_jump;
      rejection[0][j] = 0.0;
      curr_loss[j]    = mlres[0][0].loss;
      curr_sdev[j]    = mlres[0][0].sdev;
    }
  }
  
  iterate iter_i {  // number of annealing cycles
    int i = iter_i + 1;  // i ranges [1:n] in the swift script so that [0] can be the initial condition

    // set new temperature, rejection threshold, and dx values for this cycle
    float temperature = T_start*exp( @tofloat( i-1 ) * ( jlog( T_end ) - jlog( T_start ) ) / @tofloat( annealing_cycles ) );

    tracef( @strcat( "multi_annealing: AR: i=%i ....T = ", color( 3, "%f" ),"\n" ), i, temperature );

    if ( i <  restartIndex ) { 
      tracef( "skipping index %i - less than restart index %i\n", i, restartIndex );
    }
    else { if ( i == restartIndex ) { // reset variables from the restart file
      dx[i] = [ckpt.dx0, ckpt.dx1, ckpt.dx2, ckpt.dx3, ckpt.dx4];
      rejection[i] = [ckpt.rej0, ckpt.rej1, ckpt.rej2, ckpt.rej3, ckpt.rej4];
      x[i][0] = ckpt.alpha_i;
      x[i][1] = ckpt.alpha_m;
      x[i][2] = ckpt.beta;
      x[i][3] = ckpt.gamma;
      x[i][4] = ckpt.delta;
      curr_loss[((i+1)*NEVOPARAMS)-1] = ckpt.loss;
      curr_sdev[((i+1)*NEVOPARAMS)-1] = ckpt.sdev;
    }
    else { // i > restartIndex: proceed as normal whether restarting or not

      // On each new "major" cycle within the annealing_cycles (other than the first) set new rejection and dx values
  
      if (i %% cycle == 1 && i > 1) {
        tracef( "multi_annealing: new cycle at i=%i\n", i );
        tracef( color( Pink, "multi_annealing: AR: New cycle at %i: prev dx[0-4]=[%f %f %f %f %f]\n" ),
                       i, dx[i-1][0], dx[i-1][1], dx[i-1][2], dx[i-1][3], dx[i-1][4] );
        foreach k in [0:NEVOPARAMS-1] {
          float newrejection = rejection[i-1][k] / @tofloat( cycle );
          if (newrejection > 0.0) {
            dx[i][k] = dx[i-1][k] / ( newrejection / Target_rejection );
            // FIXME: re-enable: rejection[i][k]=0.0;
            trejection[i][k]=0.0;
          }
          else {
            dx[i][k] = dx[i-1][k] * 2.0;
            // FIXME: re-enable: rejection[i][k]=rejection[i-1][k];
                      trejection[i][k]=newrejection;
          }
                  // FIXME: HANGS? : tracef(color(Red,"Recomputed rejection: i=%d k=%d dx[i][k]=%f\n"), i, k, dx[i][k]);
        }
        tracef( color( Blue, "multi_annealing: AR: New cycle at %i: dx[0-4]=[%f %f %f %f %f]\n" ),
                               i, dx[i][0], dx[i][1], dx[i][2], dx[i][3], dx[i][4] );
      }
      else { // If not new cycle, set dx[i][*] from previous dx ([i-1]). rejection[i][j] is set later.
        foreach k in [0:NEVOPARAMS-1] {
          dx[i][k] = dx[i-1][k];
        }
      }
      iterate j {  // Try a new value for each non-fixed param; then write results and accept or reject
        int curr = ( i * NEVOPARAMS ) + j;
        int prev = curr-1;
  
        if ( /* (!FIX_VARIABLES) || */ ( var_fixed[j] == 0 ) ) {  // Adjustable vars
             // fixed=1,1,0,0,0: FIXME: FIX_VARIABLES flag has faulty logic but OK when TRUE
          float try_x[];
          foreach k in [0:NEVOPARAMS-1] { // Select the evolve params to try
            if (k < j) {
              try_x[k] = x[i][k]; // already set x[i][k]
            }
            else {
              if (k == j) {
                try_x[k] = newx(x[i-1][j], dx[i-1][j]); // permute x[i-1][j]
              }
              else { // k > j
                try_x[k] = x[i-1][k]; // use x[i-1][k] (from prior cycle)
              }
            }
          }
          tracef( @strcat( "multi_annealing: AR: ", color( 10,"%f" ), " ", color( 9,"%i" ),"\n" ), try_x[j], j );
          mlres[i][j] = multi_loss( T_start, T_end, annealing_cycles, Target_rejection, starting_jump,
                                    i, j, try_x, target_innov, evolve_reruns ); // do the N evolve()'s, N=evolve_rerusn
          tracef( "multi_annealing: AR: %f +- %f\n", mlres[i][j].loss, mlres[i][j].sdev );
          // Beyond this point, x[] and dx[] are being set for this i,j
          float ALOT = 100000000000.0; // 100,000,000,000. = 10^11
          if (mlres[i][j].loss < ALOT) {
            fprintf( best_filename, "N, %fs, %i, %i, %f, %f, |, %i, %f, [, %f, %f, %f, %f, %f, ], %f\n",
                    mlres[i][j].tavg, i-1, j, dx[i][j], rejection[i][j], @toint(target_innov), mlres[i][j].loss,
                    try_x[0], try_x[1], try_x[2], try_x[3], try_x[4], mlres[i][j].sdev );
                    // Note i-1 field: print that way to match with C++ output
  
                    // fprintf( "max_dist_swift.txt", color( Red,"multi_annealing: AF: max_dist.txt - tbd\n" ) );
                    // FIXME: max_dist is global set in evolve()
          }
          else { // does this ever occur? if so did we want to still do the ratio computation above???
            fprintf( best_filename, "A, %fs, %i, %i, %f, %f, |, %i, %f, [, %f, %f, %f, %f, %f, ], %f\n",
                     mlres[i][j].tavg, i-1, j, dx[i][j], rejection[i][j], @toint(target_innov), mlres[i][j].loss,
                     try_x[0], try_x[1], try_x[2], try_x[3], try_x[4], mlres[i][j].sdev );
                     // Note i-1 field: print that way to match with C++ output
            //tracef( "multi_annealing: Loss %f > ALOT at [i][j] = [%d][%d]\n", mlres[i][j].loss, i ,j );
          }
          float ratio = min( 1.0, exp( -( mlres[i][j].loss - curr_loss[prev] ) / temperature ) );
          float r = (random()); //Java already returns a random float between [0.0-1.0]
          tracef("multi_annealing: AR: %f vs %f\n", r, ratio);
          if (r > ratio) { // Reject new parameter
            x[i][j] = x[i-1][j];
            if (i %% cycle == 1 && i > 1) {
              rejection[i][j] = trejection[i][j] + 1.0;    // FIXME: triple-check this!
            }
            else {
              rejection[i][j] = rejection[i-1][j] + 1.0;  // FIXME: AR: Is this correct? incr rejection?
            }
            curr_loss[curr] = curr_loss[prev];
            curr_sdev[curr] = curr_sdev[prev];
            // FIXME: AR: the following prints seem to replicate values in the .cpp version - please clarify.
            tracef( "multi_annealing: AR: %i,%i %i Did not accept: %f (%i)\n", i, j, i, try_x[j], j );
            tracef( "multi_annealing: AR: %f %f %f %f %f\n", try_x[0],try_x[1],try_x[2],try_x[3],try_x[4] );
          }
          else { // Accept new parameter
            tracef( "multi_annealing: Accepting try_x[j], i=%i j=%i\n",i,j );
            x[i][j] = try_x[j];
            if (i %% cycle == 1 && i > 1) {
              rejection[i][j] = trejection[i][j];    // FIXME: triple-check this!
            }
            else {
              rejection[i][j] = rejection[i-1][j];  // FIXME: AR: Is this correct? no incr of rejection?
            }
            curr_loss[curr] = mlres[i][j].loss;
            curr_sdev[curr] = mlres[i][j].sdev;
            tracef( "multi_annealing: Accepting try_x[j], i=%i j=%i try_x[j]=%f\n", i, j, try_x[j] );
            float rj[];
            foreach k in [0:NEVOPARAMS-1] {
              if (k <= j) {
                rj[k] = rejection[i][k]; // Was either set from previous j or just set for this j
              }
              else {
                rj[k] = rejection[i-1][k]; // Not yet set, use previous
              }
            }
            tracef(@strcat("multi_annealing: AR: [%i][%i] ", color(8,"Rejection counts: "),
                           color(1,"%f"), " ", color(7,"%f"), " ", color(5,"%f"), " ", color(9,"%f"), " ", color(6,"%f"), "\n\n"),
                           i, j, rj[0], rj[1], rj[2], rj[3], rj[4]);
            tracef(@strcat("multi_annealing: AR: %i ", color(8,"***** Did accept! "),
                           color(1,"%f"), " ", color(7,"%f"), " ", color(5,"%f"), " ", color(9,"%f"), " ", color(6,"%f"), "\n\n"),
                           i, try_x[0], try_x[1], try_x[2], try_x[3], try_x[4]);
            }
          }
          else {// Fixed Vars
            x[i][j] = x[i-1][j];
            rejection[i][j] = rejection[i-1][j];
            curr_loss[curr] = curr_loss[prev];
            curr_sdev[curr] = curr_sdev[prev];
            // dx[i][j] not set for fixed vars
          }
        } until( j == (NEVOPARAMS-iterate_adjust) );
      }} // of if/else 
    } until( iter_i == (annealing_cycles-iterate_adjust) );
}

(Res r) multi_loss(float t_start, float t_end, int annealing_steps, float t_rejection, float starting_jump,
                   int ci, int cj, float x[], float target_innov, int evolve_reruns)
{
  tracef("%q\n", x);
  file rfile[];
  //file ofile[]; // FIXME: to obtain timings and otehr stats

  tracef( "multi_loss: entered: ci=%i cj=%i target_innov=%f evolve_reruns=%i x=%q\n",ci, cj, target_innov,evolve_reruns,x );

  int appCalls = @toint(@tofloat(evolve_reruns)/@tofloat(rerunsPerApp)); // FIXME: handle fractional issues and rounding etc.
                                                                         // For now must divide evenly
  tracef("multi_loss appCalls=%i\n", appCalls);

  foreach i in [1:appCalls] { // repeats of the evolove() - same as n_reruns
    //file outfile; // FIXME: map and save in future
    string args[] = [ // FIXME: move this to a setargs() function
            //    alpha_i           alpha_m        beta           gamma          delta          target_innov
                 @strcat(x[0]),  @strcat(x[1]), @strcat(x[2]), @strcat(x[3]), @strcat(x[4]), @strcat(target_innov),
            //     n_epochs n_steps   evolve_reruns           range
            //    "40000",   "20",  @strcat(evolve_reruns),    "2",
                  "40000",   "20",  @strcat(rerunsPerApp),     "2",
            //    verbose_level
                    "1",
            //    T_start           T_end           Annealing_steps           Target_rejection      Starting_jump
                  @strcat(t_start), @strcat(t_end), @strcat(annealing_steps), @strcat(t_rejection), @strcat(starting_jump),
            //    FREEZE: alpha_i alpha_m beta gamma delta
                          "0",    "0",    "0", "0",  "0",
            //    operation-code:(m,a)  Nworkers           seed
                  "m",                  @strcat(Nworkers), @arg("seed", "0" ) ];

    file graph <"movie_graph.txt">;

    (/*outfile,*/ rfile[i]) = evolve(args, graph);
    tracef("multi_loss: i=%i calling evolve, args=%q\n", i, args);
    // tracef("multi_loss: after evolve: i=%i %k %k\n", i, outfile, rfile[i]);
  }

  string anneal_cycle;
  if(ci==0) {
    anneal_cycle="init";
  }
  else {
    anneal_cycle=@strcat(ci-1);
  }
    
  file sumfile<single_file_mapper; file=@strcat("sumloss/T",target_innov,".",anneal_cycle,".",cj,".sumloss")>;
  file logfile<single_file_mapper; file=@strcat("sumloss/T",target_innov,".",anneal_cycle,".",cj,".sumlog")>;

  (sumfile, logfile) = sumloss(rfile);
  r = readData(sumfile);

  tracef("multi_loss: returning: ci=%i cj=%i r.loss=%f r.sdev=%f\n",ci,cj,r.loss,r.sdev);
  // file statfile = sumstats(ofile);  FIXME: to obtain timings and other stats
  // s = readStat(statsfile);          FIXME: to obtain timings and other stats
}

optimizer_serial_sweep() // Implements logic of python driver script
{

  int minrange = @toint(@arg("minrange", "58"));
  int maxrange = @toint(@arg("maxrange", "59"));
  int rangeinc = @toint(@arg("rangeinc", "50"));

  // FIXME: add provision for random priming and random param values when x[i] == -100 (see optimizer.cpp main())

  int nreps = @toint(@arg("nreps", "1"));

//file bestfile <single_file_mapper; file=@strcat("output/T",target,".R",rep,".best_opt_some")>;
//file maxfile <single_file_mapper; file=@strcat("output/T",target,".R",rep,".max_dist")>;

#  foreach target_innov in [minrange:maxrange:rangeinc] {
  iterate i {
    int target_innov = minrange + (i*rangeinc);
    foreach rep in [1:nreps] {

      string best_filename = @strcat( "best.T", @strcat(target_innov), ".R", @strcat(rep), ".txt" );
      file outfile;  // <single_file_mapper; file=@strcat("output/T",target_innov,".R",rep,".out")>;
      file lossfile; // <single_file_mapper; file=@strcat("output/T",target_innov,".R",rep,".loss_data")>;

      (outfile,lossfile) = multi_annealing(best_filename,
		                           @tofloat(@arg("tstart", "2.0")),
                                           @tofloat(@arg("tend", "0.01")),
                                           @tofloat(@arg("trejection", "0.3")),
                                           @toint(@arg("evoreruns", "100")),
                                           @tofloat(@arg("startingjump", "2.3")),
                                           [ @tofloat(@arg("alphai", "0.0")),
                                             @tofloat(@arg("alpham", "0.0")),
                                             @tofloat(@arg("beta", "4.0")),
                                             @tofloat(@arg("gamma", "50.0")),
                                             @tofloat(@arg("delta", "-1.0"))
                                           ],
                                           @tofloat(target_innov),
                                           @toint(@arg("annealingcycles", "50")) );
    }
  } until(target_innov >= (maxrange-rangeinc));
}

optimizer_sweep() // Implements logic of python driver script
{

  int minrange = @toint(@arg("minrange", "58"));
  int maxrange = @toint(@arg("maxrange", "59"));
  int rangeinc = @toint(@arg("rangeinc", "50"));

  // FIXME: add provision for random priming and random param values when x[i] == -100 (see optimizer.cpp main())

  int nreps = @toint(@arg("nreps", "1"));

//file bestfile <single_file_mapper; file=@strcat("output/T",target,".R",rep,".best_opt_some")>;
//file maxfile <single_file_mapper; file=@strcat("output/T",target,".R",rep,".max_dist")>;

  foreach target_innov in [minrange:maxrange:rangeinc] {
    foreach rep in [1:nreps] {

      string best_filename = @strcat( "best.T", @strcat(target_innov), ".R", @strcat(rep), ".txt" );
      file outfile;  // <single_file_mapper; file=@strcat("output/T",target_innov,".R",rep,".out")>;
      file lossfile; // <single_file_mapper; file=@strcat("output/T",target_innov,".R",rep,".loss_data")>;

      (outfile,lossfile) = multi_annealing(best_filename,
		                           @tofloat(@arg("tstart", "2.0")),
                                           @tofloat(@arg("tend", "0.01")),
                                           @tofloat(@arg("trejection", "0.3")),
                                           @toint(@arg("evoreruns", "100")),
                                           @tofloat(@arg("startingjump", "2.3")),
                                           [ @tofloat(@arg("alphai", "0.0")),
                                             @tofloat(@arg("alpham", "0.0")),
                                             @tofloat(@arg("beta", "4.0")),
                                             @tofloat(@arg("gamma", "50.0")),
                                             @tofloat(@arg("delta", "-1.0"))
                                           ],
                                           @tofloat(target_innov),
                                           @toint(@arg("annealingcycles", "50")) );
    }
  }
}

main()
{
  optimizer_sweep();
}

main();

/*

  Program structure:

  main
  optimizer_sweep()
  multi_annealing()
  multi_loss()
  evolve()
  sumloss()

  Example parameter sets:

  for target in range(58,59,50):
  for i in range(1):
  args="./toptimizer 0 0 4 50 -1 "+target+" 40000 20 75    2 1 2. 0.01 2 0.3 2.3 1 1 1 0 0 m // > out.T"+str(target)+".i"+str(i)
  os.system(args);

  string fastargs1[] = [
  "0", "0", "4", "50", "-1", @strcat(target),
  "40000", "20", "1000", "2",
  "1",
  "2.", "0.01", "100", "0.3", "2.3",
  "1", "1", "0", "0", "0"];
  string fastargs2[] = [
  "0", "0", "4", "50", "-1", @strcat(target),
  "40000", "20", "1000", "2",
  "1",
  "2.", "0.01",  "5", "0.3", "2.3",
  "1", "1", "0", "0", "0", "m"];
  string fastargs3[] = [
  "0", "0", "4", "50", "-1", @strcat(target),
  "40000", "20", @strcat(repeats), "2",
  "1",
  "2.", "0.01",  "2", "0.3", "2.3",
  "1", "1", "0", "0", "0", "m"];
*/

(string args[]) setargs()
{
    // string longargs[] = @strcat("0 0 4 50 -1 ",target," 40000 20 1000 2 1 2. 0.01 100 0.3 2.3 1 1 0 0 0 m");

    //  [alpha_i alpha_m beta gamma delta target_innov
    //  [n_epochs n_steps n_reruns] [range]
    //  [verbose_level]
    //  [T_start T_end Annealing_steps Target_rejection Starting_jump]
    //  [FREEZE_alpha_i FREEZE_alpha_m FREEZE_beta FREEZE_gamma FREEZE_delta] [operation-code:(m,a) Nworkers]
}

////////////////// HOLD JUNK

//                    tracef(@strcat("multi_annealing: AR: %i ", color(8,"Rejection counts: "),
//                                   color( /* 2 */ 1," %f"), "\n\n"),
//                                   i, rejection[i][j] ); // , rejection[i][1], rejection[i][2], rejection[i][3], rejection[i][4]);
// FIXME: determine correct rejection[] values to avoid hanging:
//                    tracef(@strcat("multi_annealing: AR: %i ", color(8,"Rejection counts: "),
//                                   color( /* 2 */ 1," %f"), color(7," %f"), color(5," %f"), color(9," %f"), color(6," %f"), "\n\n"),
//                                   rejection[i][0], rejection[i][1], rejection[i][2], rejection[i][3], rejection[i][4]);
// END FIXME
