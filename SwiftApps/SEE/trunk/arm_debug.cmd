option ampl_include "instances/debug\
.";
option solver pathampl;
option solver semiampl;
option path_options "nms=no cum_ite_lim=100000 min_ite_lim=10000";
option semi_options "nms=no";
load ces.so;
function seq random;
function cesp;
function d_cesp;
option presolve 0;
model armington.mod;
model;
set RegSub ordered;
set ProSub ordered;
set ConSub ordered;
set ComSub ordered;
set ImpCom ordered;
set ImpSub ordered;
param CapDep {Regions_All};
param CapRatio {Regions_All};
param LabRatio {Regions_All,0..61};
data arm_debug.dat;
data;
set RegSub = ROW EUA;
set ProSub = AGR OIL GAS COL PTL MFS ELC TRL TRA TRS GOV INV;
set ConSub = CON1;
set ComSub = LND LAB CAP NAT
 AGR OIL GAS COL PTL MFS ELC TRL TRA TRS GOV INV;
set ImpCom = AGR OIL GAS COL PTL MFS ELC TRL TRA TRS GOV;
set ImpSub = AGR_I OIL_I GAS_I COL_I PTL_I MFS_I ELC_I TRL_I TRA_I TRS_I GOV_I;

param CapRatio :=
  ROW  0.09875029639641582
  EUA  0.0675871621464177
;

param CapDep :=
  ROW  0.04
  EUA  0.04
;

param LabRatio :
include labor_growth.dat
;

commands armington_process.cmd;
include armington_output.cmd;

/*
display {r in Regions, k in Region_Commodities[r] diff Homogenous_Commodities}
 (sum{p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Outputs[p,rp]} Producers_Revenues[p,rp,r,k] + sum{c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Supplies[c,rc]} Consumers_Revenues[c,rc,r,k] + sum{c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Endowments[c,rc]} Consumers_Endowment[c,rc,r,k] - sum{p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Inputs[p,rp]} Producers_Expenditures[p,rp,r,k] - sum{c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Demands[c,rc]} Consumers_Expenditures[c,rc,r,k]);

display {k in Homogenous_Commodities}
 (sum{p in Producers, rp in Producers_Regions[p], (r,k) in Producers_Outputs[p,rp]} Producers_Revenues[p,rp,r,k] + sum{c in Consumers, rc in Consumers_Regions[c], (r,k) in Consumers_Supplies[c,rc]} Consumers_Revenues[c,rc,r,k] + sum{c in Consumers, rc in Consumers_Regions[c], (r,k) in Consumers_Endowments[c,rc]} Consumers_Endowment[c,rc,r,k] - sum{p in Producers, rp in Producers_Regions[p], (r,k) in Producers_Inputs[p,rp]} Producers_Expenditures[p,rp,r,k] - sum{c in Consumers, rc in Consumers_Regions[c], (r,k) in Consumers_Demands[c,rc]} Consumers_Expenditures[c,rc,r,k]);
*/

solve Producers_Problems_All > solve.dat;
solve Consumers_Problems_All > solve.dat;

for {t in 0..60} {
  solve Markets_Problems_All > solve.dat;

  printf "%6d %11s %11s\n", t, 'LAB', 'CAP' > limits.dat;
  printf {c in Consumers, r in Consumers_Regions[c]} "%6s % 5.4e % 5.4e\n", r, Consumers_Supplies_Limit[c,r,r,'LAB'], Consumers_Supplies_Limit[c,r,r,'CAP'] > limits.dat;
  printf "\n" > limits.dat;

  for {r in RegSub} {
    printf "%6s", r > ratio.dat;
    for {p in ProSub} {
      printf " %11s", p > ratio.dat;
    }
    for {c in ConSub} {
      printf " %11s", c > ratio.dat;
    }
    for {i in RegSub} {
      printf " %11s", i > ratio.dat;
    }
    printf " %5d\n", t > ratio.dat;
  
    for {k in ComSub} {
      printf "%6s", k > ratio.dat;
      printf {p in ProSub} " % 5.4e", if ((p in Producers) and (r in Producers_Regions[p]) and ((r,k) in Producers_Inputs[p,r])) then Producers_Input[p,r,r,k] else 0.0 > ratio.dat;
      printf {c in ConSub} " % 5.4e", if ((c in Consumers) and (r in Consumers_Regions[c]) and ((r,k) in Consumers_Demands[c,r])) then Consumers_Demand[c,r,r,k] else if ((c in Consumers) and (r in Consumers_Regions[c]) and ((r,k) in Consumers_Supplies[c,r])) then -Consumers_Supply[c,r,r,k] else 0.0 > ratio.dat;
     printf "\n" > ratio.dat;
    }
  
    for {k in ImpSub} {
      printf "%6s", k > ratio.dat;
      printf {p in ProSub} " % 5.4e", if ((p in Producers) and (r in Producers_Regions[p]) and ((r,k) in Producers_Inputs[p,r])) then Producers_Input[p,r,r,k] else 0.0 > ratio.dat;
      printf {c in ConSub} " % 5.4e", if ((c in Consumers) and (r in Consumers_Regions[c]) and ((r,k) in Consumers_Demands[c,r])) then Consumers_Demand[c,r,r,k] else if ((c in Consumers) and (r in Consumers_Regions[c]) and ((r,k) in Consumers_Supplies[c,r])) then -Consumers_Supply[c,r,r,k] else 0.0 > ratio.dat;
      for {ki in ImpCom: ord(k) = ord(ki)} {
        printf {ri in Regions_All} " % 5.4e", if ((k in Producers) and (r in Producers_Regions[k]) and ((ri,ki) in Producers_Inputs[k,r])) then Producers_Input[k,r,ri,ki] else 0.0 > ratio.dat;
      }
      printf "\n" > ratio.dat;
    }
    printf "\n" > ratio.dat;
  }

  for {r in RegSub} {
    printf "%6s", r > expend.dat;
    for {p in ProSub} {
      printf " %11s", p > expend.dat;
    }
    for {c in ConSub} {
      printf " %11s", c > expend.dat;
    }
    for {i in RegSub} {
      printf " %11s", i > expend.dat;
    }
    printf " %5d\n", t > expend.dat;
  
    for {k in ComSub} {
      printf "%6s", k > expend.dat;
      printf {p in ProSub} " % 5.4e", if ((p in Producers) and (r in Producers_Regions[p]) and ((r,k) in Producers_Inputs[p,r])) then Producers_Expenditures[p,r,r,k]*Price[r,k]*Producers_Input[p,r,r,k] else 0.0 > expend.dat;
      printf {c in ConSub} " % 5.4e", if ((c in Consumers) and (r in Consumers_Regions[c]) and ((r,k) in Consumers_Demands[c,r])) then Consumers_Expenditures[c,r,r,k]*Price[r,k]*Consumers_Demand[c,r,r,k] else if ((c in Consumers) and (r in Consumers_Regions[c]) and ((r,k) in Consumers_Supplies[c,r])) then -Consumers_Revenues[c,r,r,k]*Price[r,k]*Consumers_Supply[c,r,r,k] else 0.0 > expend.dat;
     printf "\n" > expend.dat;
    }
  
    for {k in ImpSub} {
      printf "%6s", k > expend.dat;
      printf {p in ProSub} " % 5.4e", if ((p in Producers) and (r in Producers_Regions[p]) and ((r,k) in Producers_Inputs[p,r])) then Producers_Expenditures[p,r,r,k]*Price[r,k]*Producers_Input[p,r,r,k] else 0.0 > expend.dat;
      printf {c in ConSub} " % 5.4e", if ((c in Consumers) and (r in Consumers_Regions[c]) and ((r,k) in Consumers_Demands[c,r])) then Consumers_Expenditures[c,r,r,k]*Price[r,k]*Consumers_Demand[c,r,r,k] else if ((c in Consumers) and (r in Consumers_Regions[c]) and ((r,k) in Consumers_Supplies[c,r])) then -Consumers_Revenues[c,r,r,k]*Price[r,k]*Consumers_Supply[c,r,r,k] else 0.0 > expend.dat;
      for {ki in ImpCom: ord(k) = ord(ki)} {
        printf {ri in Regions_All} " % 5.4e", if ((k in Producers) and (r in Producers_Regions[k]) and ((ri,ki) in Producers_Inputs[k,r])) then Producers_Expenditures[k,r,ri,ki]*Price[ri,ki]*Producers_Input[k,r,ri,ki] else 0.0 > expend.dat;
      }
      printf "\n" > expend.dat;
    }
    printf "\n" > expend.dat;
  }

  printf "%6d", t > price.dat;
  printf {r in RegSub} " %11s", r > price.dat;
  printf "\n" > price.dat;
  for {k in ComSub} {
    printf "%6s", k > price.dat;
    printf {r in RegSub} " % 5.4e", if ((r in Regions) and (k in Region_Commodities[r])) then Price[r,k] else 0.0 > price.dat;
    printf "\n" > price.dat;
  }
  for {k in ImpSub} {
    printf "%6s", k > price.dat;
    printf {r in RegSub} " % 5.4e", if ((r in Regions) and (k in Region_Commodities[r])) then Price[r,k] else 0.0 > price.dat;
    printf "\n" > price.dat;
  }
  printf "\n" > price.dat;

  let {c in Consumers, r in Consumers_Regions[c], (rs,'CAP') in Consumers_Supplies[c,r]} Consumers_Supplies_Limit[c,r,rs,'CAP'] := (1-CapDep[rs])*Consumers_Supplies_Limit[c,r,rs,'CAP'] + CapRatio[r]*Consumers_Demand[c,r,rs,'INV'];
  let {c in Consumers, r in Consumers_Regions[c], (rs,'NAT') in Consumers_Supplies[c,r]} Consumers_Supplies_Limit[c,r,rs,'NAT'] := 1;
  let {c in Consumers, r in Consumers_Regions[c], (rs,'LND') in Consumers_Supplies[c,r]} Consumers_Supplies_Limit[c,r,rs,'LND'] := 1;
  let {c in Consumers, r in Consumers_Regions[c], (rs,'LAB') in Consumers_Supplies[c,r]} Consumers_Supplies_Limit[c,r,rs,'LAB'] := LabRatio[r,t+1];
}
