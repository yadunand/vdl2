##### Producer Variables #####
#
# Producers_Input: quantity of the input and intermediate commodities used
# Producers_Output: quantity of the output commodities produced
#
#####

var Producers_Input {p in Producers, r in Producers_Regions[p], Producers_Inputs[p,r] union Producers_Intermeds[p,r]} := 1.0;
var Producers_Output {p in Producers, r in Producers_Regions[p], Producers_Outputs[p,r]} := 1.0;

check {p in Producers, r in Producers_Regions[p]}: card(Producers_Inputs[p,r] inter Producers_Intermeds[p,r]) = 0;
check {p in Producers, r in Producers_Regions[p]}: card(Producers_Outputs[p,r] inter Producers_Intermeds[p,r]) = 0;

param Producers_PTR {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} :=
  Producers_RRPriceTax_Revenues[p,r,ro,ko]*Producers_RRPriceRate_Revenues[p,r,ro,ko] +
  Producers_ROPriceTax_Revenues[p,r,ro,ko]*Producers_ROPriceRate_Revenues[p,r,ro,ko];
param Producers_QTR {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} :=
  Producers_RRQuantityTax_Revenues[p,r,ro,ko]*Producers_RRQuantityRate_Revenues[p,r,ro,ko] +
  Producers_ROQuantityTax_Revenues[p,r,ro,ko]*Producers_ROQuantityRate_Revenues[p,r,ro,ko];

param Producers_PTE {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} :=
  Producers_RRPriceTax_Expenditures[p,r,ri,ki]*Producers_RRPriceRate_Expenditures[p,r,ri,ki] +
  Producers_RIPriceTax_Expenditures[p,r,ri,ki]*Producers_RIPriceRate_Expenditures[p,r,ri,ki];
param Producers_QTE {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} :=
  Producers_RRQuantityTax_Expenditures[p,r,ri,ki]*Producers_RRQuantityRate_Expenditures[p,r,ri,ki] +
  Producers_RIQuantityTax_Expenditures[p,r,ri,ki]*Producers_RIQuantityRate_Expenditures[p,r,ri,ki];

Producers_Tax_Def {rr in Regions}:
  Producers_Tax[rr] complements Producers_Tax[rr] = 
    sum {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]: r == rr} (Producers_RRPriceTax_Revenues[p,r,ro,ko]*Producers_RRPriceRate_Revenues[p,r,ro,ko]*(if ko in Homogenous_Commodities then HPrice[ko] else Price[ro,ko]) + Producers_RRQuantityTax_Revenues[p,r,ro,ko]*Producers_RRQuantityRate_Revenues[p,r,ro,ko])*Producers_Output[p,r,ro,ko] +
    sum {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]: ro == rr} (Producers_ROPriceTax_Revenues[p,r,ro,ko]*Producers_ROPriceRate_Revenues[p,r,ro,ko]*(if ko in Homogenous_Commodities then HPrice[ko] else Price[ro,ko]) + Producers_ROQuantityTax_Revenues[p,r,ro,ko]*Producers_ROQuantityRate_Revenues[p,r,ro,ko])*Producers_Output[p,r,ro,ko] +
   sum {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]: r == rr} (Producers_RRPriceTax_Expenditures[p,r,ri,ki]*Producers_RRPriceRate_Expenditures[p,r,ri,ki]*(if ki in Homogenous_Commodities then HPrice[ki] else Price[ri,ki]) + Producers_RRQuantityTax_Expenditures[p,r,ri,ki]*Producers_RRQuantityRate_Expenditures[p,r,ri,ki])*Producers_Input[p,r,ri,ki] +
   sum {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]: ri == rr} (Producers_RIPriceTax_Expenditures[p,r,ri,ki]*Producers_RIPriceRate_Expenditures[p,r,ri,ki]*(if ki in Homogenous_Commodities then HPrice[ki] else Price[ri,ki]) + Producers_RIQuantityTax_Expenditures[p,r,ri,ki]*Producers_RIQuantityRate_Expenditures[p,r,ri,ki])*Producers_Input[p,r,ri,ki];

##### Producer Optimization Problem #####
#
# maximize Producers_Profits {p in Producers, r in Producers_Regions[p]}: 
#   (sum {(ro,ko) in Producers_Outputs[p,r]} 
#      ((Producers_Revenues[p,r,ro,ko] - Producers_PTR[p,r,ro,ko])*(if ko in Homogenous_Commodities then HPrice[ko] else Price[ro,ko]) - Producers_QTR[p,r,ro,ko])*Producers_Output[p,r,ro,ko] -
#    sum {(ri,ki) in Producers_Inputs[p,r]}
#      ((Producers_Expenditures[p,r,ri,ki] + Producers_PTE[p,r,ri,ki])*(if ki in Homogenous_Commodities then HPrice[ki] else Price[ri,ki]) + Producers_QTE[p,r,ri,ki])*Producers_Input[p,r,ri,ki]
#   )/Producers_Scale[p,r];
#
# Producers_Functions_Positive_Def {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Positive[p,r,f]}:
#   (if (ro,ko) in Producers_Outputs[p,r] then Producers_Output[p,r,ro,ko] else Producers_Input[p,r,ro,ko]) <= cesp(Producers_Functions_Scale[p,r,f,ro,ko]*Producers_Functions_Scale_Factor[p,r,f,ro,ko], Producers_Functions_Sigma[p,r,f,ro,ko], {(ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0} (Producers_Factors_Index[p,r,ri,ki], Producers_Functions_Share[p,r,f,ro,ko,ri,ki], Producers_Input[p,r,ri,ki]));
#
# Producers_Functions_Leontief_Def {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Leontief[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0}:
#   (if (ro,ko) in Producers_Outputs[p,r] then Producers_Output[p,r,ro,ko] else Producers_Input[p,r,ro,ko]) <= Producers_Functions_Scale[p,r,f,ro,ko]*Producers_Functions_Scale_Factor[p,r,f,ro,ko]*Producers_Input[p,r,ri,ki];
#
##### Notes #####
#
# (1) All variables are constrained to be nonnegative
#
##### First-Order Optimality Conditions #####
#
# Producers_Profit: report of producer profit given the optimal PInput and POutput
#
# m_pro_pos: multiplier on Positive production functions
# m_pro_leo: multiplier on Leontief production functions
#
#####

var Producers_Profit {p in Producers, r in Producers_Regions[p]} := 1.0;

var m_pro_pos {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Positive[p,r,f]} := 1.0;
var m_pro_leo {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Leontief[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0} := 1.0;

subject to

Producers_Profits_Def {p in Producers, r in Producers_Regions[p]}: 
  Producers_Profit[p,r] complements Producers_Profit[p,r] =
    sum {(ro,ko) in Producers_Outputs[p,r]} 
      ((Producers_Revenues[p,r,ro,ko] - Producers_PTR[p,r,ro,ko])*(if ko in Homogenous_Commodities then HPrice[ko] else Price[ro,ko]) - Producers_QTR[p,r,ro,ko])*Producers_Output[p,r,ro,ko] -
    sum {(ri,ki) in Producers_Inputs[p,r]}
      ((Producers_Expenditures[p,r,ri,ki] + Producers_PTE[p,r,ri,ki])*(if ki in Homogenous_Commodities then HPrice[ki] else Price[ri,ki]) + Producers_QTE[p,r,ri,ki])*Producers_Input[p,r,ri,ki];

Producers_Opt_Input {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r] union Producers_Intermeds[p,r]}:
  0 <= Producers_Input[p,r,ri,ki] complements 
    (if (ri,ki) in Producers_Inputs[p,r] then ((Producers_Expenditures[p,r,ri,ki] + Producers_PTE[p,r,ri,ki])*(if ki in Homogenous_Commodities then HPrice[ki] else Price[ri,ki]) + Producers_QTE[p,r,ri,ki])/Producers_Scale[p,r]) +
    (if (ri,ki) in Producers_Intermeds[p,r] then 
      (sum {f in Producers_Functions_Regions[p,r]: (ri,ki) in Producers_Functions_Positive[p,r,f]} m_pro_pos[p,r,f,ri,ki] + 
       sum {f in Producers_Functions_Regions[p,r]: (ri,ki) in Producers_Functions_Leontief[p,r,f]} (sum {(ri2,ki2) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ri,ki,ri2,ki2] > 0} m_pro_leo[p,r,f,ri,ki,ri2,ki2]))) -
    sum {f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Positive[p,r,f]: (ri,ki) in Producers_Functions_Inputs[p,r,f] and Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0} d_cesp(Producers_Factors_Index[p,r,ri,ki], Producers_Functions_Scale[p,r,f,ro,ko]*Producers_Functions_Scale_Factor[p,r,f,ro,ko], Producers_Functions_Sigma[p,r,f,ro,ko], {(ri2,ki2) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri2,ki2] > 0} (Producers_Factors_Index[p,r,ri2,ki2], Producers_Functions_Share[p,r,f,ro,ko,ri2,ki2], Producers_Input[p,r,ri2,ki2]))*m_pro_pos[p,r,f,ro,ko] - 
    sum {f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Leontief[p,r,f]: (ri,ki) in Producers_Functions_Inputs[p,r,f] and Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0} Producers_Functions_Scale[p,r,f,ro,ko]*Producers_Functions_Scale_Factor[p,r,f,ro,ko]*m_pro_leo[p,r,f,ro,ko,ri,ki] >= 0;

Producers_Opt_Output {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]}:
  0 <= Producers_Output[p,r,ro,ko] complements 
    sum {f in Producers_Functions_Regions[p,r]: (ro,ko) in Producers_Functions_Positive[p,r,f]} m_pro_pos[p,r,f,ro,ko] + 
    sum {f in Producers_Functions_Regions[p,r]: (ro,ko) in Producers_Functions_Leontief[p,r,f]} (sum {(ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0} m_pro_leo[p,r,f,ro,ko,ri,ki]) -
    ((Producers_Revenues[p,r,ro,ko] - Producers_PTR[p,r,ro,ko])*(if ko in Homogenous_Commodities then HPrice[ko] else Price[ro,ko]) - Producers_QTR[p,r,ro,ko])/Producers_Scale[p,r] >= 0;

Producers_Functions_Positive_Def {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Positive[p,r,f]}:
  0 <= m_pro_pos[p,r,f,ro,ko] complements cesp(Producers_Functions_Scale[p,r,f,ro,ko]*Producers_Functions_Scale_Factor[p,r,f,ro,ko], Producers_Functions_Sigma[p,r,f,ro,ko], {(ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0} (Producers_Factors_Index[p,r,ri,ki], Producers_Functions_Share[p,r,f,ro,ko,ri,ki], Producers_Input[p,r,ri,ki])) - (if (ro,ko) in Producers_Intermeds[p,r] then Producers_Input[p,r,ro,ko] else Producers_Output[p,r,ro,ko]) >= 0;

Producers_Functions_Leontief_Def {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Leontief[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0}:
  0 <= m_pro_leo[p,r,f,ro,ko,ri,ki] complements Producers_Functions_Scale[p,r,f,ro,ko]*Producers_Functions_Scale_Factor[p,r,f,ro,ko]*Producers_Input[p,r,ri,ki] - (if (ro,ko) in Producers_Intermeds[p,r] then Producers_Input[p,r,ro,ko] else Producers_Output[p,r,ro,ko]) >= 0;

