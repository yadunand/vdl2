##### Producer Variables #####
#
# Producers_Input: quantity of the input commodities used
# Producers_Output: quantity of the output commodities produced
#
# Producers_Functions_Input: quantity of the inputs used by the production function
# Producers_Functions_Output: quantity of the outputs produced by the production function
#
##### Notes #####
#
# (1) Input and Output are the primary optimization variables
# (2) Functions_Input and Functions_Output are internal variables
#
##### Example #####
#
# Input[p1, usa, usa, agr];
# Input[p1, usa, eur, agr];
# Input[p1, usa, usa, lab];
# Input[p1, usa, usa, cap];
# Output[p1, usa, usa, agi];
#
# FInput[p1, usa, f1, usa, lab];
# FInput[p1, usa, f1, usa, cap];
# FOutput[p1, usa, f1, usa, kla];
#
# FInput[p1, usa, f2, usa, agr];
# FInput[p1, usa, f2, eur, agr];
# FInput[p1, usa, f2, usa, kla];
# FOutput[p1, usa, f2, usa, agi];
#
# Input[p1, eur, usa, agr];
# Input[p1, eur, eur, agr];
# Input[p1, eur, eur, lab];
# Input[p1, eur, eur, cap];
# Output[p1, eur, eur, agi];
#
# FInput[p1, eur, f1, eur, lab];
# FInput[p1, eur, f1, eur, cap];
# FOutput[p1, eur, f1, eur, kla];
#
# FInput[p1, eur, f2, usa, agr];
# FInput[p1, eur, f2, eur, agr];
# FInput[p1, eur, f2, eur, kla];
# FOutput[p1, eur, f2, eur, agi];
#
# Input[p1, row, usa, agr];
# Input[p1, row, eur, agr];
# Input[p1, row, row, agr];
# Input[p1, row, row, lab];
# Input[p1, row, row, cap];
# Output[p1, row, eur, agi];
#
# FInput[p1, row, f1, row, lab];
# FInput[p1, row, f1, row, cap];
# FOutput[p1, row, f1, row, kla];
#
# FInput[p1, row, f2, usa, agr];
# FInput[p1, row, f2, eur, agr];
# FInput[p1, row, f2, row, agr];
# FInput[p1, row, f2, row, kla];
# FOutput[p1, row, f2, row, agi];
#
#####

var Producers_Input {p in Producers, r in Producers_Regions[p], Producers_Inputs[p,r]} := 1.0;
var Producers_Output {p in Producers, r in Producers_Regions[p], Producers_Outputs[p,r]} := 1.0;

var Producers_Functions_Input {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], Producers_Functions_Inputs[p,r,f]} := 1.0;
var Producers_Functions_Output {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], Producers_Functions_Outputs[p,r,f]} := 1.0;

##### Producer Optimization Problem #####
#
# maximize Producers_Profits {p in Producers, r in Producers_Regions[p]}: 
#   (sum {(ro,ko) in Producers_Outputs[p,r]} ((Producers_Revenues[p,r,ro,ko] - 
#                                              Producers_RRPriceTax_Revenues[p,r,ro,ko]*Producers_RRPriceRate_Revenues[p,r,ro,ko] -
#                                              Producers_ROPriceTax_Revenues[p,r,ro,ko]*Producers_ROPriceRate_Revenues[p,r,ro,ko]
#                                             )*(if ko in Homogenous_Commodities the HPrice[ko] else Price[ro,ko]) - 
#                                             Producers_RRQuantityTax_Revenues[p,r,ro,ko]*Producers_RRQuantityRate_Revenues[p,r,ro,ko] -
#                                             Producers_ROQuantityTax_Revenues[p,r,ro,ko]*Producers_ROQuantityRate_Revenues[p,r,ro,ko]
#                                            )*Producers_Output[p,r,ro,ko] - 
#    sum {(ri,ki) in Producers_Inputs[p,r]}  ((Producers_Expenditures[p,r,ri,ki] +
#                                              Producers_RRPriceTax_Expenditures[p,r,ri,ki]*Producers_RRPriceRate_Expenditures[p,r,ri,ki] +
#                                              Producers_RIPriceTax_Expenditures[p,r,ri,ki]*Producers_RIPriceRate_Expenditures[p,r,ri,ki]
#                                             )*(if ki in Homogenous_Commodities then HPrice[ki] else Price[ri,ki]) + 
#                                             Producers_RRQuantityTax_Expenditures[p,r,ri,ki]*Producers_RRQuantityRate_Expenditures[p,r,ri,ki] +
#                                             Producers_RIQuantityTax_Expenditures[p,r,ri,ki]*Producers_RIQuantityRate_Expenditures[p,r,ri,ki]
#                                            )*Producers_Input[p,r,ri,ki]
#   )/Producers_Scale[p,r];
#
# Producers_Functions_Positive_Def {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Positive[p,r,f]}:
#   Producers_Functions_Output[p,r,f,ro,ko] <= cesp(Producers_Functions_Scale[p,r,f,ro,ko]*Producers_Functions_Scale_Factor[p,r,f,ro,ko], Producers_Functions_Sigma[p,r,f,ro,ko], {(ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0} (Producers_Factors_Index[p,r,ri,ki], Producers_Functions_Share[p,r,f,ro,ko,ri,ki], Producers_Functions_Input[p,r,f,ri,ki]));
#
# Producers_Functions_Leontief_Def {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Leontief[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0}:
#   Producers_Functions_Output[p,r,f,ro,ko] <= Producers_Functions_Scale[p,r,f,ro,ko]*Producers_Functions_Scale_Factor[p,r,f,ro,ko]*Producers_Functions_Input[p,r,f,ri,ki];
#
# Producers_Inputs_Balance_Def {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Inputs[p,r]}:
#   Producers_Input[p,r,rf,kf] >= sum {f in Producers_Functions_Regions[p,r]: (rf,kf) in Producers_Functions_Inputs[p,r,f]} Producers_Functions_Input[p,r,f,rf,kf];
#
# Producers_Outputs_Balance_Def {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Outputs[p,r]}:
#   Producers_Output[p,r,rf,kf] <= sum {f in Producers_Functions_Regions[p,r]: (rf,kf) in Producers_Functions_Outputs[p,r,f]} Producers_Functions_Output[p,r,f,rf,kf];
#
# Producers_Intermeds_Balance_Def {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Intermeds[p,r]}:
#   sum {f in Producers_Functions_Regions[p,r]: (rf,kf) in Producers_Functions_Outputs[p,r,f]} Producers_Functions_Output[p,r,f,rf,kf] >= sum {f in Producers_Functions_Regions[p,r]: (rf,kf) in Producers_Functions_Inputs[p,r,f]} Producers_Functions_Input[p,r,f,rf,kf] ;
#
##### Notes #####
#
# (1) All variables are constrained to be nonnegative
#
##### First-Order Optimality Conditions #####
#
# Producers_Profit: report of producer profit given the optimal PInput and POutput
#
# m_pro_pos: multiplier on Positive production functions
# m_pro_leo: multiplier on Leontief production functions
# m_pro_in_bal: multiplier on input commodity balance constraints
# m_pro_out_bal: multiplier on output commodity balance constraints
# m_pro_inter_bal: multiplier on intermediate commodity balance constraints
#
#####

var Producers_Profit {p in Producers, r in Producers_Regions[p]} := 1.0;

var m_pro_pos {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Positive[p,r,f]} := 1.0;
var m_pro_leo {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Leontief[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0} := 1.0;

var m_pro_in_bal {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Inputs[p,r]} := 1.0;
var m_pro_out_bal {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Outputs[p,r]} := 1.0;
var m_pro_inter_bal {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Intermeds[p,r]} := 1.0;

subject to

Producers_Profits_Def {p in Producers, r in Producers_Regions[p]}: 
  Producers_Profit[p,r] complements Producers_Profit[p,r] =
    sum {(ro,ko) in Producers_Outputs[p,r]} ((Producers_Revenues[p,r,ro,ko] - 
                                              Producers_RRPriceTax_Revenues[p,r,ro,ko]*Producers_RRPriceRate_Revenues[p,r,ro,ko] -
                                              Producers_ROPriceTax_Revenues[p,r,ro,ko]*Producers_ROPriceRate_Revenues[p,r,ro,ko]
                                             )*(if ko in Homogenous_Commodities then HPrice[ko] else Price[ro,ko]) - 
                                             Producers_RRQuantityTax_Revenues[p,r,ro,ko]*Producers_RRQuantityRate_Revenues[p,r,ro,ko] -
                                             Producers_ROQuantityTax_Revenues[p,r,ro,ko]*Producers_ROQuantityRate_Revenues[p,r,ro,ko]
                                            )*Producers_Output[p,r,ro,ko] - 
    sum {(ri,ki) in Producers_Inputs[p,r]}  ((Producers_Expenditures[p,r,ri,ki] + 
                                              Producers_RRPriceTax_Expenditures[p,r,ri,ki]*Producers_RRPriceRate_Expenditures[p,r,ri,ki] +
                                              Producers_RIPriceTax_Expenditures[p,r,ri,ki]*Producers_RIPriceRate_Expenditures[p,r,ri,ki]
                                             )*(if ki in Homogenous_Commodities then HPrice[ki] else Price[ri,ki]) + 
                                             Producers_RRQuantityTax_Expenditures[p,r,ri,ki]*Producers_RRQuantityRate_Expenditures[p,r,ri,ki] +
                                             Producers_RIQuantityTax_Expenditures[p,r,ri,ki]*Producers_RIQuantityRate_Expenditures[p,r,ri,ki]
                                            )*Producers_Input[p,r,ri,ki];

Producers_Tax_Def {rr in Regions}:
  Producers_Tax[rr] complements Producers_Tax[rr] = 
    sum {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]: r == rr} (Producers_RRPriceTax_Revenues[p,r,ro,ko]*Producers_RRPriceRate_Revenues[p,r,ro,ko]*(if ko in Homogenous_Commodities then HPrice[ko] else Price[ro,ko]) + Producers_RRQuantityTax_Revenues[p,r,ro,ko]*Producers_RRQuantityRate_Revenues[p,r,ro,ko])*Producers_Output[p,r,ro,ko] +
    sum {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]: ro == rr} (Producers_ROPriceTax_Revenues[p,r,ro,ko]*Producers_ROPriceRate_Revenues[p,r,ro,ko]*(if ko in Homogenous_Commodities then HPrice[ko] else Price[ro,ko]) + Producers_ROQuantityTax_Revenues[p,r,ro,ko]*Producers_ROQuantityRate_Revenues[p,r,ro,ko])*Producers_Output[p,r,ro,ko] +
   sum {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]: r == rr} (Producers_RRPriceTax_Expenditures[p,r,ri,ki]*Producers_RRPriceRate_Expenditures[p,r,ri,ki]*(if ki in Homogenous_Commodities then HPrice[ki] else Price[ri,ki]) + Producers_RRQuantityTax_Expenditures[p,r,ri,ki]*Producers_RRQuantityRate_Expenditures[p,r,ri,ki])*Producers_Input[p,r,ri,ki] +
   sum {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]: ri == rr} (Producers_RIPriceTax_Expenditures[p,r,ri,ki]*Producers_RIPriceRate_Expenditures[p,r,ri,ki]*(if ki in Homogenous_Commodities then HPrice[ki] else Price[ri,ki]) + Producers_RIQuantityTax_Expenditures[p,r,ri,ki]*Producers_RIQuantityRate_Expenditures[p,r,ri,ki])*Producers_Input[p,r,ri,ki];

Producers_Opt_Input {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]}:
  0 <= Producers_Input[p,r,ri,ki] complements ((Producers_Expenditures[p,r,ri,ki] + 
                                                Producers_RRPriceTax_Expenditures[p,r,ri,ki]*Producers_RRPriceRate_Expenditures[p,r,ri,ki] +
                                                Producers_RIPriceTax_Expenditures[p,r,ri,ki]*Producers_RIPriceRate_Expenditures[p,r,ri,ki]
                                               )*(if ki in Homogenous_Commodities then HPrice[ki] else Price[ri,ki]) +
                                               Producers_RRQuantityTax_Expenditures[p,r,ri,ki]*Producers_RRQuantityRate_Expenditures[p,r,ri,ki] +
                                               Producers_RIQuantityTax_Expenditures[p,r,ri,ki]*Producers_RIQuantityRate_Expenditures[p,r,ri,ki]
                                              )/Producers_Scale[p,r] - m_pro_in_bal[p,r,ri,ki] >= 0;

Producers_Opt_Output {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]}:
  0 <= Producers_Output[p,r,ro,ko] complements m_pro_out_bal[p,r,ro,ko] - ((Producers_Revenues[p,r,ro,ko] - 
                                                                            Producers_RRPriceTax_Revenues[p,r,ro,ko]*Producers_RRPriceRate_Revenues[p,r,ro,ko] -
                                                                            Producers_ROPriceTax_Revenues[p,r,ro,ko]*Producers_ROPriceRate_Revenues[p,r,ro,ko]
                                                                           )*(if ko in Homogenous_Commodities then HPrice[ko] else Price[ro,ko]) - 
                                                                           Producers_RRQuantityTax_Revenues[p,r,ro,ko]*Producers_RRQuantityRate_Revenues[p,r,ro,ko] -
                                                                           Producers_ROQuantityTax_Revenues[p,r,ro,ko]*Producers_ROQuantityRate_Revenues[p,r,ro,ko]
                                                                          )/Producers_Scale[p,r] >= 0;

Producers_Opt_Functions_Input {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ri,ki) in Producers_Functions_Inputs[p,r,f]}:
  0 <= Producers_Functions_Input[p,r,f,ri,ki] complements (if (ri,ki) in Producers_Inputs[p,r] then m_pro_in_bal[p,r,ri,ki]) + (if (ri,ki) in Producers_Intermeds[p,r] then m_pro_inter_bal[p,r,ri,ki]) - sum {(ro,ko) in Producers_Functions_Positive[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0} d_cesp(Producers_Factors_Index[p,r,ri,ki], Producers_Functions_Scale[p,r,f,ro,ko]*Producers_Functions_Scale_Factor[p,r,f,ro,ko], Producers_Functions_Sigma[p,r,f,ro,ko], {(r1,k1) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,r1,k1] > 0} (Producers_Factors_Index[p,r,r1,k1], Producers_Functions_Share[p,r,f,ro,ko,r1,k1], Producers_Functions_Input[p,r,f,r1,k1]))*m_pro_pos[p,r,f,ro,ko] - sum {(ro,ko) in Producers_Functions_Leontief[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0} Producers_Functions_Scale[p,r,f,ro,ko]*Producers_Functions_Scale_Factor[p,r,f,ro,ko]*m_pro_leo[p,r,f,ro,ko,ri,ki] >= 0;

Producers_Opt_Functions_Output {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]}:
  0 <= Producers_Functions_Output[p,r,f,ro,ko] complements (if (ro,ko) in Producers_Functions_Positive[p,r,f] then m_pro_pos[p,r,f,ro,ko]) + (if (ro,ko) in Producers_Functions_Leontief[p,r,f] then sum {(ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0} m_pro_leo[p,r,f,ro,ko,ri,ki]) - (if (ro,ko) in Producers_Outputs[p,r] then m_pro_out_bal[p,r,ro,ko]) - (if (ro,ko) in Producers_Intermeds[p,r] then m_pro_inter_bal[p,r,ro,ko]) >= 0;

Producers_Functions_Positive_Def {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Positive[p,r,f]}:
  0 <= m_pro_pos[p,r,f,ro,ko] complements cesp(Producers_Functions_Scale[p,r,f,ro,ko]*Producers_Functions_Scale_Factor[p,r,f,ro,ko], Producers_Functions_Sigma[p,r,f,ro,ko], {(ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0} (Producers_Factors_Index[p,r,ri,ki], Producers_Functions_Share[p,r,f,ro,ko,ri,ki], Producers_Functions_Input[p,r,f,ri,ki])) - Producers_Functions_Output[p,r,f,ro,ko] >= 0;

Producers_Functions_Leontief_Def {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Leontief[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0}:
  0 <= m_pro_leo[p,r,f,ro,ko,ri,ki] complements Producers_Functions_Scale[p,r,f,ro,ko]*Producers_Functions_Scale_Factor[p,r,f,ro,ko]*Producers_Functions_Input[p,r,f,ri,ki] - Producers_Functions_Output[p,r,f,ro,ko] >= 0;

Producers_Inputs_Balance_Def {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Inputs[p,r]}:
  0 <= m_pro_in_bal[p,r,rf,kf] complements Producers_Input[p,r,rf,kf] - sum {f in Producers_Functions_Regions[p,r]: (rf,kf) in Producers_Functions_Inputs[p,r,f]} Producers_Functions_Input[p,r,f,rf,kf] >= 0;

Producers_Outputs_Balance_Def {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Outputs[p,r]}:
  0 <= m_pro_out_bal[p,r,rf,kf] complements sum {f in Producers_Functions_Regions[p,r]: (rf,kf) in Producers_Functions_Outputs[p,r,f]} Producers_Functions_Output[p,r,f,rf,kf] - Producers_Output[p,r,rf,kf] >= 0;

Producers_Intermeds_Balance_Def {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Intermeds[p,r]}:
  0 <= m_pro_inter_bal[p,r,rf,kf] complements sum {f in Producers_Functions_Regions[p,r]: (rf,kf) in Producers_Functions_Outputs[p,r,f]} Producers_Functions_Output[p,r,f,rf,kf] - sum {f in Producers_Functions_Regions[p,r]: (rf,kf) in Producers_Functions_Inputs[p,r,f]} Producers_Functions_Input[p,r,f,rf,kf] >= 0;

