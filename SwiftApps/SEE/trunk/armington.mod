##### Static CGE Model in Arrow-Debreu Format #####
#
# Producers : maximize profit using a nested CES production function
# Consumers : maximize utility using a nested CES utility function with budget
# Markets   : set prices to clear market (supply equals demand)
# Trade     : complete with Armington bundles
#
# Limitation: Translog and Diewert cost functions not supported
#	    : Perfect substitution functions not supported
#           : Balancing of incomplete Social Accounting Matrix not done
#           : Calibration of disutility functions not done
#
# Extensions: household production functions
#           : transportation sector
#           : government sector
#           : incomplete markets
#           : imperfect competition
#
#####
#
# Fix structure in consumer first order conditions
#   Deal with the preprocessing issues
#
# Add melting rates for inputs from other regions
# Add support for perfect substitutes
# Add taxes, subsidies, and cap-and-trade with safety valve
#
# Notes:
#  (1) Leontief functions in calibrated form have shares of one
#
#####

##### Declare Sets #####
#
# Regions: all regions in the system
# Commodities: all goods and factors
#
##### Derived Sets #####
#
# Region_Commodities_All: all possible commodities in the region
# Region_Commodities_Drop: commodities dropped from the region
# Region_Commodities: final commodities in the region
# Region_Numeraire: all commodities with price fixed to one
# Numeraire: final commodities with price fixed to one
#
##### Notes #####
#

# (1) There must be at least one region
# (2) There must be at least two commodities
# (3) Each region must have at least one commodity
# (4) Each commodity must be referenced by at least on region
# (5) There must be at most one numeraire set
#
#####

set Regions_All ordered dimen 1;
set Regions_Drop within Regions_All default {};
set Regions = Regions_All diff Regions_Drop;

set Commodities_All ordered dimen 1 default {};
set Commodities_Drop within Commodities_All default {};
set Commodities = Commodities_All diff Commodities_Drop;
set Homogenous_Commodities within Commodities_All default {};

set Endogenous_Taxes_All ordered dimen 1 default {};
set Endogenous_Taxes_Drop within Endogenous_Taxes_All default {};
set Endogenous_Taxes = Endogenous_Taxes_All diff Endogenous_Taxes_Drop;

set Region_Commodities_All {r in Regions_All} = Commodities_All;
set Region_Commodities_Drop {r in Regions_All} within Region_Commodities_All[r] default {};
set Region_Commodities {r in Regions} = Region_Commodities_All[r] diff Region_Commodities_Drop[r];

set Region_Numeraire {r in Regions_All} within Region_Commodities_All[r] diff Homogenous_Commodities default {};
set Numeraire {r in Regions} = Region_Numeraire[r] diff Region_Commodities_Drop[r];

var Producers_Tax {r in Regions} := 0.0;
var Consumers_Tax {r in Regions} := 0.0;
var Total_Tax {r in Regions} := 0;

check: card(Regions) > 0;
check: card(Commodities) > 1;
check {r in Regions}: card(Region_Commodities[r]) > 0;
check {k in Commodities}: card({r in Regions: k in Region_Commodities[r]}) > 0;
check: sum {r in Regions} card(Numeraire[r]) < 2;

subject to

Total_Tax_Def {r in Regions}:
  Total_Tax[r] complements Total_Tax[r] = Producers_Tax[r] + Consumers_Tax[r];

##### Market Prices #####
#
# Price: the market price for each commodity
#
##### Notes #####
#
# (1) There is a price associated with each commodity traded in the region
#
#####

var Price {r in Regions, Region_Commodities[r] diff Homogenous_Commodities} := 1.0;
var HPrice {Homogenous_Commodities} := 1.0;

##### Producers #####
#
# Producers_All: the set of all producers
# Producers_Drop: producers dropped
# Producers: final set of producers
#
# Producers_Regions_All: all regions replicating the producer
# Producers_Regions_Drop: regions dropped from replication
# Procucers_Regions: final set of replicating regions
#
# Producers_Local_Inputs: commodities input to the production functions
# Producers_Local_Bundle_Inputs: commodity bundles input to the functions from producer regions
# Producers_Intnl_Bundle_Inputs: commodity bundles input to the functions from all regions
# Producers_Intnl_Inputs: (region, commodity) pairs input to the functions
#
# Producers_Local_Outputs: commodities output by the production functions
# Producers_Local_Bundle_Outputs: commodity bundles output by the functions from producer regions
# Producers_Intnl_Bundle_Outputs: commodity bundles output by the functions from all regions
# Producers_Intnl_Outputs: (region, commodity) pairs output by the functions
#
# Producers_Inter_All: all intermediate commodities input and output by the functions
# Producers_Inter_Drop: dropped intermediate commodities
# Producers_Inter: final intermediate commodities input and output by the functions
#
##### Derived Sets and Parameters #####
#
# Producers_Inputs_All: all inputs to the producer in each region
# Producers_Inputs_Drop: inputs dropped from the producer in each region
# Producers_Inputs: final inputs to the producer in each region
#
# Producers_Outputs_All: all outputs to the producer in each region
# Producers_Outputs_Drop: outputs dropped from the produced in each region
# Producers_Outputs: final outputs to the producer in each region
#
# Producers_Intermeds_All: all intermediate inputs/outputs to the producer in each region
# Producers_Intermeds_Drop: intermediates dropped from the produced in each region
# Producers_Intermeds: final intermediates to the producer in each region
#
# Producers_All_Inputs: possible inputs to the functions
# Producers_All_Outputs: possible outputs to the functions
# Producers_Factors: all possible inputs and outputs to the functions
# Producers_Factors_Index: unique index of each factor
#
##### Notes #####
#
# Checks needed
#   Consistency on the input types
#   Outputs need to be referenced once
#
# (1) At least one producer
# (2) Each producer replicated in at least one region
# (3) Each producer has at least one input in each region
# (4) Each producer has at least one output in each region
# (5) Input and output products are actual products in the regions
# (4) Intermediate products must not be commodities
# (6) Each input can be in multiple input sets
# (7) Each output can be in only one output set to ensure correct replication
#
##### Example #####
#
# Intnl_Inputs = (usa,agr), (eur,agr)
# Local_Inputs = agr, lab, cap
# Local_Outputs = agi
# Inter = kla
#
# Inputs[usa] = (usa,agr), (eur,agr),            (usa,lab), (usa,cap)
# Inputs[eur] = (usa,agr), (eur,agr),            (eur,lab), (eur,cap)
# Inputs[row] = (usa,agr), (eur,agr), (row,agr), (row,lab), (row,cap)
#
# Outputs[usa]= (usa,agi)
# Outputs[eur]= (eur,agi)
# Outputs[row]= (row,agi)
#
# All_Functions_Inputs[usa] = (usa,agr), (eur,agr),            (usa,lab), (usa,cap), (usa,kla)
# All_Functions_Inputs[eur] = (usa,agr), (eur,agr),            (eur,lab), (eur,cap), (eur,kla)
# All_Functions_Inputs[row] = (usa,agr), (eur,agr), (row,agr), (row,lab), (row,cap), (row,kla)
#
# All_Functions_Outputs[usa]= (usa,agi), (usa,kla)
# All_Functions_Outputs[eur]= (eur,agi), (eur,kla)
# All_Functions_Outputs[row]= (row,agi), (row,kla)
#
#####

set Producers_All dimension 1 default {};
set Producers_Drop default {};
set Producers = Producers_All diff Producers_Drop;

set Producers_Regions_All {Producers_All} within Regions_All default Regions_All;
set Producers_Regions_Drop {p in Producers_All} within Producers_Regions_All[p] default {};
set Producers_Regions {p in Producers} = Producers_Regions_All[p] diff Producers_Regions_Drop[p]; 

set Producers_Local_Inputs {Producers_All} within Commodities_All default {};
set Producers_Local_Bundle_Inputs {Producers_All} within Commodities_All default {};
set Producers_Intnl_Bundle_Inputs {Producers_All} within Commodities_All default {};
set Producers_Intnl_Inputs {Producers_All} within Regions_All cross Commodities_All default {};

set Producers_Local_Outputs {Producers_All} within Commodities_All default {};
set Producers_Local_Bundle_Outputs {p in Producers_All} within (Commodities_All diff Producers_Local_Outputs[p]) default {};
set Producers_Intnl_Bundle_Outputs {p in Producers_All} within (Commodities_All diff Producers_Local_Outputs[p] diff Producers_Local_Bundle_Outputs[p]) default {};
set Producers_Intnl_Outputs {p in Producers_All} within (Regions_All cross (Commodities_All diff Producers_Local_Outputs[p] diff Producers_Local_Bundle_Outputs[p] diff Producers_Intnl_Bundle_Outputs[p])) union ((Regions_All diff Producers_Regions_All[p]) cross (Commodities_All diff Producers_Intnl_Bundle_Outputs[p])) default {};

set Producers_Inter_All {Producers_All} default {};
set Producers_Inter_Drop {p in Producers_All} within Producers_Inter_All[p] default {};
set Producers_Inter {p in Producers} = Producers_Inter_All[p] diff Producers_Inter_Drop[p];

set Producers_Inputs_All {p in Producers_All, r in Producers_Regions_All[p]} = Producers_Intnl_Inputs[p] union (setof {k in Producers_Local_Inputs[p]} (r,k)) union (Regions_All cross Producers_Intnl_Bundle_Inputs[p]) union (Producers_Regions_All[p] cross Producers_Local_Bundle_Inputs[p]);
set Producers_Inputs_Drop {p in Producers_All, r in Producers_Regions_All[p]} within Producers_Inputs_All[p,r] default {};
set Producers_Inputs {p in Producers, r in Producers_Regions[p]} = Producers_Inputs_All[p,r] diff Producers_Inputs_Drop[p,r];

set Producers_Outputs_All {p in Producers_All, r in Producers_Regions_All[p]} = Producers_Intnl_Outputs[p] union (setof {k in Producers_Local_Outputs[p]} (r,k)) union (Regions_All cross Producers_Intnl_Bundle_Outputs[p]) union (Producers_Regions_All[p] cross Producers_Local_Bundle_Outputs[p]);
set Producers_Outputs_Drop {p in Producers_All, r in Producers_Regions_All[p]} within Producers_Outputs_All[p,r] default {};
set Producers_Outputs {p in Producers, r in Producers_Regions[p]} = Producers_Outputs_All[p,r] diff Producers_Outputs_Drop[p,r];

set Producers_Intermeds_All {p in Producers_All, r in Producers_Regions_All[p]} = setof {k in Producers_Inter_All[p]} (r,k);
set Producers_Intermeds_Drop {p in Producers_All, r in Producers_Regions_All[p]} within Producers_Intermeds_All[p,r] default {};
set Producers_Intermeds {p in Producers, r in Producers_Regions[p]} = Producers_Intermeds_All[p,r] diff Producers_Intermeds_Drop[p,r];

set Producers_All_Inputs {p in Producers, r in Producers_Regions[p]} = Producers_Inputs[p,r] union Producers_Intermeds[p,r];
set Producers_All_Outputs {p in Producers, r in Producers_Regions[p]} = Producers_Outputs[p,r] union Producers_Intermeds[p,r];
set Producers_Factors {p in Producers, r in Producers_Regions[p]} = Producers_All_Inputs[p,r] union Producers_All_Outputs[p,r];
param Producers_Factors_Index {p in Producers, r in Producers_Regions[p], Producers_Factors[p,r]} = seq();

param Producers_Expenditures_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} >= 0, default 0;
param Producers_Expenditures {p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} >= 0, default 0;

param Producers_RRPriceTax_Expenditures_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} default 0;
param Producers_RRPriceTax_Expenditures {p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} default 0;
param Producers_RRPriceRate_Expenditures_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} default 1;
param Producers_RRPriceRate_Expenditures {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} = Producers_RRPriceRate_Expenditures_All[p,r,ri,ki];

param Producers_RIPriceTax_Expenditures_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} default 0;
param Producers_RIPriceTax_Expenditures {p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} default 0;
param Producers_RIPriceRate_Expenditures_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} default 1;
param Producers_RIPriceRate_Expenditures {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} = Producers_RIPriceRate_Expenditures_All[p,r,ri,ki];

param Producers_RRQuantityTax_Expenditures_All{p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} default 0;
param Producers_RRQuantityTax_Expenditures {p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} default 0;
param Producers_RRQuantityRate_Expenditures_All{p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} default 1;
param Producers_RRQuantityRate_Expenditures {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} = Producers_RRQuantityRate_Expenditures_All[p,r,ri,ki];

param Producers_RIQuantityTax_Expenditures_All{p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} default 0;
param Producers_RIQuantityTax_Expenditures {p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} default 0;
param Producers_RIQuantityRate_Expenditures_All{p in Producers_All, r in Producers_Regions_All[p], Producers_Inputs_All[p,r]} default 1;
param Producers_RIQuantityRate_Expenditures {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} = Producers_RIQuantityRate_Expenditures_All[p,r,ri,ki];

param Producers_RRPriceTax_Revenues_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Outputs_All[p,r]} default 0;
param Producers_RRPriceTax_Revenues {p in Producers_All, r in Producers_Regions_All[p], Producers_Outputs_All[p,r]} default 0;
param Producers_RRPriceRate_Revenues_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Outputs_All[p,r]} default 1;
param Producers_RRPriceRate_Revenues {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} = Producers_RRPriceRate_Revenues_All[p,r,ro,ko];

param Producers_ROPriceTax_Revenues_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Outputs_All[p,r]} default 0;
param Producers_ROPriceTax_Revenues {p in Producers_All, r in Producers_Regions_All[p], Producers_Outputs_All[p,r]} default 0;
param Producers_ROPriceRate_Revenues_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Outputs_All[p,r]} default 1;
param Producers_ROPriceRate_Revenues {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} = Producers_ROPriceRate_Revenues_All[p,r,ro,ko];

param Producers_RRQuantityTax_Revenues_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Outputs_All[p,r]} default 0;
param Producers_RRQuantityTax_Revenues {p in Producers_All, r in Producers_Regions_All[p], Producers_Outputs_All[p,r]} default 0;
param Producers_RRQuantityRate_Revenues_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Outputs_All[p,r]} default 1;
param Producers_RRQuantityRate_Revenues {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} = Producers_RRQuantityRate_Revenues_All[p,r,ro,ko];

param Producers_ROQuantityTax_Revenues_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Outputs_All[p,r]} default 0;
param Producers_ROQuantityTax_Revenues {p in Producers_All, r in Producers_Regions_All[p], Producers_Outputs_All[p,r]} default 0;
param Producers_ROQuantityRate_Revenues_All {p in Producers_All, r in Producers_Regions_All[p], Producers_Outputs_All[p,r]} default 1;
param Producers_ROQuantityRate_Revenues {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} = Producers_ROQuantityRate_Revenues_All[p,r,ro,ko];

param Producers_Revenues {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} = sum {(ri,ki) in Producers_Inputs[p,r]} (Producers_Expenditures[p,r,ri,ki] + Producers_RRPriceTax_Expenditures[p,r,ri,ki] + Producers_RIPriceTax_Expenditures[p,r,ri,ki] + Producers_RRQuantityTax_Expenditures[p,r,ri,ki] + Producers_RIQuantityTax_Expenditures[p,r,ri,ki]) + Producers_RRPriceTax_Revenues[p,r,ro,ko] + Producers_ROPriceTax_Revenues[p,r,ro,ko] + Producers_RRQuantityTax_Revenues[p,r,ro,ko] + Producers_ROQuantityTax_Revenues[p,r,ro,ko];

check: card(Producers) > 0;
check {p in Producers}: card(Producers_Regions[p]) > 0;
check {p in Producers}: card(Commodities inter Producers_Inter[p]) = 0;
check {p in Producers, r in Producers_Regions[p]}: card(Producers_Inputs[p,r]) > 0;
check {p in Producers, r in Producers_Regions[p]}: card(Producers_Outputs[p,r]) > 0;
check {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]}: ki in Region_Commodities[ri];
check {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]}: ko in Region_Commodities[ro];

##### Production Functions #####
#
# Producers_Functions_All: the set of production functions for each producer
# Producers_Functions_Drop: dropped production functions for each producer
# Producers_Functions: final set of production functions for each producer
#
# Producers_Functions_Local_Inputs: commodities input to the production function
# Producers_Functions_Local_Bundle_Inputs: commodity bundles input to the function from producer regions
# Producers_Functions_Intnl_Bundle_Inputs: commodity bundles input to the function from all regions
# Producers_Functions_Intnl_Inputs: (region, commodity) pairs input to function
#
# Producers_Functions_Local_Outputs: commodities output by the production function
# Producers_Functions_Local_Bundle_Outputs: commodity bundles output by the function from producer regions
# Producers_Functions_Intnl_Bundle_Outputs: commodity bundles output by the function from all regions
# Producers_Functions_Intnl_Outputs: (region, commodity) pairs output by the function
#
##### Derived Sets #####
#
# Producers_Functions_Regions_All: all regions for each production function
# Producers_Functions_Regions_Drop: dropped regions for each production function
# Producers_Functions_Regions: final regions for each production function
#
# Producers_Functions_Inputs_All: all inputs to each function for each producer
# Producers_Functions_Inputs_Drop: dropped inputs to each function for each producer
# Producers_Functions_Inputs: final inputs to each function for each producer
#
# Producers_Functions_Outputs_All: all outputs to each function for each producer
# Producers_Functions_Outputs_Drop: dropped outputs to each function for each producer
# Producers_Functions_Outputs: final outputs to each function for each producer
#
##### Notes #####
#
#  (1) Each producer must have at least one production function
#  (2) Each production function must appear in at least one region
#  (3) Each region must have at least one production function
#  (4) Each production function must have at least one input
#  (5) Each production function must have at least one output
#  (6) Each input and intermediate must occur as an input to at least one function
#  (7) Each output and intermediate must occur as an output to at least one function
#
##### Example #####
#
# Functions = f1, f2;
#
# Local_Inputs[f1] = lab, cap;
# Local_Outputs[f1] = kla;
#
# Intnl_Inputs[f2] = (usa,agr), (eur,agr);
# Local_Inputs[f2] = agr, kla;
# Local_Outputs[f2] = agi;
#
# Function_Inputs[usa,f1] = (usa,lab), (usa,cap);
# Function_Inputs[eur,f1] = (eur,lab), (eur,cap);
# Function_Inputs[row,f1] = (row,lab), (eur,cap);
# Function_Outputs[usa,f1] = (usa,kla);
# Function_Outputs[eur,f1] = (eur,kla);
# Function_Outputs[row,f1] = (row,kla);
#
# Function_Inputs[usa,f2] = (usa,agr), (eur,agr), (usa,kla);
# Function_Inputs[eur,f2] = (usa,agr), (eur,agr), (eur,kla);
# Function_Inputs[row,f2] = (usa,agr), (eur,agr), (row,agr), (row,kla);
# Function_Outputs[usa,f2] = (usa,agi);
# Function_Outputs[eur,f2] = (eur,agi);
# Function_Outputs[row,f2] = (row,agi);
#
#####

set Producers_Functions_All {Producers_All} default {};
set Producers_Functions_Drop {p in Producers_All} within Producers_Functions_All[p] default {};
set Producers_Functions {p in Producers} = Producers_Functions_All[p] diff Producers_Functions_Drop[p];

set Producers_Functions_Local_Inputs {p in Producers_All, f in Producers_Functions_All[p]} within (Producers_Local_Inputs[p] union Producers_Inter_All[p]) default {};
set Producers_Functions_Local_Bundle_Inputs {p in Producers_All, f in Producers_Functions_All[p]} within Producers_Local_Bundle_Inputs[p] default {};
set Producers_Functions_Intnl_Bundle_Inputs {p in Producers_All, f in Producers_Functions_All[p]} within Producers_Intnl_Bundle_Inputs[p] default {};
set Producers_Functions_Intnl_Inputs {p in Producers_All, f in Producers_Functions_All[p]} within Producers_Intnl_Inputs[p] default {};

set Producers_Functions_Local_Outputs {p in Producers_All, f in Producers_Functions_All[p]} within (Producers_Local_Outputs[p] union Producers_Inter_All[p]) default {};
set Producers_Functions_Local_Bundle_Outputs {p in Producers_All, f in Producers_Functions_All[p]} within Producers_Local_Bundle_Outputs[p] default {};
set Producers_Functions_Intnl_Bundle_Outputs {p in Producers_All, f in Producers_Functions_All[p]} within Producers_Intnl_Bundle_Outputs[p] default {};
set Producers_Functions_Intnl_Outputs {p in Producers_All, f in Producers_Functions_All[p]} within Producers_Intnl_Outputs[p] default {};

set Producers_Functions_Regions_All {p in Producers_All, r in Producers_Regions_All[p]} = Producers_Functions_All[p];
set Producers_Functions_Regions_Drop {p in Producers_All, r in Producers_Regions_All[p]} within Producers_Functions_Regions_All[p,r] default {};
set Producers_Functions_Regions {p in Producers, r in Producers_Regions[p]} = Producers_Functions_Regions_All[p,r] diff Producers_Functions_Regions_Drop[p,r];

set Producers_Functions_Inputs_All {p in Producers_All, r in Producers_Regions_All[p], f in Producers_Functions_Regions_All[p,r]} = Producers_Functions_Intnl_Inputs[p,f] union (setof {k in Producers_Functions_Local_Inputs[p,f]} (r,k)) union (Regions_All cross Producers_Functions_Intnl_Bundle_Inputs[p,f]) union (Producers_Regions_All[p] cross Producers_Functions_Local_Bundle_Inputs[p,f]);
set Producers_Functions_Inputs_Drop {p in Producers_All, r in Producers_Regions_All[p], f in Producers_Functions_Regions_All[p,r]} within Producers_Functions_Inputs_All[p,r,f] default {};
set Producers_Functions_Inputs {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r]} = Producers_Functions_Inputs_All[p,r,f] diff Producers_Functions_Inputs_Drop[p,r,f];

set Producers_Functions_Outputs_All {p in Producers_All, r in Producers_Regions_All[p], f in Producers_Functions_Regions_All[p,r]} = Producers_Functions_Intnl_Outputs[p,f] union (setof {k in Producers_Functions_Local_Outputs[p,f]} (r,k)) union (Regions_All cross Producers_Functions_Intnl_Bundle_Outputs[p,f]) union (Producers_Regions_All[p] cross Producers_Functions_Local_Bundle_Outputs[p,f]);
set Producers_Functions_Outputs_Drop {p in Producers_All, r in Producers_Regions_All[p], f in Producers_Functions_Regions_All[p,r]} within Producers_Functions_Outputs_All[p,r,f] default {};
set Producers_Functions_Outputs {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r]} = Producers_Functions_Outputs_All[p,r,f] diff Producers_Functions_Outputs_Drop[p,r,f];

check {p in Producers}: card(Producers_Functions[p]) > 0;
check {p in Producers, f in Producers_Functions[p]}: card({r in Producers_Regions[p]: f in Producers_Functions_Regions[p,r]}) > 0;
check {p in Producers, r in Producers_Regions[p]}: card(Producers_Functions_Regions[p,r]) > 0;
check {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r]}: card(Producers_Functions_Inputs[p,r,f]) > 0;
check {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r]}: card(Producers_Functions_Outputs[p,r,f]) > 0;
check {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_All_Inputs[p,r]}: card({f in Producers_Functions_Regions[p,r]: (ri,ki) in Producers_Functions_Inputs[p,r,f]}) > 0;
check {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_All_Outputs[p,r]}: card({f in Producers_Functions_Regions[p,r]: (ro,ko) in Producers_Functions_Outputs[p,r,f]}) > 0;

##### Production Function Parameters #####
#
# Producers_Functions_Local_Sigma: elasticity for each function and local output 
# Producers_Functions_Local_Bundle_Sigma: elasticity for each function and local bundle output 
# Producers_Functions_Intnl_Bundle_Sigma: elasticity for each function and intnl bundle output 
# Producers_Functions_Intnl_Sigma: elasticity for each function and intnl output 
#
# Producers_Functions_Scale: scale factor for each region, function, and output
# Producers_Functions_Share: share factor for each region, function, output, and input
#
##### Derived Sets #####
#
# Producers_Functions_Sigma: elasticity for each region, function and output
# Producers_Functions_Positive: set of ces functions with positive elasticity
# Producers_Functions_Leontief: set of leontief functions
#
##### Notes #####
#
# (1) Sigma constant across regions; one sigma per function per output commodity
# (2) Scale differs across regions; one scale per function per output commodity
# (3) Share differs across regions; one share per function per input commodity
# (4) Leontief functions and other ces functions are treated differently
# (5) Leontief functions are those with zero elasticity of substitution
# (6) Each function must have at least one nonzero share and scale
# (7) Sum of the share parameters must be one
#
##### Definition ####
#
# Let rho = 1 - 1/sigma
# Then output = gamma*(sum_i alpha[i]*input[i]^rho)^(1/rho)
#
#####

param Producers_Functions_Local_Sigma {p in Producers_All, f in Producers_Functions_All[p], Producers_Functions_Local_Outputs[p,f]} >= 0, default 1;
param Producers_Functions_Local_Bundle_Sigma {p in Producers_All, f in Producers_Functions_All[p], Producers_Functions_Local_Bundle_Outputs[p,f]} >= 0, default 1;
param Producers_Functions_Intnl_Bundle_Sigma {p in Producers_All, f in Producers_Functions_All[p], Producers_Functions_Intnl_Bundle_Outputs[p,f]} >= 0, default 1;
param Producers_Functions_Intnl_Sigma {p in Producers_All, f in Producers_Functions_All[p], Producers_Functions_Intnl_Outputs[p,f]} >= 0, default 1;
param Producers_Functions_Sigma {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]} = (if (ro,ko) in Producers_Functions_Intnl_Outputs[p,f] then Producers_Functions_Intnl_Sigma[p,f,ro,ko] else (if ko in Producers_Functions_Intnl_Bundle_Outputs[p,f] then Producers_Functions_Intnl_Bundle_Sigma[p,f,ko] else (if ko in Producers_Functions_Local_Bundle_Outputs[p,f] then Producers_Functions_Local_Bundle_Sigma[p,f,ko] else Producers_Functions_Local_Sigma[p,f,ko])));

param Producers_Functions_Scale {p in Producers_All, r in Producers_Regions_All[p], f in Producers_Functions_Regions_All[p,r], (ro,ko) in Producers_Functions_Outputs_All[p,r,f]} >= 0, default 1;
param Producers_Functions_Scale_Factor {p in Producers_All, r in Producers_Regions_All[p], f in Producers_Functions_Regions_All[p,r], (ro,ko) in Producers_Functions_Outputs_All[p,r,f]} >= 0, default 1;

param Producers_Functions_Share {p in Producers_All, r in Producers_Regions_All[p], f in Producers_Functions_Regions_All[p,r], (ro,ko) in Producers_Functions_Outputs_All[p,r,f], (ri,ki) in Producers_Functions_Inputs_All[p,r,f]} >= 0, default 0;

set Producers_Functions_Leontief {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r]} = {(ro,ko) in Producers_Functions_Outputs[p,r,f]: Producers_Functions_Sigma[p,r,f,ro,ko] = 0};
set Producers_Functions_Positive {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r]} = {(ro,ko) in Producers_Functions_Outputs[p,r,f]: Producers_Functions_Sigma[p,r,f,ro,ko] > 0};

check {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r]}: card({(ro,ko) in Producers_Functions_Outputs[p,r,f]: Producers_Functions_Scale[p,r,f,ro,ko] > 0}) > 0;
check {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]}: card({(ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0}) > 0;
check {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]}: abs((sum {(ri,ki) in Producers_Functions_Inputs[p,r,f]} Producers_Functions_Share[p,r,f,ro,ko,ri,ki]) - 1.0) <= 1e-10;

param Producers_Scale {p in Producers, r in Producers_Regions[p]} := 1; # sum {(ri,ki) in Producers_Inputs[p,r]} Producers_Expenditures[p,r,ri,ki];

include subproblems/producer_tree.mod;

##### Consumers #####
#
# Consumers_All: the set of all consumers
# Consumers_Drop: consumers dropped
# Consumers: final set of consumers
#
# Consumers_Regions_All: all regions replicating the consumer
# Consumers_Regions_Drop: regions dropped from replication
# Procucers_Regions: final set of replicating regions
#
# Consumers_Local_Demands: local commodities demanded
# Consumers_Local_Bundle_Demands: commodity bundles demanded from consumer regions
# Consumers_Intnl_Bundle_Demands: commodity bundles demanded from all regions
# Consumers_Intnl_Demands: (region, commodity) pairs demanded
#
# Consumers_Local_Supplies: commodities supplied
# Consumers_Local_Bundle_Supplies: commodity bundles supplied to consumer regions
# Consumers_Intnl_Bundle_Supplies: commodity bundles supplied to all regions
# Consumers_Intnl_Supplies: (region, commodity) pairs supplied
#
# Consumers_Utilities_Inter_All: all intermediate utility input and output
# Consumers_Utilities_Inter_Drop: dropped intermediate utilities
# Consumers_Utilities_Inter: final intermediate utilities
#
# Consumers_Utilities_Objec_All: all intermediates used in objective function
# Consumers_Utilities_Objec_Drop: dropped objective intermediates
# Consumers_Utilities_Objec: final intermediates used in objective function
#
# Consumers_Disutilities_Inter_All: all intermediate disutility input and output
# Consumers_Disutilities_Inter_Drop: dropped intermediate disutilities
# Consumers_Disutilities_Inter: final intermediate disutilities
#
# Consumers_Disutilities_Objec_All: all intermediates used in objective function
# Consumers_Disutilities_Objec_Drop: dropped objective intermediates
# Consumers_Disutilities_Objec: final intermediates used in objective function
#
##### Derived Sets and Parameters #####
#
# Consumers_Demands_All: all commodities demanded by the consumer in each region
# Consumers_Demands_Drop: demand commodities dropped in each region
# Consumers_Demands: final commodities demanded by the consumer in each region
#
# Consumers_Supplies_All: all commodities supplied by the consumer in each region
# Consumers_Supplies_Drop: supply commodities dropped in each region
# Consumers_Supplies: final commodities supplied by the consumer in each region
#
# Consumers_Utilities_Intermeds_All: all utility intermediates for the consumer in each region
# Consumers_Utilities_Intermeds_Drop: utility intermediates dropped in each region
# Consumers_Utilities_Intermeds: final utility intermediates for the consumer in each region
#
# Consumers_Disutilities_Intermeds_All: all disutility intermediates for the consumer in each region
# Consumers_Disutilities_Intermeds_Drop: disutility intermediates dropped in each region
# Consumers_Disutilities_Intermeds: final disutility intermediates for the consumer in each region
#
# Consumers_Utilities_Objectives_All: all utility objectives for the consumer in each region
# Consumers_Utilities_Objectives_Drop: utility objectives dropped in each region
# Consumers_Utilities_Objectives: final utility objectives for the consumer in each region
#
# Consumers_Disutilities_Objectives_All: all disutility objectives for the consumer in each region
# Consumers_Disutilities_Objectives_Drop: disutility objectives dropped in each region
# Consumers_Disutilities_Objectives: final disutility objectives for the consumer in each region
#
# Consumers_Utilities_Objectives_Weight: weight of utility output in objective function
# Consumers_Disutilities_Objectives_Weight: weight of disutility output in objective function
#
# Consumers_All_Inputs: all inputs to the functions in each region
# Consumers_All_Outputs: all outputs to the functions in each region
# Consumers_Factors: all inputs and outputs to the functions in each region
# Consumers_Factors_Index: index used when referencing factors
#
##### Notes #####
#
# Checks needed
#   Consistency on the demands types
#   Supplies need to be referenced once
#
# (1) At least one consumer
# (2) Each consumer replicated in at least one region
# (3) Each consumer has at least one commodity demanded in each region
# (4) Each consumer has at least one commodity supplied in each region
# (5) Each consumer has at least one term in the objective function
# (6) Supply and demand commodities must be disjoint
# (7) Intermediates must not overlap with commodities
# (8) Utility and disutility intermediates must be disjoint
#
##### Example #####
#
# Intnl_Demands = (usa,agr), (eur,agr)
# Local_Demands = agr, agi, sav
# Local_Supplies = lab, cap
#
# Demands[usa] = (usa,agr), (eur,agr),            (usa,agi), (usa,sav)
# Demands[eur] = (usa,agr), (eur,agr),            (eur,agi), (eur,sav)
# Demands[row] = (usa,agr), (eur,agr), (row,agr), (row,agi), (eur,sav)
#
# Supplies[usa]= (usa,lab), (usa,cap)
# Supplies[eur]= (eur,lab), (eur,cap)
# Supplies[row]= (row,lab), (row,cap)
#
# All_Demands[usa] = (usa,agr), (eur,agr),            (usa,agi), (usa,sav)
# All_Demands[eur] = (usa,agr), (eur,agr),            (eur,agi), (eur,sav)
# All_Demands[row] = (usa,agr), (eur,agr), (row,agr), (row,agi), (row,sav)
#
# All_Supplies[usa]= (usa,lab), (usa,cap)
# All_Supplies[eur]= (eur,lab), (eur,cap)
# All_Supplies[row]= (row,lab), (row,cap)
#
#####

set Consumers_All default {};
set Consumers_Drop default {};
set Consumers = Consumers_All diff Consumers_Drop;

set Consumers_Regions_All {Consumers_All} within Regions default Regions;
set Consumers_Regions_Drop {c in Consumers_All} within Consumers_Regions_All[c] default {};
set Consumers_Regions {c in Consumers} = Consumers_Regions_All[c] diff Consumers_Regions_Drop[c]; 

set Consumers_Local_Demands {Consumers_All} within Commodities_All default {};
set Consumers_Local_Bundle_Demands {Consumers_All} within Commodities_All default {};
set Consumers_Intnl_Bundle_Demands {Consumers_All} within Commodities_All default {};
set Consumers_Intnl_Demands {Consumers_All} within Regions_All cross Commodities_All default {};

set Consumers_Local_Supplies {Consumers_All} within Commodities_All default {};
set Consumers_Local_Bundle_Supplies {c in Consumers_All} within Commodities_All default {};
set Consumers_Intnl_Bundle_Supplies {c in Consumers_All} within Commodities_All default {};
set Consumers_Intnl_Supplies {Consumers_All} within Regions_All cross Commodities_All default {};

set Consumers_Utilities_Inter_All {Consumers_All} default {};
set Consumers_Utilities_Inter_Drop {c in Consumers_All} within Consumers_Utilities_Inter_All[c] default {};
set Consumers_Utilities_Inter {c in Consumers} = Consumers_Utilities_Inter_All[c] diff Consumers_Utilities_Inter_Drop[c];

set Consumers_Utilities_Objec_All {c in Consumers_All} within Consumers_Utilities_Inter_All[c] default {};
set Consumers_Utilities_Objec_Drop {c in Consumers_All} within Consumers_Utilities_Objec_All[c] default {};
set Consumers_Utilities_Objec {c in Consumers} = Consumers_Utilities_Objec_All[c] diff Consumers_Utilities_Objec_Drop[c];

set Consumers_Disutilities_Inter_All {Consumers_All} default {};
set Consumers_Disutilities_Inter_Drop {c in Consumers_All} within Consumers_Disutilities_Inter_All[c] default {};
set Consumers_Disutilities_Inter {c in Consumers} = Consumers_Disutilities_Inter_All[c] diff Consumers_Disutilities_Inter_Drop[c];

set Consumers_Disutilities_Objec_All {c in Consumers_All} within Consumers_Disutilities_Inter_All[c] default {};
set Consumers_Disutilities_Objec_Drop {c in Consumers_All} within Consumers_Disutilities_Objec_All[c] default {};
set Consumers_Disutilities_Objec {c in Consumers} = Consumers_Disutilities_Objec_All[c] diff Consumers_Disutilities_Objec_Drop[c];

set Consumers_Demands_All {c in Consumers_All, r in Consumers_Regions_All[c]} = Consumers_Intnl_Demands[c] union (setof {k in Consumers_Local_Demands[c]} (r,k)) union (Regions cross Consumers_Intnl_Bundle_Demands[c]) union (Consumers_Regions[c] cross Consumers_Local_Bundle_Demands[c]);
set Consumers_Demands_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Demands_All[c,r] default {};
set Consumers_Demands {c in Consumers, r in Consumers_Regions[c]} = Consumers_Demands_All[c,r] diff Consumers_Demands_Drop[c,r];

set Consumers_Supplies_All {c in Consumers_All, r in Consumers_Regions_All[c]} = Consumers_Intnl_Supplies[c] union (setof {k in Consumers_Local_Supplies[c]} (r,k)) union (Regions cross Consumers_Intnl_Bundle_Supplies[c]) union (Consumers_Regions[c] cross Consumers_Local_Bundle_Supplies[c]);
set Consumers_Supplies_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Supplies_All[c,r] default {};
set Consumers_Supplies {c in Consumers, r in Consumers_Regions[c]} = Consumers_Supplies_All[c,r] diff Consumers_Supplies_Drop[c,r];

set Consumers_Utilities_Intermeds_All {c in Consumers_All, r in Consumers_Regions_All[c]} = setof {k in Consumers_Utilities_Inter_All[c]} (r,k);
set Consumers_Utilities_Intermeds_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Utilities_Intermeds_All[c,r] default {};
set Consumers_Utilities_Intermeds {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Intermeds_All[c,r] diff Consumers_Utilities_Intermeds_Drop[c,r];

set Consumers_Disutilities_Intermeds_All {c in Consumers_All, r in Consumers_Regions_All[c]} = setof {k in Consumers_Disutilities_Inter_All[c]} (r,k);
set Consumers_Disutilities_Intermeds_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Disutilities_Intermeds_All[c,r] default {};
set Consumers_Disutilities_Intermeds {c in Consumers, r in Consumers_Regions[c]} = Consumers_Disutilities_Intermeds_All[c,r] diff Consumers_Disutilities_Intermeds_Drop[c,r];

set Consumers_Utilities_Objectives_All {c in Consumers_All, r in Consumers_Regions_All[c]} = setof {k in Consumers_Utilities_Objec_All[c]} (r,k);
set Consumers_Utilities_Objectives_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Utilities_Objectives_All[c,r] default {};
set Consumers_Utilities_Objectives {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Objectives_All[c,r] diff Consumers_Utilities_Objectives_Drop[c,r];

set Consumers_Disutilities_Objectives_All {c in Consumers_All, r in Consumers_Regions_All[c]} = setof {k in Consumers_Disutilities_Objec_All[c]} (r,k);
set Consumers_Disutilities_Objectives_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Disutilities_Objectives_All[c,r] default {};
set Consumers_Disutilities_Objectives {c in Consumers, r in Consumers_Regions[c]} = Consumers_Disutilities_Objectives_All[c,r] diff Consumers_Disutilities_Objectives_Drop[c,r];

param Consumers_Utilities_Objectives_Weight {c in Consumers_All, r in Consumers_Regions_All[c], (ro,ko) in Consumers_Utilities_Objectives_All[c,r]} >= 0, default 1;
param Consumers_Disutilities_Objectives_Weight {c in Consumers_All, r in Consumers_Regions_All[c], (ro,ko) in Consumers_Disutilities_Objectives_All[c,r]} >= 0, default 1;

set Consumers_All_Inputs {c in Consumers, r in Consumers_Regions[c]} = Consumers_Supplies[c,r] union Consumers_Demands[c,r] union Consumers_Utilities_Intermeds[c,r] union Consumers_Disutilities_Intermeds[c,r];
set Consumers_All_Outputs {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Intermeds[c,r] union Consumers_Disutilities_Intermeds[c,r];
set Consumers_Factors {c in Consumers, r in Consumers_Regions[c]} = Consumers_All_Inputs[c,r] union Consumers_All_Outputs[c,r];
param Consumers_Factors_Index {c in Consumers, r in Consumers_Regions[c], Consumers_Factors[c,r]} = seq();

param Consumers_Expenditures_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} >= 0, default 0;
param Consumers_Expenditures {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} >= 0, default 0;

param Consumers_RRPriceTax_Expenditures_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} default 0;
param Consumers_RRPriceTax_Expenditures {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} default 0;
param Consumers_RRPriceRate_Expenditures_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} default 1;
param Consumers_RRPriceRate_Expenditures {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} = Consumers_RRPriceRate_Expenditures_All[c,r,rd,kd];

param Consumers_RDPriceTax_Expenditures_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} default 0;
param Consumers_RDPriceTax_Expenditures {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} default 0;
param Consumers_RDPriceRate_Expenditures_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} default 1;
param Consumers_RDPriceRate_Expenditures {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} = Consumers_RDPriceRate_Expenditures_All[c,r,rd,kd];

param Consumers_RRQuantityTax_Expenditures_All{c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} default 0;
param Consumers_RRQuantityTax_Expenditures {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} default 0;
param Consumers_RRQuantityRate_Expenditures_All{c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} default 1;
param Consumers_RRQuantityRate_Expenditures {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} = Consumers_RRQuantityRate_Expenditures_All[c,r,rd,kd];

param Consumers_RDQuantityTax_Expenditures_All{c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} default 0;
param Consumers_RDQuantityTax_Expenditures {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} default 0;
param Consumers_RDQuantityRate_Expenditures_All{c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Demands_All[c,r]} default 1;
param Consumers_RDQuantityRate_Expenditures {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} = Consumers_RDQuantityRate_Expenditures_All[c,r,rd,kd];

param Consumers_Revenues_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} > 0, default 0;
param Consumers_Revenues {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} > 0, default 0;

param Consumers_RRPriceTax_Revenues_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} default 0;
param Consumers_RRPriceTax_Revenues {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} default 0;
param Consumers_RRPriceRate_Revenues_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} default 1;
param Consumers_RRPriceRate_Revenues {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]} = Consumers_RRPriceRate_Revenues_All[c,r,rs,ks];

param Consumers_RSPriceTax_Revenues_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} default 0;
param Consumers_RSPriceTax_Revenues {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} default 0;
param Consumers_RSPriceRate_Revenues_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} default 1;
param Consumers_RSPriceRate_Revenues {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]} = Consumers_RSPriceRate_Revenues_All[c,r,rs,ks];

param Consumers_RRQuantityTax_Revenues_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} default 0;
param Consumers_RRQuantityTax_Revenues {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} default 0;
param Consumers_RRQuantityRate_Revenues_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} default 1;
param Consumers_RRQuantityRate_Revenues {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]} = Consumers_RRQuantityRate_Revenues_All[c,r,rs,ks];

param Consumers_RSQuantityTax_Revenues_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} default 0;
param Consumers_RSQuantityTax_Revenues {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} default 0;
param Consumers_RSQuantityRate_Revenues_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Supplies_All[c,r]} default 1;
param Consumers_RSQuantityRate_Revenues {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]} = Consumers_RSQuantityRate_Revenues_All[c,r,rs,ks];

param Consumers_Scale {c in Consumers, r in Consumers_Regions[c]} := 1; # sum {(rs,ks) in Consumers_Supplies[c,r]} Consumers_Revenues[c,r,rs,ks];

check: card(Consumers) > 0;
check {c in Consumers}: card(Consumers_Regions[c]) > 0;
check {c in Consumers, r in Consumers_Regions[c]}: card(Consumers_Demands[c,r]) + card(Consumers_Supplies[c,r]) > 0;
check {c in Consumers, r in Consumers_Regions[c]}: card(Consumers_Utilities_Objectives[c,r]) + card(Consumers_Disutilities_Objectives[c,r]) > 0;
check {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Demands[c,r]}: ki in Region_Commodities[ri];
check {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Supplies[c,r]}: ko in Region_Commodities[ro];
check {c in Consumers, r in Consumers_Regions[c]}: card(Consumers_Demands[c,r] inter Consumers_Supplies[c,r]) = 0;
check {c in Consumers}: card(Consumers_Utilities_Inter[c] inter Commodities) = 0;
check {c in Consumers}: card(Consumers_Disutilities_Inter[c] inter Commodities) = 0;
check {c in Consumers}: card(Consumers_Utilities_Inter[c] inter Consumers_Disutilities_Inter[c]) = 0;

check {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Utilities_Objectives[c,r]}: Consumers_Utilities_Objectives_Weight[c,r,ro,ko] > 0;
check {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Disutilities_Objectives[c,r]}: Consumers_Disutilities_Objectives_Weight[c,r,ro,ko] > 0;

##### Utility Functions #####
#
# Consumers_Utilities_All: the set of utility functions for each consumer
# Consumers_Utilities_Drop: dropped utility functions for each consumer
# Consumers_Utilities: final set of utility functions for each consumer
#
# Consumers_Utilities_Local_Inputs: commodities input to the utility function
# Consumers_Utilities_Local_Bundle_Inputs: commodity bundles input to the function from consumer regions
# Consumers_Utilities_Intnl_Bundle_Inputs: commodity bundles input to the function from all regions
# Consumers_Utilities_Intnl_Inputs: (region, commodity) pairs input to function
#
# Consumers_Utilities_Local_Outputs: intermediate utilities output by the utility function
#
##### Derived Sets #####
#
# Consumers_Utilities_Regions_All: all regions for each utility function
# Consumers_Utilities_Regions_Drop: dropped regions for each utility function
# Consumers_Utilities_Regions: final regions for each utility function
#
# Consumers_Utilities_Inputs_All: all inputs to each function for each consumer
# Consumers_Utilities_Inputs_Drop: dropped inputs to each function for each consumer
# Consumers_Utilities_Inputs: final inputs to each function for each consumer
#
# Consumers_Utilities_Outputs_All: all outputs to each function for each consumer
# Consumers_Utilities_Outputs_Drop: dropped outputs to each function for each consumer
# Consumers_Utilities_Outputs: final outputs to each function for each consumer
#
##### Notes #####
#
# (1) Each utility function must appear in at least one region
# (2) Each utility function must have at least one input
# (3) Each utility function must have one output
# (4) Each utility intermediate must occur as an output to one function
#
#####

set Consumers_Utilities_All {Consumers_All} default {};
set Consumers_Utilities_Drop {c in Consumers_All} within Consumers_Utilities_All[c] default {};
set Consumers_Utilities {c in Consumers} = Consumers_Utilities_All[c] diff Consumers_Utilities_Drop[c];

set Consumers_Utilities_Local_Inputs {c in Consumers_All, f in Consumers_Utilities_All[c]} within (Consumers_Local_Demands[c] union Consumers_Local_Supplies[c] union Consumers_Utilities_Inter_All[c] union Consumers_Disutilities_Inter_All[c]) default {};
set Consumers_Utilities_Local_Bundle_Inputs {c in Consumers_All, f in Consumers_Utilities_All[c]} within (Consumers_Local_Bundle_Demands[c] union Consumers_Local_Bundle_Supplies[c]) default {};
set Consumers_Utilities_Intnl_Bundle_Inputs {c in Consumers_All, f in Consumers_Utilities_All[c]} within (Consumers_Intnl_Bundle_Demands[c] union Consumers_Intnl_Bundle_Supplies[c]) default {};
set Consumers_Utilities_Intnl_Inputs {c in Consumers_All, f in Consumers_Utilities_All[c]} within (Consumers_Intnl_Demands[c] union Consumers_Intnl_Supplies[c]) default {};

set Consumers_Utilities_Local_Outputs {c in Consumers_All, f in Consumers_Utilities_All[c]} within Consumers_Utilities_Inter_All[c] default {};

set Consumers_Utilities_Regions_All {c in Consumers_All, r in Consumers_Regions_All[c]} = Consumers_Utilities_All[c];
set Consumers_Utilities_Regions_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Utilities_Regions_All[c,r] default {};
set Consumers_Utilities_Regions {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Regions_All[c,r] diff Consumers_Utilities_Regions_Drop[c,r];

set Consumers_Utilities_Inputs_All {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Utilities_Regions_All[c,r]} = Consumers_Utilities_Intnl_Inputs[c,f] union (setof {k in Consumers_Utilities_Local_Inputs[c,f]} (r,k)) union (Regions_All cross Consumers_Utilities_Intnl_Bundle_Inputs[c,f]) union (Consumers_Regions_All[c] cross Consumers_Utilities_Local_Bundle_Inputs[c,f]);
set Consumers_Utilities_Inputs_Drop {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Utilities_Regions_All[c,r]} within Consumers_Utilities_Inputs_All[c,r,f] default {};
set Consumers_Utilities_Inputs {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r]} = Consumers_Utilities_Inputs_All[c,r,f] diff Consumers_Utilities_Inputs_Drop[c,r,f];

set Consumers_Utilities_Outputs_All {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Utilities_Regions_All[c,r]} = setof {k in Consumers_Utilities_Local_Outputs[c,f]} (r,k);
set Consumers_Utilities_Outputs_Drop {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Utilities_Regions_All[c,r]} within Consumers_Utilities_Outputs_All[c,r,f] default {};
set Consumers_Utilities_Outputs {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r]} = Consumers_Utilities_Outputs_All[c,r,f] diff Consumers_Utilities_Outputs_Drop[c,r,f];

check {c in Consumers, f in Consumers_Utilities[c]}: card({r in Consumers_Regions[c]: f in Consumers_Utilities_Regions[c,r]}) > 0;
check {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r]}: card(Consumers_Utilities_Inputs[c,r,f]) > 0;
check {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r]}: card(Consumers_Utilities_Outputs[c,r,f]) = 1;
check {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Utilities_Intermeds[c,r]}: card({f in Consumers_Utilities_Regions[c,r]: (ro,ko) in Consumers_Utilities_Outputs[c,r,f]}) = 1;

check {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Utilities_Objectives[c,r]}: (ro,ko) in Consumers_Utilities_Intermeds[c,r];

##### Utility Function Parameters #####
#
# Consumers_Utilities_Local_Sigma: elasticity for each function and local output 
# Consumers_Utilities_Scale: scale factor for each region, function, and output
# Consumers_Utilities_Share: share factor for each region, function, output, and input
#
##### Derived Sets #####
#
# Consumers_Utilities_Sigma: elasticity for each region, function and output
# Consumers_Utilities_Positive: set of ces functions with positive elasticity
# Consumers_Utilities_Leontief: set of leontief functions
#
##### Notes #####
#
# (1) Sigma differs across regions; one sigma per function per output commodity
# (2) Scale differs across regions; one scale per function per output commodity
# (3) Share differs across regions; one share per function per input commodity
# (4) Leontief functions and other ces functions are treated differently
# (5) Leontief functions are those with zero elasticity of substitution
# (6) Each function must have at least one nonzero share and scale
# (7) Sum of the share parameters must be one
#
##### Definition ####
#
# Let rho = 1 - 1/sigma
# Then output = gamma*(sum_i alpha[i]*input[i]^rho)^(1/rho)
#
#####

param Consumers_Utilities_Local_Sigma {c in Consumers_All, r in Consumers_Regions[c], f in Consumers_Utilities_All[c], Consumers_Utilities_Local_Outputs[c,f]} >= 0, default 1;
param Consumers_Utilities_Sigma {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f]} = Consumers_Utilities_Local_Sigma[c,r,f,ko];

param Consumers_Utilities_Scale {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Utilities_Regions_All[c,r], (ro,ko) in Consumers_Utilities_Outputs_All[c,r,f]} >= 0, default 1;

param Consumers_Utilities_Share {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Utilities_Regions_All[c,r], (ro,ko) in Consumers_Utilities_Outputs_All[c,r,f], (ri,ki) in Consumers_Utilities_Inputs_All[c,r,f]} >= 0, default 0;

set Consumers_Utilities_Leontief {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r]} = {(ro,ko) in Consumers_Utilities_Outputs[c,r,f]: Consumers_Utilities_Sigma[c,r,f,ro,ko] = 0};
set Consumers_Utilities_Positive {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r]} = {(ro,ko) in Consumers_Utilities_Outputs[c,r,f]: Consumers_Utilities_Sigma[c,r,f,ro,ko] > 0};

check {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r]}: card({(ro,ko) in Consumers_Utilities_Outputs[c,r,f]: Consumers_Utilities_Scale[c,r,f,ro,ko] > 0}) > 0;
check {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f]}: card({(ri,ki) in Consumers_Utilities_Inputs[c,r,f]: Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] > 0}) > 0;
check {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f]}: abs((sum {(ri,ki) in Consumers_Utilities_Inputs[c,r,f]} Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki]) - 1.0) <= 1e-10;

##### Disutility Functions #####
#
# Consumers_Disutilities_All: the set of disutility functions for each consumer
# Consumers_Disutilities_Drop: dropped disutility functions for each consumer
# Consumers_Disutilities: final set of disutility functions for each consumer
#
# Consumers_Disutilities_Local_Inputs: commodities input to the disutility function
# Consumers_Disutilities_Local_Bundle_Inputs: commodity bundles input to the function from consumer regions
# Consumers_Disutilities_Intnl_Bundle_Inputs: commodity bundles input to the function from all regions
# Consumers_Disutilities_Intnl_Inputs: (region, commodity) pairs input to function
#
# Consumers_Disutilities_Local_Outputs: intermediate disutilities output by the disutility function
#
##### Derived Sets #####
#
# Consumers_Disutilities_Regions_All: all regions for each disutility function
# Consumers_Disutilities_Regions_Drop: dropped regions for each disutility function
# Consumers_Disutilities_Regions: final regions for each disutility function
#
# Consumers_Disutilities_Inputs_All: all inputs to each function for each consumer
# Consumers_Disutilities_Inputs_Drop: dropped inputs to each function for each consumer
# Consumers_Disutilities_Inputs: final inputs to each function for each consumer
#
# Consumers_Disutilities_Outputs_All: all outputs to each function for each consumer
# Consumers_Disutilities_Outputs_Drop: dropped outputs to each function for each consumer
# Consumers_Disutilities_Outputs: final outputs to each function for each consumer
#
##### Notes #####
#
# (1) Each disutility function must appear in at least one region
# (2) Each disutility function must have at least one input
# (3) Each disutility function must have one output
# (4) Each disutility intermediate must occur as an output to one function
#
#####

set Consumers_Disutilities_All {Consumers_All} default {};
set Consumers_Disutilities_Drop {c in Consumers_All} within Consumers_Disutilities_All[c] default {};
set Consumers_Disutilities {c in Consumers} = Consumers_Disutilities_All[c] diff Consumers_Disutilities_Drop[c];

set Consumers_Disutilities_Local_Inputs {c in Consumers_All, f in Consumers_Disutilities_All[c]} within (Consumers_Local_Demands[c] union Consumers_Local_Supplies[c] union Consumers_Utilities_Inter_All[c] union Consumers_Disutilities_Inter[c]) default {};
set Consumers_Disutilities_Local_Bundle_Inputs {c in Consumers_All, f in Consumers_Disutilities_All[c]} within (Consumers_Local_Bundle_Demands[c] union Consumers_Local_Bundle_Supplies[c]) default {};
set Consumers_Disutilities_Intnl_Bundle_Inputs {c in Consumers_All, f in Consumers_Disutilities_All[c]} within (Consumers_Intnl_Bundle_Demands[c] union Consumers_Intnl_Bundle_Supplies[c]) default {};
set Consumers_Disutilities_Intnl_Inputs {c in Consumers_All, f in Consumers_Disutilities_All[c]} within (Consumers_Intnl_Demands[c] union Consumers_Intnl_Supplies[c]) default {};

set Consumers_Disutilities_Local_Outputs {c in Consumers_All, f in Consumers_Disutilities_All[c]} within Consumers_Disutilities_Inter[c] default {};

set Consumers_Disutilities_Regions_All {c in Consumers_All, r in Consumers_Regions_All[c]} = Consumers_Disutilities_All[c];
set Consumers_Disutilities_Regions_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Disutilities_Regions_All[c,r] default {};
set Consumers_Disutilities_Regions {c in Consumers, r in Consumers_Regions[c]} = Consumers_Disutilities_Regions_All[c,r] diff Consumers_Disutilities_Regions_Drop[c,r];

set Consumers_Disutilities_Inputs_All {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Disutilities_Regions_All[c,r]} = Consumers_Disutilities_Intnl_Inputs[c,f] union (setof {k in Consumers_Disutilities_Local_Inputs[c,f]} (r,k)) union (Regions_All cross Consumers_Disutilities_Intnl_Bundle_Inputs[c,f]) union (Consumers_Regions_All[c] cross Consumers_Disutilities_Local_Bundle_Inputs[c,f]);
set Consumers_Disutilities_Inputs_Drop {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Disutilities_Regions_All[c,r]} within Consumers_Disutilities_Inputs_All[c,r,f] default {};
set Consumers_Disutilities_Inputs {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r]} = Consumers_Disutilities_Inputs_All[c,r,f] diff Consumers_Disutilities_Inputs_Drop[c,r,f];

set Consumers_Disutilities_Outputs_All {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Disutilities_Regions_All[c,r]} = setof {k in Consumers_Disutilities_Local_Outputs[c,f]} (r,k);
set Consumers_Disutilities_Outputs_Drop {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Disutilities_Regions_All[c,r]} within Consumers_Disutilities_Outputs_All[c,r,f] default {};
set Consumers_Disutilities_Outputs {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r]} = Consumers_Disutilities_Outputs_All[c,r,f] diff Consumers_Disutilities_Outputs_Drop[c,r,f];

check {c in Consumers, f in Consumers_Disutilities[c]}: card({r in Consumers_Regions[c]: f in Consumers_Disutilities_Regions[c,r]}) > 0;
check {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r]}: card(Consumers_Disutilities_Inputs[c,r,f]) > 0;
check {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r]}: card(Consumers_Disutilities_Outputs[c,r,f]) = 1;
check {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Disutilities_Intermeds[c,r]}: card({f in Consumers_Disutilities_Regions[c,r]: (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]}) = 1;

check {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Disutilities_Objectives[c,r]}: (ro,ko) in Consumers_Disutilities_Intermeds[c,r];

##### Disutility Function Parameters #####
#
# Consumers_Disutilities_Local_Sigma: elasticity for each function and local output 
# Consumers_Disutilities_Local_Bundle_Sigma: elasticity for each function and local bundle output 
# Consumers_Disutilities_Intnl_Bundle_Sigma: elasticity for each function and intnl bundle output 
# Consumers_Disutilities_Intnl_Sigma: elasticity for each function and intnl output 
#
# Consumers_Disutilities_Scale: scale factor for each region, function, and output
# Consumers_Disutilities_Share: share factor for each region, function, output, and input
#
##### Derived Sets #####
#
# Consumers_Disutilities_Sigma: elasticity for each region, function and output
#
##### Notes #####
#
# (1) Eta constant across regions; one sigma per function per output commodity
# (2) Scale differs across regions; one scale per function per output commodity
# (3) Share differs across regions; one share per function per input commodity
# (4) Each function must have at least one nonzero share and scale
# (5) Sum of the share parameters must be one
#
##### Definition ####
#
# Then output = gamma*(sum_i alpha[i]*input[i]^(1 + eta))
#
#####

param Consumers_Disutilities_Local_Eta {c in Consumers_All, r in Consumers_Regions[c], f in Consumers_Disutilities_All[c], Consumers_Disutilities_Local_Outputs[c,f]} >= 0, default 1;
param Consumers_Disutilities_Eta {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]} = Consumers_Disutilities_Local_Eta[c,r,f,ko];

param Consumers_Disutilities_Scale {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Disutilities_Regions_All[c,r], (ro,ko) in Consumers_Disutilities_Outputs_All[c,r,f]} >= 0, default 1;

param Consumers_Disutilities_Share {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Disutilities_Regions_All[c,r], (ro,ko) in Consumers_Disutilities_Outputs_All[c,r,f], (ri,ki) in Consumers_Disutilities_Inputs_All[c,r,f]} >= 0, default 0;

check {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r]}: card({(ro,ko) in Consumers_Disutilities_Outputs[c,r,f]: Consumers_Disutilities_Scale[c,r,f,ro,ko] > 0}) > 0;
check {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]}: card({(ri,ki) in Consumers_Disutilities_Inputs[c,r,f]: Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki] > 0}) > 0;
check {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]}: abs((sum {(ri,ki) in Consumers_Disutilities_Inputs[c,r,f]} Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki]) - 1.0) <= 1e-10;

##### Utility and Disutility Restrictions #####
#
#  (1) Each consumer must have at least one utility or disutility function
#  (2) Each region must have at least one utility or disutility function
#  (3) Each intermediate must occur as an input to one function or be referenced by the objective
#
##### Notes #####
#
# (1) This is meant to restrict the structure of the utility and disutility functions to forests
# (2) The checks as written do not prevent cycles in the structures; these are detected in the preprocessing
#
#####

check {c in Consumers}: card(Consumers_Utilities[c]) + card(Consumers_Disutilities[c]) > 0;
check {c in Consumers, r in Consumers_Regions[c]}: card(Consumers_Utilities_Regions[c,r]) + card(Consumers_Disutilities_Regions[c,r]) > 0;
check {c in Consumers, r in Consumers_Regions[c], (ri,ki) in (Consumers_Utilities_Intermeds[c,r] union Consumers_Disutilities_Intermeds[c,r])}: card({f in Consumers_Utilities_Regions[c,r]: (ri,ki) in Consumers_Utilities_Inputs[c,r,f]}) + card({f in Consumers_Disutilities_Regions[c,r]: (ri,ki) in Consumers_Disutilities_Inputs[c,r,f]}) + (if ((ri,ki) in (Consumers_Utilities_Objectives[c,r] union Consumers_Disutilities_Objectives[c,r])) then 1) = 1;

##### Input and Output Factors #####
#
#####

set Consumers_Utilities_Factors_All {c in Consumers_All, r in Consumers_Regions_All[c]} = union {f in Consumers_Utilities_Regions_All[c,r]} (Consumers_Utilities_Inputs_All[c,r,f] union Consumers_Utilities_Outputs_All[c,r,f]);
set Consumers_Utilities_Factors_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Utilities_Factors_All[c,r] default {};
set Consumers_Utilities_Factors {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Factors_All[c,r] diff Consumers_Utilities_Factors_Drop[c,r];

set Consumers_Disutilities_Factors_All {c in Consumers_All, r in Consumers_Regions_All[c]} = union {f in Consumers_Disutilities_Regions_All[c,r]} (Consumers_Disutilities_Inputs_All[c,r,f] union Consumers_Disutilities_Outputs_All[c,r,f]);
set Consumers_Disutilities_Factors_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Disutilities_Factors_All[c,r] default {};
set Consumers_Disutilities_Factors {c in Consumers, r in Consumers_Regions[c]} = Consumers_Disutilities_Factors_All[c,r] diff Consumers_Disutilities_Factors_Drop[c,r];

set Consumers_Utilities_Factors_Demands {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Factors[c,r] inter Consumers_Demands[c,r];
set Consumers_Utilities_Factors_Supplies {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Factors[c,r] inter Consumers_Supplies[c,r];
set Consumers_Utilities_Factors_Disutilities {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Factors[c,r] inter Consumers_Disutilities_Intermeds[c,r];
set Consumers_Utilities_Factors_Inputs {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Factors_Demands[c,r] union Consumers_Utilities_Factors_Supplies[c,r] union Consumers_Utilities_Factors_Disutilities[c,r];

set Consumers_Disutilities_Factors_Demands {c in Consumers, r in Consumers_Regions[c]} = Consumers_Disutilities_Factors[c,r] inter Consumers_Demands[c,r];
set Consumers_Disutilities_Factors_Supplies {c in Consumers, r in Consumers_Regions[c]} = Consumers_Disutilities_Factors[c,r] inter Consumers_Supplies[c,r];
set Consumers_Disutilities_Factors_Utilities {c in Consumers, r in Consumers_Regions[c]} = Consumers_Disutilities_Factors[c,r] inter Consumers_Utilities_Intermeds[c,r];
set Consumers_Disutilities_Factors_Inputs {c in Consumers, r in Consumers_Regions[c]} = Consumers_Disutilities_Factors_Demands[c,r] union Consumers_Disutilities_Factors_Supplies[c,r] union Consumers_Disutilities_Factors_Utilities[c,r];

set Consumers_Utilities_Factors_Objectives {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Factors[c,r] inter Consumers_Utilities_Objectives[c,r];
set Consumers_Utilities_Factors_Outputs {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Factors_Objectives[c,r] union Consumers_Disutilities_Factors_Utilities[c,r];

set Consumers_Utilities_Factors_Intermeds {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Factors[c,r] diff (Consumers_Utilities_Factors_Inputs[c,r] union Consumers_Utilities_Factors_Outputs[c,r]);

set Consumers_Disutilities_Factors_Objectives {c in Consumers, r in Consumers_Regions[c]} = Consumers_Disutilities_Factors[c,r] inter Consumers_Disutilities_Objectives[c,r];
set Consumers_Disutilities_Factors_Outputs {c in Consumers, r in Consumers_Regions[c]} = Consumers_Disutilities_Factors_Objectives[c,r] union Consumers_Utilities_Factors_Disutilities[c,r];

set Consumers_Disutilities_Factors_Intermeds {c in Consumers, r in Consumers_Regions[c]} = Consumers_Disutilities_Factors[c,r] diff (Consumers_Disutilities_Factors_Inputs[c,r] union Consumers_Disutilities_Factors_Outputs[c,r]);

set Consumers_Utilities_Factors_Supplies_All {c in Consumers_All, r in Consumers_Regions_All[c]} = Consumers_Utilities_Factors_All[c,r] inter Consumers_Supplies_All[c,r];
set Consumers_Utilities_Factors_Disutilities_All {c in Consumers_All, r in Consumers_Regions_All[c]} = Consumers_Utilities_Factors_All[c,r] inter Consumers_Disutilities_Intermeds_All[c,r];

set Consumers_Disutilities_Factors_Demands_All {c in Consumers_All, r in Consumers_Regions_All[c]} = Consumers_Disutilities_Factors_All[c,r] inter Consumers_Demands_All[c,r];
set Consumers_Disutilities_Factors_Utilities_All {c in Consumers_All, r in Consumers_Regions_All[c]} = Consumers_Disutilities_Factors_All[c,r] inter Consumers_Utilities_Intermeds_All[c,r];

set Consumers_Mapped_Factors_All {c in Consumers_All, r in Consumers_Regions_All[c]} = Consumers_Utilities_Factors_Supplies_All[c,r] union Consumers_Utilities_Factors_Disutilities_All[c,r] union Consumers_Disutilities_Factors_Demands_All[c,r] union Consumers_Disutilities_Factors_Utilities_All[c,r];
param Consumers_Mapped_Bounds_All {c in Consumers_All, r in Consumers_Regions_All[c], Consumers_Mapped_Factors_All[c,r]} >= 0, default 1;

set Consumers_Mapped_Factors {c in Consumers, r in Consumers_Regions[c]} = Consumers_Utilities_Factors_Supplies[c,r] union Consumers_Utilities_Factors_Disutilities[c,r] union Consumers_Disutilities_Factors_Demands[c,r] union Consumers_Disutilities_Factors_Utilities[c,r];

check {c in Consumers, r in Consumers_Regions[c], (rm,km) in Consumers_Mapped_Factors[c,r]}: Consumers_Mapped_Bounds_All[c,r,rm,km] > 0;

##### Endowments and Producer Ownership #####
#
# Endowments are always sold on the market
# Limits are on supply goods 
#
#####

param Consumers_Supplies_Limit {c in Consumers_All, r in Consumers_Regions_All[c], (ro,ko) in Consumers_Supplies_All[c,r]} >= 0, default Infinity;
param Consumers_Demands_Limit {c in Consumers_All, r in Consumers_Regions_All[c], (ro,ko) in Consumers_Demands_All[c,r]} >= 0, default Infinity;

param Consumers_Utilities_Factors_Limit {c in Consumers_All, r in Consumers_Regions_All[c], (rf,kf) in Consumers_Utilities_Factors_All[c,r]} >= 0, default Infinity;
param Consumers_Disutilities_Factors_Limit {c in Consumers_All, r in Consumers_Regions_All[c], (rf,kf) in Consumers_Disutilities_Factors_All[c,r]} >= 0, default Infinity;

set Consumers_Local_Endowments {Consumers_All} within Commodities_All default {};
set Consumers_Local_Bundle_Endowments {c in Consumers_All} within (Commodities_All diff Consumers_Local_Endowments[c]) default {};
set Consumers_Intnl_Bundle_Endowments {c in Consumers_All} within (Commodities_All diff Consumers_Local_Endowments[c] diff Consumers_Local_Bundle_Endowments[c]) default {};
set Consumers_Intnl_Endowments {c in Consumers_All} within (Regions_All cross (Commodities_All diff Consumers_Local_Endowments[c] diff Consumers_Local_Bundle_Endowments[c] diff Consumers_Intnl_Bundle_Endowments[c])) union ((Regions_All diff Consumers_Regions_All[c]) cross (Commodities_All diff Consumers_Intnl_Bundle_Endowments[c])) default {};

set Consumers_Endowments_All {c in Consumers_All, r in Consumers_Regions_All[c]} = Consumers_Intnl_Endowments[c] union (setof {k in Consumers_Local_Endowments[c]} (r,k)) union (Regions cross Consumers_Intnl_Bundle_Endowments[c]) union (Consumers_Regions[c] cross Consumers_Local_Bundle_Endowments[c]);
set Consumers_Endowments_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Endowments_All[c,r] default {};
set Consumers_Endowments {c in Consumers, r in Consumers_Regions[c]} = Consumers_Endowments_All[c,r] diff Consumers_Endowments_Drop[c,r];

param Consumers_Endowment {c in Consumers_All, r in Consumers_Regions_All[c], (ro,ko) in Consumers_Endowments_All[c,r]} >= 0, default 0;

set Consumers_Local_Ownerships {Consumers_All} within Producers_All default {};
set Consumers_Local_Bundle_Ownerships {c in Consumers_All} within (Producers_All diff Consumers_Local_Ownerships[c]) default {};
set Consumers_Intnl_Bundle_Ownerships {c in Consumers_All} within (Producers_All diff Consumers_Local_Ownerships[c] diff Consumers_Local_Bundle_Ownerships[c]) default {};
set Consumers_Intnl_Ownerships {c in Consumers_All} within (Regions cross (Producers_All diff Consumers_Local_Ownerships[c] diff Consumers_Local_Bundle_Ownerships[c] diff Consumers_Intnl_Bundle_Ownerships[c])) union ((Regions diff Consumers_Regions[c]) cross (Producers_All diff Consumers_Intnl_Bundle_Ownerships[c])) default {};

set Consumers_Ownerships_All {c in Consumers_All, r in Consumers_Regions_All[c]} = Consumers_Intnl_Ownerships[c] union (setof {p in Consumers_Local_Ownerships[c]} (r,p)) union (Regions cross Consumers_Intnl_Bundle_Ownerships[c]) union (Consumers_Regions[c] cross Consumers_Local_Bundle_Ownerships[c]);
set Consumers_Ownerships_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Ownerships_All[c,r] default {};
set Consumers_Ownerships {c in Consumers, r in Consumers_Regions[c]} = Consumers_Ownerships_All[c,r] diff Consumers_Ownerships_Drop[c,r];

param Consumers_Ownership {c in Consumers_All, r in Consumers_Regions_All[c], (ro,po) in Consumers_Ownerships_All[c,r]} >= 0, default 1;
param Consumers_TaxRebate {c in Consumers_All, r in Consumers_Regions_All[c]} >= 0, default 1;

check {po in Producers, ro in Producers_Regions[po]}: abs((sum{c in Consumers, r in Consumers_Regions[c]: (ro,po) in Consumers_Ownerships[c,r]} Consumers_Ownership[c,r,ro,po]) - 1.0) <= 1e-10;
check {rr in Regions}: abs((sum {c in Consumers, r in Consumers_Regions[c]: r == rr} Consumers_TaxRebate[c,r]) - 1.0) <= 1e-10;

check {c in Consumers, r in Consumers_Regions[c], (ro,po) in Consumers_Ownerships[c,r]}: ro in Producers_Regions[po];
check {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]: (rs,ks) not in Consumers_Utilities_Factors_Supplies[c,r] union Consumers_Disutilities_Factors_Supplies[c,r]}: Consumers_Supplies_Limit[c,r,rs,ks] < Infinity;

##### Consumer Variables #####
#
# Consumers_Demand: quantity of commodities demanded by consumer
# Consumers_Supply: quantity of commodities supplied by consumer
#
# Consumers_Utilities_Input: quantities of the inputs used by utility functions
# Consumers_Utilities_Output: outputs from utility functions
#
# Consumers_Disutilities_Input: quantities of the inputs used by disutility functions
# Consumers_Disutilities_Output: outputs from disutility functions
#
#####

var Consumers_Demand {c in Consumers, r in Consumers_Regions[c], Consumers_Demands[c,r]} := 1.0;
var Consumers_Supply {c in Consumers, r in Consumers_Regions[c], Consumers_Supplies[c,r]} := 1.0;

var Consumers_Utilities_Factors_Input {c in Consumers, r in Consumers_Regions[c], Consumers_Utilities_Factors_Inputs[c,r]} := 1.0;
var Consumers_Utilities_Factors_Output {c in Consumers, r in Consumers_Regions[c], Consumers_Utilities_Factors_Outputs[c,r]} := 1.0;

var Consumers_Disutilities_Factors_Input {c in Consumers, r in Consumers_Regions[c], Consumers_Disutilities_Factors_Inputs[c,r]} := 1.0;
var Consumers_Disutilities_Factors_Output {c in Consumers, r in Consumers_Regions[c], Consumers_Disutilities_Factors_Outputs[c,r]} := 1.0;

var Consumers_Utilities_Input {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], Consumers_Utilities_Inputs[c,r,f]} := 1.0;
var Consumers_Utilities_Output {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], Consumers_Utilities_Outputs[c,r,f]} := 1.0;

var Consumers_Disutilities_Input {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], Consumers_Disutilities_Inputs[c,r,f]} := 1.0;
var Consumers_Disutilities_Output {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], Consumers_Disutilities_Outputs[c,r,f]} := 1.0;

##### Optimization Problem #####
# maximize Consumers_Utilities {c in Consumers, r in Consumers_Regions[c]}: 
#   sum {(ru,ku) in Consumers_Utilities_Objectives[c,r]} Consumers_Utilities_Objectives_Weight[c,r,ru,ku]*Consumers_Utilities_Factors_Output[c,r,ru,ku] - sum {(rd,kd) in Consumers_Disutilities_Objectives[c,r]} Consumers_Disutilities_Objectives_Weight[c,r,rd,kd]*Consumers_Disutilities_Factors_Output[c,r,rd,kd];
#
# subject to
# Consumers_Utilities_Positive_Def {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Positive[c,r,f]}:
#   Consumers_Utilities_Output[c,r,f,ro,ko] <= cesp(Consumers_Utilities_Scale[c,r,f,ro,ko], Consumers_Utilities_Sigma[c,r,f,ro,ko], {(ri,ki) in Consumers_Utilities_Inputs[c,r,f]: Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] > 0} (Consumers_Factors_Index[c,r,ri,ki], Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki], Consumers_Utilities_Input[c,r,f,ri,ki]));
#
# Consumers_Utilities_Leontief_Def {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Leontief[c,r,f], (ri,ki) in Consumers_Utilities_Inputs[c,r,f]: Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] > 0}:
#   Consumers_Utilities_Output[c,r,f,ro,ko] <= Consumers_Utilities_Scale[c,r,f,ro,ko]*Consumers_Utilities_Input[c,r,f,ri,ki]/Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki];
#
# Consumers_Utilities_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rf,kf) in Consumers_Utilities_Factors[c,r]}:
#   (if ((rf,kf) in Consumers_Utilities_Factors_Inputs[c,r]) then Consumers_Utilities_Factors_Input[c,r,rf,kf]) + sum {f in Consumers_Utilities_Regions[c,r]: (rf,kf) in Consumers_Utilities_Outputs[c,r,f]} Consumers_Utilities_Output[c,r,f,rf,kf] >= (if ((rf,kf) in Consumers_Utilities_Factors_Outputs[c,r]) then Consumers_Utilities_Factors_Output[c,r,rf,kf]) + sum {f in Consumers_Utilities_Regions[c,r]: (rf,kf) in Consumers_Utilities_Inputs[c,r,f]} Consumers_Utilities_Input[c,r,f,rf,kf];
#
# Consumers_Disutilities_Def {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]}:
#   Consumers_Disutilities_Output[c,r,f,ro,ko] >= Consumers_Disutilities_Scale[c,r,f,ro,ko]*(sum {(ri,ki) in Consumers_Disutilities_Inputs[c,r,f]: Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki] > 0} Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki]*Consumers_Disutilities_Input[c,r,f,ri,ki]^(1+Consumers_Disutilities_Eta[c,r,f,ro,ko]));
#
# Consumers_Disutilities_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rf,kf) in Consumers_Disutilities_Factors[c,r]}:
#   (if ((rf,kf) in Consumers_Disutilities_Factors_Inputs[c,r]) then Consumers_Disutilities_Factors_Input[c,r,rf,kf]) + sum {f in Consumers_Disutilities_Regions[c,r]: (rf,kf) in Consumers_Disutilities_Outputs[c,r,f]} Consumers_Disutilities_Output[c,r,f,rf,kf] <= (if ((rf,kf) in Consumers_Disutilities_Factors_Outputs[c,r]) then Consumers_Disutilities_Factors_Output[c,r,rf,kf]) + sum {f in Consumers_Disutilities_Regions[c,r]: (rf,kf) in Consumers_Disutilities_Inputs[c,r,f]} Consumers_Disutilities_Input[c,r,f,rf,kf];
#
# Consumers_Utilities_Demand_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Utilities_Factors_Demands[c,r]}:
#   Consumers_Utilities_Factors_Input[c,r,rd,kd] <= Consumers_Demand[c,r,rd,kd];
#
# Consumers_Utilities_Supply_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Utilities_Factors_Supplies[c,r]}:
#   Consumers_Utilities_Factors_Input[c,r,rs,ks] <= Consumers_Mapped_Bounds_All[c,r,rs,ks] - Consumers_Supply[c,r,rs,ks];
#
# Consumers_Utilities_Disutilities_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rc,kc) in Consumers_Utilities_Factors_Disutilities[c,r]}:
#   Consumers_Utilities_Factors_Input[c,r,rc,kc] <= Consumers_Mapped_Bounds_All[c,r,rc,kc] - Consumers_Disutilities_Factors_Output[c,r,rc,kc];
#
# Consumers_Disutilities_Demand_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Disutilities_Factors_Demands[c,r]}:
#   Consumers_Disutilities_Factors_Input[c,r,rd,kd] >= Consumers_Mapped_Bounds_All[c,r,rd,kd] - Consumers_Demand[c,r,rd,kd];
#
# Consumers_Disutilities_Supply_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Disutilities_Factors_Supplies[c,r]}:
#   Consumers_Disutilities_Factors_Input[c,r,rs,ks] >= Consumers_Supply[c,r,rs,ks];
#
# Consumers_Disutilities_Utilities_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rc,kc) in Consumers_Disutilities_Factors_Utilities[c,r]}:
#   Consumers_Disutilities_Factors_Input[c,r,rc,kc] >= Consumers_Mapped_Bounds_All[c,r,rc,kc] - Consumers_Utilities_Factors_Output[c,r,rc,kc];
#
# Consumers_Budget_Def {c in Consumers, r in Consumers_Regions[c]}:
#   (sum {(rd,kd) in Consumers_Demands[c,r]} ((Consumers_Expenditures[c,r,rd,kd] + 
#                                              Consumers_RRPriceTax_Expenditures[c,r,rd,kd]*Consumers_RRPriceRate_Expenditures[c,r,rd,kd] +
#                                              Consumers_RDPriceTax_Expenditures[c,r,rd,kd]*Consumers_RDPriceRate_Expenditures[c,r,rd,kd]
#                                             )*(if kd in Homogenous_Commodities then HPrice[kd] else Price[rd,kd]) +
#                                             Consumers_RRQuantityTax_Expenditures[c,r,rd,kd]*Consumers_RRQuantityRate_Expenditures[c,r,rd,kd] +
#                                             Consumers_RDQuantityTax_Expenditures[c,r,rd,kd]*Consumers_RDQuantityRate_Expenditures[c,r,rd,kd]
#                                            )*Consumers_Demand[c,r,rd,kd]
#   )/Consumers_Scale[c,r] <= 
#   (sum {(rs,ks) in Consumers_Supplies[c,r]} ((Consumers_Revenues[c,r,rs,ks] -
#                                               Consumers_RRPriceTax_Revenues[c,r,rs,ks]*Consumers_RRPriceRate_Revenues[c,r,rs,ks] -
#                                               Consumers_RSPriceTax_Revenues[c,r,rs,ks]*Consumers_RSPriceRate_Revenues[c,r,rs,ks]
#                                              )*(if ks in Homogenous_Commodities then HPrice[ks] else Price[rs,ks]) -
#                                             Consumers_RRQuantityTax_Revenues[c,r,rs,ks]*Consumers_RRQuantityRate_Revenues[c,r,rs,ks] -
#                                             Consumers_RSQuantityTax_Revenues[c,r,rs,ks]*Consumers_RSQuantityRate_Revenues[c,r,rs,ks]
#                                             )*Consumers_Supply[c,r,rs,ks] + 
#    sum {(re,ke) in Consumers_Endowments[c,r]} (if ke in Homogenous_Commodities then HPrice[ke] else Price[re,ke])*Consumers_Endowment[c,r,re,ke] + 
#    sum {(rp,pp) in Consumers_Ownerships[c,r]} Producers_Profit[pp,rp]*Consumers_Ownership[c,r,rp,pp] +
#    Consumers_TaxRebate[c,r]*Total_Tax[r]
#   )/Consumers_Scale[c,r];
#
##### Notes #####
#
# (1) All variables are constrainted to be nonnegative
# (2) Supply variables with limits have upper bounds
# (3) Output variables in cross factors have upper bounds
# (4) Supply commodities in utility functions have upper bounds
# (5) Demand commodities in disutility functions have uppes bounds
#
##### First-Order Optimality Conditions #####
#
# Consumers_Utility: report of consumer utility
#
#####

var Consumers_Utility {c in Consumers, r in Consumers_Regions[c]} := 1.0;

var m_utl_pos {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Positive[c,r,f]} := 1.0;
var m_utl_leo {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Leontief[c,r,f], (ri,ki) in Consumers_Utilities_Inputs[c,r,f]: Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] > 0} := 1.0;
var m_utl_bal {c in Consumers, r in Consumers_Regions[c], (rf,kf) in Consumers_Utilities_Factors[c,r]} := 1.0;

var m_dutl_pow {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]} := 1.0;
var m_dutl_bal {c in Consumers, r in Consumers_Regions[c], (rf,kf) in Consumers_Disutilities_Factors[c,r]} := 1.0;

var m_utl_demand_bal {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Utilities_Factors_Demands[c,r]} := 1.0;
var m_utl_supply_bal {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Utilities_Factors_Supplies[c,r]} := 1.0;
var m_utl_dutl_bal {c in Consumers, r in Consumers_Regions[c], (rc,kc) in Consumers_Utilities_Factors_Disutilities[c,r]} := 1.0;

var m_dutl_demand_bal {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Disutilities_Factors_Demands[c,r]} := 1.0;
var m_dutl_supply_bal {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Disutilities_Factors_Supplies[c,r]} := 1.0;
var m_dutl_utl_bal {c in Consumers, r in Consumers_Regions[c], (rc,kc) in Consumers_Disutilities_Factors_Utilities[c,r]} := 1.0;

var m_con_bud {c in Consumers, r in Consumers_Regions[c]} := 1.0;

subject to

Consumers_Utilities_Def {c in Consumers, r in Consumers_Regions[c]}: 
  Consumers_Utility[c,r] complements Consumers_Utility[c,r] =
    sum {(ru,ku) in Consumers_Utilities_Objectives[c,r]} Consumers_Utilities_Objectives_Weight[c,r,ru,ku]*Consumers_Utilities_Factors_Output[c,r,ru,ku] - 
    sum {(rd,kd) in Consumers_Disutilities_Objectives[c,r]} Consumers_Disutilities_Objectives_Weight[c,r,rd,kd]*Consumers_Disutilities_Factors_Output[c,r,rd,kd];

Consumers_Tax_Def {rr in Regions}:
  Consumers_Tax[rr] complements Consumers_Tax[rr] = 
    sum {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]: r == rr} (Consumers_RRPriceTax_Revenues[c,r,rs,ks]*Consumers_RRPriceRate_Revenues[c,r,rs,ks]*(if ks in Homogenous_Commodities then HPrice[ks] else Price[rs,ks]) + Consumers_RRQuantityTax_Revenues[c,r,rs,ks]*Consumers_RRQuantityRate_Revenues[c,r,rs,ks])*Consumers_Supply[c,r,rs,ks] +
    sum {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]: rs == rr} (Consumers_RSPriceTax_Revenues[c,r,rs,ks]*Consumers_RSPriceRate_Revenues[c,r,rs,ks]*(if ks in Homogenous_Commodities then HPrice[ks] else Price[rs,ks]) + Consumers_RSQuantityTax_Revenues[c,r,rs,ks]*Consumers_RSQuantityRate_Revenues[c,r,rs,ks])*Consumers_Supply[c,r,rs,ks] +
   sum {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]: r == rr} (Consumers_RRPriceTax_Expenditures[c,r,rd,kd]*Consumers_RRPriceRate_Expenditures[c,r,rd,kd]*(if kd in Homogenous_Commodities then HPrice[kd] else Price[rd,kd]) + Consumers_RRQuantityTax_Expenditures[c,r,rd,kd]*Consumers_RRQuantityRate_Expenditures[c,r,rd,kd])*Consumers_Demand[c,r,rd,kd] +
   sum {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]: rd == rr} (Consumers_RDPriceTax_Expenditures[c,r,rd,kd]*Consumers_RDPriceRate_Expenditures[c,r,rd,kd]*(if kd in Homogenous_Commodities then HPrice[kd] else Price[rd,kd]) + Consumers_RDQuantityTax_Expenditures[c,r,rd,kd]*Consumers_RDQuantityRate_Expenditures[c,r,rd,kd])*Consumers_Demand[c,r,rd,kd];

Consumers_Opt_Demand {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]}:
  0 <= Consumers_Demand[c,r,rd,kd] <= Consumers_Demands_Limit[c,r,rd,kd] complements 
    ((Consumers_Expenditures[c,r,rd,kd] + 
      Consumers_RRPriceTax_Expenditures[c,r,rd,kd]*Consumers_RRPriceRate_Expenditures[c,r,rd,kd] +
      Consumers_RDPriceTax_Expenditures[c,r,rd,kd]*Consumers_RDPriceRate_Expenditures[c,r,rd,kd]
     )*(if kd in Homogenous_Commodities then HPrice[kd] else Price[rd,kd]) +
     Consumers_RRQuantityTax_Expenditures[c,r,rd,kd]*Consumers_RRQuantityRate_Expenditures[c,r,rd,kd] +
     Consumers_RDQuantityTax_Expenditures[c,r,rd,kd]*Consumers_RDQuantityRate_Expenditures[c,r,rd,kd]
    )*m_con_bud[c,r]/Consumers_Scale[c,r] - 
    (if (rd,kd) in Consumers_Utilities_Factors_Demands[c,r] then m_utl_demand_bal[c,r,rd,kd]) - 
    (if (rd,kd) in Consumers_Disutilities_Factors_Demands[c,r] then m_dutl_demand_bal[c,r,rd,kd]);

Consumers_Opt_Supply {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]}:
  0 <= Consumers_Supply[c,r,rs,ks] <= Consumers_Supplies_Limit[c,r,rs,ks] complements 
    (if (rs,ks) in Consumers_Utilities_Factors_Supplies[c,r] then m_utl_supply_bal[c,r,rs,ks]) + 
    (if (rs,ks) in Consumers_Disutilities_Factors_Supplies[c,r] then m_dutl_supply_bal[c,r,rs,ks]) - 
    ((Consumers_Revenues[c,r,rs,ks] -
      Consumers_RRPriceTax_Revenues[c,r,rs,ks]*Consumers_RRPriceRate_Revenues[c,r,rs,ks] -
      Consumers_RSPriceTax_Revenues[c,r,rs,ks]*Consumers_RSPriceRate_Revenues[c,r,rs,ks]
     )*(if ks in Homogenous_Commodities then HPrice[ks] else Price[rs,ks]) -
     Consumers_RRQuantityTax_Revenues[c,r,rs,ks]*Consumers_RRQuantityRate_Revenues[c,r,rs,ks] -
     Consumers_RSQuantityTax_Revenues[c,r,rs,ks]*Consumers_RSQuantityRate_Revenues[c,r,rs,ks]
    )*m_con_bud[c,r]/Consumers_Scale[c,r];

Consumers_Opt_Utilities_Factors_Input {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Utilities_Factors_Inputs[c,r]}:
  0 <= Consumers_Utilities_Factors_Input[c,r,ri,ki] <= Consumers_Utilities_Factors_Limit[c,r,ri,ki] complements (if (ri,ki) in Consumers_Utilities_Factors_Demands[c,r] then m_utl_demand_bal[c,r,ri,ki]) + (if (ri,ki) in Consumers_Utilities_Factors_Supplies[c,r] then m_utl_supply_bal[c,r,ri,ki]) + (if (ri,ki) in Consumers_Utilities_Factors_Disutilities[c,r] then m_utl_dutl_bal[c,r,ri,ki]) - m_utl_bal[c,r,ri,ki];

Consumers_Opt_Utilities_Factors_Output {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Utilities_Factors_Outputs[c,r]}:
  0 <= Consumers_Utilities_Factors_Output[c,r,ro,ko] <= Consumers_Utilities_Factors_Limit[c,r,ro,ko] complements m_utl_bal[c,r,ro,ko] - (if (ro,ko) in Consumers_Disutilities_Factors_Utilities[c,r] then m_dutl_utl_bal[c,r,ro,ko]) - (if (ro,ko) in Consumers_Utilities_Objectives[c,r] then Consumers_Utilities_Objectives_Weight[c,r,ro,ko]);

Consumers_Opt_Disutilities_Factors_Input {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Disutilities_Factors_Inputs[c,r]}:
  0 <= Consumers_Disutilities_Factors_Input[c,r,ri,ki] <= Consumers_Disutilities_Factors_Limit[c,r,ri,ki] complements m_dutl_bal[c,r,ri,ki] - (if (ri,ki) in Consumers_Disutilities_Factors_Demands[c,r] then m_dutl_demand_bal[c,r,ri,ki]) - (if (ri,ki) in Consumers_Disutilities_Factors_Supplies[c,r] then m_dutl_supply_bal[c,r,ri,ki]) - (if (ri,ki) in Consumers_Disutilities_Factors_Utilities[c,r] then m_dutl_utl_bal[c,r,ri,ki]);

Consumers_Opt_Disutilities_Factors_Output {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Disutilities_Factors_Outputs[c,r]}:
  0 <= Consumers_Disutilities_Factors_Output[c,r,ro,ko] <= Consumers_Disutilities_Factors_Limit[c,r,ro,ko] complements (if (ro,ko) in Consumers_Disutilities_Objectives[c,r] then Consumers_Disutilities_Objectives_Weight[c,r,ro,ko]) + (if (ro,ko) in Consumers_Utilities_Factors_Disutilities[c,r] then m_utl_dutl_bal[c,r,ro,ko]) - m_dutl_bal[c,r,ro,ko];

Consumers_Opt_Utilities_Input {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ri,ki) in Consumers_Utilities_Inputs[c,r,f]}:
  0 <= Consumers_Utilities_Input[c,r,f,ri,ki] complements m_utl_bal[c,r,ri,ki] - sum {(ro,ko) in Consumers_Utilities_Positive[c,r,f]: Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] > 0} d_cesp(Consumers_Factors_Index[c,r,ri,ki], Consumers_Utilities_Scale[c,r,f,ro,ko], Consumers_Utilities_Sigma[c,r,f,ro,ko], {(r1,k1) in Consumers_Utilities_Inputs[c,r,f]: Consumers_Utilities_Share[c,r,f,ro,ko,r1,k1] > 0} (Consumers_Factors_Index[c,r,r1,k1], Consumers_Utilities_Share[c,r,f,ro,ko,r1,k1], Consumers_Utilities_Input[c,r,f,r1,k1]))*m_utl_pos[c,r,f,ro,ko] - sum{(ro,ko) in Consumers_Utilities_Leontief[c,r,f]: Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] > 0} Consumers_Utilities_Scale[c,r,f,ro,ko]*m_utl_leo[c,r,f,ro,ko,ri,ki] >= 0;

Consumers_Opt_Utilities_Output {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f]}:
  0 <= Consumers_Utilities_Output[c,r,f,ro,ko] complements (if (ro,ko) in Consumers_Utilities_Positive[c,r,f] then m_utl_pos[c,r,f,ro,ko]) + (if (ro,ko) in Consumers_Utilities_Leontief[c,r,f] then sum {(ri,ki) in Consumers_Utilities_Inputs[c,r,f]: Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] > 0} m_utl_leo[c,r,f,ro,ko,ri,ki]) - m_utl_bal[c,r,ro,ko] >= 0;

Consumers_Opt_Disutilities_Input {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ri,ki) in Consumers_Disutilities_Inputs[c,r,f]}:
  0 <= Consumers_Disutilities_Input[c,r,f,ri,ki] complements sum {(ro,ko) in Consumers_Disutilities_Outputs[c,r,f]: Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki] > 0} Consumers_Disutilities_Scale[c,r,f,ro,ko]*Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki]*(1+Consumers_Disutilities_Eta[c,r,f,ro,ko])*Consumers_Disutilities_Input[c,r,f,ri,ki]^(Consumers_Disutilities_Eta[c,r,f,ro,ko])*m_dutl_pow[c,r,f,ro,ko] - m_dutl_bal[c,r,ri,ki] >= 0;

Consumers_Opt_Disutilities_Output {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]}:
  0 <= Consumers_Disutilities_Output[c,r,f,ro,ko] complements m_dutl_bal[c,r,ro,ko] - m_dutl_pow[c,r,f,ro,ko] >= 0;

Consumers_Utilities_Positive_Def {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Positive[c,r,f]}:
  0 <= m_utl_pos[c,r,f,ro,ko] complements cesp(Consumers_Utilities_Scale[c,r,f,ro,ko], Consumers_Utilities_Sigma[c,r,f,ro,ko], {(ri,ki) in Consumers_Utilities_Inputs[c,r,f]: Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] > 0} (Consumers_Factors_Index[c,r,ri,ki], Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki], Consumers_Utilities_Input[c,r,f,ri,ki])) - Consumers_Utilities_Output[c,r,f,ro,ko] >= 0;

Consumers_Utilities_Leontief_Def {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Leontief[c,r,f], (ri,ki) in Consumers_Utilities_Inputs[c,r,f]: Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] > 0}:
  0 <= m_utl_leo[c,r,f,ro,ko,ri,ki] complements Consumers_Utilities_Scale[c,r,f,ro,ko]*Consumers_Utilities_Input[c,r,f,ri,ki] - Consumers_Utilities_Output[c,r,f,ro,ko] >= 0;

Consumers_Utilities_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rf,kf) in Consumers_Utilities_Factors[c,r]}:
  0 <= m_utl_bal[c,r,rf,kf] complements (if ((rf,kf) in Consumers_Utilities_Factors_Inputs[c,r]) then Consumers_Utilities_Factors_Input[c,r,rf,kf]) + sum {f in Consumers_Utilities_Regions[c,r]: (rf,kf) in Consumers_Utilities_Outputs[c,r,f]} Consumers_Utilities_Output[c,r,f,rf,kf] - (if ((rf,kf) in Consumers_Utilities_Factors_Outputs[c,r]) then Consumers_Utilities_Factors_Output[c,r,rf,kf]) - sum {f in Consumers_Utilities_Regions[c,r]: (rf,kf) in Consumers_Utilities_Inputs[c,r,f]} Consumers_Utilities_Input[c,r,f,rf,kf] >= 0;

Consumers_Disutilities_Def {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]}:
  0 <= m_dutl_pow[c,r,f,ro,ko] complements Consumers_Disutilities_Output[c,r,f,ro,ko] - Consumers_Disutilities_Scale[c,r,f,ro,ko]*(sum {(ri,ki) in Consumers_Disutilities_Inputs[c,r,f]: Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki] > 0} Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki]*Consumers_Disutilities_Input[c,r,f,ri,ki]^(1+Consumers_Disutilities_Eta[c,r,f,ro,ko])) >= 0;

Consumers_Disutilities_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rf,kf) in Consumers_Disutilities_Factors[c,r]}:
  0 <= m_dutl_bal[c,r,rf,kf] complements (if ((rf,kf) in Consumers_Disutilities_Factors_Outputs[c,r]) then Consumers_Disutilities_Factors_Output[c,r,rf,kf]) + sum {f in Consumers_Disutilities_Regions[c,r]: (rf,kf) in Consumers_Disutilities_Inputs[c,r,f]} Consumers_Disutilities_Input[c,r,f,rf,kf] - (if ((rf,kf) in Consumers_Disutilities_Factors_Inputs[c,r]) then Consumers_Disutilities_Factors_Input[c,r,rf,kf]) - sum {f in Consumers_Disutilities_Regions[c,r]: (rf,kf) in Consumers_Disutilities_Outputs[c,r,f]} Consumers_Disutilities_Output[c,r,f,rf,kf] >= 0;

Consumers_Utilities_Demand_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Utilities_Factors_Demands[c,r]}:
  0 <= m_utl_demand_bal[c,r,rd,kd] complements Consumers_Demand[c,r,rd,kd] - Consumers_Utilities_Factors_Input[c,r,rd,kd] >= 0;

Consumers_Utilities_Supply_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Utilities_Factors_Supplies[c,r]}:
  0 <= m_utl_supply_bal[c,r,rs,ks] complements Consumers_Mapped_Bounds_All[c,r,rs,ks] - Consumers_Supply[c,r,rs,ks] - Consumers_Utilities_Factors_Input[c,r,rs,ks] >= 0;

Consumers_Utilities_Disutilities_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rc,kc) in Consumers_Utilities_Factors_Disutilities[c,r]}:
  0 <= m_utl_dutl_bal[c,r,rc,kc] complements Consumers_Mapped_Bounds_All[c,r,rc,kc] - Consumers_Disutilities_Factors_Output[c,r,rc,kc] - Consumers_Utilities_Factors_Input[c,r,rc,kc] >= 0;

Consumers_Disutilities_Demand_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Disutilities_Factors_Demands[c,r]}:
  0 <= m_dutl_demand_bal[c,r,rd,kd] complements Consumers_Disutilities_Factors_Input[c,r,rd,kd] + Consumers_Demand[c,r,rd,kd] - Consumers_Mapped_Bounds_All[c,r,rd,kd] >= 0;

Consumers_Disutilities_Supply_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Disutilities_Factors_Supplies[c,r]}:
  0 <= m_dutl_supply_bal[c,r,rs,ks] complements Consumers_Disutilities_Factors_Input[c,r,rs,ks] - Consumers_Supply[c,r,rs,ks] >= 0;

Consumers_Disutilities_Utilities_Balance_Def {c in Consumers, r in Consumers_Regions[c], (rc,kc) in Consumers_Disutilities_Factors_Utilities[c,r]}:
  0 <= m_dutl_utl_bal[c,r,rc,kc] complements Consumers_Disutilities_Factors_Input[c,r,rc,kc] + Consumers_Utilities_Factors_Output[c,r,rc,kc] - Consumers_Mapped_Bounds_All[c,r,rc,kc] >= 0;

Consumers_Budget_Def {c in Consumers, r in Consumers_Regions[c]}:
  0 <= m_con_bud[c,r] complements 
    (sum {(rs,ks) in Consumers_Supplies[c,r]} ((Consumers_Revenues[c,r,rs,ks] -
                                                Consumers_RRPriceTax_Revenues[c,r,rs,ks]*Consumers_RRPriceRate_Revenues[c,r,rs,ks] -
                                                Consumers_RSPriceTax_Revenues[c,r,rs,ks]*Consumers_RSPriceRate_Revenues[c,r,rs,ks]
                                               )*(if ks in Homogenous_Commodities then HPrice[ks] else Price[rs,ks]) -
                                              Consumers_RRQuantityTax_Revenues[c,r,rs,ks]*Consumers_RRQuantityRate_Revenues[c,r,rs,ks] -
                                              Consumers_RSQuantityTax_Revenues[c,r,rs,ks]*Consumers_RSQuantityRate_Revenues[c,r,rs,ks]
                                              )*Consumers_Supply[c,r,rs,ks] + 
     sum {(re,ke) in Consumers_Endowments[c,r]} (if ke in Homogenous_Commodities then HPrice[ke] else Price[re,ke])*Consumers_Endowment[c,r,re,ke] + 
     sum {(rp,pp) in Consumers_Ownerships[c,r]} Producers_Profit[pp,rp]*Consumers_Ownership[c,r,rp,pp] + 
     Consumers_TaxRebate[c,r]*Total_Tax[r] - 
     sum {(rd,kd) in Consumers_Demands[c,r]} ((Consumers_Expenditures[c,r,rd,kd] + 
                                               Consumers_RRPriceTax_Expenditures[c,r,rd,kd]*Consumers_RRPriceRate_Expenditures[c,r,rd,kd] +
                                               Consumers_RDPriceTax_Expenditures[c,r,rd,kd]*Consumers_RDPriceRate_Expenditures[c,r,rd,kd]
                                              )*(if kd in Homogenous_Commodities then HPrice[kd] else Price[rd,kd]) +
                                              Consumers_RRQuantityTax_Expenditures[c,r,rd,kd]*Consumers_RRQuantityRate_Expenditures[c,r,rd,kd] +
                                              Consumers_RDQuantityTax_Expenditures[c,r,rd,kd]*Consumers_RDQuantityRate_Expenditures[c,r,rd,kd]
                                             )*Consumers_Demand[c,r,rd,kd]
    )/Consumers_Scale[c,r] >= 0;

##### Market Clearing Conditions #####
# Notes
#  (1) There is a ray of feasible prices
#  (2) The numeriare pic,ks one 
#####

param Markets_Scale{r in Regions, k in Region_Commodities[r]} := 1; # sum{p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Inputs[p,rp]} Producers_Expenditures[p,rp,r,k] + sum{c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Demands[c,rc]} Consumers_Expenditures[c,rc,r,k];
param HMarkets_Scale{k in Homogenous_Commodities} := 1;

mkt {r in Regions, k in Region_Commodities[r] diff (Homogenous_Commodities union Numeraire[r])}:
  0 <= Price[r,k] complements (sum{p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Outputs[p,rp]} Producers_Revenues[p,rp,r,k]*Producers_Output[p,rp,r,k] + sum{c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Supplies[c,rc]} Consumers_Revenues[c,rc,r,k]*Consumers_Supply[c,rc,r,k] + sum{c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Endowments[c,rc]} Consumers_Endowment[c,rc,r,k] - sum{p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Inputs[p,rp]} Producers_Expenditures[p,rp,r,k]*Producers_Input[p,rp,r,k] - sum{c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Demands[c,rc]} Consumers_Expenditures[c,rc,r,k]*Consumers_Demand[c,rc,r,k])/Markets_Scale[r,k] >= 0;

num {r in Regions, k in Numeraire[r]}:
  Price[r,k] complements Price[r,k] = 1;

hmkt {k in Homogenous_Commodities}:
  0 <= HPrice[k] complements (sum{p in Producers, rp in Producers_Regions[p], (r,k) in Producers_Outputs[p,rp]} Producers_Revenues[p,rp,r,k]*Producers_Output[p,rp,r,k] + sum{c in Consumers, rc in Consumers_Regions[c], (r,k) in Consumers_Supplies[c,rc]} Consumers_Revenues[c,rc,r,k]*Consumers_Supply[c,rc,r,k] + sum{c in Consumers, rc in Consumers_Regions[c], (r,k) in Consumers_Endowments[c,rc]} Consumers_Endowment[c,rc,r,k] - sum{p in Producers, rp in Producers_Regions[p], (r,k) in Producers_Inputs[p,rp]} Producers_Expenditures[p,rp,r,k]*Producers_Input[p,rp,r,k] - sum{c in Consumers, rc in Consumers_Regions[c], (r,k) in Consumers_Demands[c,rc]} Consumers_Expenditures[c,rc,r,k]*Consumers_Demand[c,rc,r,k])/HMarkets_Scale[k] >= 0;

check {r in Regions, k in Region_Commodities[r] diff Homogenous_Commodities}: card({p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Outputs[p,rp]}) + card({c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Supplies[c,rc]}) + card({c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Endowments[c,rc] and Consumers_Endowment[c,rc,r,k] > 0}) > 0;
check {r in Regions, k in Region_Commodities[r] diff Homogenous_Commodities}: card({p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Inputs[p,rp]}) + card({c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Demands[c,rc]}) > 0;

##### Subproblem Definitions #####
#
#####

problem Producers_Problems_All:
  Producers_Profits_Def, Producers_Profit,
  Producers_Tax_Def, Producers_Tax,
  Producers_Opt_Input, Producers_Input, 
#  Producers_Opt_Functions_Input, Producers_Functions_Input, 
#  Producers_Opt_Functions_Output, Producers_Functions_Output, 
  Producers_Functions_Positive_Def, m_pro_pos, 
  Producers_Functions_Leontief_Def, m_pro_leo, 
#  Producers_Inputs_Balance_Def, m_pro_in_bal, 
#  Producers_Outputs_Balance_Def, m_pro_out_bal, 
#  Producers_Intermeds_Balance_Def, m_pro_inter_bal,
  Total_Tax_Def, Total_Tax;

problem Consumers_Problems_All:
  Consumers_Utilities_Def, Consumers_Utility,
  Consumers_Tax_Def, Consumers_Tax,
  Consumers_Opt_Demand, Consumers_Demand,
  Consumers_Opt_Supply, Consumers_Supply,
  Consumers_Opt_Utilities_Factors_Input, Consumers_Utilities_Factors_Input,
  Consumers_Opt_Utilities_Factors_Output, Consumers_Utilities_Factors_Output,
  Consumers_Opt_Disutilities_Factors_Input, Consumers_Disutilities_Factors_Input,
  Consumers_Opt_Disutilities_Factors_Output, Consumers_Disutilities_Factors_Output,
  Consumers_Opt_Utilities_Input, Consumers_Utilities_Input,
  Consumers_Opt_Utilities_Output, Consumers_Utilities_Output,
  Consumers_Opt_Disutilities_Input, Consumers_Disutilities_Input,
  Consumers_Opt_Disutilities_Output, Consumers_Disutilities_Output,
  Consumers_Utilities_Positive_Def, m_utl_pos,
  Consumers_Utilities_Leontief_Def, m_utl_leo,
  Consumers_Utilities_Balance_Def, m_utl_bal,
  Consumers_Disutilities_Def, m_dutl_pow,
  Consumers_Disutilities_Balance_Def, m_dutl_bal,
  Consumers_Utilities_Demand_Balance_Def, m_utl_demand_bal,
  Consumers_Utilities_Supply_Balance_Def, m_utl_supply_bal,
  Consumers_Utilities_Disutilities_Balance_Def, m_utl_dutl_bal,
  Consumers_Disutilities_Demand_Balance_Def, m_dutl_demand_bal,
  Consumers_Disutilities_Supply_Balance_Def, m_dutl_supply_bal,
  Consumers_Disutilities_Utilities_Balance_Def, m_dutl_utl_bal,
  Consumers_Budget_Def, m_con_bud,
  Total_Tax_Def, Total_Tax;

problem Markets_Problems_All:
  Producers_Profits_Def, Producers_Profit,
  Producers_Tax_Def, Producers_Tax,
  Producers_Opt_Input, Producers_Input, 
  Producers_Opt_Output, Producers_Output, 
#  Producers_Opt_Functions_Input, Producers_Functions_Input, 
#  Producers_Opt_Functions_Output, Producers_Functions_Output, 
  Producers_Functions_Positive_Def, m_pro_pos, 
  Producers_Functions_Leontief_Def, m_pro_leo, 
#  Producers_Inputs_Balance_Def, m_pro_in_bal, 
#  Producers_Outputs_Balance_Def, m_pro_out_bal, 
#  Producers_Intermeds_Balance_Def, m_pro_inter_bal,
  Consumers_Utilities_Def, Consumers_Utility,
  Consumers_Tax_Def, Consumers_Tax,
  Consumers_Opt_Demand, Consumers_Demand,
  Consumers_Opt_Supply, Consumers_Supply,
  Consumers_Opt_Utilities_Factors_Input, Consumers_Utilities_Factors_Input,
  Consumers_Opt_Utilities_Factors_Output, Consumers_Utilities_Factors_Output,
  Consumers_Opt_Disutilities_Factors_Input, Consumers_Disutilities_Factors_Input,
  Consumers_Opt_Disutilities_Factors_Output, Consumers_Disutilities_Factors_Output,
  Consumers_Opt_Utilities_Input, Consumers_Utilities_Input,
  Consumers_Opt_Utilities_Output, Consumers_Utilities_Output,
  Consumers_Opt_Disutilities_Input, Consumers_Disutilities_Input,
  Consumers_Opt_Disutilities_Output, Consumers_Disutilities_Output,
  Consumers_Utilities_Positive_Def, m_utl_pos,
  Consumers_Utilities_Leontief_Def, m_utl_leo,
  Consumers_Utilities_Balance_Def, m_utl_bal,
  Consumers_Disutilities_Def, m_dutl_pow,
  Consumers_Disutilities_Balance_Def, m_dutl_bal,
  Consumers_Utilities_Demand_Balance_Def, m_utl_demand_bal,
  Consumers_Utilities_Supply_Balance_Def, m_utl_supply_bal,
  Consumers_Utilities_Disutilities_Balance_Def, m_utl_dutl_bal,
  Consumers_Disutilities_Demand_Balance_Def, m_dutl_demand_bal,
  Consumers_Disutilities_Supply_Balance_Def, m_dutl_supply_bal,
  Consumers_Disutilities_Utilities_Balance_Def, m_dutl_utl_bal,
  Consumers_Budget_Def, m_con_bud,
  Total_Tax_Def, Total_Tax,
  mkt, num, Price,
  hmkt, HPrice;


