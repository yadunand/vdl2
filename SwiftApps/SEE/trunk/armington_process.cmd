##### Processing Commands #####
#
#####

##### Calibration #####
#
#  Consumer side works only for utility functions referencing the demands
#
#####

printf "Calibration\n";

param ShareTol := 1e-4; #1e-5; #1e-4;
param RegionTol := 0; #1e-5;
param term;
param mods;

param Pro_Exp {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_All_Inputs[p,r]};
param Pro_Rev {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_All_Outputs[p,r]};
param Pro_TRev {p in Producers, r in Producers_Regions[p]};
param Pro_RRev {r in Regions};

param Con_Exp {c in Consumers, r in Consumers_Regions[c], (rf,kf) in Consumers_All_Inputs[c,r]};
param Con_Utl {c in Consumers, r in Consumers_Regions[c], (rf,kf) in Consumers_All_Outputs[c,r]};
param Con_TUtl {c in Consumers, r in Consumers_Regions[c]};

let {p in Producers_All, r in Producers_Regions_All[p], (ri,ki) in Producers_Inputs_All[p,r]} Producers_Expenditures[p,r,ri,ki] := 0;
let {p in Producers_All, r in Producers_Regions_All[p], (ri,ki) in Producers_Inputs_All[p,r]} Producers_RRPriceTax_Expenditures[p,r,ri,ki] := 0;
let {p in Producers_All, r in Producers_Regions_All[p], (ri,ki) in Producers_Inputs_All[p,r]} Producers_RIPriceTax_Expenditures[p,r,ri,ki] := 0;
let {p in Producers_All, r in Producers_Regions_All[p], (ri,ki) in Producers_Inputs_All[p,r]} Producers_RRQuantityTax_Expenditures[p,r,ri,ki] := 0;
let {p in Producers_All, r in Producers_Regions_All[p], (ri,ki) in Producers_Inputs_All[p,r]} Producers_RIQuantityTax_Expenditures[p,r,ri,ki] := 0;

let {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} Producers_Expenditures[p,r,ri,ki] := Producers_Expenditures_All[p,r,ri,ki];
let {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} Producers_RRPriceTax_Expenditures[p,r,ri,ki] := Producers_RRPriceTax_Expenditures_All[p,r,ri,ki];
let {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} Producers_RIPriceTax_Expenditures[p,r,ri,ki] := Producers_RIPriceTax_Expenditures_All[p,r,ri,ki];
let {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} Producers_RRQuantityTax_Expenditures[p,r,ri,ki] := Producers_RRQuantityTax_Expenditures_All[p,r,ri,ki];
let {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} Producers_RIQuantityTax_Expenditures[p,r,ri,ki] := Producers_RIQuantityTax_Expenditures_All[p,r,ri,ki];

repeat {
  let {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_All_Inputs[p,r]} Pro_Exp[p,r,rf,kf] := -Infinity;
  let {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_All_Outputs[p,r]} Pro_Rev[p,r,rf,kf] := -Infinity;

  let {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} Pro_Exp[p,r,ri,ki] :=
    Producers_Expenditures[p,r,ri,ki] + 
    Producers_RRPriceTax_Expenditures[p,r,ri,ki] + Producers_RIPriceTax_Expenditures[p,r,ri,ki] +
    Producers_RRQuantityTax_Expenditures[p,r,ri,ki] + Producers_RIQuantityTax_Expenditures[p,r,ri,ki];

  repeat {
    let term := 1;

    for {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]: Pro_Rev[p,r,ro,ko] == -Infinity and min {(ri,ki) in Producers_Functions_Inputs[p,r,f]} Pro_Exp[p,r,ri,ki] > -Infinity} {
      let term := 0;
      let Pro_Rev[p,r,ro,ko] := sum {(ri,ki) in Producers_Functions_Inputs[p,r,f]} Pro_Exp[p,r,ri,ki];
      if ((ro,ko) in Producers_Intermeds[p,r]) then {
        let Pro_Exp[p,r,ro,ko] := Pro_Rev[p,r,ro,ko];
      }
    }
  } until term;

  if (min {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Inputs[p,r]} Pro_Exp[p,r,rf,kf] == -Infinity) or
     (min {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Outputs[p,r]} Pro_Rev[p,r,rf,kf] == -Infinity) then {
    printf "  Cycle detected or factor not referenced:\n";
    printf {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Inputs[p,r]: Pro_Exp[p,r,rf,kf] == -Infinity} "    Input:  %s %s %s %s\n", p, r, rf, kf;
    printf {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Outputs[p,r]: Pro_Rev[p,r,rf,kf] == -Infinity} "   Output: %s %s %s %s\n", p, r, rf, kf;
    exit;
  }

  let {p in Producers, r in Producers_Regions[p]} Pro_TRev[p,r] := sum {(ro,ko) in Producers_Outputs[p,r]} Pro_Rev[p,r,ro,ko];
  let {r in Regions} Pro_RRev[r] := sum {p in Producers: r in Producers_Regions[p]} Pro_TRev[p,r];

  let term := 1;
  for {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} {
    if (Pro_TRev[p,r] > 0 and Pro_Exp[p,r,ri,ki] > 0 and Pro_Exp[p,r,ri,ki] / Pro_TRev[p,r] < ShareTol) then {
      printf "  Producer share elimination (%5.4e): %s %s %s %s\n", Pro_Exp[p,r,ri,ki] / Pro_TRev[p,r], p, r, ri, ki;
      let Producers_Expenditures[p,r,ri,ki] := 0;
      let Producers_RRPriceTax_Expenditures[p,r,ri,ki] := 0;
      let Producers_RIPriceTax_Expenditures[p,r,ri,ki] := 0;
      let Producers_RRQuantityTax_Expenditures[p,r,ri,ki] := 0;
      let Producers_RIQuantityTax_Expenditures[p,r,ri,ki] := 0;
      let term := 0;
    }
  }

  for {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} {
    if (Pro_RRev[r] > 0 and Pro_Rev[p,r,ro,ko] > 0 and Pro_Rev[p,r,ro,ko] / Pro_RRev[r] < RegionTol) then {
      printf "  Producer elimination (%5.4e): %s %s %s %s\n", Pro_Rev[p,r,ro,ko] / Pro_RRev[r], p, r, ro, ko;
    }
  }
} until term;

let {p in Producers_All, r in Producers_Regions_All[p], f in Producers_Functions_Regions_All[p,r], (ro,ko) in Producers_Functions_Outputs_All[p,r,f], (ri,ki) in Producers_Functions_Inputs_All[p,r,f]} Producers_Functions_Share[p,r,f,ro,ko,ri,ki] := 0;
for  {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]} {
  if (Pro_Rev[p,r,ro,ko] > 0 and Pro_Exp[p,r,ri,ki] > 0) then {
    let Producers_Functions_Share[p,r,f,ro,ko,ri,ki] := Pro_Exp[p,r,ri,ki] / Pro_Rev[p,r,ro,ko];
  }
}

let {c in Consumers_All, r in Consumers_Regions_All[c], (rd,kd) in Consumers_Demands_All[c,r]} Consumers_Expenditures[c,r,rd,kd] := 0;
let {c in Consumers_All, r in Consumers_Regions_All[c], (rd,kd) in Consumers_Demands_All[c,r]} Consumers_RRPriceTax_Expenditures[c,r,rd,kd] := 0;
let {c in Consumers_All, r in Consumers_Regions_All[c], (rd,kd) in Consumers_Demands_All[c,r]} Consumers_RDPriceTax_Expenditures[c,r,rd,kd] := 0;
let {c in Consumers_All, r in Consumers_Regions_All[c], (rd,kd) in Consumers_Demands_All[c,r]} Consumers_RRQuantityTax_Expenditures[c,r,rd,kd] := 0;
let {c in Consumers_All, r in Consumers_Regions_All[c], (rd,kd) in Consumers_Demands_All[c,r]} Consumers_RDQuantityTax_Expenditures[c,r,rd,kd] := 0;

let {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Consumers_Expenditures[c,r,rd,kd] := Consumers_Expenditures_All[c,r,rd,kd];
let {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Consumers_RRPriceTax_Expenditures[c,r,rd,kd] := Consumers_RRPriceTax_Expenditures_All[c,r,rd,kd];
let {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Consumers_RDPriceTax_Expenditures[c,r,rd,kd] := Consumers_RDPriceTax_Expenditures_All[c,r,rd,kd];
let {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Consumers_RRQuantityTax_Expenditures[c,r,rd,kd] := Consumers_RRQuantityTax_Expenditures_All[c,r,rd,kd];
let {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Consumers_RDQuantityTax_Expenditures[c,r,rd,kd] := Consumers_RDQuantityTax_Expenditures_All[c,r,rd,kd];

repeat {
  let {c in Consumers, r in Consumers_Regions[c], (rf,kf) in Consumers_All_Inputs[c,r]} Con_Exp[c,r,rf,kf] := -Infinity;
  let {c in Consumers, r in Consumers_Regions[c], (rf,kf) in Consumers_All_Outputs[c,r]} Con_Utl[c,r,rf,kf] := -Infinity;

  let {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Con_Exp[c,r,rd,kd] :=
    Consumers_Expenditures[c,r,rd,kd] + 
    Consumers_RRPriceTax_Expenditures[c,r,rd,kd] + Consumers_RDPriceTax_Expenditures[c,r,rd,kd] +
    Consumers_RRQuantityTax_Expenditures[c,r,rd,kd] + Consumers_RDQuantityTax_Expenditures[c,r,rd,kd];

  repeat {
    let term := 1;

    for {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f]: Con_Utl[c,r,ro,ko] == -Infinity and min {(rd,kd) in Consumers_Utilities_Inputs[c,r,f]} Con_Exp[c,r,rd,kd] > -Infinity} {
      let term := 0;
      let Con_Utl[c,r,ro,ko] := sum {(rd,kd) in Consumers_Utilities_Inputs[c,r,f]} Con_Exp[c,r,rd,kd];
      if ((ro,ko) in Consumers_Utilities_Intermeds[c,r]) then {
        let Con_Exp[c,r,ro,ko] := Con_Utl[c,r,ro,ko];
      }
    }
  } until term;

  if (min {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Con_Exp[c,r,rd,kd] == -Infinity) or
     (min {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Utilities_Objectives[c,r]} Con_Utl[c,r,ro,ko] == -Infinity) then {
    printf "  Cycle detected or factor not referenced:\n";
    printf {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]: Con_Exp[c,r,rd,kd] == -Infinity} "   Demand :  %s %s %s %s\n", c, r, rd, kd;
    printf {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Utilities_Objectives[c,r]: Con_Utl[c,r,ro,ko] == -Infinity} "   Utility: %s %s %s %s\n", c, r, ro, ko;
    exit;
  }

  let {c in Consumers, r in Consumers_Regions[c]} Con_TUtl[c,r] := sum {(ro,ko) in Consumers_Utilities_Objectives[c,r]} Con_Utl[c,r,ro,ko];

  let term := 1;
  for {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} {
    if (Con_TUtl[c,r] > 0 and Con_Exp[c,r,rd,kd] > 0 and Con_Exp[c,r,rd,kd] / Con_TUtl[c,r] < ShareTol) then {
      printf "  Consumer share elimination (%5.4e): %s %s %s %s\n", Con_Exp[c,r,rd,kd] / Con_TUtl[c,r], c, r, rd, kd;
      let Consumers_Expenditures[c,r,rd,kd] := 0;
      let Consumers_RRPriceTax_Expenditures[c,r,rd,kd] := 0;
      let Consumers_RDPriceTax_Expenditures[c,r,rd,kd] := 0;
      let Consumers_RRQuantityTax_Expenditures[c,r,rd,kd] := 0;
      let Consumers_RDQuantityTax_Expenditures[c,r,rd,kd] := 0;
      let term := 0;
    }
  }
} until term;

let {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Utilities_Regions_All[c,r], (ro,ko) in Consumers_Utilities_Outputs_All[c,r,f], (ri,ki) in Consumers_Utilities_Inputs_All[c,r,f]} Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] := 0;
for  {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f], (ri,ki) in Consumers_Utilities_Inputs[c,r,f]} {
  if (Con_Utl[c,r,ro,ko] > 0 and Con_Exp[c,r,ri,ki] > 0) then {
    let Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] := Con_Exp[c,r,ri,ki] / Con_Utl[c,r,ro,ko];
  }
}

##### Information for Processing #####
#
#####

param Iter;

set Commodities_New_Drop within Commodities_All;
set Producers_New_Drop within Producers_All;

set Region_Commodities_New_Drop {r in Regions} within Region_Commodities_All[r];

set Producers_Regions_New_Drop {p in Producers_All} within Producers_Regions_All[p];

set Producers_Inputs_New_Drop {p in Producers_All, r in Producers_Regions_All[p]} within Producers_Inputs_All[p,r];
set Producers_Outputs_New_Drop {p in Producers_All, r in Producers_Regions_All[p]} within Producers_Outputs_All[p,r];
set Producers_Intermeds_New_Drop {p in Producers_All, r in Producers_Regions_All[p]} within Producers_Intermeds_All[p,r];

set Producers_Functions_New_Drop {p in Producers_All} within Producers_Functions_All[p];
set Producers_Functions_Regions_New_Drop {p in Producers_All, r in Producers_Regions_All[p]} within Producers_Functions_All[p];

set Producers_Functions_Inputs_New_Drop {p in Producers_All, r in Producers_Regions_All[p], f in Producers_Functions_Regions_All[p,r]} within Producers_Functions_Inputs_All[p,r,f];
set Producers_Functions_Outputs_New_Drop {p in Producers_All, r in Producers_Regions_All[p], f in Producers_Functions_Regions_All[p,r]} within Producers_Functions_Outputs_All[p,r,f];

param Producers_Functions_Share_Sum {p in Producers_All, r in Producers_Regions_All[p], f in Producers_Functions_Regions_All[p,r], (ro,ko) in Producers_Functions_Outputs_All[p,r,f]};

set Consumers_Regions_New_Drop {p in Consumers_All} within Consumers_Regions_All[p];

set Consumers_Demands_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Demands_All[c,r];
set Consumers_Supplies_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Supplies_All[c,r];

set Consumers_Utilities_Objectives_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Utilities_Objectives_All[c,r];
set Consumers_Disutilities_Objectives_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Disutilities_Objectives_All[c,r];

set Consumers_Utilities_Factors_Inputs_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Utilities_Factors_All[c,r];
set Consumers_Utilities_Factors_Outputs_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Utilities_Factors_All[c,r];
set Consumers_Utilities_Factors_Intermeds_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Utilities_Factors_All[c,r];
set Consumers_Utilities_Intermeds_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Utilities_Intermeds_All[c,r];

set Consumers_Disutilities_Factors_Inputs_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Disutilities_Factors_All[c,r];
set Consumers_Disutilities_Factors_Outputs_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Disutilities_Factors_All[c,r];
set Consumers_Disutilities_Factors_Intermeds_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Disutilities_Factors_All[c,r];
set Consumers_Disutilities_Intermeds_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Disutilities_Intermeds_All[c,r];

set Consumers_Utilities_New_Drop {c in Consumers_All} within Consumers_Utilities_All[c];
set Consumers_Utilities_Regions_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Utilities_All[c];

set Consumers_Disutilities_New_Drop {c in Consumers_All} within Consumers_Disutilities_All[c];
set Consumers_Disutilities_Regions_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Disutilities_All[c];

set Consumers_Utilities_Inputs_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Utilities_Regions_All[c,r]} within Consumers_Utilities_Inputs_All[c,r,f];
set Consumers_Utilities_Outputs_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Utilities_Regions_All[c,r]} within Consumers_Utilities_Outputs_All[c,r,f];

set Consumers_Disutilities_Inputs_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Disutilities_Regions_All[c,r]} within Consumers_Disutilities_Inputs_All[c,r,f];
set Consumers_Disutilities_Outputs_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Disutilities_Regions_All[c,r]} within Consumers_Disutilities_Outputs_All[c,r,f];

set Consumers_Ownerships_New_Drop {c in Consumers_All, r in Consumers_Regions_All[c]} within Consumers_Ownerships_All[c,r];

param Consumers_Utilities_Share_Sum {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Utilities_Regions_All[c,r], (ro,ko) in Consumers_Utilities_Outputs_All[c,r,f]};

param Consumers_Disutilities_Share_Sum {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Disutilities_Regions_All[c,r], (ro,ko) in Consumers_Disutilities_Outputs_All[c,r,f]};

repeat {
repeat {

  ##### Producers #####
  #
  #####

  printf "Processing Producer Functions\n";

  let Iter := 0;

  ##### Modify for zero scale #####
  #
  #####

  printf {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Scale[p,r,f,ro,ko] = 0} "    Zero scale modification: %s %s %s %s %s %s %s\n", p, r, f, ro, ko, ri, ki;

  let {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Scale[p,r,f,ro,ko] = 0} Producers_Functions_Share[p,r,f,ro,ko,ri,ki] := 0;

  repeat {

    let Iter := Iter + 1;
    printf "  Iteration %d\n", Iter;

    #####
    #
    # Eliminate input commodities for each function
    #   Zero share parameters across the outputs imply its not used
    #   Zero scale parameters across the outputs imply its not used
    #
    ##### Example #####
    #
    #  out1  <=  1.0*ces(1.0, in1, 0.0, in2, 0.0, in3)
    #  out2  <=  1.0*ces(0.0, in1, 0.0, in2, 0.0, in3)
    #  out3  <=  0.0*ces(1.0, in1, 1.0, in2, 1.0, in3)
    #
    ##### Reduced Problem #####
    #
    #  out1  <= 1.0*ces(1.0, in1)
    #  out2  <= 1.0*ces(0.0, in1)
    #  out3  <= 0.0*ces(1.0, in1)
    #
    #####

    let {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r]} Producers_Functions_Inputs_New_Drop[p,r,f] := {(ri,ki) in Producers_Functions_Inputs[p,r,f]: card({(ro,ko) in Producers_Functions_Outputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] = 0}) = card({(ro,ko) in Producers_Functions_Outputs[p,r,f]})};

    printf {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ri,ki) in Producers_Functions_Inputs_New_Drop[p,r,f]} "    Zero share elimination: %s %s %s %s %s\n", p, r, f, ri, ki;

    let {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r]} Producers_Functions_Inputs_Drop[p,r,f] := Producers_Functions_Inputs_Drop[p,r,f] union Producers_Functions_Inputs_New_Drop[p,r,f];

    #####
    #
    # Eliminate output commodities
    #   Zero scale parameters imply its not generated
    #   Zero inputs imply its not used generated
    #
    ##### Example ####
    #
    #  out1  <= 1.0*ces(1.0, in1)
    #  out2  <= 1.0*ces(0.0, in1)
    #  out3  <= 0.0*ces(1.0, in1)
    #
    ##### Reduced Problem #####
    #
    #  out1  <= 1.0*ces(1.0, in1)
    #
    #####
    
    let {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r]} Producers_Functions_Outputs_New_Drop[p,r,f] := {(ro,ko) in Producers_Functions_Outputs[p,r,f]: card(Producers_Functions_Inputs[p,r,f]) = 0};
  
    printf {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs_New_Drop[p,r,f]} "    Zero input elimination: %s %s %s %s %s\n", p, r, f, ro, ko;
  
    let {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r]} Producers_Functions_Outputs_Drop[p,r,f] := Producers_Functions_Outputs_Drop[p,r,f] union Producers_Functions_Outputs_New_Drop[p,r,f];
  
    #####
    #
    # Eliminate intermediates
    #
    #####
    
    printf {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Intermeds[p,r]: card({f in Producers_Functions_Regions[p,r]: (ri,ki) in Producers_Functions_Outputs[p,r,f]}) = 0} "    Intermed not generated: %s %s %s %s\n", p, r, ri, ki;
  
    printf {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Intermeds[p,r]: card({f in Producers_Functions_Regions[p,r]: (ri,ki) in Producers_Functions_Inputs[p,r,f]}) = 0} "    Intermed not used: %s %s %s %s\n", p, r, ri, ki;
  
    let {p in Producers, r in Producers_Regions[p]} Producers_Intermeds_New_Drop[p,r] := {(ri,ki) in Producers_Intermeds[p,r]: card({f in Producers_Functions_Regions[p,r]: (ri,ki) in Producers_Functions_Outputs[p,r,f]}) = 0 or card({f in Producers_Functions_Regions[p,r]: (ri,ki) in Producers_Functions_Inputs[p,r,f]}) = 0};
  
    if ((sum {p in Producers, r in Producers_Regions[p]} card(Producers_Intermeds_New_Drop[p,r])) = 0) then break;
  
    printf {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]: Producers_Functions_Scale[p,r,f,ro,ko] > 0 and (ro,ko) in Producers_Intermeds_New_Drop[p,r]} "      Set zero scale: %s %s %s %s %s\n", p, r, f, ro, ko;
  
    let {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]: (ro,ko) in Producers_Intermeds_New_Drop[p,r]} Producers_Functions_Scale[p,r,f,ro,ko] := 0;
  
    printf {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]: Producers_Functions_Share[p,r,f,ro,ko,ri,ki] > 0 and ((ri,ki) in Producers_Intermeds_New_Drop[p,r] or (ro,ko) in Producers_Intermeds_New_Drop[p,r])} "      Set zero share: %s %s %s %s %s %s %s\n", p, r, f, ro, ko, ri, ki;
  
    let {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]: (ri,ki) in Producers_Intermeds_New_Drop[p,r] or (ro,ko) in Producers_Intermeds_New_Drop[p,r]} Producers_Functions_Share[p,r,f,ro,ko,ri,ki] := 0;
  
    let {p in Producers, r in Producers_Regions[p]} Producers_Intermeds_Drop[p,r] := Producers_Intermeds_Drop[p,r] union Producers_Intermeds_New_Drop[p,r];
  };

  #####
  #
  #  Eliminate functions
  #  
  #####

  printf "Processing Functions\n";
  
  let {p in Producers, r in Producers_Regions[p]} Producers_Functions_Regions_New_Drop[p,r] := {f in Producers_Functions_Regions[p,r]: card(Producers_Functions_Outputs[p,r,f]) + card(Producers_Functions_Inputs[p,r,f]) = 0};
  
  printf {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions_New_Drop[p,r]} "  Regional function elimination: %s %s %s\n", p, r, f;
  
  let {p in Producers, r in Producers_Regions[p]} Producers_Functions_Regions_Drop[p,r] := Producers_Functions_Regions_Drop[p,r] union Producers_Functions_Regions_New_Drop[p,r];
  
  let {p in Producers} Producers_Functions_New_Drop[p] := {f in Producers_Functions[p]: card({r in Producers_Regions[p]: f in Producers_Functions_Regions[p,r]}) = 0};
  
  printf {p in Producers, f in Producers_Functions_New_Drop[p]} "  Function elimination: %s %s\n", p, f;
  
  let {p in Producers} Producers_Functions_Drop[p] := Producers_Functions_Drop[p] union Producers_Functions_New_Drop[p];
  
  #####
  #
  # Eliminate Inputs and Outputs
  #
  #####
  
  printf "Processing Inputs and Outputs\n";
  
  let {p in Producers, r in Producers_Regions[p]} Producers_Inputs_New_Drop[p,r] := {(ri,ki) in Producers_Inputs[p,r]: card({f in Producers_Functions_Regions[p,r]: (ri,ki) in Producers_Functions_Inputs[p,r,f]}) = 0};
  
  printf {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs_New_Drop[p,r]} "  Input elimination: %s %s %s %s\n", p, r, ri, ki;
  
  let {p in Producers, r in Producers_Regions[p]} Producers_Inputs_Drop[p,r] := Producers_Inputs_Drop[p,r] union Producers_Inputs_New_Drop[p,r];
  
  let {p in Producers, r in Producers_Regions[p]} Producers_Outputs_New_Drop[p,r] := {(ro,ko) in Producers_Outputs[p,r]: card({f in Producers_Functions_Regions[p,r]: (ro,ko) in Producers_Functions_Outputs[p,r,f]}) = 0};
  
  printf {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs_New_Drop[p,r]} "  Output elimination: %s %s %s %s\n", p, r, ro, ko;
  
  let {p in Producers, r in Producers_Regions[p]} Producers_Outputs_Drop[p,r] := Producers_Outputs_Drop[p,r] union Producers_Outputs_New_Drop[p,r];
  
  #####
  #
  # Eliminate producers and ownerships
  #
  ####
  
  printf "Processing Producers and Owners\n";
  
  let {p in Producers} Producers_Regions_New_Drop[p] := {r in Producers_Regions[p]: card(Producers_Inputs[p,r]) + card(Producers_Outputs[p,r]) = 0};
  
  printf {p in Producers, r in Producers_Regions_New_Drop[p]} "  Producer elimination: %s %s\n", p, r;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Ownerships_New_Drop[c,r] := {(ro,po) in Consumers_Ownerships[c,r]: ro in Producers_Regions_New_Drop[po]};
  
  printf {c in Consumers, r in Consumers_Regions[c], (ro,po) in Consumers_Ownerships_New_Drop[c,r]} "  Ownership drops: %s %s %s %s\n", c, r, ro, po;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Ownerships_Drop[c,r] := Consumers_Ownerships_Drop[c,r] union Consumers_Ownerships_New_Drop[c,r];
  
  let {p in Producers} Producers_Regions_Drop[p] := Producers_Regions_Drop[p] union Producers_Regions_New_Drop[p];
  
  #####
  #
  # Process shares; make the sum equal one
  #
  #####

  printf "Processing Shares\n";
  
  let {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]} Producers_Functions_Share_Sum[p,r,f,ro,ko] := sum {(ri,ki) in Producers_Functions_Inputs[p,r,f]} Producers_Functions_Share[p,r,f,ro,ko,ri,ki];
  
  printf {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]: abs(Producers_Functions_Share_Sum[p,r,f,ro,ko]-1.0) > 1e-10} "  Modifying shares (%5.4e): %s %s %s %s %s \n", abs(Producers_Functions_Share_Sum[p,r,f,ro,ko]-1.0), p, r, f, ro, ko;
  
  let {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]} Producers_Functions_Share[p,r,f,ro,ko,ri,ki] := Producers_Functions_Share[p,r,f,ro,ko,ri,ki] / Producers_Functions_Share_Sum[p,r,f,ro,ko];
  
  ##### Consumers #####
  #
  #####
  
  printf "Processing Consumer Functions\n";
  
  ##### Objective Function #####
  #
  # maximize  1.0*util1 - 1.0*dutil2
  #
  # util1  <= 1.0*ces(0.0, uin1, 1.0, uin2)
  # util2  <= 0.0*ces(1.0, uin3, 1.0, uin4)
  # util3  <= 1.0*ces(0.0, uin5, 0.0, uin6)
  #
  # dutil1 <= 1.0*ces(0.0, din1, 1.0, din2)
  # dutil2 <= 1.0*ces(1.0, din3, 1.0, din4)
  #
  #####
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Utilities_Objectives_New_Drop[c,r] := {(ro,ko) in Consumers_Utilities_Objectives[c,r]: Consumers_Utilities_Objectives_Weight[c,r,ro,ko] = 0};
  
  printf {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Utilities_Objectives_New_Drop[c,r]} "  Zero utility objective weight: %s %s %s %s\n", c, r, ro, ko;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Utilities_Objectives_Drop[c,r] := Consumers_Utilities_Objectives_Drop[c,r] union Consumers_Utilities_Objectives_New_Drop[c,r];
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Disutilities_Objectives_New_Drop[c,r] := {(ro,ko) in Consumers_Disutilities_Objectives[c,r]: Consumers_Disutilities_Objectives_Weight[c,r,ro,ko] = 0};
  
  printf {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Disutilities_Objectives_New_Drop[c,r]} "  Zero disutility objective weight: %s %s %s %s\n", c, r, ro, ko;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Disutilities_Objectives_Drop[c,r] := Consumers_Disutilities_Objectives_Drop[c,r] union Consumers_Disutilities_Objectives_New_Drop[c,r];
  
  let Iter := 0;
  repeat {
  
    ##### Sample Problem #####
    #
    # maximize  1.0*util1 + 0.0*util2 - 0.0*dutil1 - 1.0*dutil2
    #
    # util1  <= 1.0*ces(0.0, uin1, 1.0, uin2)
    # util2  <= 0.0*ces(1.0, uin3, 1.0, uin4)
    # util3  <= 1.0*ces(0.0, uin5, 0.0, uin6)
    #
    # dutil1 <= 1.0*ces(0.0, din1, 1.0, din2)
    # dutil2 <= 1.0*ces(1.0, din3, 1.0, din4)
    #
    #####
  
    let Iter := Iter + 1;
    printf "  Iteration %d\n", Iter;
  
    ##### Utility Functions #####
    #
    # maximize  1.0*util1 - 1.0*dutil2
    #
    # util1  <= 1.0*ces(1.0, uin2)
    # util2  <= 0.0*ces()
    # util3  <= 1.0*ces()
    #
    # dutil1 <= 1.0*ces(0.0, din1, 1.0, din2)
    # dutil2 <= 1.0*ces(1.0, din3, 1.0, din4)
    #
    #####
  
    printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f], (ri,ki) in Consumers_Utilities_Inputs[c,r,f]: Consumers_Utilities_Scale[c,r,f,ro,ko] = 0} "    Zero scale utility modification: %s %s %s %s %s %s %s\n", c, r, f, ro, ko, ri, ki;
  
    let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f], (ri,ki) in Consumers_Utilities_Inputs[c,r,f]: Consumers_Utilities_Scale[c,r,f,ro,ko] = 0} Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] := 0;
  
    let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r]} Consumers_Utilities_Inputs_New_Drop[c,r,f] := {(ri,ki) in Consumers_Utilities_Inputs[c,r,f]: card({(ro,ko) in Consumers_Utilities_Outputs[c,r,f]: Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] = 0}) = card({(ro,ko) in Consumers_Utilities_Outputs[c,r,f]})};
  
    printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ri,ki) in Consumers_Utilities_Inputs_New_Drop[c,r,f]} "    Zero share utility elimination: %s %s %s %s %s\n", c, r, f, ri, ki;
  
    let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r]} Consumers_Utilities_Inputs_Drop[c,r,f] := Consumers_Utilities_Inputs_Drop[c,r,f] union Consumers_Utilities_Inputs_New_Drop[c,r,f];
  
    #####
    #
    # Eliminate utilities
    #   Zero scale parameters imply its not generated
    #   Zero inputs imply its not used generated
    #
    # maximize  1.0*util1 - 1.0*dutil2
    #
    # util1  <= 1.0*ces(1.0, uin2)
    # util2  <= 0.0
    # util3  <= 0.0
    #
    # dutil1 <= 1.0*ces(1.0, din2)
    # dutil2 <= 1.0*ces(1.0, din3, 1.0, din4)
    #
    #####
    
    let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r]} Consumers_Utilities_Outputs_New_Drop[c,r,f] := {(ro,ko) in Consumers_Utilities_Outputs[c,r,f]: card(Consumers_Utilities_Inputs[c,r,f]) = 0};
  
    printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs_New_Drop[c,r,f]} "    Zero input utility elimination: %s %s %s %s %s\n", c, r, f, ro, ko;
  
    let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r]} Consumers_Utilities_Outputs_Drop[c,r,f] := Consumers_Utilities_Outputs_Drop[c,r,f] union Consumers_Utilities_Outputs_New_Drop[c,r,f];
  
    #####
    #
    # Eliminate intermediates
    #
    # maximize  1.0*util1 - 1.0*dutil2
    #
    # util1  <= 1.0*ces(1.0, uin2)
    #
    # dutil1 <= 0.0*ces(1.0, din2)
    # dutil2 <= 1.0*ces(1.0, din3, 1.0, din4)
    #
    #####
    
    printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Utilities_Intermeds[c,r]: card({f in Consumers_Utilities_Regions[c,r]: (ri,ki) in Consumers_Utilities_Outputs[c,r,f]}) = 0} "    Intermed utility not generated: %s %s %s %s\n", c, r, ri, ki;
  
    printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Utilities_Intermeds[c,r]: card({f in Consumers_Utilities_Regions[c,r]: (ri,ki) in Consumers_Utilities_Inputs[c,r,f]}) + (if (ri,ki) in Consumers_Utilities_Objectives[c,r] then 1) + (if (ri,ki) in Consumers_Disutilities_Factors_Utilities[c,r] then 1) = 0} "    Intermed utility not used: %s %s %s %s\n", c, r, ri, ki;
  
    let {c in Consumers, r in Consumers_Regions[c]} Consumers_Utilities_Intermeds_New_Drop[c,r] := {(ri,ki) in Consumers_Utilities_Intermeds[c,r]: card({f in Consumers_Utilities_Regions[c,r]: (ri,ki) in Consumers_Utilities_Outputs[c,r,f]}) = 0 or card({f in Consumers_Utilities_Regions[c,r]: (ri,ki) in Consumers_Utilities_Inputs[c,r,f]}) + (if (ri,ki) in Consumers_Utilities_Objectives[c,r] then 1) + (if (ri,ki) in Consumers_Disutilities_Factors_Utilities[c,r] then 1) = 0};
  
    printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f]: Consumers_Utilities_Scale[c,r,f,ro,ko] > 0 and (ro,ko) in Consumers_Utilities_Intermeds_New_Drop[c,r]} "    Set zero utility scale: %s %s %s %s %s\n", c, r, f, ro, ko;
  
    let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f]: (ro,ko) in Consumers_Utilities_Intermeds_New_Drop[c,r]} Consumers_Utilities_Scale[c,r,f,ro,ko] := 0;
  
    let {c in Consumers, r in Consumers_Regions[c]} Consumers_Utilities_Intermeds_Drop[c,r] := Consumers_Utilities_Intermeds_Drop[c,r] union Consumers_Utilities_Intermeds_New_Drop[c,r];
  
    ##### Disuitility Functions #####
    #
    # maximize  1.0*util1 - 1.0*dutil2
    #
    # util1  <= 1.0*ces(1.0, uin2)
    # util2  <= 0.0*ces()
    # util3  <= 1.0*ces()
    #
    # dutil1 <= 1.0*ces(1.0, din2)
    # dutil2 <= 1.0*ces(1.0, din3, 1.0, din4)
    #
    #####
  
    printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f], (ri,ki) in Consumers_Disutilities_Inputs[c,r,f]: Consumers_Disutilities_Scale[c,r,f,ro,ko] = 0} "    Zero scale disutility modification: %s %s %s %s %s %s %s\n", c, r, f, ro, ko, ri, ki;
  
    let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f], (ri,ki) in Consumers_Disutilities_Inputs[c,r,f]: Consumers_Disutilities_Scale[c,r,f,ro,ko] = 0} Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki] := 0;
  
    let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r]} Consumers_Disutilities_Inputs_New_Drop[c,r,f] := {(ri,ki) in Consumers_Disutilities_Inputs[c,r,f]: card({(ro,ko) in Consumers_Disutilities_Outputs[c,r,f]: Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki] = 0}) = card({(ro,ko) in Consumers_Disutilities_Outputs[c,r,f]})};
  
    printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ri,ki) in Consumers_Disutilities_Inputs_New_Drop[c,r,f]} "    Zero share disutility elimination: %s %s %s %s %s\n", c, r, f, ri, ki;
  
    let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r]} Consumers_Disutilities_Inputs_Drop[c,r,f] := Consumers_Disutilities_Inputs_Drop[c,r,f] union Consumers_Disutilities_Inputs_New_Drop[c,r,f];
  
    #####
    #
    # Eliminate disutilities
    #   Zero scale parameters imply its not generated
    #   Zero inputs imply its not used generated
    #
    # maximize  1.0*util1 - 1.0*dutil2
    #
    # util1  <= 1.0*ces(1.0, uin2)
    # util2  <= 0.0
    # util3  <= 0.0
    #
    # dutil1 <= 1.0*ces(1.0, din2)
    # dutil2 <= 1.0*ces(1.0, din3, 1.0, din4)
    #
    #####
    
    let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r]} Consumers_Disutilities_Outputs_New_Drop[c,r,f] := {(ro,ko) in Consumers_Disutilities_Outputs[c,r,f]: card(Consumers_Disutilities_Inputs[c,r,f]) = 0};
  
    printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs_New_Drop[c,r,f]} "    Zero input disutility elimination: %s %s %s %s %s\n", c, r, f, ro, ko;
  
    let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r]} Consumers_Disutilities_Outputs_Drop[c,r,f] := Consumers_Disutilities_Outputs_Drop[c,r,f] union Consumers_Disutilities_Outputs_New_Drop[c,r,f];
  
    #####
    #
    # Eliminate intermediates
    #
    # maximize  1.0*util1 - 1.0*dutil2
    #
    # util1  <= 1.0*ces(1.0, uin2)
    #
    # dutil1 <= 0.0*ces(1.0, din2)
    # dutil2 <= 1.0*ces(1.0, din3, 1.0, din4)
    #
    #####
    
    printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Disutilities_Intermeds[c,r]: card({f in Consumers_Disutilities_Regions[c,r]: (ri,ki) in Consumers_Disutilities_Outputs[c,r,f]}) = 0} "    Intermed disutility not generated: %s %s %s %s\n", c, r, ri, ki;
  
    printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Disutilities_Intermeds[c,r]: card({f in Consumers_Disutilities_Regions[c,r]: (ri,ki) in Consumers_Disutilities_Inputs[c,r,f]}) + (if (ri,ki) in Consumers_Disutilities_Objectives[c,r] then 1) + (if (ri,ki) in Consumers_Utilities_Factors_Disutilities[c,r] then 1) = 0} "    Intermed disutility not used: %s %s %s %s\n", c, r, ri, ki;
  
    let {c in Consumers, r in Consumers_Regions[c]} Consumers_Disutilities_Intermeds_New_Drop[c,r] := {(ri,ki) in Consumers_Disutilities_Intermeds[c,r]: card({f in Consumers_Disutilities_Regions[c,r]: (ri,ki) in Consumers_Disutilities_Outputs[c,r,f]}) = 0 or card({f in Consumers_Disutilities_Regions[c,r]: (ri,ki) in Consumers_Disutilities_Inputs[c,r,f]}) + (if (ri,ki) in Consumers_Disutilities_Objectives[c,r] then 1) + (if (ri,ki) in Consumers_Utilities_Factors_Disutilities[c,r] then 1) = 0};
  
    printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]: Consumers_Disutilities_Scale[c,r,f,ro,ko] > 0 and (ro,ko) in Consumers_Disutilities_Intermeds_New_Drop[c,r]} "    Set zero disutility scale: %s %s %s %s %s\n", c, r, f, ro, ko;
  
    let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]: (ro,ko) in Consumers_Disutilities_Intermeds_New_Drop[c,r]} Consumers_Disutilities_Scale[c,r,f,ro,ko] := 0;
  
    let {c in Consumers, r in Consumers_Regions[c]} Consumers_Disutilities_Intermeds_Drop[c,r] := Consumers_Disutilities_Intermeds_Drop[c,r] union Consumers_Disutilities_Intermeds_New_Drop[c,r];
  
    if ((sum {c in Consumers, r in Consumers_Regions[c]} (card(Consumers_Utilities_Intermeds_New_Drop[c,r]) + card(Consumers_Disutilities_Intermeds_New_Drop[c,r]))) = 0) then break;
  };
  
  #####
  #
  #  Eliminate functions
  #  
  #####
  
  printf "Processing Functions\n";
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Utilities_Regions_New_Drop[c,r] := {f in Consumers_Utilities_Regions[c,r]: card(Consumers_Utilities_Outputs[c,r,f]) + card(Consumers_Utilities_Inputs[c,r,f]) = 0};
  
  printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions_New_Drop[c,r]} "  Regional utility elimination: %s %s %s\n", c, r, f;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Utilities_Regions_Drop[c,r] := Consumers_Utilities_Regions_Drop[c,r] union Consumers_Utilities_Regions_New_Drop[c,r];
  
  let {c in Consumers} Consumers_Utilities_New_Drop[c] := {f in Consumers_Utilities[c]: card({r in Consumers_Regions[c]: f in Consumers_Utilities_Regions[c,r]}) = 0};
  
  printf {c in Consumers, f in Consumers_Utilities_New_Drop[c]} "  Utility elimination: %s %s\n", c, f;
  
  let {c in Consumers} Consumers_Utilities_Drop[c] := Consumers_Utilities_Drop[c] union Consumers_Utilities_New_Drop[c];
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Disutilities_Regions_New_Drop[c,r] := {f in Consumers_Disutilities_Regions[c,r]: card(Consumers_Disutilities_Outputs[c,r,f]) + card(Consumers_Disutilities_Inputs[c,r,f]) = 0};
  
  printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions_New_Drop[c,r]} "  Regional disutility elimination: %s %s %s\n", c, r, f;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Disutilities_Regions_Drop[c,r] := Consumers_Disutilities_Regions_Drop[c,r] union Consumers_Disutilities_Regions_New_Drop[c,r];
  
  let {c in Consumers} Consumers_Disutilities_New_Drop[c] := {f in Consumers_Disutilities[c]: card({r in Consumers_Regions[c]: f in Consumers_Disutilities_Regions[c,r]}) = 0};
  
  printf {c in Consumers, f in Consumers_Disutilities_New_Drop[c]} "  Disutility elimination: %s %s\n", c, f;
  
  let {c in Consumers} Consumers_Disutilities_Drop[c] := Consumers_Disutilities_Drop[c] union Consumers_Disutilities_New_Drop[c];
  
  #####
  #
  # Eliminate Utility Factors
  #
  #####
  
  printf "Processing Utility Factors\n";
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Utilities_Factors_Inputs_New_Drop[c,r] := {(ri,ki) in Consumers_Utilities_Factors_Inputs[c,r]: card({f in Consumers_Utilities_Regions[c,r]: (ri,ki) in Consumers_Utilities_Inputs[c,r,f]}) = 0};
  
  printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Utilities_Factors_Inputs_New_Drop[c,r]} "  Utility input factor elimination: %s %s %s %s\n", c, r, ri, ki;
  
  printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Utilities_Factors_Inputs_New_Drop[c,r] inter Consumers_Utilities_Factors_Supplies[c,r]} "    Set supply limit %5.4e: %s %s %s %s\n", min(Consumers_Supplies_Limit[c,r,ri,ki], Consumers_Mapped_Bounds_All[c,r,ri,ki]), c, r, ri, ki;
  
  let {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Utilities_Factors_Inputs_New_Drop[c,r] inter Consumers_Utilities_Factors_Supplies[c,r]} Consumers_Supplies_Limit[c,r,ri,ki] := min(Consumers_Supplies_Limit[c,r,ri,ki], Consumers_Mapped_Bounds_All[c,r,ri,ki]);
  
  printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Utilities_Factors_Inputs_New_Drop[c,r] inter Consumers_Utilities_Factors_Disutilities[c,r]} "    Set disutility output limit %5.4e: %s %s %s %s\n", min(Consumers_Disutilities_Factors_Limit[c,r,ri,ki], Consumers_Mapped_Bounds_All[c,r,ri,ki]), c, r, ri, ki;
  
  let {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Utilities_Factors_Inputs_New_Drop[c,r] inter Consumers_Utilities_Factors_Disutilities[c,r]} Consumers_Disutilities_Factors_Limit[c,r,ri,ki] := min(Consumers_Disutilities_Factors_Limit[c,r,ri,ki], Consumers_Mapped_Bounds_All[c,r,ri,ki]);
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Utilities_Factors_Outputs_New_Drop[c,r] := {(ro,ko) in Consumers_Utilities_Factors_Outputs[c,r]: card({f in Consumers_Utilities_Regions[c,r]: (ro,ko) in Consumers_Utilities_Outputs[c,r,f]}) = 0};

  printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Utilities_Factors_Outputs_New_Drop[c,r]} "  Utility output factor elimination: %s %s %s %s\n", c, r, ri, ki;

  printf {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Utilities_Factors_Outputs_New_Drop[c,r] inter Consumers_Utilities_Objectives[c,r]} "    Set objective weight to zero: %s %s %s %s\n", c, r, ro, ko;

  let {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Utilities_Factors_Outputs_New_Drop[c,r] inter Consumers_Utilities_Objectives[c,r]} Consumers_Utilities_Objectives_Weight[c,r,ro,ko] := 0.0;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Utilities_Factors_Intermeds_New_Drop[c,r] := {(ro,ko) in Consumers_Utilities_Factors_Intermeds[c,r]: (card({f in Consumers_Utilities_Regions[c,r]: (ro,ko) in Consumers_Utilities_Inputs[c,r,f]}) = 0) and  (card({f in Consumers_Utilities_Regions[c,r]: (ro,ko) in Consumers_Utilities_Outputs[c,r,f]}) = 0)};
  
  printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Utilities_Factors_Intermeds_New_Drop[c,r]} "  Utility intermediate factor elimination: %s %s %s %s\n", c, r, ri, ki;
  
  #####
  #
  # Eliminate Disutility Factors
  #
  #####
  
  printf "Processing Disutility Factors\n";
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Disutilities_Factors_Inputs_New_Drop[c,r] := {(ri,ki) in Consumers_Disutilities_Factors_Inputs[c,r]: card({f in Consumers_Disutilities_Regions[c,r]: (ri,ki) in Consumers_Disutilities_Inputs[c,r,f]}) = 0};
  
  printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Disutilities_Factors_Inputs_New_Drop[c,r]} "  Disutility input factor elimination: %s %s %s %s\n", c, r, ri, ki;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Disutilities_Factors_Outputs_New_Drop[c,r] := {(ro,ko) in Consumers_Disutilities_Factors_Outputs[c,r]: card({f in Consumers_Disutilities_Regions[c,r]: (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]}) = 0};
  
  printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Disutilities_Factors_Outputs_New_Drop[c,r]} "  Disutility output factor elimination: %s %s %s %s\n", c, r, ri, ki;
  
  printf {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Disutilities_Factors_Outputs_New_Drop[c,r] inter Consumers_Disutilities_Objectives[c,r]} "    Set objective weight to zero: %s %s %s %s\n", c, r, ro, ko;
  
  let {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Disutilities_Factors_Outputs_New_Drop[c,r] inter Consumers_Disutilities_Objectives[c,r]} Consumers_Disutilities_Objectives_Weight[c,r,ro,ko] := 0.0;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Disutilities_Factors_Intermeds_New_Drop[c,r] := {(ro,ko) in Consumers_Disutilities_Factors_Intermeds[c,r]: (card({f in Consumers_Disutilities_Regions[c,r]: (ro,ko) in Consumers_Disutilities_Inputs[c,r,f]}) = 0) and  (card({f in Consumers_Disutilities_Regions[c,r]: (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]}) = 0)};
  
  printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Disutilities_Factors_Intermeds_New_Drop[c,r]} "  Utility intermediate factor elimination: %s %s %s %s\n", c, r, ri, ki;
  
  ##### Process Factor Drops #####
  #
  #####
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Utilities_Factors_Drop[c,r] := Consumers_Utilities_Factors_Drop[c,r] union Consumers_Utilities_Factors_Inputs_New_Drop[c,r] union Consumers_Utilities_Factors_Outputs_New_Drop[c,r] union Consumers_Utilities_Factors_Intermeds_New_Drop[c,r];
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Disutilities_Factors_Drop[c,r] := Consumers_Disutilities_Factors_Drop[c,r] union Consumers_Disutilities_Factors_Inputs_New_Drop[c,r] union Consumers_Disutilities_Factors_Outputs_New_Drop[c,r] union Consumers_Disutilities_Factors_Intermeds_New_Drop[c,r];
  
  #####
  #
  # Eliminate Demands
  # Supplies not eliminated; preprocessor will handle them
  #
  #####
  
  printf "Processing Demands\n";
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Demands_New_Drop[c,r] := {(ri,ki) in Consumers_Demands[c,r]: card({f in Consumers_Utilities_Regions[c,r]: (ri,ki) in Consumers_Utilities_Inputs[c,r,f]}) + card({f in Consumers_Disutilities_Regions[c,r]: (ri,ki) in Consumers_Disutilities_Inputs[c,r,f]}) = 0};
  
  printf {c in Consumers, r in Consumers_Regions[c], (ri,ki) in Consumers_Demands_New_Drop[c,r]} "  Demand elimination: %s %s %s %s\n", c, r, ri, ki;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Demands_Drop[c,r] := Consumers_Demands_Drop[c,r] union Consumers_Demands_New_Drop[c,r];
  
  #####
  #
  # Process shares; make the sum equal one
  #
  #####
  
  printf "Processing Shares\n";
  
  let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f]} Consumers_Utilities_Share_Sum[c,r,f,ro,ko] := sum {(ri,ki) in Consumers_Utilities_Inputs[c,r,f]} Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki];
  
  printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f]: abs(Consumers_Utilities_Share_Sum[c,r,f,ro,ko]-1.0) > 1e-10} "  Modifying shares (%5.4e): %s %s %s %s %s \n", abs(Consumers_Utilities_Share_Sum[c,r,f,ro,ko]-1.0), c, r, f, ro, ko;
  
  let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f], (ri,ki) in Consumers_Utilities_Inputs[c,r,f]} Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] := Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] / Consumers_Utilities_Share_Sum[c,r,f,ro,ko];
  
  let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]} Consumers_Disutilities_Share_Sum[c,r,f,ro,ko] := sum {(ri,ki) in Consumers_Disutilities_Inputs[c,r,f]} Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki];
  
  printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]: abs(Consumers_Disutilities_Share_Sum[c,r,f,ro,ko]-1.0) > 1e-10} "  Modifying shares (%5.4e): %s %s %s %s %s \n", abs(Consumers_Disutilities_Share_Sum[c,r,f,ro,ko]-1.0), c, r, f, ro, ko;
  
  let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f], (ri,ki) in Consumers_Disutilities_Inputs[c,r,f]} Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki] := Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki] / Consumers_Disutilities_Share_Sum[c,r,f,ro,ko];
  
  #####
  #
  # Process commodities
  #
  #####
  
  let {r in Regions} Region_Commodities_New_Drop[r] := {k in Region_Commodities[r] diff Homogenous_Commodities: card({p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Outputs[p,rp]}) + card({c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Supplies[c,rc]}) + card({c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Endowments[c,rc] and Consumers_Endowment[c,rc,r,k] > 0}) = 0};
  
  printf {r in Regions, k in Region_Commodities_New_Drop[r]} "Zero supply commodity: %s %s\n", r, k;
  
  let {r in Regions} Region_Commodities_New_Drop[r] := {k in Region_Commodities[r] diff Homogenous_Commodities: card({p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Inputs[p,rp]}) + card({c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Demands[c,rc]}) = 0};
  
  printf {r in Regions, k in Region_Commodities_New_Drop[r]} "Zero demand commodity: %s %s\n", r, k;
  
  let {r in Regions} Region_Commodities_New_Drop[r] := {k in Region_Commodities[r] diff Homogenous_Commodities: ({p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Inputs[p,rp]} union {c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Demands[c,rc]}) within ({p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Outputs[p,rp]} union {c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Supplies[c,rc]} union {c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Endowments[c,rc] and Consumers_Endowment[c,rc,r,k] > 0})};

  printf {r in Regions, k in Region_Commodities_New_Drop[r]} "Demand within Supply: %s %s\n", r, k;

  let {r in Regions} Region_Commodities_New_Drop[r] := Region_Commodities_New_Drop[r] union {k in Region_Commodities[r] diff Homogenous_Commodities: (card({p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Outputs[p,rp]}) + card({c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Supplies[c,rc]}) + card({c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Endowments[c,rc] and Consumers_Endowment[c,rc,r,k] > 0}) = 0) or (card({p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Inputs[p,rp]}) + card({c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Demands[c,rc]}) = 0)};
  
  if (sum {r in Regions} card(Region_Commodities_New_Drop[r]) = 0) then break;
  
  printf {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]: ko in Region_Commodities_New_Drop[ro]} "  Producer output commodity elimination: %s %s %s %s %s\n", p, r, f, ro, ko;
  
  let {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]: ko in Region_Commodities_New_Drop[ro]} Producers_Functions_Scale[p,r,f,ro,ko] := 0;
  
  printf {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]: ko in Region_Commodities_New_Drop[ro] or ki in Region_Commodities_New_Drop[ri]} "  Producer input commodity elimination: %s %s %s %s %s %s %s\n", p, r, f, ro, ko, ri, ki;
  
  let {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]: ko in Region_Commodities_New_Drop[ro] or ki in Region_Commodities_New_Drop[ri]} Producers_Functions_Share[p,r,f,ro,ko,ri,ki] := 0;
  
  printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f]: ko in Region_Commodities_New_Drop[ro]} "  Utility output commodity elimination: %s %s %s %s %s\n", c, r, f, ro, ko;
  
  let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f]: ko in Region_Commodities_New_Drop[ro]} Consumers_Utilities_Scale[c,r,f,ro,ko] := 0;
  
  printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]: ko in Region_Commodities_New_Drop[ro]} "  Disutility output commodity elimination: %s %s %s %s %s\n", c, r, f, ro, ko;
  
  let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f]: ko in Region_Commodities_New_Drop[ro]} Consumers_Disutilities_Scale[c,r,f,ro,ko] := 0;
  
  printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f], (ri,ki) in Consumers_Utilities_Inputs[c,r,f]: ki in Region_Commodities_New_Drop[ri]} "  Utility input commodity elimination: %s %s %s %s %s %s %s\n", c, r, f, ro, ko, ri, ki;
  
  let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f], (ri,ki) in Consumers_Utilities_Inputs[c,r,f]: ki in Region_Commodities_New_Drop[ri]} Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] := 0;
  
  printf {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f], (ri,ki) in Consumers_Disutilities_Inputs[c,r,f]: ki in Region_Commodities_New_Drop[ri]} "  Disutility input commodity elimination: %s %s %s %s %s %s %s\n", c, r, f, ro, ko, ri, ki;
  
  let {c in Consumers, r in Consumers_Regions[c], f in Consumers_Disutilities_Regions[c,r], (ro,ko) in Consumers_Disutilities_Outputs[c,r,f], (ri,ki) in Consumers_Disutilities_Inputs[c,r,f]: ki in Region_Commodities_New_Drop[ri]} Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki] := 0;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Demands_New_Drop[c,r] := {(rd,kd) in Consumers_Demands[c,r]: kd in Region_Commodities_New_Drop[rd]};
  
  printf {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands_New_Drop[c,r]} "  Demand elimination: %s %s %s %s\n", c, r, rd, kd;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Demands_Drop[c,r] := Consumers_Demands_Drop[c,r] union Consumers_Demands_New_Drop[c,r];
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Supplies_New_Drop[c,r] := {(rs,ks) in Consumers_Supplies[c,r]: ks in Region_Commodities_New_Drop[rs]};
  
  printf {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies_New_Drop[c,r]} "  Supply elimination: %s %s %s %s\n", c, r, rs, ks;
  
  let {c in Consumers, r in Consumers_Regions[c]} Consumers_Supplies_Drop[c,r] := Consumers_Supplies_Drop[c,r] union Consumers_Supplies_New_Drop[c,r];
  
  let {r in Regions} Region_Commodities_Drop[r] := Region_Commodities_Drop[r] union Region_Commodities_New_Drop[r];
} 

##### Eliminate Producers #####
#
#####

let Producers_New_Drop := {p in Producers: card(Producers_Regions[p]) = 0};

printf {p in Producers_New_Drop} "Drop producer: %s\n", p;

let Producers_Drop := Producers_Drop union Producers_New_Drop;

##### Eliminate Commodities #####
#
#####

let Commodities_New_Drop := {k in Commodities: card({r in Regions: k in Region_Commodities[r]}) = 0};

printf {k in Commodities_New_Drop} "Drop commodity: %s\n", k;

let Commodities_Drop := Commodities_Drop union Commodities_New_Drop;

##### Issue Warnings #####
#
#####

printf {p in Producers, r in Producers_Regions[p]: card(Producers_Inputs[p,r]) = 1} "Single input: %s %s\n", p, r;

##### Recalibrate the model #####
#
#####

printf "Recalibrating\n";
let mods := 0;

let {p in Producers_All, r in Producers_Regions_All[p], (ri,ki) in Producers_Inputs_All[p,r]} Producers_Expenditures[p,r,ri,ki] := 0;
let {p in Producers_All, r in Producers_Regions_All[p], (ri,ki) in Producers_Inputs_All[p,r]} Producers_RRPriceTax_Expenditures[p,r,ri,ki] := 0;
let {p in Producers_All, r in Producers_Regions_All[p], (ri,ki) in Producers_Inputs_All[p,r]} Producers_RIPriceTax_Expenditures[p,r,ri,ki] := 0;
let {p in Producers_All, r in Producers_Regions_All[p], (ri,ki) in Producers_Inputs_All[p,r]} Producers_RRQuantityTax_Expenditures[p,r,ri,ki] := 0;
let {p in Producers_All, r in Producers_Regions_All[p], (ri,ki) in Producers_Inputs_All[p,r]} Producers_RIQuantityTax_Expenditures[p,r,ri,ki] := 0;

let {p in Producers_All, r in Producers_Regions_All[p], (ro,ko) in Producers_Outputs_All[p,r]} Producers_RRPriceTax_Revenues[p,r,ro,ko] := 0;
let {p in Producers_All, r in Producers_Regions_All[p], (ro,ko) in Producers_Outputs_All[p,r]} Producers_ROPriceTax_Revenues[p,r,ro,ko] := 0;
let {p in Producers_All, r in Producers_Regions_All[p], (ro,ko) in Producers_Outputs_All[p,r]} Producers_RRQuantityTax_Revenues[p,r,ro,ko] := 0;
let {p in Producers_All, r in Producers_Regions_All[p], (ro,ko) in Producers_Outputs_All[p,r]} Producers_ROQuantityTax_Revenues[p,r,ro,ko] := 0;

let {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} Producers_RRPriceTax_Revenues[p,r,ro,ko] := Producers_RRPriceTax_Revenues_All[p,r,ro,ko];
let {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} Producers_ROPriceTax_Revenues[p,r,ro,ko] := Producers_ROPriceTax_Revenues_All[p,r,ro,ko];
let {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} Producers_RRQuantityTax_Revenues[p,r,ro,ko] := Producers_RRQuantityTax_Revenues_All[p,r,ro,ko];
let {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} Producers_ROQuantityTax_Revenues[p,r,ro,ko] := Producers_ROQuantityTax_Revenues_All[p,r,ro,ko];

for {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} {
  if ((sum {f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]: (ri,ki) in Producers_Functions_Inputs[p,r,f]} Producers_Functions_Share[p,r,f,ro,ko,ri,ki]) > 0) then {
    let Producers_Expenditures[p,r,ri,ki] := Producers_Expenditures_All[p,r,ri,ki];
    let Producers_RRPriceTax_Expenditures[p,r,ri,ki] := Producers_RRPriceTax_Expenditures_All[p,r,ri,ki];
    let Producers_RIPriceTax_Expenditures[p,r,ri,ki] := Producers_RIPriceTax_Expenditures_All[p,r,ri,ki];
    let Producers_RRQuantityTax_Expenditures[p,r,ri,ki] := Producers_RRQuantityTax_Expenditures_All[p,r,ri,ki];
    let Producers_RIQuantityTax_Expenditures[p,r,ri,ki] := Producers_RIQuantityTax_Expenditures_All[p,r,ri,ki];
  }
}

reset data Pro_Exp, Pro_Rev, Pro_TRev, Pro_RRev;
repeat {
  let {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_All_Inputs[p,r]} Pro_Exp[p,r,rf,kf] := -Infinity;
  let {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_All_Outputs[p,r]} Pro_Rev[p,r,rf,kf] := -Infinity;

  let {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} Pro_Exp[p,r,ri,ki] :=
    Producers_Expenditures[p,r,ri,ki] + 
    Producers_RRPriceTax_Expenditures[p,r,ri,ki] + Producers_RIPriceTax_Expenditures[p,r,ri,ki] +
    Producers_RRQuantityTax_Expenditures[p,r,ri,ki] + Producers_RIQuantityTax_Expenditures[p,r,ri,ki];

  repeat {
    let term := 1;

    for {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f]: Pro_Rev[p,r,ro,ko] == -Infinity and min {(ri,ki) in Producers_Functions_Inputs[p,r,f]} Pro_Exp[p,r,ri,ki] > -Infinity} {
      let term := 0;
      let Pro_Rev[p,r,ro,ko] := sum {(ri,ki) in Producers_Functions_Inputs[p,r,f]} Pro_Exp[p,r,ri,ki];
      if ((ro,ko) in Producers_Intermeds[p,r]) then {
        let Pro_Exp[p,r,ro,ko] := Pro_Rev[p,r,ro,ko];
      }
    }
  } until term;

  if (min {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Inputs[p,r]} Pro_Exp[p,r,rf,kf] == -Infinity) or
     (min {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Outputs[p,r]} Pro_Rev[p,r,rf,kf] == -Infinity) then {
    printf "  Cycle detected or factor not referenced:\n";
    printf {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Inputs[p,r]: Pro_Exp[p,r,rf,kf] == -Infinity} "    Input:  %s %s %s %s\n", p, r, rf, kf;
    printf {p in Producers, r in Producers_Regions[p], (rf,kf) in Producers_Outputs[p,r]: Pro_Rev[p,r,rf,kf] == -Infinity} "   Output: %s %s %s %s\n", p, r, rf, kf;
    exit;
  }

  let {p in Producers, r in Producers_Regions[p]} Pro_TRev[p,r] := sum {(ro,ko) in Producers_Outputs[p,r]} Pro_Rev[p,r,ro,ko];
  let {r in Regions} Pro_RRev[r] := sum {p in Producers: r in Producers_Regions[p]} Pro_TRev[p,r];

  let term := 1;
  for {p in Producers, r in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,r]} {
    if (Pro_TRev[p,r] > 0 and Pro_Exp[p,r,ri,ki] > 0 and Pro_Exp[p,r,ri,ki] / Pro_TRev[p,r] < ShareTol) then {
      printf "  Producer share elimination (%5.4e): %s %s %s %s\n", Pro_Exp[p,r,ri,ki] / Pro_TRev[p,r], p, r, ri, ki;
      let Producers_Expenditures[p,r,ri,ki] := 0;
      let Producers_RRPriceTax_Expenditures[p,r,ri,ki] := 0;
      let Producers_RIPriceTax_Expenditures[p,r,ri,ki] := 0;
      let Producers_RRQuantityTax_Expenditures[p,r,ri,ki] := 0;
      let Producers_RIQuantityTax_Expenditures[p,r,ri,ki] := 0;
      let term := 0;
      let mods := mods + 1;
    }
  }

  for {p in Producers, r in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,r]} {
    if (Pro_RRev[r] > 0 and Pro_Rev[p,r,ro,ko] > 0 and Pro_Rev[p,r,ro,ko] / Pro_RRev[r] < RegionTol) then {
      printf "  Producer elimination (%5.4e): %s %s %s %s\n", Pro_Rev[p,r,ro,ko] / Pro_RRev[r], p, r, ro, ko;
    }
  }
} until term;

let {p in Producers_All, r in Producers_Regions_All[p], f in Producers_Functions_Regions_All[p,r], (ro,ko) in Producers_Functions_Outputs_All[p,r,f], (ri,ki) in Producers_Functions_Inputs_All[p,r,f]} Producers_Functions_Share[p,r,f,ro,ko,ri,ki] := 0;
for  {p in Producers, r in Producers_Regions[p], f in Producers_Functions_Regions[p,r], (ro,ko) in Producers_Functions_Outputs[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]} {
  if (Pro_Rev[p,r,ro,ko] > 0 and Pro_Exp[p,r,ri,ki] > 0) then {
    let Producers_Functions_Share[p,r,f,ro,ko,ri,ki] := Pro_Exp[p,r,ri,ki] / Pro_Rev[p,r,ro,ko];
  }
}

let {c in Consumers_All, r in Consumers_Regions_All[c], (rd,kd) in Consumers_Demands_All[c,r]} Consumers_Expenditures[c,r,rd,kd] := 0;
let {c in Consumers_All, r in Consumers_Regions_All[c], (rd,kd) in Consumers_Demands_All[c,r]} Consumers_RRPriceTax_Expenditures[c,r,rd,kd] := 0;
let {c in Consumers_All, r in Consumers_Regions_All[c], (rd,kd) in Consumers_Demands_All[c,r]} Consumers_RDPriceTax_Expenditures[c,r,rd,kd] := 0;
let {c in Consumers_All, r in Consumers_Regions_All[c], (rd,kd) in Consumers_Demands_All[c,r]} Consumers_RRQuantityTax_Expenditures[c,r,rd,kd] := 0;
let {c in Consumers_All, r in Consumers_Regions_All[c], (rd,kd) in Consumers_Demands_All[c,r]} Consumers_RDQuantityTax_Expenditures[c,r,rd,kd] := 0;

let {c in Consumers_All, r in Consumers_Regions_All[c], (rs,ks) in Consumers_Supplies_All[c,r]} Consumers_Revenues[c,r,rs,ks] := 0;
let {c in Consumers_All, r in Consumers_Regions_All[c], (rs,ks) in Consumers_Supplies_All[c,r]} Consumers_RRPriceTax_Revenues[c,r,rs,ks] := 0;
let {c in Consumers_All, r in Consumers_Regions_All[c], (rs,ks) in Consumers_Supplies_All[c,r]} Consumers_RSPriceTax_Revenues[c,r,rs,ks] := 0;
let {c in Consumers_All, r in Consumers_Regions_All[c], (rs,ks) in Consumers_Supplies_All[c,r]} Consumers_RRQuantityTax_Revenues[c,r,rs,ks] := 0;
let {c in Consumers_All, r in Consumers_Regions_All[c], (rs,ks) in Consumers_Supplies_All[c,r]} Consumers_RSQuantityTax_Revenues[c,r,rs,ks] := 0;

let {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Consumers_Expenditures[c,r,rd,kd] := Consumers_Expenditures_All[c,r,rd,kd];
let {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Consumers_RRPriceTax_Expenditures[c,r,rd,kd] := Consumers_RRPriceTax_Expenditures_All[c,r,rd,kd];
let {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Consumers_RDPriceTax_Expenditures[c,r,rd,kd] := Consumers_RDPriceTax_Expenditures_All[c,r,rd,kd];
let {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Consumers_RRQuantityTax_Expenditures[c,r,rd,kd] := Consumers_RRQuantityTax_Expenditures_All[c,r,rd,kd];
let {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Consumers_RDQuantityTax_Expenditures[c,r,rd,kd] := Consumers_RDQuantityTax_Expenditures_All[c,r,rd,kd];

let {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]} Consumers_Revenues[c,r,rs,ks] := Consumers_Revenues_All[c,r,rs,ks];
let {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]} Consumers_RRPriceTax_Revenues[c,r,rs,ks] := Consumers_RRPriceTax_Revenues_All[c,r,rs,ks];
let {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]} Consumers_RSPriceTax_Revenues[c,r,rs,ks] := Consumers_RSPriceTax_Revenues_All[c,r,rs,ks];
let {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]} Consumers_RRQuantityTax_Revenues[c,r,rs,ks] := Consumers_RRQuantityTax_Revenues_All[c,r,rs,ks];
let {c in Consumers, r in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,r]} Consumers_RSQuantityTax_Revenues[c,r,rs,ks] := Consumers_RSQuantityTax_Revenues_All[c,r,rs,ks];

reset data Con_Exp, Con_Utl, Con_TUtl;
repeat {
  let {c in Consumers, r in Consumers_Regions[c], (rf,kf) in Consumers_All_Inputs[c,r]} Con_Exp[c,r,rf,kf] := -Infinity;
  let {c in Consumers, r in Consumers_Regions[c], (rf,kf) in Consumers_All_Outputs[c,r]} Con_Utl[c,r,rf,kf] := -Infinity;

  let {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Con_Exp[c,r,rd,kd] :=
    Consumers_Expenditures[c,r,rd,kd] + 
    Consumers_RRPriceTax_Expenditures[c,r,rd,kd] + Consumers_RDPriceTax_Expenditures[c,r,rd,kd] +
    Consumers_RRQuantityTax_Expenditures[c,r,rd,kd] + Consumers_RDQuantityTax_Expenditures[c,r,rd,kd];

  repeat {
    let term := 1;

    for {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f]: Con_Utl[c,r,ro,ko] == -Infinity and min {(rd,kd) in Consumers_Utilities_Inputs[c,r,f]} Con_Exp[c,r,rd,kd] > -Infinity} {
      let term := 0;
      let Con_Utl[c,r,ro,ko] := sum {(rd,kd) in Consumers_Utilities_Inputs[c,r,f]} Con_Exp[c,r,rd,kd];
      if ((ro,ko) in Consumers_Utilities_Intermeds[c,r]) then {
        let Con_Exp[c,r,ro,ko] := Con_Utl[c,r,ro,ko];
      }
    }
  } until term;

  if (min {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} Con_Exp[c,r,rd,kd] == -Infinity) or
     (min {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Utilities_Objectives[c,r]} Con_Utl[c,r,ro,ko] == -Infinity) then {
    printf "  Cycle detected or factor not referenced:\n";
    printf {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]: Con_Exp[c,r,rd,kd] == -Infinity} "   Demand :  %s %s %s %s\n", c, r, rd, kd;
    printf {c in Consumers, r in Consumers_Regions[c], (ro,ko) in Consumers_Utilities_Objectives[c,r]: Con_Utl[c,r,ro,ko] == -Infinity} "   Utility: %s %s %s %s\n", c, r, ro, ko;
    exit;
  }

  let {c in Consumers, r in Consumers_Regions[c]} Con_TUtl[c,r] := sum {(ro,ko) in Consumers_Utilities_Objectives[c,r]} Con_Utl[c,r,ro,ko];

  let term := 1;
  for {c in Consumers, r in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,r]} {
    if (Con_TUtl[c,r] > 0 and Con_Exp[c,r,rd,kd] > 0 and Con_Exp[c,r,rd,kd] / Con_TUtl[c,r] < ShareTol) then {
      printf "  Consumer share elimination (%5.4e): %s %s %s %s\n", Con_Exp[c,r,rd,kd] / Con_TUtl[c,r], c, r, rd, kd;
      let Consumers_Expenditures[c,r,rd,kd] := 0;
      let Consumers_RRPriceTax_Expenditures[c,r,rd,kd] := 0;
      let Consumers_RDPriceTax_Expenditures[c,r,rd,kd] := 0;
      let Consumers_RRQuantityTax_Expenditures[c,r,rd,kd] := 0;
      let Consumers_RDQuantityTax_Expenditures[c,r,rd,kd] := 0;
      let term := 0;
      let mods := mods + 1;
    }
  }
} until term;

let {c in Consumers_All, r in Consumers_Regions_All[c], f in Consumers_Utilities_Regions_All[c,r], (ro,ko) in Consumers_Utilities_Outputs_All[c,r,f], (ri,ki) in Consumers_Utilities_Inputs_All[c,r,f]} Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] := 0;
for  {c in Consumers, r in Consumers_Regions[c], f in Consumers_Utilities_Regions[c,r], (ro,ko) in Consumers_Utilities_Outputs[c,r,f], (ri,ki) in Consumers_Utilities_Inputs[c,r,f]} {
  if (Con_Utl[c,r,ro,ko] > 0 and Con_Exp[c,r,ri,ki] > 0) then {
    let Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki] := Con_Exp[c,r,ri,ki] / Con_Utl[c,r,ro,ko];
  }
}

printf"Modification: %d\n", mods;
} until (!mods);

