##### Output the model #####
#
#####

for {r in Regions} {
  printf "Region: %s\n", r;

  for {p in Producers: r in Producers_Regions[p]} {
    printf "  Producer: %s\n", p;
    
    printf "    Maximize";
    for {(ro,ko) in Producers_Outputs[p,r]} {
      if (ko in Homogenous_Commodities) then {
        printf " + (%5.4e*hprice[%s] - %5.4e)*out[%s,%s,%s,%s]", 
          (Producers_Revenues[p,r,ro,ko] - 
           Producers_RRPriceTax_Revenues[p,r,ro,ko]*Producers_RRPriceRate_Revenues[p,r,ro,ko] -
            Producers_ROPriceTax_Revenues[p,r,ro,ko]*Producers_ROPriceRate_Revenues[p,r,ro,ko])/Producers_Scale[p,r], ko, 
          (Producers_RRQuantityTax_Revenues[p,r,ro,ko]*Producers_RRQuantityRate_Revenues[p,r,ro,ko] +
           Producers_ROQuantityTax_Revenues[p,r,ro,ko]*Producers_ROQuantityRate_Revenues[p,r,ro,ko])/Producers_Scale[p,r], p, r, ro, ko;
      }
      else {
        printf " + (%5.4e*price[%s,%s] - %5.4e)*out[%s,%s,%s,%s]", 
          (Producers_Revenues[p,r,ro,ko] - 
           Producers_RRPriceTax_Revenues[p,r,ro,ko]*Producers_RRPriceRate_Revenues[p,r,ro,ko] -
            Producers_ROPriceTax_Revenues[p,r,ro,ko]*Producers_ROPriceRate_Revenues[p,r,ro,ko])/Producers_Scale[p,r], ro, ko, 
          (Producers_RRQuantityTax_Revenues[p,r,ro,ko]*Producers_RRQuantityRate_Revenues[p,r,ro,ko] +
           Producers_ROQuantityTax_Revenues[p,r,ro,ko]*Producers_ROQuantityRate_Revenues[p,r,ro,ko])/Producers_Scale[p,r], p, r, ro, ko;
      }
    }
    for {(ri,ki) in Producers_Inputs[p,r]} {
      if (ki in Homogenous_Commodities) then {
        printf " - (%5.4e*hprice[%s] + %5.4e)*in[%s,%s,%s,%s]", 
          (Producers_Expenditures[p,r,ri,ki] +
           Producers_RRPriceTax_Expenditures[p,r,ri,ki]*Producers_RRPriceRate_Expenditures[p,r,ri,ki] +
           Producers_RIPriceTax_Expenditures[p,r,ri,ki]*Producers_RIPriceRate_Expenditures[p,r,ri,ki])/Producers_Scale[p,r], ki,
          (Producers_RRQuantityTax_Expenditures[p,r,ri,ki]*Producers_RRQuantityRate_Expenditures[p,r,ri,ki] +
           Producers_RIQuantityTax_Expenditures[p,r,ri,ki]*Producers_RIQuantityRate_Expenditures[p,r,ri,ki])/Producers_Scale[p,r], p, r, ri, ki;
      }
      else {
        printf " - (%5.4e*price[%s,%s] + %5.4e)*in[%s,%s,%s,%s]", 
          (Producers_Expenditures[p,r,ri,ki] +
           Producers_RRPriceTax_Expenditures[p,r,ri,ki]*Producers_RRPriceRate_Expenditures[p,r,ri,ki] +
           Producers_RIPriceTax_Expenditures[p,r,ri,ki]*Producers_RIPriceRate_Expenditures[p,r,ri,ki])/Producers_Scale[p,r], ri, ki,
          (Producers_RRQuantityTax_Expenditures[p,r,ri,ki]*Producers_RRQuantityRate_Expenditures[p,r,ri,ki] +
           Producers_RIQuantityTax_Expenditures[p,r,ri,ki]*Producers_RIQuantityRate_Expenditures[p,r,ri,ki])/Producers_Scale[p,r], p, r, ri, ki;
      }
    }
    printf "\n\n    Subject to\n";
      
    for {f in Producers_Functions_Regions[p,r]} {
      for {(ro,ko) in Producers_Functions_Positive[p,r,f]} {
        printf "      fout[%s,%s,%s] <= %5.4e*ces(%5.4e", f, ro, ko, Producers_Functions_Scale[p,r,f,ro,ko], Producers_Functions_Sigma[p,r,f,ro,ko];
        for {(ri,ki) in Producers_Functions_Inputs[p,r,f]} {
          printf ", %5.4e, fin[%s,%s,%s]", Producers_Functions_Share[p,r,f,ro,ko,ri,ki], f, ri, ki;
        }
        printf ")\n";
      }

      for {(ro,ko) in Producers_Functions_Leontief[p,r,f], (ri,ki) in Producers_Functions_Inputs[p,r,f]} {
        printf "      fout[%s,%s,%s] <= %5.4e*fin[%s,%s,%s]\n", f, ro, ko, Producers_Functions_Scale[p,r,f,ro,ko], f, ri, ki;
      }
    }

    for {(rf,kf) in Producers_Inputs[p,r]} {
      printf "      in[%s,%s] >=", rf, kf;

      for {f in Producers_Functions_Regions[p,r]: (rf,kf) in Producers_Functions_Inputs[p,r,f]} {
        printf " + fin[%s,%s,%s]", f, rf, kf;
      }
      printf "\n";
    }

    for {(rf,kf) in Producers_Outputs[p,r]} {
      printf "      out[%s,%s] <=", rf, kf;

      for {f in Producers_Functions_Regions[p,r]: (rf,kf) in Producers_Functions_Outputs[p,r,f]} {
        printf " + fout[%s,%s,%s]", f, rf, kf;
      }
      printf "\n";
    }

    for {(rf,kf) in Producers_Intermeds[p,r]} {
      printf "     ";
      for {f in Producers_Functions_Regions[p,r]: (rf,kf) in Producers_Functions_Outputs[p,r,f]} {
        printf " + fout[%s,%s,%s]", f, rf, kf;
      }
      printf " >=";
      for {f in Producers_Functions_Regions[p,r]: (rf,kf) in Producers_Functions_Inputs[p,r,f]} {
        printf " + fin[%s,%s,%s]", f, rf, kf;
      }
      printf "\n";
    }
    printf "\n";
  }
  printf "\n";

  for {c in Consumers: r in Consumers_Regions[c]} {
    printf "  Consumer: %s\n", c;
    
    printf "    Maximize";
    for {(ru,ku) in Consumers_Utilities_Objectives[c,r]} {
      printf " + %5.4e*uout[%s,%s,%s,%s]", Consumers_Utilities_Objectives_Weight[c,r,ru,ku], c, r, ru, ku;
    }
    for {(rd,kd) in Consumers_Disutilities_Objectives[c,r]} {
      printf " - %5.4e*dout[%s,%s,%s,%s]", Consumers_Disutilities_Objectives_Weight[c,r,rd,kd], c, r, rd, kd;
    }
    printf "\n\n    Subject to\n";
      
    for {f in Consumers_Utilities_Regions[c,r]} {
      for {(ro,ko) in Consumers_Utilities_Positive[c,r,f]} {
        printf "      ufout[%s,%s,%s] <= %5.4e*ces(%5.4e", f, ro, ko, Consumers_Utilities_Scale[c,r,f,ro,ko], Consumers_Utilities_Sigma[c,r,f,ro,ko];
        for {(ri,ki) in Consumers_Utilities_Inputs[c,r,f]} {
          printf ", %5.4e, ufin[%s,%s,%s]", Consumers_Utilities_Share[c,r,f,ro,ko,ri,ki], f, ri, ki;
        }
        printf ")\n";
      }

      for {(ro,ko) in Consumers_Utilities_Leontief[c,r,f], (ri,ki) in Consumers_Utilities_Inputs[c,r,f]} {
        printf "      ufout[%s,%s,%s] <= %5.4e*ufin[%s,%s,%s]\n", f, ro, ko, Consumers_Utilities_Scale[c,r,f,ro,ko], f, ri, ki;
      }
    }

    for {f in Consumers_Disutilities_Regions[c,r]} {
      for {(ro,ko) in Consumers_Disutilities_Outputs[c,r,f]} {
        printf "      dfout[%s,%s,%s] >= %5.4e*dut(%5.4e", f, ro, ko, Consumers_Disutilities_Scale[c,r,f,ro,ko], Consumers_Disutilities_Eta[c,r,f,ro,ko];
        for {(ri,ki) in Consumers_Disutilities_Inputs[c,r,f]} {
          printf ", %5.4e, dfin[%s,%s,%s]", Consumers_Disutilities_Share[c,r,f,ro,ko,ri,ki], f, ri, ki;
        }
        printf ")\n";
      }
    }

    for {(rf,kf) in Consumers_Utilities_Factors[c,r]} {
      if (rf,kf) in Consumers_Utilities_Factors_Inputs[c,r] then {
        printf "      uin[%s,%s]", rf, kf;
      }
      else {
        printf "     ";
      }

      for {f in Consumers_Utilities_Regions[c,r]: (rf,kf) in Consumers_Utilities_Outputs[c,r,f]} {
        printf " + ufout[%s,%s,%s]", f, rf, kf;
      }
      printf " >=";

      if (rf,kf) in Consumers_Utilities_Factors_Outputs[c,r] then {
        printf " uout[%s,%s]", rf, kf;
      }
      for {f in Consumers_Utilities_Regions[c,r]: (rf,kf) in Consumers_Utilities_Inputs[c,r,f]} {
        printf " + ufin[%s,%s,%s]", f, rf, kf;
      }
      printf "\n";
    }

    for {(rf,kf) in Consumers_Disutilities_Factors[c,r]} {
      if (rf,kf) in Consumers_Disutilities_Factors_Inputs[c,r] then {
        printf "      din[%s,%s]", rf, kf;
      }
      else {
        printf "     ";
      }

      for {f in Consumers_Disutilities_Regions[c,r]: (rf,kf) in Consumers_Disutilities_Outputs[c,r,f]} {
        printf " + dfout[%s,%s,%s]", f, rf, kf;
      }
      printf " <=";

      if (rf,kf) in Consumers_Disutilities_Factors_Outputs[c,r] then {
        printf " dout[%s,%s]", rf, kf;
      }
      for {f in Consumers_Disutilities_Regions[c,r]: (rf,kf) in Consumers_Disutilities_Inputs[c,r,f]} {
        printf " + dfin[%s,%s,%s]", f, rf, kf;
      }
      printf "\n";
    }

    for {(rd,kd) in Consumers_Utilities_Factors_Demands[c,r]} {
      printf "      uin[%s,%s] <= dem[%s,%s,%s,%s]\n", rd, kd, c, r, rd, kd;
    }

    for {(rs,ks) in Consumers_Utilities_Factors_Supplies[c,r]} {
      printf "      uin[%s,%s] <= %5.4e - sup[%s,%s,%s,%s]\n", rs, ks, Consumers_Mapped_Bounds_All[c,r,rs,ks], c, r, rs, ks;
    }

    for {(rc,kc) in Consumers_Utilities_Factors_Disutilities[c,r]} {
      printf "      uin[%s,%s] <= %5.4e - dout[%s,%s]\n", rc, kc, Consumers_Mapped_Bounds_All[c,r,rc,kc], rc, kc;
    }

    for {(rd,kd) in Consumers_Disutilities_Factors_Demands[c,r]} {
      printf "      din[%s,%s] >= %5.4e - dem[%s,%s,%s,%s]\n", rd, kd, Consumers_Mapped_Bounds_All[c,r,rd,kd], c, r, rd, kd;
    }

    for {(rs,ks) in Consumers_Disutilities_Factors_Supplies[c,r]} {
      printf "      din[%s,%s] >= sup[%s,%s,%s,%s]\n", rs, ks, c, r, rs, ks;
    }

    for {(rc,kc) in Consumers_Disutilities_Factors_Utilities[c,r]} {
      printf "      din[%s,%s] >= %5.4e - uout[%s,%s]\n", rc, kc, Consumers_Mapped_Bounds_All[c,r,rc,kc], rc, kc;
    }

    printf "     ";
    for {(rd,kd) in Consumers_Demands[c,r]} {
      if (kd in Homogenous_Commodities) then {
        printf " + (%5.4e*hprice[%s] + %5.4e)*dem[%s,%s,%s,%s]", 
          (Consumers_Expenditures[c,r,rd,kd] +
           Consumers_RRPriceTax_Expenditures[c,r,rd,kd]*Consumers_RRPriceRate_Expenditures[c,r,rd,kd] +
           Consumers_RDPriceTax_Expenditures[c,r,rd,kd]*Consumers_RDPriceRate_Expenditures[c,r,rd,kd])/Consumers_Scale[c,r], kd,
          (Consumers_RRQuantityTax_Expenditures[c,r,rd,kd]*Consumers_RRQuantityRate_Expenditures[c,r,rd,kd] +
           Consumers_RDQuantityTax_Expenditures[c,r,rd,kd]*Consumers_RDQuantityRate_Expenditures[c,r,rd,kd])/Consumers_Scale[c,r], c, r, rd, kd;
      }
      else {
        printf " + (%5.4e*price[%s,%s] + %5.4e)*dem[%s,%s,%s,%s]", 
          (Consumers_Expenditures[c,r,rd,kd] +
           Consumers_RRPriceTax_Expenditures[c,r,rd,kd]*Consumers_RRPriceRate_Expenditures[c,r,rd,kd] +
           Consumers_RDPriceTax_Expenditures[c,r,rd,kd]*Consumers_RDPriceRate_Expenditures[c,r,rd,kd])/Consumers_Scale[c,r], rd, kd,
          (Consumers_RRQuantityTax_Expenditures[c,r,rd,kd]*Consumers_RRQuantityRate_Expenditures[c,r,rd,kd] +
           Consumers_RDQuantityTax_Expenditures[c,r,rd,kd]*Consumers_RDQuantityRate_Expenditures[c,r,rd,kd])/Consumers_Scale[c,r], c, r, rd, kd;
      }
    }
    printf " <=";
    for {(rs,ks) in Consumers_Supplies[c,r]} {
      if (ks in Homogenous_Commodities) then {
        printf " + (%5.4e*hprice[%s] - %5.4e)*sup[%s,%s,%s,%s]", 
          (Consumers_Revenues[c,r,rs,ks] -
           Consumers_RRPriceTax_Revenues[c,r,rs,ks]*Consumers_RRPriceRate_Revenues[c,r,rs,ks] -
           Consumers_RSPriceTax_Revenues[c,r,rs,ks]*Consumers_RSPriceRate_Revenues[c,r,rs,ks])/Consumers_Scale[c,r], ks, 
        (  Consumers_RRQuantityTax_Revenues[c,r,rs,ks]*Consumers_RRQuantityRate_Revenues[c,r,rs,ks] +
           Consumers_RSQuantityTax_Revenues[c,r,rs,ks]*Consumers_RSQuantityRate_Revenues[c,r,rs,ks])/Consumers_Scale[c,r], c, r, rs, ks;
      }
      else {
        printf " + (%5.4e*price[%s,%s] - %5.4e)*sup[%s,%s,%s,%s]", 
          (Consumers_Revenues[c,r,rs,ks] -
           Consumers_RRPriceTax_Revenues[c,r,rs,ks]*Consumers_RRPriceRate_Revenues[c,r,rs,ks] -
           Consumers_RSPriceTax_Revenues[c,r,rs,ks]*Consumers_RSPriceRate_Revenues[c,r,rs,ks])/Consumers_Scale[c,r], rs, ks, 
        (  Consumers_RRQuantityTax_Revenues[c,r,rs,ks]*Consumers_RRQuantityRate_Revenues[c,r,rs,ks] +
           Consumers_RSQuantityTax_Revenues[c,r,rs,ks]*Consumers_RSQuantityRate_Revenues[c,r,rs,ks])/Consumers_Scale[c,r], c, r, rs, ks;
      }
    }

    for {(re,ke) in Consumers_Endowments[c,r]} {
      if (ke in Homogenous_Commodities) then {
        printf " + %5.4e*hprice[%s]", Consumers_Endowment[c,r,re,ke]/Consumers_Scale[c,r], ke;
      }
      else {
        printf " + %5.4e*price[%s,%s]", Consumers_Endowment[c,r,re,ke]/Consumers_Scale[c,r], re, ke;
      }
    }

    for {(rp,pp) in Consumers_Ownerships[c,r]} {
      printf " + %5.4e*profit[%s,%s]", Consumers_Ownership[c,r,rp,pp]/Consumers_Scale[c,r], pp, rp;
    }
    printf " + %5.4e*tax[%s]\n", Consumers_TaxRebate[c,r], r;

    for {(rl,kl) in Consumers_Utilities_Factors_Inputs[c,r]: Consumers_Utilities_Factors_Limit[c,r,rl,kl] < Infinity} {
      printf "      0 <= uin[%s,%s] <= %5.4e\n", rl, kl, Consumers_Utilities_Factors_Limit[c,r,rl,kl];
    }

    for {(rl,kl) in Consumers_Utilities_Factors_Outputs[c,r]: Consumers_Utilities_Factors_Limit[c,r,rl,kl] < Infinity} {
      printf "      0 <= uout[%s,%s] <= %5.4e\n", rl, kl, Consumers_Utilities_Factors_Limit[c,r,rl,kl];
    }

    for {(rl,kl) in Consumers_Disutilities_Factors_Inputs[c,r]: Consumers_Disutilities_Factors_Limit[c,r,rl,kl] < Infinity} {
      printf "      0 <= din[%s,%s] <= %5.4e\n", rl, kl, Consumers_Disutilities_Factors_Limit[c,r,rl,kl];
    }

    for {(rl,kl) in Consumers_Disutilities_Factors_Outputs[c,r]: Consumers_Disutilities_Factors_Limit[c,r,rl,kl] < Infinity} {
      printf "      0 <= dout[%s,%s] <= %5.4e\n", rl, kl, Consumers_Disutilities_Factors_Limit[c,r,rl,kl];
    }

    for {(rl,kl) in Consumers_Supplies[c,r]: Consumers_Supplies_Limit[c,r,rl,kl] < Infinity} {
      printf "      0 <= sup[%s,%s,%s,%s] <= %5.4e\n", c, r, rl, kl, Consumers_Supplies_Limit[c,r,rl,kl];
    }

    for {(rl,kl) in Consumers_Demands[c,r]: Consumers_Demands_Limit[c,r,rl,kl] < Infinity} {
      printf "      0 <= dem[%s,%s,%s,%s] <= %5.4e\n", c, r, rl, kl, Consumers_Demands_Limit[c,r,rl,kl];
    }
    printf "\n";
  }
  printf "\n";
  
  printf "  tax[%s] =", r;
  for {p in Producers, rr in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,rr]: r == rr} {
    if (Producers_RRPriceTax_Revenues[p,rr,ro,ko]*Producers_RRPriceRate_Revenues[p,rr,ro,ko] or Producers_RRQuantityTax_Revenues[p,rr,ro,ko]*Producers_RRQuantityRate_Revenues[p,rr,ro,ko]) then {
      printf " + (%5.4e*price[%s,%s] + %5.4e)*out[%s,%s,%s,%s]",
        Producers_RRPriceTax_Revenues[p,rr,ro,ko]*Producers_RRPriceRate_Revenues[p,rr,ro,ko], ro, ko,
        Producers_RRQuantityTax_Revenues[p,rr,ro,ko]*Producers_RRQuantityRate_Revenues[p,rr,ro,ko], p, rr, ro, ko;
    }
  }
  for {p in Producers, rr in Producers_Regions[p], (ro,ko) in Producers_Outputs[p,rr]: r == ro} {
    if (Producers_ROPriceTax_Revenues[p,rr,ro,ko]*Producers_ROPriceRate_Revenues[p,rr,ro,ko] or Producers_ROQuantityTax_Revenues[p,rr,ro,ko]*Producers_ROQuantityRate_Revenues[p,rr,ro,ko]) then {
      printf " + (%5.4e*price[%s,%s] + %5.4e)*out[%s,%s,%s,%s]",
        Producers_ROPriceTax_Revenues[p,rr,ro,ko]*Producers_ROPriceRate_Revenues[p,rr,ro,ko], ro, ko,
        Producers_ROQuantityTax_Revenues[p,rr,ro,ko]*Producers_ROQuantityRate_Revenues[p,rr,ro,ko], p, rr, ro, ko;
    }
  }
  for {p in Producers, rr in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,rr]: r == rr} {
    if (Producers_RRPriceTax_Expenditures[p,rr,ri,ki]*Producers_RRPriceRate_Expenditures[p,rr,ri,ki] or Producers_RRQuantityTax_Expenditures[p,rr,ri,ki]*Producers_RRQuantityRate_Expenditures[p,rr,ri,ki]) then {
      printf " + (%5.4e*price[%s,%s] + %5.4e)*in[%s,%s,%s,%s]",
        Producers_RRPriceTax_Expenditures[p,rr,ri,ki]*Producers_RRPriceRate_Expenditures[p,rr,ri,ki], ri, ki,
        Producers_RRQuantityTax_Expenditures[p,rr,ri,ki]*Producers_RRQuantityRate_Expenditures[p,rr,ri,ki], p, rr, ri, ki;
    }
  }
  for {p in Producers, rr in Producers_Regions[p], (ri,ki) in Producers_Inputs[p,rr]: r == ri} {
    if (Producers_RIPriceTax_Expenditures[p,rr,ri,ki]*Producers_RIPriceRate_Expenditures[p,rr,ri,ki] or Producers_RIQuantityTax_Expenditures[p,rr,ri,ki]*Producers_RIQuantityRate_Expenditures[p,rr,ri,ki]) then {
      printf " + (%5.4e*price[%s,%s] + %5.4e)*in[%s,%s,%s,%s]",
        Producers_RIPriceTax_Expenditures[p,rr,ri,ki]*Producers_RIPriceRate_Expenditures[p,rr,ri,ki], ri, ki,
        Producers_RIQuantityTax_Expenditures[p,rr,ri,ki]*Producers_RIQuantityRate_Expenditures[p,rr,ri,ki], p, rr, ri, ki;
    }
  }
  for {c in Consumers, rr in Consumers_Regions[c], (rs,ks) in Consumers_Supplies[c,rr]: r == rs} {
    if (Consumers_RSPriceTax_Revenues[c,rr,rs,ks]*Consumers_RSPriceRate_Revenues[c,rr,rs,ks] or Consumers_RSQuantityTax_Revenues[c,rr,rs,ks]*Consumers_RSQuantityRate_Revenues[c,rr,rs,ks]) then {
      printf " + (%5.4e*price[%s,%s] + %5.4e)*sup[%s,%s,%s,%s]",
        Consumers_RSPriceTax_Revenues[c,rr,rs,ks]*Consumers_RSPriceRate_Revenues[c,rr,rs,ks], rs, ks,
        Consumers_RSQuantityTax_Revenues[c,rr,rs,ks]*Consumers_RSQuantityRate_Revenues[c,rr,rs,ks], c, rr, rs, ks;
    }
  }
  for {c in Consumers, rr in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,rr]: r == rr} {
    if (Consumers_RRPriceTax_Expenditures[c,rr,rd,kd]*Consumers_RRPriceRate_Expenditures[c,rr,rd,kd] or Consumers_RRQuantityTax_Expenditures[c,rr,rd,kd]*Consumers_RRQuantityRate_Expenditures[c,rr,rd,kd]) then {
      printf " + (%5.4e*price[%s,%s] + %5.4e)*dem[%s,%s,%s,%s]",
        Consumers_RRPriceTax_Expenditures[c,rr,rd,kd]*Consumers_RRPriceRate_Expenditures[c,rr,rd,kd], rd, kd,
        Consumers_RRQuantityTax_Expenditures[c,rr,rd,kd]*Consumers_RRQuantityRate_Expenditures[c,rr,rd,kd], c, rr, rd, kd;
    }
  }
  for {c in Consumers, rr in Consumers_Regions[c], (rd,kd) in Consumers_Demands[c,rr]: r == rd} {
    if (Consumers_RDPriceTax_Expenditures[c,rr,rd,kd]*Consumers_RDPriceRate_Expenditures[c,rr,rd,kd] or Consumers_RDQuantityTax_Expenditures[c,rr,rd,kd]*Consumers_RDQuantityRate_Expenditures[c,rr,rd,kd]) then {
      printf " + (%5.4e*price[%s,%s] + %5.4e)*dem[%s,%s,%s,%s]",
        Consumers_RDPriceTax_Expenditures[c,rr,rd,kd]*Consumers_RDPriceRate_Expenditures[c,rr,rd,kd], rd, kd,
        Consumers_RDQuantityTax_Expenditures[c,rr,rd,kd]*Consumers_RDQuantityRate_Expenditures[c,rr,rd,kd], c, rr, rd, kd;
    }
  }
  printf "\n\n";

  for {k in Region_Commodities[r] diff Homogenous_Commodities} {
    printf "  Market[%s,%s]\n", r, k;
    printf "    0 <= price[%s,%s] complements", r, k;
    for {p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Outputs[p,rp]} {
      printf " + %5.4e*out[%s,%s,%s,%s]", Producers_Revenues[p,rp,r,k]/Markets_Scale[r,k], p, rp, r, k;
    }

    for {c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Supplies[c,rc]} {
      printf " + %5.4e*sup[%s,%s,%s,%s]", Consumers_Revenues[c,rc,r,k]/Markets_Scale[r,k], c, rc, r, k;
    }

    for {c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Endowments[c,rc]} {
      printf " + 5.4e", Consumers_Endowment[c,rc,r,k]/Markets_Scale[r,k];
    }

    printf " >=";
    for {p in Producers, rp in Producers_Regions[p]: (r,k) in Producers_Inputs[p,rp]} {
      printf " + %5.4e*in[%s,%s,%s,%s]", Producers_Expenditures[p,rp,r,k]/Markets_Scale[r,k], p, rp, r, k;
    }

    for {c in Consumers, rc in Consumers_Regions[c]: (r,k) in Consumers_Demands[c,rc]} {
      printf " + %5.4e*dem[%s,%s,%s,%s]", Consumers_Expenditures[c,rc,r,k]/Markets_Scale[r,k], c, rc, r, k;
    }
    printf "\n\n";
  }
}

for {k in Homogenous_Commodities} {
  printf "  HMarket[%s]\n", k;
  printf "    0 <= hprice[%s] complements", k;
  for {p in Producers, rp in Producers_Regions[p], (r,k) in Producers_Outputs[p,rp]} {
    printf " + %5.4e*out[%s,%s,%s,%s]", Producers_Revenues[p,rp,r,k]/HMarkets_Scale[k], p, rp, r, k;
  }

  for {c in Consumers, rc in Consumers_Regions[c], (r,k) in Consumers_Supplies[c,rc]} {
    printf " + %5.4e*sup[%s,%s,%s,%s]", Consumers_Revenues[c,rc,r,k]/HMarkets_Scale[k], c, rc, r, k;
  }

  for {c in Consumers, rc in Consumers_Regions[c], (r,k) in Consumers_Endowments[c,rc]} {
    printf " + 5.4e", Consumers_Endowment[c,rc,r,k]/HMarkets_Scale[k];
  }

  printf " >=";
  for {p in Producers, rp in Producers_Regions[p], (r,k) in Producers_Inputs[p,rp]} {
    printf " + %5.4e*in[%s,%s,%s,%s]", Producers_Expenditures[p,rp,r,k]/HMarkets_Scale[k], p, rp, r, k;
  }

  for {c in Consumers, rc in Consumers_Regions[c], (r,k) in Consumers_Demands[c,rc]} {
    printf " + %5.4e*dem[%s,%s,%s,%s]", Consumers_Expenditures[c,rc,r,k]/HMarkets_Scale[k], c, rc, r, k;
  }
  printf "\n\n";
}
