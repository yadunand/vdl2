#!/bin/bash

while getopts ":i:" options; do
  case $options in
    i) export instance=$OPTARG ;;
    *) exit 1;;
  esac
done

echo "expend result/$instance/expend.dat";
echo "limits result/$instance/limits.dat";
echo "price result/$instance/price.dat";
echo "ratio result/$instance/ratio.dat";
echo "solve result/$instance/solve.dat";

echo "ofile result/$instance/stdout";

echo "out.expend_out result/$instance/expend.out";
echo "out.price_out result/$instance/price.out";
echo "out.ratio_out result/$instance/ratio.out";
