type AmplIn;
type StdOut;

type AmplCmd {
  AmplIn mod;
  AmplIn process;
  AmplIn output;
  AmplIn so;
  AmplIn tree;
}

type ExpendDat;
type LimitsDat;
type PriceDat;
type RatioDat;
type SolveDat;

type ExpendOut;
type PriceOut;
type RatioOut;

type AmplFilter {
  ExpendOut expend_out;
  PriceOut price_out;
  RatioOut ratio_out; 
}

type AmplResult {
  ExpendDat expend;
  LimitsDat limits;
  PriceDat price;
  RatioDat ratio;
  SolveDat solve;
  StdOut ofile;
  AmplFilter out;
}

app (AmplResult result) run_ampl (string prefix, string instanceID, AmplCmd cmd)
{
  run_ampl prefix instanceID stdout=@filename(result.ofile);
}

AmplCmd const_cmd <ext;exec="./cmd_mapper.sh">;
/* Update this to reflect the location of your dataset" */
string data_dir = "/panfs/panasas/CMS/data/SEE/rdraws/all";

/* Update this to reflect the length of your dataset */
int runs[]=[1:5000];
foreach i in runs {
  
  string instanceID = @strcat("run", i);

  AmplResult res <ext;exec="./instance_mapper.sh", i=instanceID>;
  res = run_ampl(data_dir, instanceID, const_cmd);
}
