#!/bin/bash

# Swift invocation scripts for various runs

#swift -runid localrun -sites.file ./sites.local.xml \
    #-tc.file ./tc.data ampl.swift
#swift -runid teraport_coaster -sites.file ./sites_coaster-teraport.xml \
   #-tc.file ./tc.data ampl.swift
#swift -runid teraport -sites.file ./sites-teraport.xml \
   #-tc.file ./tc.data ampl.swift
swift -runid rdraws-all_firefly  -config swift.properties \
    -sites.file ./ff-grid.xml -tc.file ./tc.data ampl.swift
#swift -runid ranger_test  -config swift.properties \
    #-sites.file ./tgranger-sge-gram2.xml -tc.file ./tc.data ampl.swift
