program5r_matrix;

%util=linspace(-2.44,4.9103*2,45); 
%util=linspace(0,(5)^0.5,45); util=util.^2;%util=util-2.35; 
load grid
load param
load iteration
UU=grid_regimes;%linspace(-3.42,5.5,40);
%L=linspace(0,1,101);
%W=[linspace(-80,21.1111,27) linspace(25,49,20)  linspace(49,53.13,5)];

d=0;
obj=q1q1+q2q2+ss;
%UTIL1r=zeros(101,52);
%UTIL2r=zeros(101,52);
Ind_r_d=zeros(10,101*52);
Pr_r_d=zeros(10,101*52);

UU1=(v1v1-a1a1.^0.5);
UU2=(v2v2-a2a2.^0.5);
tt=0;
w_new=zeros(40);
for d1=3:40
    %fob=L(d1)*(v1v1-a1a1.^0.5)+(1-L(d1))*(v2v2-a2a2.^0.5);
    for d2=3:40
    d=d+1;    


A=[A2;A3;-UU1;-UU2;M;pc];
b=[zeros(size(A2,1),1);zeros(size(A3,1),1);-UU(d1);-UU(d2);zeros(size(M,1),1);1];
%le=size([A2;A3;-UU1;-UU2],1);
le=size([A2;A3],1);

cputime-tt
tt=cputime;


[vobj,x,mu,status,colstat,it]=lpcplex(-obj,A,b,zeros(size(v1v1)),ones(size(v1v1)),[1:le],[],15000);
cputime-tt
%Supr(d)=vobj;
%UTIL1r(d1,d2)=(v1v1-a1a1.^0.5)*x;
%UTIL2r(d1,d2)=(v2v2-a2a2.^0.5)*x;
STr(d1,d2)=status;
k=find(x>0.00001);
if status==1;
    w_new(d1,d2)=-vobj;
Ind_r_d(1:length(k),d)=k;
Pr_r_d(1:length(k),d)=x(k);

end

if w_new(d1,d2)<-40;
    w_new(d1,d2)=-500
end



if status>1
     w_new(d1,d2)=-500;
 end

%[util(d1) util(d2)]
load iteration;
[d1 d2 iteration]
status

end
end
save supr_exp6 STr w_new Ind_r_d Pr_r_d
