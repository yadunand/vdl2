clear all
% grid of variables
load 0results_interin_4
%c1=[0:40];
%c2=[0:40];
load grid;
%v1=linspace(-2.44,4.9103*2,45);  
v1=grid;%linspace(0,(5)^0.5,45); v1=v1.^2;%v1=v1-2.35; 
%v2=linspace(-2.44,4.9103*2,45); 
v2=v1;
q1=[2 20];
q2=[2 20];
a1=[2 6];
a2=[2 6];
a1i=[6,2];
a2i=[6,2];

sg=length(v1);
%load 0results_i_r_simpler

load tech

%P=[0.6979 0.0021 0.0021 0.2979;0.2991 0.4009 0.0009 0.2991;0.2991 0.0009 0.4009 0.2991;...
%        0.2979 0.0021 0.0021 0.6979];



v1v1=kron(ones(1,sg*16),v1);
v1v1_ind=kron(ones(1,sg*16),[1:sg]);
v2v2=kron(ones(1,16),kron(v2,ones(1,sg)));
v2v2_ind=kron(ones(1,16),kron([1:sg],ones(1,sg)));
q1q1=kron(ones(1,8),kron(q1,ones(1,sg*sg)));
q2q2=kron(ones(1,4),kron(q2,ones(1,sg*sg*2)));
a1a1=kron(ones(1,2),kron(a1,ones(1,sg*sg*4)));
a2a2=kron(a2,ones(1,sg*sg*8));
a2a2i=kron(a2i,ones(1,sg*sg*8));
a1a1i=kron(ones(1,2),kron(a1i,ones(1,sg*sg*4)));
ss=kron(ones(1,16),S_interin(:)');

%for su=1:length(v1v1)
    %susu(su)=Sg_r(v1v1_ind(su),v2v2_ind(su));
    %end


%incentive constraints

U1=v1v1-(a1a1.^0.5);          %utilities for the recommended action
U2=v2v2-(a2a2.^0.5);

u1=v1v1-(a1a1i.^0.5);         %utilities from alternative action
u2=v2v2-(a2a2i.^0.5);


  %incentive constraint of agent 1
  
  
A21=kron([1 0 1 0;0 1 0 1],ones(1,sg*sg*4)).*[U1;U1];          
pp1=[P(2,:)./P(1,:) P(1,:)./P(2,:) P(4,:)./P(3,:) P(3,:)./P(4,:)];
P1=kron([pp1;pp1],ones(1,sg*sg));
A22=kron([1 0 1 0;0 1 0 1],ones(1,sg*sg*4)).*(P1.*[u1;u1]);
A2=A22-A21;

   %incentive constraint of agent 2
   
   
   A31=kron([1 1 0 0;0 0 1 1],ones(1,sg*sg*4)).*[U2;U2];
   pp2=[P(3,:)./P(1,:) P(4,:)./P(2,:) P(1,:)./P(3,:) P(2,:)./P(4,:)];
   P2=kron([pp2;pp2],ones(1,sg*sg));
   A32=kron([1 1 0 0;0 0 1 1],ones(1,sg*sg*4)).*(P2.*[u2;u2]);
   A3=A32-A31;
   
   %clearing unuseful matrixes
   %clear A21 A22 pp1 pp2 P1 P2 A31 A32
   
   
  %mother nature constraint
  id=eye(16);
  id2=kron(eye(4),[1;1;1;1]);
 pr1=P';
  
  %pr=kron(pr1(:)',ones(1,26*26));
  pr=pr1(:)';
  M1=sparse(zeros(size(v1v1)));
  M2=sparse(zeros(size(v1v1)));
  
  for i=1:16
      M1(i,:)=sparse(kron(id(i,:),ones(1,sg*sg)));
      M2(i,:)=sparse(kron(id2(i,:),ones(1,sg*sg*4))*pr(i));
  end
  
M=M1-M2;

    % probability constraint
    
    pc=ones(size(v1v1));
    

 

        
        