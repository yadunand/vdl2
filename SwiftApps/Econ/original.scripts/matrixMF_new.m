% collective organizations code
function[MF]=matrixMF_new(lambda)


clear MF
grid=[];
load grid;
load param

v=grid;
vv1=kron(ones(1,sg1),v);%%%%%%%%%%%NEW
vv2=kron(v,ones(1,sg1));%%%%%%%%%%%%NEW
a1=[effs];                   %effort 1 levels
a2=[effs];                   %effort 2 levels
q1=[2 20];                  %output 1 levels
q2=[2 20];                  %output 2 levels
%mi=linspace(0,1,101);       %mi grid

sg=length(v)^2;

%load 0results_i_g
% Elements for each state.
v1v1=kron(ones(1,16*1),vv1);
v2v2=kron(ones(1,16*1),vv2);
%vv_ind=kron(ones(1,16*1),[1:sg]);
q1q1=kron(ones(1,8*1),kron([2 20],ones(1,sg)));
q2q2=kron(ones(1,4*1),kron([2 20],ones(1,2*sg)));
a1a1=kron(ones(1,2),kron([effs],ones(1,4*sg)));
a2a2=kron(ones(1,1),kron([effs],ones(1,8*sg)));

%utility with weights given by mi

ff=v1v1*lambda+(1-lambda)*v2v2;                   % no effort
f=v1v1*lambda+(1-lambda)*v2v2-lambda*(a1a1.^0.5)-(1-lambda)*(a2a2.^0.5);

load tech

V=[effs(1) effs(2) effs(1) effs(2); effs(1) effs(1) effs(2) effs(2)].^0.5;%[2 6 2 6; 2 2 6 6].^0.5;

M1=sparse(blkdiag(P(1,:).^(-1),P(2,:).^(-1),P(3,:).^(-1),P(4,:).^(-1)));
Palt=[P(2,:);P(3,:);P(4,:);P(1,:);P(3,:);P(4,:)...
        ;P(1,:);P(2,:);P(4,:);P(1,:);P(2,:);P(3,:)];
M2=kron(sparse(eye(1)),kron(kron(M1,[1;1;1]).*...
    [Palt Palt Palt Palt],ones(1,sg)));;

count=1;
I=sparse(eye(4));
MI=sparse(eye(1));
PR=sparse(kron(kron(kron(I,MI),ones(1,sg*4)),[1;1;1]));

eff=[effs(2) effs(1); effs(1) effs(2);effs(2) effs(2);effs(1) effs(1);effs(1) effs(2);effs(2) effs(2);effs(1) effs(1);
effs(2) effs(1);effs(2) effs(2);effs(1) effs(1);effs(2) effs(1);effs(1) effs(2)];

count=1;

    for c2=1:12;
        MF(count,:)=sparse(M2(count,:).*(ff-lambda*(eff(c2,1)^0.5)...
            -(1-lambda)*(eff(c2,2)^0.5)))-sparse(PR(count,:).*f);
        count=count+1;
        c2;
    end





