%This is a program for Pareto program of collateral model with multiple
%goods

clear all;
tic;
%%Define global variables

global alpha lh beta H S G rho phi E R p J 
%%%Set up parameter values and define relevant spaces
%number of HHs
H = 2;
%number of states
S = 2;
%number of goods
G = 2;                      
rho = 2;                  %inverse of elasticity of substitution
phi = 1;                    %utility parameter of good-2
beta = 1.00;               %discount factor

%Probability
p(1,1) = 1/2;
%p(2,1) = 1/3;
p(S, 1) = 1-sum(p(1:S-1,1));

%Return to collateral good
R = ones(S,1);
%fraction of each type
alpha = ones(1, H)./H;

%Promises
J1 = 2^S-1;
J2 = 2^(S-1)-1;
J=J1+J2;
%J=1;
%A = [1 1 1];
A1 = [1   0;
      0   1;
      1   1];
A2 = [1   0];

% C11 = ones(J1, 1);
% C12 = ones(J2, 1);

%D1 = A1.*(kron((Pz.*R)', ones(J1,1)));
D2 = A2.*(kron(R', ones(J2,1)));

%Endowment
        E=zeros(G,1+S,H);
        %Consumption endowment of good-1 in period 0
        E(1,1,1)=3;
        E(1,1,2)=3;
        %E(1,1,3)=3;
        
        %Consumption endowment of good-2 in period 0
        E(2,1,1)=4;
        E(2,1,2)=4;
        %E(2,1,3)=3;

        %Consumption endowment of good-1 in state 1
        E(1,2,1)=1;
        E(1,2,2)=5;
        %E(1,2,3)=1;
        
        %Consumption endowment of good-2 in state 1
        E(2,2,1)=5;
        E(2,2,2)=1;
        %E(2,2,3)=1;
        
        %Consumption endowment of good-1 in state 2
        E(1,3,1)=5;
        E(1,3,2)=1;
        %E(1,3,3)=2;
        
        %Consumption endowment of good-2 in state 2
        E(2,3,1)=1;
        E(2,3,2)=5;
        %E(2,3,3)=1;
        
%         %Consumption endowment of good-1 in state 3
%         E(1,4,1)=1;
%         E(1,4,2)=2;
%         E(1,4,3)=5;
%         
%         %Consumption endowment of good-2 in state 3
%         E(2,4,1)=1.25;
%         E(2,4,2)=1.5;
%         E(2,4,3)=1.25;


%%Generating Grids
%consumption in period-1
nC(1,1) = 17;
nC(1,2) = 17;
%nC(1,3) = 2;
C0n(1,1:nC(1,1)) = linspace(1,3,nC(1,1));
C0n(2,1:nC(1,2)) = linspace(1,4,nC(1,2));
%C0n(3,1:nC(1,3)) = linspace(0,1,nC(1,3));


nC0 = 1;
for g=1:G
    nC0 = nC0*nC(1,g);
end

nCf(1,1) = nC(1,1);     
nCb(1,1) = nC0;
for g=1:G-1
    nCf(1,g+1) = nCf(1,g)*nC(1,g+1);
    nCb(1,g+1) = nCb(1,g)/nC(1,g);
end

C0 = [];
C0 = sparse(C0);
C0 = zeros(G, nC0);
for g=1:G
    C0(g,:) = kron(ones(1,nCf(1,g)/nC(1,g)), kron(C0n(g,1:nC(1,g)), ones(1,nCb(1,g)/nC(1,g))));
end

clear nCf nCb

%Clubs
% nZ = 2;
% Z(1,:) = linspace(1, 6/2, nZ);
% Z(2,:) = linspace(1, 6/2, nZ);
% Z(3,:) = linspace(1, 6/2, nZ);
nZ = 4;
for s=1:S
    Z(s,:) = linspace(0.5, 1, nZ);
end

%Z(:,1) = (3/(3+0.5))*ones(S,1);
%Z(:,5) = (3/(3+0.5625))*ones(S,1);

nZ0 = nZ^S;

nZf(1,1) = nZ;     
nZb(1,1) = nZ0;
for s=1:S-1
    nZf(1,s+1) = nZf(1,s)*nZ;
    nZb(1,s+1) = nZb(1,s)/nZ;
end

Z0 = [];
IZ0 = [];
Z0 = sparse(Z0);
IZ0 = sparse(IZ0);
Z0 = zeros(S, nZ0);
IZ0 = zeros(S*nZ, nZ0);
for s=1:S
    Z0(s,:) = kron(ones(1,nZf(1,s)/nZ), kron(Z(s,:), ones(1,nZb(1,s)/nZ)));
    PZ(s,:) = phi*(Z0(s,:).^(rho));
    IZ0((s-1)*nZ+1:s*nZ,:) = kron(ones(1,nZf(1,s)/nZ), kron(eye(nZ), ones(1,nZb(1,s)/nZ)));
end
D1=[];
D1 = sparse(D1);
D1 = kron(PZ, ones(1,J1)).*kron(ones(1, nZ0), (A1.*kron(R', ones(J1,1)))');
Jhat=J1*nZ0 + J2;
        
%collateral holding
nK = 10;
maxK = 1;
K(1,:) = linspace(0,maxK,nK);
K(1,6) = 0.5000;
% nK =1;
% K =0;
%Contracts holding

% %Feasible K adn Theta
%RT = 2*maxK; %maximum possible theta
nT = 750;
nKT = nK*nT;
KT = [];
thetar=[];
thetar1=[];
thetar = sparse(thetar);
thetar1 = sparse(thetar1);
KT = sparse(KT);
IZ = [];
X=[];
X = sparse(X);
IZ = sparse(IZ);
nX = nKT*nZ0;
count=0;
for i=1:nZ0
    z = Z0(:,i);
    iz = IZ0(:,i);
    nx =0;
    for kk = 1:nK
        kt=0;
        criterion = K(1,kk);
        if K(1,kk)==0
            KT(1,(kk-1)*nT+1:(kk-1)*nT+Jhat+1) = kron(K(1,kk), ones(1,Jhat+1));
            KT(2:Jhat+1,(kk-1)*nT+1:(kk-1)*nT+Jhat+1) = [zeros(Jhat,1) maxK*eye(Jhat)];
            kt = kt+Jhat+1;
            while kt < nT
                thetar1 = maxK*(rand(Jhat,1) - K(1,kk));
                zn = ceil(Jhat*rand(1));
                thetarr = [kron(thetar1, ones(1,zn)) kron([maxK zeros(1,zn)], ones(Jhat,1))];
                pick =  ceil((2*zn+1)*rand(Jhat,1));
                for j = 1:Jhat
                    thetar(j,1) = thetarr(j,pick(j));
                end            
                kt =kt +1;
                KT(1,(kk-1)*nT+kt) = K(1,kk);
                KT(2:Jhat+1,(kk-1)*nT+ kt) = thetar; 
            end

        else
            KT(1,(kk-1)*nT+1:(kk-1)*nT+2*Jhat+1) = kron(K(1,kk), ones(1,2*Jhat+1));
            KT(2:Jhat+1,(kk-1)*nT+1:(kk-1)*nT+J1+Jhat+2) = [zeros(Jhat,1) K(1,kk)*eye(Jhat) -K(1,kk)*[zeros((i-1)*J1,J1); eye(J1); zeros(Jhat-i*J1,J1)] -K(1,kk)*[zeros(Jhat-J2,J2);eye(J2)]];
            kt = kt+J1+J2+Jhat+1;
            while kt < nT
                thetar1 = maxK*(rand(Jhat,1)-(K(1,kk)/maxK)*[zeros((i-1)*J1,1); ones(J1,1); zeros(Jhat-i*J1-J2,1); ones(J2,1)]);
                zn = ceil(Jhat*rand(1));
                thetarr = [kron(thetar1, ones(1,zn)) kron([2 zeros(1,zn) -2], ones(Jhat,1))];
                pick =  ceil((2*zn+2)*rand(Jhat,1));
                for j = 1:Jhat
                    thetar(j,1) = thetarr(j,pick(j));
                end
                criterion = K(1,kk) + sum(min(0,thetar));
                if criterion <0
                    thetar = (K(1,kk)/(K(1,kk) - criterion))*thetar;
                    count = count+1;
                end
                kt =kt +1;
                KT(1,(kk-1)*nT+kt) = K(1,kk);
                KT(2:Jhat+1,(kk-1)*nT+ kt) = thetar; 
            end
        end
    end
    X = [X [KT; kron(z, ones(1,nKT))]];
    IZ= [IZ kron(iz,ones(1,nKT))];
end
    
clear nKTf nKTb KTheta0 thetar1 thetarr thetar
   
clear nZf nZb Z0 IZ0 KT
%Optimal consumption transfers in state s conditional on club z_s
for h = 1:H
    for nx =1: nX
        Pz(:,nx) = phi*(X(1+Jhat+1:1+Jhat+S,nx)).^(rho);
        m(:,nx) = E(1,2:1+S,h)'+ D1*X(2:1+Jhat-J2,nx)+ Pz(:,nx).*(E(2,2:1+S,h)' + R(:,1)*X(1,nx) + D2'*X(1+Jhat-J2+1:1+Jhat,nx));
        C1(:,nx,h) = m(:,nx)./(1 + (Pz(:,nx)*phi).^(1/rho));
        C2(:,nx,h) = (m(:,nx) - C1(:,nx,h))./Pz(:,nx);
        tau1(:,nx,h) = C1(:,nx,h) - (E(1,2:1+S,h)'+ D1*X(2:1+Jhat-J2,nx));
        tau2(:,nx,h) = C2(:,nx,h) - (E(2,2:1+S,h)' +     R(:,1)*X(1,nx) + D2'*X(1+Jhat-J2+1:1+Jhat,nx));
    end
end    
        
%Number of Choice Variables

nv = nC0 + nX;
nV = nv*H;
Xv =[];
Xv = sparse(Xv);
[rX,cX] = size(X);
C0v = [C0 zeros(G,nX)];
Xv = [zeros(rX,nC0) X];

C0H = [];
XH = [];
tauH=[];
IZH=[];
XH = sparse(XH);
hh=[];
for h=1:H
   C0H = [C0H C0v];
   XH = [XH Xv];
   hh = [hh h*ones(1,nv)];
   tauH=[tauH zeros(S,nC0) tau2(:,:,h)];
   IZH = [IZH zeros(nZ*S,nC0) IZ];
end
%x = [x(:,h)]_h
%% Objective function using obj.m function

% Assign lower and upper bounds for x, probability measure or choice variables here

UB=[];
LB=[zeros(nV,1)];

%Probability Constraints
beq1 = ones(2*H,1);
Aeq1= [];
Aeq1 = sparse(Aeq1);
%Aeq1 = zeros(2*H,nV);
for h=1:H
        Aeq1((h-1)*2+1,(h-1)*nv +1:(h-1)*nv+nC0) = ones(1, nC0);
        Aeq1((h-1)*2+2,(h-1)*nv +nC0+1:(h-1)*nv+nC0+nX) = ones(1, nX);
end

%Resource constraint for good-1 in period-0
beq2 = 0;
Aeq2 = [];
Aeq2 = sparse(Aeq2);
for h=1:H
    beq2 = beq2 + alpha(1,h)*E(1,1,h);
    Aeq2 = [Aeq2 C0(1,:)*alpha(1,h) zeros(1,nX)];
end
% Aeq2 = [C0(1,:)*alpha(1,1) zeros(1,nX) C0(1,:)*alpha(1,2) zeros(1,nX)];

%Resource constraint for good-2 in period-0
beq3 = 0;
Aeq3 = [];
Aeq3 = sparse(Aeq3);
for h=1:H
    beq3 = beq3 + alpha(1,h)*E(2,1,h);
    Aeq3 = [Aeq3 C0(2,:)*alpha(1,h) X(1,:)*alpha(1,h)];
end

% Aeq3 = [C0(2,:)*alpha(1,1) X(1,:)*alpha(1,1) C0(2,:)*alpha(1,2) X(1,:)*alpha(1,2)];

%Resource constraint for contracts
beq4 = zeros(Jhat,1);
Aeq4 =[];
Aeq4 = sparse(Aeq4);
for h=1:H
  Aeq4 = [Aeq4 zeros(Jhat,nC0) X(2:1+Jhat,:)*alpha(1,h)];  
end

% Aeq4 = [zeros(J,nC0) X(2:1+J,:)*alpha(1,1) zeros(J,nC0) X(2:1+J,:)*alpha(1,2)];

%Resource constraint for good-1 in club-zs, ordered by [z_1(1); z_1(2) ... z_1(nZ)...z_S(nZ)]
beq5 = zeros(nZ*S,1);
Aeq5 = [];
Aeq5 = sparse(Aeq5);
for h=1:H
    Aeq5 = [Aeq5 zeros(nZ*S,nC0) kron(alpha(1,h)*tau2(:,:,h), ones(nZ,1)).*IZ];
end
%Form matrices
beq=[beq1; beq4; beq5];
Aeq=[Aeq1; Aeq4; Aeq5];
b=[beq2; beq3];
A=[Aeq2; Aeq3];

%Objective function
lh = ones(1,H)./H;
%lh = rand(1,H);
%lh = lh./sum(lh);
lh = [0.40    0.60];
for h=1:H
   U(h,:) = -lh(1,h)*alpha(1,h)*[uh(C0) beta*Vh([C1(:,:,h); C2(:,:,h)])];
end
%clear C0 C1 C2
obj = vec(U');

%%Compute using linprog
% x0 = ones(nV,1)./nV;
% x0(nV,1) = 1;
% x0(nV/2,1) =1;

disp(size(A));
disp(size(b));


options=optimset('LargeScale', 'on', 'Display','on','TolFun',10^-10,'MaxIter',100000,'TolX',10^-10);
[x,fval,exitflag,output,lambda]=linprog(obj, A, b, Aeq, beq, LB, UB, [], options);
exitflag
Opimal = -fval

%Recovering the results
xp = find(x>5*10^-2);

%Recovering prices
Ph10 = lambda.ineqlin(1);
Ph20 = lambda.ineqlin(2);
Phahat = lambda.eqlin(5:4+Jhat-J2);
Pha = lambda.eqlin(5+Jhat-J2:4+Jhat);
Phz = lambda.eqlin(5+Jhat:4+Jhat+S*nZ);


%Price
P10 = Ph10/Ph10;
P20 = Ph20 /Ph10;
Pahat = Phahat/Ph10;
Pa = Pha/Ph10;
Pze = Phz/Ph10;

for s=1:S
   Pzs(s,:) = [zeros(1,(s-1)*nZ) Pze((s-1)*nZ+1:s*nZ)' zeros(1,nZ*S-s*nZ)];
end
PzH = Pzs*IZH(:,xp);
Ptau = 0;


Pb = [P10 P20 P20 Pahat' Pa']*[C0H(:,xp); XH(1,xp); XH(1+1:1+Jhat,xp)] + sum(PzH.*tauH(:,xp));

%consumption in period-0
disp('     c01         c02         k        z1       z2       prob       prices         hh')
disp('------------------------------------------------------------------------------------')
disp([C0H(1,xp)'   C0H(2,xp)'    full(XH(1,xp))'    full(XH(1+Jhat+1:1+Jhat+S,xp))'   x(xp)   Pb'  hh(xp)'])


C0op = C0H(:,xp);
XHop = XH(:,xp);
xop = x(xp);
hhop = hh(xp);

save('result_samelh_crra3.mat', 'C0op', 'XHop', 'xop', 'Pb', 'hhop', 'P20', 'Pze')

time = toc