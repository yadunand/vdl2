function [f] = Vh(x)
global rho phi S p
[m n] = size(x);
if m == 2*S
    f = p'*((x(1:S,:).^(1-rho))./(1-rho) + phi*((x(S+1:2*S,:).^(1-rho))./(1-rho)));
else
    f = p'*((x(1:S,:)'.^(1-rho))./(1-rho) + phi*((x(S+1:2*S,:)'.^(1-rho))./(1-rho)));
end

