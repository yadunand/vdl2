clear all

load a0groups_results_4
load param
load iteration

u1g=UTIL1(:,1:52);
u2g=UTIL2(:,1:52);

mg1=kron(linspace(0,1,101)',ones(1,52));mg1=mg1(:);mg1=mg1';mg2=1-mg1 
%W=[linspace(-80,21.1111,27) linspace(25,49,20) linspace(49,53.13,5)];
%W=[linspace(-80,21.1111,20) linspace(25,65,20) linspace(67,85,12)];
W1=kron(ones(101,1),W)
W1=W1(:,1:52);
st=ST(:,1:52);

%w=linspace(-64,48,15);

%w=linspace(-64,48,15);
W1=[W1(:)]';
u1=[u1g(:)]';
u2=[u2g(:)]';
st=[st(:)]';
for s=1:length(W1)
    if st(s)==3
        W1(s)=-1000^7;
    end
     %    if u1(s)>15
      %  W1(s)=-1000^7;
   % end
    %    if u2(s)>15
     %   W1(s)=-1000^7;
    %end
end

load grid

UU=grid_regimes;%linspace(-3.42,5.5,40);

%UU=linspace(-4.94,10,40);

indexes_ugrid_g=zeros(10,1600);
probs_ugrid_g=zeros(10,1600);
s_u_g=zeros(40);
w_u_g=zeros(40);




IN=[-u1;-u2];
EQ=[ones(size(u1))];
beq=1;
LO=zeros(size(u1));
UP=ones(size(u1));

count=0;
RP=zeros(40);
G=ones(40);
for loop1=1:40
    for loop2=1:40
        count=count+1;
        b=[-UU(loop1);-UU(loop2)];
        %[vobj,X,lambda23,status,colstat,it]=lpcplex(-W1,[IN;EQ],[b;beq],LO,UP,[1:length(b)],[],150000);  
        [vobj,X,lambda23,status,colstat,it]=lpcplex(-W1,[IN;EQ],[b;beq],LO,UP,[1:(length(b)+1)],[],150000); 
        s_u_g(loop1,loop2)=status;
        mu1(loop1,loop2)=mg2*X;
        
        
        %if status==1;
        ij=find(X>0.001);
        indexes_ugrid_g(1:length(ij),count)=ij;
        probs_ugrid_g(1:length(ij),count)=X(ij);
        w_u_g(loop1,loop2)=-vobj;
        %else w_u_g(loop1,loop2)=-100;
        %end
    if status>1
        w_u_g(loop1,loop2)=-500;
    end
           
         load iteration;
        [loop1 loop2 iteration]
    end
end

 save results_ugrid_g5 indexes_ugrid_g probs_ugrid_g w_u_g s_u_g   