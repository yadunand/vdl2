#!/usr/bin/python
# 
# Input files must be stage<X>SolverOutput.<N>, will be renamed to tgz and uncompressed
# Second stage consists of files <XXX>-25_2.mps.out and <xxx>-25_2.mps.sol
#

import glob
import os
import sys
import tarfile
import re
import math
import csv

def msg():
  print "Hello"
  print sys.argv
  print len(sys.argv)

def extractSolutions():
  # array of input files
  # files appear as stage<N>SolverOutput.<K>
  inputs=glob.glob('stage*SolverOutput.*')
  for input in inputs:
    renamedInput="%s.tgz" % input
    print input + ' -> ' +  renamedInput
    os.rename(input,renamedInput)
    tar=tarfile.open(renamedInput)
    for tarinfo in tar:
      tar.extract(tarinfo)
    tar.close()
  print "Done Extracting"

objectives = {}
solutions = {}


def mergeResults():
  #CHANGEME 
  # input: name_loop1_loop2.mps.type
  inputs = glob.glob('*_*_*.mps.*')
  for input in inputs:
    # matches <X>-24_11.mps.out and <X>-24_11.mps.sol
	# the first group is characters and digits: interim, groups1, groups2, relPerf, general
    m=re.match("(\w+)_(\d+)_(\d+).mps.(\w{3})",input)
    #print m.group(1, 2, 3, 4)
    operationName=m.group(1)
    index1=m.group(2)
    index2=m.group(3)
    resultType=m.group(4)
    outputFile='%s_%s_%s.mps.%s' % (operationName, index1, index2, resultType)
    #print outputFile
    resultsKey=index1 + '_' + index2 + '_' + resultType
    #print 'Key= '+resultsKey

    resultsFile=open(outputFile)
    if resultType=='out':
      for line in resultsFile.readlines():
        noop=1
      #NOTE: I will match both Optimal and PrimalInfeasible objectives... are these both ok?
      # FIXME FIXME FIXME
      if (re.match('PrimalInfeasible objective (-?\d*\.?\d*e?.*) .*',line)!=None):
        #TODO: CHANGE THIS VALUE
        obj=999999
        objectives[resultsKey]=obj
        continue
    
      if (re.match('Stopped objective (-?\d*\.?\d*e?.*) .*',line)!=None):
        #TODO: CHANGE THIS VALUE
        obj=999999
        objectives[resultsKey]=obj
        continue    
    
      m=re.match('Optimal objective (-?\d*\.?\d*) - (\d+) iterations time .*',line)
      obj=m.group(1)
      #print obj
      objectives[resultsKey]=obj

    if resultType=='sol':
      solutions[resultsKey]=[]
      #print resultsKey+": "
      for line in resultsFile.readlines():
        # NOTE: I am eliminating "bad" entries (with probabilities very small (Ne-X values)
        # TODO: verify with researcher
        #print line
        if (re.match('\*+',line)!=None):
          continue
        if (re.match('\s*\d+\s*\w+\s*-?[1-9]\.*\d*e-\d+\s*',line)!=None):
          #print "Skipping line %s" % line
          continue
        
        
        m=re.match('\s*(\d+)\s*(\w+)\s*(\d*\.*\d*)\s*',line)
        #print "result: "+m.group(1)+", "+m.group(3)
        solutions[resultsKey].append(m.group(1))
        solutions[resultsKey].append(m.group(3))
        
      #print solutions[resultsKey]
    resultsFile.close()
    

      
def saveOptimizationResults():
  if len(sys.argv) < 2 :
    print "Too few arguments;"
    sys.exit(0)     
  #for x in results.keys():
  #  print "Cell: ",x,"\tValues: ",results[x]
  #print results
  startingValue=math.floor(math.sqrt(2*len(objectives)))
  print "Starting value: %d" % startingValue
  
  potentialMaxFirstValue=startingValue
  potentialMaxFirstKey='%d_1_out' % potentialMaxFirstValue
  
  while (objectives.has_key(potentialMaxFirstKey)):
    #print "It does have the key, INCREASE"
    maxKey=potentialMaxFirstKey
    maxValue=potentialMaxFirstValue
    potentialMaxFirtstValue=potentialMaxFirstValue+1
    potentialMaxFirstKey='%d_1_out' % potentialMaxFirstValue
    #print "Check if %s exists" % potentialMaxKey

  while (objectives.has_key(potentialMaxFirstKey)!= 1):
    #print "it does not have the key, DECREASE"
    maxKey=potentialMaxFirstKey
    potentialMaxFirstValue=potentialMaxFirstValue-1
    maxValue=potentialMaxFirstValue
    potentialMaxFirstKey='%d_1_out' % potentialMaxFirstValue
    #print "Check if %s exists" % potentialMaxKey

  print "potentialMaxFirstValue %s" % potentialMaxFirstValue

  potentialMaxSecondValue=startingValue
  potentialMaxSecondKey='1_%d_out' % potentialMaxSecondValue
  
  while (objectives.has_key(potentialMaxSecondKey)):
    #print "It does have the key, INCREASE"
    maxKey=potentialMaxSecondKey
    maxValue=potentialMaxFirstValue
    potentialMaxSecondValue=potentialMaxSecondValue+1
    potentialMaxSecondKey='1_%d_out' % potentialMaxSecondValue
    #print "Check if %s exists" % potentialMaxKey

  while (objectives.has_key(potentialMaxSecondKey)!= 1):
    #print "it does not have the key, DECREASE"
    maxKey=potentialMaxSecondKey
    potentialMaxSecondValue=potentialMaxSecondValue-1
    maxValue=potentialMaxSecondValue
    potentialMaxSecondKey='1_%d_out' % potentialMaxSecondValue
    #print "Check if %s exists" % potentialMaxKey

  print "potentialMaxSecondValue %s" % potentialMaxSecondValue
  
  #I know I will have lots of arguments, but the first 4 may contain the magic file names
  #find the proper optFileName, should end in _opt.csv
  if (sys.argv[1])[-8:]=='_opt.csv':
    optFileName=sys.argv[1]
  elif (sys.argv[2])[-8:]=='_opt.csv':
    optFileName=sys.argv[2]
  elif (sys.argv[3])[-8:]=='_opt.csv':
    optFileName=sys.argv[3]
  else:
    optFileName=sys.argv[4] 
    
  print "Opt file name is: %s" % optFileName    
    
  writer = csv.writer(open(optFileName,"w"))
  
  for col in range (1,potentialMaxFirstValue+1):
    currRow=[]
    for row in range(1,potentialMaxSecondValue+1):
      index='%d_%d_out' %(col,row)
      #print index+": "+objectives[index]
      currRow.append(objectives[index])
    #print currRow
    writer.writerow(currRow)
  

      
def saveSolutionsResults():
  if len(sys.argv) < 4 :
    print "Too few arguments;"
    sys.exit(0) 
  startingValue=math.floor(math.sqrt(2*len(objectives)))
  print "Starting value: %d" % startingValue
  
  potentialMaxFirstValue=startingValue
  potentialMaxFirstKey='%d_1_out' % potentialMaxFirstValue
  
  while (objectives.has_key(potentialMaxFirstKey)):
    #print "It does have the key, INCREASE"
    maxKey=potentialMaxFirstKey
    maxValue=potentialMaxFirstValue
    potentialMaxFirtstValue=potentialMaxFirstValue+1
    potentialMaxFirstKey='%d_1_out' % potentialMaxFirstValue
    #print "Check if %s exists" % potentialMaxKey

  while (objectives.has_key(potentialMaxFirstKey)!= 1):
    #print "it does not have the key, DECREASE"
    maxKey=potentialMaxFirstKey
    potentialMaxFirstValue=potentialMaxFirstValue-1
    maxValue=potentialMaxFirstValue
    potentialMaxFirstKey='%d_1_out' % potentialMaxFirstValue
    #print "Check if %s exists" % potentialMaxKey

  print "potentialMaxFirstValue %s" % potentialMaxFirstValue

  potentialMaxSecondValue=startingValue
  potentialMaxSecondKey='1_%d_out' % potentialMaxSecondValue
  
  while (objectives.has_key(potentialMaxSecondKey)):
    #print "It does have the key, INCREASE"
    maxKey=potentialMaxSecondKey
    maxValue=potentialMaxFirstValue
    potentialMaxSecondValue=potentialMaxSecondValue+1
    potentialMaxSecondKey='1_%d_out' % potentialMaxSecondValue
    #print "Check if %s exists" % potentialMaxKey

  while (objectives.has_key(potentialMaxSecondKey)!= 1):
    #print "it does not have the key, DECREASE"
    maxKey=potentialMaxSecondKey
    potentialMaxSecondValue=potentialMaxSecondValue-1
    maxValue=potentialMaxSecondValue
    potentialMaxSecondKey='1_%d_out' % potentialMaxSecondValue
    #print "Check if %s exists" % potentialMaxKey

  print "potentialMaxSecondValue %s" % potentialMaxSecondValue

  indexMatrix = {}
  probMatrix = {}
  
  if (sys.argv[1])[-10:]=='_index.csv':
    indexFileName=sys.argv[1]
  elif (sys.argv[2])[-10:]=='_index.csv':
    indexFileName=sys.argv[2]  
  elif (sys.argv[3])[-10:]=='_index.csv':
      indexFileName=sys.argv[3] 
  else:
    indexFileName=sys.argv[4]  

  print "Index file name is: %s" % indexFileName

                    
  if (sys.argv[1])[-9:]=='_prob.csv':
    probFileName=sys.argv[1]
  elif (sys.argv[2])[-9:]=='_prob.csv':
    probFileName=sys.argv[2]
  elif (sys.argv[3])[-9:]=='_prob.csv':
    probFileName=sys.argv[3]    
  else:
    probFileName=sys.argv[4]                   

  print "Prob file name is: %s" % probFileName
  
  indexWriter = csv.writer(open(indexFileName,"w"))
  probWriter = csv.writer(open(probFileName,"w"))
  
  
  #reorder the entries, figure out the max length
  maxXSize=0;
  for row in range(1,potentialMaxFirstValue+1):
    for col in range (1,potentialMaxSecondValue+1):
      index='%d_%d_sol' %(row,col)
      #print "index : %s" % solutions[index]
      currList=solutions[index]
      currListLen=len(currList)

      indexMatrix[index]=[]
      probMatrix[index]=[]
      
      objIndex='%d_%d_out' %(row,col)
      if objectives[objIndex] == 999999 :
        #print "Bad Entry at %d_%d, currListLent = %d" %(row,col,currListLen)
        if currListLen>10:
            currList=currList[:10]  
            currListLen = len(currList)
            # this contains potentially a lot of crap
      else :
        if (currListLen>2*maxXSize):
              maxXSize=currListLen/2
        
      #print "Current List length: %d" % currListLen
      for i in range(0,currListLen/2):
        indexMatrix[index].append(currList[i*2])
        probMatrix[index].append(currList[i*2+1])
      
  print "Max solution size: %d" %maxXSize
  #print out the entries
  for i in range(0,maxXSize):
    indexRow=[]
    probRow=[]
    for row in range(1,potentialMaxFirstValue+1):
      for col in range (1,potentialMaxSecondValue+1):
        index='%d_%d_sol' %(row,col)
        #print solutions[index]
        currProbList=probMatrix[index]
        currIndexList=indexMatrix[index]
        currListLen=len(currIndexList)
        #print "CurrListlen = %d" %currListLen
        
        if (i<currListLen):
          indexRow.append(currIndexList[i])
          probRow.append(currProbList[i])
        else:
          indexRow.append(0)
          probRow.append(0)
        
    if len(indexRow)!=len(probRow):
        print "ERROR: At %s %s , probs and indexes are not equal size" % (row, col)
    indexWriter.writerow(indexRow)
    probWriter.writerow(probRow)
  

#msg()
extractSolutions()

mergeResults()
saveSolutionsResults()
saveOptimizationResults()
