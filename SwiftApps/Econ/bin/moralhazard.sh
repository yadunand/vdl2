#!/bin/bash

####
# 
#  Load the matlab code from file $1, pass on the parameters $2=size of batch and 
#  $3 = starting position of the batch in the linearized grid
#  Then invoke the solver to execute the resulting files
#  Name the output files such that the workflow knows what to return to the user
#
####

export LOADPATH=/home/tstef/local/x64/lp_solve_5.5/extra/octave/lpsolve
export LD_LIBRARY_PATH=/home/tstef/local/x64/lp_solve_5.5/lpsolve55:$LD_LIBRARY_PATH
export OCTAVE_BATCH_SIZE=$2
export OCTAVE_BATCH_POS=$3
export OCTAVE_INPUT_FILE_NAME=$4
export OCTAVE_OUTPUT_FILE_NAME=$5
/home/tstef/local/x64/octave-2.9.9/bin/octave $1

for i in *.mps; do /home/tstef/local/x64/Clp-1.3.3/bin/clp $i -maxIt 1000 -solve -solution $i.sol > $i.out; done

if [ -e "groups1_save.mat" ]; then
    tar czvf $5.$3 *.out *.sol groups1_save.mat
else
	tar czvf $5.$3 *.out *.sol
fi

