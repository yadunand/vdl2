
alias swift="swift  -tc.file ~/local/swift-conf/tc.data -sites.file ~/local/swift-conf/sites.xml"

export JAVA_HOME=/opt/jdk1.6.0
#export ANT_HOME=/home/tiberius/local/apache-ant-1.7.0
#export VDS_HOME=/home/tiberius/local/cog/modules/vdsk/dist/vdsk-1.0
#export VDS_HOME=/home/tiberius/local/vdsk-070226
#export VDS_HOME=/opt/vdsk-070429
export VDS_HOME=/Users/tiberius/local/cogl/modules/vdsk/dist/vdsk-1.0
export PATH=$PATH:$VDS_HOME/bin:$JAVA_HOME/bin

export GLOBUS_TCP_PORT_RANGE=50000,51000
