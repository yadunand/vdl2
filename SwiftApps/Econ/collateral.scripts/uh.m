function [f] = uh(x)
global rho phi
[m n] = size(x);
if m == 2
    f = (x(1,:).^(1-rho))./(1-rho) + phi*((x(2,:).^(1-rho))./(1-rho));
else
    f =(x(1,:)'.^(1-rho))./(1-rho) + phi*((x(2,:)'.^(1-rho))./(1-rho));
end

