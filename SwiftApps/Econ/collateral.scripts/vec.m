function v = vec(M)

[m n] = size(M);

v = zeros(m*n,1);
for k=1:n
    v((k-1)*m+1:k*m,1) = M(:,k);
end

    