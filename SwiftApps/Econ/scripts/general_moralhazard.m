clear all
%load results_ugrid_r7 
%load supr_exp6
%%%%%% load from stage 4
%%% Make parameter dependent later
load('-ascii','stage_4_opt.csv');
w_new=-stage_4_opt;
load('-ascii','stage_4_index.csv');
Ind_r_d=stage_4_index;
load('-ascii','stage_4_prob.csv'); 
Pr_r_d=stage_4_prob;


%%% load stage 3
%load results_ugrid_g5
%%% Make parameter dependent later

load('-ascii','stage_3_opt.csv');
w_u_g=-stage_3_opt;
load('-ascii','stage_3_index.csv');
indexes_ugrid_g=stage_3_index;
load('-ascii','stage_3_prob.csv'); 
probs_ugrid_g=stage_3_prob;


load inputs/param;

w_u_r=w_new;
%j_u_r=find(w_u_r-cnc<-3); w_u_r(j_u_r)=-100000000;
%j_u_g=find(w_u_g<-3); w_u_g(j_u_g)=-100000000;


load inputs/grid
UU=grid_regimes;%linspace(-3.42,5.5,40);
UU1=grid_total;%linspace(-3.42,5.5,30);

%UU=linspace(-4.94,6,40);
w_u_r1=w_u_r(:)';

U1=kron(ones(1,40),UU);
U1in=kron(ones(1,40),1:40);
U2=kron(UU,ones(1,40));
U2in=kron(1:40,ones(1,40));


%jj1=find(U1in>25);jj2=find(U2in>25);
%w_u_r1(jj1)=-10^5;w_u_r1(jj2)=-10^5;

U1=[U1 U1];
U2=[U2 U2];
W=[w_u_g(:)' w_u_r1-cnc];
i_g=kron([1 0],ones(1,length(U1)/2));
i_r=kron([0 1],ones(1,length(U1)/2));


%UU=linspace(-4.94,6,40);

EQ=ones(size(U1));
obj=W;

IN=-[U1;U2];

count=0;

LO=zeros(size(U1));
UP=ones(size(U1));

w_total=zeros(30);
p_g=zeros(30);
p_r=zeros(30);

a_r=ones(30)*2
a_r2=ones(30)*2

Ind_total=zeros(10,1600);
Probs_total=zeros(10,1600);
load inputs/ep;


%%%%%%%%%%%%% start setting up the lp_problem %%%%%%%%%%%%%%%%%%%%%

[lp_solve_m,lp_solve_n] = size([IN;EQ]);
lp_handle = octlpsolve('make_lp', lp_solve_m, lp_solve_n);
octlpsolve('set_verbose', lp_handle, 3);
octlpsolve('set_mat', lp_handle, [IN;EQ]);

octlpsolve('set_obj_fn', lp_handle, -W);
%octlpsolve('set_maxim', lp_handle); % default is solving minimum lp.


for i = 1:length(LO)
    octlpsolve('set_lowbo', lp_handle, i, LO(i));
end

for i = 1:length(UP)
    octlpsolve('set_upbo', lp_handle, i, UP(i));
end

% everythig shoud be equality
lp_ineq_val=zeros(2,1);

for i = 1:length(lp_ineq_val)
  if lp_ineq_val(i) < 0
        con_type = 1;
  elseif lp_ineq_val(i) == 0
        con_type = 3;
  else
        con_type = 2;
  end
  octlpsolve('set_constr_type', lp_handle, i, con_type);
end


for i1=1:30
    for i2=1:30
       count=count+1; 
       b=[-UU1(i1);-UU1(i2)];
       
  		octlpsolve('set_rh_vec', lp_handle, [b;1]);
		octlpsolve('write_mps',lp_handle,["groups_2_",int2str(i1),"_",int2str(i2),".mps"]);
        %[vobj,X,lambda23,status,colstat,it]=lpcplex(-W,[IN;EQ],[b;1],LO,UP,[],[],15000);   
        

%        k=find(X>0.0001);
%        Ind_total(1:length(k),count)=k;
%        Probs_total(1:length(k),count)=X(k);
%              
%        w_total(i1,i2)=-vobj;
%        
%        if UU1(i1)+UU1(i2)<ep
%        
%       % w_total(i1,i2)=-100000000
%        end
%        if status>1
%        w_total(i1,i2)=-500;
%        end
        
%        p_g(i1,i2)=i_g*X;
%        p_r(i1,i2)=i_r*X;
%        [i1 i2]
%        
%        if p_g(i1,i2)>0.5
%            a_r(i1,i2)=0;a_r2(i1,i2)=0;
%        end
%        
%        if (abs(w_total(i1,i2)-w_u_g(i1,i2))+abs(w_total(i1,i2)-w_u_r(i1,i2)))<0.1
%            a_r(i1,i2)=1;
%        end    
%                if (abs(w_total(i1,i2)-w_u_g(i1,i2))+abs(w_total(i1,i2)-w_u_r(i1,i2)))<1
%            a_r2(i1,i2)=1;
%        end    
    end
end

%j_r=find(w_total<-3); w_total(j_r)=-10000000;
%w_u_g(j_L)=-10000000;
%w_u_r1(j_L)=-10000000;

%save results_total7_3 p_g p_r w_total Ind_total Probs_total a_r a_r2




