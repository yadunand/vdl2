clear all
% grid of variables

%replace with the computed solutions
%load 0results_interin_4

common_name=getenv("OCTAVE_INPUT_FILE_NAME");

opt_name=[common_name,"_opt"];
opt_file=[opt_name,".csv"];
load('-ascii',opt_file);
S_interin=-stage_1_opt;

%load solutions_index.csv;
index_name=[common_name,"_index"];
index_file=[index_name,".csv"];
load('-ascii',index_file);
index_i_r=[stage_1_index zeros(3,681)];
%index_i_r=[solutions_index zeros(3,681)] ; %684=1035-351

%load solutions_prob.csv;
prob_name=[common_name,"_prob"];
prob_file=[prob_name,".csv"];
load('-ascii',prob_file); 
%prob_i_r=[solutions_prob zeros(3,684)];
prob_i_r=[stage_1_prob zeros(3,684)];


%c1=[0:40];
%c2=[0:40];
load inputs/grid;
%v1=linspace(-2.44,4.9103*2,45);  
v1=grid;%linspace(0,(5)^0.5,45); v1=v1.^2;%v1=v1-2.35; 
%v2=linspace(-2.44,4.9103*2,45); 
v2=v1;
q1=[2 20];
q2=[2 20];
a1=[2 6];
a2=[2 6];
a1i=[6,2];
a2i=[6,2];

sg=length(v1);
%load 0results_i_r_simpler

load inputs/tech

%P=[0.6979 0.0021 0.0021 0.2979;0.2991 0.4009 0.0009 0.2991;0.2991 0.0009 0.4009 0.2991;...
%        0.2979 0.0021 0.0021 0.6979];



v1v1=kron(ones(1,sg*16),v1);
v1v1_ind=kron(ones(1,sg*16),[1:sg]);
v2v2=kron(ones(1,16),kron(v2,ones(1,sg)));
v2v2_ind=kron(ones(1,16),kron([1:sg],ones(1,sg)));
q1q1=kron(ones(1,8),kron(q1,ones(1,sg*sg)));
q2q2=kron(ones(1,4),kron(q2,ones(1,sg*sg*2)));
a1a1=kron(ones(1,2),kron(a1,ones(1,sg*sg*4)));
a2a2=kron(a2,ones(1,sg*sg*8));
a2a2i=kron(a2i,ones(1,sg*sg*8));
a1a1i=kron(ones(1,2),kron(a1i,ones(1,sg*sg*4)));
ss=kron(ones(1,16),S_interin(:)');

%for su=1:length(v1v1)
    %susu(su)=Sg_r(v1v1_ind(su),v2v2_ind(su));
    %end


%incentive constraints

U1=v1v1-(a1a1.^0.5);          %utilities for the recommended action
U2=v2v2-(a2a2.^0.5);

u1=v1v1-(a1a1i.^0.5);         %utilities from alternative action
u2=v2v2-(a2a2i.^0.5);


  %incentive constraint of agent 1
  
  
A21=kron([1 0 1 0;0 1 0 1],ones(1,sg*sg*4)).*[U1;U1];          
pp1=[P(2,:)./P(1,:) P(1,:)./P(2,:) P(4,:)./P(3,:) P(3,:)./P(4,:)];
P1=kron([pp1;pp1],ones(1,sg*sg));
A22=kron([1 0 1 0;0 1 0 1],ones(1,sg*sg*4)).*(P1.*[u1;u1]);
A2=A22-A21;

   %incentive constraint of agent 2
   
   
   A31=kron([1 1 0 0;0 0 1 1],ones(1,sg*sg*4)).*[U2;U2];
   pp2=[P(3,:)./P(1,:) P(4,:)./P(2,:) P(1,:)./P(3,:) P(2,:)./P(4,:)];
   P2=kron([pp2;pp2],ones(1,sg*sg));
   A32=kron([1 1 0 0;0 0 1 1],ones(1,sg*sg*4)).*(P2.*[u2;u2]);
   A3=A32-A31;
   
   %clearing unuseful matrixes
   %clear A21 A22 pp1 pp2 P1 P2 A31 A32
   
   
  %mother nature constraint
  id=eye(16);
  id2=kron(eye(4),[1;1;1;1]);
 pr1=P';
  
  %pr=kron(pr1(:)',ones(1,26*26));
  pr=pr1(:)';
  M1=sparse(zeros(size(v1v1)));
  M2=sparse(zeros(size(v1v1)));
  
  for i=1:16
      M1(i,:)=sparse(kron(id(i,:),ones(1,sg*sg)));
      M2(i,:)=sparse(kron(id2(i,:),ones(1,sg*sg*4))*pr(i));
  end
  
M=M1-M2;

    % probability constraint
    
    pc=ones(size(v1v1));
    

 
%=========== start main program ============
        

%util=linspace(-2.44,4.9103*2,45); 
%util=linspace(0,(5)^0.5,45); util=util.^2;%util=util-2.35; 
%load grid %done above
load inputs/param
load inputs/iteration
UU=grid_regimes;%linspace(-3.42,5.5,40);
%L=linspace(0,1,101);
%W=[linspace(-80,21.1111,27) linspace(25,49,20)  linspace(49,53.13,5)];

d=0;
obj=q1q1+q2q2+ss;
%UTIL1r=zeros(101,52);
%UTIL2r=zeros(101,52);
Ind_r_d=zeros(10,101*52); %this might need changing (the 10)
Pr_r_d=zeros(10,101*52);

UU1=(v1v1-a1a1.^0.5);
UU2=(v2v2-a2a2.^0.5);
tt=0;
w_new=zeros(40);


A=[A2;A3;-UU1;-UU2;M;pc];



%%%%%%%%%%%%% start setting up the lp_problem %%%%%%%%%%%%%%%%%%%%%

[lp_solve_m,lp_solve_n] = size(A);
lp_handle = octlpsolve('make_lp', lp_solve_m, lp_solve_n);
octlpsolve('set_verbose', lp_handle, 3);
octlpsolve('set_mat', lp_handle, A);

octlpsolve('set_obj_fn', lp_handle, -obj);
%octlpsolve('set_maxim', lp_handle); % default is solving minimum lp.

LO=zeros(size(v1v1));
UP=ones(size(v1v1));

for i = 1:size(v1v1)
    octlpsolve('set_lowbo', lp_handle, i, LO(i));
end

for i = 1:size(v1v1)
    octlpsolve('set_upbo', lp_handle, i, UP(i));
end

%%%%%%%%% end setting up the common part of the LP problem %%%%%%%%%%%%%%%%%%



batch_size=str2num(getenv("OCTAVE_BATCH_SIZE"));
batch_no=str2num(getenv("OCTAVE_BATCH_POS"));

elements_processed=0;

for d1=1:40

        if(d1 < (batch_no / batch_size)+1)
                continue;
        endif
        if(d1 > (batch_no / batch_size)+1)
                break;
        endif

    %fob=L(d1)*(v1v1-a1a1.^0.5)+(1-L(d1))*(v2v2-a2a2.^0.5);

    for d2=1:40    
	    d=d+1;    

		b=[zeros(size(A2,1),1);zeros(size(A3,1),1);-UU(d1);-UU(d2);zeros(size(M,1),1);1];
		%le=size([A2;A3;-UU1;-UU2],1);
		le=size([A2;A3],1);

		eq=size(A,1)-le;
		%lp_ineq_val=[-1*ones(eq,1);zeros(eq,1)];
		lp_ineq_val=[-1*ones(le,1);zeros(eq,1)];

for i = 1:length(lp_ineq_val)
  if lp_ineq_val(i) < 0
        con_type = 1;
  elseif lp_ineq_val(i) == 0
        con_type = 3;
  else
        con_type = 2;
  end
  octlpsolve('set_constr_type', lp_handle, i, con_type);
end

		octlpsolve('set_rh_vec', lp_handle, b);		
		octlpsolve('write_mps',lp_handle,["relPerf_",int2str(d1),"_",int2str(d2),".mps"]);

		%[vobj,x,mu,status,colstat,it]=lpcplex(-obj,A,b,zeros(size(v1v1)),ones(size(v1v1)),[1:le],[],15000);


%cputime-tt
%Supr(d)=vobj;
%UTIL1r(d1,d2)=(v1v1-a1a1.^0.5)*x;
%UTIL2r(d1,d2)=(v2v2-a2a2.^0.5)*x;
%STr(d1,d2)=status;
%k=find(x>0.00001);
%if status==1;
%    w_new(d1,d2)=-vobj;
%	Ind_r_d(1:length(k),d)=k;
%	Pr_r_d(1:length(k),d)=x(k);
%end

%if w_new(d1,d2)<-40;
%    w_new(d1,d2)=-500
%end



%if status>1
%     w_new(d1,d2)=-500;
% end

%[util(d1) util(d2)]
%load iteration;
%[d1 d2 iteration]
%status

end
end
%save supr_exp6 STr w_new Ind_r_d Pr_r_d
%
