clear all

%replace this with loading from our own files, resulted from the workflow, stage 2
%remember to add the path to the inputs/ directory
%load inputs/a0groups_results_4

common_name=getenv("OCTAVE_INPUT_FILE_NAME");

opt_name=[common_name,"_opt"];
opt_file=[opt_name,".csv"];
load('-ascii',opt_file);
% resulting variable: stage_2_opt;

%load solutions_index.csv;
index_name=[common_name,"_index"];
index_file=[index_name,".csv"];
load('-ascii',index_file);
%index_i_r=stage_2_index 

%load solutions_prob.csv;
prob_name=[common_name,"_prob"];
prob_file=[prob_name,".csv"];
load('-ascii',prob_file); 

%load('-ascii','stage_2_opt.csv');
%load('-ascii','stage_2_prob.csv');
%load('-ascii','stage_2_index.csv');

load groups1_save
UTIL1=zeros(101,52);
UTIL2=zeros(101,52);
ST=ones(101,52);


for d1=1:101
	for d2=1:52
%		%Storing results
		%reconstruct X: it is the probabilities
		indexSparseX=(d1-1)*52+d2;
		sparseXValue=stage_2_prob(:,indexSparseX);
		sparseXIndex=stage_2_index(:,indexSparseX);
		
		sparseCols=[1];
		zeroElements=0;
		lastNonZeroIndex=length(sparseXIndex);
		while (sparseXIndex(lastNonZeroIndex)==0)
			lastNonZeroIndex=lastNonZeroIndex-1;
			if (lastNonZeroIndex==1)
				sparseXIndex=0;
				sparseXValue=1;
				break;
			end
			zeroElements=zeroElements+1;
		endwhile
		
		sparseRows=sparseXIndex(1:end-zeroElements);
		sparseValue=sparseXValue(1:end-zeroElements);
		
		sparseX=sparse(sparseRows+1,sparseCols,sparseValue);
		fullX=full(sparseX);
		fillingSize=length(v1v1) - length(fullX);
		completeX=[fullX;zeros(fillingSize,1)];
		
		%size(v1v1)= 1   10816 (row x cols)
		UTIL1(d1,d2)=[(v1v1-a1a1.^0.5)]*completeX;
		UTIL2(d1,d2)=[(v2v2-a2a2.^0.5)]*completeX;
		% FIXME !!!!!!!!! FIXME
		if (stage_2_opt(d1,d2) == 999999)
			ST(d1,d2)=3;
			UTIL1(d1,d2)=-20;
			UTIL2(d1,d2)=-20;
		endif
	

% NOT USED
%		k=find(X>0.00001);
%		if status==1;
%			Ind_g(1:length(k),d)=k;
%			Pr_g(1:length(k),d)=X(k);
%		end

%IF THERE IS NO SOLUTION - FIXME
%		if status>1;
%			UTIL1(d1,d2)=-20;
%			UTIL2(d1,d2)=-20; 
%		end
	end
end

% same as in groups 1, as indicated by Gabriel
W=[linspace(-80,21.1111,15) linspace(22,64.2,10) linspace(65,90,27)];


load inputs/param
load inputs/iteration

u1g=UTIL1(:,1:52);
u2g=UTIL2(:,1:52);

mg1=kron(linspace(0,1,101)',ones(1,52));mg1=mg1(:);mg1=mg1';mg2=1-mg1 ;
%W=[linspace(-80,21.1111,27) linspace(25,49,20) linspace(49,53.13,5)];
%W=[linspace(-80,21.1111,20) linspace(25,65,20) linspace(67,85,12)];
W1=kron(ones(101,1),W);
W1=W1(:,1:52);
st=ST(:,1:52);

%w=linspace(-64,48,15);

%w=linspace(-64,48,15);
W1=[W1(:)]';
u1=[u1g(:)]';
u2=[u2g(:)]';
st=[st(:)]';
for s=1:length(W1)
    if st(s)==3
        W1(s)=-1000^7;
    end
     %    if u1(s)>15
      %  W1(s)=-1000^7;
   % end
    %    if u2(s)>15
     %   W1(s)=-1000^7;
    %end
end

load inputs/grid

UU=grid_regimes;%linspace(-3.42,5.5,40);

%UU=linspace(-4.94,10,40);

indexes_ugrid_g=zeros(10,1600);
probs_ugrid_g=zeros(10,1600);
s_u_g=zeros(40);
w_u_g=zeros(40);




IN=[-u1;-u2];
EQ=[ones(size(u1))];
beq=1;
LO=zeros(size(u1));
UP=ones(size(u1));


%%%%%%%%%%%%% start setting up the lp_problem %%%%%%%%%%%%%%%%%%%%%

[lp_solve_m,lp_solve_n] = size([IN;EQ]);
lp_handle = octlpsolve('make_lp', lp_solve_m, lp_solve_n);
octlpsolve('set_verbose', lp_handle, 3);
octlpsolve('set_mat', lp_handle, [IN;EQ]);

octlpsolve('set_obj_fn', lp_handle, -W1);
%octlpsolve('set_maxim', lp_handle); % default is solving minimum lp.


for i = 1:length(LO)
    octlpsolve('set_lowbo', lp_handle, i, LO(i));
end

for i = 1:length(UP)
    octlpsolve('set_upbo', lp_handle, i, UP(i));
end

%%%%%%%%% end setting up the common part of the LP problem %%%%%%%%%%%%%%%%%%


batch_size=str2num(getenv("OCTAVE_BATCH_SIZE"));
batch_no=str2num(getenv("OCTAVE_BATCH_POS"));

elements_processed=-1;

count=0;
RP=zeros(40);
G=ones(40);
for loop1=1:40

        batch_no/batch_size
        if(loop1 < (batch_no / batch_size)+1)
                continue;
        endif
        if(loop1 > (batch_no / batch_size)+1)
                break;
        endif

    for loop2=1:40
    
    		% make batches
			%elements_processed=elements_processed+1;
            %if((elements_processed < batch_no) || (elements_processed > (batch_no+batch_size-1)))      
            %        continue;
            %endif
    
        count=count+1;
        b=[-UU(loop1);-UU(loop2)];
        %[vobj,X,lambda23,status,colstat,it]=lpcplex(-W1,[IN;EQ],[b;beq],LO,UP,[1:length(b)],[],150000);  
        %[vobj,X,lambda23,status,colstat,it]=lpcplex(-W1,[IN;EQ],[b;beq],LO,UP,[1:(length(b)+1)],[],150000); 

%TODO: Optimize here, we probably know the size of b beforehand

%make all constraints to be equalities, replace the 3 with length(b) - defined later
lp_ineq_val=[-1*ones(size(b));zeros(size(beq))];

for i = 1:length(lp_ineq_val)
  if lp_ineq_val(i) < 0
        con_type = 1;
  elseif lp_ineq_val(i) == 0
        con_type = 3;
  else
        con_type = 2;
  end
  octlpsolve('set_constr_type', lp_handle, i, con_type);
end

		octlpsolve('set_rh_vec', lp_handle, [b;beq]);
		octlpsolve('write_mps',lp_handle,["groups2_",int2str(loop1),"_",int2str(loop2),".mps"]);
		

% this one goes into the merge function

%        s_u_g(loop1,loop2)=status;
%        mu1(loop1,loop2)=mg2*X;
        
        
%        %if status==1;
%        ij=find(X>0.001);
%        indexes_ugrid_g(1:length(ij),count)=ij;
%        probs_ugrid_g(1:length(ij),count)=X(ij);
%        w_u_g(loop1,loop2)=-vobj;
%        %else w_u_g(loop1,loop2)=-100;
%        %end
%    if status>1
%        w_u_g(loop1,loop2)=-500;
%    end
           
%         load iteration;
%        [loop1 loop2 iteration]
    end
end

% save results_ugrid_g5 indexes_ugrid_g probs_ugrid_g w_u_g s_u_g   