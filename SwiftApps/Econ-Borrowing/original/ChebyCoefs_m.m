function coefs=ChebyCoefs_m(z1,y,degree,a1,b1,a2,b2,a3,b3)
%degree=n;
%Evaluates T matrix 

T1=zeros(length(z1),degree+1);

for i=1:degree+1
    T1(:,i)=cos((i-1)*acos(z1));
end

%%% Compute chebyshev coefficiens
m=length(z1);
coefs=zeros(degree+1,1);
ind=0;
for i=1:degree+1
    for j=1:degree+1
        for k=1:degree+1
            Numer1=kron(T1(:,i),ones(m^2,1));
            Numer2=kron(T1(:,j),ones(m,1));
            Numer2=kron(ones(m,1),Numer2);
            Numer3=kron(ones(m^2,1),T1(:,k));
            tot=Numer1.*Numer2.*Numer3;
            Numer=tot'*y;
            
            
    Denom=(T1(:,i)'*T1(:,i)).*(T1(:,j)'*T1(:,j)).*(T1(:,k)'*T1(:,k));
    ind=ind+1;
    coefs(ind)=Numer/Denom;
end
    end
end

