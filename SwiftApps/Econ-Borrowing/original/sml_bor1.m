%%%% linear program for conning
function f=mle(betas,inf_bor,for_bor,both_bor,no_bor,Kash,Assets,edu,age,g,g1,tau,simu,e1,e2,n,m)
load 'borrowing_data1'
load simulation
global zzq L1 zzq1 br simu tau
npop=length(edu);
K=Kash(1);

%%%%building talent
lambda=betas(1);
Talent=lambda*edu;

%%%building the monitoring cost
beta=betas(2);
cost_bank=age*beta;

%%%%fixed cost for money-lender
cm=betas(3);

%%%desutility parameter 
kapha=betas(4);

alpha=.5;
delta=.5;

%%%%optimization
mmax=2000;
Tol=1.0e-7;
options=optimset('TolX',Tol,'TolFun',Tol,'MaxIter',mmax,'MaxFunEvals',mmax,'TolCon',Tol,'display','off');


%%%%%%%%mins and maxs %%%%%%
t_hat=kron(Talent,ones(1,simu))+e1;
cost_b=kron(cost_bank,ones(1,simu))+e2;

a1=min(min(t_hat));
b1=max(max(t_hat));

a2=min(min(cost_b));
b2=max(max(cost_b));

a3=min(Assets);
b3=max(Assets);

z1=-cos((2*[1:m]'-1)*pi/(2*m));

%%%% Adjust the nodes to the [a,b] interval
x1=(z1+1)*(b1-a1)/2+a1;
x2=(z1+1)*(b2-a2)/2+a2;
x3=(z1+1)*(b3-a3)/2+a3;
u1=0;
%%% Evaluate f at the approximation nodes
for j=1:m
    for k=1:m
        for p=1:m
z=x1(j);
cb=x2(k);
A=x3(p);

ini=zeros(3,1)+.1;

%%%% banks and money lenders %%%%
LB=[-100000 -100000 -10000];
UB=[100 100 100];
[X1,vobj1,exitflag1]=fmincon(@utl_1,ini,[],[],[],[],LB,UB,@ic_1,options...
    ,z, kapha, K, alpha, g, A, delta, g1, cm, cb);

%%%% only banks %%%%
LB=[-100000 ];
UB=[1000 ];
[X2,vobj2,exitflag2]=fmincon(@utl_b,ini,[],[],[],[],LB,UB,@ic_b,options...
    ,z, kapha, K, alpha, g, A, delta, g1, cm, cb);

%%% only money lenders %%%%
LB=[-100000 -10000];
UB=[100 100] ;
[X3,vobj3,exitflag3]=fmincon(@utl_ml,ini,[],[],[],[],LB,UB,@ic_ml,options...
    ,z, kapha, K, alpha, g, A, delta, g1, cm, cb);

d=K;
pl=d^alpha/(1+d^alpha);

if exitflag1<=0 
    vobj1=-pl*z;
end
if exitflag2<=0 
    vobj2=-pl*z;
end
if exitflag3<=0 
    vobj3=-pl*z;
end

vobj=max(-vobj2,-vobj3);
vobj=max(-vobj1,vobj);
u1=u1+1;
y4(u1)=vobj;
y1(u1)=-vobj1;
y2(u1)=-vobj2;
y3(u1)=-vobj3;
        end
    end
end
PL=Kash.^alpha;
PL=PL./(1+PL);

% compute the chebyshev coefficients
coefs1=ChebyCoefs_m(z1,y1',n,a1,b1,a2,b2,a3,b3);
coefs2=ChebyCoefs_m(z1,y2',n,a1,b1,a2,b2,a3,b3);
coefs3=ChebyCoefs_m(z1,y3',n,a1,b1,a2,b2,a3,b3);


% Compute approximation function values of test grid

for j=1:simu
approxyy1(:,j)=ChebyEvaluate_m(t_hat(:,j),cost_b(:,j),Assets,coefs1,n,m,a1,b1,a2,b2,a3,b3);
approxyy2(:,j)=ChebyEvaluate_m(t_hat(:,j),cost_b(:,j),Assets,coefs2,n,m,a1,b1,a2,b2,a3,b3);
approxyy3(:,j)=ChebyEvaluate_m(t_hat(:,j),cost_b(:,j),Assets,coefs3,n,m,a1,b1,a2,b2,a3,b3);
no_b(:,j)=PL.*t_hat(:,j);
end


both=approxyy1;
form=approxyy2;
inf=approxyy3;

   
    p1=exp(no_b/tau);
    p2=exp(both/tau);
    p3=exp(form/tau);
    p4=exp(inf/tau);
    tot=p1+p2+p3+p4;
    
    part1=p1./tot;
    part2=p2./tot;
    part3=p3./tot;
    part4=p4./tot;
    
    part1=mean(part1')';
    part2=mean(part2')';
    part3=mean(part3')';
    part4=mean(part4')';

ind1=find(part1<1.0000e-300);
part1(ind1)=1.0000e-300;

ind1=find(part2<1.0000e-300);
part2(ind1)=1.0000e-300;

ind1=find(part3<1.0000e-300);
part3(ind1)=1.0000e-300;

ind1=find(part4<1.0000e-300);
part4(ind1)=1.0000e-300;   

f4=log(part1).*no_bor+log(part2).*both_bor+log(part3).*for_bor+log(part4).*inf_bor;
f=-mean(f4);
%store current best
 if f<L1
 L1=f;
 zzq1=betas;
 end
 
 br=br+1;        %iteration counter
 
 %Display on-going information on screen
 disp(['This iteration, no. ',num2str(br)])
 disp(f)
 disp('Current min is:')
 disp(L1);
 disp(['Current Params'          'Optimal Params'       'True Params'])
 disp([betas'                    zzq1'                 t_betas'])
 
 save par_est1 br L1 zzq1         %save current best

