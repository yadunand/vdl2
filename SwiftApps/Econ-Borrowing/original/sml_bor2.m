%%%% linear program for conning
function f=mle(betas,inf_bor,for_bor,both_bor,no_bor,g,g1,Kash,Assets,edu,age,tau,simu,e1,e2)
load 'borrowing_data1'
load simulation
global zzq L1 zzq1 br simu tau

npop=length(edu);


lambda=betas(1);
Talent=lambda*edu;

beta=betas(2);
cost_bank=age*beta;

omega=betas(3);
cost_m=omega;

kapha=betas(4);


alpha=.5;
delta=.5;
%%%%optimization
mmax=2000;
Tol=1.0e-7;
options=optimset('TolX',Tol,'TolFun',Tol,'MaxIter',mmax,'MaxFunEvals',mmax,'TolCon',Tol,'display','off');

%%%using the simulations to build talent and cost of borrowing from the
%%%bank
t_hat=kron(Talent,ones(1,simu))+e1;
cost_b=kron(cost_bank,ones(1,simu))+e2;


%%% Evaluate for each j individual and each k simulation
cm=cost_m;
for j=1:npop
A=Assets(j);
K=Kash(j);

    for k=1:simu
        
z=t_hat(j,k);
cb=cost_b(j,k);


ini=zeros(3,1)+.1;

%%%% banks and money lenders %%%%
LB=[-100000 -100000 -10000];
UB=[100 100 100];
[X1,vobj1,exitflag1]=fmincon(@utl_1,ini,[],[],[],[],LB,UB,@ic_1,options...
    ,z, kapha, K, alpha, g, A, delta, g1, cm, cb);

%%%% only banks %%%%
LB=[-100000 ];
UB=[1000 ];
[X2,vobj2,exitflag2]=fmincon(@utl_b,ini,[],[],[],[],LB,UB,@ic_b,options...
    ,z, kapha, K, alpha, g, A, delta, g1, cm, cb);

%%% only money lenders %%%%
LB=[-100000 -10000];
UB=[100 100] ;
[X3,vobj3,exitflag3]=fmincon(@utl_ml,ini,[],[],[],[],LB,UB,@ic_ml,options...
    ,z, kapha, K, alpha, g, A, delta, g1, cm, cb);

d=K;
pl=d^alpha/(1+d^alpha);

if exitflag1<=0 
    vobj1=-pl*z+10;
end
if exitflag2<=0 
    vobj2=-pl*z+10;
end
if exitflag3<=0 
    vobj3=-pl*z+10;
end


y4(j,k)=pl*z;
y1(j,k)=-vobj1;
y2(j,k)=-vobj2;
y3(j,k)=-vobj3;
    end
end


no_b=y4;
both=y1;
form=y2;
inf=y3;

    p1=exp(no_b/tau);
    p2=exp(both/tau);
    p3=exp(form/tau);
    p4=exp(inf/tau);
    tot=p1+p2+p3+p4;
    
    part1=p1./tot;
    part2=p2./tot;
    part3=p3./tot;
    part4=p4./tot;
    
    part1=mean(part1')';
    part2=mean(part2')';
    part3=mean(part3')';
    part4=mean(part4')';

ind1=find(part1<1.0000e-300);
part1(ind1)=1.0000e-300;

ind1=find(part2<1.0000e-300);
part2(ind1)=1.0000e-300;

ind1=find(part3<1.0000e-300);
part3(ind1)=1.0000e-300;

ind1=find(part4<1.0000e-300);
part4(ind1)=1.0000e-300;   

f4=log(part1).*no_bor+log(part2).*both_bor+log(part3).*for_bor+log(part4).*inf_bor;
f=-mean(f4);
%store current best
 if f<L1
 L1=f;
 zzq1=betas;
 end
 
 br=br+1;        %iteration counter
 
 %Display on-going information on screen
 disp(['This iteration, no. ',num2str(br)])
 disp(f)
 disp('Current min is:')
 disp(L1);
 disp(['Current Params'          'Optimal Params'       'True Params'])
 disp([betas'                    zzq1'                 t_betas'])
 
 save par_est2 br L1 zzq1         %save current best
% 

