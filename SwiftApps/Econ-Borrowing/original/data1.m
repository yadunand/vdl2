%%%% linear program for conning
clear all
%global z kapha K alp g A delta g1 cm cb
warning off all

%%%%laoding the data
load 'borrowing_data1'

%%%%To proceed with the estimation, we need to create randon errors, that
%%%%will be e1 and specify the number of simulations and tau
global zzq L1 zzq1 br 
tau=.1;
simu=5;

%%%%creating the error terms
e1=randn(npop,simu)*sig1;
e2=randn(npop,simu);


%%%%restricting the parmeters
LB=[0.001 0.001 0.001 0.001];
UB=[10 10 10 30 ];


%%%%number of evaluation points, m, for the chebyshev aproximation, and ,n,
%%%%is the degree of the polynomial
zzq1=t_betas;
n=2;
m=n+1;
save simulation simu tau e1 e2 m n
%%%%how long does it take to evaluate the likelihood
betas=t_betas;
tic;
sml_bor1(betas,inf_bor,for_bor,both_bor,no_bor,Kash,Assets,edu,age,g,g1,tau,simu,e1,e2,n,m)
toc;

tic;
sml_bor2(betas,inf_bor,for_bor,both_bor,no_bor,g,g1,Kash,Assets,edu,age,tau,simu,e1,e2)
toc;


br=0;
L1=1e8;
betas=rand(4,1);
betas=10*betas';

%%%%running the SML with any starting points
options=psoptimset('MaxIter',2000,'MaxFunEvals',2000,'display','iter','TolFun',10e-3 );
[coef1,f1,exit1]=patternsearch(@sml_bor1,betas,[],[],[],[],LB,UB,options);


%%%%running the SML with the true parameters
br=0;
L1=1e8;
[coef2,f2,exit2]=patternsearch(@sml_bor1,t_betas,[],[],[],[],LB,UB,options);

%%%%finding parameters with alternative approach
[coef3,f3,exit3]=patternsearch(@sml_bor2,betas,[],[],[],[],LB,UB,options);