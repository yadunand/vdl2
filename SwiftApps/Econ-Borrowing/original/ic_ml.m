function [c,ceq]=ic(x,z, kapha, K, alp, g, A, delta, g1, cm, cb)

%%% this program verifies that the equaltiy and inequality constraints are
%%% met for the case of borrowing from moenylenders

l=x(1);
k=x(2);

Im=exp(l);
cost=exp(k);

%%%%outside option%%%%

d=K;
pl=d^alp/(1+d^alp);

%%%%the probability of sucess is%%%

temp1=(Im+K)^alp;
ph=temp1/(1+temp1);

%%%%%the payments for the moneylender are, in the case of failure
Wf=0; 
%%% and in the case of sucess, using the profit constraint
Ws=(cost+Im*g1+cm)/ph-Wf*(1-ph)/ph;

%%%for a given cost, the B(c) function is%%%%
bc=1/(1+cost^delta);

%%%%the payments for the entreprenuer are
Sf=-Wf;    
Ss=z-Ws;

%%%%incentive constraint, for the individual
c1=(ph-pl)*Ss+(pl-ph)*Sf-(Im)*kapha*bc;

c=[-c1];

%%% the constraints with equality are imposed in the caculations
%%% so they are ignored in the program

ceq=[];


