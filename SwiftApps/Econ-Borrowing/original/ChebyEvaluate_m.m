function fval=ChebyEvaluate(x1,x2,x3,coefs,n,m,a1,b1,a2,b2,a3,b3)
% x1=Talent;
% x2=Kash;
% x3=Assets;
% coefs=coefs1;
% The degree of chebyshev polynomial
d=n;
m=n+1;
% part1=0;
% part2=0;
% part3=0;

%%%Initialization
fval=zeros(size(x1));
T1=0;T2=0;T3=0;
%Normalizes x vector
z1=2*(x1-a1)/(b1-a1)-1;
z2=2*(x2-a2)/(b2-a2)-1;
z3=2*(x3-a3)/(b3-a3)-1;
for i=1:length(x1)
    for j=1:d+1
    T1(j)=cos((j-1)*acos(z1(i)));
    T2(j)=cos((j-1)*acos(z2(i)));
    T3(j)=cos((j-1)*acos(z3(i)));
    end
    part1=kron(T1',ones(m^2,1));
    part2=kron(T2',ones(m,1));
    part2=kron(ones(m,1),part2);
    part3=kron(ones(m^2,1),T3');
    T=part1.*part2.*part3;
    fval(i)=sum(T'*coefs);
end

