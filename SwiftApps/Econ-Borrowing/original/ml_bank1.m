%%%% Generating data for the borrowing program
clear all
warning off all

%%%%number of people in the sample 
npop=100;
%%%generating education variable
edu=round(rand(npop,1)*7)+1;
lambda=7;
sig1=2;
eps=randn(npop,1)*sig1;
%%%generating talent
Talent=lambda*edu+eps;

%%%%Generating cahs and assets
Kash=rand(npop,1)*.7;
Assets=rand(npop,1)*100;

%%%%interest rate   
g=1.1;
g1=1.1;

%%%%the cost of using borrowing from banks is
age=round(rand(npop,1)*3)+2;
beta=.5;
eps2=randn(npop,1);
cost_b=beta*age+eps2;

%%%%the cost of using borrowing from moneylenders is
omega=.2;
cost_m=omega;

%%%%ppb. of success
alpha=.5;
%%%%utility from diversion
kapha=10.5;

%%%%only moneylenders, funcion B(c)
delta=.5;

%%%The parameters we want to recover are%%%%
t_betas=[lambda beta omega kapha];
%%%%optimization, solve for the every contract and find the best option for
%%%%each individual
mmax=2000;
Tol=1.0e-7;
options=optimset('TolX',Tol,'TolFun',Tol,'MaxIter',mmax,'MaxFunEvals',mmax,'TolCon',Tol,'display','off');

for j=1:npop   
    
K=Kash(j);
A=Assets(j);    
z=Talent(j);    
alp=alpha;
cb=cost_b(j);
cm=cost_m;
ini=zeros(3,1)-.1;

%%%% banks and money lenders %%%%
LB=[-100000 -100000 -10000];
UB=[100 100 100];
[X1,vobj1,exitflag1]=fmincon(@utl_1,ini,[],[],[],[],LB,UB,@ic_1,options...
    ,z, kapha, K, alpha, g, A, delta, g1, cm, cb);

%%%% only banks %%%%
LB=[-100000 ];
UB=[1000 ];
[X2,vobj2,exitflag2]=fmincon(@utl_b,ini,[],[],[],[],LB,UB,@ic_b,options...
    ,z, kapha, K, alpha, g, A, delta, g1, cm, cb);

%%% only money lenders %%%%
LB=[-100000 -10000];
UB=[100 100] ;
[X3,vobj3,exitflag3]=fmincon(@utl_ml,ini,[],[],[],[],LB,UB,@ic_ml,options...
    ,z, kapha, K, alpha, g, A, delta, g1, cm, cb);

d=K;

%%% self-finance option
pl=d^alpha/(1+d^alpha);

%%%% if the programs fail to find a solution, 
if exitflag1<=0 
    vobj1=-pl*z+10;
end
if exitflag2<=0 
    vobj2=-pl*z+10;
end
if exitflag3<=0 
    vobj3=-pl*z+10;
end

max1=max(-vobj2,-vobj3);
max1=max(max1,pl*z);

max2=max(-vobj1,-vobj3);
max2=max(max2,pl*z);

max3=max(-vobj1,-vobj2);
max3=max(max3,pl*z);

max4=max(-vobj1,-vobj2);
max4=max(max4,-vobj1);

%%%%what gives the highest expected utility level
if -vobj1>=max1
   both_bor(j,1)=1;
   for_bor(j,1)=0;
   inf_bor(j,1)=0;
   no_bor(j,1)=0;
end
if -vobj2>max2
   both_bor(j,1)=0;
   for_bor(j,1)=1;
   inf_bor(j,1)=0;
   no_bor(j,1)=0;
end
if -vobj3>max3
   both_bor(j,1)=0;
   for_bor(j,1)=0;
   inf_bor(j,1)=1;
   no_bor(j,1)=0;
end
if pl*z>=max4
   both_bor(j,1)=0;
   for_bor(j,1)=0;
   inf_bor(j,1)=0;
   no_bor(j,1)=1;
end
end

%%%%proportion of entreprnuers borrowing from each source%%%%
disp(['Not borrowing'])
disp([mean(no_bor)])
disp(['Only Formal'])
disp([mean(for_bor)])
disp(['Only Informal'])
disp([mean(inf_bor)])
disp(['Both'])
disp([mean(both_bor)])
 


save ('borrowing_data1','inf_bor','for_bor','both_bor','no_bor','Kash','Assets','t_betas',...
    'npop','g','g1','edu','age' ,'sig1')

