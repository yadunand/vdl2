function f=utl(x,z, kapha, K, alp, g, A, delta, g1, cm, cb)

%%% this program must find the loan size made by the bank, Ib,
%%% the loan size made by the moneylender, Im, and the monitoring cost,
%%% cost

h=x(1);
l=x(2);
k=x(3);

Ib=exp(h);
Im=exp(l);
cost=exp(k);

%%%%outside option%%%%
d=K;
pl=d^alp/(1+d^alp);

%%%%the probability of sucess is%%%
temp1=(Ib+Im+K)^alp;
ph=temp1/(1+temp1);

%%%%The payment for the money lender are, in the case of failure
Wf=0;
%%%%and using the profit constraint, in the case of sucess
Ws=(cost+cm+Im*g1)/ph-Wf*(1-ph)/ph;

%%%for a given cost, the B(c) function is%%%%
cost1=cost;
bc=1/(1+cost1^delta);

%%%the payment for the entreprenuer in case of sucess is
Sf=-A;
%%% and in the case of failure, using the zero profits constraint for the bank 
Ss=z-Ws-(Sf+Wf)*(1-ph)/ph-(Ib*g+cb)/ph;


%%%the expected utility level is%%%%
f=ph*Ss+(1-ph)*Sf;

f=-f;

