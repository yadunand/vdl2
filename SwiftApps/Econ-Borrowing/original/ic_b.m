function [c,ceq]=ic(x,z, kapha, K, alp, g, A, delta, g1, cm, cb)
%%% this program verifies that the equaltiy and inequality constraints are
%%% met for the case of borrowing from banks 

h=x(1);
Ib=exp(h);

%%%%outside option%%%%
d=K;
pl=d^alp/(1+d^alp);

%%%%the probability of sucess is%%%
temp1=(Ib+K)^alp;
ph=temp1/(1+temp1);

%%%for a given cost, the B(c) function is%%%%
cost1=0;
bc=1/(1+cost1^delta);

%%%the payment for the entreprenuer in case of sucess is
Sf=-A;
%%% and in the case of failure, using the zero profits constraint for the bank 
Ss=z-(Sf)*(1-ph)/ph-(Ib*g+cb)/ph;


%%%%incentive constraint for the individual
c1=(ph-pl)*Ss+(pl-ph)*Sf-(Ib)*kapha*bc;

%%%%participation constraint
c3=Sf+A;

c=[-c1 -c3];

%%% the constraints with equality are imposed in the caculations
%%% so they are ignored in the program
ceq=[];



