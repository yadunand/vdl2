function f=utl(x,z, kapha, K, alp, g, A, delta, g1, cm, cb)

%%% this program must find the loan size made by the bank

h=x(1);
Ib=exp(h);

%%%%outside option%%%%
d=K;
pl=d^alp/(1+d^alp);

%%%%the probability of sucess is%%%

temp1=(Ib+K)^alp;
ph=temp1/(1+temp1);

%%%for a given cost, the B(c) function is%%%%
cost1=0;
bc=1/(1+cost1^delta);

%%%the payment for the entreprenuer in case of sucess is
Sf=-A;
%%% and in the case of failure, using the zero profits constraint for the bank 
Ss=z-(Sf)*(1-ph)/ph-(Ib*g+cb)/ph;

%%%the expected utility level is%%%%
f=ph*Ss+(1-ph)*Sf;

f=-f;
