function [c,ceq]=ic(x,z, kapha, K, alp, g, A, delta, g1, cm, cb)

%%% this program verifies that the equaltiy and inequality constraints are
%%% met for the case of borrowing from banks and moenylenders

h=x(1);
l=x(2);
k=x(3);

Ib=exp(h);
Im=exp(l);
cost=exp(k);

%%%%outside option%%%%

d=K;
pl=d^alp/(1+d^alp);

%%%%the probability of sucess is%%%
temp1=(Ib+Im+K)^alp;
ph=temp1/(1+temp1);


%%%%The payment for the money lender are, in the case of failure
Wf=0;
%%%%and using the profit constraint, in the case of sucess
Ws=(cost+cm+Im*g1)/ph-Wf*(1-ph)/ph;

%%%for a given cost, the B(c) function is%%%%
cost1=cost;
bc=1/(1+cost1^delta);

%%%the payment for the entreprenuer in case of sucess is
Sf=-A;
%%% and in the case of failure, using the zero profits constraint for the bank 
Ss=z-Ws-(Sf+Wf)*(1-ph)/ph-(Ib*g+cb)/ph;


%%%%incentive constraint 
%%%% for the entreprenuer 
c1=(ph-pl)*Ss+(pl-ph)*Sf-(Ib+Im)*kapha*bc;

%%% for the money lender
c2=(ph-pl)*Ws+(pl-ph)*Wf-cost-cm;
c=[-c1 -c2];

%%% the constraints with equality are imposed in the caculations
%%% so they are ignored in the program

ceq=[];



