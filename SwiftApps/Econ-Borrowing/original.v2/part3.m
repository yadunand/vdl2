function part3 
load 'part2'
no_b=y4;
both=y1;
form=y2;
inf=y3;

    p1=exp(no_b/tau);
    p2=exp(both/tau);
    p3=exp(form/tau);
    p4=exp(inf/tau);
    tot=p1+p2+p3+p4;
    
    part1=p1./tot;
    part2=p2./tot;
    part3=p3./tot;
    part4=p4./tot;
    
    part1=mean(part1')';
    part2=mean(part2')';
    part3=mean(part3')';
    part4=mean(part4')';

ind1=find(part1<1.0000e-300);
part1(ind1)=1.0000e-300;

ind1=find(part2<1.0000e-300);
part2(ind1)=1.0000e-300;

ind1=find(part3<1.0000e-300);
part3(ind1)=1.0000e-300;

ind1=find(part4<1.0000e-300);
part4(ind1)=1.0000e-300;   

f4=log(part1).*no_bor+log(part2).*both_bor+log(part3).*for_bor+log(part4).*inf_bor;
f=-mean(f4);

save result f