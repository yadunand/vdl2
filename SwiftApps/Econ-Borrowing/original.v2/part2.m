function part2
load part1


%%%%% TIBI
for j=1:npop
A=Assets(j);
K=Kash(j);


%%%%% TIBI
    for k=1:simu
%%%%optimization
mmax=2000;
Tol=1.0e-7;
options=optimset('TolX',Tol,'TolFun',Tol,'MaxIter',mmax,'MaxFunEvals',mmax,'TolCon',Tol,'display','off');
        
z=t_hat(j,k);
cb=cost_b(j,k);
cm=cost_m(j,k);

ini=zeros(3,1)+.1;

%%%% banks and money lenders %%%%
LB=[-100000 -100000 -10000];
UB=[100 100 100];
[X1,vobj1,exitflag1]=fmincon(@utl_1,ini,[],[],[],[],LB,UB,@ic_1,options...
    ,z, kapha, K, alpha1, g, A, delta, g1, cm, cb);

%%%% only banks %%%%
LB=[-100000 ];
UB=[1000 ];
[X2,vobj2,exitflag2]=fmincon(@utl_b,ini,[],[],[],[],LB,UB,@ic_b,options...
    ,z, kapha, K, alpha1, g, A, delta, g1, cm, cb);

%%% only money lenders %%%%
LB=[-100000 -10000];
UB=[100 100] ;
[X3,vobj3,exitflag3]=fmincon(@utl_ml,ini,[],[],[],[],LB,UB,@ic_ml,options...
    ,z, kapha, K, alpha1, g, A, delta, g1, cm, cb);

d=K;
pl=d^alpha1/(1+d^alpha1);

if exitflag1<=0 
    vobj1=-pl*z+10;
end
if exitflag2<=0 
    vobj2=-pl*z+10;
end
if exitflag3<=0 
    vobj3=-pl*z+10;
end


y4(j,k)=pl*z;
y1(j,k)=-vobj1;
y2(j,k)=-vobj2;
y3(j,k)=-vobj3;
    end
end

save ('part2','tau','y1','y2','y3','y4','no_bor','both_bor','for_bor','inf_bor')





%%%%%%%%%% FUNCTIONS 


function f=utl_ml(x,z, kapha, K, alp, g, A, delta, g1, cm, cb)

%%% this program must find  the loan size made by the moneylender, Im, 
%%% and the monitoring cost, cost

l=x(1);
k=x(2);

Im=exp(l);
cost=exp(k);

%%%%outside option%%%%

d=K;
pl=d^alp/(1+d^alp);

%%%%the probability of sucess is%%%
temp1=(Im+K)^alp;
ph=temp1/(1+temp1);


%%%%%the payments for the moneylender are, in the case of failure
Wf=0; 
%%% and in the case of sucess, using the profit constraint
Ws=(cost+Im*g1+cm)/ph-Wf*(1-ph)/ph;

%%%for a given cost, the B(c) function is%%%%
bc=1/(1+cost^delta);

%%%%the payments for the entreprenuer are
Sf=-Wf;    
Ss=z-Ws;

%%%the expected utility level is%%%%
f=ph*Ss+(1-ph)*Sf;

f=-f;


function [c,ceq]=ic_ml(x,z, kapha, K, alp, g, A, delta, g1, cm, cb)

%%% this program verifies that the equaltiy and inequality constraints are
%%% met for the case of borrowing from moenylenders

l=x(1);
k=x(2);

Im=exp(l);
cost=exp(k);

%%%%outside option%%%%

d=K;
pl=d^alp/(1+d^alp);

%%%%the probability of sucess is%%%

temp1=(Im+K)^alp;
ph=temp1/(1+temp1);

%%%%%the payments for the moneylender are, in the case of failure
Wf=0; 
%%% and in the case of sucess, using the profit constraint
Ws=(cost+Im*g1+cm)/ph-Wf*(1-ph)/ph;

%%%for a given cost, the B(c) function is%%%%
bc=1/(1+cost^delta);

%%%%the payments for the entreprenuer are
Sf=-Wf;    
Ss=z-Ws;

%%%%incentive constraint, for the individual
c1=(ph-pl)*Ss+(pl-ph)*Sf-(Im)*kapha*bc;

c=[-c1];

%%% the constraints with equality are imposed in the caculations
%%% so they are ignored in the program

ceq=[];


function [c,ceq]=ic_1(x,z, kapha, K, alp, g, A, delta, g1, cm, cb)

%%% this program verifies that the equaltiy and inequality constraints are
%%% met for the case of borrowing from banks and moenylenders

h=x(1);
l=x(2);
k=x(3);

Ib=exp(h);
Im=exp(l);
cost=exp(k);

%%%%outside option%%%%

d=K;
pl=d^alp/(1+d^alp);

%%%%the probability of sucess is%%%
temp1=(Ib+Im+K)^alp;
ph=temp1/(1+temp1);


%%%%The payment for the money lender are, in the case of failure
Wf=0;
%%%%and using the profit constraint, in the case of sucess
Ws=(cost+cm+Im*g1)/ph-Wf*(1-ph)/ph;

%%%for a given cost, the B(c) function is%%%%
cost1=cost;
bc=1/(1+cost1^delta);

%%%the payment for the entreprenuer in case of sucess is
Sf=-A;
%%% and in the case of failure, using the zero profits constraint for the bank 
Ss=z-Ws-(Sf+Wf)*(1-ph)/ph-(Ib*g+cb)/ph;


%%%%incentive constraint 
%%%% for the entreprenuer 
c1=(ph-pl)*Ss+(pl-ph)*Sf-(Ib+Im)*kapha*bc;

%%% for the money lender
c2=(ph-pl)*Ws+(pl-ph)*Wf-cost-cm;
c=[-c1 -c2];

%%% the constraints with equality are imposed in the caculations
%%% so they are ignored in the program

ceq=[];



function f=utl_1(x,z, kapha, K, alp, g, A, delta, g1, cm, cb)

%%% this program must find the loan size made by the bank, Ib,
%%% the loan size made by the moneylender, Im, and the monitoring cost,
%%% cost

h=x(1);
l=x(2);
k=x(3);

Ib=exp(h);
Im=exp(l);
cost=exp(k);

%%%%outside option%%%%
d=K;
pl=d^alp/(1+d^alp);

%%%%the probability of sucess is%%%
temp1=(Ib+Im+K)^alp;
ph=temp1/(1+temp1);

%%%%The payment for the money lender are, in the case of failure
Wf=0;
%%%%and using the profit constraint, in the case of sucess
Ws=(cost+cm+Im*g1)/ph-Wf*(1-ph)/ph;

%%%for a given cost, the B(c) function is%%%%
cost1=cost;
bc=1/(1+cost1^delta);

%%%the payment for the entreprenuer in case of sucess is
Sf=-A;
%%% and in the case of failure, using the zero profits constraint for the bank 
Ss=z-Ws-(Sf+Wf)*(1-ph)/ph-(Ib*g+cb)/ph;


%%%the expected utility level is%%%%
f=ph*Ss+(1-ph)*Sf;

f=-f;

function [c,ceq]=ic_b(x,z, kapha, K, alp, g, A, delta, g1, cm, cb)
%%% this program verifies that the equaltiy and inequality constraints are
%%% met for the case of borrowing from banks 

h=x(1);
Ib=exp(h);

%%%%outside option%%%%
d=K;
pl=d^alp/(1+d^alp);

%%%%the probability of sucess is%%%
temp1=(Ib+K)^alp;
ph=temp1/(1+temp1);

%%%for a given cost, the B(c) function is%%%%
cost1=0;
bc=1/(1+cost1^delta);

%%%the payment for the entreprenuer in case of sucess is
Sf=-A;
%%% and in the case of failure, using the zero profits constraint for the bank 
Ss=z-(Sf)*(1-ph)/ph-(Ib*g+cb)/ph;


%%%%incentive constraint for the individual
c1=(ph-pl)*Ss+(pl-ph)*Sf-(Ib)*kapha*bc;

%%%%participation constraint
c3=Sf+A;

c=[-c1 -c3];

%%% the constraints with equality are imposed in the caculations
%%% so they are ignored in the program
ceq=[];



function f=utl_b(x,z, kapha, K, alp, g, A, delta, g1, cm, cb)

%%% this program must find the loan size made by the bank

h=x(1);
Ib=exp(h);

%%%%outside option%%%%
d=K;
pl=d^alp/(1+d^alp);

%%%%the probability of sucess is%%%

temp1=(Ib+K)^alp;
ph=temp1/(1+temp1);

%%%for a given cost, the B(c) function is%%%%
cost1=0;
bc=1/(1+cost1^delta);

%%%the payment for the entreprenuer in case of sucess is
Sf=-A;
%%% and in the case of failure, using the zero profits constraint for the bank 
Ss=z-(Sf)*(1-ph)/ph-(Ib*g+cb)/ph;

%%%the expected utility level is%%%%
f=ph*Ss+(1-ph)*Sf;

f=-f;

