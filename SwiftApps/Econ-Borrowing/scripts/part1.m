function part1 %=f(betas)

load borrowing_data1
betas(1)=0.5;
betas(2)=0.5;
betas(3)=0.5;
betas(4)=0.5;

simu=100;

lambda=betas(1);
Talent=lambda*edu;

beta=betas(2);
cost_bank=age*beta;

omega=betas(3);

cost_ml=omega*dista;

kapha=betas(4);

delta=.5;
%%%%optimization


%%%%%%%%mins and maxs %%%%%%
t_hat=kron(Talent,ones(1,simu))+e1;
cost_b=kron(cost_bank,ones(1,simu))+e2;
cost_m=kron(cost_ml,ones(1,simu))+e3;

e1=randn(npop,simu)*sig1;
e2=randn(npop,simu);
e3=randn(npop,simu);

save ('part1','inf_bor','for_bor','both_bor','no_bor','Kash','Assets','t_betas','age',...
    'npop','g','g1','tau','e1','e2','e3','edu','simu','sig1','dista','delta','alpha1','npop','lambda','beta','omega','kapha','delta'...
    ,'Talent','cost_bank','cost_ml','t_hat','cost_b','cost_m','npop')

