#!/bin/bash

#!/bin/bash

cat << EOF > list.txt
a
b
c
EOF

cat << EOF > x.sh
#!/bin/bash

echo \$\@
EOF

chmod 755 x.sh

echo "List, no input parameters and files"
./swiftforeach.py list swiftK localhost "" bash x.sh "" list.txt tmpoutlist a.log list1.out ""

echo "Range, no input parameters and files"
./swiftforeach.py range swiftK localhost "" bash x.sh "" 0 9 1 tmpoutrange b.log list2.out ""

echo "List, string input parameters"
./swiftforeach.py range swiftK localhost "" bash x.sh "" 0 9 1 tmpoutrange b.log list2.out "hello there list!"

echo "Range, string input parameters"
./swiftforeach.py range swiftK localhost "" bash x.sh "" 0 9 1 tmpoutrange b.log list2.out "hi there range!"

echo "List, no input parameters and files stdin /dev/null"
./swiftforeach.py list swiftK localhost "" bash x.sh "/dev/null" list.txt tmpoutlist a.log list1.out ""

echo "T list"
./swiftforeach.py list swiftT localhost "" bash x.sh "" list.txt tmpoutlist a.log list1.out ""

echo "T range"
./swiftforeach.py range swiftT localhost "" bash x.sh "" 0 9 1 tmpoutlist a.log list1.out "hello there T"

echo "Call K from Galaxy"
python /Users/ketan/galaxy-dist/tools/swift/swiftforeach.py         "range"         "swiftK"         "localhost"          "swiftarg"         "sh"         "/Users/ketan/galaxy-dist/database/files/000/dataset_348.dat"         "/dev/null"         "0"         "9"         "1"         "$HOME/sandbox2"         "/Users/ketan/galaxy-dist/database/files/000/dataset_353.dat"         "/Users/ketan/galaxy-dist/database/files/000/dataset_354.dat"         "hello how are you"

echo "Call T from Galaxy"
python /nfs2/ketan/galaxy-dist/tools/swift/swiftforeach.py "range" "swiftT" "localhost" "" "sh" "/nfs2/ketan/galaxy-dist/database/files/000/dataset_1.dat" "None" "0" "9" "1" "$HOME/swift-sandbox" "/nfs2/ketan/galaxy-dist/database/files/000/dataset_12.dat" "/nfs2/ketan/galaxy-dist/database/files/000/dataset_13.dat" "hello how are you"
