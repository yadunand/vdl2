import files;
import string;
import io;

app (file _out, file _err) anapp(file _exec, int _i, string _stringargs[], file _fileargs[], file _stdin){
    "bash" _exec _i _stringargs _fileargs @stdin=_stdin @stdout=_out @stderr=_err;
}

main{
    string stringargs[]=["hello","there","T"];
    file fileargs[];
    file stdinfile = input("/dev/null");

    file exec=input("x.sh");
    foreach i in [0:99:1]{
        file out <sprintf("tmpoutlist/%i.out", i)>;
        file err <sprintf("tmpoutlist/%i.err", i)>;
        (out,err) = anapp(exec, i, stringargs, fileargs, stdinfile);
    }
    printf("%s", "Done!");
}
