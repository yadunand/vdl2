#!/bin/bash

# generates sites file in /tmp/sites.xml based on one of the following argument provided
# localhost
# stampede
# midway
# uc3

# defaults to localhost of none of the above provided
echo $@ > /tmp/buildsiteargs.txt
# numeric range
sites=$1
wdir=$2
# use "here" document for configs

cat << EOF > $wdir/pre.txt
<?xml version="1.0" encoding="UTF-8"?>
<config xmlns="http://www.ci.uchicago.edu/swift/SwiftSites">
EOF

cat << EOF > $wdir/post.txt
</config>
EOF

IFS=',' read -ra S <<< "$sites"
for site in "${S[@]}" ; do
case "$site" in
midway )
cat << EOF >> $wdir/pool.xml
<pool handle="midway">
  <execution provider="coaster" url="swift.rcc.uchicago.edu" jobmanager="ssh-cl:slurm"/>
  <profile namespace="globus" key="queue">sandyb</profile>
  <profile namespace="globus" key="jobsPerNode">16</profile>
  <profile namespace="globus" key="maxWalltime">02:00:00</profile>
  <profile namespace="globus" key="maxTime">7500</profile>
  <profile namespace="globus" key="highOverAllocation">100</profile>
  <profile namespace="globus" key="lowOverAllocation">100</profile>
  <profile namespace="globus" key="slots">2</profile>
  <profile namespace="globus" key="maxNodes">1</profile>
  <profile namespace="globus" key="nodeGranularity">1</profile>
  <profile namespace="karajan" key="jobThrottle">.31</profile>
  <profile namespace="karajan" key="initialScore">10000</profile>
  <filesystem provider="local" />
  <workdirectory>/tmp/swift.work</workdirectory>
</pool>
EOF
;;

blues )
cat << EOF >> $wdir/pool.xml
<pool handle="blues">
  <execution provider="coaster" jobmanager="local:pbs"/>
  <filesystem provider="local" url="none" />
  <profile namespace="globus" key="maxtime">4300</profile>
  <profile namespace="globus" key="maxWalltime">01:10:00</profile>
  <profile namespace="globus" key="jobsPerNode">16</profile>
  <profile namespace="globus" key="slots">1</profile>
  <profile namespace="globus" key="ppn">16</profile>
  <profile namespace="globus" key="nodeGranularity">1</profile>
  <profile namespace="globus" key="maxnodes">1</profile>
  <profile namespace="karajan" key="jobThrottle">0.20</profile>
  <profile namespace="karajan" key="initialScore">10000</profile>
  <workdirectory>/tmp/swift.workdir</workdirectory>
</pool>
EOF
;;

uc3 )
cat << EOF >> $wdir/pool.xml
<pool handle="uc3">
  <execution provider="coaster" url="uc3-sub.uchicago.edu" jobmanager="ssh-cl:condor"/>
  <!--<execution provider="coaster" jobmanager="local:condor"/>-->
  <profile namespace="karajan" key="jobThrottle">.09</profile>
  <profile namespace="karajan" key="initialScore">1000</profile>
  <profile namespace="globus"  key="jobsPerNode">1</profile>
  <profile namespace="globus"  key="maxWalltime">3600</profile>
  <profile namespace="globus"  key="nodeGranularity">1</profile>
  <profile namespace="globus"  key="highOverAllocation">100</profile>
  <profile namespace="globus"  key="lowOverAllocation">100</profile>
  <profile namespace="globus"  key="slots">1</profile>
  <profile namespace="globus"  key="condor.Requirements">UidDomain == "osg-gk.mwt2.org"</profile>
  <profile namespace="globus"  key="maxNodes">1</profile>
  <profile namespace="globus"  key="condor.+AccountingGroup">"group_friends.{env.USER}"</profile>
  <profile namespace="globus"  key="jobType">nonshared</profile>
  <filesystem provider="local" />
  <!--<workdirectory>/home/maheshwari/test/catsn/swift.work</workdirectory>-->
  <workdirectory>/tmp/swift.work</workdirectory>
</pool>
EOF
;;

stampede )
cat << EOF >> $wdir/pool.xml
<pool handle="stampede">
  <execution provider="coaster" jobmanager="ssh-cl:slurm" url="stampede.tacc.utexas.edu"/>
  <filesystem provider="local"/>
  <profile namespace="globus" key="jobsPerNode">16</profile>
  <profile namespace="globus" key="ppn">16</profile>
  <profile namespace="globus" key="project">TG-CCR130051</profile>
  <profile namespace="globus" key="queue">normal</profile>
  <profile namespace="globus" key="slurm.mail-user">ketancmaheshwari@gmail.com</profile>
  <profile namespace="globus" key="slurm.mail-type">ALL</profile>
  <profile namespace="globus" key="maxWalltime">02:00:00</profile>
  <profile namespace="globus" key="maxTime">8000</profile>
  <profile namespace="globus" key="highOverAllocation">100</profile>
  <profile namespace="globus" key="lowOverAllocation">100</profile>
  <profile namespace="globus" key="slots">1</profile>
  <profile namespace="globus" key="maxNodes">1</profile>
  <profile namespace="globus" key="nodeGranularity">1</profile>
  <profile namespace="karajan" key="jobThrottle">.15</profile>
  <profile namespace="karajan" key="initialScore">10000</profile>
  <workdirectory>/tmp/swift.work</workdirectory>
</pool>
EOF
;;

tukey )
cat << EOF >> $wdir/pool.xml
<pool handle="tukey">
  <filesystem provider="local" />
  <execution provider="coaster" jobmanager="local:cobalt"/>
  <profile namespace="globus"  key="project">ExM</profile>
  <profile namespace="karajan" key="jobthrottle">5.11</profile>
  <profile namespace="karajan" key="initialScore">10000</profile>
  <profile namespace="globus"  key="jobsPerNode">8</profile>
  <profile namespace="globus"  key="slots">1</profile>
  <profile namespace="globus"  key="maxNodes">1</profile>
  <profile namespace="globus"  key="nodeGranularity">1</profile>
  <profile namespace="globus"  key="maxTime">4300</profile>
  <workdirectory>/home/{env.USER}/swiftwork</workdirectory>
</pool>
EOF
;;

localhost )
cat << EOF >> $wdir/pool.xml
<pool handle="localhost">
  <execution provider="local" />
  <profile namespace="karajan" key="jobThrottle">0.07</profile>
  <profile namespace="karajan" key="initialScore">10000</profile>
  <filesystem provider="local"/>
  <profile namespace="swift" key="stagingMethod">local</profile>
  <workdirectory>/tmp/swift.work</workdirectory>
</pool>
EOF
;;

* )
echo "defaulting to localhost"
cat << EOF >> $wdir/pool.xml
<pool handle="localhost">
  <execution provider="local" />
 <profile namespace="karajan" key="jobThrottle">0.07</profile>
 <profile namespace="karajan" key="initialScore">10000</profile>
 <filesystem provider="local"/>
 <profile namespace="swift" key="stagingMethod">local</profile>
 <workdirectory>/tmp/swift.work</workdirectory>
</pool>
EOF
;;
esac
done

cat $wdir/pre.txt $wdir/pool.xml $wdir/post.txt > $wdir/sites.xml

