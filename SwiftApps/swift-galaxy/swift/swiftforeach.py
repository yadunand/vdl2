#!/usr/bin/env python

"""
This is the Swift foreach driver for Galaxy. It receives inputs from the Swift
"foreach" Galaxy tool and depending on the options specified, builds a
canonical Swift/K or Swift/T script and runs over specified computational
environment.
"""

import subprocess
import sys
import os
import distutils.spawn
import traceback
import fnmatch
import re

def setwdir():
    return subprocess.check_output(["mktemp", "-d", "/tmp/swift-gal.XXXX"])

def genargs(args):
    for a in args:
        yield a

def buildcfg(wdir):
    f=open(wdir+"/cf", 'w')
    f.write("use.provider.staging=true\n")
    f.write("wrapperlog.always.transfer=true\n")
    f.write("execution.retries=0\n")
    f.write("provider.staging.pin.swiftfiles=false\n")
    f.write("sitedir.keep=true\n")
    f.write("lazy.errors=false\n")
    f.close()

def main():
    myargs = genargs(sys.argv)
    try:
        this=next(myargs)
        range_or_list = next(myargs)
        k_or_t = next(myargs)
        sites = next(myargs)
        swiftargs = next(myargs)
        interpreter = next(myargs)
        executable = next(myargs)
        stdin = next(myargs)

        if range_or_list == "list":
            #list of items
            listfile = next(myargs)
            n = sum(1 for line in open(listfile))
        else:
            # numeric range
            rstart = next(myargs)
            rend = next(myargs)
            stepsize = next(myargs)
            n = (int(rend) - int(rstart))/int(stepsize)
            
        outloc = next(myargs)
        outlistfile = next(myargs)
        logfile = next(myargs)
        stringargs = next(myargs)
    except:
        traceback.print_exc()
        sys.exit(1)

    #which swift
    swift = distutils.spawn.find_executable("swift")
    
    BASEDIR = os.path.dirname(os.path.realpath(__file__))

    # workout the file args
    fileargs=[]
    for f in myargs:
        fileargs.append(f)

    #workout the file array only if user adds args else make it blank
    if fileargs:
        if k_or_t == "swiftK":
            filearrayexpr="file fileargs[] <fixed_array_mapper; files=\""+','.join(fileargs)+"\">;\n"
        else: #T
            filearrayexpr="file fileargs[]="+str(fileargs)+";\n"
    else:
        filearrayexpr="file fileargs[];\n"
    
    # create outloc dir
    if not os.path.exists(outloc):
        os.makedirs(outloc)
        
    # workout the stringargs
    if stringargs:
        stringarrayexpr="string stringargs[]="+re.sub('\'','"',str(stringargs.split()))+";\n"
    else:
        stringarrayexpr="string stringargs[];\n"
    
    wdir = setwdir().rstrip()

    #workout standard input file
    if stdin != "None":
        if k_or_t == "swiftK":
            stdinfilexpr = "file stdinfile<\""+stdin+"\">;\n"
        else:
            stdinfilexpr = "file stdinfile = input(\""+stdin+"\");\n"
    else:
        #print "no stdin"
        if k_or_t == "swiftK":
            stdinfilexpr = "file stdinfile<\"/dev/null\">;\n"
        else:
            stdinfilexpr = "file stdinfile = input(\"/dev/null\");\n"


    #Build Swift source code
    f = open(wdir+"/script.swift", "w");
    
    if k_or_t == "swiftK":
        
        f.write("type file; \n")
        f.write("app (file _out, file _err) anapp(file _exec, int _i, string _stringargs[], file _fileargs[], file _stdin){ \n")
        f.write("    "+interpreter+" @_exec _i _stringargs @_fileargs stdin=@_stdin stdout=@_out stderr=@_err; \n")
        f.write("}\n")
        f.write(stringarrayexpr)
        f.write(filearrayexpr)
        f.write(stdinfilexpr)
        f.write("file exec<\""+executable+"\">;\n")
        
        if range_or_list == "list":

            f.write("file listfile<\""+listfile+"\">;\n")
            f.write("string list[] = readData(listfile);\n");
            f.write("foreach l, i in list{\n")
            f.write("    file out <single_file_mapper; file=@strcat(\""+outloc+"\", \"/\", l, \".\", i, \".out\")>;\n")
            f.write("    file err <single_file_mapper; file=@strcat(\""+outloc+"\", \"/\", l, \".\", i, \".err\")>;\n")

        else:

            f.write("foreach i in ["+rstart+":"+rend+":"+stepsize+"]{\n")
            f.write("    file out <single_file_mapper; file=@strcat(\""+outloc+"\", \"/\", i, \".out\")>;\n")
            f.write("    file err <single_file_mapper; file=@strcat(\""+outloc+"\", \"/\", i, \".err\")>;\n")
        
        f.write("    (out,err) = anapp(exec, i, stringargs, fileargs, stdinfile);\n")
        f.write("}\n")

        #build site
        if os.path.dirname(sys.argv[0]):
            basedir=os.path.dirname(sys.argv[0])
        else:
            basedir='.'
        #print "%s %s %s" %(basedir, sites, wdir)
        subprocess.call([basedir+"/buildsite.sh", sites, wdir])

        #build config file
        buildcfg(wdir)
        
        #build tc file
        with open(wdir+"/tc", 'w') as tc:
            for site in sites.split():
                tc.write(site+' '+interpreter+' '+interpreter)

    else: # T

        f.write("import files;\n");
        f.write("import string;\n");
        f.write("import io;\n");
        f.write("\n");
        f.write(" app (file _out, file _err) anapp(file _exec, int _i, string _stringargs[], file _fileargs[], file _stdin){\n");
        f.write("  \"" +interpreter+"\"  _exec _i _stringargs _fileargs @stdin=_stdin @stdout=_out @stderr=_err; \n");
        f.write("}\n");
        f.write("main{\n");
        f.write(stringarrayexpr + " \n");
        f.write(filearrayexpr + " \n");
        f.write(stdinfilexpr + " \n");
        f.write("file exec = input(\""+executable+"\");\n");
        
        if range_or_list == "list":
        
            f.write(" \n");
            f.write("file listfile=input(\""+listfile+"\");\n")
            f.write("string list[] = split(read(listfile),);\n");
            f.write("foreach l, i in list{\n")
            f.write("    file out <sprintf(\""+outloc+"/%s.%i.out\", l, i)>;\n")
            f.write("    file err <sprintf(\""+outloc+"/%s.%i.err\", l, i)>;\n")
        
        else:
        
            f.write("foreach i in ["+rstart+":"+rend+":"+stepsize+"]{ \n");
            f.write("    file out <sprintf(\""+outloc+"/%i.out\", i)>; \n");
            f.write("    file err <sprintf(\""+outloc+"/%i.err\", i)>; \n");
        
        f.write("    (out,err) = anapp(exec, i, stringargs, fileargs, stdinfile);\n")
        f.write("}\n");
        f.write("printf(\"%s\", \"Done!\");\n");
        f.write("}\n");
    
    f.close()  

    #Invocation
    if k_or_t == "swiftK":
        
        with open(logfile,"wb") as errfile:
            exitcode=subprocess.check_call([swift, "-sites.file", wdir+"/sites.xml", "-tc.file", wdir+"/tc", "-config", wdir+"/cf", wdir+"/script.swift", swiftargs],stderr=errfile)
        print exitcode
 
    else:
        
        #print "Invoking T"
        stc=distutils.spawn.find_executable("stc")
        java=distutils.spawn.find_executable("java")
        turbine= distutils.spawn.find_executable("turbine")
        
        if sites == "localhost":
            
            print "localhost"
            with open(logfile, "wb") as errfile:
                exitcode=subprocess.check_call([stc, "-j", java, wdir+"/script.swift"], stderr=errfile)

            if exitcode == 0:
                with open(logfile, "wb") as errfile, open("/dev/null", "r") as infile:
                    exitcode=subprocess.check_call([turbine, "-n", str(n), wdir+"/script.tcl", swiftargs], stdin=infile, stderr=errfile)
                    
        elif sites == "Vesta":
            
            print "vesta"
            pwd=os.path.getcwd()
            turbine_home="/home/wozniak/Public/sfw/turbine"
            with open(logfile, "wb") as errfile:
                exitcode=subprocess.check_call([stc, "-t", "checkpointing", "-r", pwd, "-j", java, wdir+"/script.swift"], stderr=errfile)
            
            if exitcode == 0:
                with open(logfile, "wb") as errfile, open("/dev/null", "r") as infile:
                    exitcode=subprocess.check_call([turbine_home+"/scripts/submit/cobalt/turbine-cobalt-run.zsh", "-n", str(n), wdir+"/script.tcl"], stdin=infile, stderr=errfile)
        
        elif sites == "Mira":
        
            print "mira"
            pwd=os.path.getcwd()
            turbine_home="/home/wozniak/Public/sfw/ppc64/turbine"
            stc="/home/wozniak/Public/sfw/ppc64/stc/bin/stc"
            with open(logfile, "wb") as errfile:
                exitcode=subprocess.check_call([stc, "-t", "checkpointing", "-r", pwd, "-j", java, wdir+"/script.swift"], stderr=errfile)
            
            if exitcode == 0:
                with open(logfile, "wb") as errfile, open("/dev/null", "r") as infile:
                    exitcode=subprocess.check_call([turbine_home+"/scripts/submit/cobalt/turbine-cobalt-run.zsh", "-n", str(n), wdir+"/script.tcl"], stdin=infile, stderr=errfile)

        else:
        
            print "Site not recognized."

    #Populate output file
    outlist=subprocess.check_output(["find", outloc, "-type", "f", "-iname", "*.out"])
    f=open(outlistfile, "w")
    f.write(outlist);
    f.close()

if __name__=='__main__':
    main()
