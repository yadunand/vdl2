#post processing, set build for data and add additional data to history^M
from galaxy import datatypes, config, jobs, tools
from galaxy.model import HistoryDatasetAssociation
import operator
import shutil 
import os
import glob
from os.path import basename

def createDsRef(outDataItems, param_dict, outputName, outputFormat, outputDsRefName, nameSpace):
    dsrefXML="<?xml version=\"1.0\" ?>\n"
    dsrefXML=dsrefXML+"<dataSetRefs xmlns=\"http://ci.uchicago.edu/galaxy-es/datasetrefs/"+nameSpace+"\" >\n"

    dsrefFileName=""

    for outDataName, outDataItem in outDataItems:
        if outDataName==outputDsRefName:
            dsrefFileName=outDataItem.get_file_name()
        else:
            if outDataName==outputName:
                dsrefXML=dsrefXML+"<dataSetRef id=\"%s\" name=\"%s\" path=\"%s\" type=\"%s\"/>\n" % (outDataItem.id,outDataItem.name,outDataItem.get_file_name(),outputFormat)

    print "dsrefFileName:"+dsrefFileName

    primaryCollectedDatasets=param_dict["__collected_datasets__"]["primary"]

    for primaryCollectedDataset in primaryCollectedDatasets:
        primaryCollectedDatasetItems=primaryCollectedDatasets[primaryCollectedDataset]
        for primaryCollectedDatasetItem in primaryCollectedDatasetItems:
            outDataItem=primaryCollectedDatasetItems[primaryCollectedDatasetItem]
            if outputFormat in outDataItem.extension:

                dsrefXML=dsrefXML+"<dataSetRef id=\"%s\" name=\"%s\" path=\"%s\" type=\"%s\"/>\n" % (outDataItem.id,outDataItem.name,outDataItem.get_file_name(),outDataItem.extension)

    dsrefXML=dsrefXML+"</dataSetRefs>\n"

    return dsrefFileName, dsrefXML

def writeDsRef(dsrefFileName, dsrefXML):
    dsrefFile = open(dsrefFileName, "w", 0)
    dsrefFile.write(dsrefXML)
    dsrefFile.flush()
    dsrefFile.close()

def setSizeAndPeak(outDataItems,outputDsRefName):
    for outDataName, outDataItem in outDataItems:
        if outDataName==outputDsRefName:
            outDataItem.dataset.file_size=None
            outDataItem.set_size()
            outDataItem.set_peek()

def writeoutlist(app, inp_data, out_data, param_dict, tool, stdout, stderr):

    outDataItems = out_data.items()
    outDataItems = sorted(outDataItems, key=operator.itemgetter(0))

    dsrefAcmoFileName, dsrefAcmoXML = createDsRef(outDataItems,param_dict,"acmo","acmo.csv","acmoDsRef","facit/acmo")

    writeDsRef(dsrefAcmoFileName, dsrefAcmoXML)

    setSizeAndPeak(outDataItems,"acmoDsRef")


    app.model.context.flush()

