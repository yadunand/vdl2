#!/bin/bash

echo $@ > /tmp/args.txt
wdir=$(mktemp -d /tmp/swift-gal.XXXX)

# numeric range
sites=$1 # comma separated list of sites
shift
swiftargs=$1 
shift
interpreter=$1
shift
executable=$1
shift
rstart=$1
shift
rend=$1
shift
stepsize=$1
shift
outloc=$1
shift
outlistfile=$1
shift
logfile=$1
shift

stringargs=$1
shift

#workout the array only if user adds args else make it blank
if [ "${stringargs}"  ] ; then
    stringarrayexpr='string stringargs[]=['$(echo $stringargs | sed -e 's/ *$//g' -e 's/[^ ][^ ]*/"&"/g' -e 's/ /,/g')'];';
else
    stringarrayexpr='string stringargs[];'
fi

fileargs=()
while [ $# -gt 0 ] ; do
    fileargs+=("\"$1\"")
  shift
done

#workout the array only if user adds file args else make it blank
if [ ${#fileargs[@]} -gt 0 ] ; then
    tmp=$(printf -- '%s,' "${fileargs[@]}")
    filearrayexpr='file fileargs[]=['$(echo $tmp | sed s/,$//)'];';
else
    filearrayexpr='file fileargs[];'
fi

#Build Swift source code
cat << EOF > $wdir/script.swift
type file;

app (file _out, file _err) anapp(file _exec, int _i, string _stringargs[], file _fileargs[]){
    $interpreter @_exec _i _stringargs @_fileargs stdout=@_out stderr=@_err;
}

$stringarrayexpr
$filearrayexpr

file exec<"$executable">;
foreach i in [$rstart:$rend:$stepsize]{
    file out <single_file_mapper; file=@strcat("$outloc", "/", i, ".out")>;
    file err <single_file_mapper; file=@strcat("$outloc", "/", i, ".err")>;
    (out,err) = anapp(exec, i, stringargs, fileargs);
}
EOF

swift=$(which swift)

#build site
BASEDIR=$(dirname $0)
$BASEDIR/buildsite.sh $sites $wdir

#build config
cat << EOF > $wdir/cf
use.provider.staging=true
wrapperlog.always.transfer=true
execution.retries=0
provider.staging.pin.swiftfiles=false
sitedir.keep=true
lazy.errors=false
EOF

#build tc
IFS=',' read -ra S <<< "$sites"
for site in "${S[@]}" ; do
cat << EOF >> $wdir/tc
$site $interpreter $interpreter
EOF
done

$swift -sites.file $wdir/sites.xml -tc.file $wdir/tc -config $wdir/cf $wdir/script.swift "${swiftargs}" 2>$logfile
EXITCODE=$?

#`for i in \`find $HOME/swift-sandbox -type f\`; do echo "\`basename $i\` $i"; done`
#Populate output file
cat << EOF > $outlistfile
`for i in $(find $outloc -type f -iname "*.out"); do echo "$i"; done`
EOF

#cleanup
#rm -rf /tmp/sites.xml /tmp/tc /tmp/cf /tmp/script.swift 

if [ "$EXITCODE" -ne "0" ]; then
       cat $logfile >&2
fi

exit $EXITCODE

#dum ditty dum ditty dum dum dum
