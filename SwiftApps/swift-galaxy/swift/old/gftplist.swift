type file;

app (file _status, file _stdout, file _stderr) stage (string _src, string _dst){
   stage _src _dst @_status stdout=@_stdout stderr=@_stderr;
}

string src="gsiftp://trestles-dm2.sdsc.edu:2811/home/ketan/storage/";
string dst="gsiftp://gridftp.stampede.tacc.utexas.edu:2811/scratch/01739/ketan/dest/";

string data[] = readData("datlist.txt");

foreach d, i in data{
  file status <single_file_mapper; file=@strcat("outdir/status.",i,".txt")>;
  file out <single_file_mapper; file=@strcat("outdir/std.",i,".out")>;
  file err <single_file_mapper; file=@strcat("outdir/std.",i,".err")>;
  //trace (@strcat(src, d));
  (status, out, err)  = stage (@strcat(src, d), @strcat(dst, d));
}
