#!/bin/bash

#set -x
set -e

site=$1
data=$2
n=$3
outdir=$4
logfile=$5
htmlfile=$6
mkdir -p $outdir

swift=`which swift`
# use "here" document for configs

cat << EOF > /tmp/script.swift
### <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
# Place your Swift script here
# Make sure all data references are full-path
### <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
EOF

case "$site" in
midway )
cat << 'EOF' > /tmp/sites.xml
<?xml version="1.0" encoding="UTF-8"?>
<config xmlns="http://www.ci.uchicago.edu/swift/SwiftSites">
 <pool handle="midway">
  <execution provider="coaster" url="swift.rcc.uchicago.edu" jobmanager="ssh-cl:slurm"/>
  <profile namespace="globus" key="queue">sandyb</profile>
  <profile namespace="globus" key="jobsPerNode">16</profile>
  <profile namespace="globus" key="maxWalltime">02:00:00</profile>
  <profile namespace="globus" key="maxTime">7500</profile>
  <profile namespace="globus" key="highOverAllocation">100</profile>
  <profile namespace="globus" key="lowOverAllocation">100</profile>
  <profile namespace="globus" key="slots">2</profile>
  <profile namespace="globus" key="maxNodes">1</profile>
  <profile namespace="globus" key="nodeGranularity">1</profile>
  <profile namespace="karajan" key="jobThrottle">.31</profile>
  <profile namespace="karajan" key="initialScore">10000</profile>
  <workdirectory>/tmp/swift.work</workdirectory>
 </pool>
</config>
EOF
;;

uc3 )
cat << 'EOF' > /tmp/sites.xml
<?xml version="1.0" encoding="UTF-8"?>
<config xmlns="http://www.ci.uchicago.edu/swift/SwiftSites">
 <pool handle="uc3">
   <execution provider="coaster" url="uc3-sub.uchicago.edu" jobmanager="ssh-cl:condor"/>
   <!--<execution provider="coaster" jobmanager="local:condor"/>-->
   <profile namespace="karajan" key="jobThrottle">.09</profile>
   <profile namespace="karajan" key="initialScore">1000</profile>
   <profile namespace="globus"  key="jobsPerNode">1</profile>
   <profile namespace="globus"  key="maxWalltime">3600</profile>
   <profile namespace="globus"  key="nodeGranularity">1</profile>
   <profile namespace="globus"  key="highOverAllocation">100</profile>
   <profile namespace="globus"  key="lowOverAllocation">100</profile>
   <profile namespace="globus"  key="slots">1</profile>
   <profile namespace="globus"  key="condor.Requirements">UidDomain == "osg-gk.mwt2.org"</profile>
   <profile namespace="globus"  key="maxNodes">1</profile>
   <profile namespace="globus"  key="condor.+AccountingGroup">"group_friends.{env.USER}"</profile>
   <profile namespace="globus"  key="jobType">nonshared</profile>
   <filesystem provider="local" url="none" />
   <!--<workdirectory>/home/maheshwari/test/catsn/swift.work</workdirectory>-->
   <workdirectory>/tmp/swift.work</workdirectory>
 </pool>
</config>
EOF
;;

stampede )
cat << 'EOF' > /tmp/sites.xml
<?xml version="1.0" encoding="UTF-8"?>
<config xmlns="http://www.ci.uchicago.edu/swift/SwiftSites">
<pool handle="stampede">
  <execution provider="coaster" jobmanager="ssh-cl:slurm" url="stampede.tacc.utexas.edu"/>
  <filesystem provider="local"/>
  <profile namespace="globus" key="jobsPerNode">16</profile>
  <profile namespace="globus" key="ppn">16</profile>
  <profile namespace="globus" key="project">TG-STA110005S</profile>
  <profile namespace="globus" key="queue">normal</profile>
  <profile namespace="globus" key="slurm.mail-user">ketancmaheshwari@gmail.com</profile>
  <profile namespace="globus" key="slurm.mail-type">ALL</profile>
  <profile namespace="globus" key="maxWalltime">02:00:00</profile>
  <profile namespace="globus" key="maxTime">8000</profile>
  <profile namespace="globus" key="highOverAllocation">100</profile>
  <profile namespace="globus" key="lowOverAllocation">100</profile>
  <profile namespace="globus" key="slots">1</profile>
  <profile namespace="globus" key="maxNodes">1</profile>
  <profile namespace="globus" key="nodeGranularity">1</profile>
  <profile namespace="karajan" key="jobThrottle">.15</profile>
  <profile namespace="karajan" key="initialScore">10000</profile>
  <workdirectory>/tmp/swift.work</workdirectory>
</pool>
</config>
EOF
;;

localhost )
cat << 'EOF' > /tmp/sites.xml
<?xml version="1.0" encoding="UTF-8"?>
<config xmlns="http://www.ci.uchicago.edu/swift/SwiftSites">
<pool handle="localhost">
  <execution provider="local" jobmanager="local:local"/>
  <filesystem provider="local"/>
  <!--<workdirectory>/home/kcm92/powergridapps/swiftscripts/swift.work</workdirectory>-->
  <workdirectory>/tmp/swift.work</workdirectory>
  <profile namespace="karajan" key="jobThrottle">0.31</profile>
</pool>
</config>
EOF
;;

* )
echo "defaulting to localhost"

cat << 'EOF' > /tmp/sites.xml
<?xml version="1.0" encoding="UTF-8"?>
<config xmlns="http://www.ci.uchicago.edu/swift/SwiftSites">
<pool handle="localhost">
  <execution provider="local" jobmanager="local:local"/>
  <filesystem provider="local"/>
  <!--<workdirectory>/home/kcm92/powergridapps/swiftscripts/swift.work</workdirectory>-->
  <workdirectory>/tmp/swift.work</workdirectory>
  <profile namespace="karajan" key="jobThrottle">0.31</profile>
</pool>
</config>
EOF
;;
esac

cat << 'EOF' > /tmp/cf
use.provider.staging=true
wrapperlog.always.transfer=true
execution.retries=1
provider.staging.pin.swiftfiles=false
sitedir.keep=true
lazy.errors=false
EOF

cat << 'EOF' > /tmp/tc
localhost echo echo
localhost cat cat
stampede echo echo
stampede cat cat
uc3 echo echo
uc3 cat cat
midway echo echo
midway cat cat
EOF

$swift -sites.file /tmp/sites.xml -tc.file /tmp/tc -config /tmp/cf /tmp/catsn.swift -n=$n -data=$data 2> $logfile
EXITCODE=$?

#for i in `find $outdir`
#do
#  lastfile=$i
#done
#bname=`basename $lastfile`

cat << EOF > $htmlfile
<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Catsn output</title>
</head>
<body>
<h2>Swift run trace plots</h2>

<a href="activeplot.png" alt="active plot">active plot</a>
<a href="cumulativeplot.png" alt="cumulative plot">cumulative plot</a>

<h2>Browse output of catsn application</h2>
`for i in \`find $outdir -type f\`; do echo "<a href=\`basename $i\`>Output: \`basename $i\`</a><br/>"; done`
</body>
</html>
EOF

if [ "$EXITCODE" -ne "0" ]; then
       cat $logfile >&2
fi
exit $EXITCODE
