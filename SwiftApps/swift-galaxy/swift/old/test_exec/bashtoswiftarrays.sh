a=()
while [ $# -gt 0 ] ; do
  a+=("\"$1\"")
  shift 
done

tmp=$(printf -- '%s,' "${a[@]}")

expr='['$(echo $tmp | sed s/,$//)']'

echo $expr

#Build Swift source code
#cat << EOF > /tmp/script.swift
#type file;
#
#app (file _out, file _err) anapp(file _exec, int _i, string _args){
#    sh @_exec _i _args stdout=@_out stderr=@_err;
#}
##string args="$a";
#string args[] = [];
#file exec<"/bin/cat">;
#foreach i in [0:2:1]{
#    file out <single_file_mapper; file=@strcat("/tmp/swift-sandbox", "/", i, ".out")>;
#    file err <single_file_mapper; file=@strcat("/tmp/swift-sandbox", "/", i, ".err")>;
#    (out,err) = anapp(exec, i, args);
#}
#EOF

