#!/usr/bin/env python

import subprocess
import sys
#import rpy
import os

def setwdir():
    return subprocess.check_output(["mktemp", "-d", "/tmp/swift-gal.XXXX"])

# numeric range
def main():
    print "%d" % (len(sys.argv))
    print "%s" % (sys.argv)
    try:
        sites=sys.argv[1] # comma separated list of sites
        swiftargs=sys.argv[2]
        interpreter=sys.argv[3]
        executable=sys.argv[4]
        rstart=sys.argv[5]
        rend=sys.argv[6]
        stepsize=sys.argv[7]
        outloc=sys.argv[8]
        outlistfile=sys.argv[9]
        logfile=sys.argv[10]
        stringargs=sys.argv[11]
    except:
        #rpy.stop_error('Wrong number or type of args')
        sys.exit(1)
    
    print sites
    print swiftargs
    print interpreter

    #workout the array only if user adds args else make it blank
    if stringargs:
        stringarrayexpr="string stringargs[]="+str(stringargs.split())+";\n"
    else:
        stringarrayexpr="string stringargs[];\n"
    
    fileargs=list()
    for files in sys.argv[12:]:
        fileargs.append(files)
    
    #workout the array only if user adds file args else make it blank
    if fileargs: 
        filearrayexpr="file fileargs[]="+str(fileargs)+";\n"
    else:
        filearrayexpr="file fileargs[];\n"

    wdir=setwdir().rstrip()
    #Build Swift source code
    f=open(wdir+"/script.swift", 'w');
    f.write("type file; \n")
    
    f.write("app (file _out, file _err) anapp(file _exec, int _i, string _stringargs[], file _fileargs[]){ \n")
    f.write("    "+interpreter+" @_exec _i _stringargs @_fileargs stdout=@_out stderr=@_err; \n")
    f.write("}\n")
    
    f.write(stringarrayexpr)
    f.write(filearrayexpr)
    
    f.write("file exec<'"+executable+"'>;\n")
    f.write("foreach i in ["+rstart+":"+rend+":"+stepsize+"]{\n")
    f.write("    file out <single_file_mapper; file=@strcat("+outloc+", '/', i, '.out')>;\n")
    f.write("    file err <single_file_mapper; file=@strcat('"+outloc+"', '/', i, '.err')>;\n")
    f.write("    (out,err) = anapp(exec, i, "+stringargs+", "+",".join(fileargs)+");\n")
    f.write("}\n")
    f.close()

    swift=subprocess.check_output(["which", "swift"]).rstrip()
    
    #build site
    if os.path.dirname(sys.argv[0]):
        basedir=os.path.dirname(sys.argv[0])
    else:
        basedir='.'
        
    print "%s %s %s" %(basedir, sites, wdir)
    subprocess.call([basedir+"/buildsite.sh", sites, wdir])
    
    #build config
    f=open(wdir+"/cf", 'w');
    f.write('use.provider.staging=true')
    f.write('wrapperlog.always.transfer=true')
    f.write('execution.retries=0')
    f.write('provider.staging.pin.swiftfiles=false')
    f.write('sitedir.keep=true')
    f.write('lazy.errors=false')
    f.close()
    

    #build tc
    f=open(wdir+"/tc", 'w');
    for site in sites.split():
        f.write(site+' '+interpreter+' '+interpreter)
    f.close()

    #exitcode=subprocess.call([swift, '-sites.file', wdir+"/sites.xml", '-tc.file', wdir+"/tc", "-config", wdir+"/cf", wdir+"/script.swift", swiftargs, "2>logfile"])
    
    #`for i in \`find $HOME/swift-sandbox -type f\`; do echo "\`basename $i\` $i"; done`
    #Populate output file
    outlist=subprocess.check_output(["find", outloc, "-type", "f", "-iname", "*.out"])
    f=open(outlistfile)
    f.write(outlist);
    f.close()
    #dum ditty dum ditty dum dum dum

if __name__=='__main__':
    main()
