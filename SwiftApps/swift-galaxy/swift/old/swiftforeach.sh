#!/bin/bash
# originally list

#echo $@ > /tmp/args.txt
#set workdir
wdir=$(mktemp -d /tmp/swift-gal.XXXX)

#determine loop mode: over a list or a range
range_or_list=$1
shift

#determine invocation: Swift /K or /T
k_or_t=$1
shift

#build commandline arguments

sites=$1 
shift
swiftargs=$1 
shift
interpreter=$1
shift
executable=$1
shift
stdin=$1
shift

if [ "$range_or_list" = "list" ] ; then
    listfile=$1
    shift
    n=$(wc -l < $listfile)
else
    rstart=$1
    shift
    rend=$1
    shift
    stepsize=$1
    shift
    n=$(( ($rend - $rstart) / $stepsize ))
fi

outloc=$1
shift
logfile=$1
shift
outlistfile=$1
shift
stringargs=$1
shift 

swift=$(\which swift 2>/dev/null)
BASEDIR=$(dirname $0)

#workout the file args
fileargs=()
while [ $# -gt 0 ] ; do
  fileargs+=("\"$1\"")
  shift
done

#create outloc dir
mkdir -p $outloc

#workout the stringargs 
if [ "${stringargs}"  ] ; then
    stringarrayexpr='string stringargs[]=['$(echo $stringargs | sed -e 's/ *$//g' -e 's/[^ ][^ ]*/"&"/g' -e 's/ /,/g')'];';
else
    stringarrayexpr='string stringargs[];'
fi

#workout standard input file
if [ "${stdin}" ] ; then
    if [ "${k_or_t}" = "swiftK" ] ; then
       stdinfilexpr='file stdinfile<'\"${stdin}\"'>;'
    else
       stdinfilexpr='file stdinfile = input('\"${stdin}\"');'
    fi
else
    if [ "${k_or_t}" = "swiftK" ] ; then
        stdinfilexpr='file stdinfile<'\"/dev/null\"'>;'
    else
        stdinfilexpr='file stdinfile = input('\"/dev/null\"');'
    fi
fi

#workout the array only if user adds args else make it blank
if [ ${#fileargs[@]} -gt 0 ] ; then
  tmp=$(printf -- '%s,' "${fileargs[@]}")
  filearrayexpr='file fileargs[] <fixed_array_mapper; files="'$(echo $tmp | sed -e 's/,$//' -e 's/\"//g')'">;';
else
  filearrayexpr='file fileargs[];'
fi
 
#build swift source
 if [ "$range_or_list" == "list" ] ; then ## list

 cat << EOF > $wdir/script.swift
 /*
 *
 * Swift source script is generated automatically via the Swift-Galaxy
 * integration tool. Be careful before making any changes to it directly as
 * it might be tied with other configuration and paramter values.
 *
 */
 type file;
 
 app (file _out, file _err) anapp(file _exec, int _i, string _stringargs[], file _fileargs[], file _stdin){
     $interpreter @_exec _i _stringargs @_fileargs stdin=@_stdin stdout=@_out stderr=@_err;
 }
 $stringarrayexpr
 $filearrayexpr
 $stdinfilexpr
 
 file exec<"$executable">;
 file listfile<"$listfile">;
 string list[] = readData(listfile);
 foreach l, i in list{
     file out <single_file_mapper; file=@strcat("$outloc", "/", l, ".", i, ".out")>;
     file err <single_file_mapper; file=@strcat("$outloc", "/", l, ".", i, ".err")>;
     (out,err) = anapp(exec, i, stringargs, fileargs, stdinfile);
 }
EOF

else ## range

cat << EOF > $wdir/script.swift
 /*
 *
 * Swift source script is generated automatically via the Swift-Galaxy
 * integration tool. Be careful before making any changes to it directly as
 * it might be tied with other configuration and paramter values.
 *
 */
type file;

app (file _out, file _err) anapp(file _exec, int _i, string _stringargs[], file _fileargs[], file _stdin){
    $interpreter @_exec _i _stringargs @_fileargs stdin=@_stdin stdout=@_out stderr=@_err;
}

$stringarrayexpr
$filearrayexpr
$stdinfilexpr

file exec<"$executable">;
foreach i in [$rstart:$rend:$stepsize]{
    file out <single_file_mapper; file=@strcat("$outloc", "/", i, ".out")>;
    file err <single_file_mapper; file=@strcat("$outloc", "/", i, ".err")>;
    (out,err) = anapp(exec, i, stringargs, fileargs, stdinfile);
}
EOF

fi

touch None # Create a "None" file in case user does not specify any stdin file


############# End common setup ################


if [ "$k_or_t" = "swiftK"  ] ; then #### invoke K
 
$BASEDIR/buildsite.sh $sites $wdir
 
#build config
cat << EOF > $wdir/cf
use.provider.staging=true
wrapperlog.always.transfer=true
execution.retries=0
provider.staging.pin.swiftfiles=false
sitedir.keep=true
lazy.errors=false
EOF
 
#build tc from sites and interpreter
IFS=',' read -ra S <<< "$sites"
for site in "${S[@]}" ; do
cat << EOF >> $wdir/tc
$site $interpreter $interpreter
EOF
done

$swift -sites.file $wdir/sites.xml -tc.file $wdir/tc -config $wdir/cf $wdir/script.swift "${swiftargs}" 2>$logfile
EXITCODE=$?

else 

### Invoke T
 
 #workout the array only if user adds args else make it blank
 if [ ${#fileargs[@]} -gt 0 ] ; then
     tmp=$(printf -- '%s,' "${fileargs[@]}")
     filearrayexpr='file fileargs[] <fixed_array_mapper; files="'$(echo $tmp | sed -e 's/,$//' -e 's/\"//g')'">;';
 else
     filearrayexpr='file fileargs[];'
 fi
 
 cat << EOF > $wdir/script.swift
 /*
 *
 * Swift source script is generated automatically via the Swift-Galaxy
 * integration tool. Be careful before making any changes to it directly as
 * it might be tied with other configuration and paramter values.
 *
 */
 
 import files;
 import string;
 import io;
 
 app (file _out, file _err) anapp(file _exec, int _i, string _stringargs[], file _fileargs[], file _stdin){
     "$interpreter" _exec _i _stringargs _fileargs @stdin=_stdin @stdout=_out @stderr=_err;
 }

main{
 $stringarrayexpr
 $filearrayexpr
 $stdinfilexpr
 
 file exec=input("$executable");
 $( if [ "$range_or_list" = "list" ] ; then
  echo "file listfile=input("$listfile");"
  echo "string list[] = split(read(listfile),"\n");"
  echo "foreach l, i in list{"
  echo "    file out <sprintf(\"$outloc/%s.%i.out\", l, i)>;"
  echo "    file err <sprintf(\"$outloc/%s.%i.err\", l, i)>;"
 else
  echo "foreach i in [$rstart:$rend:$stepsize]{"
  echo "    file out <sprintf(\"$outloc/%i.out\", i)>;"
  echo "    file err <sprintf(\"$outloc/%i.err\", i)>;"
 fi
 )
     (out,err) = anapp(exec, i, stringargs, fileargs, stdinfile);
 }
 printf("%s", "Done!");
 }
EOF

EXITCODE=0
case $sites in  
  "localhost" )
   #echo "in localhost" >> /tmp/status.txt
   STC=$(\which stc)
   #echo "got stc" >> /tmp/status.txt
   turbine=$(\which turbine)
   #echo "got turbine" >> /tmp/status.txt
   $STC -j $(\which java) $wdir/script.swift 2>$logfile
   #echo "done running stc" >> /tmp/status.txt
   #echo $turbine -n $n $wdir/script.tcl "${swiftargs}" >> /tmp/status.txt
   $turbine -n $n $wdir/script.tcl "${swiftargs}" </dev/null
   #echo "done running turbine, for $n" >> /tmp/status.txt
   EXITCODE=$?
   #echo "exit code obtained" >> /tmp/status.txt
	;;
"Vesta" )
  TURBINE_HOME=/home/wozniak/Public/sfw/turbine
  STC=/home/wozniak/Public/sfw/stc/bin/stc
  $STC -j $(\which java) -t checkpointing -r ${PWD} $wdir/script.swift 2>$logfile
  $TURBINE_HOME/scripts/submit/cobalt/turbine-cobalt-run.zsh -n $n -s $HOME/cf $wdir/script.tcl
  EXITCODE=$?
	;;
"Mira" )
  TURBINE_HOME=/home/wozniak/Public/sfw/ppc64/turbine
  STC=/home/wozniak/Public/sfw/ppc64/stc/bin/stc
  $STC -j $(\which java) -t checkpointing -r ${PWD} $wdir/script.swift 2>$logfile
  $TURBINE_HOME/scripts/submit/cobalt/turbine-cobalt-run.zsh -n $n -i $HOME/prerun.sh -s $HOME/cf $wdir/script.tcl
  EXITCODE=$?
	;;
esac

fi
 
cat << EOF > $outlistfile
$(for i in $(find $outloc -type f -iname "*.out"); do echo "$i"; done)
EOF
 
if [ "$EXITCODE" -ne "0" ]; then
      cat $logfile >&2
fi

exit $EXITCODE

