# ampl model declaration
# V. M. Zavala - MCS/ANL 2012

# scenarios 
param NS;                        # number of scenarios 
set SCEN := 1..NS;               # set of scenarios 

# dispatch horizon
param T;                         # horizon length
param TH;                        # simulation horizon (hours)
param dt;                        # length of time step [hr]
set HORIZON := 0..T-1;           # horizon minus one                     
set TESTTIME:=0..T;              # set of test times
param idx_data;                  # auxiliary parameter
param idx_state;                 # auxiliary parameter

# lines
set LIN;                         # set of lines
param  status{LIN}, symbolic;    # line status
param snd_bus{LIN};              # sending bus
param rec_bus{LIN};              # receiving bus
param       X{LIN};              # reactance - Ohm
param       V{LIN};              # voltage drop - kV
param    Pmax{LIN};              # maximum line capacity - MW
param install_cost{LIN};         # annualized installation cost $/yr 

# buses
set BUS;                         # set of buses
param lat_bus{BUS};              # latitude of bus 
param lon_bus{BUS};              # longitude of bus
param      ref_bus, symbolic;    # reference bus

# generators
set GEN;
param    bus_gen{GEN};            # generator bus
param     np_cap{GEN};            # name place capacity - MW
param    sum_cap{GEN};            # summer capacity - MW
param    win_cap{GEN};            # winter capacity - MW
param    min_cap{GEN};            # minimum capacity - MW
param       fuel{GEN}, symbolic;  # type of fuel for generator
param  min_hrate{GEN};            # heat rate at minimum capacity - BTU/kWh 
param  min_power{GEN};            # power at minimum capacity - MW
param     max_ur{GEN};            # minimum up ramp - MW
param     max_dr{GEN};            # maximum down ramp - MW
param    lat_gen{GEN};            # latitude of generation bus
param    lon_gen{GEN};            # longitude of generation bus
param   gen_cost{GEN};            # generation cost $/MWhr
param  Pgen_init{GEN};            # initial conditions generators MW
param clgen_cost{TESTTIME};       # closed-loop generation cost $/MWh

# fuels
set FUEL;
param   Description{FUEL}, symbolic;  # fuel description
param            HV{FUEL};            # heating value (Mbtu/unit)
param     Unitprice{FUEL};            # unit price ($/unit)
param         Units{FUEL}, symbolic;  # units

# loads
set TIME;                            # set of times for loads
set LOAD;                            # set of loads
param   bus_load{LOAD};              # load bus
param loads{TIME,LOAD};              # loads in space and time - MW
param wind_totalmh{SCEN,TESTTIME};   # total wind power - MW

# wind data
set WIND;
param wind_total{SCEN,TIME};        # total wind power - MW
param   bus_wind{WIND};             # wind bus
param wind_share{WIND};             # wind share of total power MW/MW
 
# variables
var  Pgen{SCEN,TESTTIME,GEN};                            # generation levels - MW
var Pload{SCEN,TESTTIME,LOAD};                           # load levels - MW
var         yuc{TESTTIME,j in GEN} >= 0, <= 1;           # ON/OFF Status generators
var slackp{s in SCEN, t in TESTTIME}>=0;                 # slack - for modeling purposes only
var slackm{s in SCEN, t in TESTTIME}>=0;                 # slack - for modeling purposes only
var Pgen_initial{GEN};                                   # initial state for generators - MW

# params for inference
param fix_x; 

# power flow equations 
pfeq{s in SCEN, t in TESTTIME}:  
         +(sum{i in GEN}  Pgen[s,t,i]) 
         -(sum{i in LOAD} Pload[s,t,i])
         + wind_totalmh[s,t]
         = slackp[s,t]-slackm[s,t];
                  
# upper bound and lower bounds for generation flows [MW]
Pgenequb{s in SCEN,t in TESTTIME,i in GEN}:  Pgen[s,t,i] <=  np_cap[i]*yuc[t,i];
Pgeneqlb{s in SCEN,t in TESTTIME,i in GEN}:  Pgen[s,t,i] >=  0;

Pgenequbinit{i in GEN}:  Pgen_initial[i] <=  np_cap[i]*yuc[1,i];
Pgeneqlbinit{i in GEN}:  Pgen_initial[i] >=  0;

# upper and lower bounds on ramps
upramp{s in SCEN,t in HORIZON,i in GEN}:   Pgen[s,t+1,i] -  Pgen[s,t,i] <= +max_ur[i]; 
loramp{s in SCEN,t in HORIZON,i in GEN}:   Pgen[s,t,i]  - Pgen[s,t+1,i] >= -max_dr[i];      

upramp0{s in SCEN,i in GEN}:   Pgen[s,1,i] -  Pgen_initial[i] <= +max_ur[i]; 
loramp0{s in SCEN,i in GEN}:   Pgen_initial[i]  - Pgen[s,1,i] >= -max_dr[i];      

# time-dependent generation costs
var   tot_gen_cost{s in SCEN,t in TESTTIME} = 1e+0*sum{i in GEN}  (gen_cost[i]*Pgen[s,t,i]); 

# time-dependent startup/shutdown costs
var   tot_start_cost{t in TESTTIME} = 1e+3*(sum{i in GEN} yuc[t,i]); 

# time-dependent slack costs
var   tot_slack_cost{s in SCEN,t in TESTTIME} = 1e+4*(slackp[s,t]+slackm[s,t]);

# cost function
minimize cost: sum{t in TESTTIME} tot_start_cost[t]
               + (1/(card(SCEN)))*sum{s in SCEN,t in TESTTIME} tot_gen_cost[s,t]
               + (1/(card(SCEN)))*sum{s in SCEN,t in TESTTIME} tot_slack_cost[s,t]; 
