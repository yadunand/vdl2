#!/bin/bash

site=$1
shift
script=$1
shift
logfile=$1
shift
remoteurl=$1
shift
throttle=$1
shift
project=$1
shift
slots=$1
shift
queue=$1
shift
nodes=$1
shift
nodegranularity=$1
shift
jobspernode=$1
shift
ppn=$1
shift
walltime=$1
shift
maxtime=$1


ln -s $script script.swift
swift=$(which swift)
# use "here" document for configs
BASEDIR=$(dirname $0)
$BASEDIR/buildsite.sh $site

#build config
cat << EOF > /tmp/cf
use.provider.staging=false
wrapperlog.always.transfer=true
execution.retries=0
provider.staging.pin.swiftfiles=false
sitedir.keep=true
lazy.errors=false
EOF

#build tc
cat << EOF > /tmp/tc
localhost sh /bin/sh
localhost echo echo
localhost cat cat
uc3 sh /bin/sh
uc3 echo  /bin/echo
uc3 cat   /bin/cat
stampede sh /bin/sh
stampede echo echo
stampede cat cat
midway echo  echo
midway sh /bin/sh
midway cat   cat
EOF

$swift -sites.file /tmp/sites.xml -tc.file /tmp/tc -config /tmp/cf script.swift 2> $logfile
EXITCODE=$?

if [ "$EXITCODE" -ne "0" ]; then
       cat std.err >&2
fi
#cp std.err $logfile
#rm std.err
exit $EXITCODE
