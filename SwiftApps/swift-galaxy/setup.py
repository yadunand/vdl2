#!/usr/bin/env python

"""
Setup script to install swift-galaxy framework to your Galaxy installation.

Usage: ./setup.py <path/to/galaxy>
"""

import xml.etree.ElementTree as ET
import subprocess
import sys
import os
import distutils.spawn
import traceback
import fnmatch
import tarfile
import urllib

galaxyloc=sys.argv[1]

#step 0. Download swift-galaxy.tgz and swift-rel.tgz untar and set path
if not os.path.isfile("/tmp/swift-galaxy.tgz"):
    urllib.urlretrieve("http://mcs.anl.gov/~ketan/swift-galaxy.tgz", filename="/tmp/swift-galaxy.tgz")
    swiftgaltar = tarfile.open("/tmp/swift-galaxy.tgz")
    swiftgaltar.extractall(path="/tmp")
    swiftgaltar.close()

if not os.path.isfile("/tmp/swift-0.94.1-RC3.tar.gz"):
    urllib.urlretrieve("http://swift-lang.org/packages/swift-0.94.1-RC3.tar.gz", filename="/tmp/swift-0.94.1-RC3.tar.gz")
    swiftar = tarfile.open("/tmp/swift-0.94.1-RC3.tar.gz")
    swiftar.extractall(path="/tmp")
    swiftar.close()
    os.environ["PATH"] = os.environ["PATH"] + ":/tmp/swift-0.94.1-RC3/bin"

#step 1. Create symlink to the swift directory from the tools directory of galaxyloc if not already present
if os.path.lexists(galaxyloc+"/tools/swift"):
    os.remove(galaxyloc+"/tools/swift")

os.symlink ("/tmp/swift-galaxy/swift", galaxyloc+"/tools/swift");

#step 2. update the tool_conf.xml
tree = ET.parse(galaxyloc+"/config/tool_conf.xml.main")
root = tree.getroot()

if 'swift' not in root.attrib:
    root.append((ET.fromstring('<section name="swift" id="swift">\n <tool file="swift/tryswift.xml" />\n <tool file="swift/genericswift.xml" />\n <tool file="swift/swiftforeach.xml" />\n </section>\n ')))

tree.write (galaxyloc+"/config/tool_conf.xml.main")

#step 3. Print message
print "Swift/Galaxy Installed."

