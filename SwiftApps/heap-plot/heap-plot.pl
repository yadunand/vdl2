#!/usr/bin/perl -w
use strict;
use Time::Local 'timelocal_nocheck';

# Usage: ./heap-plot.pl run000.log
if(@ARGV != 1) {
   print "Usage: $0 <swift.log>\n";
   exit 1;
}

# Given "Active: 10", or "HeapMax: 9361686528,", return only values
sub get_value
{
   my $statement = $_[0];
   my $value = (split /:/, $statement)[1];
   $value =~ s/,//g;
   return $value;
}

# Convert Swift time stamp to seconds
sub timestamp_to_seconds
{
   (my $date, my $hhmmss, my @junk) = split('\s+', $_[0]);
   $hhmmss = (split /,/, $hhmmss)[0];
   (my $year, my $month, my $monthday) = split('\-', $date);
   (my $hh, my $mm, my $ss) = split(':', $hhmmss);
   my $time = timelocal_nocheck($ss, $mm, $hh, $monthday-1, $month, $year);
   return $time;
}
my $first_timestamp=0;

# Read log
my $filename=shift;
open(LOG, "$filename") || die "Unable to open $filename\n";
my @filedata = <LOG>;
close(LOG);

# Open data files used for plotting
open(DAT, ">heap-plot.dat") || die "Unable to create heap-plot.dat\n";
print DAT "Time Max Crt Used\n";
open(DAT2, ">heap-plot2.dat") || die "Unable to create heap-plot2.dat\n";
print DAT2 "Time Active Selecting Submitted\n";
open(DAT3, ">heap-plot3.dat") || die "Unable to create heap-plot3.dat\n";
print DAT3 "Time Completed\n";

foreach my $line(@filedata) {

   if( $line =~ m/RuntimeStats\$ProgressTicker/ ) {

      # Heap line
      if( $line =~ m/HeapMax/ ) {
         $line =~ s/HeapMax: /HeapMax:/g;
         $line =~ s/CrtHeap: /CrtHeap:/g;
         $line =~ s/UsedHeap: /UsedHeap:/g;
         my @words = split('\s+', $line);
         my $timestamp = timestamp_to_seconds($line);
         if($first_timestamp == 0) { $first_timestamp = $timestamp; $timestamp=0; } 
         else { $timestamp = $timestamp - $first_timestamp; }
         my ($max, $crt, $used) = (0) x 3;
         foreach my $word(@words) {
            if($word =~ /HeapMax:/)          { $max  = get_value($word); }
            elsif($word =~ /CrtHeap:/)       { $crt  = get_value($word); }
            elsif($word =~ /UsedHeap:/)      { $used = get_value($word); }
         }  
         print DAT "$timestamp $max $crt $used\n";
      }

      # Ticker line
      else {
         $line =~ s/Selecting site:/SelectingSite:/g;
         my @words = split('\s+', $line);
         my $timestamp = timestamp_to_seconds($line);
         if($first_timestamp == 0) { $first_timestamp = $timestamp; $timestamp=0; } 
         else { $timestamp = $timestamp - $first_timestamp; }   
         my ($active, $submitted, $selecting, $completed) = (0) x 4;
         foreach my $word(@words) {
            if($word =~ /Active:/)           { $active    = get_value($word); }
            elsif($word =~ /Submitted:/)     { $submitted = get_value($word); }
            elsif($word =~ /SelectingSite:/) { $selecting = get_value($word); } 
            elsif($word =~ /successfully:/)  { $completed = get_value($word); }
         }
         print DAT2 "$timestamp $active $submitted $selecting\n";
         if($completed != 0) {
            print DAT3 "$timestamp $completed\n";
         }
      }
   }
}

close(DAT);
close(DAT2);
close(DAT3);
open(GP, ">heap-plot.gp") || die "Unable to create heap-plot.gp";

my $gp = <<END;
set key autotitle columnhead
set terminal png size 1000,1000 enhanced
set output "heap-plot.png"

set multiplot layout 3,1 title "Heap Plot"

set xlabel "Time in sec"
set ylabel "Size in MB"
set autoscale x

plot "heap-plot.dat" using 1:(\$2/1048576) with lines, \\
                  '' using 1:(\$3/1048576) with lines, \\
                  '' using 1:(\$4/1048576) with lines

set xlabel "Time in sec"
set ylabel "Number of tasks"
plot "heap-plot2.dat" using 1:(\$2) with lines, \\
                   '' using 1:(\$3) with lines, \\
                   '' using 1:(\$4) with lines

set ylabel "Completed"
plot "heap-plot3.dat" using 1:(\$2) with lines

END

print GP $gp; 
close(GP);

system("gnuplot heap-plot.gp");
unlink("heap-plot.gp");
unlink("heap-plot.dat");
unlink("heap-plot2.dat");
