#!/home/zzhang/workplace/epd-7.0-2-rh5-x86/bin/python2.7
#/usr/bin/env python

# Import graphviz
import sys
sys.path.append('..')
sys.path.append('/usr/lib/graphviz/python/')
sys.path.append('/usr/lib64/graphviz/python/')

# Import pygraph
#from pythongraph import *
from matplotlib.patches import Circle, PathPatch
import matplotlib.lines as mlines
import matplotlib
matplotlib.use('Agg')

from matplotlib.font_manager import FontProperties
'''
from pygraph.classes.graph import graph
from pygraph.classes.digraph import digraph
from pygraph.algorithms.searching import breadth_first_search
from pygraph.algorithms.accessibility import accessibility, connected_components
from pygraph.algorithms.minmax import shortest_path, heuristic_search
from pygraph.classes.digraph import digraph
from pygraph.algorithms.heuristics.euclidean import euclidean
from pygraph.algorithms.heuristics.chow import chow
#from pygraph.readwrite.dot import write
'''
from string import replace
import numpy as np
from matplotlib import pyplot
from pylab import *
import operator
from decimal import *
import math as mt
import random
import networkx as nx

import pickle
import os

#----------------------------------------------------------
def string_wrap(st, mode):
    
    st = str(st)
    
    if mode==1:
        st = "\033[1;29m" + st + "\033[0m"
    elif mode==2:
        st = "\033[1;34m" + st + "\033[0m"
    elif mode==3:
        st = "\033[1;44m" + st + "\033[0m"
    elif mode==4:
        st = "\033[1;35m" + st + "\033[0m"
    elif mode==5:
        st = "\033[1;33;44m" + st + "\033[0m"
    elif mode==6:
        st = "\033[1;47;34m" + st + "\033[0m"
    elif mode==7:
        st = "\033[1;1;31m" + st + "\033[0m"
    elif mode==8:
        st = "\033[1;1;33m" + st + "\033[0m"
    elif mode==9:
        st = "\033[1;1;43;34m" + st + "\033[0m"
    elif mode==10:
        st = "\033[1;1;37m" + st + "\033[0m"
    elif mode==11:
        st = "\033[1;30;47m" + st + "\033[0m"
    else:
        st = st + ' '
    return st
#-----------------------------------------------------------
def draw_graph(visible,pos,name,number,dpidpi,Nce,Ncv,scale):
	fig = pyplot.figure(num=None,dpi=dpidpi,facecolor='w',edgecolor='k')
	rect = [0.1,0.13,0.8,0.77]
	ax = plt.subplot(111)

	ax.axis('off')


	xmin = inf
	xmax = -inf
	ymin = inf
	ymax = -inf
	part = 0.01
	
	for i in range(Ncv):
		x,y=pos[i]
		if x > xmax:
			xmax = x
		elif x < xmin:
			xmin = x
		if y > ymax:
			ymax = y
		elif y < ymin:
			ymin = y
			
	xlim(xmin-(xmax-xmin)*part,xmax+(xmax-xmin)*part)
	ylim(ymin-(ymax-ymin)*part,ymax+(ymax-ymin)*part)

	for i in range(Ncv):
		x,y=pos[i]
		r = sqrt(float(G.degree(i)))/pi
		circle = Circle((x, y), r*scale, facecolor='none',
                edgecolor=(0.3,0.3,0.3), linewidth=0.5, alpha=0.5)
		ax.add_patch(circle)
	

	savefig(name+str(number)+'.png', dpi=dpidpi, facecolor='w', edgecolor='w',
        orientation='portrait', papertype=None, format='png',
        transparent=False, bbox_inches=None, pad_inches=0.1)

#---------------------------------------
def connected_components(G):
	"""Return nodes in connected components of graph.

    Parameters
    ----------
    G : NetworkX Graph
       An undirected graph.

    Returns
    -------
    comp : list of lists
       A list of nodes for each component of G.

    See Also       
    --------
    strongly_connected_components

    Notes
    -----
    The list is ordered from largest connected component to smallest.
    For undirected graphs only. 
    """
	if G.is_directed():
		raise nx.NetworkXError,\
              """Not allowed for directed graph G.
              Use UG=G.to_undirected() to create an undirected graph."""
	seen={}
	components=[]
	for v in G:      
		if v not in seen:
			c=nx.single_source_shortest_path_length(G,v)
			components.append(c.keys())
			seen.update(c)
	components.sort(key=len,reverse=True)            
	return components            


def number_connected_components(G):
	"""Return number of connected components in graph.

    Parameters
    ----------
    G : NetworkX Graph
       An undirected graph.

    Returns
    -------
    n : integer
       Number of connected components

    See Also       
    --------
    connected_components

    Notes
    -----
    For undirected graphs only. 
    """
	return len(connected_components(G))


def is_connected(G):
	"""Test graph connectivity

    Parameters
    ----------
    G : NetworkX Graph
       An undirected graph.

    Returns
    -------
    connected : bool
      True if the graph is connected, false otherwise.

    Examples
    --------
    >>> G=nx.path_graph(4)
    >>> print nx.is_connected(G)
    True

    See Also
    --------
    connected_components

    Notes
    -----
    For undirected graphs only. 
    """
	if G.is_directed():
		raise nx.NetworkXError(\
            """Not allowed for directed graph G.
Use UG=G.to_undirected() to create an undirected graph.""")

	if len(G)==0:
		raise nx.NetworkXPointlessConcept(
            """Connectivity is undefined for the null graph.""")

	return len(nx.single_source_shortest_path_length(G,
                                              G.nodes_iter().next()))==len(G)


def connected_component_subgraphs(G):
	"""Return connected components as subgraphs.

    Parameters
    ----------
    G : NetworkX Graph
       An undirected graph.

    Returns
    -------
    glist : list
      A list of graphs, one for each connected component of G.

    Examples
    --------
    Get largest connected component as subgraph

    >>> G=nx.path_graph(4)
    >>> G.add_edge(5,6)
    >>> H=nx.connected_component_subgraphs(G)[0]

    See Also
    --------
    connected_components

    Notes
    -----
    The list is ordered from largest connected component to smallest.
    For undirected graphs only. 
    """

	cc=connected_components(G)
	graph_list=[]
	
	for c in cc:
		graph_list.append(G.subgraph(c))
	return graph_list


#=======================
def node_connected_component(G,n):
	"""Return nodes in connected components of graph containing node n.

    Parameters
    ----------
    G : NetworkX Graph
       An undirected graph.

    n : node label       
       A node in G

    Returns
    -------
    comp : lists
       A list of nodes in component of G containing node n.

    See Also       
    --------
    connected_components

    Notes
    -----
    For undirected graphs only. 
    """
	if G.is_directed():
		raise nx.NetworkXError,\
              """Not allowed for directed graph G.
              Use UG=G.to_undirected() to create an undirected graph."""
	return nx.single_source_shortest_path_length(G,n).keys()
	
#=======================================================	
def generate_other_graphs(type, Nv, Ne, n_e, epsilon,p):

	if type==1:
		G=nx.barabasi_albert_graph(Nv,n_e)
	elif type==2:
		G=nx.erdos_renyi_graph(Nv,p)
	elif type==3:
		G=nx.watts_strogatz_graph(Nv,n_e,p)
	else:
		G=nx.random_lobster(Nv,p,p)

	Ncv = G.number_of_nodes()
	Nce = G.number_of_edges()
	C = array(list(G.degree(G.nodes()).values()))
	E = array(list(G.degree(G.edges()).values()))

	#--------------------
	ncomp = 2
	while 1==1:
		ncomp = number_connected_components(G)
		if ncomp==1: 
			print 'only',ncomp,'contected component'
			break
		comp = connected_components(G)
		#print comp
		print ncomp,'contected components'
	
	# connect two largest components through least connected nodes
		tmp1 = []
		tmp2 = []
		for t in comp[0]:
			if C[t]==min(C[comp[0]]):
				tmp1.append(t)
		for t in comp[1]:
			if C[t]==min(C[comp[1]]):
				tmp2.append(t)
		i1 = random.sample(tmp1,1)
		i2 = random.sample(tmp2,1)
		print 'Connecting',i1[0],'and',i2[0]
		G.add_edge(i1[0],i2[0])

	print G.number_of_nodes(), ' nodes'
	print G.number_of_edges(), ' edges'


	pos = nx.spring_layout(G, dim=2, pos=None, 
		fixed=None, iterations=4400, weighted=False, scale=1)

	visible = zeros([Nce,1],int)

	number = 0
	name  = 'test'
	dpidpi=150
	scale = 1./200.
	#draw_graph(C,E,visible,pos,name,number,dpidpi,Nce,Ncv,scale)
#---pickle output
#
	s = raw_input('Pickle the graph? (y/n):')

	if s=='y':
		file1 = open('graph1.pickled', 'w')
		pickle.dump(G,file1)
		pickle.dump(pos,file1)
		pickle.dump(C,file1)
		pickle.dump(E,file1)
		pickle.dump((Nce,Ncv),file1)
		file1.close()
#--------------------------------------
	return G,Ncv,Nce,C,E,pos
#---------------------------------------
#==================================================
def pick_edge_nodes(C, epsilon, Ncv, Nce, E, n_e):

	k = random.randint(1,n_e)
	Ncv+=1
	curr=Ncv
	tmp = []
	Cs = C.sum()
	
	for i in range(k):
		pick = random.random()
		
		cumsum = epsilon/(Cs+epsilon)
	
		# join new node	
		if cumsum > pick:
			Ncv+=1
			C[Ncv]+=1
			Nce+=1
			E[Nce,0]=curr
			E[Nce,1]=Ncv
			print '[',curr,Ncv,']'
			print '***************'
	
		else:
			j=-1
		# old node
			Cs = C.sum()
			while cumsum < pick and j<Ncv:
				j+=1
				cumsum += C[j]/(Cs+epsilon)
			
			if j not in tmp:
				Nce+=1
				E[Nce,0]=curr
				E[Nce,1]=j
				C[j]+=1
				tmp.append(j)
				print '{',curr,j,'}'


	C[Ncv]+=k
	Nce+=k
	
	return E,C,Ncv,Nce

#
#*******************************************
def generate_graph(Nv,Ne,epsilon,n_e):

	Ncv = -1
	Nce = 0

	C = zeros([Nv,1],int)
	E = zeros([Ne*3,2],int)
	G = nx.Graph()

	while Ncv < Nv-1:
		(E, C, Ncv, Nce) = pick_edge_nodes(C, epsilon, Ncv, Nce, E, n_e)
		print Ncv,Nce

	for i in range(Nce+1):
		G.add_nodes_from(E[i,:])
		G.add_edge(E[i,0],E[i,1])
	
	print G.number_of_nodes(), ' nodes'
	print G.number_of_edges(), ' edges'

	ncomp = 2
	while 1:
		ncomp = number_connected_components(G)
		if ncomp==1: 
			break
		comp = connected_components(G)
		#print comp
		print ncomp,'contected components'
	
	# connect two largest components through least connected nodes
		tmp1 = []
		tmp2 = []
		for t in comp[0]:
			if C[t]==min(C[comp[0]]):
				tmp1.append(t)
		for t in comp[1]:
			if C[t]==min(C[comp[1]]):
				tmp2.append(t)
		i1 = random.sample(tmp1,1)
		i2 = random.sample(tmp2,1)
		print 'Connecting',i1[0],'and',i2[0]
		G.add_edge(i1[0],i2[0])


	pos = nx.spring_layout(G, dim=2, pos=pos1, 
		fixed=None, iterations=15500, weighted=True, scale=1)

	visible = zeros([Nce,1],int)

	number = 0
	name  = 'test'
	dpidpi=150
	scale = 1./200.

#---pickle output
#
	s = raw_input('Pickle the graph? (y/n):')

	if s=='y':
		file1 = open('graph3.pickled', 'w')
		pickle.dump(G,file1)
		pickle.dump(pos,file1)
		pickle.dump(C,file1)
		pickle.dump(E,file1)
		pickle.dump((Nce,Ncv),file1)
		file1.close()
#----------------
	return G,Ncv,Nce,C,E,pos
#---------------------------------------


#---------------------------------------
def draw_current_graph(pos,name,number,dpidpi,Nce,Ncv,scale, G_current,
	                   alpha_i,alpha_m,beta,gamma,delta,step,loss,novel,
	                   edge_counts,page_rank,for_print, output_dir):
	fig = pyplot.figure(num=None,dpi=dpidpi,facecolor='k',edgecolor='w')
	rect = [0.1,0.13,0.8,0.77]
	ax = plt.subplot(111)

	ax.axis('off')
	rect = ax.patch
	rect.set_facecolor('k')

	xmin = inf
	xmax = -inf
	ymin = inf
	ymax = -inf
	part = 0.01
	
	for i in range(Ncv):
		x,y=pos[i]
		if x > xmax:
			xmax = x
		elif x < xmin:
			xmin = x
		if y > ymax:
			ymax = y
		elif y < ymin:
			ymin = y
			
	xlim(xmin-(xmax-xmin)*part,xmax+(xmax-xmin)*part)
	ylim(ymin-(ymax-ymin)*part,ymax+(ymax-ymin)*part)

	for i in range(Ncv):
		x,y=pos[i]
		r = sqrt(float(G.degree(i)))/pi
		circle = Circle((x, y), r*scale, facecolor='none',
                edgecolor=(0.4,0.4,0.4), linewidth=0.5, alpha=0.5)
		ax.add_patch(circle)
		
		
	# add current graph
	nmax = G.number_of_edges()
	cmap=get_cmap(hot)
	for ee in G_current.edges():
		#print ee
		x,y=pos[ee[0]]
		x1,y1=pos[ee[1]]
		
		if edge_counts[ee] <= 10:
			xx = (edge_counts[ee]-1.)/10.
		else:
			xx = 1.
			
		col=cm.jet(xx)
		al = min(1.,0.23+0.02*edge_counts[ee])
		lww = min(8.,1.1+0.01*edge_counts[ee])
		line = mlines.Line2D([x,x1], [y,y1], lw=lww, alpha=al, color=col)


		ax.add_line(line)
	
	#adding a key
	
	y_step = (ymax-ymin)*part*3.5
	y_start = ymin+10*y_step
	for i in range(11):
		col=cm.jet(i/10.)
		x = xmin-(xmax-xmin)*part*12
		x1 = xmin-(xmax-xmin)*part*12 + (xmax-xmin)*part/30.
		y = y_start-float(i)*y_step
		line = mlines.Line2D([x,x1], [y,y], lw=2, alpha=0.35, color=col)
		ax.add_line(line)
		if i < 10 and i > 0:
			tmp = '%d times ' % (i+1)
		elif i==0:
			tmp = '%d ' % (i+1)
		else:
			tmp = '%d or more' % (i+1)
		ax.text(x1+(xmax-xmin)*part/60.,y, tmp, fontname='Synchro LET', color=col, alpha=0.3) 


	if page_rank == 1:
		tmp_page = ' (Page Rank)'
	else:
		tmp_page = ' (Node Degree)'
		
	tmp = ('$\\alpha_{\\iota} = %4.2f, \\alpha_{\\mu} = %4.2f,\\beta = %4.2f, \\gamma = %4.2f,'+  \
	      '\\delta = %4.2f, \\Delta=%d \\;(%d)\\;%d$' + \
	      ' published'+tmp_page) % (alpha_i,alpha_m,beta,gamma, \
	                   delta,step,number,int(float(step)*float(number)))
	tmp1 = 'Novel %d (%d possible), Relative Loss %g, Loss %g' % (novel, nmax, loss/(novel+1.), loss)
	
	ax.text(xmin-(xmax-xmin)*part*8.,ymax+(ymax-ymin)*part*4, tmp, fontname='Synchro LET', color='k') 
	ax.text(xmin-(xmax-xmin)*part*8.,ymin-(ymax-ymin)*part*10, tmp1, fontname='Synchro LET', color='k') 
	
	ext = 'png'
	
	if for_print==True:
		savefig(name+str(number)+'.pdf', dpi=200, facecolor='w', edgecolor='w',
        	orientation='portrait', papertype=None, format='pdf',
        	transparent=False, bbox_inches=None, pad_inches=0.1)

	savefig(output_dir+'/'+name+str(number)+'.'+ext, dpi=dpidpi, facecolor='w', edgecolor='w',
        orientation='portrait', papertype=None, format=ext,
        transparent=False, bbox_inches=None, pad_inches=0.1)

	return 0

#====================================================================================
def pick_edges(G_curr, current_E, G, alpha_i, alpha_m, beta, gamm, 
	    delta, step, edge_counts, name, count, dpidpi, 
	    loss, novel, page_rank, k_max, reruns,
	    history, history_novel, history_loss, history_rel_loss, history_nodes, output_dir):
		
# compute shortest distances
	length=nx.all_pairs_shortest_path_length(G_curr)
	
	Nv = G_curr.number_of_nodes()
	rank = zeros([Nv,1],float)
	prob = zeros([Nv,Nv],float)
	
# compute ranks

	if page_rank==0:
		for ed in G_curr.edges():
			rank[ed[0]]+=1
			rank[ed[1]]+=1
	else:
		nodes = nx.pagerank_numpy(G_curr)
		minn=min(nodes.values())
		for no in nodes.keys():
			rank[no] = nodes[no].real/minn.real
		#print max(rank)[0], min(rank)[0]

	n_l_inf = 0.
	for i in range(Nv):
		n_l_inf += float(len(length[i]))/2.

	n_inf = Nv*(Nv-1)/2. - n_l_inf	
	nu = float(n_inf)**(delta+1.)/(float(n_inf)**(delta+1.)+float(n_l_inf))
		
	for i in range(Nv):
		for j in range(i+1,Nv-1):
		
			f = alpha_i*log(min([rank[i]+1,rank[j]+1])) +\
				alpha_m*log(max([rank[i]+1,rank[j]+1]))

			if j not in length[i]:	
				prob[i,j] = exp(log(nu) + f)
			else:
				g = beta*log(float(length[i][j])/float(k_max)) +\
					gamm*log(1. - float(length[i][j])/float(k_max))

				prob[i,j] = exp(log(1.-nu) + f + g) 
							

	summa=0.
	p_fail = 0.
	good = {}
	k=0
	p_success = zeros([G.number_of_edges(),1],float)
	for i in range(Nv):
		for j in range(i+1, Nv-1):
			summa+=prob[i,j]
			if j not in G[i]:
				p_fail += prob[i,j]
			else:
				p_success[k]=prob[i,j]
				good[k]=(i,j)
				k+=1
	
	prob = prob/summa
	p_fail = p_fail/summa
	
	p_success = p_success/(sum(p_success))
	
	failed = 0.
	success = 0
	
	while success < step:
	
		z = np.random.geometric(1.-p_fail, size=1)
		loss += z + 1
		failed += z[0]
		#print 'failed',z[0]
		
		pick0 = random.random()
		summ = 0.
		
		first=1
		for i in range(len(p_success)):
			summ+=p_success[i]
			if summ >= pick0:
				ni = good[i][0]
				nj = good[i][1]
				break
		i = ni
		j = nj
		if j not in G[i]:
			failed += 1
		else:
			if j not in G_curr[i]:
				G_curr.add_edge(i,j)
			edge_counts[(i,j)]+=1
			edge_counts[(j,i)]+=1
			success += 1
	novel=G_curr.number_of_edges()
	for_print = False 
	rel_loss = float(loss)/float(novel)
	
        #modify the original code to plot for every call of pick_edges()
        '''
	if reruns == 1:
		draw_current_graph(pos,name,count,dpidpi,Nce,Ncv,scale,G_curr,
			alpha_i, alpha_m, beta, gamma, delta, step, 
			loss, novel, edge_counts, page_rank,for_print)
	
	draw_current_graph(pos,name,count,dpidpi,Nce,Ncv,scale,G_curr,
			alpha_i, alpha_m, beta, gamma, delta, step, 
			loss, novel, edge_counts, page_rank,for_print, output_dir)
        '''
	for i in range(G.number_of_edges()):
		history[count-1,i] += edge_counts[G.edges()[i]]
		history_nodes[count-1,G.edges()[i][0]] += edge_counts[G.edges()[i]]
		history_nodes[count-1,G.edges()[i][1]] += edge_counts[G.edges()[i]]

	history_novel[count-1,0] += novel
	history_novel[count-1,1] += novel**2
	history_loss[count-1,0] += loss
	history_loss[count-1,1] += loss**2
	history_rel_loss[count-1,0] += rel_loss
	history_rel_loss[count-1,1] += rel_loss**2
	
	print string_wrap(str(rerun),2),count,string_wrap(str(G_curr.number_of_edges()),3),'novel, ',rel_loss,'relative loss'
	
	prob=[]
	
	
	return G_curr, current_E, failed, edge_counts, loss, novel, history, history_novel, history_loss, history_rel_loss
#===================================================================================
# vertices
# edges
# Nv
# Ne
# C -- connectivity
# E edges
Nv = 900
Ne = 2700
Ncv = -1
Nce = -1
epsilon = 0.5

#------- try reading pickled graph
s = 'y'

if s=='y':

	file2 = open('for_movies.pickled', 'r')
	G = pickle.load(file2)
	pos = pickle.load(file2)
	file2.close()
	
	C = array(list(G.degree(G.nodes()).values()))
	E = array(list(G.degree(G.edges()).values()))
	Nce = G.number_of_edges()
	Ncv = G.number_of_nodes()
	
#-----------------------------------------
step = 50
i=1
current_E=[]

alpha_i = 3.
alpha_m = 1.5
beta = 0.
gamma = 0.
delta = 0.0

page_rank=0


dpidpi=150
scale = 1/200.
nE = G.number_of_edges()
nV = G.number_of_nodes()
edge_counts = {}


version = 1
count = 0
i=1
name = 'universes'
k_max = nE



#=============================================================
reruns = 1
number_of_epochs = 10
history = zeros([number_of_epochs, nE], float)
history_novel = zeros([number_of_epochs, 2], float)
history_loss = zeros([number_of_epochs, 2], float)
history_rel_loss = zeros([number_of_epochs, 2], float)

history_nodes = zeros([number_of_epochs, nV], float)
G_curr = nx.MultiGraph()
G_curr.add_nodes_from(G)
loss = 0.
novel = 0.

#=============================================================
'''
if reruns == 1:
	draw_current_graph(pos,name,count,dpidpi,Nce,Ncv,scale,G_curr,
		alpha_i, alpha_m, beta, gamma, delta, step, 
		loss, novel, edge_counts, page_rank, False)

for rerun in range(reruns):
	for e in G.edges():
		edge_counts[(e[0],e[1])]=0.
		edge_counts[(e[1],e[0])]=0.	
		
	G_curr = nx.MultiGraph()
	G_curr.add_nodes_from(G)
	loss = 0.
	novel = 0.
	
	for epoch in range(number_of_epochs):
		(G_curr, current_E, failed, 
		edge_counts, loss, novel,
		history, history_novel, 
		history_loss, history_rel_los) = pick_edges(G_curr, current_E, G, 
								    alpha_i, alpha_m, beta, gamma, delta, step, 
		                            edge_counts, name, (epoch+1), dpidpi, loss, 
		                            novel, page_rank, k_max, reruns,
		                            history, history_novel, history_loss, 
		                            history_rel_loss, history_nodes)	
	if reruns == 1:
		file1 = open('histories.pickled-2', 'w')
		pickle.dump([rerun+1,reruns,number_of_epochs],file1)
		pickle.dump([alpha_i, alpha_m, beta, gamma, delta, step, page_rank],file1)
		pickle.dump(history,file1)
		pickle.dump(history_novel,file1)
		pickle.dump(history_loss,file1)
		pickle.dump(history_rel_loss,file1)		
		pickle.dump(history_nodes,file1)	
		
		file1.close()
'''
rerun = int(sys.argv[1])
outputdir = 'output/rerun'+str(rerun)
if not os.path.isdir(outputdir):
    os.makedirs(outputdir)

for e in G.edges():
    edge_counts[(e[0],e[1])]=0.
    edge_counts[(e[1],e[0])]=0.	
		
G_curr = nx.MultiGraph()
G_curr.add_nodes_from(G)
loss = 0.
novel = 0.

for epoch in range(number_of_epochs):
    (G_curr, current_E, failed, 
     edge_counts, loss, novel,
     history, history_novel, 
     history_loss, history_rel_los) = pick_edges(G_curr, current_E, G, 
                                                 alpha_i, alpha_m, beta, gamma, delta, step, 
                                                 edge_counts, name, (epoch+1), dpidpi, loss, 
                                                 novel, page_rank, k_max, rerun,
                                                 history, history_novel, history_loss, 
                                                 history_rel_loss, history_nodes, outputdir)	

file1 = open(outputdir+'/histories.pickled'+'-'+str(rerun), 'w')
pickle.dump([reruns,reruns,number_of_epochs],file1)
pickle.dump([alpha_i, alpha_m, beta, gamma, delta, step, page_rank],file1)
pickle.dump(history,file1)
pickle.dump(history_novel,file1)
pickle.dump(history_loss,file1)
pickle.dump(history_rel_loss,file1)		
pickle.dump(history_nodes,file1)	
file1.close()

        
print 'Done!'


	
