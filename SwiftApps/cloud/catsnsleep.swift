type file;
type script;
type data;

app (file o) catnap (script _catnap, string _delay, data _i){
  bash @_catnap _delay @_i stdout=@o;
}

string delay=@arg("s","1");

data datafile<"data.txt">;
script scriptfile<"catnap.sh">;

file out[]<simple_mapper; location="outdir", prefix="f.",suffix=".out">;

foreach j in [1:@toint(@arg("n","1"))] {
  out[j] = catnap(scriptfile, delay, datafile);
}

