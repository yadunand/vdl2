
// some parameters

int badmonkeycount = 0;
int goodmonkeycount = 3600;

type file;

(file b) badmonkey(file a) {
  app {
    badmonkey @a @b;
  }
}

(file b) goodmonkey(file a) {
  app {
    goodmonkey @a @b;
  }
}

file inputfile <"in.txt">;
file intermediatefile = goodmonkey(inputfile);

file g[] <simple_mapper;prefix="outg.",suffix=".txt">;

file b[] <simple_mapper;prefix="outb.",suffix=".txt">;

foreach ix in [0:goodmonkeycount-1] {
  g[ix] = goodmonkey(intermediatefile);
}

foreach ix in [0:badmonkeycount-1] {
  b[ix] = badmonkey(intermediatefile);
}

