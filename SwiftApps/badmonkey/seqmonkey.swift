
// some parameters

int goodmonkeycount = 3600;

type file;

(file b) badmonkey(file a) {
  app {
    badmonkey @a @b;
  }
}

(file b) goodmonkey(file a) {
  app {
    goodmonkey @a @b;
  }
}

file inputfile <"in.txt">;
file intermediatefile = goodmonkey(inputfile);

file g[] <simple_mapper;prefix="outg.",suffix=".txt">;

file b[] <simple_mapper;prefix="outb.",suffix=".txt">;

iterate ix {

  g[ix] = goodmonkey(intermediatefile);
} until(ix>goodmonkeycount);

