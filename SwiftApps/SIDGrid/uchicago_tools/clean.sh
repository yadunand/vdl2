#!/bin/sh

# should be run in the user's current swift job directory 
# will commit logs to the repository and remove run files 

rsync --archive --ignore-existing *.log *.d login.ci.uchicago.edu:/home/benc/swift-logs/$(whoami)/ --verbose
rm *log
rm *kml
rm -rf *.d

