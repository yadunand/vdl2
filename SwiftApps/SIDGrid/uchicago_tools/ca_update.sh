#!/bin/bash

export CNARI=/disks/ci-gpfs/fmri/cnari
mv $CNARI/swift/ca $CNARI/swift/ca_old
mkdir $CNARI/swift/ca
echo "ncsa mercury"
scp login-hg.ncsa.teragrid.org:/etc/grid-security/certificates/* $CNARI/swift/ca
echo "ncsa done......."
echo "ranger"
scp tg457040@tg-login.ranger.tacc.teragrid.org:/etc/grid-security/certificates/* $CNARI/swift/ca
echo "ranger done....."
echo "bigred"
scp tg-skenn@login.bigred.iu.teragrid.org:/etc/grid-security/certificates/* $CNARI/swift/ca
echo "bigred done......"
echo "lonestar" 
scp tg457040@tg-login.lonestar.tacc.teragrid.org:/etc/grid-security/certificates/* $CNARI/swift/ca
echo "lonestar done...."
echo "ucanl32"
scp tg-viz-login.uc.teragrid.org:/etc/grid-security/certificates/* $CNARI/swift/ca
echo "ucanl32 done...."
