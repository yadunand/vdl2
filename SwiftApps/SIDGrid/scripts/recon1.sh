#!/bin/bash

# $1 == user
# $2 == subject
# $3 == input image

####export FREESURFER_HOME=/soft/community/SIDGrid/freesurfer

echo "freesurfer home is:::::"
echo $FREESURFER_HOME

export SUBJECTS_DIR=$PWD
source $FREESURFER_HOME/SetUpFreeSurfer.sh

mkdir -p $2/mri/transforms
mkdir $2/label
mkdir $2/stats
mkdir $2/surf
mkdir $2/scripts
mkdir $2/mri/orig

echo "finished making directory tree, about to do converts"
echo "mri_convert $3 $1/$2/mri/rawavg.mgz..........."
echo "mri_convert $1/$2/mri/rawavg.mgz $1/$2/mri/orig.mgz
--conform........"

mri_convert $3 $2/mri/rawavg.mgz
mri_convert $2/mri/rawavg.mgz $2/mri/orig.mgz --conform

recon-all -s $2 -nuintensitycor
recon-all -s $2 -talairach
recon-all -s $2 -normalization
recon-all -s $2 -skullstrip

## end recon1 proper, begin recon2 both hemis

recon-all -s $2 -gcareg
recon-all -s $2 -canorm
recon-all -s $2 -careg
recon-all -s $2 -rmneck
recon-all -s $2 -skull-lta
recon-all -s $2 -calabel
recon-all -s $2 -segstats
recon-all -s $2 -normalization2
recon-all -s $2 -segmentation
recon-all -s $2 -fill
recon-all -s $2 -tessellate

tar cf $2.tar $2 

