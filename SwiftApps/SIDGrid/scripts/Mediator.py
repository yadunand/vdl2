#!/usr/bin/env python

import MySQLdb
import sys
import os


#..............................................................................

class Mediator:

    #..........................................................................

    def __init__(self):
        self.user = ""
        self.conf = ""
        self.db_name = ""
        self.host = ""
        self.query = ""
        self.subquery = ""
        self.r_swift_args = ""
        self.r_script = ""
        self.r_infile = ""
        self.begin_vox = "1"
        self.end_vox = "2"
        self.begin_ts = "-1"
        self.end_ts = "-1"
        self.vox = -1
        self.subject = ""
        self.batchstep = "1"
        self.helpmsg = ""
        self.outprefix = "defaultoutname"
        self.all_queries = []
        self.qout_r_args = []
        self.g_db = None
        self.voxfile = "false"

    #..........................................................................    

    def	run (self,query):
        self.db.query ( query )
	res = self.db.use_result ()
	rows = []
	while ( res ):
	      cols = []
	      row = res.fetch_row  ()

	      if  len(row) == 0:
		  break;
	
	      for i in range(len(row[0])):
		  cols.append ( row[0][i] )
		  
	      rows.append(cols)
    	
	
	return rows
        
 
    #..........................................................................
      
    def connect(self):
        print "connecting....."
        if self.g_db == None:
          try:
              self.g_db = MySQLdb.connect( self.host , self.user , self.passwd , self.db_name );

          except MySQLdb.Error,e:
              print 'Fatal Error ... '
              print 'Error connecting to database ...\n', e.args[0], e.args[1]
              sys.exit (-1)

        self.db = self.g_db
        print "connected"
        self.run( "set autocommit=1" ) 

    #..........................................................................


    def get_opts (self,allargstr):
        if len(allargstr)<5:
            argstr = allargstr[1].split(' ')
        else:
            argstr = allargstr
        print "length of argstr "+str(len(argstr))
        i = 0
        for o in argstr:
            print "arg is "+o
            if o == "--db":
                self.db_name = argstr[i+1]
            elif o == "--user":
                self.user = argstr[i+1]
            elif o == "--conf":
                self.conf = argstr[i+1]
            elif o == "--host":
                self.host = argstr[i+1]
            elif o == "--query":
                qstr = ""
                j = i
                while (argstr[j+1].find("--")<=-1):
                    qstr = qstr+" "+argstr[j+1].strip()
                    j+=1
                self.query = qstr
            elif o == "--r_swift_args":
                self.r_swift_args = argstr[i+1]
            elif o == "--r_script":
                self.r_script = argstr[i+1]
            elif o == "--r_infile":
                self.r_infile = argstr[i+1]
            elif o == "--begin_vox":
                self.begin_vox = argstr[i+1]
            elif o == "--end_vox":
                self.end_vox = argstr[i+1]
            elif o == "--subject":
                self.subject = argstr[i+1]
            elif o == "--batchstep":
                self.batchstep = argstr[i+1]
            elif o == "--outprefix":
                self.outprefix = argstr[i+1]
            elif o == "--begin_ts":
                self.begin_ts = argstr[i+1]
            elif o == "--end_ts":
                self.end_ts = argstr[i+1]
            elif o == "--vox_file":
                self.voxfile = argstr[i+1]
            elif o == "--vox":
                self.vox = argstr[i+1]
            elif o == "--subquery":
                self.subquery = argstr[i+1]
            elif (o.find("--")>-1):
                print "o is this "+o
                print "and i is this "+str(i)
                print self.gethelp(o)
                sys.exit()
            i = i+1        
                
    #...................................................................

    def gethelp(self,option):
        helpmsg = """
        Unknown Option: """+option+"""
        usage:  --db --user --conf --host --query --r_swift_args --rscript --r_infile --begin_vox --end_vox --subject --batchstep --outprefix --vox_file --vox --begin_ts --end_ts --subquery
        """
        return helpmsg
        
    #...................................................................
    
    def load (self,dict): 
        dict = {}
        fd = open ( self.conf, "r" )
        while 1:
            line = fd.readline ()
            if not line		      : break
            if line[0] == "#"	      : continue
            if len(line.split("=")) < 2 : continue 
            
            key	= line.split('=') [0]
            value = line.split('=') [1]
            
            # dict [key.strip()]=value.strip()
            
        fd.close ()
        return value
    #.....................................................................

    def gen_queries(self):
        if(self.vox > -1):
            tmp = self.query.replace("VOX",str(self.vox))
            tmp2 = tmp.replace("SUBJECT", self.subject)
            if(self.begin_ts < 0):
                self.all_queries.append(tmp2)
                self.qout_r_args.append(str(self.vox))
            else:
                self.query = tmp
        if(self.begin_ts > -1):
            bgn = int(self.begin_ts)
            end = int(self.end_ts)
            querystr = ""
            for h in range(bgn,end):
                tmp = self.subquery.replace("TSVAR", str(h))
                querystr = querystr+tmp+", "
            querystr = querystr+self.subquery.replace("TSVAR", str(end))
            querystr = self.query.replace("SUBQUERY", querystr)
            self.all_queries.append(querystr)
            self.qout_r_args.append("NULL")
                
        else:
            bgn = int(self.begin_vox)
            end = int(self.end_vox)
            stp = int(self.batchstep)
            for i in range(bgn,end,stp):
                beginbatch = i
                endbatch = i+stp-1
                tmp = self.query.replace("BEGIN_BATCH",str(beginbatch))
                tmp2= tmp.replace("END_BATCH",str(endbatch))
                tmp3= tmp2.replace("SUBJECT", self.subject)
                self.all_queries.append(tmp3)
                self.qout_r_args.append(str(beginbatch)+"_"+str(endbatch))
        pass

    #.......................................................................

    def gen_queries_noncontig(self):
        allqueries = []
        query = self.query
        qcount = 0
        voxfname = self.voxfile
        voxfile = open ( voxfname, "r" )
        for line in voxfile.readlines ():
            tmp = query.replace("VOX_BATCH", "("+line.strip()+");")
            self.all_queries.append(tmp)
            self.qout_r_args.append(str(qcount)+"_")
            qcount = qcount + 1 
        voxfile.close()

    #.......................................................................

    def run_query(self, query):
        print "RUNNING QUERY: "+query
        self.passwd = self.load('PASSWORD')
        self.passwd = self.passwd.strip()
        self.connect()
        all_res = self.run(query)
     
        cols = []
        rows = []
        urg = []
        
        n_cols = len(all_res[0])
        n_rows = len(all_res)
        
        outfilename = self.subject+".result"
        self.r_infile = outfilename
        outfile = open(outfilename, "w")

        for i in range(n_rows):
            rowstr = ""
            for j in range(n_cols):
                rowstr = rowstr+str(all_res[i][j])+"\t"
            outfile.write(rowstr+"\n")
        outfile.close()

    #...................................................................

    def run_r(self, batchnum):
        # TODO check that all exist
        os.environ['R_SWIFT_ARGS'] = self.qout_r_args[batchnum]+" "+self.r_swift_args
        os.environ['R_INPUT'] = str(self.r_infile)
        os.system('R CMD BATCH --vanilla '+self.r_script)
        pass

    #...................................................................

    def tar_out(self):
        tarstr = "tar -cf "+self.outprefix+".tar *"
        os.system(tarstr)

#......................MAIN...........................................

med = Mediator ()
med.get_opts(sys.argv)
if med.voxfile == "false":
    med.gen_queries()
else:
    med.gen_queries_noncontig()
n=0
for q in med.all_queries:
    med.run_query(q)
    med.run_r(n)
    n=n+1 
med.tar_out()
