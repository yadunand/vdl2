#!/bin/bash

# $1 == usr
# $2 == subj
# $3 == <subj>L.tar
# $4 == <subj>R.tar

tar -xf $3
tar --keep-newer-files -xf $4

echo "untarred and combined hemi directories. running aparc2aseg......."

export SUBJECTS_DIR=$PWD
source $FREESURFER_HOME/SetUpFreeSurfer.sh

recon-all -s $2 -aparc2aseg

tar cf $2COMPLETE.tar $2


