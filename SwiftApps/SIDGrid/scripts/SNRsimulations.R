allinputs <- Sys.getenv("R_SWIFT_ARGS")
inputfilename <- noquote(strsplit(allinputs," ")[[1]][1])
start_num <- as.numeric(noquote(strsplit(allinputs," ")[[1]][2]))
iter <- 1/start_num
tval <- as.numeric(noquote(strsplit(allinputs," ")[[1]][3]))
outname <- paste("results/iter",start_num,"_tval",tval,"_forSNR.txt",sep="")
print(iter)
print(tval)
print(outname)

data <- read.table(inputfilename);
c <- rowSums(data);
#cnorm <- (c/10000)+1  # this is for modeling 1 %
cnorm <- (c/20000)+1  # this is for modeling 0.5 %

prob_result <- c();
tresult <- c();
simulations <- 10000;

for (i in 1:simulations){
    #---- 1/SNR = sd for rnorm. e.g., 1/5 SNR = .2. 1/10 SNR = .1.
    w <- rnorm(200, 0, iter) # 200 N(0,1) variates
    v = filter(w, sides=2, rep(1,3)/3) # moving average
    noise.c <- (cnorm+v)
    gfit <- lm(noise.c~data[,1]+data[,2])    
    currentT <- as.numeric(summary(gfit)$coefficients[8])
    tresult <- c(tresult,currentT)
}
prob <- (length(which(tresult > tval)))/simulations
prob_result <- t(c(iter,prob))
write.table(prob_result,outname,row.names=FALSE,col.names=FALSE)
