#!/bin/bash

# $1 == user
# $2 == subject
# $3 == <subject>.tar

echo "untarring subject dir.................."
echo $3

tar -xf $3


echo "freesurfer home is:::::"
echo $FREESURFER_HOME

export SUBJECTS_DIR=$PWD
source $FREESURFER_HOME/SetUpFreeSurfer.sh

## end recon1 proper, begin recon2 both hemis

recon-all -s $2 -gcareg
recon-all -s $2 -canorm
recon-all -s $2 -careg
recon-all -s $2 -rmneck
recon-all -s $2 -skull-lta
recon-all -s $2 -calabel
recon-all -s $2 -segstats
recon-all -s $2 -normalization2
recon-all -s $2 -segmentation
recon-all -s $2 -fill
recon-all -s $2 -tessellate

tar cf $3 $2 
cp $3 results/

