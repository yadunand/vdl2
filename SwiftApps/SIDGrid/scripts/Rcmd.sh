#!/bin/bash

# arg 1 is the name of the inputfile
# arg 2 is the perm matrix
# arg 3 is the perm number
# arg 4 is name of the output file
# arg 5 is the name of the script

echo "***************in perm, checking rlibs, RLIBS = $R_LIBS"

echo "R CMD BATCH '--no-restore --args $1 $2 $3 $4' $5"

echo "R CMD BATCH '--no-restore --args $1 $2 $3 $4' $5" | sh
