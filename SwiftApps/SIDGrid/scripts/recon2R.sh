#!/bin/bash

# $1 == user
# $2 == subject
# $3 == <subject>.tar

echo "untarring.................."

tar -xf $3

####export FREESURFER_HOME=/soft/community/SIDGrid/freesurfer

echo "freesurfer home is :::::"
echo $FREESURFER_HOME

export SUBJECTS_DIR=$PWD
source $FREESURFER_HOME/SetUpFreeSurfer.sh

recon-all -s $2 -smooth1 -hemi rh
recon-all -s $2 -inflate1 -hemi rh
recon-all -s $2 -qsphere -hemi rh
recon-all -s $2 -fix -hemi rh
recon-all -s $2 -finalsurfs -hemi rh
recon-all -s $2 -smooth2 -hemi rh
recon-all -s $2 -inflate2 -hemi rh
recon-all -s $2 -cortribbon -hemi rh
recon-all -s $2 -sphere -hemi rh
echo "running: mris_register -dist 5 -curv $SUBJECTS_DIR/$2/surf/rh.sphere ${FREESURFER_HOME}/average/rh.average.curvature.filled.buckner40.tif $SUBJECTS_DIR/$2/surf/rh.sphere.reg"
mris_register -dist 5 -curv $SUBJECTS_DIR/$2/surf/rh.sphere ${FREESURFER_HOME}/average/rh.average.curvature.filled.buckner40.tif $SUBJECTS_DIR/$2/surf/rh.sphere.reg
echo "running: mris_register -dist 5 -curv -reverse $SUBJECTS_DIR/$2/surf/rh.sphere ${FREESURFER_HOME}/average/rh.average.curvature.filled.buckner40.tif $SUBJECTS_DIR/$2/surf/lh.rh.sphere.reg"
mris_register -dist 5 -curv -reverse $SUBJECTS_DIR/$2/surf/rh.sphere ${FREESURFER_HOME}/average/rh.average.curvature.filled.buckner40.tif $SUBJECTS_DIR/$2/surf/lh.rh.sphere.reg
#recon-all -s $2 -surfreg -hemi rh
#recon-all -s $2 -contrasurfreg -hemi rh
recon-all -s $2 -avgcurv -hemi rh
recon-all -s $2 -cortparc -hemi rh
recon-all -s $2 -parcstats -hemi rh
recon-all -s $2 -cortparc2 -hemi rh
recon-all -s $2 -parcstats2 -hemi rh

tar cf $2R.tar $2
 

