#!/bin/bash

# $1 == user
# $2 == subject
# $3 == <subject>.tar

echo "untarring.................."

tar -xf $3

####export FREESURFER_HOME=/soft/community/SIDGrid/freesurfer

echo "freesurfer home is :::::"
echo $FREESURFER_HOME

export SUBJECTS_DIR=$PWD
source $FREESURFER_HOME/SetUpFreeSurfer.sh

recon-all -s $2 -smooth1 -hemi lh
recon-all -s $2 -inflate1 -hemi lh
recon-all -s $2 -qsphere -hemi lh
recon-all -s $2 -fix -hemi lh
recon-all -s $2 -finalsurfs -hemi lh
recon-all -s $2 -smooth2 -hemi lh
recon-all -s $2 -inflate2 -hemi lh
recon-all -s $2 -cortribbon -hemi lh
recon-all -s $2 -sphere -hemi lh
echo "running: mris_register -dist 5 -curv $SUBJECTS_DIR/$2/surf/lh.sphere ${FREESURFER_HOME}/average/lh.average.curvature.filled.buckner40.tif $SUBJECTS_DIR/$2/surf/lh.sphere.reg"
mris_register -dist 5 -curv $SUBJECTS_DIR/$2/surf/lh.sphere ${FREESURFER_HOME}/average/lh.average.curvature.filled.buckner40.tif $SUBJECTS_DIR/$2/surf/lh.sphere.reg
echo "running: mris_register -dist 5 -curv -reverse $SUBJECTS_DIR/$2/surf/lh.sphere ${FREESURFER_HOME}/average/lh.average.curvature.filled.buckner40.tif $SUBJECTS_DIR/$2/surf/rh.lh.sphere.reg"
mris_register -dist 5 -curv -reverse $SUBJECTS_DIR/$2/surf/lh.sphere ${FREESURFER_HOME}/average/lh.average.curvature.filled.buckner40.tif $SUBJECTS_DIR/$2/surf/rh.lh.sphere.reg
#recon-all -s $2 -surfreg -hemi lh
#recon-all -s $2 -contrasurfreg -hemi lh
recon-all -s $2 -avgcurv -hemi lh
recon-all -s $2 -cortparc -hemi lh
recon-all -s $2 -parcstats -hemi lh
recon-all -s $2 -cortparc2 -hemi lh
recon-all -s $2 -parcstats2 -hemi lh

tar cf $2L.tar $2
 

