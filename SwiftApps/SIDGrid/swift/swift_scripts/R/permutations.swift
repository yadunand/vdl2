# for running the script permutations.R
# swift will give R the permutations number
# and the matrices for running the t-test
# and will produce a file <permnumber>.dat

type file{}

(file rout) perm_r (file infilename, file pmatrix, int pnum, string outfilename, file scriptname)
	{
	app
		{
                Rcmd  @filename(infilename) @filename(pmatrix) pnum outfilename @filename(scriptname);
		}
	}

string user = @arg("user");

## the .R script is hardcoded here to use the version which 
## will take the commandline arg for the permutation number

file r_script<single_file_mapper; file="/disks/gpfs/fmri/cnari/scripts/permutations.R">;


file perm_matrix<single_file_mapper; file="reduced.perm.matrix">;
file ip<single_file_mapper; file="gestureB.lh.forperm.txt">;


foreach i in [1:10]
	{
	file r_out <single_file_mapper; file=@strcat(i,".dat")>;
	r_out = perm_r(ip, perm_matrix, i, @strcat(i,".dat"), r_script);
	}


	
