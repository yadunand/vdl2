type file{}

(file pdfFile, file resA, file resB) permScript (file scriptFile, file tsfile, file preprocR, file peakfitR){
	app{
		RPermInvoke @filename(scriptFile)  @filename(tsfile)  @filename(pdfFile) @filename(resA) @filename(resB);
	}
}


genPerms(){
	file script<"scripts/Shellpeak.R">;
	file preprocR<"inputs/preprocess.Enewsmooth.R">;
	file peakfitR<"inputs/peakfit_2.1.R">;
	
	int inputFileNo[] = [57:70];
        foreach file in inputFileNo {
                string inputName=@strcat("inputs/ts.",file);
		string outputPDFName=@strcat("result.",file,".pdf");
		string outputAName=@strcat("result.",file,".a");
		string outputBName=@strcat("result.",file,".b");

		file inFile<single_file_mapper; file=inputName>;
		file outPDFFile<single_file_mapper; file=outputPDFName>;
		file outAFile<single_file_mapper; file=outputAName>;
		file outBFile<single_file_mapper; file=outputBName>;
                (outPDFFile,outAFile,outBFile)=permScript(script,inFile, preprocR, peakfitR);
        }
}

genPerms();
