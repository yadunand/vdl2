#--- this is for the GAproject surface projections
#--- testing SUMA obj
type file{}

type AFNI_obj{
    file HEAD;
    file BRIK;
}

type SUMA_obj{
    file smoothwm;
    file pial;
}

app (file out_1d) AFNI_Vol2Surf (file spec_file, SUMA_obj surface, string map, AFNI_obj surfvol, AFNI_obj grid_parent){
    AFNI_3dVol2Surf "-spec" @spec_file "-surf_A" @surface.smoothwm "-surf_B" @surface.pial
    "-map_func" map "-f_steps" 15 "-f_index" "voxels" "-oob_index" "-1" "-oob_value" 0 "-no_headers" "-outcols_NSD_format"
    "-sv" @surfvol.BRIK "-grid_parent" @grid_parent.BRIK "-out_1D" @filename(out_1d);
}


string mapFunctions[] = ["ave"];
string hemis[] = ["lh","rh"];
string conditions[] = ["speech"];

foreach map in mapFunctions{
    foreach h in hemis{
        foreach cc in conditions{
            file spec_file<single_file_mapper; file=@strcat("inputs/011_",h,".spec")>;
            SUMA_obj surface<simple_mapper; location="inputs", prefix=@strcat(h,"."), suffix=".asc">;
            AFNI_obj surfvol<simple_mapper; location="inputs", prefix=@strcat("011_SurfVol_Alnd_Exp+orig.")>;
            AFNI_obj grid_parent<simple_mapper; location="inputs", prefix=@strcat(cc,"InclusiveCrit+orig.")>;
            file output1D<single_file_mapper; file=@strcat("outputsTEST/s011_",h,"_",cc,"InclusiveCrit",map,".1D")>;
            output1D = AFNI_Vol2Surf(spec_file, surface, map, surfvol, grid_parent);
        }
    }
}

