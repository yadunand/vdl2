#---- pinche deconvolution
type file{}

type AFNI_obj{
    file HEAD;
    file BRIK;
}

type stims{
    string labels[];
    string fnames[];
}

app (AFNI_obj deconvBucketResult, AFNI_obj irfResult, file deconvXsave) AFNI_pincheDeconvolution (AFNI_obj normalized, stims stimuli, file censor, file concat, file motionfile, file designmatrix, string outPrefix){
    AFNI_3dDeconvolve "-input" @filename(normalized.BRIK) "-polort" 3 "-tshift" "-num_stimts" 10
    "-stim_file" 1 stimuli.fnames[0] "-stim_label" 1 stimuli.labels[0]
    "-stim_file" 2 stimuli.fnames[1] "-stim_label" 2 stimuli.labels[1]
    "-stim_file" 3 stimuli.fnames[2] "-stim_label" 3 stimuli.labels[2]
    "-stim_file" 4 stimuli.fnames[3] "-stim_label" 4 stimuli.labels[3]
    "-stim_file" 5 stimuli.fnames[4] "-stim_base" 5 "-stim_label" 5 stimuli.labels[4]
    "-stim_file" 6 stimuli.fnames[5] "-stim_base" 6 "-stim_label" 6 stimuli.labels[5]
    "-stim_file" 7 stimuli.fnames[6] "-stim_base" 7 "-stim_label" 7 stimuli.labels[6]
    "-stim_file" 8 stimuli.fnames[7] "-stim_base" 8 "-stim_label" 8 stimuli.labels[7]
    "-stim_file" 9 stimuli.fnames[8] "-stim_base" 9 "-stim_label" 9 stimuli.labels[8]
    "-stim_file" 10 stimuli.fnames[9] "-stim_base" 10 "-stim_label" 10 stimuli.labels[9]
    "-stim_maxlag" 1 11
    "-stim_maxlag" 2 11
    "-stim_maxlag" 3 11
    "-stim_maxlag" 4 11
    "-censor" @filename(censor)
    "-iresp" 1 @strcat(outPrefix,".IRF")
    "-full_first" "-tout" "-nodmbase" "-xsave" "-nfirst" "0" "-concat" @filename(concat) "-bucket"
    outPrefix;
}


string declarelist[] = ["PK2"];
foreach subject in declarelist{
    ## map inputs
    file designmatrix<single_file_mapper; file="for3dDecon.PK2_4condSHORTcat_all.1D">;
    file motionfile<single_file_mapper; file=@strcat("motion_",subject,".1D")>;
    file concat<single_file_mapper; file=@strcat("concat_runs",subject,".1D")>;
    file censor<single_file_mapper; file=@strcat("motion_",subject,"_censor.1D")>;
    AFNI_obj normalized<simple_mapper; prefix=@strcat("TScat_nrmlzdPerChng.",subject,"+orig.")>;

    ## map outputs
    string outPrefix = @strcat("DeconvResults_",subject);
    file xsave<single_file_mapper; file=@strcat(outPrefix,".xsave")>;
    AFNI_obj bucket<simple_mapper; prefix=@strcat(outPrefix,"+orig.")>;
    AFNI_obj irf<simple_mapper; prefix=@strcat(outPrefix,".IRF+orig.")>;

    #---- stimuli info
    stims stimuli;
    stimuli.labels = ["still_pa","move_pa","still_noise","move_noise","roll","pitch","yaw","dx","dy","dz"];
    stimuli.fnames = [@strcat(@filename(designmatrix),"[0]"),@strcat(@filename(designmatrix),"[1]"),
            @strcat(@filename(designmatrix),"[2]"),@strcat(@filename(designmatrix),"[3]"),
            @strcat(@filename(motionfile),"[1]"),@strcat(@filename(motionfile),"[2]"),
            @strcat(@filename(motionfile),"[3]"),@strcat(@filename(motionfile),"[4]"),
            @strcat(@filename(motionfile),"[5]"),@strcat(@filename(motionfile),"[6]")];

    (bucket, irf, xsave) = AFNI_pincheDeconvolution(normalized, stimuli, censor, concat, motionfile, designmatrix, outPrefix);
}
