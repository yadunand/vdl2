## this is a script for running queries and passing the result to R
## for analysis (via Mediator.py). 
## all of the swift variables should be given your own values
## for a query. in the 'query_outline', BEGIN_BATCH, END_BATCH
## and SUBJECT will be filled-in on the remote site by Mediator.py
## which will loop through and fill them in for each run based on the 
## voxel range and step size you provide.

type file{}

(file qout, file rout) run_query (string allcatargs, file config, file dot_r)
	{
	app
		{
                Mediator allcatargs stdout=@filename(qout) @filename(dot_r);
		}
	}

string user = @arg("user");
string db = "EMBLEM1";
string host = "tp-neurodb.ci.uchicago.edu";

string query_outline = "select gesture4B, gesture5B, gesture6B from emblemfemlh where voxel between BEGIN_BATCH and END_BATCH and subject=SUBJECT";

file r_script<single_file_mapper; file="readInput.R">;
file config<single_file_mapper; file="user.config">;

string group_begin_vox = "1";
string group_end_vox = "5000";
string group_batchstep = "1000";

string subj_ids = ["3","5","6","7","8","10","11","12","13","14","15"];
foreach id,i in subj_ids {
	string r_swift_args = @strcat("subj",id);
	string med_args = @strcat("--user ", user,
			  " --conf ", @filename(config),
			  " --db ", db,
			  " --host ", host,
			  " --query ", query_outline,
			  " --r_script ", @filename(r_script),
			  " --begin_vox ", group_begin_vox,
			  " --end_vox ", group_end_vox,
			  " --subject ", id,
			  " --batchstep ", "1000",
			  " --r_swift_args ", r_swift_args,
			  " --subject ", id);
	file q_result <single_file_mapper; file=@strcat(id,".qresult")>;
	file r_result <single_file_mapper; file=@strcat(id, ".tar")>;
	(q_result, r_result) = run_query(med_args, r_script, config);
}


	
