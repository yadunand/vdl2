#---- coded 08.August.2009
type file{}
type Rscript;
type PermAnaly{
    Rscript R;
    file Rdata;
}

app (file rout) run_query (string allcatargs, file config, PermAnaly code){
    Mediator allcatargs @code.R;
}


loop_query(int bvox, string user, string db, string host, string query_outline, PermAnaly code, file config, string id, string h, int perm, int batchstep){
    int evox = bvox+(batchstep-1);
    string outName = @strcat("permout",h,"/perm",perm,"_",h,bvox,"_",evox);
    string med_args = @strcat("--user ","andric",
        " --conf ", @filename(config),
        " --db ", db,
        " --host ", host,
        " --query ", query_outline,
        " --r_script ", @filename(code.R),
        " --begin_vox ", bvox,
        " --end_vox ", evox,
        " --outprefix ", "TOLDJA",
        " --batchstep ", batchstep,
        " --r_swift_args ", outName," ",perm,
        " --subject ", id);
    file r_result <single_file_mapper; file=@strcat(outName,".txt")>;
    r_result = run_query(med_args, config, code);
}

string user = @arg("user");
string db = "EMBLEM1";
string host = "tp-neurodb.ci.uchicago.edu";
file config<single_file_mapper; file="user.config">;

PermAnaly code<simple_mapper; location="PermScripts", prefix="FriedmanPerm.">;

string hemilist[] = ["lh"];
int batchstep = 7000;
int permbrains[] = [1:2000:1];
int mybatches[] = [1:196000:batchstep];
string id = "perminterp";

foreach h in hemilist{
    foreach perm in permbrains{
        foreach batch in mybatches{
            string query_outline = @strcat("SELECT subject, vertex, speech_lag, emblem_lag, embspeech_lag FROM interpCCF_",h," WHERE vertex BETWEEN BEGIN_BATCH and END_BATCH");
            loop_query(batch, user, db, host, query_outline, code, config, id, h, perm, batchstep);
        }
    }
}
