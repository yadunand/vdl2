#---- Friedman Test summer 2009. re-doing with interpolated data
## type declarations
type file{}
type Rscript;

## Mediator app declaration
app (file qout, file rout) run_query (string allcatargs, file config, Rscript code){
    Mediator allcatargs stdout=@filename(qout) @filename(code);
}


loop_query(int bvox, string user, string db, string host, string query_outline, Rscript code, file config, string id, string h){
    int evox = bvox+499;
    string outName = @strcat("output",h,"/",h,id,bvox,"_",evox);
    string med_args = @strcat("--user ","andric"," --conf ", @filename(config)," --db ", db," --host ", host,
        " --query ", query_outline," --begin_vox ", bvox," --end_vox ", evox," --batchstep ", "500",
        " --r_script ", @filename(code)," --r_swift_args ",outName," --outprefix ", "TOLDJA",
        " --subject ", id);
    file q_result <single_file_mapper; file=@strcat("Qresults/",h,id,bvox,".qresult")>;
    file r_result <single_file_mapper; file=@strcat(outName,".txt")>;
    (q_result, r_result) = run_query(med_args, config, code);
}

## parameters Mediator needs
string user = @arg("user");
string db = "EMBLEM1";
string host = "tp-neurodb.ci.uchicago.edu";
file config<single_file_mapper; file="user.config">;

## mapping the R code
Rscript code<single_file_mapper; file="scripts/FriedmanTest.R">;

## variables to move across
string hemilist[] = ["rh"];
int mybatches[] = [1:196000:500];
string id = "interp";

foreach h in hemilist{
    foreach batch in mybatches {
        string query_outline = @strcat("SELECT subject, vertex, speech_lag, emblem_lag, embspeech_lag FROM interpCCF_",h," WHERE vertex BETWEEN BEGIN_BATCH and END_BATCH");
        loop_query(batch, user, db, host, query_outline, code, config, id, h);
    }
}
