type file{}

type surfclust_table{}


(surfclust_table surfclustOutput) SurfClust (file specFile, file surfFile, file input, float threshVal, string prefix){
    app {
        SurfClust "-spec" @filename(specFile) "-surf_A" @filename(surfFile) "-input" @filename(input) "1 -rmm 2.8 -thresh_col 1 -thresh" threshVal "-amm2 2 -sort_n_nodes -prepend_node_index -prefix" prefix;
    }
}


string SeedRegions = ["IDEAL"];
string hemis = ["lh","rh"];
float vertexThresh[] = [6.0];

foreach seed in SeedRegions{
    foreach h in hemis{
        foreach thresh in vertexThresh{
            file HORRYspec <single_file_mapper;file=@strcat("HORRY.",h,".mesh140_std.spec")>;
            file HORRYsurf <single_file_mapper;file=@strcat("HORRY.",h,".mesh140_std.pial.asc")>;
            string inputName=@strcat("results/",h,"_",seed,"_new_sorted.txt");
            file inputFile <single_file_mapper;file=inputName>;
            string outPrefix=@strcat(h,"_",seed,"_PERMTEST",thresh);
            surfclust_table surfclustOutput <single_file_mapper; file=@strcat(h,"_",seed,"_PERMTEST6_ClstTable_r2.8_a2.0.1D")>;
            (surfclustOutput) = SurfClust(HORRYspec,HORRYsurf,inputFile,thresh,outPrefix);
        }
    }
}
