#!/usr/bin/python

import os
import commands
import sys
import time


seed = ['IDEAL']
hemis = ['lh','rh']

os.chdir(os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/NonParametric_lag_analy/results/")
print os.getcwd()

def surf_clust(ss,h):
    print "surfclust for seed: "+ss+"\nhemisphere: "+h+"\ntime started: "+time.ctime()
    print commands.getoutput("SurfClust \
    -spec HORRY."+h+".mesh140_std.spec -surf_A HORRY."+h+".mesh140_std.pial.asc  \
    -input "+h+"_"+ss+"_new_sorted.txt 1 -rmm 2.8 -thresh_col 1 -thresh 6.0 -amm2 2 -sort_n_nodes \
    -out_clusterdset -out_roidset -prepend_node_index -out_fulllist \
    -prefix "+h+"_"+ss+"_clustered_p05")



for ss in seed:
    for h in hemis:
        surf_clust(ss,h)
