#!/usr/bin/python

import commands
import time
import os
import sys

#---- VERY IMPORTANT: CURRENTLY, THIS SCRIPT IS HARDCODED FOR ONE HEMI AT A TIME
starts = range(1,196000,500)
hemis = ['lh']
#subjs = [5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,30]


for h in hemis:
    os.chdir(os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/NonParametric_lag_analy/results/")
    print os.getcwd()
    outfile = os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/NonParametric_lag_analy/results/missing_"+h+"starts.txt"
    missed = ""
    for num in starts:
        voxdata = "IDEAL"+h+"_NonParam"+`num`+"_"+`(num+499)`+".txt"
        if os.path.exists(voxdata): pass
        else:
            missed += `num`+"\n"
            
    file = open(outfile,"w")
    file.write(missed)
    file.close()
