#!/usr/bin/python

import os
import commands
import time


hemis = ['lh','rh']
seeds = ['IDEAL']


def concat_sort(ss,h):
    print "starting concat_sort process\ntime is: "+time.ctime()
    os.chdir(os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/NonParametric_lag_analy/results/")
    print os.getcwd()
    commands.getoutput("cat *"+h+"*0.txt > "+h+"_"+ss+"_gather.txt")
    print "word count of gather file ("+h+"_"+ss+"_gather.txt): "+commands.getoutput("wc "+h+"_"+ss+"_gather.txt")
    commands.getoutput("sort -g -k 1 "+h+"_"+ss+"_gather.txt > "+h+"_"+ss+"_sorted.txt")
    print "word count of sorted file ("+h+"_"+ss+"_sorted.txt): "+commands.getoutput("wc "+h+"_"+ss+"_sorted.txt")
    commands.getoutput("rm -rf "+h+"_"+ss+"_gather.txt")


for ss in seeds:
    for h in hemis:
        concat_sort(ss,h)
