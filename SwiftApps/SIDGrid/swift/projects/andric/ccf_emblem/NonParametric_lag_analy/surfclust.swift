#---- re-coded: 10.August.2009
type file{}

app (file surfclustOutput[]) SurfClust (file spec, file pial, file inputFile, float thresh, float rmm, string outPrefix){
        SurfClust "-spec" @spec "-surf_A" @pial "-input" @inputFile "1" 
        "-rmm" rmm "-thresh_col" "1" "-thresh" thresh "-amm2" "2" "-sort_n_nodes" 
        "-out_clusterdset" "-out_roidset" "-prepend_node_index" "-out_fulllist" "-prefix" outPrefix;
}


string id = "interp";
string hemis[] = ["lh","rh"];
float vertexThresh[] = [8.8];
float rmm_vals[] = [4.7, 4.8];

foreach h in hemis{
    foreach thresh in vertexThresh{
        foreach rmm in rmm_vals{
            file spec<single_file_mapper;file=@strcat("HORRY.",h,".mesh140_std.spec")>;
            file pial<single_file_mapper;file=@strcat("HORRY.",h,".mesh140_std.pial.asc")>;
            file inputFile <single_file_mapper;file=@strcat("output",h,"/new",h,"_FriedmanResult.1D")>;
            string outPrefix=@strcat("output",h,"/interp",h,"Thresh",thresh);
            string vals_name = @strcat(outPrefix,"_Clustered_r",rmm,"_a2.0.niml.dset, ");
            string msk_name = @strcat(outPrefix,"_ClstMsk_r",rmm,"_a2.0.niml.dset, ");
            string table_name = @strcat(outPrefix,"_ClstTable_r",rmm,"_a2.0.1D");
            string out_files = @strcat(vals_name, msk_name, table_name);
            file surfclustOutput[]<fixed_array_mapper;files=out_files>;
            surfclustOutput = SurfClust(spec, pial, inputFile, thresh, rmm, outPrefix);
        }
    }
}
