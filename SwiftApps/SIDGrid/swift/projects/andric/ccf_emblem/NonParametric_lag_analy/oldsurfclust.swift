type file{}

type surfclust_obj{
    file vals_dset;
    file mask_dset;
    file table;
};


(surfclust_obj surfclustOutput) SurfClust (file specFile, file surfFile, file input, float threshVal, string prefix){
    app {
        SurfClust "-spec" @filename(specFile) "-surf_A" @filename(surfFile) "-input" @filename(input) "-rmm 2.8 -thresh_col 1 -thresh" threshVal "-amm2 2 -sort_n_nodes -out_clusterdset -out_roidset -prepend_node_index -out_fulllist -prefix" prefix;
    }
}


string SeedRegions = ["IDEAL"];
string hemis = ["lh","rh"];
float vertexThresh[] = [6.0,9.0];

foreach seed in SeedRegions{
    foreach h in hemis{
        foreach thresh in vertexThresh{
            string HORRYsurf_dir = @strcat("/disks/gpfs/fmri/NL-EMB/fs.subjs/HORRY/SUMA/");
            file HORRYspec <simple_mapper;location=HORRYsurf_dir, file=@strcat("HORRY.",h,".mesh140_std.spec")>;
            file HORRYsurf <simple_mapper;location=HORRYsurf_dir, file=@strcat("HORRY.",h,".mesh140_std.pial.asc")>;
            string inputName=@strcat("results/",h,"_",seed,"_new_sorted.txt");
            file inputFile <single_file_mapper;file=inputName>;
            string outPrefix=@strcat(h,"_",seed,"_thresh",thresh);
            surfclust_obj surfclustOutput <simple_mapper;prefix=outPrefix>;
            (surfclustOutput) = SurfClust(HORRYspec,HORRYsurf,inputFile,thresh,outPrefix);
        }
    }
}
