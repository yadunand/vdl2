#!/usr/bin/python

import os
import commands
import time


brains = range(1001,3001,1)
#brains = range(1,3,1)

def concat_sort(nn):
    commands.getoutput("cat "+`nn`+"PermFriedman*_*.txt > PermBrain"+`nn`+"_gather.txt")
    print "gather file (PermBrain"+`nn`+"_gather.txt): "+commands.getoutput("wc PermBrain"+`nn`+"_gather.txt")
    commands.getoutput("sort -g -k 1 PermBrain"+`nn`+"_gather.txt > PermBrain"+`nn`+"_output.txt")
    print "count of sorted file (PermBrain"+`nn`+"_output.txt): "+commands.getoutput("wc PermBrain"+`nn`+"_output.txt")
    commands.getoutput("rm -rf "+`nn`+"PermFriedman*_*.txt")
    commands.getoutput("rm -rf PermBrain"+`nn`+"_gather.txt")



os.chdir(os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/NonParametric_lag_analy/NewPermutations/")
print os.getcwd()
print "starting concat_sort process\ntime is: "+time.ctime()

for nn in brains:
    concat_sort(nn)
