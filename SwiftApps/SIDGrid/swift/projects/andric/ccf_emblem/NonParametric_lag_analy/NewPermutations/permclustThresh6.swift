# Tuesday; July 22, 2008
# checked over and set again on Friday; August 22, 2008
# this version loops also the rmm value
type file{}

type surfclust_table{}


(surfclust_table surfclustOutput) SurfClust (file specFile, file surfFile, file input, int threshVal, float rmmVal, string prefix){
    app {
        SurfClust "-spec" @filename(specFile) "-surf_A" @filename(surfFile) "-input" @filename(input) "1" "-rmm" rmmVal "-thresh_col" "1" "-thresh" threshVal "-amm2" "2" "-sort_n_nodes" "-prepend_node_index" "-prefix" prefix;
    }
}


int vertexThresh[] = [6];
int permbrains[] = [3:3000:1];
#int permbrains[] = [1:2:1];
float rmm_vals[] = [2.2,2.5];

foreach brain in permbrains{
    foreach thresh in vertexThresh{
        foreach rmm in rmm_vals{
            file HORRYspec <single_file_mapper;file=@strcat("HORRY.lh.mesh140_std.spec")>;
            file HORRYsurf <single_file_mapper;file=@strcat("HORRY.lh.mesh140_std.pial.asc")>;
            string inputName=@strcat("PermBrain",brain,"_output.txt");
            file inputFile <single_file_mapper;file=inputName>;
            string outPrefix=@strcat(brain,"_Thresh",thresh);
            surfclust_table surfclustOutput <single_file_mapper; file=@strcat(outPrefix,"_ClstTable_r",rmm,"_a2.0.1D")>;
            (surfclustOutput) = SurfClust(HORRYspec,HORRYsurf,inputFile,thresh,rmm,outPrefix);
        }
    }
}
