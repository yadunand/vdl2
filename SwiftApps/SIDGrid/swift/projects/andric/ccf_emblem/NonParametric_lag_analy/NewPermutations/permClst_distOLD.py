#!/usr/bin/python

# reads a set of output files from surfclust
# and copies the line containing the largest
# cluster (most nodes) and writes all of
# those lines to a new file.
# if the output file contains no data, a
# 0 is entered

### no user parameters need be entered

import commands
import os
import sys
from stat import *

file_list = commands.getoutput("ls *_Thresh9_ClstTable_r2.8_a2.0.1D")

fl = file_list.split('\n')


filename = "permClst_distributionThresh9.txt"
perm_clust_out = open("permClst_distributionThresh9.txt", "w")

for f in fl:
    sz = os.stat(f)[ST_SIZE]
    if sz == 0:
        perm_clust_out.write('0\n')
    else:
        clst_file = open(f, "r")
        ln_16 = clst_file.readlines()[16]
        perm_clust_out.write(ln_16)
        clst_file.close()
        
perm_clust_out.close()
commands.getoutput("sort -r -g -k 2 "+filename+" > srtdPerm_DistributionThresh9.txt")
