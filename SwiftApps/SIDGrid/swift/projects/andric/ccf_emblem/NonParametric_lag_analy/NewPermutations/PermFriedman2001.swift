#---- coded Tuesday; July 22, 2008
type file{}

(file qout, file rout) run_query (string allcatargs, file config, file r_script, file r_config_conds, file r_config_subjs){
    app{
        Mediator allcatargs stdout=@filename(qout) @filename(r_script) @filename(r_config_conds) @filename(r_config_subjs);
    }
}

string user = @arg("user");
string db = "EMBLEM1";
string host = "tp-neurodb.ci.uchicago.edu";
#string host = "login4.ranger.tacc.utexas.edu";

file r_script<single_file_mapper; file="scripts/NewPermFriedman.R">;
file r_config_conds<single_file_mapper; file="config_file.Rdata">;
file r_config_subjs<single_file_mapper; file="subjs_config_file.txt">;
file config<single_file_mapper; file="user.config">;


loop_query(int bvox, string user, string db, string host, string query_outline, file r_script, file config, file r_config_conds, file r_config_subjs, string id, int medstep, int swiftstep){
    int evox = bvox+(swiftstep-1);
    string baseid = "PermFriedman";
    string r_swift_args = @strcat(id);
    string theoutprefix = @strcat(id,baseid,bvox,"_",evox);
    string med_args = @strcat("--user ",user,
        " --conf ", "user.config",
        " --db ", db,
        " --host ", host,
        " --query ", query_outline,
        " --r_script ", @filename(r_script),
        " --begin_vox ", bvox,
        " --end_vox ", evox,
        " --outprefix ", theoutprefix,
        " --batchstep ", medstep,
        " --r_swift_args ", r_swift_args,
        " --subject ", id);
    file q_result <single_file_mapper; file=@strcat("query_results/",theoutprefix,".qresult")>;
    file r_result <single_file_mapper; file=@strcat(theoutprefix,".tar")>;
    (q_result, r_result) = run_query(med_args, r_script, config, r_config_conds, r_config_subjs);
}

int swiftstep = 28000;
int mediatorstep = 7000;
int permbrains = [2001:3000:1];
foreach perm in permbrains {
    int mybatches = [1:196000:swiftstep];
    foreach batch in mybatches {
        string id = @strcat(perm);
        string query_outline = @strcat("select subject, vertex, speech_lag, emblem_lag, embspeech_lag from ccf_phase2_lh where vertex between BEGIN_BATCH and END_BATCH");
        loop_query(batch, user, db, host, query_outline, r_script, config, r_config_conds, r_config_subjs, id, mediatorstep, swiftstep);
    }
}
