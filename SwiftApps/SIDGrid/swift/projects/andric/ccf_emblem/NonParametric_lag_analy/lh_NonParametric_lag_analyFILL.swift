type file{}

(file qout, file rout) run_query (string allcatargs, file config, file r_script){
    app{
        Mediator allcatargs stdout=@filename(qout) @filename(r_script);
    }
}

string user = @arg("user");
string db = "EMBLEM1";
string host = "tp-neurodb.ci.uchicago.edu";
string baseid = "lh_NonParam";

file r_script<single_file_mapper; file="scripts/FriedmanTest.R">;
file config<single_file_mapper; file="user.config">;


loop_query(int bvox, string user, string db, string host, string query_outline, file r_script, file config, string id){
    int evox = bvox+499;
    string r_swift_args = @strcat(id);
    string theoutprefix = @strcat(id,bvox,"_",evox);
    string med_args = @strcat("--user ","andric",
        " --conf ", "user.config",
        " --db ", db,
        " --host ", host,
        " --query ", query_outline,
        " --r_script ", @filename(r_script),
        " --begin_vox ", bvox,
        " --end_vox ", evox,
        " --outprefix ", theoutprefix,
        " --batchstep ", "500",
        " --r_swift_args ", r_swift_args,
        " --subject ", id);
    file q_result <single_file_mapper; file=@strcat("results/",theoutprefix,".qresult")>;
    file r_result <single_file_mapper; file=@strcat(theoutprefix,".txt")>;
    (q_result, r_result) = run_query(med_args, r_script, config);
}

string regions = ["IDEAL"];
foreach region in regions {
    int mybatches = [4501,45501,152501];
    foreach batch in mybatches {
        string id = @strcat(region, baseid);
        string query_outline = @strcat("select subject, vertex, speech_lag, emblem_lag, embspeech_lag from ccf_phase2_lh where seed_region = '",region,"' and vertex between BEGIN_BATCH and END_BATCH");
        loop_query(batch, user, db, host, query_outline, r_script, config, id);
    }
}
