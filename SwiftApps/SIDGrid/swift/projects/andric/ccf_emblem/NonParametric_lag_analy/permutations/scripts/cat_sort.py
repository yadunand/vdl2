#!/usr/bin/python

import os
import sys
import commands


brains = range(3,3001)

os.chdir(os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/NonParametric_lag_analy/permutations/")
print os.getcwd()

for id in brains:
    print "brain: "+`id`
    commands.getoutput("cat "+`id`+"PermFriedman* | sort -g -k 1 > brain_"+`id`+"PermFriedman.txt")
    commands.getoutput("rm -rf "+`id`+"PermFriedman*")
