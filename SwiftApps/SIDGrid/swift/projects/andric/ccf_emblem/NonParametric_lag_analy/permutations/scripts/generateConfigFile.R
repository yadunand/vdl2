cc <- as.vector(c(rep(0,12),rep(1,12)))
con <- matrix(nrow=4000,ncol=27)
mat_row <- 0;

for (i in 1:nrow(con)){
    mat_row <- mat_row + 1
    con[mat_row,] <- c(sample(cc),sample(1:3))
}
write.table(con,file="perm_config_file.txt",row.names=FALSE,col.names=FALSE,quote=FALSE)
