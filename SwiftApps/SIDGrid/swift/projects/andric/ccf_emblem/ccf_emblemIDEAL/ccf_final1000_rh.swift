type file{}

(file qout, file rout) run_query (string allcatargs, file config, file dot_r){
    app{
        Mediator allcatargs stdout=@filename(qout) @filename(dot_r);
    }
}

string user = @arg("user");
string db = "EMBLEM1";
string host = "tp-neurodb.ci.uchicago.edu";

string query_outline = "SELECT subject, voxel, speech0B, speech1B, speech2B, speech3B, speech4B, speech5B, speech6B, speech7B, speech8B, gesture0B, gesture1B, gesture2B, gesture3B, gesture4B, gesture5B, gesture6B, gesture7B, gesture8B, gestspeech0B, gestspeech1B, gestspeech2B, gestspeech3B, gestspeech4B, gestspeech5B, gestspeech6B, gestspeech7B, gestspeech8B, grasp0B, grasp1B, grasp2B, grasp3B, grasp4B, grasp5B, grasp6B, grasp7B, grasp8B FROM emblemfemrh WHERE voxel between BEGIN_BATCH and END_BATCH";

file r_script<single_file_mapper; file="scripts/grid_ccf_rh.R">;
file config<single_file_mapper; file="user.config">;

string id = "canonHRF_rh";

loop_query(int bvox, string user, string db, string host, string query_outline, file r_script, file config, string id){
	int evox = bvox+999;
	string r_swift_args = @strcat("ccf");
	string med_args = @strcat("--user ","andric",
    		" --conf ", "user.config",
    		" --db ", db,
    		" --host ", host,
    		" --query ", query_outline,
    		" --r_script ", @filename(r_script),
    		" --begin_vox ", bvox,
    		" --end_vox ", evox,
    		" --subject ", id,
    		" --batchstep ", "1000",
    		" --r_swift_args ", r_swift_args,
    		" --subject ", id);
	file q_result <single_file_mapper; file=@strcat("results/",id,bvox,"_",evox,".qresult")>;
	file r_result <single_file_mapper; file=@strcat(id,bvox,"_",evox,".tar")>;
	(q_result, r_result) = run_query(med_args, r_script, config);
}

#int mybatches = [0:191000:5000];
int mybatches = [195000];

foreach batch in mybatches {
	loop_query(batch, user, db, host, query_outline, r_script, config, id);
}
