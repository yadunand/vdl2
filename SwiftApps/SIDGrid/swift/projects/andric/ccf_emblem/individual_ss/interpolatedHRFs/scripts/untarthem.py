#!/usr/bin/python

import commands
import time
import os
import sys


hemis = ['lh','rh']
subjs = [3,5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,30]
baseDir = os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/individual_ss/interpolatedHRFs/"

for ss in subjs:
    for h in hemis:
        os.chdir(baseDir+"ss"+`ss`+"interp_results/")
        print os.getcwd()
        print time.ctime()+"\ndoing "+h
        print commands.getoutput("ls interp_"+h+"*.tar | wc")
        print commands.getoutput("find -name 'interp_"+h+"*.tar' -exec tar xf '{}' ';'")
