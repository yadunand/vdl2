#!/usr/bin/python

import os
import sys
import commands
import MySQLdb
import time

subjs = [3,5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,30]
#subjs = [3]
hemis = ['lh','rh']

ccfInterpDir = os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/individual_ss/interpolatedHRFs/"

#---- defining the insert statement function that is going to be run
def insertdata_to_db(h,ss):
    print "que hora es?\n"+time.ctime()
    try:
        file = "'"+ccfInterpDir+"ss"+`ss`+"interp_results/fordb_ss"+`ss`+"_"+h+".txt'"
        print "File loading: "+file
        insert_statement = "load data local infile "+file+" into table interpCCF_"+h+" fields terminated by ' ';"
        print "Insert statement: "+insert_statement
        cursor.execute(insert_statement)
        print "Number of rows inserted %d" % cursor.rowcount
    except MySQLdb.Error, e:
        print "Error during "+h+" "+`ss`+" inserting %d -->> %s" % (e.args[0], e.args[1])
        sys.exit (1)


#---- establishing connection to the db
try:
    connection = MySQLdb.connect(read_default_file="~/.my.cnf",db="EMBLEM1")
except MySQLdb.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1])
    sys.exit (1)


cursor = connection.cursor()

#---- running the insert statement function through the loops
for h in hemis:
    for ss in subjs:
        insertdata_to_db(h,ss)
