#!/usr/bin/python

import shutil
import os


subjs = [3,5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,30]
baseDir = os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/individual_ss/interpolatedHRFs/"


for ss in subjs:
    shutil.copy2(baseDir+"scripts/indiv_ccfinterpSupp.R", baseDir+"ss"+`ss`+"interp_results")

