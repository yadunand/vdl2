#!/usr/bin/python

import os
import commands

hemis = ['lh','rh']
subjs = [3,5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,30]
baseDir = os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/individual_ss/interpolatedHRFs/"

for ss in subjs:
    os.chdir(baseDir+"ss"+`ss`+"interp_results/")
    print os.getcwd()
    for h in hemis:
        sortfile = "sorted_"+h+"_ccf.txt"
        print h+" count: "+commands.getoutput("wc "+sortfile)
