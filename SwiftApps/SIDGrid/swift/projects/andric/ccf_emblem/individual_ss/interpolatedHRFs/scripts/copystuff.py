#!/usr/bin/python

import os
import commands

hemis = ['lh','rh']
subjs = [3,5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,30]
baseDir = os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/individual_ss/"

for ss in subjs:
    oldStuff = baseDir+"IDEALseed/ss"+`ss`+"_results/ss_vec"+`ss`+".txt"
    newStuff = baseDir+"interpolatedHRFs/ss"+`ss`+"interp_results/"
    commands.getoutput("cp "+oldStuff+" "+newStuff)
