#!/usr/bin/python

import os
import commands


starts = range(1,196000,1000)
subjs = [5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,30]
#subjs = [3]
baseDir = os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/individual_ss/interpolatedHRFs/"
hemis = ['lh','rh']


for ss in subjs:
    os.chdir(baseDir+"ss"+`ss`+"interp_results/")
    print os.getcwd()
    for h in hemis:
        outfile = baseDir+"ss"+`ss`+"interp_results/missing_"+h+"_ss"+`ss`+"starts.txt"
        missed = ""
        for num in starts:
            voxdata = h+"_"+`num`+"_"+`(num+999)`+"_ccf.txt"
            if os.path.exists(voxdata): pass
            else:
                missed += `num`+"\n"
        
        file = open(outfile,'w')
        file.write(missed)
        file.close()
