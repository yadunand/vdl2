#-- doing this analysis this time interpolating the HRFs for better lag precision
type file{}

app (file qout, file rout) run_query (string allcatargs, file config, file dot_r){
    Mediator allcatargs stdout=@filename(qout) @filename(dot_r);
}


loop_query(int bvox, string user, string db, string host, string query_outline, file r_script, file config, string id, string h, int ss){
    int evox = bvox+3999;
    string r_swift_args = h;
    string theoutprefix = @strcat("ss",ss,"interp_results/",id,bvox,"_",evox);
    string med_args = @strcat("--user ","andric",
            " --conf ", "user.config",
            " --db ", db,
            " --host ", host,
            " --query ", query_outline,
            " --r_script ", @filename(r_script),
            " --begin_vox ", bvox,
            " --end_vox ", evox,
            " --subject ", id,
            " --batchstep ", "1000",
            " --outprefix ", theoutprefix,
            " --r_swift_args ", r_swift_args,
            " --subject ", id);
    file q_result <single_file_mapper; file=@strcat("Qresults/ss",ss,"_",id,bvox,"_",evox,".qresult")>;
    file r_result <single_file_mapper; file=@strcat(theoutprefix,".tar")>;
    (q_result, r_result) = run_query(med_args, r_script, config);
}


string user = @arg("user");
string db = "EMBLEM1";
string host = "tp-neurodb.ci.uchicago.edu";

file r_script<single_file_mapper; file="scripts/indiv_ccfinterp.R">;
file config<single_file_mapper; file="user.config">;

int subjs[] = [3,5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,30];
#int subjs[] = [3];
int mybatches[] = [1:196000:4000];
string hemis[] = ["lh","rh"];
string nombre = "interp";

foreach ss in subjs{
    foreach h in hemis {
        string query_outline = @strcat("SELECT subject, voxel, speech0B, speech1B, speech2B, speech3B, speech4B, speech5B, speech6B, speech7B, speech8B, gesture0B, gesture1B, gesture2B, gesture3B, gesture4B, gesture5B, gesture6B, gesture7B, gesture8B, gestspeech0B, gestspeech1B, gestspeech2B, gestspeech3B, gestspeech4B, gestspeech5B, gestspeech6B, gestspeech7B, gestspeech8B, grasp0B, grasp1B, grasp2B, grasp3B, grasp4B, grasp5B, grasp6B, grasp7B, grasp8B FROM emblemfem",h," WHERE subject = ",ss," AND voxel between BEGIN_BATCH and END_BATCH");
        string id = @strcat(nombre,"_",h);
        foreach batch in mybatches {
            loop_query(batch, user, db, host, query_outline, r_script, config, id, h, ss);
        }
    }
}
