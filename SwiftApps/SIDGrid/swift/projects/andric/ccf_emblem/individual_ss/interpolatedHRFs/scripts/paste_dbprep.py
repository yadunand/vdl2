#!/usr/bin/python

import os
import shutil
import commands


subjs = [5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,30]
#subjs = [3]
hemis = ['lh','rh']

ccfInterpDir = os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/individual_ss/interpolatedHRFs/"
oldCCFDir = os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/individual_ss/IDEALseed/"

seed_vec = "name_vec.txt"
seed_name = oldCCFDir+seed_vec

print "copying "+seed_name+" to\n"+ccfInterpDir
shutil.copy2(seed_name,ccfInterpDir)

def paster():
    ss_dir = ccfInterpDir+"ss"+`ss`+"interp_results/"
    os.chdir(ss_dir)
    print os.getcwd()
    oldSSPath = oldCCFDir+"ss"+`ss`+"_results/"
    SSVec = "ss_vec"+`ss`+".txt"
    shutil.copy2(oldSSPath+SSVec,".")
    ROIVec = "roi_vec_"+h+"_ss"+`ss`+".txt"
    shutil.copy2(oldSSPath+ROIVec,".")
    fordb_name = "fordb_ss"+`ss`+"_"+h+".txt"
    print commands.getoutput("paste -d ' ' "+seed_name+" "+SSVec+" "+ROIVec+" sorted_"+h+"_ccf.txt > "+fordb_name)
    print "word count for "+fordb_name+" is: "+commands.getoutput("wc "+fordb_name)
    

for ss in subjs:
    for h in hemis:
        paster()
