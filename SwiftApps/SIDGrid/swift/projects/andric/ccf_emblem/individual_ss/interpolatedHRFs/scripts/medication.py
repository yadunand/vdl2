#!/usr/bin/python

#---- this procedure is to go through each dir and find the missing text files then run R to fill in the gaps...
# must first run 'find_missing_files.py'
### this version is specific for the 'new' HNL machines (calero, guardado, manzur, pinto) because it sources a library for those distributions
import commands
import os
import sys
import MySQLdb


#subjs = [5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,30]
#subjs = [6]
#subjs = [11,12]
subjs = [10]
#subjs = [13,14,15,17,18,19,20,21]
#subjs = [22,23,24,25,26,27,28,30]
hemis = ['rh']
#hemis = ['lh','rh']
baseDir = os.getenv("cnari")+"/swift/projects/andric/ccf_emblem/individual_ss/interpolatedHRFs/"

def the_supplement(ss,h):
    ss_dir = baseDir+"ss"+`ss`+"interp_results/"
    #missingfilename = ss_dir+"missing_"+`ss`+h+"_starts.txt"
    missingfilename = ss_dir+"missing_"+h+"_ss"+`ss`+"starts.txt"
    print "missing file is: "+missingfilename
    missingfile = open(missingfilename,"r")
    missingnums = missingfile.read().split()
    print "missing numbers are: "+str(missingnums)

    for num in missingnums:
        begin_vox = int(num)
        end_vox = int(num)+999
        outprefix = ss_dir+"/"+h+"_"+num+"_"+`end_vox`
        print "outprefix: "+outprefix+"\n"
        checkfile = outprefix+"_ccf.txt"
        outfile = outprefix+".txt"
        print "begin vox: "+`begin_vox`+" and end vox: "+`end_vox`+"\n"
        while os.path.exists(checkfile)==False:
            try:
                connection = MySQLdb.connect(db="EMBLEM1",read_default_file="~/.my.cnf")
            except MySQLdb.Error, e:
                print "Error %d: %s" % (e.args[0], e.args[1])
                sys.exist (1)


            query = "SELECT subject, voxel, speech0B, speech1B, speech2B, speech3B, speech4B, speech5B, speech6B, speech7B, speech8B, gesture0B, gesture1B, gesture2B, gesture3B, gesture4B, gesture5B, gesture6B, gesture7B, gesture8B, gestspeech0B, gestspeech1B, gestspeech2B, gestspeech3B, gestspeech4B, gestspeech5B, gestspeech6B, gestspeech7B, gestspeech8B, grasp0B, grasp1B, grasp2B, grasp3B, grasp4B, grasp5B, grasp6B, grasp7B, grasp8B FROM emblemfem"+h+" WHERE subject = "+`ss`+" and voxel between "+`begin_vox`+" and "+`end_vox`+";"

            cursor = connection.cursor()
            cursor.execute(query)
            result = list(cursor.fetchall())
            bully = ''
            for rr in list(result):
                bully += str(rr[0])+" "+str(rr[1])+" "+str(rr[2])+" "+str(rr[3])+" "+str(rr[4])+" "+str(rr[5])+" "+str(rr[6])+" "+str(rr[7])+" "+str(rr[8])+" "+str(rr[9])+" "+str(rr[10])+" "+str(rr[11])+" "+str(rr[12])+" "+str(rr[13])+" "+str(rr[14])+" "+str(rr[15])+" "+str(rr[16])+" "+str(rr[17])+" "+str(rr[18])+" "+str(rr[19])+" "+str(rr[20])+" "+str(rr[21])+" "+str(rr[22])+" "+str(rr[23])+" "+str(rr[24])+" "+str(rr[25])+" "+str(rr[26])+" "+str(rr[27])+" "+str(rr[28])+" "+str(rr[29])+" "+str(rr[30])+" "+str(rr[31])+" "+str(rr[32])+" "+str(rr[33])+" "+str(rr[34])+" "+str(rr[35])+" "+str(rr[36])+" "+str(rr[37])+"\n"


            file = open(outfile,"w")
            file.write(bully)
            file.close()

            os.environ["R_FILE"] = outprefix
            os.chdir(ss_dir)
            print "now working in: \n"+os.getcwd()
            os.system("R CMD BATCH --vanilla ./indiv_ccfinterpSupp.R")
            begin_vox = begin_vox + 1
            print "next begin_vox is: "+`begin_vox`+"\n"



for ss in subjs:
    for h in hemis:
        the_supplement(ss,h)
