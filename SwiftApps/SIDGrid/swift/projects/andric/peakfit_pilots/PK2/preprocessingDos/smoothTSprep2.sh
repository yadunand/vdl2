#!/bin/tcsh

foreach h (lh rh)
echo "doing $h"; date
awk '{$1="";$2="";print}' cleanTSsmooth_${h}_PK2.1D.dset | awk '{sub(/^[ \t]+/, ""); print}' > cleanTSsmooth_${h}_PK2justTS.1D
end
