#!/usr/bin/python

import commands
import os
import sys
import MySQLdb
import time


hemis = ['lh','rh']

try:
    connection = MySQLdb.connect(read_default_file="~/.my.cnf",db="HEL")
except MySQLdb.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1])
    sys.exit (1)

cursor = connection.cursor()

def insertdata_to_db(h):
    print "que hora es?\n"+time.ctime()
    try:
        file = "'/gpfs/pads/fmri/cnari/swift/projects/andric/peakfit_pilots/PK2/preprocessingDos/mesh50_"+h+"_PK2fordb.txt'"
        print "File loading: "+file
        insert_statement = "load data local infile "+file+" into table peakTS_data"+h+" fields terminated by ' ';"
        print "Insert statement: "+insert_statement
        cursor.execute(insert_statement)
        print "Number of rows inserted: %d" % cursor.rowcount
    except MySQLdb.Error, e:
        print "Error loading peakFunc_data"+h+"  %d -->> %s" % (e.args[0], e.args[1])
        sys.exit (1)


for h in hemis:
    insertdata_to_db(h)
