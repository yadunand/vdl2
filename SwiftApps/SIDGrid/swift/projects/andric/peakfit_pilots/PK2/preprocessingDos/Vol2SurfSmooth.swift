#--- this is for the PK2 surface projections and smoothing on the surface
## cleanTScat_PK2allruns is a TS that has had slice time correction, covariates for motion, wm, and ventricles. 
## did each run separate and concatenated at end.
type file{}
type AFNI_obj{
    file HEAD;
    file BRIK;
}


app (file vol2surfOutfile) AFNI_Vol2Surf (file spec_file, file smoothwm, file pial, string map, AFNI_obj surfvol, AFNI_obj grid_parent){
    AFNI_3dVol2Surf "-spec" @spec_file "-surf_A" @smoothwm "-surf_B" @pial
    "-map_func" map "-f_steps" 15 "-f_index" "voxels" "-oob_index" "-1" "-oob_value" 0 "-no_headers" "-outcols_NSD_format"
    "-sv" @surfvol.BRIK "-grid_parent" @grid_parent.BRIK "-out_1D" @filename(vol2surfOutfile);
}

app (file surfsmoothOutfile) SurfSmooth (file spec_file, file smoothwm, file vol2surfOutfile){
    SurfSmooth "-spec" @spec_file "-surf_A" @smoothwm "-met" "HEAT_07" "-input" @vol2surfOutfile
    "-fwhm" 4 "-Niter" "-1" "-add_index" "-output" @filename(surfsmoothOutfile);
}

(file vol2surfOutfile, file surfsmoothOutfile) ProjectSmoothProc (file spec_file, file smoothwm, file pial, string map, AFNI_obj surfvol, AFNI_obj grid_parent){
    (vol2surfOutfile)= AFNI_Vol2Surf(spec_file, smoothwm, pial, map, surfvol, grid_parent);
    (surfsmoothOutfile) = SurfSmooth(spec_file, smoothwm, vol2surfOutfile);
}


string declarelist[] = ["PK2"];
string mapFunctions[] = ["ave"];
string hemis[] = ["lh","rh"];
string conditions[] = ["cleanTScat_PK2allruns"];

foreach subject in declarelist{
    foreach map in mapFunctions{
        foreach h in hemis{
            foreach cc in conditions{
                string baseDir = @strcat("/gpfs/pads/fmri/cnari/swift/projects/andric/peakfit_pilots/",subject,"/");
                file spec_file<single_file_mapper; file=@strcat(baseDir,subject,"surfaces/SUMA/",subject,"_",h,".spec")>;
                file smoothwm<single_file_mapper; file=@strcat(baseDir,subject,"surfaces/SUMA/",h,".smoothwm.asc")>;
                file pial<single_file_mapper; file=@strcat(baseDir,subject,"surfaces/SUMA/",h,".pial.asc")>;
                AFNI_obj surfvol<simple_mapper; location=@strcat(baseDir,subject,"surfaces/SUMA"), prefix=@strcat(subject,"_SurfVol_Alnd_Exp+orig.")>;
                AFNI_obj grid_parent<simple_mapper; location=@strcat(baseDir,"preprocessingDos"), prefix=@strcat(cc,"+orig.")>;
                file vol2surfOutfile<single_file_mapper; file=@strcat(baseDir,"preprocessingDos/",cc,"_",h,"_map",map,".1D")>;
                file surfsmoothOutfile<single_file_mapper; file=@strcat(baseDir,"preprocessingDos/",cc,"_",h,"Smooth_map",map,".1D.dset")>;
                (vol2surfOutfile, surfsmoothOutfile) = ProjectSmoothProc(spec_file, smoothwm, pial, map, surfvol, grid_parent);
            }
        }
    }
}
