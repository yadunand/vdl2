#!/bin/tcsh

## this is for the cleaned time series that have been volreg'd with slice timing correction
set subj = "PK2"

3dTcat -prefix cleanTScat_PK2allruns cleanTS_PK2run1+orig cleanTS_PK2run2+orig cleanTS_PK2run3+orig cleanTS_PK2run4+orig cleanTS_PK2run5+orig cleanTS_PK2run6+orig cleanTS_PK2run7+orig cleanTS_PK2run8+orig
