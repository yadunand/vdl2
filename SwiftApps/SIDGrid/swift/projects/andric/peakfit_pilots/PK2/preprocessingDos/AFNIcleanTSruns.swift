#-- deconvolve cleaning up TS
#---- doing this run by run
type file{}

type AFNI_obj{
    file HEAD;
    file BRIK;
}

type stims{
    string labels[];
    string fnames[];
}

app (AFNI_obj deconvBucketResult, AFNI_obj errtsResult) AFNI_pincheDeconvolution (AFNI_obj normalized, stims stimuli, file censor, file motionfile, file WMmask, file Ventmask, string outPrefix, string outerrtsPrefix){
    AFNI_3dDeconvolve "-jobs" 2 "-input" @filename(normalized.BRIK) "-polort" 3 "-tshift" "-num_stimts" 8
    "-stim_file" 1 stimuli.fnames[0]
    "-stim_file" 2 stimuli.fnames[1]
    "-stim_file" 3 stimuli.fnames[2]
    "-stim_file" 4 stimuli.fnames[3]
    "-stim_file" 5 stimuli.fnames[4]
    "-stim_file" 6 stimuli.fnames[5]
    "-stim_file" 7 stimuli.fnames[6]
    "-stim_file" 8 stimuli.fnames[7]
    "-censor" @filename(censor)
    "-full_first" "-rout"
    "-bucket" outPrefix
    "-errts" outerrtsPrefix;
}


string declarelist[] = ["PK2"];
int runs[] = [1:8];
foreach subject in declarelist{
    foreach rr in runs{
    ## map inputs
        file motionfile<single_file_mapper; file=@strcat("mot_",subject,"_run",rr)>;
        file WMmask<single_file_mapper; file=@strcat("whitemask.nrmlzdPerChng.",subject,".run",rr,".1D")>;
        file Ventmask<single_file_mapper; file=@strcat("ventmask.nrmlzdPerChng.",subject,".run",rr,".1D")>;
        file censor<single_file_mapper; file=@strcat("mot_",subject,"_run",rr,"_censor.1D")>;
        AFNI_obj normalized<simple_mapper; prefix=@strcat("nrmlzdPerChng.",subject,".run",rr,"+orig.")>;
    
        ## map outputs
        string outPrefix = @strcat("garbage_bucketTS_",subject,"run",rr);
        string outerrtsPrefix = @strcat("cleanTS_",subject,"run",rr);
        AFNI_obj bucket<simple_mapper; prefix=@strcat(outPrefix,"+orig.")>;
        AFNI_obj errts<simple_mapper; prefix=@strcat(outerrtsPrefix,"+orig.")>;
    
        #---- stimuli info
        stims stimuli;
        stimuli.labels = ["roll","pitch","yaw","dx","dy","dz","whitemask","ventmask"];
        stimuli.fnames = [@strcat(@filename(motionfile),"[1]"),@strcat(@filename(motionfile),"[2]"),@strcat(@filename(motionfile),"[3]"),
                        @strcat(@filename(motionfile),"[4]"),@strcat(@filename(motionfile),"[5]"),@strcat(@filename(motionfile),"[6]"),
                        @filename(WMmask),@filename(Ventmask)];
    
        (bucket, errts) = AFNI_pincheDeconvolution(normalized, stimuli, censor, motionfile, WMmask, Ventmask, outPrefix, outerrtsPrefix);
    }
}
