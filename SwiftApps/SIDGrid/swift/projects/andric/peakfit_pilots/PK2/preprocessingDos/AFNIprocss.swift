#---- doing volreg, despike, mean, percent sig change for PK2
#-- now using 'tshift' for slice timing correction
type file{}

type AFNI_obj{
    file HEAD;
    file BRIK;
};


app (AFNI_obj volregResult, file resultVolregMotion) AFNI_volreg (string baseName, AFNI_obj inputTS, AFNI_obj referenceTS, string volregBaseString){
    AFNI_3dvolreg "-twopass" "-twodup" "-dfile" @strcat("mot_",baseName) "-base" @strcat(volregBaseString,"+orig[225]") "-tshift" 4 "-prefix" @strcat("volreg.",baseName) @inputTS.BRIK;
}

app (AFNI_obj despikeResult) AFNI_despike (string baseName, AFNI_obj volregdInputTS){
    AFNI_3dDespike "-prefix" @strcat("despiked.",baseName) @volregdInputTS.BRIK;
}

app (AFNI_obj meanResult) AFNI_mean (AFNI_obj despikeResult, string baseName){
    AFNI_3dTstat "-mean" "-prefix" @strcat("mean.",baseName) @despikeResult.BRIK;
}

app (AFNI_obj nrmlzPerChngResult) AFNI_normalizePerChng (AFNI_obj despikeResult, AFNI_obj meanResult, string baseName){
    AFNI_3dcalc "-a" @despikeResult.BRIK "-b" @meanResult.BRIK "-expr" @strcat( "((a-b)/b)*100" ) "-fscale" "-prefix" @strcat("nrmlzdPerChng.",baseName);
}

(AFNI_obj despikeResult, AFNI_obj meanResult, AFNI_obj nrmlzPerChngResult) AFNI_PerChngTS (string baseName, AFNI_obj volregdInput){
    (despikeResult) = AFNI_despike(baseName, volregdInput);
    (meanResult) = AFNI_mean(despikeResult, baseName);
    (nrmlzPerChngResult) = AFNI_normalizePerChng(despikeResult, meanResult, baseName);
}


string declarelist[] = ["PK2"];
foreach subject in declarelist{
    int runs[] = [1:8];
    foreach run in runs{
        string baseName = @strcat(subject,".run",run);
        AFNI_obj inputTS<simple_mapper; prefix=@strcat("ts.",run,"+orig.")>;
        AFNI_obj volregResult<simple_mapper;prefix=@strcat("volreg.",baseName,"+orig.")>;
        file resultVolregMotion<single_file_mapper; file=@strcat("mot_",baseName)>;
        AFNI_obj volregRefTS<simple_mapper; prefix=@strcat("ts.4+orig.")>;
        string volregBaseString = @strcat("ts.4");
        AFNI_obj despikeResult<simple_mapper; prefix=@strcat("despiked.",baseName,"+orig.")>;
        AFNI_obj meanResult<simple_mapper; prefix=@strcat("mean.",baseName,"+orig.")>;
        AFNI_obj nrmlzPerChngResult<simple_mapper; prefix=@strcat("nrmlzdPerChng.",baseName,"+orig.")>;
        (volregResult, resultVolregMotion) = AFNI_volreg(baseName, inputTS, volregRefTS, volregBaseString);
        (despikeResult, meanResult, nrmlzPerChngResult) = AFNI_PerChngTS(baseName, volregResult);
    }
}
