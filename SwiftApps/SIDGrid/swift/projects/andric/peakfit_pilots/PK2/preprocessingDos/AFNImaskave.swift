# getting a white matter and ventricle mask for each PK2 run
type file{}

type AFNI_obj{
    file HEAD;
    file BRIK;
};

app (file maskaveResult) AFNI_maskave (AFNI_obj mask, AFNI_obj inputTS){
    AFNI_3dmaskave "-mask" @mask.BRIK @inputTS.BRIK stdout=@maskaveResult;
}

string names[] = ["white","vent"];
int runs[] = [1:8];
foreach nn in names{
    foreach rr in runs{
        AFNI_obj mask<simple_mapper; prefix=@strcat(nn,".roi.frac.qrtmask+orig.")>;
        AFNI_obj inputTS<simple_mapper; prefix=@strcat("nrmlzdPerChng.PK2.run",rr,"+orig.")>;
        file maskaveResult<single_file_mapper; file=@strcat(nn,"mask.nrmlzdPerChng.PK2.run",rr,".1D")>;
        maskaveResult = AFNI_maskave(mask, inputTS);
    }
}
