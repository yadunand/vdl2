#----  doing peakfit on PK2 pilot subject and experimenting with code
type file{}
type Rscript;
type PeakfitR{
    Rscript ShellpeakPK2runbyrun;
    Rscript preprocessEnewsmooth;
    Rscript peakfitv2v1;
}
type PeakResult{
    file pdf;
    file a;
    file b;
}


app (PeakResult outObj) peakScript (PeakfitR code, file tsfile){
    RInvoke @code.ShellpeakPK2runbyrun @filename(tsfile) @filename(outObj);
}

PeakfitR code<simple_mapper; location="peak_scriptsV2", suffix=".R">;

string declarelist[] = ["PK2"];
int runs[] = [1:8];
string hemilist[] = ["lh"];
int inputRegionNo[] = [54];

foreach subject in declarelist{
    foreach rn in runs{
        foreach h in hemilist{
            foreach rr in inputRegionNo{
                ## map inputs
                file inFile<single_file_mapper; file=@strcat("input/",subject,"_peakTS_data",h,".",rr,"run",rn)>;
                ## map results
                PeakResult outObj<simple_mapper; location="results", prefix=@strcat(subject,".",rr,"_",h,"run",rn,".result.")>;
                outObj=peakScript(code, inFile);
            }
        }
    }
}
