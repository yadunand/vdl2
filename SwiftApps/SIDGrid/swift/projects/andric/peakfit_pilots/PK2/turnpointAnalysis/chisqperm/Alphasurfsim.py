#!/usr/bin/python

import os
import sys

class AlphaSurfSim:

    def __init__(self):
        self.r_swift_args = ""
        self.r_script = ""
        self.permOut = ""
        self.spec = ""
        self.smoothwm = ""
        self.surfsmoothOut = ""
        self.pial = ""
        self.maskname = ""
        self.thresh = ""
        self.pval = ""
        self.rmm = ""
        self.surfclustOut = ""
        self.outname = ""
        self.proc_input = ""
        self.proc_type = ""
        

    def get_opts (self,allargstr):
        print "length of argstr "+str(len(allargstr))
        i = 0
        for o in allargstr:
            print "arg is "+o
            if o == "--r_swift_args":
                rstr = ""
                j = i
                while (allargstr[j+1].find("--")<=-1):
                    rstr = rstr+allargstr[j+1].strip()+" "
                    j+=1
                self.r_swift_args= rstr
            elif o == "--r_script":
                self.r_script = allargstr[i+1]
            elif o == "--permOut":
                self.permOut = allargstr[i+1]
            elif o == "--spec":
                self.spec = allargstr[i+1]
            elif o == "--smoothwm":
                self.smoothwm = allargstr[i+1]
            elif o == "--surfsmoothOut":
                self.surfsmoothOut = allargstr[i+1]
            elif o == "--pial":
                self.pial = allargstr[i+1]
            elif o == "--maskname":
                self.maskname = allargstr[i+1]
            elif o == "--thresh":
                self.thresh = allargstr[i+1]
            elif o == "--pval":
                self.pval = allargstr[i+1]
            elif o == "--rmm":
                self.rmm = allargstr[i+1]
            elif o == "--surfclustOut":
                self.surfclustOut = allargstr[i+1]
            elif o == "--proc_input":
                self.proc_input = allargstr[i+1]
            elif o == "--outputname":
                self.outname = allargstr[i+1]
            elif o == "--procedure":
                self.proc_type = allargstr[i+1]
            i = i+1
    
    def get_nodenum(self):
        surf = open(self.pial).read().split("\n")
        self.numnodes = surf[1].split()[0]
        
    def get_clstr(self):
        cluster_file = open(self.proc_input,"r").read().split("\n")
        cluster_file_length = len(cluster_file)-1
        clstrlist = ""
        for i in range(16,cluster_file_length):
            numNd = int(cluster_file[i].split()[1])
            clstrlist += ""+cluster_file[i].split()[0]+" "+`numNd`+"\n"

        outfile = open(self.outname,"w")
        outfile.write(clstrlist)
        outfile.close()

    def get_Perm_clstr(self):
        cluster_file = open(self.proc_input,"r").read().split("\n")
        cluster_file_length = len(cluster_file)-1
        if len(cluster_file)-1 == 1:
            clusterSize = cluster_file[0]+" \n"
        else:
            numNd = int(cluster_file[16].split()[1])
            clusterSize = `numNd`+" \n"

        outfile = open(self.outname,"w")
        outfile.write(clusterSize)
        outfile.close()

    def get_clstrmass(self):
        cluster_file = open(self.proc_input,"r").read().split("\n")
        cluster_file_length = len(cluster_file)-1
        clstrmasslist = ""
        for i in range(16,cluster_file_length):
            numNd = int(cluster_file[i].split()[1])
            Mean = float(cluster_file[i].split()[3])
            mass = numNd*Mean
            massval = "%.3f" % mass
            clstrmasslist += ""+cluster_file[i].split()[0]+" "+massval+"\n"

        outfile = open(self.outname,"w")
        outfile.write(clstrmasslist)
        outfile.close()

    def get_Perm_clstrmass(self):
        cluster_file = open(self.proc_input,"r").read().split("\n")
        if len(cluster_file)-1 == 1:
            clstrmassval = cluster_file[0]+" \n"
        else:
            numNd = int(cluster_file[16].split()[1])
            Mean = float(cluster_file[16].split()[3])
            mass = numNd*Mean
            massval = "%.3f" % mass
            clstrmassval = massval+" \n"

        outfile = open(self.outname,"w")
        outfile.write(clstrmassval)
        outfile.close()
    
    def run_r(self,proc):
        os.environ["PROC"] = proc
        os.environ["NUM_NODES"] = self.numnodes
        os.environ["PERM_OUT"] = self.permOut
        os.environ["PVAL"] = self.pval
        os.environ["surfsmoothOut"] = self.surfsmoothOut
        os.environ["maskname"] = self.maskname
        os.system("R CMD BATCH --vanilla "+self.r_script)
        pass
        
    def surfsmooth(self):
        os.system("SurfSmooth -spec "+self.spec+" -surf_A "+self.smoothwm+" -met HEAT_07 -input "+self.permOut+" -target_fwhm 4 -Niter -1 -add_index -output "+self.surfsmoothOut)
    
    def surfclust(self):
        os.system("SurfClust -spec "+self.spec+" -surf_A "+self.pial+" -input "+self.maskname+" 0 -rmm "+self.rmm+" -thresh_col 0 -thresh .99 -amm2 2 -sort_n_nodes -prepend_node_index -prefix "+self.surfclustOut)
        
        
        
clstr = AlphaSurfSim()
clstr.get_opts(sys.argv)
clstr.get_nodenum()
clstr.run_r("genperm")
clstr.surfsmooth()
clstr.run_r("cutoff")
clstr.surfclust()
if clstr.proc_type == "NumberNodes":
    clstr.get_clstr()
elif clstr.proc_type == "PermNumberNodes":
    clstr.get_Perm_clstr()
elif clstr.proc_type == "ClusterMass":
    clstr.get_clstrmass()
elif clstr.proc_type == "PermClusterMass":
    clstr.get_Perm_clstrmass()
