##-- coded: 13.Sept.2009
type file{}
type Rscript;

## generate permutation brain
app (file permOUT) permJob (Rscript code){
    RInvoke @code @filename(permOUT);
}

## smooth the permutation brain
app (file surfsmoothOutfile) SurfSmooth (file spec, file smoothwm, file permOUT){
    SurfSmooth "-spec" @spec "-surf_A" @smoothwm "-met" "HEAT_07" "-input" @permOUT
    "-fwhm" 4 "-Niter" "-1" "-add_index" "-output" @filename(surfsmoothOutfile);
}

## cluster the smoothed permutation brain
app (file surfclustOutput) SurfClust (file spec, file pial, file surfsmoothOutfile, float thresh, float rmm, string outPrefix){
        SurfClust "-spec" @spec "-surf_A" @pial "-input" @surfsmoothOutfile "1" "-rmm" rmm "-thresh_col" "1"
        "-thresh" thresh "-amm2" "2" "-sort_n_nodes" "-prepend_node_index" "-prefix" outPrefix;
}

## calculate the cluster mass and keep the biggest one
app (file clusterMassOUT) ClusterMass (file ClstrMassScript, file surfclustOutput){
    python @ClstrMassScript "--input" @filename(surfclustOutput) "--outputname" @filename(clusterMassOUT) "--procedure" "PermNumberNodes";
}

(file permOUT, file surfsmoothOutfile, file surfclustOutput, file clusterMassOUT) PermClusterMass (Rscript code, file ClstrMassScript, file spec, file smoothwm, file pial, float thresh, float rmm, string outPrefix){
    permOUT = permJob(code);
    surfsmoothOutfile = SurfSmooth(spec, smoothwm, permOUT);
    surfclustOutput = SurfClust(spec, pial, surfsmoothOutfile, thresh, rmm, outPrefix);
    clusterMassOUT = ClusterMass(ClstrMassScript, surfclustOutput);
}


Rscript code<single_file_mapper; file="scripts/generatePerm.R">;
file ClstrMassScript<single_file_mapper; file="ClstrMasstr.py">;

string idss[] = ["test"];
string way = "oneway";
int lag = 0;
string h = "lh";
float vertexThresh[] = [1.96];
float rmm_vals[] = [4.1];
int permbrains[] = [1:1000:1];
#int permbrains[] = [1];

foreach id in idss{
    foreach perm in permbrains{
        foreach thresh in vertexThresh{
            foreach rmm in rmm_vals{
                file permOUT<single_file_mapper; file=@strcat("perm",perm,".1D")>;
                file spec<single_file_mapper;file=@strcat("HORRY.",h,".mesh140_std.spec")>;
                file smoothwm<single_file_mapper; file=@strcat("HORRY.",h,".mesh140_std.smoothwm.asc")>;
                file pial<single_file_mapper;file=@strcat("HORRY.",h,".mesh140_std.pial.asc")>;
                string outPrefix=@strcat("perm",perm,id,"_",h,"_lag",lag,way,"Thresh",thresh);
                file surfsmoothOutfile<single_file_mapper; file=@strcat(outPrefix,"smooth.1D.dset")>;
                file surfclustOutput<single_file_mapper; file=@strcat(outPrefix,"_ClstTable_r",rmm,"_a2.0.1D")>;
                file clusterMassOUT<single_file_mapper; file=@strcat("perm",perm,id,"_",h,"_lag",lag,way,"_r",rmm,"Thresh",thresh,"nnodes.txt")>;
                (permOUT, surfsmoothOutfile, surfclustOutput, clusterMassOUT) = PermClusterMass(code, ClstrMassScript, spec, smoothwm, pial, thresh, rmm, outPrefix);
            }
        }
    }
}
