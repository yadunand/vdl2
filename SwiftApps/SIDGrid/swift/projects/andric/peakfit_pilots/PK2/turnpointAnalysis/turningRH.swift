#--- turnpoints across the brain via Mediator - for PK2
## type declarations:
type file{}
type Rscript;
## a type for simple mapping the two output files:

## Mediator app declaration:
app (file tarOut) run_query (string med_args, file config, Rscript code, file Annot){
    Mediator med_args @filename(code) @filename(Annot);
}

## this process sets parameters and calls Mediator:
loop_query(int vert, string user, string db, string host, string query_outline, Rscript code, file config, string subject, string h, int beginTS, int endTS, file Annot){
    string outPrefix = @strcat("move_vs_still_vert",vert,h);
    string tarPrefix = @strcat("turning2_out",h,"/vert",vert,"_",h);
    file tarOut<single_file_mapper; file=@strcat(tarPrefix,".tgz")>;
    string med_args = @strcat("--user ","andric"," --conf ", @filename(config)," --db ", db," --host ", host,
        " --vox ", vert," --subject ", subject," --subquery tsTSVAR"," --begin_ts ",beginTS," --end_ts ",endTS,
        " --query ", query_outline," --r_swift_args ",outPrefix," ",vert," ",h," ",@filename(tarOut), " --outprefix ", "FAH_Q", " --r_script ",@filename(code));
    (tarOut) = run_query(med_args, config, code, Annot);
}

## needed parameters to use Mediator:
string user = @arg("user");
string db = "HEL";
string host = "tp-neurodb.ci.uchicago.edu";
file config<single_file_mapper; file="user.config">;

## mapping the R code:
Rscript code<single_file_mapper; file="Rturning/turnchi.R">;
file Annot<single_file_mapper; file="Rturning/for3dDecon.PK2_4condSHORTcat_all.1D">;

## variables to move across in the foreach loops:
string declarelist[] = ["PK2smth"];
string hemilist[] = ["rh"];
int vertices[] = [0:131422:1];

foreach subject in declarelist{
    foreach h in hemilist{
        int beginTS = 0;
        int endTS = 1807;
        string query_outline = @strcat("SELECT SUBQUERY FROM peakTS_data",h," WHERE subject = '",subject,"' AND vertex=VOX");
        foreach vert in vertices{
            loop_query(vert, user, db, host, query_outline, code, config, subject, h, beginTS, endTS, Annot);
        }
    }
}
