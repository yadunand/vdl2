#!/usr/bin/python

import os
import fnmatch
import commands
import time


def finder(L,value):
    listsubset = []
    for ff in L:
        if fnmatch.fnmatch(ff,value):
            listsubset += [ff]

    return listsubset
    


baseDir = os.getenv("cnari")+"/swift/projects/andric/peakfit_pilots/PK2/turnpointAnalysis"
os.chdir(baseDir+"/turning2_outlh")
print os.getcwd()

thefiles = os.listdir(os.getcwd())
print "number of files in this dir: "+`len(thefiles)`+" :: "+time.ctime()
tgzfiles = finder(thefiles,"*.tgz")
print "number of tgz files in this dir: "+`len(tgzfiles)`+" :: "+time.ctime()

print "Starting untar "+time.ctime()
for tt in tgzfiles:
    os.system("tar -xzf "+tt)


print "Finished at "+time.ctime()
