#!/usr/bin/python

import os
import fnmatch
import shutil
import time


def finder(L,value):
    listsubset = []
    for ff in L:
        if fnmatch.fnmatch(ff,value):
            listsubset += [ff]
        
    return listsubset


h = "rh"
baseDir = os.getenv("cnari")+"/swift/projects/andric/peakfit_pilots/PK2/turnpointAnalysis"
outputDir = baseDir+"/turning2_out"+h
viewDir = baseDir+"/views"

os.chdir(outputDir)
print os.getcwd()

thefiles = os.listdir(os.getcwd())
print "Number of files in this dir: "+`len(thefiles)`

for i in range(0,2):
    onewayBrain = "move_vs_still_"+h+"_lag"+`i`+".oneway.1D"
    print onewayBrain+" starting: "+time.ctime()
    onewayfiles = finder(thefiles,"move_vs_still*.lag"+`i`+".oneway.chisqresult")
    print "Found all files in this subset, now building brain "+time.ctime()
    brainUNO = ''
    for pa in onewayfiles:
        brainUNO += open(pa,"r").read()
    
    onewayResult = open(onewayBrain,"w")
    print "Now WRITING "+onewayBrain+" -- "+time.ctime()
    onewayResult.write(brainUNO)
    onewayResult.close()
    print "Now COPYING "+onewayBrain
    shutil.copy2(onewayBrain,viewDir)


    twowayBrain = "move_vs_still_"+h+"_lag"+`i`+".twoway.1D"
    print twowayBrain+" starting: "+time.ctime()
    twowayfiles = finder(thefiles,"move_vs_still*.lag"+`i`+".twoway.chisqresult")
    print "Found all files in this subset, now building brain "+time.ctime()
    brainDOS = ''
    for pa in twowayfiles:
        brainDOS += open(pa,"r").read()
    
    twowayResult = open(twowayBrain,"w")
    print "Now WRITING "+twowayBrain+" -- "+time.ctime()
    twowayResult.write(brainDOS)
    twowayResult.close()
    print "Now COPYING "+twowayBrain
    shutil.copy2(twowayBrain,viewDir)
