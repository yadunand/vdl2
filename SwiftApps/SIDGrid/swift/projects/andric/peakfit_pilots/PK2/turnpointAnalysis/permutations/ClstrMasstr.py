#!/usr/bin/python

## Sept.2009: This is for grabbing the cluster information (after running SurfClust).
## Can run 4 types of procedures. Specify after "--procedure" flag: 
## 1) "NumberNodes" >>> Writing out the cluster size via number of nodes after SurfClust on a single input.
## 2) "PermNumberNodes" >>> Writing out the largest cluster's number of nodes on clustered permutation output
## 3) "ClusterMass" >>> calculating the cluster mass: the number of nodes times the statistic value on a single clustered input.
## 4) "PermClusterMass" >>> calculating the cluster mass on clustered permutation output, writing out the largest clustermass

import sys

class ClstrMasstr:

    def __init__(self):
        self.input = ""
        self.outname = ""
        self.proc_type = ""

    def get_opts (self,allargstr):
        print "length of argstr "+str(len(allargstr))
        i = 0
        for o in allargstr:
            print "arg is "+o
            if o == "--input":
                self.input = allargstr[i+1]
            elif o == "--outputname":
                self.outname = allargstr[i+1]
            elif o == "--procedure":
                self.proc_type = allargstr[i+1]
            print "input: "+self.input
            print "outname: "+self.outname
            print "procedure: "+self.proc_type
            i = i+1

    def get_clstr(self):
        cluster_file = open(self.input,"r").read().split("\n")
        cluster_file_length = len(cluster_file)-1
        clstrlist = ""
        for i in range(16,cluster_file_length):
            numNd = int(cluster_file[i].split()[1])
            clstrlist += ""+cluster_file[i].split()[0]+" "+`numNd`+"\n"

        outfile = open(self.outname,"w")
        outfile.write(clstrlist)
        outfile.close()

    def get_Perm_clstr(self):
        cluster_file = open(self.input,"r").read().split("\n")
        cluster_file_length = len(cluster_file)-1
        if len(cluster_file)-1 == 1:
            clusterSize = cluster_file[0]+" \n"
        else:
            numNd = int(cluster_file[16].split()[1])
            clusterSize = `numNd`+" \n"
        
        outfile = open(self.outname,"w")
        outfile.write(clusterSize)
        outfile.close()

    def get_clstrmass(self):
        cluster_file = open(self.input,"r").read().split("\n")
        cluster_file_length = len(cluster_file)-1
        clstrmasslist = ""
        for i in range(16,cluster_file_length):
            numNd = int(cluster_file[i].split()[1])
            Mean = float(cluster_file[i].split()[3])
            mass = numNd*Mean
            massval = "%.3f" % mass
            clstrmasslist += ""+cluster_file[i].split()[0]+" "+massval+"\n"

        outfile = open(self.outname,"w")
        outfile.write(clstrmasslist)
        outfile.close()

    def get_Perm_clstrmass(self):
        cluster_file = open(self.input,"r").read().split("\n")
        if len(cluster_file)-1 == 1:
            clstrmassval = cluster_file[0]+" \n"
        else:
            numNd = int(cluster_file[16].split()[1])
            Mean = float(cluster_file[16].split()[3])
            mass = numNd*Mean
            massval = "%.3f" % mass
            clstrmassval = massval+" \n"
        
        outfile = open(self.outname,"w")
        outfile.write(clstrmassval)
        outfile.close()



clstr = ClstrMasstr()
clstr.get_opts(sys.argv)
if clstr.proc_type == "NumberNodes":
    clstr.get_clstr()
elif clstr.proc_type == "PermNumberNodes":
    clstr.get_Perm_clstr()
elif clstr.proc_type == "ClusterMass":
    clstr.get_clstrmass()
elif clstr.proc_type == "PermClusterMass":
    clstr.get_Perm_clstrmass()
