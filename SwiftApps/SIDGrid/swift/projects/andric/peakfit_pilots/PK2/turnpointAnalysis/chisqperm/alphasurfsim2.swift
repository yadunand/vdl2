## pinche AlphaSim surface style
type file{}
type Rscript;

app (file clusterResult) AlphaSurfSim (file Alphasurf, Rscript code, string permOut, file spec, file smoothwm, file pial, string smoothout, string maskout, float rmm, string surfclustout, string inputclustout){
    python @Alphasurf "--r_script" @code "--permOut" permOut  "--spec" @spec "--smoothwm" @smoothwm "--pial" @pial "--surfsmoothOut" smoothout "--maskname" maskout "--rmm" rmm "--pval" 0.05 "--surfclustOut" surfclustout "--proc_input" inputclustout "--procedure" "PermNumberNodes" "--outputname" @filename(clusterResult);
}

file Alphasurf<single_file_mapper; file="Alphasurfsim.py">;
Rscript code<single_file_mapper; file="fahQchsq.R">;

string h = "lh";
float rmm_vals[] = [3.1];
int permbrains[] = [1:1000];
#int permbrains[] = [1:2];

foreach perm in permbrains{
    foreach rmm in rmm_vals{
        string permOut = @strcat("perm",perm,".1D");
        file spec<single_file_mapper;file=@strcat("PK2_",h,".spec")>;
        file smoothwm<single_file_mapper;file=@strcat(h,".smoothwm.asc")>;
        file pial<single_file_mapper;file=@strcat(h,".pial.asc")>;
        string smoothout = @strcat("perm",perm,"_",h,"smooth.1D.dset");
        string maskout = @strcat("perm",perm,"_",h,"smoothmask.1D");
        string surfclustout = @strcat("perm",perm,"_",h,"clustout");
        string inputclustout = @strcat("perm",perm,"_",h,"clustout_ClstTable_r",rmm,"_a2.0.1D");
        file clusterResult<single_file_mapper; file=@strcat("perm",perm,"_",h,"rmm",rmm,"nnodes.txt")>;
        clusterResult = AlphaSurfSim(Alphasurf, code, permOut, spec, smoothwm, pial, smoothout, maskout, rmm, surfclustout, inputclustout);
    }
}
