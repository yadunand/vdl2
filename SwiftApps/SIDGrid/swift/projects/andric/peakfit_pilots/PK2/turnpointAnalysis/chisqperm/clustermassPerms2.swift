##-- coded: 13.Sept.2009
type file{}
type Rscript;

## generate permutation brain
app (external permOUT) permJob (Rscript code, string permOUTname){
    RInvoke @code permOUTname;
}

## smooth the permutation brain
app (external surfsmoothOutfile) SurfSmooth (file spec, file smoothwm, external permOUT, string surfsmoothOutfilename){
    SurfSmooth "-spec" @spec "-surf_A" @smoothwm "-met" "HEAT_07" "-input" @permOUT
    "-fwhm" 4 "-Niter" "-1" "-add_index" "-output" surfsmoothOutfilename;
}

## cluster the smoothed permutation brain
app (external surfclustOutput) SurfClust (file spec, file pial, external surfsmoothOutfile, float thresh, float rmm, string outPrefix){
        SurfClust "-spec" @spec "-surf_A" @pial "-input" @surfsmoothOutfile "1" "-rmm" rmm "-thresh_col" "1"
        "-thresh" thresh "-amm2" "2" "-sort_n_nodes" "-prepend_node_index" "-prefix" outPrefix;
}

## calculate the cluster mass and keep the biggest one
app (file clusterMassOUT) ClusterMass (file ClstrMassScript, external surfclustOutput){
    python @ClstrMassScript "--input" @filename(surfclustOutput) "--outputname" @filename(clusterMassOUT) "--procedure" "PermClusterMass";
}

(external permOUT, external surfsmoothOutfile, external surfclustOutput, file clusterMassOUT) PermClusterMass (Rscript code, file ClstrMassScript, file spec, file smoothwm, file pial, float thresh, float rmm, string outPrefix, string permOUTname, string surfsmoothOutfilename){
    permOUT = permJob(code, permOUTname);
    surfsmoothOutfile = SurfSmooth(spec, smoothwm, permOUT, surfsmoothOutfilename);
    surfclustOutput = SurfClust(spec, pial, surfsmoothOutfile, thresh, rmm, outPrefix);
    clusterMassOUT = ClusterMass(ClstrMassScript, surfclustOutput);
}


Rscript code<single_file_mapper; file="scripts/generatePerm.R">;
file ClstrMassScript<single_file_mapper; file="ClstrMasstr.py">;

string idss[] = ["pa_vs_noise"];
string way = "oneway";
int lag = 0;
string h = "lh";
float vertexThresh[] = [3.84];
float rmm_vals[] = [3.1];
#int permbrains[] = [1:1000:1];
int permbrains[] = [1];

foreach id in idss{
    foreach perm in permbrains{
        foreach thresh in vertexThresh{
            foreach rmm in rmm_vals{
                external permOUT;
                string permOUTname=@strcat("perm",perm,".1D");
                file spec<single_file_mapper;file=@strcat("PK2_",h,".spec")>;
                file smoothwm<single_file_mapper; file=@strcat(h,".smoothwm.asc")>;
                file pial<single_file_mapper;file=@strcat(h,".pial.asc")>;
                string outPrefix=@strcat("perm",perm,id,"_",h,"_lag",lag,way,"Thresh",thresh);
                external surfsmoothOutfile;
                string surfsmoothOutfilename=@strcat(outPrefix,"smooth.1D.dset");
                external surfclustOutput;
                #string surfclustOutputname=@strcat(outPrefix,"_ClstTable_r",rmm,"_a2.0.1D");
                file clusterMassOUT<single_file_mapper; file=@strcat("perm",perm,id,"_",h,"_lag",lag,way,"_r",rmm,"Thresh",thresh,"PERMMASS.txt")>;
                (permOUT, surfsmoothOutfile, surfclustOutput, clusterMassOUT) = PermClusterMass(code, ClstrMassScript, spec, smoothwm, pial, thresh, rmm, outPrefix, permOUTname, surfsmoothOutfilename);
            }
        }
    }
}
