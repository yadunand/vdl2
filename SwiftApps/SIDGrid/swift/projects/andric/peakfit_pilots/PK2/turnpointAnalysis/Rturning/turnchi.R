### TURNPOINTS
library(akima)
library(pastecs)

allargs <- Sys.getenv("R_SWIFT_ARGS")
print(allargs)

outprefix <- noquote(strsplit(allargs," ")[[1]][2])
print(outprefix)
output_file_prefix <- paste(outprefix)
vertnum <- noquote(strsplit(allargs," ")[[1]][3])
vertnum <- as.numeric(vertnum)
print(vertnum)
hemi <- noquote(strsplit(allargs," ")[[1]][4])
h <- paste(hemi)
print(h)
tar_name <- noquote(strsplit(allargs," ")[[1]][5])
tar_name <- paste(tar_name)
print(tar_name)

inputfilename <- Sys.getenv("R_INPUT")

inputTS <- as.matrix(read.table(inputfilename))
inputTS <- data.frame(inputTS[1,])[,1]
print(length(inputTS))


#### Now do peak and valley analysis based on turnpoints

ts_turnpoints <- turnpoints(inputTS)
ts_peaks <- (1:length(inputTS))[extract(ts_turnpoints, no.tp = FALSE, peak = TRUE, pit = FALSE)]
ts_valleys <- (1:length(inputTS))[extract(ts_turnpoints, no.tp = FALSE, peak = FALSE, pit = TRUE)]
lower = 1

ts_peaks_and_valleys <- NULL
# this if loop is for when we start with a valley
if (ts_peaks[1] > ts_valleys[1]) {
    # this section fills from time 1 to the pitt with "v"
    upper <- ts_valleys[lower]
    for (i in seq(lower,upper)) {
        ts_peaks_and_valleys <- c(ts_peaks_and_valleys, "v")
    }
    # this section alternates between the base of a peak to the peak back down to the pitt, etc
    for (lower in c(1:min(length(ts_peaks),length(ts_valleys)))) {
        valley_index <- ts_valleys[lower]
        peak_index <- ts_peaks[lower]
            for (i in seq(valley_index,peak_index-1)) {
                ts_peaks_and_valleys <- c(ts_peaks_and_valleys, "p")
            }
            if (lower < max(length(ts_peaks),length(ts_valleys))) {
                valley_index <- ts_valleys[lower+1]
                for (i in seq(peak_index,valley_index-1)) {
                    ts_peaks_and_valleys <- c(ts_peaks_and_valleys, "v")
                }
            }
    }
    # if you started with valley end with peak
    valley_index <- max(ts_valleys,ts_peaks)+1
    peak_index <- length(inputTS)
    for (i in seq(valley_index,peak_index)) {
        ts_peaks_and_valleys <- c(ts_peaks_and_valleys, "p")
    }
    #PCs_peaks_and_valleys[,column] <- ts_peaks_and_valleys
    #column <- column +1
}   else if (ts_peaks[1] < ts_valleys[1]) {  # this if loop is for when we start with a peak
    # this section fills from time 1 to the peak with "p"
    upper <- ts_peaks[lower]
    for (i in seq(lower,upper)) {
        ts_peaks_and_valleys <- c(ts_peaks_and_valleys, "p")
    }
    # this section alternates between the base of a peak to the peak back down to the pitt, etc
        for (lower in c(1:(min(length(ts_peaks),length(ts_valleys))))) {
        valley_index <- ts_peaks[lower]
        peak_index <- ts_valleys[lower]
            for (i in seq(valley_index,peak_index-1)) {
                ts_peaks_and_valleys <- c(ts_peaks_and_valleys, "v")
            }
            if (lower < max(length(ts_peaks),length(ts_valleys))) {
                valley_index <- ts_peaks[lower+1]
                for (i in seq(peak_index,valley_index-1)) {
                    ts_peaks_and_valleys <- c(ts_peaks_and_valleys, "p")
                }
            }
    }
    # if you started with peak end with valley
    valley_index <- max(ts_valleys,ts_peaks)+1
    peak_index <- length(inputTS)
    for (i in seq(valley_index,peak_index)) {
        ts_peaks_and_valleys <- c(ts_peaks_and_valleys, "v")
    }
    #PCs_peaks_and_valleys[,column] <- ts_peaks_and_valleys
    #column <- column +1    
}

### would've written out here, but trying tpchisqtest instead

write.table(ts_peaks_and_valleys,"ts_peak_valley.tmp",row.name = F,col.name = F,append=FALSE)
ts_peaks_and_valleys <- read.table("ts_peak_valley.tmp")

coded <- read.table("Rturning/for3dDecon.PK2_4condSHORTcat_all.1D")
annotation_recode <- ""
annotation_recode[coded[,1]==1] <- "still"
annotation_recode[coded[,2]==1] <- "move"
annotation_recode[coded[,3]==1] <- "still"
annotation_recode[coded[,4]==1] <- "move"
annotation_recode[which(is.na(annotation_recode))] <- "blank"

# set the resulting recoded attributes of interest as variables
# the primary attribute should go first
dimension_1 <- "move"
dimension_2 <- "still"


#### source call was here !!!!!!!

## don't need to read in this var, never wrote it out..........ts_peaks_and_valleys <- read.table(inputfilename)
## annotation shift:
## by TR (ts & coding not resampled here)
#matrix_to_add_to_ts_peaks_and_valleys <- matrix(ncol=1,nrow=10,data=0)
#ts_peaks_and_valleys <- rbind(ts_peaks_and_valleys,matrix_to_add_to_ts_peaks_and_valleys)

# set lags 
#lags <- as.factor(c(0,.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10))
lags <- as.factor(c(0,1,2,3,4,5,6))

for (lag in as.numeric(levels(lags))) {

    # Now we need to pad the "annotation_recode" file in an appropriate manner
    if (lag == 0) {
        annotation_recode_short <- as.matrix(annotation_recode)
        #add_to_end_of_annotation_recode_short <- matrix(ncol=1,nrow=0,data=0)
        #annotation_recode_long <- as.character(rbind(annotation_recode_short,add_to_end_of_annotation_recode_short))
        annotation_recode_long  <- annotation_recode_short
    }

    if (lag > 0 && lag < 6) {
        annotation_recode_short <- as.matrix(annotation_recode)
        beginning_number_of_rows <- lag*1
        end_number_of_rows <- 6-lag*1
        add_to_beginning_of_annotation_recode_short <- matrix(ncol=1,nrow=beginning_number_of_rows,data=0)
        add_to_end_of_annotation_recode_short <- matrix(ncol=1,nrow=end_number_of_rows,data=0)
        annotation_recode_long <- as.character(rbind(add_to_beginning_of_annotation_recode_short,annotation_recode_short,add_to_end_of_annotation_recode_short))
    }

    if (lag == 6) {
        annotation_recode_short <- as.matrix(annotation_recode)
        add_to_beginning_of_annotation_recode_short <- matrix(ncol=1,nrow=6,data=0)
        annotation_recode_long <- as.character(rbind(add_to_beginning_of_annotation_recode_short,annotation_recode_short))
    }
    
        contingency_table <- matrix(ncol=2,nrow=2)
    one_way_contingency_table_results <- matrix(nrow = 1, ncol= 5)
    #colnames(one_way_contingency_table_results) <- c("attribute_at_peak", "attribute_at_valley", "x_square", "p_value")
    two_way_contingency_table_results <- matrix(nrow = 1, ncol= 7)
    #colnames(two_way_contingency_table_results) <- c("attribute_at_peak", "no_attribute_at_peak", "attribute_at_valley", "no_attribute_at_valley", "x_square", "p_value")

    ts_PVdata <- ts_peaks_and_valleys[,1]
    contingency_table <- matrix(ncol=2,nrow=2,dimnames = list(c(dimension_1, dimension_2),c("Peak", "Pit")))
    # This converts the amount of time to TR.  Do we like this?
    contingency_table[1,1] <- length(which(ts_PVdata[annotation_recode_long==dimension_1]=="p"))
    contingency_table[1,2] <- length(which(ts_PVdata[annotation_recode_long==dimension_1]=="v"))
    contingency_table[2,1] <- length(which(ts_PVdata[annotation_recode_long==dimension_2]=="p"))
    contingency_table[2,2] <- length(which(ts_PVdata[annotation_recode_long==dimension_2]=="v"))
    one_way_chisq_contingency_table <- chisq.test(as.table(contingency_table[1,]))
    two_way_chisq_contingency_table <- chisq.test(as.table(contingency_table))

    one_way_contingency_table_results[1,] <- c(vertnum,as.vector(c(as.vector(contingency_table[1,]),one_way_chisq_contingency_table$statistic, one_way_chisq_contingency_table$p.value)))
    two_way_contingency_table_results[1,] <- c(vertnum,as.vector(contingency_table),two_way_chisq_contingency_table$statistic, two_way_chisq_contingency_table$p.value)

    write.table(round(one_way_contingency_table_results,4),paste(output_file_prefix,".lag",lag,".oneway.chisqresult",sep=""),row.name = FALSE,col.name = FALSE,append=FALSE)

    write.table(round(two_way_contingency_table_results,4),paste(output_file_prefix,".lag",lag,".twoway.chisqresult",sep=""),row.name = FALSE,col.name = FALSE,append=FALSE)
}
system(paste("tar cfz ",tar_name," *.chisqresult",sep=""));
