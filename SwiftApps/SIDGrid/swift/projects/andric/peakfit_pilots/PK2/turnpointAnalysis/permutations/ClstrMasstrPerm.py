#!/usr/bin/python

import sys

class ClstrMasstr:

    def __init__(self):
        self.input = ""
        self.outname = ""

    def get_opts (self,allargstr):
        print "length of argstr "+str(len(allargstr))
        i = 0
        for o in allargstr:
            print "arg is "+o
            if o == "--input":
                self.input = allargstr[i+1]
            elif o == "--outputname":
                self.outname = allargstr[i+1]
            print "input: "+self.input
            print "outname: "+self.outname
            i = i+1

    def run_clstrmass(self):
        cluster_file = open(self.input,"r").read().split("\n")
        cluster_file_length = len(cluster_file)-1
        clstrmasslist = ""
        
        if len(cluster_file)-1 == 1:
            clstrmasslist = cluster_file[0]+" \n"
        else:
            numNd = int(cluster_file[16].split()[1])
            Mean = float(cluster_file[16].split()[3])
            mass = numNd*Mean
            massval = "%.3f" % mass
            clstrmasslist += massval+" \n"

        outfile = open(self.outname,"w")
        outfile.write(clstrmasslist)
        outfile.close()


clstmass = ClstrMasstr()
clstmass.get_opts(sys.argv)
clstmass.run_clstrmass()
