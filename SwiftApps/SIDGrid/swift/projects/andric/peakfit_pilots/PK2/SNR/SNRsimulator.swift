type file{}
#--- for doing simulations on the peak analysis pilot
#--- original coding 27.Jan.2009
#--- this was recoded 17.Feb.2009 to adjust to a different design used for PK2

app (file simResult) simScript (file scriptFile, file inputFile, int iter, float tval){
    RInvoke  @filename(scriptFile) @filename(inputFile) iter tval;
}


genSims(){
    file script<"scripts/SNRsimulations.R">;
    file inputData<"waver_out_par1concat4Cond.txt">;
    int SNRstarts[] = [20:110:1];
    foreach snr in SNRstarts{
        float ThreshVals[] = [1.98, 2.59];
        foreach val in ThreshVals{
            string outResultName = @strcat("resultsHALFpercent/iter",snr,"_tval",val,"_par1.txt");
            file outResult<single_file_mapper; file=outResultName>;
            (outResult)=simScript(script,inputData,snr,val);
        }
    }
}

genSims();
