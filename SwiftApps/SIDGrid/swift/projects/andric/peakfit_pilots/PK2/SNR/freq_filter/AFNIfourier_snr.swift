#--- low pass filtering
type file{}

type AFNI_obj{
    file HEAD;
    file BRIK;
}

app (AFNI_obj fourierResult) AFNI_FourierFilter (string baseName, AFNI_obj inputTS){
    AFNI_3dFourier "-prefix" @strcat("./fourierTS.",baseName) "-lowpass" .1 "-highpass" .009 @inputTS.BRIK;
}

app (AFNI_obj meanResult) AFNI_mean (AFNI_obj meanInput, string baseName){
    AFNI_3dTstat "-mean" "-prefix" @strcat("./fourier_mean.",baseName) @meanInput.BRIK;
}

app (AFNI_obj stdevResult) AFNI_stdev (AFNI_obj stdevInput, string baseName){
    AFNI_3dTstat "-stdev" "-prefix" @strcat("./fourier_stdev.",baseName)  @stdevInput.BRIK;
}

app (AFNI_obj ratioResult) AFNI_doratio (AFNI_obj stdevResult, AFNI_obj meanResult , string baseName){
    AFNI_3dcalc "-verbose" "-a" @meanResult.BRIK "-b" @stdevResult.BRIK "-expr" "(a/b)" "-prefix" @strcat("fourier_snr.",baseName);
}

(AFNI_obj fourierResult, AFNI_obj stdevResult, AFNI_obj meanResult, AFNI_obj ratioResult) AFNI_FourierSNR (string baseName, AFNI_obj inputTS){
    (fourierResult) = AFNI_FourierFilter(baseName, inputTS);
    (meanResult) = AFNI_mean(fourierResult, baseName);
    (stdevResult) = AFNI_stdev(fourierResult, baseName);
    (ratioResult)  =  AFNI_doratio(stdevResult, meanResult, baseName);
}


string declarelist[] = ["PK2"];
foreach subject in declarelist{
    int runs[] = [1:8];
    foreach run in runs{
        string baseName = @strcat(subject,".run",run);
        AFNI_obj inputTS<simple_mapper; prefix=@strcat("volreg.",baseName,"+orig.")>;
        AFNI_obj fourierResult<simple_mapper; prefix=@strcat("fourierTS.",baseName,"+orig.")>;
        AFNI_obj meanResult<simple_mapper;prefix=@strcat("fourier_mean.",baseName,"+orig.")>;
        AFNI_obj stdevResult<simple_mapper;prefix=@strcat("fourier_stdev.",baseName,"+orig.")>;
        AFNI_obj ratioResult<simple_mapper;prefix=@strcat("fourier_snr.",baseName,"+orig.")>;
        (fourierResult, stdevResult, meanResult, ratioResult) = AFNI_FourierSNR(baseName, inputTS);
    }
}
