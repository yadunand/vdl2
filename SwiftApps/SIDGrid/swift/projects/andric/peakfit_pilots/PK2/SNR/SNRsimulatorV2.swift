type file{}
#--- for doing simulations on the peak analysis pilot
#--- original coding 27.Jan.2009
#--- this was recoded 17.Feb.2009 to adjust to a different design used for PK2

app (file simResult) simScript (file scriptFile, file inputFile, int iter, float tval, int pnum){
    RInvoke  @filename(scriptFile) @filename(inputFile) iter tval pnum;
}


int parSet[] = [1:8];
int SNRstarts[] = [20:110:1];
float ThreshVals[] = [1.98, 2.59];

foreach pnum in parSet{
    file script<"scripts/SNRsimulationsV2.R">;
    file inputData<single_file_mapper; file=@strcat("inputs/waver_out_SHORTpar",pnum,"concat4Cond.txt")>;
    foreach snr in SNRstarts{
        foreach val in ThreshVals{
            file outResult<single_file_mapper; file=@strcat("resultsHALFpercent/iter",snr,"_tval",val,"_par",pnum,".txt")>;
            (outResult)=simScript(script,inputData,snr,val,pnum);
        }
    }
}
