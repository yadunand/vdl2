#!/usr/bin/python

import commands
import os
import sys

#---- this version doesn't use convolved hrfs

parSets = range(1,10)

def DeconvoleNoData(p):
    print commands.getoutput("3dDeconvolve -nodata \
    -nlast 239 -polort 3 -num_stimts 4 \
    -stim_file 1 waver_out_par"+`p`+"_still_paCond.1D -stim_label 1 still_pa \
    -stim_file 2 waver_out_par"+`p`+"_move_paCond.1D -stim_label 2 move_pa \
    -stim_file 3 waver_out_par"+`p`+"_still_noiseCond.1D -stim_label 3 still_noise \
    -stim_file 4 waver_out_par"+`p`+"_move_noiseCond.1D -stim_label 4 move_noise \
    -stim_maxlag 1 11 \
    -stim_maxlag 2 11 \
    -stim_maxlag 3 11 \
    -stim_maxlag 4 11 \
        > deconNodata_par"+`p`+"V2.out")


for p in parSets:
    DeconvoleNoData(p)
