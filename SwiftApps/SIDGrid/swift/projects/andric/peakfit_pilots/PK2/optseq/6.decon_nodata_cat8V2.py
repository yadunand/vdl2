#!/usr/bin/python

import commands
import os
import sys

#--- this version doesn't used convolved input files, uses the 1 and 0 event markers

def DeconvoleNoData():
    print commands.getoutput("3dDeconvolve -nodata \
    -nlast 1919 -polort 3 -num_stimts 4 \
    -stim_file 1 for3dDecon.PK2_4cond-cat8.1D[0] -stim_label 1 still_pa \
    -stim_file 2 for3dDecon.PK2_4cond-cat8.1D[1] -stim_label 2 move_pa \
    -stim_file 3 for3dDecon.PK2_4cond-cat8.1D[2] -stim_label 3 still_noise \
    -stim_file 4 for3dDecon.PK2_4cond-cat8.1D[3] -stim_label 4 move_noise \
    -stim_maxlag 1 11 \
    -stim_maxlag 2 11 \
    -stim_maxlag 3 11 \
    -stim_maxlag 4 11 \
        > deconNodata_cat8V2.out")


DeconvoleNoData()
