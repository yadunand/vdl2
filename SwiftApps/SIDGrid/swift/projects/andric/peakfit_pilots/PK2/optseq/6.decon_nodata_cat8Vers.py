#!/usr/bin/python

import commands
import os
import sys


def DeconvoleNoData():
    print commands.getoutput("3dDeconvolve -nodata \
    -nlast 1919 -polort 3 -num_stimts 4 \
    -stim_file 1 waver_out_cat8_still_paCond.1D -stim_label 1 still_pa \
    -stim_file 2 waver_out_cat8_move_paCond.1D -stim_label 2 move_pa \
    -stim_file 3 waver_out_cat8_still_noiseCond.1D -stim_label 3 still_noise \
    -stim_file 4 waver_out_cat8_move_noiseCond.1D -stim_label 4 move_noise \
    -glt 1 contrast_AvB \
    -glt 1 contrast_AvB \
    -glt 1 contrast_AvB \
        > deconNodata_cat8.out")


DeconvoleNoData()
