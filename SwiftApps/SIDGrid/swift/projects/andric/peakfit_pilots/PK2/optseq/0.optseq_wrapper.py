#!/usr/bin/python

import commands
import os
import time

print "optseq beginning: "+time.ctime()
print commands.getoutput("optseq2 \
    --ntp 240 \
    --tr 1.5 \
    --psdwin 0 18 \
    --ev still_pa 1.5 30 \
    --ev move_pa 1.5 30 \
    --ev still_noise 1.5 30 \
    --ev move_noise 1.5 30 \
    --polyfit 2 \
    --focb 1500 \
    --nkeep 10 \
    --nsearch 10000 \
    --o PK2_4cond")

print "optseq is done: "+time.ctime()
