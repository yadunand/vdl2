#!/usr/bin/python

import commands
import sys
import os

columns = {"still_pa":0, "move_pa":1, "still_noise":2, "move_noise":3}
parSets = range(1,10)

print os.getcwd()

for p in parSets:
    for col in columns:
        print "doing: "+col+" column number: "+`columns[col]`+"\n"
        print commands.getoutput("waver -GAM -dt 1.5 -numout 226 -input for3dDecon.PK2_4condSHORT-00"+`p`+".1D["+`columns[col]`+"] > waver_out_SHORTpar"+`p`+"_"+col+"Cond.1D")
