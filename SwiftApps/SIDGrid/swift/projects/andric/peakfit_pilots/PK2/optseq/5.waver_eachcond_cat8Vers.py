#!/usr/bin/python

import commands
import sys
import os

columns = {"still_pa":0, "move_pa":1, "still_noise":2, "move_noise":3}
print os.getcwd()

#for p in parSets:
for col in columns:
    print "doing: "+col+" column number: "+`columns[col]`+"\n"
#        print "waver -GAM -dt 1.5 -numout 240 -input for3dDecon.PK2_4cond-00"+`p`+".1D["+`columns[col]`+"] > waver_out_par"+`p`+"_"+col+"Cond.1D"
#        print commands.getoutput("waver -GAM -dt 1.5 -numout 240 -input for3dDecon.PK2_4cond-00"+`p`+".1D["+`columns[col]`+"] > waver_out_par"+`p`+"_"+col+"Cond.1D")
    commands.getoutput("waver -GAM -dt 1.5 -numout 1920 -input for3dDecon.PK2_4cond-cat8.1D["+`columns[col]`+"] > waver_out_cat8_"+col+"Cond.1D")
