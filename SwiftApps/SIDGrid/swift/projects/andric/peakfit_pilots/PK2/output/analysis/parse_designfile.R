dd = read.table("for3dDecon.PK2_4condSHORTcat_all.1D")
tr = c(1:226)
runs = 8
start = 1
end = length(tr)

for (i in 1:runs){
    design = dd[start:end,]
    start = start + length(tr)
    end = end + length(tr)
    out = paste("run",i,"Design.txt",sep="")
    write.table(design,file=out,col.names=F,quote=F)
}
print(start)
print(end)
