#!/bin/tcsh

set subj = "PK2"

echo "cat motion"
cat mot_$subj.run1 mot_$subj.run2 mot_$subj.run3 mot_$subj.run4 mot_$subj.run5 mot_$subj.run6 mot_$subj.run7 mot_$subj.run8 > motion_$subj.1D

echo "now plot motion rotations and translations"
1dplot -ps -volreg -xlabel 'Time Point' motion_$subj.1D'[1-6]' > motion_$subj.ps
