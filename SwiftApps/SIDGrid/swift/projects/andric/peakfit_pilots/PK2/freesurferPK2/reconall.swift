type file{}
type mgzfile;
#---- for reconall on PK2 pilot

(file subj_dir) recon1 (string subj, mgzfile inimage) {
    app {
        recon1 @arg("user") subj @inimage;
    }
}

(file subj_dirL) recon2L (string subj, file subj_dir) {
    app {
        recon2L @arg("user") subj @subj_dir;
        }
}


(file subj_dirR) recon2R (string subj, file subj_dir) {
    app {
        recon2R @arg("user") subj @subj_dir;
        }
}

(file subj_dirCOMPLETE) reconFINAL (string subj, file subj_dirL, file subj_dirR) {
    app {
        reconFINAL @arg("user") subj @subj_dirL @subj_dirR;
        }
}


mgzfile inimage <single_file_mapper;file=@arg("inimage")>;
string subj = @arg("subj");
file subject_data <single_file_mapper;file=@strcat(subj, ".tar")>;
file subject_data2L <single_file_mapper;file=@strcat(subj,"L.tar")>;
file subject_data2R <single_file_mapper;file=@strcat(subj,"R.tar")>;
file subject_dataCOMPLETE <single_file_mapper;file=@strcat(subj,"COMPLETE.tar")>;

subject_data = recon1(subj, inimage);
subject_data2L = recon2L (subj, subject_data);
subject_data2R = recon2R (subj, subject_data);
subject_dataCOMPLETE = reconFINAL (subj, subject_data2L, subject_data2R);
