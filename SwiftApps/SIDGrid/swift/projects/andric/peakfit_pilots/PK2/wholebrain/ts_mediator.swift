#--- peak analysis across the brain via Mediator
## type declarations:
type file{}
type Rscript;
## a type for simple mapping the 3 R scripts used:
type PeakfitR{
    Rscript subqrymediator;
    Rscript preprocessEnewsmooth;
    Rscript peakfitv2v1;
}
## a type for simple mapping the two output files:
type PeakResult{
    file pdf;
    file a;
    file b;
}

## Mediator app declaration:
app (file q_result, PeakResult outObj) run_query (string med_args, file config, PeakfitR code){
    Mediator med_args stdout=@filename(q_result) @filename(code.subqrymediator);
}

## this process sets parameters and calls Mediator:
loop_query(int vert, string user, string db, string host, string query_outline, PeakfitR code, file config, string subject, string h, int beginTS, int endTS){
    string theoutprefix = "FAH_Q";
    PeakResult outObj<simple_mapper; location="results", prefix=@strcat(subject,"_",h,"vert",vert,".result.")>;
    file q_result <single_file_mapper; file=@strcat("Qresults/",subject,"_",h,"vert",vert,".qresult")>;
    string med_args = @strcat("--user ","andric"," --conf ", @filename(config)," --db ", db," --host ", host,
        " --vox ", vert," --subject ", subject," --subquery tsTSVAR"," --begin_ts ",beginTS," --end_ts ",endTS,
        " --query ", query_outline," --r_swift_args ", @filename(outObj)," --outprefix ", theoutprefix, " --r_script ",@filename(code.subqrymediator));
    trace(med_args);
    (q_result, outObj) = run_query(med_args, config, code);
}

## needed parameters to use Mediator:
string user = @arg("user");
string db = "HEL";
string host = "tp-neurodb.ci.uchicago.edu";
file config<single_file_mapper; file="user.config">;

## mapping the R code:
PeakfitR code<simple_mapper; location="Rscripts", suffix=".R">;

## variables to move across in the foreach loops:
string declarelist[] = ["PK2smth"];
string hemilist[] = ["lh"];
int vertices[] = [3];

foreach subject in declarelist{
    foreach h in hemilist{
        int beginTS = 0;
        int endTS = 1807;
        string query_outline = @strcat("SELECT SUBQUERY FROM peakTS_data",h," WHERE subject = '",subject,"' AND vertex=VOX");
        foreach vert in vertices{
            loop_query(vert, user, db, host, query_outline, code, config, subject, h, beginTS, endTS);
        }
    }
}
