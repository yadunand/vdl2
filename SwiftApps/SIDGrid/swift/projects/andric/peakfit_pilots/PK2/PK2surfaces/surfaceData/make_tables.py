#!/usr/bin/python

import os
import sys
import commands
import MySQLdb
import time

try:
    connection = MySQLdb.connect(read_default_file="~/.my.cnf",db="HEL")
except MySQLdb.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1])
    sys.exit (1)

cursor = connection.cursor()

def create_new_table():
    print "que hora es?\n"+time.ctime()
    try:
        create_table_statement = 'CREATE TABLE peakFunc_datalh (subject VARCHAR(30), vertex INTEGER, roi INTEGER, snr_run1 FLOAT, snr_run2 FLOAT, snr_run3 FLOAT, snr_run4 FLOAT, snr_run5 FLOAT, snr_run6 FLOAT, snr_run7 FLOAT, snr_run8 FLOAT, stillpa0B FLOAT, stillpa0T FLOAT, stillpa1B FLOAT, stillpa1T FLOAT, stillpa2B FLOAT, stillpa2T FLOAT, stillpa3B FLOAT, stillpa3T FLOAT, stillpa4B FLOAT, stillpa4T FLOAT, stillpa5B FLOAT, stillpa5T FLOAT, stillpa6B FLOAT, stillpa6T FLOAT, stillpa7B FLOAT, stillpa7T FLOAT, stillpa8B FLOAT, stillpa8T FLOAT, stillpa9B FLOAT, stillpa9T FLOAT, stillpa10B FLOAT, stillpa10T FLOAT, stillpa11B FLOAT, stillpa11T FLOAT, movepa0B FLOAT, movepa0T FLOAT, movepa1B FLOAT, movepa1T FLOAT, movepa2B FLOAT, movepa2T FLOAT, movepa3B FLOAT, movepa3T FLOAT, movepa4B FLOAT, movepa4T FLOAT, movepa5B FLOAT, movepa5T FLOAT, movepa6B FLOAT, movepa6T FLOAT, movepa7B FLOAT, movepa7T FLOAT, movepa8B FLOAT, movepa8T FLOAT, movepa9B FLOAT, movepa9T FLOAT, movepa10B FLOAT, movepa10T FLOAT, movepa11B FLOAT, movepa11T FLOAT, stillnoise0B FLOAT, stillnoise0T FLOAT, stillnoise1B FLOAT, stillnoise1T FLOAT, stillnoise2B FLOAT, stillnoise2T FLOAT, stillnoise3B FLOAT, stillnoise3T FLOAT, stillnoise4B FLOAT, stillnoise4T FLOAT, stillnoise5B FLOAT, stillnoise5T FLOAT, stillnoise6B FLOAT, stillnoise6T FLOAT, stillnoise7B FLOAT, stillnoise7T FLOAT, stillnoise8B FLOAT, stillnoise8T FLOAT, stillnoise9B FLOAT, stillnoise9T FLOAT, stillnoise10B FLOAT, stillnoise10T FLOAT, stillnoise11B FLOAT, stillnoise11T FLOAT, movenoise0B FLOAT, movenoise0T FLOAT, movenoise1B FLOAT, movenoise1T FLOAT, movenoise2B FLOAT, movenoise2T FLOAT, movenoise3B FLOAT, movenoise3T FLOAT, movenoise4B FLOAT, movenoise4T FLOAT, movenoise5B FLOAT, movenoise5T FLOAT, movenoise6B FLOAT, movenoise6T FLOAT, movenoise7B FLOAT, movenoise7T FLOAT, movenoise8B FLOAT, movenoise8T FLOAT, movenoise9B FLOAT, movenoise9T FLOAT, movenoise10B FLOAT, movenoise10T FLOAT, movenoise11B FLOAT, movenoise11T FLOAT);'
        cursor.execute(create_table_statement)
    except MySQLdb.Error, e:
        print "Error !!!!!" % (e.args[0], e.args[1])
        sys.exit (1)


create_new_table()
