#--- this is for the PK2 surface projections
type file{}

type AFNI_obj{
    file HEAD;
    file BRIK;
}


app (file out_1d) AFNI_Vol2Surf (file spec_file, file smoothwm, file pial, string map, AFNI_obj surfvol, AFNI_obj grid_parent){
    AFNI_3dVol2Surf "-spec" @spec_file "-surf_A" @smoothwm "-surf_B" @pial
    "-map_func" map "-f_steps" 15 "-f_index" "voxels" "-oob_index" "-1" "-oob_value" 0 "-no_headers" "-outcols_NSD_format"
    "-sv" @surfvol.BRIK "-grid_parent" @grid_parent.BRIK "-out_1D" @filename(out_1d);
}


string declarelist[] = ["PK2"];
string mapFunctions[] = ["ave"];
string hemis[] = ["lh","rh"];
string conditions[] = ["TScat_Dtrnd.nrmlzdPerChng"];

foreach subject in declarelist{
    foreach map in mapFunctions{
        foreach h in hemis{
            foreach cc in conditions{
                string baseDir = @strcat("/disks/ci-gpfs/fmri/cnari/swift/projects/andric/peakfit_pilots/",subject,"/");
                file spec_file<single_file_mapper; file=@strcat(baseDir,subject,"surfaces/SUMA/",subject,"_",h,".spec")>;
                file smoothwm<single_file_mapper; file=@strcat(baseDir,subject,"surfaces/SUMA/",h,".smoothwm.asc")>;
                file pial<single_file_mapper; file=@strcat(baseDir,subject,"surfaces/SUMA/",h,".pial.asc")>;
                AFNI_obj surfvol<simple_mapper; location=@strcat(baseDir,subject,"surfaces/SUMA"), prefix=@strcat(subject,"_SurfVol_Alnd_Exp+orig.")>;
                AFNI_obj grid_parent<simple_mapper; location=@strcat(baseDir,"preprocessing"), prefix=@strcat(cc,".",subject,"+orig.")>;
                file output1D<single_file_mapper; file=@strcat(baseDir,subject,"surfaces/surfaceData/",subject,"_",h,"_",cc,"map",map,".1D")>;
                output1D = AFNI_Vol2Surf(spec_file, smoothwm, pial, map, surfvol, grid_parent);
            }
        }
    }
}
