type file {}						
type mgzfile;

(file subj_dir) autorecon2 (string subj, file subj_dir_r1) { 			
    app {
        autorecon2_BOTH @arg("user") subj @subj_dir;
    }
}

(file subj_dirL) recon2L (string subj, file subj_dir) {
    app {
	recon2L @arg("user") subj @subj_dir;
	}
}


(file subj_dirR) recon2R (string subj, file subj_dir) {
    app {
	recon2R @arg("user") subj @subj_dir;
	}
}

(file subj_dirCOMPLETE) reconFINAL (string subj, file subj_dirL, file subj_dirR) {
    app {
	reconFINAL @arg("user") subj @subj_dirL @subj_dirR;
	}
}
	

string subj = @arg("subj");
# initial input subject dir should already be tar'ed 
file subject_data_r1 <single_file_mapper;file=@strcat(subj, ".tar")>;

file subject_data <single_file_mapper;file=@strcat(subj, ".tar")>;
file subject_data2L <single_file_mapper;file=@strcat(subj,"L.tar")>;
file subject_data2R <single_file_mapper;file=@strcat(subj,"R.tar")>;
file subject_dataCOMPLETE <single_file_mapper;file=@strcat(subj,"COMPLETE.tar")>;

subject_data = autorecon2(subj, subject_data_r1);
subject_data2L = recon2L (subj, subject_data);
subject_data2R = recon2R (subj, subject_data);
subject_dataCOMPLETE = reconFINAL (subj, subject_data2L, subject_data2R);



