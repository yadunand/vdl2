#---- doing volreg, despike, mean, percent sig change for PK1
type file{}

type AFNI_obj{
    file HEAD;
    file BRIK;
};


app (AFNI_obj volregResult, file resultVolregMotion) AFNI_volreg (string baseName, AFNI_obj inputTS, AFNI_obj referenceTS, string volregBaseString){
    AFNI_3dvolreg "-twopass" "-twodup" "-dfile" @strcat("mot_",baseName) "-base" @strcat(volregBaseString,"+orig[199]") "-prefix" @strcat("volreg.",baseName) @inputTS.BRIK;
}

app (AFNI_obj despikeResult) AFNI_despike (string baseName, AFNI_obj volregdInputTS){
    AFNI_3dDespike "-prefix" @strcat("despiked.",baseName) @volregdInputTS.BRIK;
}

app (AFNI_obj meanResult) AFNI_mean (AFNI_obj despikeResult, string baseName){
    AFNI_3dTstat "-mean" "-prefix" @strcat("mean.",baseName) @despikeResult.BRIK;
}

app (AFNI_obj nrmlzPerChngResult) AFNI_normalizePerChng (AFNI_obj despikeResult, AFNI_obj meanResult, string baseName){
    AFNI_3dcalc "-a" @despikeResult.BRIK "-b" @meanResult.BRIK "-expr" "'((a-b)/b)" "*" "100'" "-fscale" "-prefix" @strcat("nrmlzdPerChng.",baseName);
}

app (AFNI_obj TScatResult) AFNI_TScat (string subject, AFNI_obj PerCh2, AFNI_obj PerCh3, AFNI_obj PerCh4, AFNI_obj PerCh5, AFNI_obj PerCh6, AFNI_obj PerCh7, AFNI_obj PerCh8, AFNI_obj PerCh9){
    AFNI_3dTcat "-prefix" @strcat("nrmlzdPerChng_TScat.",subject) @PerCh2.BRIK @PerCh3.BRIK @PerCh4.BRIK @PerCh5.BRIK @PerCh6.BRIK @PerCh7.BRIK @PerCh8.BRIK @PerCh9.BRIK;
}

(AFNI_obj despikeResult, AFNI_obj meanResult, AFNI_obj nrmlzPerChngResult) AFNI_PerChngTS (string baseName, AFNI_obj volregdInput){
    (despikeResult) = AFNI_despike(baseName, volregdInput);
    (meanResult) = AFNI_mean(despikeResult, baseName);
    (nrmlzPerChngResult) = AFNI_normalizePerChng(despikeResult, meanResult, baseName);
}
 

string declarelist[] = ["PK1"];
foreach subject in declarelist{
    int runs[] = [2:9];
    foreach run in runs{
        string baseName = @strcat(subject,".run",run);
        AFNI_obj inputTS<simple_mapper; prefix=@strcat("ts.",run,"+orig.")>;
        AFNI_obj volregResult<simple_mapper;prefix=@strcat("volreg.",baseName,"+orig.")>;
        file resultVolregMotion<single_file_mapper; file=@strcat("mot_",baseName)>;
        AFNI_obj volregRefTS<simple_mapper; prefix=@strcat("ts.5+orig.")>;
        string volregBaseString = @strcat("ts.5");
        AFNI_obj despikeResult<simple_mapper; prefix=@strcat("despiked.",baseName,"+orig.")>;
        AFNI_obj meanResult<simple_mapper; prefix=@strcat("mean.",baseName,"+orig.")>;
        AFNI_obj nrmlzPerChngResult<simple_mapper; prefix=@strcat("nrmlzdPerChng.",baseName,"+orig.")>;
        (volregResult, resultVolregMotion) = AFNI_volreg(baseName, inputTS, volregRefTS, volregBaseString);
        (despikeResult, meanResult, nrmlzPerChngResult) = AFNI_PerChngTS(baseName, volregResult);
    }
    string subj = @strcat(subject);
    AFNI_obj PerCh2<simple_mapper; prefix=@strcat("nrmlzdPerChng.",subject,".run2+orig.")>;
    AFNI_obj PerCh3<simple_mapper; prefix=@strcat("nrmlzdPerChng.",subject,".run3+orig.")>;
    AFNI_obj PerCh4<simple_mapper; prefix=@strcat("nrmlzdPerChng.",subject,".run4+orig.")>;
    AFNI_obj PerCh5<simple_mapper; prefix=@strcat("nrmlzdPerChng.",subject,".run5+orig.")>;
    AFNI_obj PerCh6<simple_mapper; prefix=@strcat("nrmlzdPerChng.",subject,".run6+orig.")>;
    AFNI_obj PerCh7<simple_mapper; prefix=@strcat("nrmlzdPerChng.",subject,".run7+orig.")>;
    AFNI_obj PerCh8<simple_mapper; prefix=@strcat("nrmlzdPerChng.",subject,".run8+orig.")>;
    AFNI_obj PerCh9<simple_mapper; prefix=@strcat("nrmlzdPerChng.",subject,".run9+orig.")>;
    AFNI_obj TScatResult<simple_mapper; prefix=@strcat("nrmlzdPerChng_TScat.",subject,"+orig.")>;
    (TScatResult) = AFNI_TScat(subj, PerCh2, PerCh3, PerCh4, PerCh5, PerCh6, PerCh7, PerCh8, PerCh9);
}
