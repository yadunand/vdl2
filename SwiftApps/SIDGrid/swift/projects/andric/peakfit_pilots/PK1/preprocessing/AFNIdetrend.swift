#---- doing 3dDetrend
type file{}

type AFNI_obj{
    file HEAD;
    file BRIK;
}

app (AFNI_obj detrendResult) AFNI_detrend (string outPrefix, AFNI_obj inputTS){
    AFNI_3dDetrend "-prefix" outPrefix "-polort" 3 @filename(inputTS.BRIK);
}


string declarelist[] = ["PK1"];
foreach subject in declarelist{
    int runs[] = [2:9];
    foreach run in runs{
        string baseName = @strcat(subject,".run",run);
        AFNI_obj inputTS<simple_mapper; prefix=@strcat("nrmlzdPerChng.",baseName,"+orig.")>;
        string outPrefix = @strcat("Dtrnd.nrmlzdPerChng.",baseName);
        AFNI_obj detrendResult<simple_mapper; prefix=@strcat("Dtrnd.",@filename(inputTS))>;
        detrendResult = AFNI_detrend(outPrefix, inputTS);
    }
}
