#---- pinche deconvolution
type file{}

type AFNI_obj{
    file HEAD;
    file BRIK;
}

type stims{
    string labels[];
    string fnames[];
}

app (AFNI_obj deconvBucketResult, AFNI_obj irfResult, file deconvXsave) AFNI_pincheDeconvolution (AFNI_obj normalized, stims stimuli, file concat, file motionfile, file designmatrix, string outPrefix){
    AFNI_3dDeconvolve "-input" @filename(normalized.BRIK) "-polort" 3 "-tshift" "-num_stimts" 8
    "-stim_file" 1 stimuli.fnames[0] "-stim_label" 1 stimuli.labels[0]
    "-stim_file" 2 stimuli.fnames[1] "-stim_label" 2 stimuli.labels[1]
    "-stim_file" 3 stimuli.fnames[2] "-stim_base" 3 "-stim_label" 3 stimuli.labels[2]
    "-stim_file" 4 stimuli.fnames[3] "-stim_base" 4 "-stim_label" 4 stimuli.labels[3]
    "-stim_file" 5 stimuli.fnames[4] "-stim_base" 5 "-stim_label" 5 stimuli.labels[4]
    "-stim_file" 6 stimuli.fnames[5] "-stim_base" 6 "-stim_label" 6 stimuli.labels[5]
    "-stim_file" 7 stimuli.fnames[6] "-stim_base" 7 "-stim_label" 7 stimuli.labels[6]
    "-stim_file" 8 stimuli.fnames[7] "-stim_base" 8 "-stim_label" 8 stimuli.labels[7]
    "-stim_maxlag" 1 8
    "-stim_maxlag" 2 8
    "-iresp" 1 @strcat(outPrefix,".IRF")
    "-full_first" "-tout" "-nodmbase" "-xsave" "-nfirst" "0" "-concat" @filename(concat) "-bucket"
    outPrefix;
}


string declarelist[] = ["PK1"];
foreach subject in declarelist{
    ## map inputs
    file designmatrix<single_file_mapper; file="for3dDeconvolve.ntp200TR1.5faces-002.1D">;
    file motionfile<single_file_mapper; file=@strcat("motion_",subject,"visual.1D")>;
    file concat<single_file_mapper; file=@strcat("concat_runs",subject,".1D")>;
    AFNI_obj normalized<simple_mapper; prefix=@strcat("visualTScat_nrmlzdPerChng.",subject,"+orig.")>;

    ## map outputs
    string outPrefix = @strcat("visualDeconvResults_",subject);
    file xsave<single_file_mapper; file=@strcat(outPrefix,".xsave")>;
    AFNI_obj bucket<simple_mapper; prefix=@strcat(outPrefix,"+orig.")>;
    AFNI_obj irf<simple_mapper; prefix=@strcat(outPrefix,".IRF+orig.")>;

    #---- stimuli info
    #---- 2 conditions: bianconeri ('white and black') and color squares
    stims stimuli;
    stimuli.labels = ["bianconeri","color","roll","pitch","yaw","dx","dy","dz"];
    stimuli.fnames = [@strcat(@filename(designmatrix),"[0]"),@strcat(@filename(designmatrix),"[1]"),
              @strcat(@filename(motionfile),"[1]"),@strcat(@filename(motionfile),"[2]"),
              @strcat(@filename(motionfile),"[3]"),@strcat(@filename(motionfile),"[4]"),
              @strcat(@filename(motionfile),"[5]"),@strcat(@filename(motionfile),"[6]")];

    (bucket, irf, xsave) = AFNI_pincheDeconvolution(normalized, stimuli, concat, motionfile, designmatrix, outPrefix);
}
