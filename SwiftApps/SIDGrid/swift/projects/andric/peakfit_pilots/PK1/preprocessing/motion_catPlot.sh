#!/bin/tcsh

echo "cat motion"
#cat mot_PK1.run2 mot_PK1.run3 mot_PK1.run4 mot_PK1.run5 mot_PK1.run6 mot_PK1.run7 mot_PK1.run8 mot_PK1.run9 > motion_PK1.1D
cat mot_PK1.run2 mot_PK1.run3 mot_PK1.run4 mot_PK1.run5 > motion_PK1audio.1D
cat mot_PK1.run6 mot_PK1.run7 mot_PK1.run8 mot_PK1.run9 > motion_PK1visual.1D

echo "now plot motion rotations and translations"
#1dplot -ps -volreg -xlabel 'Time Point' 'motion_PK1.1D[1-6]' > motion_PK1.ps
1dplot -ps -volreg -xlabel 'Time Point' 'motion_PK1audio.1D[1-6]' > motion_PK1audio.ps
1dplot -ps -volreg -xlabel 'Time Point' 'motion_PK1visual.1D[1-6]' > motion_PK1visual.ps
