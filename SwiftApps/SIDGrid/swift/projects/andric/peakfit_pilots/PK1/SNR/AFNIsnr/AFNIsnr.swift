#---- for doing SNR on the PK001 afni data
type file{}

type AFNI_obj{
file HEAD;
file BRIK;
};


app (AFNI_obj volregResult, file resultVolregMotion) AFNI_volreg (string baseName, AFNI_obj inputTS, AFNI_obj referenceTS, string volregBaseString){
    AFNI_3dvolreg "-twopass" "-twodup" "-dfile" @strcat("mot_",baseName) "-base" @strcat(volregBaseString,"+orig[199]") "-prefix" @strcat("volreg.",baseName) @inputTS.BRIK;
}

app (AFNI_obj meanResult) AFNI_mean (AFNI_obj meanInput, string baseName){
    AFNI_3dTstat "-mean" "-prefix" @strcat("./mean.",baseName) @meanInput.BRIK;
}

app (AFNI_obj stdevResult) AFNI_stdev (AFNI_obj stdevInput, string baseName){
    AFNI_3dTstat "-stdev" "-prefix" @strcat("./stdev.",baseName)  @stdevInput.BRIK;
}

app (AFNI_obj detrendResult) AFNI_detrend (AFNI_obj detrendInput, string baseName){
    AFNI_3dDetrend "-polort" "3" "-prefix" @strcat("detrend.",baseName) @detrendInput.BRIK;
}

app (AFNI_obj ratioResult) AFNI_doratio (AFNI_obj stdevResult, AFNI_obj meanResult , string baseName){
    AFNI_3dcalc "-verbose" "-a" @meanResult.BRIK "-b" @stdevResult.BRIK "-expr" "(a/b)" "-prefix" @strcat("snr.",baseName);
}

(AFNI_obj detrendResult, AFNI_obj stdevResult, AFNI_obj meanResult, AFNI_obj ratioResult) AFNI_snr (string baseName, AFNI_obj inputTS){
    (meanResult) = AFNI_mean(inputTS, baseName);
    (detrendResult) = AFNI_detrend(inputTS, baseName);
    (stdevResult) = AFNI_stdev(detrendResult, baseName);
    (ratioResult)  =  AFNI_doratio(stdevResult, meanResult, baseName);
}


string declarelist[] = ["testPK1"];
foreach subject in declarelist{
    int runs[] = [2:9];
    foreach run in runs{
        AFNI_obj srun<simple_mapper; prefix=@strcat("ts.",run,"+orig.")>;
        string baseName = @strcat(subject,".run",run);
        AFNI_obj volregResult<simple_mapper;prefix=@strcat("volreg.",baseName,"+orig.")>;
        AFNI_obj meanResult<simple_mapper;prefix=@strcat("mean.",baseName,"+orig.")>;
        AFNI_obj stdevResult<simple_mapper;prefix=@strcat("stdev.",baseName,"+orig.")>;
        AFNI_obj detrendResult<simple_mapper;prefix=@strcat("detrend.",baseName,"+orig.")>;
        AFNI_obj ratioResult<simple_mapper;prefix=@strcat("snr.",baseName,"+orig.")>;
        // begin set up volreg parameters
        file resultVolregMotion<single_file_mapper; file=@strcat("mot_",baseName)>;
        AFNI_obj volregRefTS<simple_mapper; prefix=@strcat("ts.5+orig.")>;
        string volregBaseString = @strcat("ts.5");
        (volregResult, resultVolregMotion) = AFNI_volreg(baseName, srun, volregRefTS, volregBaseString);
        (detrendResult, stdevResult, meanResult, ratioResult) = AFNI_snr(baseName, volregResult);
    }
}
