type file{}
#--- for doing simulations on the peak analysis pilot
#--- original coding 27.Jan.2009

app (file simResult) simScript (file scriptFile, file inputFile, int iter, float tval){
    RInvoke  @filename(scriptFile) @filename(inputFile) iter tval;
}


genSims(){
    file script<"scripts/SNRsimulations.R">;
    file inputData<"speech_whitenoise_pasted.txt">;
    
    int SNRstarts[] = [30:110:1];
    foreach snr in SNRstarts{
        float ThreshVals[] = [1.98, 2.59];
        foreach val in ThreshVals{
            
            string outResultName = @strcat("results/iter",snr,"_tval",val,"_forSNR.txt");
            file outResult<single_file_mapper; file=outResultName>;
            (outResult)=simScript(script,inputData,snr,val);
        }
    }
}

genSims();
