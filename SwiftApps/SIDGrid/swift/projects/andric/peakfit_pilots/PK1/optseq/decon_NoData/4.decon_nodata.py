#!/usr/bin/python

import sys
import os
import commands


parSets = range(1,4)

def DeconvoleNoData(p):
    print commands.getoutput("3dDeconvolve -nodata \
    -nlast 199 -polort 3 -num_stimts 2 \
    -stim_file 1 waver_out_par"+`p`+"_bianconeriCond.1D -stim_label 1 bianconeri \
    -stim_file 2 waver_out_par"+`p`+"_colorCond.1D -stim_label 2 color \
        > deconNodata2_par"+`p`+".out")


for p in parSets:
    DeconvoleNoData(p)
