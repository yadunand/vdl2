dd <- read.table("ntp226Run1V1-004.par")
 limdd <- dd[,2:3]
# REMEMBER to set TR here
 tr = 1
 # repeat 0s as many times as conditions in the study
 row.template <- rep(0,6)

myexpantion <- function(x) {
	cond <- x[1]
	length <- x[2]/tr
	if (cond == 0) {
		for (i in 1:length) {
		print(row.template)
		}
	}
	if (cond > 0) {
		for (i in 1:length) {
		currentline <- row.template
		currentline[cond] <- 1
		print(currentline)
		rm(currentline)
		}
	}
}

sink(file="for3dDecon.ntp226Run1V1-004.par")
apply(limdd, 1, myexpantion)
sink()
