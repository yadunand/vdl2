#!/usr/bin/python

import commands
import sys
import os

columns = {"bianconeri":0, "color":1}
parSets = range(1,4)

#os.chdir(os.getenv("gpfscls")+"/subjects/NL-CLS-006/scripts_runbyrun/decon_matrix/")
print os.getcwd()

for p in parSets:
    for col in columns:
        print "doing: "+col+" column number: "+`columns[col]`+"\n"
        print "waver -GAM -dt 1.5 -numout 200 -input for3dDecon.ntp200TR1.5faces-00"+`p`+".1D["+`columns[col]`+"] > waver_out_par"+`p`+"_"+col+"Cond.1D"
        print commands.getoutput("waver -GAM -dt 1.5 -numout 200 -input for3dDecon.ntp200TR1.5faces-00"+`p`+".1D["+`columns[col]`+"] > waver_out_par"+`p`+"_"+col+"Cond.1D")
