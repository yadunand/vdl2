#!/usr/bin/python

import commands
import sys
import os

columns = {"speech":0, "whitenoise":1}

#os.chdir(os.getenv("gpfscls")+"/subjects/NL-CLS-006/scripts_runbyrun/decon_matrix/")
print os.getcwd()

for col in columns:
    print "doing: "+col+" column number: "+`columns[col]`+"\n"
    print "waver -GAM -dt 1.5 -numout 200 -input for3dDecon.ntp200TR1.5faces-002.par["+`columns[col]`+"] > waver_out_sound_"+col+".1D"
    print commands.getoutput("waver -GAM -dt 1.5 -numout 200 -input for3dDecon.ntp200TR1.5faces-002.par["+`columns[col]`+"] > waver_out_sound_"+col+".1D")
