#!/usr/bin/python

import commands
import os
import time

print "optseq beginning: "+time.ctime()
print commands.getoutput("optseq2 \
    --ntp 200 \
    --tr 1.5 \
    --psdwin 0 18 \
    --ev face 1.5 50\
    --ev scrambled 1.5 50\
    --polyfit 2 \
    --focb 1500 \
    --nkeep 3 \
    --nsearch 10000 \
    --o ntp200TR1.5faces")

print "optseq is done: "+time.ctime()
