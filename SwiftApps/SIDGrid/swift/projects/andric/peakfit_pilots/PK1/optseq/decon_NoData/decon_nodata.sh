#!/bin/tcsh

#-- deconvolve with no data
3dDeconvolve -nodata \
-nlast 799 \
-polort 3 \
-num_stimts 2 \
-stim_file 1 ../for3dDeconvolve.ntp200TR1.5faces-002.1D\[0\] -stim_label 1 bianconeri \
-stim_file 2 ../for3dDeconvolve.ntp200TR1.5faces-002.1D\[1\] -stim_label 2 color \
-stim_maxlag 1 8 \
-stim_maxlag 2 8 \
 > decon_nodata.out
