dd <- read.table("ntp200TR1.5faces-002.par")
limdd <- dd[,2:3]
tr = 1.5
#---- repeat 0s as many times as conditions in the study
#--- here just doing for presence of speech sound only, so will make all non-rest == 1
limdd[,1][which(limdd[,1] > 0)] = 1
row.template <- rep(0,1)

myexpantion <- function(x){
    cond <- x[1]
    length <- x[2]/tr
    if (cond == 0){
        for (i in 1:length){
            print(row.template)
        }
    }
    if (cond > 0){
        for (i in 1:length){
            currentline <- row.template
            currentline[cond] <- 1
            print(currentline)
            rm(currentline)
        }
    }
}

sink(file="for3dDecon.ntp200TR1.5faces-002.par")
apply(limdd, 1, myexpantion)
sink()
