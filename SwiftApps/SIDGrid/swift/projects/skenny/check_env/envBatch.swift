type file {}						//define a type for file
(file t) env () { 			//procedure declaration
    app {
        env stdout=@filename(t);	//redirect stdout to a file
    }
}

file e = env();
file f = env();
file g = env();
file h = env();
file i = env();
file j = env();
file k = env();
file l = env();
file m = env();
file n = env();
