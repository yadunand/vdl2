type file {}						//define a type for file
(file t) env () { 			//procedure declaration
    app {
        env stdout=@filename(t);	//redirect stdout to a file
    }
}

file e = env();		//procedure call
