###  generate openmx models in parallel
###  by number of connections

type file{}

# produce .RData file for each model object to be tested

(file mxModels) mxmodel_generator (int conn, int ncol, file dot_r, file sdat, file spdat){
    app{
	RInvoke @filename(dot_r) conn ncol @filename(sdat) @filename(spdat);
    }
}

# map input and output for generator


file modelgen<single_file_mapper;file="scripts/mxModelGen.R">;
file sdat<single_file_mapper;file="matrices/s.dat">;
file spdat<single_file_mapper;file="matrices/sp.dat">;

	file potentialmodels<single_file_mapper;file=@strcat("models.tar")>;
	(potentialmodels) = mxmodel_generator(4, 4, modelgen, sdat, spdat);




