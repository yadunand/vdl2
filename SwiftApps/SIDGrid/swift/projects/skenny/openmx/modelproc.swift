type file{}

# query database for timeseries data and pass to 
# R script to calculate observed covariance and
# run model (RAM)

(file fitmodel, file func_value) simple_covariance (string allcatargs, file config, file dot_r, file mxmodel){
    app{
        Mediator allcatargs @filename(dot_r) @filename(mxmodel);
    }
}

model_data(string query, string cond, int modnum, file r_script){
	file pmod<single_file_mapper; file=@strcat("modelObjects/",modnum,"_model.rdata")>;
	string db = "EMBLEM1";
	string host = "tp-neurodb.ci.uchicago.edu";
	file config<single_file_mapper; file="user.config">;
	string user = @arg("user");
	string r_swift_args = @strcat(modnum,"_model.rdata");
	string med_args = @strcat("--user ", user,
    		" --conf ", @filename(config),
    		" --db ", db,
    		" --host ", host,
    		" --query ", query,
    		" --r_script ", @filename(r_script),
    		" --r_swift_args ", r_swift_args,
    		" --begin_vox ", 1,
		" --end_vox ", 2,
		" --batchstep ", 1,
		" --outprefix ", @strcat(modnum,"_modeldata"),
    		" --subject ", "model");
	file resmod <single_file_mapper; file=@strcat(cond,"/",modnum,"/","model.rdata")>;
	file modmin <single_file_mapper;file=@strcat(cond,"/",modnum,"/","function.min")>;
	(resmod, modmin) = simple_covariance(med_args, config, r_script, pmod);
}

#............................main................................................

# generate queries based on condition and roi's

string conditions = ["gestspeech", "speech", "gesture", "grasp"];
string queries[];
foreach cond,c in conditions{
	queries[c] = @strcat("SELECT avg(",cond,"0B), avg(",cond,"1B), avg(",cond,"2B), avg(",cond,"3B), avg(",cond,"4B), avg(",cond,"5B), avg(",cond,"6B), avg(",cond,"7B), avg(",cond,"8B) FROM emblemfemlh where roi = 42 or roi = 34 or roi = 33 or roi = 60 group by roi;");	
}

file cov_script<single_file_mapper; file="scripts/SimpleCovariance.R">;

int models = [1:2];

foreach m in models{
	foreach query,q in queries {
		model_data(query,conditions[q],m,cov_script);
		}
	    }


