type Mol {}
type Bin {}
type Common {}
type DOCKOut {}
type Molout{}
type DockRunSummary {}
type file {}

(external bout)bcast (Bin binary, Common flex_def, Common flex_tbl, Common grid_bmp, Common
grid_in, Common grid_nrg, Common grid_out, Common rec_box, Common receptor,
Common sample_grid, Common selected_spheres, Common template_in, Common vdw,
Common awkscript)
{
        app {
            bcast @filename(binary)  @filename(flex_def)
            @filename(flex_tbl) @filename(grid_bmp) @filename(grid_in)
            @filename(grid_nrg) @filename(grid_out) @filename(rec_box)
            @filename(receptor) @filename(sample_grid)
	    @filename(selected_spheres)
            @filename(template_in) @filename(vdw) @filename(awkscript);
        }
}

app (DOCKOut ofile, Molout moloutfile) rundock_zhao (Mol molfile, Bin binary,
Common flex_def, Common flex_tbl, Common grid_bmp, Common grid_in, Common
grid_nrg, Common grid_out, Common rec_box, Common receptor, Common sample_grid, Common selected_spheres,
Common template_in, Common vdw, Common awkscript, external bout)
{
   rundock_zhao @filename(molfile) @filename(binary) @filename(flex_def)
   @filename(flex_tbl) @filename(grid_bmp) @filename(grid_in)
   @filename(grid_nrg) @filename(grid_out) @filename(rec_box)
   @filename(receptor) @filename(sample_grid) @filename(selected_spheres)
   @filename(template_in) @filename(vdw) @filename(awkscript) stdout=@filename(ofile);
}

app (DockRunSummary finalfile) sumdockresults(DOCKOut r[] )
{
   summary @filenames(r) stdout=@filename(finalfile);
}


Mol texts[] <filesys_mapper;location="input/", suffix=".mol2">;
//Mol texts[] <filesys_mapper;suffix=".mol2">;


(DOCKOut result[])doall(Mol texts[])
{
  Bin binary <"common/dock6.O3.cn">;
  Common flex_def <"common/flex.defn">;
  Common flex_tbl <"common/flex_drive.tbl">;
  Common grid_bmp <"common/grid.bmp">;
  Common grid_in <"common/grid.in">;
  Common grid_nrg <"common/grid.nrg">;
  Common grid_out <"common/grid.out">;
  Common rec_box <"common/rec_box.pdb">;
  Common receptor <"common/receptor_charged.mol2">;
  Common sample_grid <"common/sample_grid.in">;
  Common selected_spheres <"common/selected_spheres.sph">;
  Common template_in <"common/template.in">;
  Common vdw <"common/vdw_AMBER_parm99.defn">;
  Common awkscript <"common/awkscript">;

  external bout;

  bout=bcast(binary, flex_def, flex_tbl, grid_bmp, grid_in, grid_nrg, grid_out, rec_box, receptor, sample_grid, selected_spheres, template_in, vdw, awkscript);

  foreach p,i in texts {
    /*DOCKOut r <regexp_mapper;  
             source=@p,  
             match="input/(.*)\\.mol2",  
             transform="result/\\1.out"  
    >;*/
    
    Molout mof <regexp_mapper;  
             source=@p,  
             match="input/(.*)\\.mol2",  
             transform="result/\\1.mol2.scored"  
    >;

  (result[i], mof) = rundock_zhao(p, binary, flex_def, flex_tbl, grid_bmp, grid_in, grid_nrg, grid_out, rec_box, receptor, sample_grid, selected_spheres, template_in, vdw, awkscript, bout);
  }

  
}
 
// Main

DockRunSummary summary <"summary.txt">;
DOCKOut result[];
result=doall(texts);
summary = sumdockresults(result);
