# this script must be invoked inside of bash, not plain sh
PATH=/fuse/bin:/fuse/usr/bin:$PATH
infosection() {
	echo >& "$INFO"
	echo "_____________________________________________________________________________" >& "$INFO"
	echo >& "$INFO"
	echo "        $1" >& "$INFO" 
	echo "_____________________________________________________________________________" >& "$INFO"
	echo >& "$INFO"
}

info() {
	infosection "uname -a"
	uname -a 2>&1 >& "$INFO"
	infosection "id"
	id 2>&1 >& "$INFO"
	infosection "env"
	env 2>&1 >& "$INFO"
	infosection "df"
	df 2>&1 >& "$INFO"
	infosection "/proc/cpuinfo"
	cat /proc/cpuinfo 2>&1 >& "$INFO"
	infosection "/proc/meminfo"
	cat /proc/meminfo 2>&1 >& "$INFO"
	infosection "command line"
	echo $COMMANDLINE 2>&1 >& "$INFO"
}

logstate() {
	echo "Progress " `date +"%Y-%m-%d %H:%M:%S"` " $@" >& "$INFO"
}

log() {
	echo "$@" >& "$INFO"
}

fail() {
	EC=$1
	shift
	log $@
	info
	exit $EC
}

checkError() {
	if [ "$?" != "0" ]; then
		fail $@
	fi
}

checkEmpty() {
	if [ "$1" == "" ]; then
		shift
		fail 254 $@
	fi
}

getarg() {
	NAME=$1
	shift
	VALUE=""
	SHIFTCOUNT=0
	if [ "$1" == "$NAME" ]; then
		shift
		let "SHIFTCOUNT=$SHIFTCOUNT+1"
		while [ "${1:0:1}" != "-" ] && [ "$#" != "0" ]; do
			VALUE="$VALUE $1"
			shift
			let "SHIFTCOUNT=$SHIFTCOUNT+1"
		done
	else
		fail 254 "Missing $NAME argument"
	fi
	VALUE="${VALUE:1}"
}

openinfo() {
	exec 3<> $1
	INFO=3
}

closeinfo() {
	exec 3>&-
}

cioinput() {
    INPUT=$1
    FILEPATH=`dirname $INPUT`
    FILENAME=`basename $INPUT`
    TYPE=${INPUT%%/*}
    echo "INPUT_TYPE: $TYPE" >> /dev/shm/cio
    if [ "$TYPE" == "common" ] && [ -e /dev/shm/share/$FILENAME ]; then
	echo "cioinput(): link for common input $INPUT" >> /dev/shm/cio
	ln -s "/dev/shm/share/$FILENAME" "$DIR/$L"
    elif [ "$TYPE" == "_concurrent" ]; then
	echo "cioinput(): toruscp for intermediate data $INPUT" >> /dev/shm/cio
	mkdir -p $FILEPATH
	echo "cioinput(): $DIR/$FILEPATH" >> /dev/shm/cio
	/home/zzhang/cio/bin/toruscp.sh $FILENAME $DIR/$FILEPATH
    else
	echo "cioinput(): copy from GPFS $INPUT" >> /dev/shm/cio
	cp "$PWD/shared/$L" "$DIR/$L"
    fi
}
ciooutput() {
    OUTPUT=$1
    FILEPATH=`dirname $OUTPUT`
    FILENAME=`basename $OUTPUT`
    TYPE=${OUTPUT%%/*}
    echo "OUTPUT_TYPE: $TYPE" >> /dev/shm/cio

    if [ "$TYPE" == "_concurrent" ]; then
	echo "ciooutput(): write intermediate data $OUTPUT" >> /dev/shm/cio
	/home/zzhang/cio/bin/chtput.rb $FILENAME $RANK
	cp $OUTPUT /dev/shm/share/
    else
	echo "ciooutput(): write regular data $OUTPUT" >> /dev/shm/cio
	#dd if="$OUTPUT" of="$WFDIR/shared/$OUTPUT" bs=128k
	echo "$OUTPUT /chirp/multi/${CHIRP_ADD}@stripe/" >> /dev/shm/chirp_add
	cp "$OUTPUT" /chirp/multi/${CHIRP_ADD}@stripe/
    fi
}
echo $@ >> /dev/shm/log
RANK=`echo $CONTROL_INIT | awk -F, '{print $4}'`
echo "RANK: $RANK" >> /dev/shm/log
COMMANDLINE=$@
WFDIR=$PWD
ID=$1
checkEmpty "$ID" "Missing job ID"

shift

getarg "-jobdir" "$@"
JOBDIR=$VALUE
shift $SHIFTCOUNT

checkEmpty "$JOBDIR" "Missing job directory prefix"
mkdir -p /dev/shm/swift-info/$JOBDIR

closeinfo
openinfo "/dev/shm/swift-info/$JOBDIR/${ID}-info"
#openinfo "/dev/null"

logstate "LOG_START"

getarg "-e" "$@"
EXEC=$VALUE
shift $SHIFTCOUNT

getarg "-out" "$@"
STDOUT=$VALUE
shift $SHIFTCOUNT

getarg "-err" "$@"
STDERR=$VALUE
shift $SHIFTCOUNT

getarg "-i" "$@"
STDIN=$VALUE
shift $SHIFTCOUNT

getarg "-d" "$@"
DIRS=$VALUE
shift $SHIFTCOUNT

getarg "-if" "$@"
INF=$VALUE
shift $SHIFTCOUNT

getarg "-of" "$@"
OUTF=$VALUE
shift $SHIFTCOUNT

getarg "-k" "$@"
KICKSTART=$VALUE
shift $SHIFTCOUNT

if [ "$1" == "-a" ]; then
	shift
else
	fail 254 "Missing arguments (-a option)"
fi

if [ "X$SWIFT_JOBDIR_PATH" != "X" ]; then
  DIR=${SWIFT_JOBDIR_PATH}/$JOBDIR/$ID
  COPYNOTLINK=1
else
  DIR=/dev/shm/swift-work/$JOBDIR/$ID
  COPYNOTLINK=0
fi

PATH=$PATH:/bin:/usr/bin

if [ "$PATHPREFIX" != "" ]; then
export PATH=$PATHPREFIX:$PATH
fi

IFS="^"

logstate "CREATE_JOBDIR"
mkdir -p $DIR

logstate "CREATE_INPUTDIR"

for D in $DIRS ; do
    mkdir -p "$DIR/$D"
    checkError 254 "Failed to create input directory $D"
done

#cd $DIR
logstate "LINK_INPUTS"
for L in $INF ; do
	if [ $COPYNOTLINK = 1 ]; then
	        cp "$PWD/shared/$L" "$DIR/$L"
		checkError 254 "Failed to copy input file $L"
	else
	    cioinput $L
	        #cp "$PWD/shared/$L" "$DIR/$L"
		checkError 254 "Failed to link input file $L"
	fi
done

logstate "EXECUTE"

cd $DIR

if [ "$KICKSTART" == "" ]; then
	if [ "$STDIN" == "" ]; then
	    "$EXEC" "$@" 1>"$STDOUT" 2>"$STDERR"
	else
	    "$EXEC" "$@" 1>"$STDOUT" 2>"$STDERR" <"$STDIN"
	fi
	checkError $? "Exit code $?"
else
	if [ ! -f "$KICKSTART" ]; then
		fail 254 "The Kickstart executable ($KICKSTART) was not found"		
	elif [ ! -x "$KICKSTART" ]; then
		fail 254 "The Kickstart executable ($KICKSTART) does not have the executable bit set"
	else
		mkdir -p $WFDIR/kickstart/$JOBDIR
		if [ "$STDIN" == "" ]; then
			"$KICKSTART" -H -o "$STDOUT" -e "$STDERR" "$TMPEXEC" "$@" 1>kickstart.xml 2>"$STDERR"
		else
			"$KICKSTART" -H -o "$STDOUT" -i "$STDIN" -e "$STDERR" "$TMPEXEC" "$@" 1>kickstart.xml 2>"$STDERR"
		fi
		export APPEXIT=$?
		mv -f kickstart.xml "$WFDIR/kickstart/$JOBDIR/$ID-kickstart.xml" 2>&1 >& "$INFO"
		checkError 254 "Failed to copy Kickstart record to shared directory"
		if [ "$APPEXIT" != "0" ]; then
			fail $APPEXIT "Exit code $APPEXIT"
		fi
	fi
fi

logstate "EXECUTE_DONE"

MISSING=
for O in $OUTF ; do
	if [ ! -f "$DIR/$O" ]; then
		if [ "$MISSING" == "" ]; then
			MISSING=$O
		else
			MISSING="$MISSING, $O"
		fi
	fi
done
if [ "$MISSING" != "" ]; then
	fail 254 "The following output files were not created by the application: $MISSING"
fi

logstate "COPYING_OUTPUTS"
for O in $OUTF ; do
	#cp "$DIR/$O" "$WFDIR/shared/$O" 2>&1 >& "$INFO"
        #cp "$DIR/$O" "$WFDIR/shared/$O"
        #dd if="$DIR/$O" of="$WFDIR/shared/$JOBDIR/$O" bs=128k
        #dd if="$DIR/$O" of="$WFDIR/shared/$O" bs=128k
        ciooutput $O
	checkError 254 "Failed to copy output file $O to shared directory"
done

logstate "RM_JOBDIR"

closeinfo
#rm -f "$WFDIR/info/$JOBDIR/${ID}-info"
#echo "$WFDIR/info/$JOBDIR/${ID}-info" >> /dev/shm/log
#mkdir -p "$WFDIR/info/$JOBDIR/"
#dd if=/dev/shm/swift-info/$JOBDIR/${ID}-info of="$WFDIR/info/$JOBDIR/${ID}-info" bs=128k
#dd if=/dev/shm/swift-info/$JOBDIR/${ID}-info of="/fuse/tmp/${ID}-info" bs=128k
