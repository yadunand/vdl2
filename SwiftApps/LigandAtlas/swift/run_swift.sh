#!/bin/bash

 if [ -z "$4" ]; then 
              echo "usage: $0 <FALKON_ID> <NUM_NODES> <swift_script> <working_dir>"
              exit 1
          fi

CUR_DIR=`pwd`
                       

SERVICE_LIST_FILE=/home/falkon/users/${USER}/${1}/config/Client-service-URIs.config

                                               
NUM_NODES=$2
SWIFT_SCRIPT=$3
WORKING=$4
let NUM_ION=NUM_NODES/64

echo "waiting for at least ${NUM_NODES} nodes to register before submitting workload..."
falkon-wait-for-allocation.sh ${SERVICE_LIST_FILE} ${NUM_ION}
                                
echo "found at least ${NUM_NODES} registered, submitting workload..."

rm -rf sites.xml

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<config xmlns=\"http://www.griphyn.org/chimera/GVDS-PoolConfig\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.5\">" > sites.xml

cat $SERVICE_LIST_FILE | awk -v var=$WORKING 'NR>1 {printf "<pool handle=\"bgp%.3d\">\n\t<gridftp  url=\"local://localhost\"/>\n\t<execution provider=\"deef\" url=\"http://%s:50001/wsrf/services/GenericPortal/core/WS/GPFactoryService\"/>\n\t<workdirectory>%s</workdirectory>\n\t<profile namespace=\"karajan\" key=\"jobThrottle\">8</profile>\n\t<profile namespace=\"karajan\" key=\"initialScore\">1000</profile>\n</pool>\n\n", NR-2, $(1), var}' >> sites.xml

echo "</config>" >> sites.xml

time swift -sites.file ./sites.xml -tc.file ./tc.data $3
