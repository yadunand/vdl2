#!/fuse/bin/bash

if [ -z "$2" ]; then
    echo "usage: $0 <FileName> <Path>"
    echo "usage: $0 hello /dev/shm"
    exit 1
fi

FILENAME=$1
FILEPATH=$2
echo "Full name : $FILENAME"
FILE=`basename $FILENAME`
echo "FILE name: $FILENAME"
RANK=`/home/zzhang/cio/bin/chtget.rb $FILENAME`
echo "RANK: $RANK"
IP=`/home/zzhang/cio/bin/ipcal 10.128.0.0 $RANK`
echo "IP: $IP"
/home/zzhang/chirp/bin/chirp_get -a address $IP $FILE $FILEPATH/$FILE
