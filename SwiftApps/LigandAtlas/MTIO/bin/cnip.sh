#!/fuse/bin/bash
LD_LIBRARY_PATH=/lib:/fuse/lib:/fuse/usr/lib
PATH=/fuse/bin:/fuse/usr/bin:$PATH

/home/iskra/ZeptoOS/packages/cnip/prebuilt/cn-ipfwd.2409 &

while [ ! -f /tmp/ifconfig.cmd ]; do
    sleep 1
done

. /tmp/ifconfig.cmd

RANK=`echo $CONTROL_INIT | awk -F, '{print $4}'`

if [ -d /dev/shm/share ];
    then
    rm -rf /dev/shm/share 
fi
mkdir /dev/shm/share
echo "address:192.168.1.* rwlda" >>/dev/shm/share/.__acl
echo "address:10.* rwlda" >> /dev/shm/share/.__acl
echo "address:127.0.0.1 rwlda" >> /dev/shm/share/.__acl

/home/zzhang/chirp/bin/chirp_server -r /dev/shm/share &

if [ "$RANK" = "0" ];
then
    /home/zzhang/cio/bin/hashserver.rb &
else    
    /home/zzhang/falkon/worker/run.worker-c-bgp.sh $1 $2 $3 $4 $5 $6 $7
fi

echo $RANK > /dev/shm/rank
sleep 3600
