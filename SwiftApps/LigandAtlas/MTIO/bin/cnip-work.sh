#!/fuse/bin/bash
LD_LIBRARY_PATH=/lib:/fuse/lib:/fuse/usr/lib
PATH=/fuse/bin:/fuse/usr/bin:$PATH

#Torus START
/home/iskra/ZeptoOS/packages/cnip/prebuilt/cn-ipfwd.2409 &

while [ ! -f /tmp/ifconfig.cmd ]; do
    sleep 1
done

. /tmp/ifconfig.cmd

#IP Calculation
RANK=`echo $CONTROL_INIT | awk -F, '{print $4}'`
FIRST=`expr $RANK % 256`
SECOND=`expr $RANK / 256`

BASE=`expr $FIRST / 64`
MODULO=`expr $FIRST % 64`
CHIRP_BASE=`expr $BASE \* 64`
CHIRP_1=`expr $CHIRP_BASE + 1`
CHIRP_2=`expr $CHIRP_BASE + 2`
CHIRP_3=`expr $CHIRP_BASE + 3`
CHIRP_4=`expr $CHIRP_BASE + 4`
export CHIRP_ADD=10.128.$SECOND.$CHIRP_1
#Chirp START
if [ -d /dev/shm/share ];
    then
    rm -rf /dev/shm/share 
fi
mkdir /dev/shm/share
echo "address:192.168.1.* rwlda" >>/dev/shm/share/.__acl
echo "address:10.* rwlda" >> /dev/shm/share/.__acl
echo "address:127.0.0.1 rwlda" >> /dev/shm/share/.__acl

/home/zzhang/chirp/bin/chirp_server -r /dev/shm/share &

#MAIN
if [ "$RANK" = "0" ] 
then
    /home/zzhang/cio/bin/hashserver.rb &
elif [ "$MODULO" = "1" ] 
then
    cd /dev/shm/share
    mkdir stripe
    cd stripe
    cp ../.__acl .
    mkdir root
    cp .__acl root/
    echo 10.128.$SECOND.$CHIRP_1 >> hosts
    echo 10.128.$SECOND.$CHIRP_2 >> hosts
    echo 10.128.$SECOND.$CHIRP_3 >> hosts
    echo 10.128.$SECOND.$CHIRP_4 >> hosts
    echo bigvolfiles > key

    mkdir /chirp
    /home/zzhang/chirp/bin/chirp_fuse -a address /chirp
    /home/zzhang/cio/bin/collector.sh &
elif [ "$MODULO" = "2" ] || [ "$MODULO" = "3" ] || [ "$MODULO" = "4" ] 
then
    sleep 3600
else
    mkdir /chirp
    /home/zzhang/chirp/bin/chirp_fuse -a address /chirp
    /home/zzhang/falkon/worker/run.worker-c-bgp.sh $1 $2 $3 $4 $5 $6 $7
fi

sleep 3600
