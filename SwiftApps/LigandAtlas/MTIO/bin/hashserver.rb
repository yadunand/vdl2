#!/home/espinosa/local/bin/ruby

require 'drb'

class HashServer
  attr_reader :file

  def initialize
    @file = {}
  end

  def put(fname, rank)
    @file[fname] = rank
  end

  def get(fname)
    @file[fname]
  end

end

server = HashServer.new

DRb.start_service('druby://*:9000', server)
DRb.thread.join

