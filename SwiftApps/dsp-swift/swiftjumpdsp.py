#!/usr/bin/env python

"""
This is the Swift foreach driver for Galaxy. It receives inputs from the Swift
"foreach" Galaxy tool and depending on the options specified, builds a
canonical Swift/K or Swift/T script and runs over specified computational
environment.

swiftjumpdsp.py
        "${inlocation}"
        "${a}" 
        "${n}"
        "${p}"
        "${I}"
        "${outlist}"

"""

import subprocess
import sys
import os
import distutils.spawn
import traceback
import fnmatch
import re
import platform

def setwdir():
    return subprocess.check_output(["mktemp", "-d", "/tmp/swift-gal.XXXX"])

def genargs(args):
    for a in args:
        yield a

def main():
    myargs = genargs(sys.argv)
    try:
        this=next(myargs)
        inloc = next(myargs)
        a = next(myargs)
        n = next(myargs)
        p = next(myargs)
        I = next(myargs)
        local_or_compute = next(myargs)
        outloc = next(myargs)
        outlistfile = next(myargs)
        logfile=next(myargs)
        plotimg=next(myargs)
    except:
        traceback.print_exc()
        sys.exit(1)

    if platform.system() == "Linux":
        homedir="/home/ketan"
    else:
        homedir="/Users/ketan"
        
    #which swift
    baseloc = homedir+"/SwiftApps/dsp-swift"
    swift = homedir+"/swift-0.95/cog/modules/swift/dist/swift-svn/bin/swift"
    swiftargs="-loc="+inloc+" -a="+a+" -n="+n+"  -pval="+p+" -I="+I+" -outloc="+outloc+" -plotimg="+plotimg
    siteval=baseloc+"/sites.local.xml"

    if local_or_compute == "compute":
        siteval=baseloc+"/sites.blues.xml"

    #Invocation
    with open(logfile,"wb") as errfile:
        exitcode=subprocess.check_call([swift, "-sites.file", siteval, "-tc.file", baseloc+"/apps", "-config", baseloc+"/cf", baseloc+"/dsp.blues.swift", swiftargs],stderr=errfile)
    #print exitcode

    #Populate output file
    outlist=subprocess.check_output(["find", outloc, "-type", "f", "-iname", "*.out"])
    f=open(outlistfile, "wb")
    f.write("The results files are as follows:\n")
    f.write(outlist)
    f.close()

if __name__=='__main__':
    main()
