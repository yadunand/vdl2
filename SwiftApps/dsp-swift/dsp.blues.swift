/* ==
This is a Swift workflow script for the DSP application. The script contains the following parts:

-- A one-line type declaration
-- App function definition
-- Input files and variables definitions
-- Output files definitions
-- Invocation of app function
== */

type file;

/* == App function definitions == */

/* == DSP app definition == */
app (file _out, file _err) dsp_app (string _filespath, file _sto, file _cor, file _tim, int _a, int _n, int _p, int _I)
{
   dsp "-f" _filespath "-a" _a "-n" _n "-p" _p "-I" _I stdout=@_out stderr=@_err; 
}


/* == Plot app definition == */
app (file _img) plot_app (file[] _fromdsp)
{
   plotme @_img @_fromdsp; 
}

/* == Input files definitions: Make sure these files are available in the current directory == */
string inloc=arg("loc","/lcrc/project/NEXTGENOPT/Packages/examples/smps");

file stofiles[] <filesys_mapper; location=inloc, pattern="*.sto">;
file corfiles[] <structured_regexp_mapper; source=stofiles, match="^(.*)sto$", transform="\\1cor">;
file timfiles[] <structured_regexp_mapper; source=stofiles, match="^(.*)sto$", transform="\\1tim">;

int a=toInt(arg("a","1"));
int n=toInt(arg("n","1"));
int p=toInt(arg("pval","1"));
int I=toInt(arg("I","10"));

string outdir=arg("outloc","outdir");

file outs[];

foreach in_tim,i in timfiles{

  /* == Output files definitions == */
  file out <single_file_mapper; file=strcat(outdir, "/std_", i, ".out")>;
  file err <single_file_mapper; file=strcat(outdir, "/std_", i, ".err")>;
  string filespath[]=strsplit(filename(in_tim),"\\.");

  //tracef(" stofiles[%i] = %s\ncorfiles[%i] = %s\ntimfiles[%i] = %s\n", i, filename(stofiles[i]), i, filename(corfiles[i]), i, filename(timfiles[i]));

  /* == App invocation == */
  (out, err) = dsp_app (filespath[0], stofiles[i], corfiles[i], timfiles[i], a, n, p, I);
  outs[i]=out;
}             

file img<single_file_mapper; file=arg("plotimg", "img.png")>;

img=plot_app(outs);

