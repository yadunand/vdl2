//import "Swiftreduce_lib";
type file;
type script;
// type fileptr will be a regular file to swift, but the contents of
// which, will be
// interpreted as globally addressable paths to one or more files.
// The current format of a fileptr is:
//   SiteX_Node0 /path/to/file0 /path/to/file1 /path/to/file2 ...
//   ...
//   SiteY_Noden /path/to/file0 ...
type fileptr;

app (fileptr out, file err) map_data (script wrapper,script map, int recsize)
{
    bash @wrapper @map recsize stdout=@out stderr=@err;
}

app (fileptr out) aggregate (script a, fileptr map_results[])
{
    bash @a @map_results stdout=@out;
}

// There must be one reduce function per site, and one for local
app (file out, file err) reduce_bgl (script wrapper, script reducer, string cmd, fileptr all_map){
    bgl_bash @wrapper @reducer cmd @all_map stdout=@out stderr=@err;
}

app (file out, file err) reduce_mid (script wrapper, script reducer, string cmd, fileptr all_map){
    mid_bash @wrapper @reducer cmd @all_map stdout=@out stderr=@err;
}

app (file out, file err) reduce_local (script wrapper, script reducer, string cmd, file site_results[]){
    loc_bash @wrapper @reducer cmd @site_results stdout=@out stderr=@err;
}


int files = @toInt(@arg("files","0"));
int fsize = @toInt(@arg("filesize","10")); // This would make 10^4 records per file

// List of sites:
string sites[] = ["beagle", "midway"];
// Defaults:
script map_wrapper    <"map_wrapper.sh">;
script red_wrapper    <"reduce_wrapper.sh">;
script aggr           <"aggregate.sh">;
// User scripts:
//script map_func       <"map_core.sh">;
script map_func       <"examples/map_genrands.sh">;
//script red_func    <"reduce_core.sh">;'
script red_func       <"examples/reduce_sort.sh">;


fileptr map_out[] <simple_mapper; prefix="map", suffix=".out">;
file    map_err[] <simple_mapper; prefix="map", suffix=".err">;
// Map stage. File pointers map_out[]
foreach item,i in [0:files-1] {
    (map_out[i], map_err[i]) = map_data (map_wrapper, map_func, fsize);
}

// Aggregate all fileptrs into one fileptr
fileptr all_map <"all_map.out">;
all_map = aggregate(aggr, map_out);

// Reduce/Local combine at node level
// TODO: Fix/file bug for foreach loop with array inside issue
file redsite_out[] <simple_mapper; prefix="reduce", suffix=".out">;
file redsite_err[] <simple_mapper; prefix="reduce", suffix=".err">;
(redsite_out[0], redsite_err[0]) = reduce_bgl (red_wrapper, red_func, "local", all_map);
(redsite_out[1], redsite_err[1]) = reduce_mid (red_wrapper, red_func, "local", all_map);

// Final global reduce
file final_out <"final_result">;
file final_err <"final_errors">;
(final_out, final_err) = reduce_local (red_wrapper, red_func, "naive", redsite_out);

