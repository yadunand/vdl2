type file;
type fileptr;
type script;

app (file json) runEP ( file imf, file epw, string params[] )
{
  RunEP "--imf" @imf "--epw" @epw "--outall" "temp.tgz" "--outxml" "temp.xml" "--outjson" @json "--params" params;
}

app (fileptr out, file err) map_data (script wrapper, script map, file imf, file epw, string outjson, string params[]){
 bash @wrapper @map @imf @epw outjson params stdout=@out stderr=@err;
}

file epconfig  <single_file_mapper; file=@arg("epconfig",  "SWEEP_RECT.imf")>;
file epweather <single_file_mapper; file=@arg("epweather", "CHICAGO.epw")>;

string outdir=@arg("outdir","output");

type param {
  string pnum;
  string pname;
  string pvals;
}

param  pset[] = readData(@arg("sweep","sweep.txt"));
string pval[][];

foreach p, pn in pset {
  string val[] = @strsplit(p.pvals,",");
  foreach v, vn in val {
    pval[pn][vn]=v;
  }
}

/*
fileptr map_out[] <simple_mapper; prefix="map", suffix=".out">;
file  map_err[] <simple_mapper; prefix="map", suffix=".err">;
*/

script map_wrapper <"../../map_wrapper.sh">;
script red_wrapper <"../../reduce_wrapper.sh">;
script aggr <"../../aggregate.sh">;
script map_func <"map_eplus.sh">;
script red_func <"reduce_eplus.sh">;

foreach v0 in pval[0] {
foreach v1 in pval[1] {
foreach v2 in pval[2] {
foreach v3 in pval[3] {
foreach v4 in pval[4] {
foreach v5 in pval[5] {
foreach v6 in pval[6] {
foreach v7 in pval[7] {

 string fileid = @strjoin(["/ep",v0,v1,v2,v3,v4,v5,v6,v7],".");
 
 fileptr map_out <single_file_mapper; file=@strcat("map",fileid,".out")>;
 file map_err <single_file_mapper; file=@strcat("map",fileid,".err")>;

 (map_out, map_err) = map_data (map_wrapper, map_func, epconfig, epweather,
                            "outjson", [pset[0].pname, v0, pset[1].pname, v1, pset[2].pname,
                            v2, pset[3].pname, v3, pset[4].pname, v4, pset[5].pname, v5,
                            pset[6].pname, v6, pset[7].pname, v7]);

}}}}}}}}

/*
fileptr all_map <"all_map.out">;
all_map = aggregate(aggr, map_out);
file redsite_out[] <simple_mapper; prefix="reduce", suffix=".out">;
file redsite_err[] <simple_mapper; prefix="reduce", suffix=".err">;
(redsite_out[1], redsite_err[1]) = reduce_mid (red_wrapper, red_func, "local", all_map);
*/
