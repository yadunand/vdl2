#!/bin/bash

executable="$HOME/blast-2.2.26/bin/blastall";
pname=$1
queryfile=$2
fastafile=$3
tmpout=$4
e=$5
f=$6

$executable -p $pname -i $queryfile -d $fastafile -o $tmpout -e $e -T -F $f >/dev/null
cat $tmpout
rm -rf $tmpout >/dev/null

