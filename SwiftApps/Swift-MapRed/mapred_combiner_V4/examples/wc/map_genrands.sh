#!/bin/bash
ARG1=$1;
LOWERLIMIT=0
UPPERLIMIT=1000000
SLICESIZE=10000  # 10^4 records padded to 100B would result in 1MB file
#Generate $SLICESIZE number of 100B records of format:  RANDOM_NUMBER 1
shuf -i $LOWERLIMIT-$UPPERLIMIT -n $SLICESIZE | awk '{printf "%-99s\n", $0}' > tmp_shuf
for i in $(seq 1 1 $ARG1)
do
    cat tmp_shuf
done
rm tmp_shuf
