#!/bin/bash

for f in $*
do
    sort -n $f -o $f
done
sort -n -m $*
rm $*
