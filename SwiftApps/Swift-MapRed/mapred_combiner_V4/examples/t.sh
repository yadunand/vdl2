#!/bin/bash

LOGS=OFF
log()
{
    [ "$LOGS" == "ON" ] && echo "[$(date +'%Y-%m-%d %H:%M:%S.%N')] : $*" 1>&2
}

log "start do"
sleep 1
log "stop do"
