#!/bin/bash


# We don't fetch because we don't know where files reside ?
# actually we do know where files reside.

leaf ()
{
    filepointers=($(cat $*))
    center=${filepointers[0]}
    echo "Accumulating ${filepointers[*]} at $center"
    #exec_client
}

# Divide into K and Conquer
foo()
{
    #echo "Foo() called with args : [$*]"
    LEVEL=$1   ; shift
    KWAY=$1    ; shift
    FILEPTR=$1 ; shift
    NODE_COUNT=`wc -l < $FILEPTR`
    # In case what we have is a few nodes in the start case
    if (( "$NODE_COUNT" <= "$KWAY" ))
    then
        leaf $FILEPTR
        return
    else
        LINES_PER_SPLIT=$(($(( $NODE_COUNT + $KWAY - 1)) / $KWAY ))
        #if (( "$LINES_PER_SPLIT" > "$KWAY" ))
        ID=$RANDOM
        #echo "Lines per split $NODE_COUNT > $KWAY"
        split --lines=$LINES_PER_SPLIT $FILEPTR "K.$LEVEL.$ID"
        for fptr in $(ls K.$LEVEL.$ID*)
        do
        {
            #echo "Split[$LEVEL] : $fptr"
            foo $(($LEVEL+1)) $KWAY $fptr
        } &
        done
        wait
    fi
}

foo 0 4 pointers
#rm K*
