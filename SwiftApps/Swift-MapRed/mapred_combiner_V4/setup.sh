#!/bin/bash

set -x
# Configure the following before proceeding the run the scripts

[ ! -z $BEAGLE_USERNAME ] || BEAGLE_USERNAME="ketan"
[ ! -z $MIDWAY_USERNAME ] || MIDWAY_USERNAME="maheshwari"

# Do not change
BEAGLE_SHARE_FOLDER=/lustre/beagle/$BEAGLE_USERNAME/bin/
MIDWAY_SHARE_FOLDER=/scratch/midway/$MIDWAY_USERNAME/bin/

echo "$BEAGLE_SHARE_FOLDER is in PATH on BEAGLE"
echo "$MIDWAY_SHARE_FOLDER is in PATH on MIDWAY"

if [ -f "map_wrapper.sh" ];then
    sed -i "s/BEAGLE\_USERNAME/$BEAGLE_USERNAME/g" map_wrapper.sh
    sed -i "s/MIDWAY\_USERNAME/$MIDWAY_USERNAME/g" map_wrapper.sh
    chmod 777 map_wrapper.sh
fi;

if [ ! -f "reduce_wrapper.sh" ]; then
    sed -i "s/BEAGLE\_USERNAME/$BEAGLE_USERNAME/g" reduce_wrapper.sh
    sed -i "s/MIDWAY\_USERNAME/$MIDWAY_USERNAME/g" reduce_wrapper.sh
    chmod 777 reduce_wrapper.sh
fi;

gcc server.c -o exec_server
gcc client.c -o exec_client

chmod a+x exec_server
chmod a+x exec_client

scp exec_server $MIDWAY_USERNAME@swift.rcc.uchicago.edu:$MIDWAY_SHARE_FOLDER
scp exec_client $MIDWAY_USERNAME@swift.rcc.uchicago.edu:$MIDWAY_SHARE_FOLDER
scp exec_server $BEAGLE_USERNAME@login4.beagle.ci.uchicago.edu:$BEAGLE_SHARE_FOLDER
scp exec_client $BEAGLE_USERNAME@login4.beagle.ci.uchicago.edu:$BEAGLE_SHARE_FOLDER

sed -i "s/BEAGLE\_USERNAME/$BEAGLE_USERNAME/g" *xml
sed -i "s/MIDWAY\_USERNAME/$MIDWAY_USERNAME/g" *xml

# Todo extend chirp check to remote machines ?
which chirp_server &>/dev/null
if [ "$?" != "0" ];then
    echo "ERROR: Chirp not available in system path"
fi;
