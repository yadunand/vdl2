#!/bin/bash

clear;
./clean.sh

#Call as check_error $? <ERR_CODE> <Error message>
check_error()
{
    if [ $1 == $2 ]
    then
        echo $3
    else
        echo "Run completed with code:$1"
    fi
}

#export COG_OPTS="-Dtcp.channel.log.io.performance=true"

verify()
{
    LOOPS=$1
    RECSIZE=$2
    ACTUAL_RESULT=($(tail -n 1 final_result))
    EXPECTED_RESULT=$(($LOOPS * $RECSIZE * 10000 ))
    if [ "$EXPECTED_RESULT" ==  "${ACTUAL_RESULT[1]}" ]
    then
	echo "---------------------------------------------"
	echo "COUNT matches expected records"
	echo "ACTUAL COUNT  : ${ACTUAL_RESULT[1]}"
	echo "EXPECTED COUNT: $EXPECTED_RESULT"
	echo "---------------------------------------------"
    else
	echo "---------------------------------------------"
	echo "COUNT does *NOT* match expected records"
	echo "ACTUAL COUNT  : ${ACTUAL_RESULT[1]}"
	echo "EXPECTED COUNT: $EXPECTED_RESULT"
	cat err_file
	echo "---------------------------------------------"
    fi
}

run_swift_chirp_combiner()
{
    SITESXML=$1
    LOOPS=$2
    RECSIZE=$3
    echo "================================================================"       >> LAB_RECORDS
    echo "RUNTYPE :SWIFT+Chirp (With local combiners), SITE:$SITE, CHUNKS:$LOOPS X FILESIZE:$RECSIZE " | tee -a LAB_RECORDS
    ( time -p swift -tc.file tc.data -config swift.properties -sites.file $SITESXML swiftreduce.swift -files=$LOOPS -filesize=$RECSIZE ) 2>&1 | tee -a LAB_RECORDS
    echo "chunks $LOOPS filesize $RECSIZE site $SITE type Swift+Chirp" >> LAB_RECORDS
    verify $LOOPS $RECSIZE | tee -a LAB_RECORDS
    echo "================================================================"
}

if [ "$2" != "" ] && [ "$3" != "" ]
then
    run_swift_chirp_combiner $*
else
    run_swift_chirp_combiner multiple.xml 20 10
fi
exit 0
