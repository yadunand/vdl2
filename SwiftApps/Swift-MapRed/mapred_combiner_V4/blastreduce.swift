type file;
type fileptr;
type fastaseq;
type query;

type script;

type headerfile;
type indexfile;
type seqfile;
type output;
type database {
  headerfile phr;
  indexfile pin;
  seqfile psq;
}


app (fastaseq out[]) split_database (fastaseq d, string n){
 fastasplitn filename(d) n;
}

app (database out) formatdb (fastaseq i){
 formatdb "-i" filename(i);
}

app (fileptr out, file err) map_data (
script wrapper,
script map,
query _queryfile,
fastaseq _fastafile,
string resfilename,
string p,
string e,
string f,
database db){
 bash 
 @wrapper 
 @map 
 p 
 filename(_queryfile) 
 filename(_fastafile) 
 resfilename 
 e 
 f 
 stdout=@out 
 stderr=@err;
}

app (fileptr out) aggregate (script a, fileptr map_results[]) {
 bash @a @map_results stdout=@out;
}

app (file out, file err) reduce_mid (script wrapper, script reducer, string cmd, fileptr all_map){
 mid_bash @wrapper @reducer cmd @all_map stdout=@out stderr=@err;
}

app (file out, file err) reduce_local (script wrapper, script reducer, string cmd, file site_results[]){
 loc_bash @wrapper @reducer cmd @site_results stdout=@out stderr=@err;
}

string num_partitions = arg("n", "10");
fastaseq partition[] <ext;exec = "examples/blast/splitmapper.sh", n = num_partitions>;
database formatdbout[] <ext; exec = "examples/blast/formatdbmapper.sh", n = num_partitions>;

script map_wrapper    <"map_wrapper.sh">;
script red_wrapper    <"reduce_wrapper.sh">;
script aggr           <"aggregate.sh">;
script map_func       <"examples/blast/map_blast.sh">;
script red_func       <"examples/blast/reduce_blast.sh">;

fileptr map_out[] <simple_mapper; prefix="map", suffix=".out">;
file  map_err[] <simple_mapper; prefix="map", suffix=".err">;

fastaseq dbin <single_file_mapper;file = arg("d", "/scratch/midway/maheshwari/nr")>;
query query_file <single_file_mapper;file = arg("i", "examples/blast/sequence.seq")>;

partition=split_database(dbin, num_partitions);

foreach item,i in partition {
 formatdbout[i]=formatdb(item);
 (map_out[i], map_err[i]) = map_data (map_wrapper, map_func, query_file, item, "tmpout", "blastp", "0.1", "F", formatdbout[i]);
}

fileptr all_map <"all_map.out">;
all_map = aggregate(aggr, map_out);
file redsite_out[] <simple_mapper; prefix="reduce", suffix=".out">;
file redsite_err[] <simple_mapper; prefix="reduce", suffix=".err">;
(redsite_out[1], redsite_err[1]) = reduce_mid (red_wrapper, red_func, "local", all_map);

// Final global reduce (Not required on a single site)
//file final_out <"final_result">;
//file final_err <"final_errors">;
//(final_out, final_err) = reduce_local (red_wrapper, red_func, "naive", redsite_out);

