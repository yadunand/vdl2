#!/bin/bash

# By default with ARG1:100 and SLICESIZE=10000, this script will generate
# 10^6 records.

if [ ! -z $1 ] && [ -f $1 ]; then
    USER_MAP_SCRIPT=$1;
    shift 1;
fi

MAP_ARGS="$@"
echo $MAP_ARGS >> /scratch/midway/maheshwari/ketan.args
BEAGLE_USERNAME=ketan
MIDWAY_USERNAME=maheshwari
FILE="input_$RANDOM.txt"
HOSTNAME=$(hostname -f)
if   [[ "$HOSTNAME" == *midway* ]]; then # On midway node
    EXECSERVER=/scratch/midway/$MIDWAY_USERNAME/bin/exec_server
    LOGFOLDER=/scratch/midway/$USER
    FOLDER="/dev/shm"
    HOSTNAME="midway_$HOSTNAME"

elif [[ "$HOSTNAME" == *nid* ]]; then    # On beagle node
    EXECSERVER=/lustre/beagle/$BEAGLE_USERNAME/bin/exec_server
    LOGFOLDER=/lustre/beagle/$USER
    FOLDER="/dev/shm"
    HOSTNAME="beagle_$HOSTNAME"
fi

TIMEOUT=1200
NC_LISTEN_PORT=29900
# Start services on Node
JOBS_LEVEL=`echo $PWD | grep -o ".*jobs"`
if [ "$?" == "0" ]
then
    mkdir -p $JOBS_LEVEL/CHIRPING
    if [ "$?" == "0" ]
    then
	    killall -u $USER chirp_server;
        echo "first chirp server"
	    echo "unix:$USER rwlds" >  $FOLDER/acl.conf
	    echo "hostname:* rwl"   >> $FOLDER/acl.conf
        which chirp_server 1>&2
	    timeout $TIMEOUT chirp_server -A $FOLDER/acl.conf -r $FOLDER &
        timeout $TIMEOUT $EXECSERVER &> $LOGFOLDER/exec_server_log-$HOSTNAME.log &
    fi
    FILE=$FOLDER/$FILE;
else
    FILE=$PWD/$FILE
fi

chmod a+x $USER_MAP_SCRIPT
./$USER_MAP_SCRIPT ${MAP_ARGS[*]} > $FILE
echo "$HOSTNAME $FILE"
ps -u $USER 1>&2
ls -thor $FILE 1>&2
exit 0;
