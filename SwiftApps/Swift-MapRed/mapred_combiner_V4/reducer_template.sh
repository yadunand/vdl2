#!/bin/bash
# The reducer is expecting to receive filenames as args
# Each file would contains strings in the format HOSTNAME FILEPATH

# The files passed here are filepointers which need to be resolved
# before they can be processed by the core reduce function.
ARGS=($*)
CMD=${ARGS[0]}
FILES=(${ARGS[*]:1})

ID=$RANDOM
HOSTNAME=$(hostname -f)
if   [[ "$HOSTNAME" == *midway* ]]; then # On midway node
    EXECSERVER=/scratch/midway/MIDWAY_USERNAME/bin/exec_server
    EXECCLIENT=/scratch/midway/MIDWAY_USERNAME/bin/exec_client
    LOGFOLDER=/scratch/midway/MIDWAY_USERNAME
    FOLDER="/dev/shm"
    HOSTID="midway_$HOSTNAME"
    SITE="midway"

elif [[ "$HOSTNAME" == *nid* ]]; then    # On beagle node
    EXECSERVER=/lustre/beagle/BEAGLE_USERNAME/bin/exec_server
    EXECCLIENT=/lustre/beagle/BEAGLE_USERNAME/bin/exec_client
    LOGFOLDER=/lustre/beagle/BEAGLE_USERNAME/Swift-MapRed/
    FOLDER="/dev/shm"
    HOSTID="beagle_$HOSTNAME"
    SITE="beagle"
fi


# This reduce function must conform to the following specification
# Takes the CMD type followed by 1 or more files as arguments
# Returns the content of the file with results
#USER_REDUCE

fetch()
{
    ID=$1
    TOKEN=$2
    IFS=$'\ ' PTR=($(echo $TOKEN | sed 's/_/\ /'))
    REMOTE=${PTR[1]}
    FILE=${PTR[2]}
    TARGETFILE=$(basename $FILE)
    REPODIR=$(dirname $FILE)

    if [ "$REMOTE" == "$HOSTNAME" ]
    then # Move to an identifiable file if local
        echo "mv $FILE $REPODIR/$ID.$RANDOM.imd2" 1>&2
        [ ! -f $FILE ] && echo "$FILE missing" 1>&2
        mv $FILE $REPODIR/$ID.$RANDOM.imd2
    else # Fetch to an identifiable file if remote
        echo "chirp_get $REMOTE $TARGETFILE $REPODIR/$ID.$RANDOM.imd2" 1>&2
        chirp_get $REMOTE $TARGETFILE $REPODIR/$ID.$RANDOM.imd2
    fi
}

plain ()
{
    ID=$1; shift
    IFS=$'\r\n' FILES=($(grep -h "$SITE" $*))
    echo "Matching files : ${FILES[*]}" 1>&2
    for file_token in ${FILES[*]k}
    do
        fetch $ID $file_token &
    done
    wait
    cat $(reduce $FOLDER/$ID*imd2)
    rm -rf $FOLDER/$ID*imd2 &> /dev/null &
    exit 0
}


local_combine()
{
    ID=$1; shift
    #IFS=$'\r\n' FILES=($(grep -h "$SITE" $*))
    LOCATIONS=($(awk '{ print $1 }' $* | grep $SITE | sort -u))
    for LOCATION in ${LOCATIONS[*]}
    do
        Node=($(echo "$LOCATION" | sed 's/[^ ]*_//' ))
        FILES=($(grep -h $Node $* | awk '{ print $2 }'))
############## LOCAL COMBINER CODE ###################
        cat <<EOF   > $Node.sh
#!/bin/bash
ID=$RANDOM
FILES=(${FILES[*]})
SITE=$SITE
EOF
        cat <<'EOF' >> $Node.sh
#USER_REDUCE

echo -e "$SITE"_"$(hostname -f) $(reduce ${FILES[*]})"
EOF
        chmod a+x $Node.sh;
############# END LOCAL COMBINER #####################
        #TODO: Move client with swift ?
        # How does builds work on different architectures ?
        cp $EXECCLIENT ./
        ./exec_client $Node $Node.sh 1> $FOLDER/$LOCATION.$ID.imd &
    done
    wait
    #cat $FOLDER/*.$ID.imd
    plain $ID $FOLDER/*.$ID.imd
}

case "$CMD" in
    "naive")
        echo "Using naive reduce" 1>&2
        cat $(reduce ${FILES[*]})
        ;;
    "plain")
        echo "Using plain reduce" 1>&2
        plain $ID ${FILES[*]}
        ;;
    "local")
        echo "Using local combiners" 1>&2
        local_combine $ID ${FILES[*]} #> $FOLDER/$ID.combine.results
        #echo "Result from local_combines :" 1>&2
        #cat $FOLDER/$ID.combine.results 1>&2
        #plain $ID $FOLDER/$ID.combine.results
        ;;
    *)
        echo "ERROR: Unknown reduction method requested" 1>&2
        ;;
esac
exit 0
##########################################################################
TOKEN=($(cat $1))
FILE=${TOKEN[1]}
CHIRPDIR=$(dirname $FILE)
echo "$HOSTNAME $CHIRPDIR $CHIRPDIR/$ID*imd2 " 1>&2
#cat $(reduce $CHIRPDIR/$ID*imd2)
echo "$HOSTNAME $(reduce $CHIRPDIR/$ID*imd2)"
rm -rf $CHIRPDIR/$ID*imd2 &> /dev/null &
exit 0
