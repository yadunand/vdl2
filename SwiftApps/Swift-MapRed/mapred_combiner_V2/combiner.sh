#!/bin/bash

ARGS=($*)
SUM=0
COUNT=0

fetch_and_process()
{
    TOKEN=($(cat $1))
    HOST=${TOKEN[0]}
    FILE=${TOKEN[1]}
    TARGET=$(basename $FILE)
    echo "HOST:$HOST, HOSTNAME:$HOSTNAME"
    if [ "$HOST" != "$HOSTNAME" ]
    then  # Fetch if file is remote        
        echo "$HOST $TARGET $FOLDER/$TARGET.imd : "; cat $FOLDER/$TARGET.imd
        RES=($(awk '{ sum += $1; count += $2 } END { print sum,count }' $FOLDER/$TARGET.imd))
        rm $FOLDER/$TARGET.imd &> /dev/null &
    else   # Process directly if file is local
        RES=($(awk '{ sum += $1; count += $2 } END { print sum,count }' $FILE))
    fi
    ID=$RANDOM
    echo "${RES[0]} ${RES[1]}" > "$CHIRP_FOLDER/$2.$RANDOM.imd2"
    echo -e "$(hostname -f) $CHIRP_FOLDER/$2.$RANDOM.imd2"
}


if [ -f ${ARGS[0]} ]
then # When fileptrs are passed to combiner
     # 1. fetch files.
     # 2. combine and return a file pointer
    FILES=(${ARGS[*]})
    ID=$RANDOM
    for file_token in ${FILES[*]}
    do
        fetch_and_process $file_token $ID &
    done
    wait
    
    TOKEN=($(cat ${FILES[0]}))
    CHIRP_FOLDER=$(dirname ${TOKEN[1]})
    cat $ID*imd2
    FINAL=($(awk '{ sum += $1; count += $2 } END { print sum,count }' $ID*imd2))
    echo "${FINAL[0]} ${FINAL[1]}" > $CHIRP_FOLDER/tree_comb.$RANDOM.imd3
    
    rm $ID*{imd,imd2} &> /dev/null &
    exit 0   
else # When strings in format node files... are passed to combiner
     # 1. Run a local_combiner on the remote node 
     # 2. Return a file pointer
    NODE=${ARGS[0]}
    FILES=(${ARGS[*]:1})    
    REMOTE_PORT=29900
    HOSTNAME=$(hostname -f)
    
############## LOCAL COMBINER CODE ################### 
cat << EOF   > $NODE.sh
#!/bin/bash 
ID=$RANDOM
FILES=(${FILES[*]})
EOF
cat <<'EOF' >> $NODE.sh
CHIRP_FOLDER=$(dirname ${FILES[0]})
#echo "DEBUG : FILES[0] ${FILES[0]}"
#echo "DEBUG : CHIRP_FOLDER $CHIRP_FOLDER"
SUM=0;COUNT=0;
for file in ${FILES[*]}; do
    [ ! -f $file ] && echo "$file is missing" &>2
    RES=($(awk '{ sum += $1; count += $2 } END { print sum,count }' $file))               
    SUM=$(($SUM+${RES[0]}))
    COUNT=$(($COUNT+${RES[1]}))
    rm $file &
done;
echo -e "$SUM $COUNT" > $CHIRP_FOLDER/combine_imd.$ID.out
echo -e "$(hostname -f) $CHIRP_FOLDER/combine_imd.$ID.out"
#echo -e "DEBUG : $SUM $COUNT" 
EOF
     chmod a+x $NODE.sh
############# END LOCAL COMBINER #####################
     cp /lustre/beagle/yadunandb/bin/exec_client .
     echo "Combiner ${ARGS[*]}" 1>&2
     echo "Combiner [./exec_client $NODE $NODE.sh]" 1>&2
     cat $NODE.sh 1>&2
     ./exec_client $NODE $NODE.sh
     exit 0
     
fi