#!/bin/bash


# By default with ARG1:100 and SLICESIZE=10000, this script will generate
# 10^6 records.  
ARG1=1
[ ! -z $1 ] && ARG1=$1

FILE="input_$RANDOM.txt"
LOWERLIMIT=0
UPPERLIMIT=1000000 # 10^6
SLICESIZE=10000     # 10^4 records padded to 100B would result in 1MB file
#SLICESIZE=1000     # 10^3  If padded to 100B would result 
HOSTNAME=$(hostname -f)

FOLDER="/dev/shm"
TIMEOUT=1200
NC_LISTEN_PORT=29900

JOBS_LEVEL=`echo $PWD | grep -o ".*jobs"`
if [ "$?" == "0" ]
then
    mkdir $JOBS_LEVEL/CHIRPING 
    if [ "$?" == "0" ]
    then
	killall -u $USER chirp_server;
	echo "unix:$USER rwlds" >  $FOLDER/acl.conf
	echo "hostname:* rwl"   >> $FOLDER/acl.conf
	timeout $TIMEOUT chirp_server -A $FOLDER/acl.conf -r $FOLDER &
    timeout $TIMEOUT /lustre/beagle/yadunandb/bin/exec_server &> /lustre/beagle/yadunandb/Swift-MapRed/mapred_combiner_V2/exec_server_log-$HOSTNAME-$RANDOM.log &
    fi
    FILE=$FOLDER/$FILE;
else
    FILE=$PWD/$FILE
fi

###### THIS IS THE ACTUAL WORK #######
touch $FILE
shuf -i $LOWERLIMIT-$UPPERLIMIT -n $SLICESIZE | awk '{printf "%-97s %s\n", $0, 1}' > tmp_shuf
for i in $(seq 1 1 $ARG1)
do
   cat tmp_shuf >> $FILE
done
rm tmp_shuf
echo "$HOSTNAME $FILE"
ps -u $USER 1>&2
###### END OF WORK ###################
exit 0;