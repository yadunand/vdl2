type file;
type script;

// type fileptr will be a regular file to swift, but the contents of which, will be
// interpreted as globally addressable paths to one or more files.
// The current format of a fileptr is:
//   Node0 /path/to/file0 /path/to/file1 /path/to/file2 ...
//   ...
//   Noden /path/to/file0 ...
// Evidently, there is room for improvement and extension.
type fileptr;

app (fileptr out, file err) map_data (script run, int recsize)
{
    bash @run recsize stdout=@out stderr=@err;
}

app (fileptr out, file err) comb_data (script comb, string node_string)
{
    bash @comb node_string stdout=@out stderr=@err;
}

app (file out, file err) reduce_data (script comb, fileptr array[])
{
    bash @comb @array stdout=@out stderr=@err;
}

app (file out, file err) get_uniq_nodes (script uniq, fileptr filepointers[])
{
    bash @uniq @filepointers stdout=@out stderr=@err;
}


int files = @toInt(@arg("files","0"));
int fsize = @toInt(@arg("filesize","10")); // This would make 10^4 records per file

script mapper   <"mapper.sh">;
script uniq     <"node_uniq.sh">;
script combiner <"combiner.sh">;
script reducer  <"reducer.sh">;

fileptr tgen_out[] <simple_mapper; prefix="tgen", suffix=".out">;
file    tgen_err[] <simple_mapper; prefix="tgen", suffix=".err">;
// Map stage. File pointers tgen_out[]
foreach item,i in [0:files-1] {
    (tgen_out[i], tgen_err[i]) = map_data(mapper, fsize);
}

// Get nodes and intermediate file local to node
file    node_out <simple_mapper; prefix="node", suffix=".out">;
file    node_err <simple_mapper; prefix="node", suffix=".err">;
(node_out, node_err) = get_uniq_nodes (uniq, tgen_out);
string nodes[] = readData(node_out);

// Invoke the remote_combiner on each node involved in map stage stage
fileptr comb_out[] <simple_mapper; prefix="comb", suffix=".out">;
file    comb_err[] <simple_mapper; prefix="comb", suffix=".err">;
foreach node_info,i in nodes {
  (comb_out[i], comb_err[i]) = comb_data(combiner, node_info);
}

// This is the final reduce stage, here results from all nodes are
// brought to a single node for reduction
// Tree based implementation could be used.
file final <"final_result">;
file errs  <"final_errors">;
(final, errs) = reduce_data(reducer, comb_out);

// This version of reduce_data uses the results from map stage directly
//(final, errs) = reduce_data(reducer, tgen_out);
