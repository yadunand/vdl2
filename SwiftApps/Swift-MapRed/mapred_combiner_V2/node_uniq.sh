#!/bin/bash

cat $* > tmp;
FILES=$*

SITES=($(awk '{ print $1 }' tmp  | sort -u))
#echo ${SITES[*]}

for SITE in ${SITES[*]}
do
    MATCH=$(grep $SITE tmp | awk '{print $2}')
    echo $SITE ${MATCH[*]}
done
rm tmp;
