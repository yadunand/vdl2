#!/bin/bash
# The combiner is expecting to receive filenames as args
# Each file would contains strings in the format HOSTNAME FILEPATH

FILES=($*)

HOSTNAME=$(hostname -f)
FOLDER=/dev/shm
SUM=0
COUNT=0

fetch_and_process()
{
    TOKEN=($(cat $1))
    HOST=${TOKEN[0]}
    FILE=${TOKEN[1]}
    TARGET=$(basename $FILE)
    echo "HOST:$HOST, HOSTNAME:$HOSTNAME"
    if [ "$HOST" != "$HOSTNAME" ]
    then  # Fetch if file is remote
        chirp_get $HOST $TARGET $FOLDER/$TARGET.imd
        echo "$HOST $TARGET $FOLDER/$TARGET.imd : "; cat $FOLDER/$TARGET.imd
        RES=($(awk '{ sum += $1; count += $2 } END { print sum,count }' $FOLDER/$TARGET.imd))
        rm $FOLDER/$TARGET.imd &> /dev/null &
    else   # Process directly if file is local
        RES=($(awk '{ sum += $1; count += $2 } END { print sum,count }' $FILE))
    fi
    echo "${RES[0]} ${RES[1]}" > "$2.$RANDOM.imd2"
}

ID=$RANDOM
for file_token in ${FILES[*]}
do  
    fetch_and_process $file_token $ID &    
done
wait

TOKEN=($(cat ${FILES[0]}))
CHIRP_FOLDER=$(dirname ${TOKEN[1]})
cat $ID*imd2
FINAL=($(awk '{ sum += $1; count += $2 } END { print sum,count }' $ID*imd2))
echo "${FINAL[0]} ${FINAL[1]}"
rm $ID*{imd,imd2} &> /dev/null &
exit 0