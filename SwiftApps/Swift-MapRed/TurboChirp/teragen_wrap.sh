#!/bin/bash

# By default with ARG1:100 and SLICESIZE=10000, this script will generate
# 10^6 records.  
ARG1=1
[ ! -z $1 ] && ARG1=$1

FILE="input_$RANDOM.txt"
LOWERLIMIT=0
UPPERLIMIT=1000000000 # 10^9
SLICESIZE=10000     # 10^4 records padded to 100B would result in 1MB file
#SLICESIZE=1000     # 10^3  If padded to 100B would result 


FOLDER="/dev/shm/$USER"
TIMEOUT=120

# clean_folder SLEEP_TIME FOLDER_NAME
clean_folder()
{
    sleep $(($k1+1));
    rm $2/* &> /dev/null
}

HOST=$(hostname -f)

ps -u $USER | grep "chirp_server" &> /dev/null
if [ "$?" != "0" ]
then
    killall -u $USER chirp_server;
    echo "unix:$USER rwlda"   >  $FOLDER/acl.conf
    echo "hostname:* rwlda"   >> $FOLDER/acl.conf
    timeout $TIMEOUT chirp_server -A $FOLDER/acl.conf -r $FOLDER &
    sleep 3;
fi

FILE=$FOLDER/$FILE;
shuf -i $LOWERLIMIT-$UPPERLIMIT -n $(($SLICESIZE*$ARG1)) | awk '{printf "%-99s\n", $0}' > $FILE
echo "$HOST $FILE"
#echo "$HOSTNAME $FILE"
exit 0