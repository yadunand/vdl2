import files;
import io;
import random;
import string;
import sys;
import location;
import assert;

app (file out, file err) gen_data  (file run, int recsize){
    "/bin/bash" run recsize @stdout=out @stderr=err
}

app (file out, file err) comb_data (file comb, file array[]){
    "/bin/bash" comb array @stdout=out @stderr=err
}

app (file out, file err) comb_data_local (file comb, string array){
    "/bin/bash" comb array @stdout=out @stderr=err
}

app (file out) get_uniq_sites (file sites, file array[]){
    "/bin/bash" sites array @stdout=out
}

main
{
    file wrapper = input_file("/lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/teragen_wrap.sh");
	file tgen_out[];

    int loop = 10;
    //int loop=toint(argv("loop"));
    int fsize = 1;
    //int fsize = toint(argv("fsize"));

    foreach item,i in [0:loop-1] {
        file out <sprintf("intermediate/tgen_%i.out", i)>;
        file err <sprintf("intermediate/tgen_%i.err", i)>;
    	(out, err) = gen_data(wrapper, fsize);
        tgen_out[i]=out;
    }

    file jobs_per_site = input_file("/lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/jobs_per_site.sh");
    file combiner_list <"intermediate/uniq_sites">;
    combiner_list = get_uniq_sites(jobs_per_site, tgen_out);

    
    string sites[] = split(read(combiner_list), "\n");
        
    file local_combine = input_file("/lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/local_combiner.sh");
    //Here we are forcing jobs to sites to run local combiners
    file comb_out[];
    foreach site,i in sites {
      // TODO: Add check for site being empty string
      if ( strlen(site) > 0 ){
        file out1 <sprintf("intermediate/sitecomb_%i.out", i)>;
        file err1 <sprintf("intermediate/sitecomb_%i.err", i)>;
        string site_info[] = split(site, " ");
        
        //TODO:  hostmap_one_worker
        location rank = hostmap_one_worker(site_info[0]);
        
        printf("Site: %s Rank: %i String: %s\n", site_info[0], rank, site);
        
        (out1, err1) = @location=rank comb_data_local(local_combine, site); 
        comb_out[i] = out1;    
      }else{
        printf("Empty string\n");
      }          
    }
    
    foreach file_item in comb_out {
      printf("Item in comb_out : %s \n", filename(file_item));
    }

    file combine = input_file("/lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/combiner.sh");
    // We are doing a simple single step reduce here
    file final <"final_result">;
    file errs  <"final_errs">;
    (final, errs) = comb_data (combine, comb_out );
}

