#!/bin/bash

TIME=/usr/bin/time
# GENERATE 1 MB

/usr/bin/time -f "%e" ./teragen_wrap.sh 1 > tmp
exit 0

VAL=0
RUNS=10
for i in `seq 1 1 RUNS`
do    
    T=`/usr/bin/time -f "%e" ./teragen_wrap.sh 1 > tmp)`
    VAL=$(($VAL+$T))
    rm tmp;
done

echo " TOTAL TIME : $VAL "
echo " RUNS       : $RUNS"
echo " AVERAGE    : "; bc <<< "scale=5; ($VAL / $RUNS)"