#!/bin/bash
# The combiner is expecting to receive filenames as args
# Each file would contains strings in the format HOSTNAME FILEPATH

ARGS=($*)
PASSED_SITE=${ARGS[0]}
FILES=(${ARGS[*]:1})

CURRENT_HOST=$(hostname -f)
#CURRENT_HOST=$HOSTNAME

SUM=0
COUNT=0

fetch_and_process()
{
    TOKEN=($(cat $1))
    HOST=${TOKEN[0]}
    FILE=${TOKEN[1]}
    TARGET=$(basename $FILE)
    if [ "$HOST" != "$CURRENT_HOST" ]
    then
        # Fetch if file is remote
        chirp_get $HOST $TARGET /tmp/$TARGET.imd
        RES=($(awk '{ sum += $1 } END { print sum,NR }' /tmp/$TARGET.imd))
        rm /tmp/$TARGET.imd &> /dev/null &
    else
        # Process directly if file is local
        RES=($(awk '{ sum += $1 } END { print sum,NR }' $FILE))
    fi
    echo "${RES[0]} ${RES[1]}" > "$2.$RANDOM.imd2"
}

ID=$RANDOM
for file_token in ${FILES[*]}
do  
    fetch_and_process $file_token $ID &    
done
wait

TOKEN=($(cat ${FILES[0]}))
CHIRP_FOLDER=$(dirname ${TOKEN[1]})

SUM=$(awk '{ sum += $1 } END { print sum }' $ID*imd2)
COUNT=$(awk '{ sum += $2 } END { print sum }' $ID*imd2)
echo "$SUM $COUNT" > $CHIRP_FOLDER/$CURRENT_HOST.$ID.imd
echo "$CURRENT_HOST $CHIRP_FOLDER/$CURRENT_HOST.$ID.imd"
echo "$SUM $COUNT"
rm $ID*{imd,imd2} &> /dev/null &
exit 0