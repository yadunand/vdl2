
# Generated by stc version 0.2.0
# date                    : 2013/09/30 14:05:49
# Turbine version         : 0.3.0
# Input filename          : /lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/teragen.swift
# Output filename         : /lustre/beagle/yadunandb/Swift-MapRed/TurboChirp
# STC home                : /lustre/beagle/yadunandb/bin/exm-install/stc
# Turbine home            : /lustre/beagle/yadunandb/bin/exm-install/turbine
# Compiler settings:
# stc.auto-declare              : true
# stc.c_preprocess              : true
# stc.codegen.no-stack          : true
# stc.codegen.no-stack-vars     : true
# stc.compiler-debug            : true
# stc.debugging                 : COMMENTS
# stc.exp.refcounting           : true
# stc.ic.output-file            : 
# stc.input_filename            : teragen.swift
# stc.log.file                  : 
# stc.log.trace                 : false
# stc.must_pass_wait_vars       : true
# stc.opt.algebra               : false
# stc.opt.array-build           : true
# stc.opt.cancel-refcounts      : true
# stc.opt.constant-fold         : true
# stc.opt.controlflow-fusion    : true
# stc.opt.dead-code-elim        : true
# stc.opt.disable-asserts       : false
# stc.opt.expand-dataflow-ops   : true
# stc.opt.expand-loop-threshold-insts: 256
# stc.opt.expand-loop-threshold-iters: 16
# stc.opt.expand-loops          : true
# stc.opt.flatten-nested        : true
# stc.opt.forward-dataflow      : true
# stc.opt.full-unroll           : false
# stc.opt.function-inline       : false
# stc.opt.function-inline-threshold: 500
# stc.opt.function-signature    : true
# stc.opt.hoist                 : true
# stc.opt.max-iterations        : 10
# stc.opt.piggyback-refcounts   : true
# stc.opt.pipeline              : false
# stc.opt.reorder-insts         : false
# stc.opt.shared-constants      : true
# stc.opt.unroll-loop-threshold-insts: 192
# stc.opt.unroll-loop-threshold-iters: 8
# stc.opt.unroll-loops          : true
# stc.opt.wait-coalesce         : true
# stc.output_filename           : 
# stc.preproc.force-cpp         : false
# stc.preproc.force-gcc         : false
# stc.preprocess_only           : false
# stc.profile                   : false
# stc.rpath                     : 
# stc.stc_home                  : /lustre/beagle/yadunandb/bin/exm-install/stc
# stc.turbine.version           : 0.3.0
# stc.turbine_home              : /lustre/beagle/yadunandb/bin/exm-install/turbine
# stc.version                   : 0.2.0

# Metadata:

package require turbine 0.3.0
namespace import turbine::*


proc swift:constants {  } {
    turbine::c::log "function:swift:constants"
    global c:i_1
    turbine::allocate_custom c:i_1 integer 1 1 1
    turbine::store_integer ${c:i_1} 1
    global c:s__
    turbine::allocate_custom c:s__ string 1 1 1
    turbine::store_string ${c:s__} " "
    global c:s__-1
    turbine::allocate_custom c:s__-1 string 1 1 1
    turbine::store_string ${c:s__-1} "\n"
}


proc f:gen_data { stack u:out u:err u:run u:recsize dr:location } {
    turbine::c::log "enter function: gen_data"
    # Alias __filename_run with type string was defined
    # Alias __filename_out with type string was defined
    # Value __ov_mapped_out with type $boolean was defined
    # Alias __filename_err with type string was defined
    # Value __ov_mapped_err with type $boolean was defined
    turbine::file_read_refcount_incr ${u:run} 1
    turbine::read_refcount_incr ${u:recsize} 1
    set filename:run [ turbine::get_file_path ${u:run} ]
    turbine::read_refcount_incr ${filename:run} 1
    set optv:mapped_out [ turbine::is_file_mapped ${u:out} ]
    if { ${optv:mapped_out} } {
        set filename:out [ turbine::get_file_path ${u:out} ]
    } else {
        # Value __of_out with type $string was defined
        set optf:out [ turbine::mktemp ]
        turbine::set_filename_val ${u:out} ${optf:out}
        set filename:out [ turbine::get_file_path ${u:out} ]
    }
    turbine::read_refcount_incr ${filename:out} 1
    set optv:mapped_err [ turbine::is_file_mapped ${u:err} ]
    if { ${optv:mapped_err} } {
        set filename:err [ turbine::get_file_path ${u:err} ]
    } else {
        # Value __of_err with type $string was defined
        set optf:err [ turbine::mktemp ]
        turbine::set_filename_val ${u:err} ${optf:err}
        set filename:err [ turbine::get_file_path ${u:err} ]
    }
    turbine::read_refcount_incr ${filename:err} 1
    turbine::rule [ list ${filename:err} ${filename:out} ${filename:run} ${u:recsize} [ turbine::get_file_status ${u:run} ] ] "gen_data-app-leaf0 ${stack} ${optv:mapped_out} {${u:err}} ${filename:out} ${filename:run} ${optv:mapped_err} ${filename:err} {${u:run}} ${u:recsize} {${u:out}}" target ${dr:location} type ${::turbine::WORK}
}


proc gen_data-app-leaf0 { stack optv:mapped_out u:err filename:out filename:run optv:mapped_err filename:err u:run u:recsize u:out } {
    # Value __v___filename_run with type $string was defined
    # Value __v_recsize with type $int was defined
    # Value __v___filename_out with type $string was defined
    # Value __v___filename_err with type $string was defined
    # Value __v_run with type $file was defined
    # Value __v_out with type $file was defined
    # Value __v_err with type $file was defined
    set v:__filename_run [ turbine::retrieve_string ${filename:run} CACHED 1 ]
    set v:recsize [ turbine::retrieve_integer ${u:recsize} CACHED 1 ]
    set v:__filename_out [ turbine::retrieve_string ${filename:out} CACHED 1 ]
    set v:__filename_err [ turbine::retrieve_string ${filename:err} CACHED 1 ]
    set v:run [ turbine::get_file ${u:run} 1 ]
    if { ${optv:mapped_out} } {
        set tcltmp:init_rc 2
    } else {
        set tcltmp:init_rc 1
    }
    set v:out [ turbine::create_local_file_ref ${v:__filename_out} ${tcltmp:init_rc} ]
    if { ${optv:mapped_err} } {
        set tcltmp:init_rc 2
    } else {
        set tcltmp:init_rc 1
    }
    set v:err [ turbine::create_local_file_ref ${v:__filename_err} ${tcltmp:init_rc} ]
    turbine::c::log [ list exec: /bin/bash ${v:__filename_run} ${v:recsize} [ dict create "stdout" ${v:__filename_out} "stderr" ${v:__filename_err} ] ]
    turbine::exec_external "/bin/bash" [ dict create "stdout" ${v:__filename_out} "stderr" ${v:__filename_err} ] ${v:__filename_run} ${v:recsize}
    turbine::set_file ${u:out} v:out
    turbine::set_file ${u:err} v:err
    turbine::decr_local_file_refcount v:out
    turbine::decr_local_file_refcount v:err
}


proc f:comb_data { stack u:out u:err u:comb u:array dr:location } {
    turbine::c::log "enter function: comb_data"
    # Alias __filename_comb with type string was defined
    # Alias __filename_out with type string was defined
    # Value __ov_mapped_out with type $boolean was defined
    # Alias __filename_err with type string was defined
    # Value __ov_mapped_err with type $boolean was defined
    turbine::file_read_refcount_incr ${u:comb} 1
    turbine::read_refcount_incr ${u:array} 1
    set filename:comb [ turbine::get_file_path ${u:comb} ]
    turbine::read_refcount_incr ${filename:comb} 1
    set optv:mapped_out [ turbine::is_file_mapped ${u:out} ]
    if { ${optv:mapped_out} } {
        set filename:out [ turbine::get_file_path ${u:out} ]
    } else {
        # Value __of_out with type $string was defined
        set optf:out [ turbine::mktemp ]
        turbine::set_filename_val ${u:out} ${optf:out}
        set filename:out [ turbine::get_file_path ${u:out} ]
    }
    turbine::read_refcount_incr ${filename:out} 1
    set optv:mapped_err [ turbine::is_file_mapped ${u:err} ]
    if { ${optv:mapped_err} } {
        set filename:err [ turbine::get_file_path ${u:err} ]
    } else {
        # Value __of_err with type $string was defined
        set optf:err [ turbine::mktemp ]
        turbine::set_filename_val ${u:err} ${optf:err}
        set filename:err [ turbine::get_file_path ${u:err} ]
    }
    turbine::read_refcount_incr ${filename:err} 1
    turbine::deeprule [ list ${filename:comb} ${filename:err} ${filename:out} ${u:array} [ turbine::get_file_status ${u:comb} ] ] [ list 0 0 0 1 0 ] [ list 0 0 0 1 1 ] "comb_data-app-leaf0 ${stack} ${optv:mapped_out} {${u:err}} ${filename:out} ${optv:mapped_err} ${filename:err} {${u:comb}} ${filename:comb} {${u:out}} ${u:array}" target ${dr:location} type ${::turbine::WORK}
}


proc comb_data-app-leaf0 { stack optv:mapped_out u:err filename:out optv:mapped_err filename:err u:comb filename:comb u:out u:array } {
    # Value __v___filename_comb with type $string was defined
    # Value __v___filename_out with type $string was defined
    # Value __v___filename_err with type $string was defined
    # Value __v_comb with type $file was defined
    # Value __v_out with type $file was defined
    # Value __v_err with type $file was defined
    set v:__filename_comb [ turbine::retrieve_string ${filename:comb} CACHED 1 ]
    set v:__filename_out [ turbine::retrieve_string ${filename:out} CACHED 1 ]
    set v:__filename_err [ turbine::retrieve_string ${filename:err} CACHED 1 ]
    set v:comb [ turbine::get_file ${u:comb} 1 ]
    if { ${optv:mapped_out} } {
        set tcltmp:init_rc 2
    } else {
        set tcltmp:init_rc 1
    }
    set v:out [ turbine::create_local_file_ref ${v:__filename_out} ${tcltmp:init_rc} ]
    if { ${optv:mapped_err} } {
        set tcltmp:init_rc 2
    } else {
        set tcltmp:init_rc 1
    }
    set v:err [ turbine::create_local_file_ref ${v:__filename_err} ${tcltmp:init_rc} ]
    set tcltmp:unpacked1 [ turbine::unpack_args ${u:array} 1 1 ]
    turbine::c::log [ list exec: /bin/bash ${v:__filename_comb} {*}${tcltmp:unpacked1} [ dict create "stdout" ${v:__filename_out} "stderr" ${v:__filename_err} ] ]
    turbine::exec_external "/bin/bash" [ dict create "stdout" ${v:__filename_out} "stderr" ${v:__filename_err} ] ${v:__filename_comb} {*}${tcltmp:unpacked1}
    turbine::set_file ${u:out} v:out
    turbine::set_file ${u:err} v:err
    turbine::decr_local_file_refcount v:out
    turbine::decr_local_file_refcount v:err
    turbine::read_refcount_decr ${u:array} 1
}


proc f:comb_data_local { stack u:out u:err u:comb u:array dr:location } {
    turbine::c::log "enter function: comb_data_local"
    # Alias __filename_comb with type string was defined
    # Alias __filename_out with type string was defined
    # Value __ov_mapped_out with type $boolean was defined
    # Alias __filename_err with type string was defined
    # Value __ov_mapped_err with type $boolean was defined
    turbine::file_read_refcount_incr ${u:comb} 1
    turbine::read_refcount_incr ${u:array} 1
    set filename:comb [ turbine::get_file_path ${u:comb} ]
    turbine::read_refcount_incr ${filename:comb} 1
    set optv:mapped_out [ turbine::is_file_mapped ${u:out} ]
    if { ${optv:mapped_out} } {
        set filename:out [ turbine::get_file_path ${u:out} ]
    } else {
        # Value __of_out with type $string was defined
        set optf:out [ turbine::mktemp ]
        turbine::set_filename_val ${u:out} ${optf:out}
        set filename:out [ turbine::get_file_path ${u:out} ]
    }
    turbine::read_refcount_incr ${filename:out} 1
    set optv:mapped_err [ turbine::is_file_mapped ${u:err} ]
    if { ${optv:mapped_err} } {
        set filename:err [ turbine::get_file_path ${u:err} ]
    } else {
        # Value __of_err with type $string was defined
        set optf:err [ turbine::mktemp ]
        turbine::set_filename_val ${u:err} ${optf:err}
        set filename:err [ turbine::get_file_path ${u:err} ]
    }
    turbine::read_refcount_incr ${filename:err} 1
    turbine::rule [ list ${filename:comb} ${filename:err} ${filename:out} ${u:array} [ turbine::get_file_status ${u:comb} ] ] "comb_data_local-app-leaf0 ${stack} ${optv:mapped_out} {${u:err}} ${filename:out} ${optv:mapped_err} ${filename:err} {${u:comb}} ${filename:comb} {${u:out}} ${u:array}" target ${dr:location} type ${::turbine::WORK}
}


proc comb_data_local-app-leaf0 { stack optv:mapped_out u:err filename:out optv:mapped_err filename:err u:comb filename:comb u:out u:array } {
    # Value __v___filename_comb with type $string was defined
    # Value __v_array with type $string was defined
    # Value __v___filename_out with type $string was defined
    # Value __v___filename_err with type $string was defined
    # Value __v_comb with type $file was defined
    # Value __v_out with type $file was defined
    # Value __v_err with type $file was defined
    set v:__filename_comb [ turbine::retrieve_string ${filename:comb} CACHED 1 ]
    set v:array [ turbine::retrieve_string ${u:array} CACHED 1 ]
    set v:__filename_out [ turbine::retrieve_string ${filename:out} CACHED 1 ]
    set v:__filename_err [ turbine::retrieve_string ${filename:err} CACHED 1 ]
    set v:comb [ turbine::get_file ${u:comb} 1 ]
    if { ${optv:mapped_out} } {
        set tcltmp:init_rc 2
    } else {
        set tcltmp:init_rc 1
    }
    set v:out [ turbine::create_local_file_ref ${v:__filename_out} ${tcltmp:init_rc} ]
    if { ${optv:mapped_err} } {
        set tcltmp:init_rc 2
    } else {
        set tcltmp:init_rc 1
    }
    set v:err [ turbine::create_local_file_ref ${v:__filename_err} ${tcltmp:init_rc} ]
    turbine::c::log [ list exec: /bin/bash ${v:__filename_comb} ${v:array} [ dict create "stdout" ${v:__filename_out} "stderr" ${v:__filename_err} ] ]
    turbine::exec_external "/bin/bash" [ dict create "stdout" ${v:__filename_out} "stderr" ${v:__filename_err} ] ${v:__filename_comb} ${v:array}
    turbine::set_file ${u:out} v:out
    turbine::set_file ${u:err} v:err
    turbine::decr_local_file_refcount v:out
    turbine::decr_local_file_refcount v:err
}


proc f:get_uniq_sites { stack u:out u:sites u:array dr:location } {
    turbine::c::log "enter function: get_uniq_sites"
    # Alias __filename_sites with type string was defined
    # Alias __filename_out with type string was defined
    # Value __ov_mapped_out with type $boolean was defined
    turbine::read_refcount_incr ${u:array} 1
    turbine::file_read_refcount_incr ${u:sites} 1
    set filename:sites [ turbine::get_file_path ${u:sites} ]
    turbine::read_refcount_incr ${filename:sites} 1
    set optv:mapped_out [ turbine::is_file_mapped ${u:out} ]
    if { ${optv:mapped_out} } {
        set filename:out [ turbine::get_file_path ${u:out} ]
    } else {
        # Value __of_out with type $string was defined
        set optf:out [ turbine::mktemp ]
        turbine::set_filename_val ${u:out} ${optf:out}
        set filename:out [ turbine::get_file_path ${u:out} ]
    }
    turbine::read_refcount_incr ${filename:out} 1
    turbine::deeprule [ list ${filename:out} ${filename:sites} ${u:array} [ turbine::get_file_status ${u:sites} ] ] [ list 0 0 1 0 ] [ list 0 0 1 1 ] "get_uniq_sites-app-leaf0 ${stack} {${u:sites}} ${optv:mapped_out} ${filename:out} {${u:out}} ${filename:sites} ${u:array}" target ${dr:location} type ${::turbine::WORK}
}


proc get_uniq_sites-app-leaf0 { stack u:sites optv:mapped_out filename:out u:out filename:sites u:array } {
    # Value __v___filename_sites with type $string was defined
    # Value __v___filename_out with type $string was defined
    # Value __v_sites with type $file was defined
    # Value __v_out with type $file was defined
    set v:__filename_sites [ turbine::retrieve_string ${filename:sites} CACHED 1 ]
    set v:__filename_out [ turbine::retrieve_string ${filename:out} CACHED 1 ]
    set v:sites [ turbine::get_file ${u:sites} 1 ]
    if { ${optv:mapped_out} } {
        set tcltmp:init_rc 2
    } else {
        set tcltmp:init_rc 1
    }
    set v:out [ turbine::create_local_file_ref ${v:__filename_out} ${tcltmp:init_rc} ]
    set tcltmp:unpacked1 [ turbine::unpack_args ${u:array} 1 1 ]
    turbine::c::log [ list exec: /bin/bash ${v:__filename_sites} {*}${tcltmp:unpacked1} [ dict create "stdout" ${v:__filename_out} ] ]
    turbine::exec_external "/bin/bash" [ dict create "stdout" ${v:__filename_out} ] ${v:__filename_sites} {*}${tcltmp:unpacked1}
    turbine::set_file ${u:out} v:out
    turbine::decr_local_file_refcount v:out
    turbine::read_refcount_decr ${u:array} 1
}


proc swift:main {  } {
    turbine::c::log "enter function: main"
    set stack 0
    global c:s__-1
    # Value __ov_wrapper with type $file was defined
    # Value __ov_jobs_per_site with type $file was defined
    # Value __ov_local_combine with type $file was defined
    # Value __ov_combine with type $file was defined
    lassign [ adlb::multicreate [ list container integer file_ref 1 1 ] [ list container integer ref 1 1 ] [ list string 2 ] [ list container integer ref 1 1 ] [ list string 1 ] [ list container integer file_ref 1 1 ] [ list string 2 ] [ list string 2 ] ] u:tgen_out t:1 t:10 u:sites t:11 u:comb_out t:34 t:35
    turbine::c::log "allocated u:tgen_out=<${u:tgen_out}> t:1=<${t:1}> t:10=<${t:10}> u:sites=<${u:sites}> t:11=<${t:11}>"
    turbine::c::log "allocated u:comb_out=<${u:comb_out}> t:34=<${t:34}> t:35=<${t:35}>"
    turbine::allocate_file2 u:wrapper "" 1
    turbine::allocate_file2 u:jobs_per_site "" 1
    turbine::allocate_file2 u:combiner_list ${t:10} 1
    turbine::allocate_file2 u:local_combine "" 1
    turbine::allocate_file2 u:combine "" 1
    turbine::allocate_file2 u:final ${t:34} 1
    turbine::allocate_file2 u:errs ${t:35} 1
    # Swift l.27: assigning expression to wrapper
    set optv:wrapper [ turbine::input_file_local "/lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/teragen_wrap.sh" ]
    turbine::set_filename_val ${u:wrapper} "/lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/teragen_wrap.sh"
    turbine::set_file ${u:wrapper} optv:wrapper
    # Swift l.30: assigning expression to loop
    # Swift l.32: assigning expression to fsize
    turbine::range_work ${t:1} 0 9 1
    # Swift l.42: assigning expression to jobs_per_site
    set optv:jobs_per_site [ turbine::input_file_local "/lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/jobs_per_site.sh" ]
    turbine::set_filename_val ${u:jobs_per_site} "/lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/jobs_per_site.sh"
    turbine::set_file ${u:jobs_per_site} optv:jobs_per_site
    turbine::store_string ${t:10} "intermediate/uniq_sites"
    # Swift l.44: assigning expression to combiner_list
    f:get_uniq_sites ${stack} ${u:combiner_list} ${u:jobs_per_site} ${u:tgen_out} -100
    # Swift l.47: assigning expression to sites
    turbine::split [ list ${u:sites} ] [ list ${t:11} ${c:s__-1} ]
    # Swift l.49: assigning expression to local_combine
    set optv:local_combine [ turbine::input_file_local "/lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/local_combiner.sh" ]
    turbine::set_filename_val ${u:local_combine} "/lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/local_combiner.sh"
    turbine::set_file ${u:local_combine} optv:local_combine
    # Swift l.75: assigning expression to combine
    set optv:combine [ turbine::input_file_local "/lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/combiner.sh" ]
    turbine::set_filename_val ${u:combine} "/lustre/beagle/yadunandb/Swift-MapRed/TurboChirp/combiner.sh"
    turbine::set_file ${u:combine} optv:combine
    turbine::store_string ${t:34} "final_result"
    turbine::store_string ${t:35} "final_errs"
    # Swift l.79: assigning expression to final
    # Swift l.79: assigning expression to errs
    f:comb_data ${stack} ${u:final} ${u:errs} ${u:combine} ${u:comb_out} -100
    # Swift l.38: assigning expression to out
    # Swift l.38: assigning expression to err
    # Swift l.39: assigning expression to tgen_out[_]
    turbine::rule [ list ${u:sites} ] "main-foreach-wait1 ${stack} ${u:sites} {${u:local_combine}} ${u:comb_out}" type ${::turbine::LOCAL}
    turbine::rule [ list ${u:comb_out} ] "main-foreach-wait2 ${stack} ${u:comb_out}" type ${::turbine::LOCAL}
    set tcltmp:container_sz [ adlb::enumerate ${t:1} count all 0 ]
    set tcltmp:iters ${tcltmp:container_sz}
    turbine::file_read_refcount_incr ${u:wrapper} [ expr { ${tcltmp:iters} - 1 } ]
    turbine::read_refcount_incr ${t:1} [ expr { ${tcltmp:iters} - 1 } ]
    adlb::write_refcount_incr ${u:tgen_out} [ expr { ${tcltmp:iters} - 1 } ]
    main-foreach0:outer ${stack} ${u:wrapper} ${u:tgen_out} ${t:1} 0 [ expr { ${tcltmp:container_sz} - 1 } ] 1
    turbine::rule [ list [ turbine::get_file_status ${u:combiner_list} ] ] "main-call_foreign-read ${stack} ${t:11} {${u:combiner_list}}" type ${::turbine::WORK}
    turbine::decr_local_file_refcount optv:wrapper
    turbine::decr_local_file_refcount optv:jobs_per_site
    turbine::decr_local_file_refcount optv:local_combine
    turbine::decr_local_file_refcount optv:combine
    turbine::read_refcount_decr ${u:tgen_out} 1
    turbine::file_read_refcount_decr ${u:errs} 1
    turbine::file_read_refcount_decr ${u:jobs_per_site} 1
    turbine::file_read_refcount_decr ${u:final} 1
    turbine::file_read_refcount_decr ${u:combine} 1
}


proc main-foreach-wait1 { stack u:sites u:local_combine u:comb_out } {
    set tcltmp:container_sz [ adlb::enumerate ${u:sites} count all 0 ]
    set tcltmp:iters ${tcltmp:container_sz}
    turbine::read_refcount_incr ${u:sites} [ expr { ${tcltmp:iters} - 1 } ]
    turbine::file_read_refcount_incr ${u:local_combine} [ expr { ${tcltmp:iters} - 1 } ]
    adlb::write_refcount_incr ${u:comb_out} [ expr { ${tcltmp:iters} - 1 } ]
    main-foreach1:outer ${stack} ${u:local_combine} ${u:comb_out} ${u:sites} 0 [ expr { ${tcltmp:container_sz} - 1 } ] 1
}


proc main-foreach1:outer { stack u:local_combine u:comb_out u:sites tcltmp:lo tcltmp:hi tcltmp:inc } {
    set tcltmp:itersleft [ expr { max(0,(${tcltmp:hi} - ${tcltmp:lo}) / ${tcltmp:inc} + 1) } ]
    if { [ expr { ${tcltmp:itersleft} <= 0 } ] } {
        return
    }
    if { [ expr { ${tcltmp:itersleft} <= 64 } ] } {
        main-foreach1:inner ${stack} ${u:local_combine} ${u:comb_out} ${u:sites} ${tcltmp:lo} ${tcltmp:hi} ${tcltmp:inc}
    } else {
        set tcltmp:skip [ expr { ${tcltmp:inc} * max(64,((${tcltmp:itersleft} - 1) / 16) + 1) } ]
        for { set tcltmp:splitstart ${tcltmp:lo} } { ${tcltmp:splitstart} <= ${tcltmp:hi} } { incr tcltmp:splitstart ${tcltmp:skip} } {
            set tcltmp:splitend [ expr { min(${tcltmp:hi},${tcltmp:splitstart} + ${tcltmp:skip} - 1) } ]
            set tcltmp:prio [ turbine::get_priority ]
            turbine::set_priority ${tcltmp:prio}
            adlb::spawn 1 "command main-foreach1:outer ${stack} {${u:local_combine}} ${u:comb_out} ${u:sites} ${tcltmp:splitstart} ${tcltmp:splitend} ${tcltmp:inc}"
            turbine::reset_priority
        }
    }
}


proc main-foreach1:inner { stack u:local_combine u:comb_out u:sites tcltmp:lo tcltmp:hi tcltmp:inc } {
    set tcltmp:splitlen [ expr { ${tcltmp:hi} - ${tcltmp:lo} + 1 } ]
    set tcltmp:contents [ adlb::enumerate ${u:sites} dict ${tcltmp:splitlen} ${tcltmp:lo} ]
    dict for {v:i0 u:site} ${tcltmp:contents} {
        lassign [ adlb::multicreate [ list integer 1 ] ] t:14
        turbine::c::log "allocated t:14=<${t:14}>"
        turbine::read_refcount_incr ${u:site} 1
        turbine::rule [ list ${u:site} ] "main-call_sync-strlen ${stack} ${u:site} ${v:i0} {${u:local_combine}} ${u:comb_out} ${t:14}"
    }
    set tcltmp:iters [ expr { max(0,(${tcltmp:hi} - ${tcltmp:lo}) / ${tcltmp:inc} + 1) } ]
    turbine::read_refcount_decr ${u:sites} [ expr { ${tcltmp:iters} } ]
}


proc main-call_sync-strlen { stack u:site v:i0 u:local_combine u:comb_out t:14 } {
    # Value __ov_site with type $string was defined
    # Value __ov___t15 with type $int was defined
    # Value __ov___t14 with type $boolean was defined
    # Value __v___t14 with type $boolean was defined
    set optv:site [ turbine::retrieve_string ${u:site} ]
    set optv:__t15 [ string length ${optv:site} ]
    set optv:__t14 [ expr { ${optv:__t15} > 0 } ]
    turbine::store_integer ${t:14} ${optv:__t14}
    set v:__t14 [ turbine::retrieve_integer ${t:14} CACHED 1 ]
    if { ${v:__t14} } {
        global c:s__
        # Value __ov___t17 with type $string was defined
        # Value __ov___t19 with type $string was defined
        lassign [ adlb::multicreate [ list string 2 ] [ list string 2 ] [ list container integer ref 1 1 ] [ list string 1 ] [ list ref 1 ] ] t:17 t:19 u:site_info t:22 t:23
        turbine::c::log "allocated t:17=<${t:17}> t:19=<${t:19}> u:site_info=<${u:site_info}> t:22=<${t:22}> t:23=<${t:23}>"
        turbine::allocate_file2 u:out1 ${t:17} 2
        turbine::allocate_file2 u:err1 ${t:19} 1
        turbine::file_read_refcount_incr ${u:local_combine} 1
        turbine::read_refcount_incr ${u:site} 2
        set optv:__t17 [ eval format [ list "intermediate/sitecomb_%i.out" ${v:i0} ] ]
        turbine::store_string ${t:17} ${optv:__t17}
        set optv:__t19 [ eval format [ list "intermediate/sitecomb_%i.err" ${v:i0} ] ]
        turbine::store_string ${t:19} ${optv:__t19}
        # Swift l.57: assigning expression to site_info
        turbine::split [ list ${u:site_info} ] [ list ${u:site} ${c:s__} ]
        # Swift l.60: assigning expression to rank
        turbine::container_reference ${u:site_info} 0 ${t:23} ref
        turbine::dereference_string ${t:22} ${t:23}
        # Swift l.62 evaluating  expression and throwing away 1 results
        # Swift l.64: assigning expression to out1
        # Swift l.64: assigning expression to err1
        # Swift l.65: assigning expression to comb_out[_]
        turbine::container_insert ${u:comb_out} ${v:i0} ${u:out1} file_ref 0
        turbine::rule [ list ${t:22} ] [ list main-call_sync-hostmap_one_worker ${stack} ${u:site} ${t:22} ${u:local_combine} ${optv:site} ${u:out1} ${u:err1} ]
    } else {
        # Value __ov___t28 with type $void was defined
        # Swift l.67 evaluating  expression and throwing away 1 results
        set optv:__t28 [ turbine::printf_local "Empty string\n" ]
    }
    turbine::read_refcount_decr ${u:site} 1
    turbine::file_read_refcount_decr ${u:local_combine} 1
    adlb::write_refcount_decr ${u:comb_out} 1
}


proc main-call_sync-hostmap_one_worker { stack u:site t:22 u:local_combine optv:site u:out1 u:err1 } {
    # Value __ov___t22 with type $string was defined
    # Value __ov_rank with type $int was defined
    # Value __ov___t24 with type $void was defined
    set optv:__t22 [ turbine::retrieve_string ${t:22} CACHED 1 ]
    set optv:rank [ ::turbine::random_rank WORKER [ adlb::hostmap_lookup ${optv:__t22} ] ]
    f:comb_data_local ${stack} ${u:out1} ${u:err1} ${u:local_combine} ${u:site} ${optv:rank}
    set optv:__t24 [ turbine::printf_local "Site: %s Rank: %i String: %s\n" ${optv:__t22} ${optv:rank} ${optv:site} ]
    turbine::read_refcount_decr ${u:site} 1
    turbine::file_read_refcount_decr ${u:local_combine} 1
    turbine::file_read_refcount_decr ${u:err1} 1
    turbine::file_read_refcount_decr ${u:out1} 1
}


proc main-foreach-wait2 { stack u:comb_out } {
    # Swift l.72 evaluating  expression and throwing away 1 results
    set tcltmp:container_sz [ adlb::enumerate ${u:comb_out} count all 0 ]
    set tcltmp:iters ${tcltmp:container_sz}
    turbine::read_refcount_incr ${u:comb_out} [ expr { ${tcltmp:iters} - 1 } ]
    main-foreach2:outer ${stack} ${u:comb_out} 0 [ expr { ${tcltmp:container_sz} - 1 } ] 1
}


proc main-foreach2:outer { stack u:comb_out tcltmp:lo tcltmp:hi tcltmp:inc } {
    set tcltmp:itersleft [ expr { max(0,(${tcltmp:hi} - ${tcltmp:lo}) / ${tcltmp:inc} + 1) } ]
    if { [ expr { ${tcltmp:itersleft} <= 0 } ] } {
        return
    }
    if { [ expr { ${tcltmp:itersleft} <= 64 } ] } {
        main-foreach2:inner ${stack} ${u:comb_out} ${tcltmp:lo} ${tcltmp:hi} ${tcltmp:inc}
    } else {
        set tcltmp:skip [ expr { ${tcltmp:inc} * max(64,((${tcltmp:itersleft} - 1) / 16) + 1) } ]
        for { set tcltmp:splitstart ${tcltmp:lo} } { ${tcltmp:splitstart} <= ${tcltmp:hi} } { incr tcltmp:splitstart ${tcltmp:skip} } {
            set tcltmp:splitend [ expr { min(${tcltmp:hi},${tcltmp:splitstart} + ${tcltmp:skip} - 1) } ]
            set tcltmp:prio [ turbine::get_priority ]
            turbine::set_priority ${tcltmp:prio}
            adlb::spawn 1 "command main-foreach2:outer ${stack} ${u:comb_out} ${tcltmp:splitstart} ${tcltmp:splitend} ${tcltmp:inc}"
            turbine::reset_priority
        }
    }
}


proc main-foreach2:inner { stack u:comb_out tcltmp:lo tcltmp:hi tcltmp:inc } {
    set tcltmp:splitlen [ expr { ${tcltmp:hi} - ${tcltmp:lo} + 1 } ]
    set tcltmp:contents [ adlb::enumerate ${u:comb_out} members ${tcltmp:splitlen} ${tcltmp:lo} ]
    foreach u:file_item ${tcltmp:contents} {
        # Alias __of_file_item with type string was defined
        set optf:file_item [ turbine::get_file_path ${u:file_item} ]
        turbine::read_refcount_incr ${optf:file_item} 1
        turbine::rule [ list ${optf:file_item} ] "main-call_foreign-printf ${stack} ${optf:file_item}"
    }
    set tcltmp:iters [ expr { max(0,(${tcltmp:hi} - ${tcltmp:lo}) / ${tcltmp:inc} + 1) } ]
    turbine::read_refcount_decr ${u:comb_out} [ expr { ${tcltmp:iters} } ]
}


proc main-call_foreign-printf { stack optf:file_item } {
    # Value __ov___of_file_item with type $string was defined
    # Value __ov___t30 with type $void was defined
    set optv:__of_file_item [ turbine::retrieve_string ${optf:file_item} CACHED 1 ]
    set optv:__t30 [ turbine::printf_local "Item in comb_out : %s \n" ${optv:__of_file_item} ]
}


proc main-foreach0:outer { stack u:wrapper u:tgen_out t:1 tcltmp:lo tcltmp:hi tcltmp:inc } {
    set tcltmp:itersleft [ expr { max(0,(${tcltmp:hi} - ${tcltmp:lo}) / ${tcltmp:inc} + 1) } ]
    if { [ expr { ${tcltmp:itersleft} <= 0 } ] } {
        return
    }
    if { [ expr { ${tcltmp:itersleft} <= 64 } ] } {
        main-foreach0:inner ${stack} ${u:wrapper} ${u:tgen_out} ${t:1} ${tcltmp:lo} ${tcltmp:hi} ${tcltmp:inc}
    } else {
        set tcltmp:skip [ expr { ${tcltmp:inc} * max(64,((${tcltmp:itersleft} - 1) / 16) + 1) } ]
        for { set tcltmp:splitstart ${tcltmp:lo} } { ${tcltmp:splitstart} <= ${tcltmp:hi} } { incr tcltmp:splitstart ${tcltmp:skip} } {
            set tcltmp:splitend [ expr { min(${tcltmp:hi},${tcltmp:splitstart} + ${tcltmp:skip} - 1) } ]
            set tcltmp:prio [ turbine::get_priority ]
            turbine::set_priority ${tcltmp:prio}
            adlb::spawn 1 "command main-foreach0:outer ${stack} {${u:wrapper}} ${u:tgen_out} ${t:1} ${tcltmp:splitstart} ${tcltmp:splitend} ${tcltmp:inc}"
            turbine::reset_priority
        }
    }
}


proc main-foreach0:inner { stack u:wrapper u:tgen_out t:1 tcltmp:lo tcltmp:hi tcltmp:inc } {
    set tcltmp:splitlen [ expr { ${tcltmp:hi} - ${tcltmp:lo} + 1 } ]
    set tcltmp:contents [ adlb::enumerate ${t:1} dict ${tcltmp:splitlen} ${tcltmp:lo} ]
    dict for {v:i u:item} ${tcltmp:contents} {
        global c:i_1
        # Value __ov___t5 with type $string was defined
        # Value __ov___t7 with type $string was defined
        lassign [ adlb::multicreate [ list string 2 ] [ list string 2 ] ] t:5 t:7
        turbine::c::log "allocated t:5=<${t:5}> t:7=<${t:7}>"
        turbine::allocate_file2 u:out ${t:5} 1
        turbine::allocate_file2 u:err ${t:7} 1
        set optv:__t5 [ eval format [ list "intermediate/tgen_%i.out" ${v:i} ] ]
        turbine::store_string ${t:5} ${optv:__t5}
        set optv:__t7 [ eval format [ list "intermediate/tgen_%i.err" ${v:i} ] ]
        turbine::store_string ${t:7} ${optv:__t7}
        f:gen_data ${stack} ${u:out} ${u:err} ${u:wrapper} ${c:i_1} -100
        turbine::container_insert ${u:tgen_out} ${v:i} ${u:out} file_ref 1
        turbine::file_read_refcount_decr ${u:err} 1
    }
    set tcltmp:iters [ expr { max(0,(${tcltmp:hi} - ${tcltmp:lo}) / ${tcltmp:inc} + 1) } ]
    turbine::file_read_refcount_decr ${u:wrapper} [ expr { ${tcltmp:iters} } ]
    turbine::read_refcount_decr ${t:1} [ expr { ${tcltmp:iters} } ]
}


proc main-call_foreign-read { stack t:11 u:combiner_list } {
    # Value __ov_combiner_list with type $file was defined
    # Value __ov___t11 with type $string was defined
    set optv:combiner_list [ turbine::get_file ${u:combiner_list} 1 ]
    set optv:__t11 [ turbine::file_read_local ${optv:combiner_list} ]
    turbine::store_string ${t:11} ${optv:__t11}
}


proc f:strlen { stack u:n u:s } {
    turbine::c::log "enter function: strlen"
    turbine::read_refcount_incr ${u:s} 1
    turbine::rule [ list ${u:s} ] "strlen-argwait ${stack} ${u:s} ${u:n}"
}


proc strlen-argwait { stack u:s u:n } {
    # Value __v_s with type $string was defined
    # Value __v_n with type $int was defined
    set v:s [ turbine::retrieve_string ${u:s} CACHED 1 ]
    set v:n [ string length ${v:s} ]
    turbine::store_integer ${u:n} ${v:n}
}


proc f:hostmap_one_worker { stack u:rank u:name } {
    turbine::c::log "enter function: hostmap_one_worker"
    turbine::read_refcount_incr ${u:name} 1
    turbine::rule [ list ${u:name} ] "hostmap_one_worker-argwait ${stack} ${u:rank} ${u:name}"
}


proc hostmap_one_worker-argwait { stack u:rank u:name } {
    # Value __v_name with type $string was defined
    # Value __v_rank with type $int was defined
    set v:name [ turbine::retrieve_string ${u:name} CACHED 1 ]
    set v:rank [ ::turbine::random_rank WORKER [ adlb::hostmap_lookup ${v:name} ] ]
    turbine::store_integer ${u:rank} ${v:rank}
}

turbine::defaults
turbine::init $engines $servers
turbine::enable_read_refcount
turbine::check_constants "WORKER" ${turbine::WORK_TASK} 0 "CONTROL" ${turbine::CONTROL_TASK} 1 "ADLB_RANK_ANY" ${adlb::RANK_ANY} -100
turbine::start swift:main swift:constants
turbine::finalize

