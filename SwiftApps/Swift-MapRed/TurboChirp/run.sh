#!/bin/bash -e

if [ "$1" == "" ]
then
    PROG=teragen.swift
else
    PROG=$1
fi


#LOGIN NODE
#~wozniak/Public/turbine-trunk-beagle-login
#export PATH=$PATH

./cleanup
export TURBINE_LOG=1
export ADLB_EXHAUST_TIME=0.1
export TURBINE_LAUNCH_OPTS="-f ./hosts.txt"
stc $PROG
[ "$?" == "0" ] && echo "Compile done"
#turbine -n 8 ${PROG%.swift}.tcl
#turbine -l -V -n 10 -f hosts.txt ${PROG%.swift}.tcl -loop=10 -fsize=1 >& ${PROG%.swift}.out

time turbine -l -n 10 -f hosts.txt ${PROG%.swift}.tcl -loop=10 -fsize=1

[ "$?" == "0" ] && echo "Execute done"
