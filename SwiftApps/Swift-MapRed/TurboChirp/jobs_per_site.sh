#!/bin/bash

cat $* > tmp;
FILES=$*

SITES=($(awk '{ print $1 }' tmp  | sort -u))

for SITE in ${SITES[*]}
do
    FILES=$(grep -l $SITE intermediate/tgen*out)
    echo $SITE ${FILES[*]}
done
rm tmp;