type file;

app (file epout) runEP ( file runep, file imf, file epw, int orientation, int height)
{
  # runenergyplus CHICAGO-EXAMPLE.imf CHICAGO.epw 
  sh @runep "--imf" @imf "--epw" @epw "--app" appurl "--out" @epout
            "--params" "ORIENTATION" orientation "HEIGHT" height ;
}

file epconfig  <single_file_mapper; file="CHICAGO-EXAMPLE.imf">;
file epweather <single_file_mapper; file="CHICAGO.epw">;
file runep     <single_file_mapper; file="RunEP.sh">;
global string appurl = "http://stash.osgconnect.net/+wilde/EnergyPlus-8.0.0.tgz";

string outdir=@arg("outdir","sweep-out");

foreach orientation in [0:90:45] {
  foreach height in [33,95] {
    file out <single_file_mapper; file=@strcat(outdir,"/ep.",orientation,".",height,".out.tgz")>;
    out = runEP(runep, epconfig, epweather, orientation, height);
  }
}

/*
app (file epout) ALTrunEP ( file runep, file imf, file epw, int orientation, int height)
{
  # runenergyplus CHICAGO-EXAMPLE.imf CHICAGO.epw 
  sh "-c" @strjoin(@runep,"--imf",@imf,"--epw",@epw,"--app",appurl,"--out",@epout,
                          "--params","orientation",orientation,"height",height," ");
}
*/
