#!/bin/bash

# By default with ARG1:100 and SLICESIZE=10000, this script will generate
# 10^6 records.  
ARG1=1
[ ! -z $1 ] && ARG1=$1

FILE="input_$RANDOM.txt"
LOWERLIMIT=0
UPPERLIMIT=10000000 # 10^8
SLICESIZE=10000     # 10^4 records padded to 100B would result in 1MB file
#SLICESIZE=1000     # 10^3  If padded to 100B would result 


#FOLDER="/lustre/beagle/yadunand"
FOLDER="/tmp/yadunandb"
TIMEOUT=100

# clean_folder SLEEP_TIME FOLDER_NAME
clean_folder()
{
    sleep $(($1+1));
    rm $2/* &> /dev/null
}

JOBS_LEVEL=`echo $PWD | grep -o ".*jobs"`
if [ "$?" == "0" ]
then
    mkdir $JOBS_LEVEL/CHIRPING 
    if [ "$?" == "0" ]
    then
	killall -u $USER chirp_server;
	echo "unix:$USER rwlds" >  $FOLDER/acl.conf
	echo "hostname:* rwl"   >> $FOLDER/acl.conf
#	timeout $TIMEOUT chirp_server -A $FOLDER/acl.conf -r $FOLDER 
	timeout $TIMEOUT chirp_server -d -o $FOLDER/$FILE -A $FOLDER/acl.conf -r $FOLDER &

    fi
    FILE=$FOLDER/$FILE;
else
    FILE=$PWD/$FILE
fi

shuf -i $LOWERLIMIT-$UPPERLIMIT -n $(($SLICESIZE*$ARG1)) | awk '{printf "%-99s\n", $0}' > $FILE

HOST=$(hostname -f)
echo "$HOST $FILE"
exit 0