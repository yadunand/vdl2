#!/bin/bash

FILES=$*
SUM=0
COUNT=0

fetch_and_process()
{
    TOKEN=($(cat $1))
    HOST=${TOKEN[0]} 
    FILE=${TOKEN[1]}
    TARGET=$(basename $FILE)
    chirp_get $HOST $TARGET $TARGET.imd
    RES=($(awk '{ sum += $1 } END { print sum,NR }' $TARGET.imd))
    echo "For $HOST:$FILE$ {RES[*]}"
    echo "${RES[0]} ${RES[1]}" > "$2.$RANDOM.imd2"
}

ID=$RANDOM
for file_token in $*
do
    fetch_and_process $file_token $ID &    
done
wait

SUM=$(awk '{ sum += $1 } END { print sum }' $ID*imd2)
COUNT=$(awk '{ sum += $2 } END { print sum }' $ID*imd2)

echo "SUM  : $SUM"
echo "COUNT: $COUNT"
exit 0
