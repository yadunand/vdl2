#!/bin/bash

FILES=$*
SUM=0
COUNT=0

cat $* > RESULT_FILE
# Find the unique sites in the RESULT_FILE to run a combiner there.
#SITES=($(sort -u -t' ' -k1,1 RESULT_FILE))
cat <<'EOF' > recombiner.sh
#!/bin/bash
#SITE=$1; shift 1
echo "Recombiner on $HOSTNAME"
FILES=$*
SUM=0;COUNT=0;
for file in ${FILES[*]}; do
    [ ! -f $file ] && echo "$file is missing" >> $HOSTNAME.parrot_run.log
    RES=($(awk '{ sum += $1 } END { print sum,NR }' $file))
    SUM=$(($SUM+${RES[0]}))
    COUNT=$(($COUNT+${RES[1]}))
done;
echo -e "$SUM $COUNT" > $HOSTNAME.imd
EOF
chmod 777 recombiner.sh

STAGE2="";
SITES=($(awk '{ print $1 }' RESULT_FILE  | sort -u))
for SITE in ${SITES[*]}
do
    echo $SITE
    FILES=($(grep $SITE RESULT_FILE | awk '{ print $2 }'))
    CHIRPDIR=$(dirname ${FILES[0]})
    chirp_put ./recombiner.sh $SITE recombiner.sh   
# We will probably abandon parrot_run
#parrot_run /chirp/$SITES/recombiner.sh ${FILES[*]}    
#    ssh $SITE "cd $CHIRPDIR; ./recombiner.sh ${FILES[*]}"
    ssh $SITE "cd /sandbox/$USER; ./recombiner.sh ${FILES[*]}"
    chirp_get $SITE $SITE.imd $SITE.imd.txt
    cat $SITE.imd.txt
    STAGE2="$STAGE2 $SITE.imd.txt"
done;

cat ${STAGE2[*]} > TEMP
SUM=($(awk '{ sum += $1 } END { print sum }' TEMP))
COUNT=($(awk '{ sum += $2 } END { print sum }' TEMP))
echo "Final results (on $HOSTNAME):"
echo "$SUM $COUNT"

exit 0;

for file_token in $*
do
    TOKEN=($(cat $file_token))
    HOST=${TOKEN[0]} 
    FILE=${TOKEN[1]}
    TARGET=$(basename $FILE)
    parrot_run /chirp/$HOST/FILE
    chirp_get $HOST $TARGET $TARGET.imd
    # TODO Try parrot_cp instead
    RES=($(awk '{ sum += $1 } END { print sum,NR }' $TARGET.imd))


    SUM=$(($SUM+${RES[0]}))
    COUNT=$(($COUNT+${RES[1]}))
done

echo "SUM  : $SUM"
echo "COUNT: $COUNT"


exit 0

