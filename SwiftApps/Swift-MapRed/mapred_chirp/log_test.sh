#!/bin/bash
clear;
#swift -tc.file tc.data -sites.file sites.xml motoko.swift

#echo "Running run_catsn.swift"
#swift -tc.file tc.data -sites.file sites.xml run_catsn.swift
export GLOBUS_HOSTNAME="swift.rcc.uchicago.edu"

if [ "$1" != "" ]
then
    SITE="$1"
else
    SITE="beagle"
fi

#Call as check_error $? <ERR_CODE> <Error message>
check_error()
{
    if [ $1 == $2 ]
    then
	echo $3
    else
	echo "Run completed with code:$1"
    fi
}


DATA=1
LOOP=1
TIMEOUT=600
time {
    dd if=/dev/zero of=test.img bs=1024 count=10 seek=$((1024*DATA))
    echo -e "Running remote_driver.swift with $DATA MB X $LOOP Invocation(s) \n"
    timeout $TIMEOUT swift -tc.file tc.data -config cf -sites.file beagle_logging.xml remote_driver.swift -loops=$((LOOP-1))
    check_error $? 124 "Run terminated by timeout of $((TIMEOUT/60)) minute(s)" 
}

killall -u yadunand java -9