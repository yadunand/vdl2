#!/bin/bash

FILES=$*
SUM=0
COUNT=0

for file_token in $*
do
    TOKEN=($(cat $file_token))
    HOST=${TOKEN[0]} 
    FILE=${TOKEN[1]}
    TARGET=$(basename $FILE)
    chirp_get $HOST $TARGET $TARGET.imd    
    RES=($(awk '{ sum += $1 } END { print sum,NR }' $TARGET.imd))
    echo "$TARGET.imd"
    SUM=$(($SUM+${RES[0]}))
    COUNT=$(($COUNT+${RES[1]}))
done
echo "SUM  : $SUM"
echo "COUNT: $COUNT"
hostname -f
exit 0