
swift -tc.file tc.data -sites.file midway.xml -config cf RunpSIMS.swift \
-model=dssat45 \
-campaign=/scratch/midway/maheshwari/psims/campaigns/pdssat/ggcmi.maize \
-soils=/scratch/midway/maheshwari/psims/data/soils/hwsd200.wrld.30min \
-weather=/scratch/midway/maheshwari/psims/data/clim/agmerra \
-refdata=/scratch/midway/maheshwari/psims/campaigns/pdssat/common.isimip \
-bintransfer=/scratch/midway/maheshwari/psims/bin/DSCSM045.EXE,/scratch/midway/maheshwari/psims/tapps/pdssat/psims2WTH.py,/scratch/midway/maheshwari/psims/tapps/pdssat/OUT2psims.py,/scratch/midway/maheshwari/psims/bin/camp2json.py,/scratch/midway/maheshwari/psims/tapps/pdssat/jsons2dssat.py \
-tappwth=psims2WTH.py:-o:GENERIC1.WTH:-v:tasmin,tasmax,rsdsr,wind \
-tappcamp=camp2json.py:-c:Campaign.nc4:-e:exp_template.json:-o:experiment.json \
-tappinp=jsons2dssat.py:-x:X1234567.MZX:-s:soil.json:-e:experiment.json:-S:SOIL.SOL \
-executable=DSCSM045.EXE:A:X1234567.MZX \
-outtypes=.WTH,.MZX,.SOL,.OUT,.json \
-postprocess=./OUT2psims.py:-i:Summary.OUT:-v:PDAT,ADAT,MDAT,CWAM,HWAM,IRCM,PRCM,ETCM:--delta:30:-s:4:-y:31:--ref_year:1980 \
-NCconcat=./bin/append.sh:PDAT,ADAT,MDAT,CWAM,HWAM,IRCM,PRCM,ETCM:PlantDate,AnthesisDate,MaturityDate,TotalBiomass,Yield,IrrigationApplied,GrowingSeasonPrecip,GrowingSeasonET:DOY,DOY,DOY,kg/ha,kg/ha,mm,mm,mm:720:360:30:31:4:1980:./parts:out.psims.dssat45.agmerra.maize.nc4

