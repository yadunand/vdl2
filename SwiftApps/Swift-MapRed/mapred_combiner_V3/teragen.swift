//import "kway";
type file;
type script;

// type fileptr will be a regular file to swift, but the contents of which, will be
// interpreted as globally addressable paths to one or more files.
// The current format of a fileptr is:
//   Node0 /path/to/file0 /path/to/file1 /path/to/file2 ...
//   ...
//   Noden /path/to/file0 ...
// Evidently, there is room for improvement and extension.
type fileptr;

app (fileptr out, file err) map_data (script run, int recsize)
{
    bash @run recsize stdout=@out stderr=@err;
}

app (fileptr out, file err) comb_data (script comb, string node_string)
{
    bash @comb node_string stdout=@out stderr=@err;
}

app (fileptr out, file err) reduce_data (script comb, fileptr array[])
{
    bash @comb @array stdout=@out stderr=@err;
}

app (file out, file err) get_uniq_nodes (script uniq, fileptr filepointers[])
{
    bash @uniq @filepointers stdout=@out stderr=@err;
}

app (file out, file err) dereference (script deref, fileptr ptr)
{
    bash @deref @ptr stdout=@out stderr=@err;
}

(fileptr final) kway (script reducer, int K_way, fileptr array[])
{
  file next_level[];
  int len   = @length(array);
  fileptr bucket[][];

  foreach index, i in [0:len-1]
  {
    int bucketid = index %/ K_way;
    bucket[bucketid][index %% K_way] = array[index];
    tracef(" %s going to bucket:%i \n", @array[index], bucketid);
  }

  //  fileptr bucket_out[] <simple_mapper; prefix="bucket", suffix=".out">;
  //  file    bucket_err[] <simple_mapper; prefix="bucket", suffix=".err">;
  fileptr bucket_out[];
  file    bucket_err[];

  tracef(" Length of array : %i  len-1 mod K_way: %i \n", len, (len-1)%/K_way);
  foreach id in [0:((len-1)%/K_way)]
  {
    (bucket_out[id], bucket_err[id])= reduce_data (reducer, bucket[id]);
    tracef("bucket[%i] contains %i items \n", id, @length(bucket[id]));
  }
  file tmp_err;
  if ( @length(bucket_out) > K_way ){
    final = kway(reducer, K_way, bucket_out);
  }else{
    (final, tmp_err) = reduce_data (reducer, bucket_out);
  }
}

int files = @toInt(@arg("files","0"));
int fsize = @toInt(@arg("filesize","10")); // This would make 10^4 records per file

script mapper   <"mapper.sh">;
script uniq     <"node_uniq.sh">;
script combiner <"combiner.sh">;
script reducer  <"reducer.sh">;
script deref    <"dereference.sh">;

fileptr tgen_out[] <simple_mapper; prefix="tgen", suffix=".out">;
file    tgen_err[] <simple_mapper; prefix="tgen", suffix=".err">;
// Map stage. File pointers tgen_out[]
foreach item,i in [0:files-1] {
    (tgen_out[i], tgen_err[i]) = map_data(mapper, fsize);
}

// Get nodes and intermediate file local to node
file    node_out <simple_mapper; prefix="node", suffix=".out">;
file    node_err <simple_mapper; prefix="node", suffix=".err">;
(node_out, node_err) = get_uniq_nodes (uniq, tgen_out);
string nodes[] = readData(node_out);

// Invoke the remote_combiner on each node involved in map stage stage
fileptr comb_out[] <simple_mapper; prefix="comb", suffix=".out">;
file    comb_err[] <simple_mapper; prefix="comb", suffix=".err">;
foreach node_info,i in nodes {
  (comb_out[i], comb_err[i]) = comb_data(combiner, node_info);
}

// This is the final reduce stage, here results from all nodes are
// brought to a single node for reduction
// Tree based implementation could be used.
fileptr p_final <"p_final.out">;
file    p_errs  <"p_final.err">;
//(p_final, p_errs) = reduce_data(reducer, comb_out);
p_final = kway(reducer, 4, comb_out);


file f_out <"final_result">;
file f_err <"final_errors">;
(f_out, f_err) = dereference (deref, p_final);
// This version of reduce_data uses the results from map stage directly
//(final, errs) = reduce_data(reducer, tgen_out);
