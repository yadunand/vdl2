#!/bin/bash
# The derefernce script takes one fileptr as input
# and dumps the content of the file to stdout.
TOKEN=($(cat $1))
REMOTE=${TOKEN[0]}
FILE=${TOKEN[1]}
TARGETFILE=$(basename $FILE)
REPODIR=$(dirname $FILE)
echo "$REMOTE $HOSTNAME" 1>&2
if [ "$REMOTE" == "$(hostname -f)" ]
then # Move to an identifiable file if local                           
    cat $FILE
else # Fetch to an identifiable file if remote                         
    echo "chirp_get $REMOTE $TARGETFILE $FILE" 1>&2
    chirp_get $REMOTE $TARGETFILE $FILE
    cat $FILE
    rm  $FILE
fi


