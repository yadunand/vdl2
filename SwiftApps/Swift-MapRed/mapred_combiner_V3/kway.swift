type file;
type script;
type fileptr;

/*
app (file out, file err) comb_data (script comb, file array[])
{
    bash @comb @array stdout=@out stderr=@err;
}
*/
app (fileptr out, file err) kway_reduce (script comb, fileptr array[])
{
  bash @comb @array stdout=@out stderr=@err;
}

// This stage is done with file pointers so as to keep
// data movement to a minimum. In the test-case being
// used this is going to give a performance hit as the 
// actual data file is as large as the fileptr and a chirp_get
// is used to fetch the file.
(fileptr final) kway (script reducer, int K_way, fileptr array[])
{
  file next_level[];
  int len	= @length(array);
  fileptr bucket[][];

  foreach index, i in [0:len-1]
  {
    int bucketid = index %/ K_way;
    bucket[bucketid][index %% K_way] = array[index];
		tracef(" %s going to bucket:%i \n", @array[index], bucketid);
  }
  
  fileptr bucket_out[] <simple_mapper; prefix="bucket", suffix=".out">;
  file    bucket_err[] <simple_mapper; prefix="bucket", suffix=".err">;
  
  foreach id in [0:(len%/K_way)]
  {
    (bucket_out[id], bucket_err[id])= kway_reduce(reducer, bucket[id]);
  }
  file tmp_err <"tmp_err">;
  if ( @length(bucket_out) > K_way ){
    final = kway(reducer, K_way, bucket_out);
  }else{
    (final, tmp_err) = kway_reduce(reducer, bucket_out);
  }
}


(fileptr final) kway_2 (script reducer, int K_way, fileptr array[])
{

  int     len  = @length(array);
  fileptr bucket[][];
  file    next_level[];
  file    tmp_err <"tmp_err">;

  foreach index, i in [0:len-1]
  {
    int bucketid = index %/ K_way;
    bucket[bucketid][index %% K_way] = array[index];
    tracef(" %s going to bucket:%i \n", @array[index], bucketid);
  }
  
  fileptr bucket_out[] <simple_mapper; prefix="bucket", suffix=".out">;
  file    bucket_err[] <simple_mapper; prefix="bucket", suffix=".err">;
  
  foreach id in [0:(len%/K_way)]
  {
    (bucket_out[id], bucket_err[id])= kway_reduce(reducer, bucket[id]);
  }
  
  if ( @length(bucket_out) > K_way ){
    final = kway(reducer, K_way, bucket_out);
  }else{
    (final, tmp_err) = kway_reduce(reducer, bucket_out);
  }
}

fileptr inputs[] <filesys_mapper; prefix="tgen", suffix=".out">;
fileptr final <"final">;

foreach i,ind in inputs{
        tracef("File[%i] : %s\n", ind,@filename(i));
}

script dummy <"dummy.sh">;
final = kway(dummy, 5, inputs);

