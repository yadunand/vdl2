#!/bin/bash

ARGS=($*)
SUM=0
COUNT=0

# When strings in format node files... are passed to combiner
# 1. Run a local_combiner on the remote node 
# 2. Return a file pointer
NODE=${ARGS[0]}
FILES=(${ARGS[*]:1})    
REMOTE_PORT=29900
HOSTNAME=$(hostname -f)

############## LOCAL COMBINER CODE ################### 
cat << EOF   > $NODE.sh
#!/bin/bash 
ID=$RANDOM
FILES=(${FILES[*]})
EOF
cat <<'EOF' >> $NODE.sh
#USER_REDUCE
echo -e "$(hostname -f) $(reduce ${FILES[*]})"
EOF
chmod a+x $NODE.sh
############# END LOCAL COMBINER #####################
cp /lustre/beagle/yadunandb/bin/exec_client .
echo "Combiner ${ARGS[*]}" 1>&2
echo "Combiner [./exec_client $NODE $NODE.sh]" 1>&2
cat $NODE.sh 1>&2
./exec_client $NODE $NODE.sh
exit 0     
