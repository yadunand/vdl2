reduce(){
    FILES=($*)
    DIRNAME=$(dirname ${FILES[0]})
    RESULT=$DIRNAME/$(hostname -f).$RANDOM.imd
    awk '{ sum += $1; count += $2 } END { print sum,count }' ${FILES[*]} > $RESULT
    echo $RESULT
    rm -rf ${FILES[*]} &> /dev/null
}
