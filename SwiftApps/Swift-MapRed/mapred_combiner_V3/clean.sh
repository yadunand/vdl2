#!/bin/bash

rm -rf *log *d                     &> /dev/null
rm -rf *swiftx *kml                &> /dev/null
rm -rf *~                          &> /dev/null
rm *{out,err,imd2}                 &> /dev/null
rm final_errors final_result       &> /dev/null
rm exec_client exec_server         &> /dev/null
