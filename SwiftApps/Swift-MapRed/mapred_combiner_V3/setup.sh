#!/bin/bash

if [ ! -f "map_core.sh" ]
then
    echo "map_core.sh missing"
    exit 0;
fi

if [ ! -f "mapper_template.sh" ];
then 
    echo "mapper_template missing"
    exit 0;
else
    sed -e "/\#USER_MAP/r map_core.sh" mapper_template.sh \
        > mapper.sh
    chmod 777 mapper.sh
fi;



if [ ! -f "reduce_core.sh" ]
then
    echo "reduce_core.sh missing"
    exit 0;
fi;

if [ ! -f "reducer_template.sh" ];
then 
    echo "reducer_template missing"
    exit 0;
else
    sed -e "/\#USER_REDUCE/r reduce_core.sh" reducer_template.sh \
        > reducer.sh
    chmod 777 reducer.sh
fi;

if [ ! -f "combiner_template.sh" ];
then 
    echo "combiner_template missing"
    exit 0
else
    sed -e "/\#USER_REDUCE/r reduce_core.sh" combiner_template.sh \
        > combiner.sh
    chmod 777 combiner.sh
fi;






