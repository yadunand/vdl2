#!/bin/bash

# By default with ARG1:100 and SLICESIZE=10000, this script will generate
# 10^6 records.  
ARG1=1
[ ! -z $1 ] && ARG1=$1

FILE="input_$RANDOM.txt"
HOSTNAME=$(hostname -f)

FOLDER="/dev/shm"
TIMEOUT=1200
NC_LISTEN_PORT=29900

# Start services on Node
JOBS_LEVEL=`echo $PWD | grep -o ".*jobs"`
if [ "$?" == "0" ]
then
    mkdir $JOBS_LEVEL/CHIRPING 
    if [ "$?" == "0" ]
    then
	    killall -u $USER chirp_server;
	    echo "unix:$USER rwlds" >  $FOLDER/acl.conf
	    echo "hostname:* rwl"   >> $FOLDER/acl.conf
	    timeout $TIMEOUT chirp_server -A $FOLDER/acl.conf -r $FOLDER &
        timeout $TIMEOUT /lustre/beagle/yadunandb/bin/exec_server &> \
            /lustre/beagle/yadunandb/Swift-MapRed/mapred_combiner_V3/exec_server_log-$HOSTNAME.log &
    fi
    FILE=$FOLDER/$FILE;
else
    FILE=$PWD/$FILE
fi

#USER_MAP

mapper $ARG1 > $FILE
echo "$HOSTNAME $FILE"
ps -u $USER 1>&2
exit 0;