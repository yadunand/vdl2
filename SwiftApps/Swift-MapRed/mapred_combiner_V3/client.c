/*
** client.c -- a stream socket client demo
* Thanks to Brian "Beej Jorgensen" Hall
* Hacked to work as a remote exec server by Yadu
* <yadunandb@ci.uchicago.edu>
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string.h>

#include <arpa/inet.h>

#define PORT "3490" // the port client will be connecting to 

#define MAXDATASIZE 2048 // max number of bytes we can get at once 

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


int main(int argc, char *argv[])
{
    int sockfd, new_fd, numbytes;  
    int filelen;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    char s[INET6_ADDRSTRLEN];    
    FILE *f_in;
    int file_size;
    char *buffer;
    char *reply_buf;
    int con_res;
    if (argc != 3) {
        fprintf(stderr,"usage: client hostname remote_script\n");
        exit(1);
    }
    
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;


    // Leaving PORT as NULL   
    if ((rv = getaddrinfo(argv[1], PORT, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }
    
    // loop through all the results and connect to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                             p->ai_protocol)) == -1) {
            perror("client: socket");
            continue;
        }

        if ( (con_res = connect(sockfd, p->ai_addr, p->ai_addrlen)) == -1) {
            close(sockfd);
            fprintf(stderr,"Connect failed with %d:%s\n", con_res, strerror(errno));
            perror("client: connect");
            continue;
        }

        break;
    }
    
    if (p == NULL) {
        fprintf(stderr, "client: failed to connect\n");
        return 2;
    }

    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
              s, sizeof s);
    fprintf(stderr,"client: connecting to %s\n", s);

    freeaddrinfo(servinfo); // all done with this structure

    //Send message to server
    fprintf(stderr,"Client sending contents of file %s to server \n",argv[2]);

    // Read file contents into a buffer;
    if ( (f_in = fopen(argv[2],"r")) == NULL ){
      fprintf(stderr, "Opening[%s] failed: %s\n", argv[2], strerror(errno));
      exit(errno);
    };

    fseek(f_in, 0, SEEK_END);
    file_size = ftell(f_in); // Find length
    fprintf(stderr, "File size: %d\n", file_size);
    fseek(f_in, 0, SEEK_SET);

    buffer = (char *) malloc ((sizeof (char)) * (file_size+2));
    fread(buffer, file_size, 1, f_in);
    buffer[file_size] = '\0';
    fclose(f_in);

    fprintf(stderr, "Content of file : \n%s\n", buffer);

    if (send(sockfd, buffer, file_size, 0) == -1)
      perror("send");
    free(buffer);

    //Receive message back from server
    reply_buf = (char *)malloc(MAXDATASIZE);
    fprintf(stderr, "Client receiving message from server \n");
    if ((numbytes = recv(sockfd, reply_buf, MAXDATASIZE-1, 0)) == -1) {
        perror("recv");
        exit(1);
    }
    //printf("Numbytes recv'd : %d\n", numbytes); 
    reply_buf[numbytes] = '\0';
    printf("%s",reply_buf);
    free(reply_buf);
    close(sockfd);

    return 0;
}
