#!/bin/bash
# The reducer is expecting to receive filenames as args
# Each file would contains strings in the format HOSTNAME FILEPATH

# The files passed here are filepointers which need to be resolved
# before they can be processed by the core reduce function.
FILES=($*)

HOSTNAME=$(hostname -f)
ID=$RANDOM

# This reduce function must conform to the following specification
# Takes 1 or more files as arguments
# Returns the absolute path of the local-file containing results.
#USER_REDUCE
reduce(){
    FILES=($*)
    DIRNAME=$(dirname ${FILES[0]})
    RESULT=$DIRNAME/$(hostname -f).$RANDOM.imd
    awk '{ sum += $1; count += $2 } END { print sum,count }' ${FILES[*]} > $RESULT
    echo $RESULT
    rm -rf ${FILES[*]} &> /dev/null
}

fetch()
{
    TOKEN=($(cat $1))
    ID=$2
    REMOTE=${TOKEN[0]}
    FILE=${TOKEN[1]}
    TARGETFILE=$(basename $FILE)
    REPODIR=$(dirname $FILE)
    echo "$REMOTE $HOSTNAME" 1>&2
    if [ "$REMOTE" == "$HOSTNAME" ]
    then # Move to an identifiable file if local
        echo "mv $FILE $REPODIR/$ID.$RANDOM.imd2" 1>&2
        mv $FILE $REPODIR/$ID.$RANDOM.imd2
    else # Fetch to an identifiable file if remote
        echo "chirp_get $REMOTE $TARGETFILE $REPODIR/$ID.$RANDOM.imd2" 1>&2
        chirp_get $REMOTE $TARGETFILE $REPODIR/$ID.$RANDOM.imd2
    fi


}

for file_token in ${FILES[*]}; 
do  
    fetch $file_token $ID &    
done
wait

TOKEN=($(cat $1))
FILE=${TOKEN[1]}
CHIRPDIR=$(dirname $FILE)
echo "$HOSTNAME $CHIRPDIR $CHIRPDIR/$ID*imd2 " 1>&2
#cat $(reduce $CHIRPDIR/$ID*imd2)
RESULT_FILE=$(reduce $CHIRPDIR/$ID*imd2)
echo "$HOSTNAME $RESULT_FILE"
echo "Content of $RESULT_FILE : $(cat $RESULT_FILE)" 1>&2
rm -rf $CHIRPDIR/$ID*imd2 &> /dev/null &
exit 0