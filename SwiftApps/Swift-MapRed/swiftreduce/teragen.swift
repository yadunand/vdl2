import files;
import io;
import random;
import string;
import sys;

app (file out, file err) gen_data (file run, int recsize){
    "/bin/bash" run recsize @stdout=out @stderr=err
}

app (file out, file err) comb_data (file comb, file array[]){
    "/bin/bash" comb array @stdout=out @stderr=err
}

/*
file tgen_out[] <simple_mapper; prefix="tgen", suffix=".out">;
file tgen_err[] <simple_mapper; prefix="tgen", suffix=".err">;
string dir = @arg("dir", "./");
*/
main{
file wrapper = input_file("teragen_wrap.sh");
file tgen_out[];
file tgen_err[];

int loop = 7;
int fsize = 10;

foreach item,i in [0:loop-1] {
    
    file out <sprintf("tgenout-%i.dat", i)>;
    file err <sprintf("tgenerr-%i.dat", i)>;

	(out, err) = gen_data(wrapper, fsize);
    tgen_out[i]=out;
    tgen_err[i]=err;

}

file combine =input_file("combiner.sh");
file final <"final_result">;
file errs <"err_file">;

(final, errs) = comb_data(combine, tgen_out);

}

