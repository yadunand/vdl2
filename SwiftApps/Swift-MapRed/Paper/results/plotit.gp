#To be run as a gnuplot script as follows:

# $ gnuplot plotit.gp

set terminal pngcairo enhanced font 'Verdana,12'

set xtics ("10" 0, "50" 1, "100" 2, "200" 3, "500" 4, "750" 5, "1000" 6)
set output "hadoop-wc-mcs.png"
set xlabel "Data size in MB"
set ylabel "time in sec"
set grid
set datafile separator "|"
plot "hadoop-wc-mcs.txt" u 2 w lp lw 3 lc rgb"green" t "localdisk", "hadoop-wc-mcs.txt" u 2:3 w errorbars lw 3 lc rgb"green" notitle, \
     "hadoop-wc-mcs-ramdisk.txt" u 2 w lp lw 3 lc rgb"blue" t "ramdisk", "hadoop-wc-mcs-ramdisk.txt" u 2:3 w errorbars lw 3 lc rgb"blue" notitle 

#set output "hadoop-wc-mcs-ramdisk.png"

#set xtics ("10" 0, "50" 1, "100" 2, "200" 3, "500" 4, "750" 5, "1000" 6)
#
#set output "p1.png"
#plot "symmetryp1.txt" u 2 w lp lw 3 lc rgb"green" t "T->S", "symmetryp1.txt" u 2:3 w errorbars lw 3 lc rgb"green" notitle,  "symmetryp1.txt" u 4 w lp lw 3 lc rgb"red" t "S->T", "symmetryp1.txt" u 4:5 w errorbars lw 3 lc rgb"red" notitle
#
#
#set output "p2.png"
#plot "symmetryp2.txt" u 2 w lp lw 3 lc rgb"green" t "T->S", "symmetryp2.txt" u 2:3 w errorbars lw 3 lc rgb"green" notitle,  "symmetryp2.txt" u 4 w lp lw 3 lc rgb"red" t "S->T", "symmetryp2.txt" u 4:5 w errorbars lw 3 lc rgb"red" notitle
#
#set output "p4.png"
#plot "symmetryp4.txt" u 2 w lp lw 3 lc rgb"green" t "T->S", "symmetryp4.txt" u 2:3 w errorbars lw 3 lc rgb"green" notitle,  "symmetryp4.txt" u 4 w lp lw 3 lc rgb"red" t "S->T", "symmetryp4.txt" u 4:5 w errorbars lw 3 lc rgb"red" notitle
#
#set output "p8.png"
#plot "symmetryp8.txt" u 2 w lp lw 3 lc rgb"green" t "T->S", "symmetryp8.txt" u 2:3 w errorbars lw 3 lc rgb"green" notitle,  "symmetryp8.txt" u 4 w lp lw 3 lc rgb"red" t "S->T", "symmetryp8.txt" u 4:5 w errorbars lw 3 lc rgb"red" notitle
#
#set output "p12.png"
#plot "symmetryp12.txt" u 2 w lp lw 3 lc rgb"green" t "T->S", "symmetryp12.txt" u 2:3 w errorbars lw 3 lc rgb"green" notitle,  "symmetryp12.txt" u 4 w lp lw 3 lc rgb"red" t "S->T", "symmetryp12.txt" u 4:5 w errorbars lw 3 lc rgb"red" notitle
#
#
#set output "p16.png"
#plot "symmetryp16.txt" u 2 w lp lw 3 lc rgb"green" t "T->S", "symmetryp16.txt" u 2:3 w errorbars lw 3 lc rgb"green" notitle,  "symmetryp16.txt" u 4 w lp lw 3 lc rgb"red" t "S->T", "symmetryp16.txt" u 4:5 w errorbars lw 3 lc rgb"red" notitle
#
#
#set output "p32.png"
#plot "symmetryp32.txt" u 2 w lp lw 3 lc rgb"green" t "T->S", "symmetryp32.txt" u 2:3 w errorbars lw 3 lc rgb"green" notitle,  "symmetryp32.txt" u 4 w lp lw 3 lc rgb"red" t "S->T", "symmetryp32.txt" u 4:5 w errorbars lw 3 lc rgb"red" notitle
#
