set terminal pngcairo enhanced font 'Verdana,12'
set surface 
#set contour surface

set view 60, 30, 1, 1
set clabel '%8.2f'
set key right
set title "Graph Title"
set xlabel "X Axis Label"
set ylabel "Y Axis Label"
set zlabel "Z Axis Label"
set output "3dplot.png"
#splot "3d.dat" using 2:1:3 notitle
#splot "3d.dat" with pm3d notitle
splot "3d.dat" using 2:1:3 with lines notitle
