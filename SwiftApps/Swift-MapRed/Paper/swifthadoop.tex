\documentclass[conference]{IEEEtran}
%
\ifCLASSINFOpdf
  \usepackage[pdftex]{graphicx}
  \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  \usepackage[dvips]{graphicx}
\fi

\usepackage{comment}
\usepackage{listings}
\usepackage{pgf}
\usepackage{amsmath}
\usepackage{subfig}
\usepackage{tikz}
\usetikzlibrary{arrows,automata}

\lstset{
  basicstyle = \ttfamily\scriptsize\color{black},%\bfseries
  keywordstyle = \color{red},%\bfseries
  keywordstyle = [2]\ttfamily\color{black},
  frame = single,
  captionpos=b,
  frame=single,
  tabsize=2,
  xleftmargin=15pt,
  numbers=left
}

\definecolor{darkgreen}{rgb}{0,0.7,0}
\newif\ifdraft
\drafttrue
\ifdraft
 \newcommand{\katznote}[1]{ {\textcolor{blue}    { ***Dan:      #1 }}}
 \newcommand{\zhaonote}[1]{{\textcolor{darkgreen}    { ***Zhao:      #1 }}}
 \newcommand{\ketanote}[1]{{\textcolor{orange}    { ***Ketan:      #1 }}}
 \newcommand{\note}[1]{ {\textcolor{red}    {\bf #1 }}}
\else
 \newcommand{\katznote}[1]{}
 \newcommand{\zhaonote}[1]{}
 \newcommand{\ketanote}[1]{}
 \newcommand{\note}[1]{}
\fi
\hyphenation{op-tical net-works semi-conduc-tor}

\begin{document}
\title{Coupling Swift and Hadoop}

\author{\IEEEauthorblockN{Auth1\IEEEauthorrefmark{1},
Auth2\IEEEauthorrefmark{1},
Auth3\IEEEauthorrefmark{3},
Authn\IEEEauthorrefmark{1}\IEEEauthorrefmark{3},
\IEEEauthorblockA{
\IEEEauthorrefmark{1}MCS Division, Argonne National Laboratory, Argonne, IL 60439}
\IEEEauthorrefmark{2}Department of Electrical and Computer Engineering,  University of British Columbia\\
\IEEEauthorrefmark{3}Computation Institute,  University of Chicago \& Argonne National Laboratory}
}

\maketitle
\begin{abstract}
The MapReduce programming model, and its common implementation, Hadoop, have
gained significant popularity in science, industry, and the parallel and
distributed systems research community. At the same time, another model, the
workflow paradigm, has grown and matured for scientific applications on
distributed systems. Both models have strengths as generic systems for
productive scientific computation. These strengths could complement each other
if applied together.

In this work, we compare and contrast features and benefits of Hadoop with the
Swift parallel scripting framework. The goal of this comparison is to determine
strengths and weaknesses of MapReduce programming model for workflow oriented
computations.

Swift and Hadoop have distinct design goals. Swift is designed to address
parallel and distributed orchestration and computation of coarse-grained tasks,
using files as the communication media. Hadoop is designed to solve problems
involving computations with finer granularity and a uniform execution pattern.

In this work we: 1) Analyze the strengths and weaknesses of Hadoop from the
scientific workflow side of fence. We identify suitable classes of applications
for each of Hadoop and Swift, 2) Compare the performance of Swift and Hadoop on
a medium-scale cluster with a representative application from each of the
MapReduce and Workflow paradigm, 3) Dissect technical and logical steps of each
of the runtime environments via a Detailed logs analysis of Swift and Hadoop
(namenode, datanode, tasktracker) logs 4) Define strategies for Swift and
Hadoop integration and coupling 

First running Swift with HDFS as underlying FS. Second, running Hadoop jobs as
Swift apps.

\end{abstract}

\begin{IEEEkeywords}
    swift, hadoop
\end{IEEEkeywords}

\IEEEpeerreviewmaketitle

\section{Introduction}
Both workflow and MapReduce paradigms have grown in capabilities and
applicability to an increasing number of applications. However, often these
paradigms and their implementations are used for parallel processing in
situations where they are sub-optimal and not efficient. The reasons for these
inefficiencies are limited awareness and available options~\cite{fired} or
both. Users are often forced to chose one solution over other to fit since it
is hard to combine both the workflows and MapReduce paradigms in a single
solution.

On one hand, the users have MapReduce implementations such as Hadoop, which
along with its HDFS file system are extremely optimized for a class of
problems, scale and hardware. Whereas on the other hand are workflow systems
such as Swift which are highly expressive to create computational chains of
arbitrary complexity, has lower barriers to prototype and test applications and
are well-suited to a wide class of parallel applications.  While they both help
rid users of many tedious clerical and technical steps involved in the
distributed application execution and management, identifying subproblems on
which each of the system is good at remains crucial for an efficient usage of
each.  Clearly, either of the paradigms bring many advantages to users over
manual and ad hoc computational arrangement. However, if not applied to right
problems they could lead to underutilized resources and inferior performance.

In this paper we identify strengths of MapReduce and workflow paradigms and
apply them together in a novel configuration such that it benefits a wider
application types. We implement our new distributed computing configuration via
the Swift framework. We demonstrate that the resulting system is superior in
both expressibility and performance for large-scale applications.

\section{Background}
\subsection{Hadoop and HDFS}
Hadoop is an application framework which is highly optimized for parallel and
distributed processing of record oriented data. The Hadoop framework consists
of many distinct components that makes up the complete system. These components
can be broadly devided into two parts: 1) The Hadoop execution framework and 2)
The HDFS file system.

Setting up Hadoop on a distributed environment such as a cluster or cloud
requires special setup and configuration. Roles for nodes need to be identified
which dictates the services to run on them. These services comprise of control,
bookkeeping, data storage, and execution coordination.

The execution framework consists of the implementation of the main Hadoop
operations: `map' and `reduce'.  The `map' phase involves preparing operations
on input data in addition to the actual computation. The map phase performs
majority of the actual application logic. The reduce phase performs an indexed
combination of results obtained from the map phase. In addition to combining
the results from map phase, reduce involves sort and shuffle of intermediate
results.  Finally, a `cleanup' task is executed which removes any temporary
files and locks created during run. The Hadoop execution framework consists of
the following components: 

\paragraph{JobTracker} A jobtracker is a service which manages the overall
MapReduce application. It farms out tasks to available nodes. Jobtracker
attempts to run jobs where the data is located. In order to find nodes where
the data is located, it queries the HDFS services.

\paragraph{TaskTracker} A task tracker is a service which manages the
individual tasks. A task tracker runs on each compute node and receives tasks
from the jobtracker. Tasktracker is responsible for managing the MapReduce
processes on a node. 

\begin{figure*}
  \begin{center}
  \includegraphics[width=6in]{fig/hadoopflow}
  \caption{Data flow of Hadoop
  \label{fig:hadooparchi}
  }
\end{center}
\end{figure*}

The Hadoop Distributed File System (HDFS) forms the data management part of
Hadoop system. HDFS is a high-throughput storage system designed principally
for MapReduce style computations. HDFS aggregates the disk space of the nodes
on which it is installed and provides a unified storage view of the aggregated
space. It comes packaged with a suite of CLI tools for basic file operations
via an API commonly implemented in Java.

HDFS is designed for batch processing applications which usually have large
data sets and write-once-read-many-times semantics. The data is stored and
managed in terms of large (several MB) chunks called `blocks'. A block of data
is the unit of management for load-balancing, physical data movement,
replication, and fault tolerance. The architecture of HDFS consists of one
master node called namenode which manages metadata, and several slave nodes
called datanode which store the data blocks. HDFS supports replication with
location awareness which ensures the block replicas are stored on unique
servers as well as location aware scheduling based on the principle that moving
computation is cheaper than moving data. Data movement in HDFS is governed via `streaming' access modes, implying
Hadoop's suitability to record-level manipulation on datasets.

The HDFS file system consists of the following components:
\paragraph{Namenode and SecondaryNamenode}
A namenode is a service that maintains the information about blocks and their
locations across the system. The notion of data location map is implemented and
managed by HDFS in the form of a block-map within the namenode.  The metadata
and files namespace on an HDFS cluster is managed by namenode process. A
`secondary' namenode assists the namenode in performing data consistency and
replication related operations. Counter intuitively, the secondary namenode
does not play the role of backup to namenode thus making namenode service a
single point of failure in the system. The actual data blocks are managed by
the 'datanode` processes running on each compute node of a cluster.

\paragraph{DataNode}
A datanode is a service that runs on each node of a system and manages the data
on that node. The service listens for requests on data blocks read and write
and performs a streaming of blocks to the requester service. 

The main principle of MapReduce computational paradigm with respect to data is
to move the tasks where the data is located. By virtue of quick availability of
data fragments in the form of blocks, Hadoop can rapidly orchestrates tasks on
distributed resources.  Much of the success of the Hadoop model thus can be
attributed to the HDFS~\cite{hdfs-main} implementation. 

With a homogeneous map operation on multiple small fragments of data, Hadoop
benefits from the HDFS's capability of splitting and spreading data across the
operational nodes while still keeping track of them via the key-value indexing.

\subsection{Swift}
Swift is a workflow framework with an expressive high-level language on the
lines of Turing complete workflow languages such as scufl~\cite{turing}. Swift
is a general purpose workflow framework for dataflow oriented computing
designed for scientific computing applications. Ordinary files are the main
inter-task communication mechanism in Swift. Swift script is a functional
programming style language with semantics based on write-once variable built on
the notion of futures. An underlying compiler takes the Swift script as input
and generates an intermediate representation which is fed to an engine which
generates parallel tasks identifying interdependencies for task orchestration. 

Swift lets users link ordinary programs via inputs and outputs. These inputs
and outputs could be in the form of files which are expressed as program
variables within Swift scripts. 

\begin{figure*}
  \begin{center}
  \includegraphics[width=4in]{fig/swiftflow}
  \caption{Dataflow of Swift
  \label{fig:swiftarchi}
  }
\end{center}
\end{figure*}

\section{Swift versus Hadoop}
In this section we discuss similarities and differences between the Swift and
Hadoop systems. Table~\ref{tbl:storage} summarizes the key features and
characteristics of Hadoop and Swift systems.

Swift is designed for scientific applications running on large-scale parallel
systems such as clusters, clouds and supercomputers.

Swift scripts are more general than Hadoop jobs which require comparatively
less setup to test and prototype on small systems.  Hadoop's low level
programming interface on the other hand presents a usability challenge as new
problems might not be readily adaptable.  A simple Hadoop program on the other
hand requires writing multiple java classes and a reasonable expertise to
understand the java API and runtimes.

In terms of system design, Hadoop is designed for a particular style of
data-intensive workload that is substantially different to what Swift
is designed or optimized for.

Hadoop comes packaged with HDFS, while Swift works with the native shared or
distributed file system. Hadoop is well-suited to environments with dedicated,
single-application clusters while Swift is well-interfaced to time-shared,
multi-user clusters. Hadoop has a rigid model of expression with respect to
application patterns while Swift can express diverse application patterns via
workflows with little invasion on application invocation methods.

Both Swift and Hadoop provide fault tolerance via retry mechanisms. Hadoop's
task tracker is responsible for retrying failed map or reduce instances while
the namenode manages retry of block reads or writes. In Swift, a retry counter
manages the task reschedule if it fails. Fault recovery, however is not
provided inherently by the Hadoop framework. This means that a crashed
application can not be restarted from the point it crashed. Swift provides a
`resume' feature which enables a failed application to restart a run from the
point of failure.

\begin{table*}[ht!]
\begin{center}
\caption{Swift versus Hadoop: Features and Characteristics}
% use packages: array
\begin{tabular}{|c|l|l|}
\hline
\textbf{Feature} & \textbf{Swift} & \textbf{Hadoop}\\
\hline
\textbf{Design Goals} & Scientific Applications & Distributed data processing\\
\textbf{Model of Computation} & Parallel Manay-Task Workflow & MapReduce\\
\textbf{Suitability to App Classes} &Multistage, loosely coupled computation & two-stage, tightly coupled computations\\
\textbf{Usability} & Easier to setup & Harder to setup \\
\textbf{Expressivity} & Turing complete, High-level & Low-level, rigid \\
\textbf{Performance} & & \\
\textbf{Fault Tolerance} & Retry, resume at point of crash & retry, no resume feature \\
\textbf{Data Storage} & Relies of FS & HDFS \\
\textbf{Data Movement} & TCP sockets, file-based & TCP stream-based \\
\textbf{External Systems Interface} & well-interfaced to clusters, supercomputers, clouds& well-interfaced to clouds \\
\textbf{Semantics} & Parallel and Flexible  & Parallel but rigid\\
\hline
\end{tabular}
\label{tbl:storage}
\end{center}
\end{table*}


\section{Experiment Setup}

\begin{figure*}
  \begin{center}
  \includegraphics[width=4in]{fig/swiftreduce.png}
  \caption{Setup of SwiftReduce
  \label{fig:swiftreduce}
  }
\end{center}
\end{figure*}

\begin{figure*}
  \begin{center}
  \includegraphics[width=4in]{fig/combiner.png}
  \caption{Setup of Combiner
  \label{fig:swiftcombiner}
  }
\end{center}
\end{figure*}

%\subsection{Cluster}

%\subsection{Cloud}

%\section{Data Locality and Task Granularity}
%Introduce, why are the two crucial in distributed computing. 

%Refine the notion of data granularity, what is its origin, staging,
%out-of-band solutions, possible hacks such as pull and push.

%Refine the notion of task granularity.
%What are the trade-offs between task granularity and data locality. What is the origin and history?

%Implicate that the task granularity in MapReduce style computing directly
%affects the amount of data that is required to be present at the compute node.

%What are the implications of the above two on Hadoop style of computation. How
%a closely integrated system such as HDFS benefits and at the same time limits
%Hadoop. How a loosely integrated data system benefits and limits Swift. These
%benefits and limitations results in an emergence of complementary qualities.
%Can we take advantage of both these features implementation on each of the
%systems to the benefits of the applications.

\section{Analysis of Swift and Hadoop Runtimes}
%An introductory paragraph on why log analysis is an effective techniques in
%dissecting process, identifying issues, and understanding such systems as Swift
%and Hadoop possibly followed by a small discussion on importance of log
%messages in debugging and understanding systems.  A distilled chronological log
%of Hadoop run.
Parallel workflow engines are fully task-asynchronous which affords them a
flexibility of composing tasks in optimal ways with respect to synchronization
timings and events. Systems such as Hadoop today have no such advanced
synchronization in place between the map and reduce stages. This means the
reduce stage will start only after the map stage has completed fully.
%TODO expand this with possibilities for Swift.

\section{Illustrative Solutions}
%Two Hadoopy solutions, Swift and Hadoop implementations of each.

\subsection{Wordcount}

\begin{figure}
  \begin{center}
  \includegraphics[width=4in]{results/hadoop-wc-mcs.png}
  \caption{Hadoop wordcount results with HDFS on RAMdisk and localdisk on a 10-workstation cluster
  \label{fig:wc}
  }
\end{center}
\end{figure}
hadoop-wc-mcs.png
%Hadoop implementation.
%Swift implementation.

\subsection{Weather Datamining}

\section{SwiftReduce}
\subsection{Shared File Systems}
\subsection{Distributed File Systems}
\subsection{Lessons Learned}

\section{Related Work}
%Introduce and bring up the general related work.

Ed Walker's implementation of MapReduce via parallel shell~\cite{shellpipes}.

Supporting MapReduce-like functionality using a parallel, fast database system
can be feasible~\cite{friendfoes} while a database would not be of much use to
support a Swift workflow.

The success of MapReduce could be attributed to the development of internet
scale datacenters and commodity hardware interconnects~\cite{bulkparallel}.

\subsection{MapReduce Variants}
There has been variants of Hadoop implemented for scientific applications such
as Marissa~\cite{marissa} enhancing HDFS access and performance with the
applications not ported for Java. We view these variants as using the basic
tenet of MapReduce paradigm where in the data motion is not performed in favor
of moving compute tasks to distributed resources.

Application of a distributed MapReduce in scientific application involving text
mining~\cite{balkir}.

Unconventional methods to solving MapReduce problems~\cite{distdelayed}.

Swift Collective Data Management (CDM)~\cite{cdm} approach to data locality
under certain conditions helps alleviate the problem of data motions.

\subsection{Workflow Implementations}

\subsection{Coupling MapReduce and Workflow}
The coupling could happen in two possible ways: adhoc Workflow systems adapt
MapReduce tasks as their stages workflow managers offered by MapReduce systems.

Recently there have been quite a few tools developed around Hadoop such as
`YARN', `oozie' and `whirr'. These tools facilitate Hadoop to be used as a more
complete system doing application-level, real world tasks. `yarn' or Yet
Another Resource Negotiator is a tool that is designed to be interfaced with
resource managers and schedulers. Yarn is similar in functionality to pilot job
systems implemented by many workflow managers. Swift's pilot job implementation
is called Coasters. oozie is a workflow manager for Hadoop jobs. OOZIE can link
multiple individual Hadoop jobs together to accomplish a meaningful chain of
tasks. Whirr is a generic service developed to interface clouds. It is an
implementation neutral approach to running Hadoop jobs on multiple clouds.
Whirr handles the management of cloud nodes such as resource provision, access
and network configuration.

All of the above mentioned systems and their functionalities are provided by
many of today's scientific workflow managers to different degrees of
sophistication.

\section{Conclusions and Future Work}
In this paper we discuss the complementary strengths of workflow- and
MapReduce-based computational models with a goal of leveraging them for
workflow oriented applications. We compare and contrast features and qualities
of each of the systems with a representative implementation: Hadoop and Swift.
We implement illustrative solutions in each of the selected systems and show
their performance on a medium-scaled shared cluster and EC2 cloud. Finally, we
consider a coupling of Hadoop and Swift to the advantage of the systems and
demonstrate implementation of some solutions with two different modes of
coupling: running Swift on top of an HDFS file system and running Hadoop jobs
as Swift applications. We discuss the results and implications. 

\section{Acknowledgments}
\begin{comment}
This work was partially supported by the U.S. Department of Energy, under
Contract No. DE-AC02-06CH11357. Some work by DSK was supported by the National
Science Foundation, while working at the Foundation. Any opinion, finding, and
conclusions or recommendations expressed in this material are those of the
authors and do not necessarily reflect the views of the National Science
Foundation.
\end{comment}

\bibliographystyle{IEEEtran}
\bibliography{ref}
\ketanote{
NOTES
\begin{itemize}
\item HDFS on RAM will provide built-in replication.
\item How to work both with and without name/value pairs: Swift has no intrinsic name/value concept, and one can find good use cases both with and without keys.  Note that the separate project to add associative arrays to Swift is one way to integrate the concept of keys
\item How to do a reduction trees (especially for non-key-based workflows) in a manner that reduces the amount of data and avoids the requirement of sending the output of every map operation back to a single site for reduction
\item The idea of file-based vs. record-based distributed computing. Distinct advantages of each from the point of view of:
\item Usability of systems
\item Adaptation to different kinds of fabric
\item Suitability to application classes
\item Performance
\item Portability of solution
\item Parallel find is a nice example especially on distributed Chirp servers
\item Experiment platforms: + Cray systems: Raven, Blue Waters, Beagle + Bluegene/p
\item Example workflow apps:
    + DSSAT : 120k jobs, real science, high priority
    + Synthetic workflow patterns: SISO, SIMO, MISO, MIMO
    + Parallel and distributed find off Chirp servers
\item Expressivity | Programmability | Interoperability
\item Why a scientist program one way or the other
\item Compare with Spark model of computation
\end{itemize}    
}
\end{document}
NOTES
-- HDFS on RAM will provide built-in replication.

- How to work both with and without name/value pairs: Swift has no intrinsic name/value concept, and one can find good use cases both with and without keys.
Note that the separate project to add associative arrays to Swift is one way to integrate the concept of keys

- How to do a reduction trees (especially for non-key-based workflows) in a manner that reduces the amount of data and avoids the requirement of sending the output of every map operation back to a single site for reduction

- The idea of file-based vs. record-based distributed computing.

Distinct advantages of each from the point of view of:

-- Usability of systems

-- Adaptation to different kinds of fabric

-- Suitability to application classes

-- Performance

-- Portability of solution

-- Parallel find is a nice example especially on distributed Chirp servers

-- Experiment platforms:
    + Cray systems: Raven, Blue Waters, Beagle
    + Bluegene/p

-- Example workflow apps:
    + DSSAT : 120k jobs, real science, high priority
    + Synthetic workflow patterns: SISO, SIMO, MISO, MIMO
    + Parallel and distributed find off Chirp servers

-- Expressivity | Programmability | Interoperability

-- Why a scientist program one way or the other

-- Compare with Spark model of computation

What is HDFS?
HDFS, inspired by Google's GFS.%TODO: citation

Mail discussion with Yadu on Zaharia's RDD paper:


=== Yadu ===
So, the idea here is to hold result between stages in memory rather that write it to disk or shared file system. 
With no persistence, they deal with the possibility of failure by keeping track of the lineage of each generated 
dataset. I think the assumption here is that, the cost of recomputing a lost data item is much smaller and 
faster than check pointing every data set to disk. RDD's can also spillover to disk when RAM is full, and they 
don't explain that too well, or if there is a possibility of thrashing.

(As for the lineage part, the whole point of using file-pointers in SwiftK was to make sure swift kept track of 
files, and in case any file is lost it could be recomputed, so this is something we can show with little effort)

There is also a very simple description of their constructs map, join, filter applied to RDDs in memory. Ofcourse
one major pitfall I see is that the functions passed are all written in scala (major plus point for swift).

Examples given are iterative processing ones, which put hadoop at a disadvantage (due to having to write to HDFS)
and they should benefit our point. They have given some page-rank algo, a clustering algo.. (i don't remember the 
rest).

So, these are the main ideas, I think. We should probably talk once you've read it. Though I can't help feel like 
these guys beat us to our ideas.

=== Ketan ===
ok, I read the paper. I think the idea of in-memory computing is not new but the way they are manipulating data in-memory is new. However, above and beyond all the speed benefits etc, there are two key limitations:
1. Applications that deal with large amount of intermediate data. If you spill over a block of data to disk, you have to make a trip to disc to make the full data available.
2. Applications which do not have any intermediate reusable data: The model pretty much proves to be memory hungry in that case unless it degenerates to a disc oriented data easily.

Apart from this, there are still inter-node and inter-cluster bandwidths which will make a large part of application data movement times.

Does these points make sense to you?

In my opinion, while the paper is relevant to our work, it does not out shine our ideas and implementation. And as you said, we are much less obscure than Scala in terms of expression and programmability.

Let's discuss this more and DSSAT implementation when we meet.
We should also take a look at this HaLoop thingy.

ToDo: DSSAT application, take a look at the Science MapReduce paper from Berkeley, Take a look at the applications from the classic MapReduce paper.

== Yadu Notes ==

Current flow is designed like this:

0. To associate files with their location we define a new swift type *fileptr*
   fileptr (for filepointer) is a regular file to swift, but it can be interpreted 
   by by apps or (in future) swiftwrap as a globally addressable filepath.
   Currently a fileptr is of the structure:
   Node0 /path/to/file0 /path/to/file1 /path/to/file2 ...
   ...
   Noden /path/to/file0 ...

1. mapper_func takes some input and generates some files as output and returns a fileptr
   So, the map stage is really just a foreach loop, filling up map_results[] array.

   map_results[] = map ( mapper_func, input_array );

2. Process the fileptr's returned from the map stage to generate a list of nodes, where
   the map jobs left intermediate data.
   Currently the get_uniq_nodes() app returns a file with the format:
   node1 file1 file2 ...
   node2 file1 file2 ...

3. Run a combiner job on every node returned from get_uniq_nodes()
   The combiner is used to reduce the results in a distributed fashion.
   A combine function is possible only when the combine function is commutative and 
   associative. As in map-reduce, a combine function maybe called on one or more files.
   For more complex reduce functions, a proper combine stage could be impossible, in 
   such cases, a combiner could be just a concatenation/compression which would result 
   in better file transfer performance to forward stages.

4. Optional-Tree reduction. There are several strategies to doing a distributed reduce
   of results from several nodes. A general K-way-tree reduction is a configurable
   method to efficiently perform compute heavy reduction operations.
   In cases where the cost is in the bulk of file transfers and reduction stage does
   not result in smaller files, a tree reduction may not be the best fit due to the
   additional transfers required between stages.
   If a tree-reduction is not chosen, a simple single level reduction could be
   used to gather all results from the nodes where results from either map/combine 
   stage could be fetched to generate the final result.


Points from discussion with Mike.

1. Function pointers or the ability to pass functions as args to other
 functions. We could make a mockup with preprocessors.
 
 map_results = map ( mapper_func, input_array );
 
 translates to :

 foreach item, i in input_array {
   map_results[i] = mapper_func (item);
 }

 This is just syntactic sugar, worth considering what additional capability truly being
 able to pass functions/apps give us.

2. Extend current filepointers to function as filepointersets.
   Instead of one filepointer pointing to one file, a file pointer could point at
   one or more files as long as the format is maintained. 
   Current code works with this.
   
   Filepointers should be treated as just file when it is being returned/copied.
   When it is passed as an arg to an app, it should go through a conceptual dereferencing.
   In this context, the filepointer should be interpreted and the actual files it points
   to should be fetched if not present locally.

3. 
   

