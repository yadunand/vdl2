#!/bin/bash

trap "echo SCRIPT CAUGHT TERM" TERM

HOST=$( hostname )
echo HOST: ${HOST}

chirp_server -p 9080 -r ${HOME}/cr_${HOST} &
JOB=${!}

wait
kill ${JOB}

exit 0
