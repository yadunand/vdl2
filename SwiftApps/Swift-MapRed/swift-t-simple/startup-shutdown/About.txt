
Simple test for chirp_server startup/shutdown as controlled by
Swift/T TURBINE_WORKER_SERVICE.

Usage on MCS compute hosts:

Create your MPICH hosts.txt file

Create chirp root directories for each host's chirp server in:
~/cr_${HOST}

Set permissive ACLs

Set: export TURBINE_WORKER_SERVICE="chirp_service.sh"

Run:

stc test-chirp.swift
turbine -l -f hosts.txt -n 4 test-chirp.tcl

The log shows that the servers are brought up and shut down.

