#!/bin/zsh

HOSTS=$1

if [[ ${#*} != 1 ]]
then
  print "usage: stop-servers.zsh HOSTS"
  exit 1
fi

# Verbose evaluation
@ ()
{
  printf "+ ${*}\n\n"
  eval ${*}
  printf "\n"
}

# Kill servers
for H in $( < ${HOSTS} )
do
  @ ssh ${H} "killall chirp_server"
done
