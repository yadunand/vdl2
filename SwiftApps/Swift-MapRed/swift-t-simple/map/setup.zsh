#!/bin/zsh -e

# Hosts file
HOSTS=$1
# Port on which to run chirp_server
PORT=$2

# Number of files to create per server
FILES=3

zmodload zsh/files

if [[ ${#*} != 2 ]]
then
  print "usage: setup.sh HOSTS PORT"
  exit 1
fi

DEFAULT_ACL=${HOME}/chirp-acl.txt
{
  print "unix:${USER} rwlda"
  print "hostname:*.mcs.anl.gov rwlda"
} > ${DEFAULT_ACL}

# Start servers
for H in $( < ${HOSTS} )
do
  CHIRP_ROOT=${HOME}/CR/${H}
  ssh ${HOST} mkdir -p ${CHIRP_ROOT}
  ssh ${H} chirp_server -p ${PORT} -r ${CHIRP_ROOT} -A ${DEFAULT_ACL} &
done

# Create and post some initial data
i=0
for H in $( < ${HOSTS} )
do
  for (( j=0 ; j<${FILES} ; j++ ))
  do
    F=f-${i}.txt
    echo "FILE:${i}" > ${F}
    chirp -a hostname ${H}:${PORT} put ${F} /${F}
    rm ${F}
    (( i++ )) || true
  done
done

# Stop servers- we start them with TURBINE_WORKER_SERVICE
./stop-servers.zsh ${HOSTS}

wait

print DONE
exit 0
