#!/bin/bash -e

trap "echo SCRIPT CAUGHT SIGTERM" TERM

HOST=$( hostname )
echo HOST: ${HOST}

# This should have been created by setup.sh:
DEFAULT_ACL=${HOME}/chirp-acl.txt
if [[ ! -f ${DEFAULT_ACL} ]]
then
  echo "could not find ${DEFAULT_ACL}"
  exit 1
fi

CHIRP_ROOT=${HOME}/CR/${HOST}
echo "CHIRP_ROOT: ${CHIRP_ROOT}"

chirp_server -d all \
             -p 9080 \
             -r ${CHIRP_ROOT} \
             -A ${DEFAULT_ACL} \
  >& ${CHIRP_ROOT}/server.log &

JOB=${!}

wait || true
kill ${JOB}

echo SERVICE DONE

exit 0
