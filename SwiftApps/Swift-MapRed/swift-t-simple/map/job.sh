#!/bin/sh -e

# Pretend user job

OUTPUT=$1
INPUT=$2

# Maximal job runtime
MAX_TIME=1

HOST=$( hostname )

echo "JOB: ${HOST} rank=${PMI_RANK} ${OUTPUT} ${INPUT}"

cd ${HOME}/CR/${HOST}

export PARROT_TIMEOUT=5

# Retrieve input
NAME=$( basename ${INPUT} )
cp -v ${INPUT} /tmp/${NAME}

echo RANK $PMI_RANK

# Emulate some compute time (0-9 seconds based on PID, rank, and nanos)
RANDOM=$(( ${$} + ${PMI_RANK} + $( date +%N ) ))
DELAY=$(( ${RANDOM} % ${MAX_TIME} + 1))
sleep ${DELAY}

# Store output
cp -v /tmp/${NAME} ${OUTPUT}
