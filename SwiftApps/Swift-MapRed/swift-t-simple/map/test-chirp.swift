
import io;
import files;
import location;
import string;
import sys;

app (file t) list_files(string host, int port)
{
  "chirp" "-a" "hostname" (sprintf("%s:%i", host, port)) "ls" "/"
          @stdout=t;
}

app (file o) job(file i)
{
  "./job.sh" o i;
}

main
{
  string hosts_txt = argv("hosts");
  int port = toint(argv("port"));

  string hosts[] = split(read(input(hosts_txt)), "\n");

  string filenames[][];

  foreach host,i in hosts
  {
    if (host != "")
    {
      printf("host: %s", host);
      file t<sprintf("contents-%s.txt",host)> = list_files(host, port);
      string s = read(t);
      string files[] = split(s, "\n");
      foreach f,j in files
      {
        if (f != "" && find(f,".log", 0, 20) == -1)
        {
          // filenames[i][j] = f;

          string g = replace(f, "f-", "g-", 0);
          printf("host: %s file: %s %s", host, f, g);
          file u_in = input_url(f);
          file u_out<g>;
          location L = hostmap_one_worker(host);
          u_out = @location=L job(u_in);
        }
      }
    }
  }

  // foreach d, k in filenames
  // {
  //       printf("k: %i", k);
  // }
}
