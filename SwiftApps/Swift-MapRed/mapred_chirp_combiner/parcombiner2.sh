#!/bin/bash

FILES=$*
SUM=0
COUNT=0

cat $* > RESULT_FILE

# Find the unique sites in the RESULT_FILE to run a combiner there.
#SITES=($(sort -u -t' ' -k1,1 RESULT_FILE))
cat <<'EOF' > recombiner.sh
#!/bin/bash
#SITE=$1; shift 1;
ID=$1; shift 1;
FILES=$*
SUM=0;COUNT=0;
for file in ${FILES[*]}; do
    [ ! -f $file ] && echo "$file is missing" >> $HOSTNAME.parrot_run.log
    RES=($(awk '{ sum += $1 } END { print sum,NR }' $file))
    SUM=$(($SUM+${RES[0]}))
    COUNT=$(($COUNT+${RES[1]}))
done;
echo -e "$SUM $COUNT" > $HOSTNAME.$ID.imd
cleanup(){
sleep 2;
ls input* | grep -v $1 | while read f; do rm $f; done
}
#cleanup $HOSTNAME.$ID.imd &
EOF
chmod 777 recombiner.sh

combine_fetch_process()
{
    SITE=$1; shift
    ID=$1;
    FILES=($(grep $SITE RESULT_FILE | awk '{ print $2 }'))
    CHIRPDIR=$(dirname ${FILES[0]})
    chirp_put ./recombiner.sh $SITE recombiner.sh   
    echo "    ssh $SITE \"cd $CHIRPDIR; ./recombiner.sh $ID ${FILES[*]}\""
    ssh $SITE "cd $CHIRPDIR; ./recombiner.sh $ID ${FILES[*]}"
    if [ "$?" == "0" ]
    then
	echo "Ssh worked"
    else
	echo "Ssh failed"
    fi
    chirp_get $SITE $SITE.$ID.imd $SITE.$ID.txt
}

STAGE2="";
SITES=($(awk '{ print $1 }' RESULT_FILE  | sort -u))
for SITE in ${SITES[*]}
do
    ID=$RANDOM
    combine_fetch_process $SITE $ID &
    STAGE2="$STAGE2 $SITE.$ID.txt"
done;
wait


cat ${STAGE2[*]} > TEMP
SUM=($(awk '{ sum += $1 } END { print sum }' TEMP))
COUNT=($(awk '{ sum += $2 } END { print sum }' TEMP))
HOST=$(hostname -f)
echo "Final results (on $HOST):"
echo "$SUM $COUNT"

exit 0;