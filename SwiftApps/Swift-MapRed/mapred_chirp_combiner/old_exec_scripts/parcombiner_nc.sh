#!/bin/bash

FILES=$*
SUM=0
COUNT=0
REMOTE_PORT=29900
cat $* > RESULT_FILE

# Find the unique sites in the RESULT_FILE to run a combiner there.
#SITES=($(sort -u -t' ' -k1,1 RESULT_FILE))
cat <<'EOF' > recombiner.sh
SUM=0;COUNT=0;
for file in ${FILES[*]}; do
    [ ! -f $file ] && echo "$file is missing" >> $HOSTNAME.parrot_run.log
    RES=($(awk '{ sum += $1 } END { print sum,NR }' $file))
    SUM=$(($SUM+${RES[0]}))
    COUNT=$(($COUNT+${RES[1]}))
    rm $file &
done;
echo -e "$SUM $COUNT"
EOF
chmod 777 recombiner.sh


combine_fetch_process()
{
    SITE=$1; shift
    ID=$1; shift
    REMOTE_PORT=$1;
    PORT=$(($REMOTE_PORT+$ID))

    FILES=($(grep $SITE RESULT_FILE | awk '{ print $2 }'))
    CHIRPDIR=$(dirname ${FILES[0]})
    #chirp_put ./recombiner.sh $SITE recombiner.sh           
    #ssh $SITE "cd $CHIRPDIR; ./recombiner.sh $ID ${FILES[*]}"
    #chirp_get $SITE $SITE.$ID.imd $SITE.$ID.txt

    echo -e "$(hostname -f) $PORT\n#!/bin/bash \n ID=$ID \n FILES=(${FILES[*]}) \n" > $SITE.$ID.sh
    cat recombiner.sh >> $SITE.$ID.sh
    nc $SITE $REMOTE_PORT < $SITE.$ID.sh
    nc -l $PORT > $SITE.$ID.txt
    #rm $SITE.$ID.sh
}

STAGE2="";
SITES=($(awk '{ print $1 }' RESULT_FILE  | sort -u))
for SITE in ${SITES[*]}
do
    ID=$RANDOM
    combine_fetch_process $SITE $ID $REMOTE_PORT &
    STAGE2="$STAGE2 $SITE.$ID.txt"
done;
wait

cat ${STAGE2[*]} > TEMP
SUM=($(awk '{ sum += $1 } END { print sum }' TEMP))
COUNT=($(awk '{ sum += $2 } END { print sum }' TEMP))
echo "Final results (on $HOSTNAME):"
echo "$SUM $COUNT"

exit 0;