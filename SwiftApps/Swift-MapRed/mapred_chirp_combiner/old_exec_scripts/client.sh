#!/bin/bash

REMOTE_PORT=20033

if [ "$1" == "" ]
then
    echo "Client needs an arg for remote"
    exit 0
fi

EXEC_FILE="tmp.$RANDOM"

PORT=$(($REMOTE_PORT + 10))
echo "$(hostname -f) $PORT" > $EXEC_FILE
cat <<'EOF' >> $EXEC_FILE
#!/bin/bash
echo "Recombiner on $HOSTNAME"
echo $(hostname -f)
EOF

listen_for_reply()
{
    nc -l $PORT > reply;
    echo "Connection received back on $PORT"
}

listen_for_reply &
nc $1 $REMOTE_PORT < $EXEC_FILE
echo "nc $1 $PORT;"

