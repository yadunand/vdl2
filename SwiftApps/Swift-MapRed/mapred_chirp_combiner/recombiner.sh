SUM=0;COUNT=0;
for file in ${FILES[*]}; do
    [ ! -f $file ] && echo "$file is missing" >> $HOSTNAME.parrot_run.log
    RES=($(awk '{ sum += $1 } END { print sum,NR }' $file))
    SUM=$(($SUM+${RES[0]}))
    COUNT=$(($COUNT+${RES[1]}))
    rm $file &
done;
echo -e "$SUM $COUNT"
