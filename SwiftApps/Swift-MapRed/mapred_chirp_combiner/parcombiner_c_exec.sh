#!/bin/bash

FILES=$*
SUM=0
COUNT=0
REMOTE_PORT=29900
cat $* > RESULT_FILE

HOSTNAME=$(hostname -f)
# Find the unique sites in the RESULT_FILE to run a combiner there.
#SITES=($(sort -u -t' ' -k1,1 RESULT_FILE))


############## LOCAL COMBINER CODE ###################
cat <<'EOF' > recombiner.sh
SUM=0;COUNT=0;
for file in ${FILES[*]}; do
    [ ! -f $file ] && echo "$file is missing" >> $HOSTNAME.parrot_run.log
    RES=($(awk '{ sum += $1 } END { print sum,NR }' $file))
    SUM=$(($SUM+${RES[0]}))
    COUNT=$(($COUNT+${RES[1]}))
    rm $file &
done;
echo -e "$SUM $COUNT"
EOF
############# END LOCAL COMBINER #####################
chmod 777 recombiner.sh

cp /lustre/beagle/yadunandb/bin/exec_client .

combine_fetch_process()
{
    SITE=$1; shift
    ID=$1; shift
    REMOTE_PORT=$1;
    PORT=$(($REMOTE_PORT+$ID))

    FILES=($(grep $SITE RESULT_FILE | awk '{ print $2 }'))
    CHIRPDIR=$(dirname ${FILES[0]})

    echo -e "#!/bin/bash \nID=$ID \nFILES=(${FILES[*]}) \n" > $SITE.$ID.sh
    cat recombiner.sh >> $SITE.$ID.sh
    #echo "./exec_client $SITE $SITE.$ID.sh"
    #cat $SITE.$ID.sh
    ./exec_client $SITE $SITE.$ID.sh | tee $SITE.$ID.txt
    #echo "Content of $SITE.$ID.txt is $(cat $SITE.$ID.txt)"
}

echo "parcombiner_c_exec running at $PWD"
STAGE2="";
SITES=($(awk '{ print $1 }' RESULT_FILE  | sort -u))
for SITE in ${SITES[*]}
do
    ID=$RANDOM
    combine_fetch_process $SITE $ID $REMOTE_PORT &
    STAGE2="$STAGE2 $SITE.$ID.txt"
done;
wait

################ REDUCE RESULTS FROM LOCAL-COMBINERS #################
cat ${STAGE2[*]} > TEMP
SUM=($(awk '{ sum += $1 } END { print sum }' TEMP))
COUNT=($(awk '{ sum += $2 } END { print sum }' TEMP))
echo "Final results (on $HOSTNAME):"
echo "$SUM $COUNT"
################ END OF REDUCE #######################################

exit 0;