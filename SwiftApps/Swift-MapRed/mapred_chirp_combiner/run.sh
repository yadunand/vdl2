#!/bin/bash
clear;
#swift -tc.file tc.data -sites.file sites.xml motoko.swift

#echo "Running run_catsn.swift"
#swift -tc.file tc.data -sites.file sites.xml run_catsn.swift


if [ "$1" != "" ]
then
    SITE="$1"
else
    SITE="beagle"
fi

if [ "$SITE" == "beagle" ]
then
    ./compile
    cp exec_server /lustre/beagle/yadunandb/bin/
    cp exec_client /lustre/beagle/yadunandb/bin/
fi;
#Call as check_error $? <ERR_CODE> <Error message>                                                                                                                                        
check_error()
{
    if [ $1 == $2 ]
    then
        echo $3
    else
        echo "Run completed with code:$1"
    fi
}
#export COG_OPTS="-Dtcp.channel.log.io.performance=true"

verify()
{
    LOOPS=$1
    RECSIZE=$2
    ACTUAL_RESULT=($(tail -n 1 final_result))
    EXPECTED_RESULT=$(($LOOPS * $RECSIZE * 10000 ))
    if [ "$EXPECTED_RESULT" ==  "${ACTUAL_RESULT[1]}" ] 
    then
	echo "---------------------------------------------"
	echo "COUNT matches expected records"
	echo "ACTUAL COUNT  : ${ACTUAL_RESULT[1]}"
	echo "EXPECTED COUNT: $EXPECTED_RESULT"
	echo "---------------------------------------------"
    else
	echo "---------------------------------------------"
	echo "COUNT does *NOT* match expected records"
	echo "ACTUAL COUNT  : ${ACTUAL_RESULT[1]}"
	echo "EXPECTED COUNT: $EXPECTED_RESULT"
	cat err_file
	echo "---------------------------------------------"
    fi
}

run_swift_chirp()
{
    LOOPS=$1
    RECSIZE=$2
    ./clean.sh
    echo "================================================================" >> LAB_RECORDS
    echo "RUNTYPE :SWIFT+Chirp (No local combiners), SITE:$SITE, CHUNKS:$LOOPS X FILESIZE:$RECSIZE " | tee -a LAB_RECORDS
    ( time -p swift -tc.file tc.data -config cf -sites.file $SITE.xml teragen.swift -files=$LOOPS -filesize=$RECSIZE ) 2>&1 | tee -a LAB_RECORDS
    echo "chunks $LOOPS filesize $RECSIZE site $SITE type Swift+Chirp" >> LAB_RECORDS
    verify $LOOPS $RECSIZE | tee -a LAB_RECORDS
    echo "================================================================"
}

run_swift_chirp_combiner()
{
    LOOPS=$1
    RECSIZE=$2
    #./clean.sh &> /dev/null
    echo "================================================================" >> LAB_RECORDS
    echo "RUNTYPE :SWIFT+Chirp (With local combiners), SITE:$SITE, CHUNKS:$LOOPS X FILESIZE:$RECSIZE " | tee -a LAB_RECORDS
    ( time -p swift -tc.file tc.data -config cf -sites.file $SITE.xml teragen.swift -files=$LOOPS -filesize=$RECSIZE ) 2>&1 | tee -a LAB_RECORDS
    echo "chunks $LOOPS filesize $RECSIZE site $SITE type Swift+Chirp" >> LAB_RECORDS
    verify $LOOPS $RECSIZE | tee -a LAB_RECORDS
    echo "================================================================"
}


run_swift_chirp_combiner 3 10;
#run_swift_chirp_combiner 10 10;
exit 0

####################SWIFT + CHIRP VERSION############################
LOOPS=10
RECSIZE=100
for LOOPS in `seq 10 10 50`
do
    for RECSIZE in `seq 0 25 200`
    do
	[ $RECSIZE -eq 0 ] && RECSIZE=1
	run_swift_chirp $LOOPS $RECSIZE
	sleep 300;
    done    
done;
exit 0
####################PROVIDER STAGING VERSION######################








































TOTALSIZE=1000000000 # Totalsize/chunksize is the  
CHUNKSIZE=100000000  # 10^8 records in each chunk
NUMCHUNKS=$(($TOTALSIZE / $CHUNKSIZE))
LOOP=1
TIMEOUT=600
time {
    echo "RUNTYPE :PROVIDER STAGING VERSION, SITE: $SITE, CHUNKS: $NUMCHUNKS"
    echo "timeout $TIMEOUT swift -tc.file tc.data -config cf -sites.file $SITE.xml teragen.swift -files=$NUMCHUNKS"
    timeout $TIMEOUT swift -tc.file tc.data -config cf -sites.file $SITE.xml teragen.swift -files=$NUMCHUNKS
    check_error $? 124 "Run terminated by timeout of $((TIMEOUT/60)) minute(s)"
} | tee -a LAB_RECORDS
exit 0


#######################CDM VERSION##############################
 

TOTALSIZE=1000000000 # Totalsize/chunksize is the  
CHUNKSIZE=100000000  # 10^8 records in each chunk: THIS IS A DEFAULT 
NUMCHUNKS=$(($TOTALSIZE / $CHUNKSIZE))
LOOP=1
TIMEOUT=600
time {
    echo "RUNTYPE :CDM VERSION, SITE: $SITE, CHUNKS: $NUMCHUNKS"
    echo "timeout $TIMEOUT swift -cdm.file $SITE.cdm -tc.file tc.data -config cf -sites.file $SITE.xml teragen.swift -files=$NUMCHUNKS"
    timeout $TIMEOUT swift -cdm.file $SITE.cdm -tc.file tc.data -config cf -sites.file $SITE.xml teragen.swift -files=$NUMCHUNKS
    check_error $? 124 "Run terminated by timeout of $((TIMEOUT/60)) minute(s)"
} | tee -a LAB_RECORDS 
exit 0

