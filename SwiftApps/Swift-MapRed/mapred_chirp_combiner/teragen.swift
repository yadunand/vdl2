type file;
type script;

app (file out, file err) gen_data (script run, int recsize)
{
    bash @run recsize stdout=@out stderr=@err;
}

app (file out, file err) comb_data (script comb, file array[])
{
    bash @comb @array stdout=@out stderr=@err;
}


file tgen_out[] <simple_mapper; prefix="tgen", suffix=".out">;
file tgen_err[] <simple_mapper; prefix="tgen", suffix=".err">;

//script wrapper <"teragen_wrap_nc.sh">;
script wrapper <"teragen_wrap_c_exec.sh">;

int files = @toInt(@arg("files","0"));
// This would make 10^4 records per file
int fsize = @toInt(@arg("filesize","10")); 

string dir = @arg("dir", "./");

foreach item,i in [0:files-1] {
	(tgen_out[i], tgen_err[i]) = gen_data(wrapper, fsize);
}


//script combine <"parcombiner_nc.sh">;
script combine <"parcombiner_c_exec.sh">;
file final <"final_result">;
file errs <"err_file">;
(final, errs) = comb_data(combine, tgen_out);
