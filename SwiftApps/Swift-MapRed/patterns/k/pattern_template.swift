type file;
type script;

app (file _outB1, file _outB2, file _outB3, file _outB4) A (script _s, file _inA1){
    @_s @_inA1 @_outB1 @_outB2 @_outB3 @_outB4;
}

app (file _outC1) B (script _s, file _inB1){
    @_s @_inB1 @_outC1;
}

app (file _res) C (script _s, file _inC1, file _inC2, file _inC3, file _inC4){
    @_s @_inC1 @_inC2 @_inC3 @_inC4 @_res;
}

file ina1<"sample.txt">;
script a<"a.sh">;
script b<"b.sh">;
script c<"c.sh">;

foreach i in [0:1]{

    file outb1 <single_file_mapper; file=@strcat("output/outb1.",i,".txt")>;
    file outb2 <single_file_mapper; file=@strcat("output/outb2.",i,".txt")>;
    file outb3 <single_file_mapper; file=@strcat("output/outb3.",i,".txt")>;
    file outb4 <single_file_mapper; file=@strcat("output/outb4.",i,".txt")>;

    file outc1 <single_file_mapper; file=@strcat("output/outc1.",i,".txt")>;
    file outc2 <single_file_mapper; file=@strcat("output/outc2.",i,".txt")>;
    file outc3 <single_file_mapper; file=@strcat("output/outc3.",i,".txt")>;
    file outc4 <single_file_mapper; file=@strcat("output/outc4.",i,".txt")>;

    file res <single_file_mapper; file=@strcat("output/res.",i,".txt")>;

    (outb1, outb2, outb3, outb4) = A (a, ina1);

    outc1                        = B (b, outb1);
    outc2                        = B (b, outb2);
    outc3                        = B (b, outb3);
    outc4                        = B (b, outb4);

    res                          = C (c, outc1, outc2, outc3, outc4);
}

