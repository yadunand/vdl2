type file;
type script;

app (file _outB1, file _outB2, file _outB3, file _outB4) A (script _s, file _inA1){
   sh @_s @_inA1 @_outB1 @_outB2 @_outB3 @_outB4;
}


file ina1<"sample1.txt">;
script a<"simo.sh">;

foreach i in [0:1]{

    file outb1 <single_file_mapper; file=@strcat("output/outb1.",i,".txt")>;
    file outb2 <single_file_mapper; file=@strcat("output/outb2.",i,".txt")>;
    file outb3 <single_file_mapper; file=@strcat("output/outb3.",i,".txt")>;
    file outb4 <single_file_mapper; file=@strcat("output/outb4.",i,".txt")>;

    (outb1, outb2, outb3, outb4) = A (a, ina1);
}

