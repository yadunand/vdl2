type file;
type script;

app (file _res) B (script _s, file _inB1, file _inB2, file _inB3, file _inB4){
   sh @_s @_inB1 @_inB2 @_inB3 @_inB4 @_res;
}

file inb1<"sample1.txt">;
file inb2<"sample2.txt">;
file inb3<"sample3.txt">;
file inb4<"sample4.txt">;

script b<"miso.sh">;

foreach i in [0:1]{
    file res <single_file_mapper; file=@strcat("output/res.",i,".txt")>;

    res = B (b, inb1, inb2, inb3, inb4);
}

