import files;
import io;
import string;
import sys;

app (file _outB1, file _outB2, file _outB3, file _outB4) A (file _inA1){
    "./simo.sh" @_inA1 @_outB1 @_outB2 @_outB3 @_outB4;
}

main
{

file in_a=input_file("sample.txt");

foreach i in [0:1]{

   string outb1=sprintf("outb1-%i.dat", i); 
   string outb2=sprintf("outb2-%i.dat", i); 
   string outb3=sprintf("outb3-%i.dat", i); 
   string outb4=sprintf("outb4-%i.dat", i); 
   file o1 <outb1>;
   file o2 <outb2>;
   file o3 <outb3>;
   file o4 <outb4>;

   (o1, o2, o3, o4) = A (in_a);
}
}
