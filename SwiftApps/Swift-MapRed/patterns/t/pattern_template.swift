import files;
import io;
import string;
import sys;

app (file _outB1, file _outB2, file _outB3, file _outB4) A (file _inA1){
    "a.sh" @_inA1 @_outB1 @_outB2 @_outB3 @_outB4;
}

app (file _outC1) B (file _inB1){
    "b.sh" @_inB1 @_outC1;
}

app (file _res) C (file _inC1, file _inC2, file _inC3, file _inC4){
    "c.sh" @_inC1 @_inC2 @_inC3 @_inC4 @_res;
}

file in_a=input_file("sample.txt");

foreach i in [0:1]{

    string outb1=sprintf("/tmp/swift.work/outb1-%i.dat", i); 
    string outb2=sprintf("/tmp/swift.work/outb2-%i.dat", i); 
    string outb3=sprintf("/tmp/swift.work/outb3-%i.dat", i); 
    string outb4=sprintf("/tmp/swift.work/outb4-%i.dat", i); 

    string outc1=sprintf("/tmp/swift.work/outc1-%i.dat", i);
    string outc2=sprintf("/tmp/swift.work/outc2-%i.dat", i);
    string outc3=sprintf("/tmp/swift.work/outc3-%i.dat", i);
    string outc4=sprintf("/tmp/swift.work/outc4-%i.dat", i);

    string res=sprintf("/tmp/swift.work/res-%i.dat", i);

    (outb1, outb2, outb3, outb4) = A (ina1);

    outc1                        = B (outb1);
    outc2                        = B (outb2);
    outc3                        = B (outb3);
    outc4                        = B (outb4);

    res                          = C (outc1, outc2, outc3, outc4);
}

