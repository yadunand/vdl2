import files;
import io;
import string;
import sys;

app (file _res) C (file _inC1, file _inC2, file _inC3, file _inC4){
    "./miso.sh" @_inC1 @_inC2 @_inC3 @_inC4 @_res;
}

main{
file in_a1=input_file("sample.txt");
file in_a2=input_file("sample.txt");
file in_a3=input_file("sample.txt");
file in_a4=input_file("sample.txt");

foreach i in [0:1]{
    string res=sprintf("/tmp/swift.work/res-%i.dat", i);
    file r <res> = C (in_a1, in_a2, in_a3, in_a4);
}
}
