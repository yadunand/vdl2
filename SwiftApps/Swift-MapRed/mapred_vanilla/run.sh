#!/bin/bash
clear;
#swift -tc.file tc.data -sites.file sites.xml motoko.swift

#echo "Running run_catsn.swift"
#swift -tc.file tc.data -sites.file sites.xml run_catsn.swift


if [ "$1" != "" ]
then
    SITE="$1"
else
    SITE="anl"
fi

#Call as check_error $? <ERR_CODE> <Error message>                                                                                                                                        
check_error()
{
    if [ $1 == $2 ]
    then
        echo $3
    else
        echo "Run completed with code:$1"
    fi
}
#export COG_OPTS="-Dtcp.channel.log.io.performance=true"

run_swift_vanilla()
{
    LOOPS=$1
    RECSIZE=$2
    ./clean.sh
    echo "================================================================" >> LAB_RECORDS
    echo "RUNTYPE :SWIFT+Chirp (No local combiners), SITE:$SITE, CHUNKS:$LOOPS X FILESIZE:$RECSIZE " | tee -a LAB_RECORDS
    ( time -p swift -tc.file tc.data -config cf -sites.file $SITE.xml teragen.swift -loops=$LOOPS -recsize=$RECSIZE ) 2>&1 | tee -a LAB_RECORDS
    echo "chunks $LOOPS filesize $RECSIZE site $SITE type Swift+Chirp" >> LAB_RECORDS
    echo "================================================================"
}

run_swift_vanilla 10 20;
exit 0

####################SWIFT + CHIRP VERSION############################
LOOPS=10
RECSIZE=100
for LOOPS in `seq 10 10 50`
do
    for RECSIZE in `seq 0 25 200`
    do
	[ $RECSIZE -eq 0 ] && RECSIZE=1
	run_swift_chirp $LOOPS $RECSIZE
	sleep 300;
    done    
done;
exit 0
####################PROVIDER STAGING VERSION######################








































TOTALSIZE=1000000000 # Totalsize/chunksize is the  
CHUNKSIZE=100000000  # 10^8 records in each chunk
NUMCHUNKS=$(($TOTALSIZE / $CHUNKSIZE))
LOOP=1
TIMEOUT=600
time {
    echo "RUNTYPE :PROVIDER STAGING VERSION, SITE: $SITE, CHUNKS: $NUMCHUNKS"
    echo "timeout $TIMEOUT swift -tc.file tc.data -config cf -sites.file $SITE.xml teragen.swift -loops=$NUMCHUNKS"
    timeout $TIMEOUT swift -tc.file tc.data -config cf -sites.file $SITE.xml teragen.swift -loops=$NUMCHUNKS
    check_error $? 124 "Run terminated by timeout of $((TIMEOUT/60)) minute(s)"
} | tee -a LAB_RECORDS
exit 0


#######################CDM VERSION##############################
 

TOTALSIZE=1000000000 # Totalsize/chunksize is the  
CHUNKSIZE=100000000  # 10^8 records in each chunk: THIS IS A DEFAULT 
NUMCHUNKS=$(($TOTALSIZE / $CHUNKSIZE))
LOOP=1
TIMEOUT=600
time {
    echo "RUNTYPE :CDM VERSION, SITE: $SITE, CHUNKS: $NUMCHUNKS"
    echo "timeout $TIMEOUT swift -cdm.file $SITE.cdm -tc.file tc.data -config cf -sites.file $SITE.xml teragen.swift -loops=$NUMCHUNKS"
    timeout $TIMEOUT swift -cdm.file $SITE.cdm -tc.file tc.data -config cf -sites.file $SITE.xml teragen.swift -loops=$NUMCHUNKS
    check_error $? 124 "Run terminated by timeout of $((TIMEOUT/60)) minute(s)"
} | tee -a LAB_RECORDS 
exit 0

