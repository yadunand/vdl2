type file_pdb;
type file_dat;
type file_exe;
type file_score;

app (file_dat dat_file)
do_one_dock
( string        param_root,
  string        param_modulo,
  file_pdb param_file_static,
  file_pdb param_file_mobile )
{
	modftdock 32
		"-modulo" @param_modulo
		"-root"   @param_root
		"-static" @param_file_static
		"-mobile" @param_file_mobile
		"-calculate_grid" @arg("grid","2.5")
		"-angle_step" "10"
		"-keep" "10"
		"-noelec";
}

app (file_dat o) read_dat (file_dat j[]){
	cat @filenames(j) stdout=@o;
}

app (file_dat o) do_the_merge (string in_dir, string wild_card, file_dat sync[]){
	do_merge in_dir wild_card stdout=@o;
}

app (file_score s) scoreit (file_exe param_distances_exe,
                            file_exe param_eval_exe,
                            file_dat param_merged_dat,
                            file_exe param_tRNA_score_exe){

///tmp/scoredat.exe /tmp/distances300r.exe /tmp/eval300.exe /tmp/${1}.dat  /tmp/tRNA_score.exe > /tmp/${1}.res
      score @param_distances_exe @param_eval_exe @param_merged_dat @param_tRNA_score_exe stdout=@s;
}

string modulus = @arg("m","100");
string str_roots[] = readData( @arg( "list" ) );


foreach str_root, pid in str_roots
{
	trace( str_root );

	string str_file_static = @strcat( "input/", str_root, ".pdb" );
	string str_file_mobile = "input/4TRA.pdb";

	file_pdb file_static < single_file_mapper; file = str_file_static >;
	file_pdb file_mobile < single_file_mapper; file = str_file_mobile >;

	file_dat dat_files[] < simple_mapper;
		padding = 3,
		location = "output",
		prefix  = @strcat( str_root, "_" ),
		suffix  = ".dat" >;
	
	file_dat merged_file[] < simple_mapper;
		location = "merged_data",
		prefix  = str_root,
		suffix  = ".dat" >;

	file_score score_file[] < simple_mapper;
		location = "score_data",
		prefix  = str_root,
		suffix  = ".res" >;

	int n = @toint(@arg("n","1"));
	// break docking jobs + do 'em in parallel
	foreach mod_index in [0:n-1] {
		string str_modulo = @strcat(mod_index, ":", modulus);
		dat_files[ mod_index ] = do_one_dock( str_root,
				str_modulo,
				file_static,
				file_mobile );
	}

//	file_dat out_files[];

        merged_file[pid]=do_the_merge("output/", @strcat(str_root, "_???.dat"), dat_files); //dat_files used for synchronization
        
        string str_file_distances_exe = "app/Scoring/distances300r.exe";
        string str_file_eval_exe = "app/Scoring/eval300.exe";
        string str_file_tRNA_score_exe = "app/Scoring/tRNA_score.exe";

	file_exe file_distances_exe < single_file_mapper; file = str_file_distances_exe >;
	file_exe file_eval_exe < single_file_mapper; file = str_file_eval_exe >;
	file_exe file_tRNA_score_exe < single_file_mapper; file = str_file_tRNA_score_exe >;

        score_file[pid]=scoreit(file_distances_exe, file_eval_exe, merged_file[pid], file_tRNA_score_exe); 
}

