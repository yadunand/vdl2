## 
# Note: The configuration in this package is suitable for Beagle environment only
##
#SWIFT_HEAP_MAX=6000M swift -config cf -tc.file tc -sites.file beagle-sites.xml ftdock.swift -n=100 -list=pdb.list.100 -grid=0.5 >std.out 2>std.err < /dev/null &

#short run for testing purpose
swift -config cf -tc.file tc -sites.file sites-beagle-small.xml ftdock.swift -n=1 -list=pdb.list.1 -grid=10

#execution for production runs
#SWIFT_HEAP_MAX=6000M swift -config cf -tc.file tc -sites.file sites-beagle.xml ftdock.swift -n=100 -list=pdb.list.100 -grid=0.5 >std.out 2>std.err
