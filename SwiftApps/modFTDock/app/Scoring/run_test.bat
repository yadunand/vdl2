#!/bin/bash

# destroy results files
rm 2RNK.dat 2RNK.res.gz

# prepare DAT files
tar xvf some_dat.tar

# make the run
time ./scoredat.bat 2RNK
