#!/bin/bash
#$ -S /bin/bash
#$ -j y

##### IMPORTANT NOTICE #####
# run only one instance per node
#  as many would use the same files and crash one another...

#  * to run a test (takes > 1h):
#  ./run_test.bat
#  * to run a shorter test (about 1 minute):
#  ./run_short.bat

############################



##   $1 is a protein      : 1jos-A
## qsub -cwd -pe threaded 3  ~/Database/scoredat.bat 1jos-A
## for i in `ls ????-?_???.dat | cut -c 1-6 | sort | uniq`; do qsub -cwd -pe threaded 3  ~/Database/scoredat.bat  ${i}; done

## NB we use "-pe threaded 3" because nodes on godzilla have 4-cores
##  hence prevents more than 2 of these to run on same node



##################################################
# prolog
#

echo ""
## useful to know which computer did the job!
uname -a
echo "processing res file for: " ${1}



##################################################
# make local copies for executables/data
# because they are used extensively

for fullpath in ./build1 ./scoredat.exe ./distances300r.exe ./eval300.exe  ./tRNA_score.exe
do

 myfile="${fullpath##*/}"
  mydir="${fullpath:0:${#fullpath} - ${#filename}}"

if [ ! -e /tmp/${myfile} ] || [ ${fullpath} -nt /tmp/${myfile} ]
then
  # copy if file not there or if
  # the source one is newer than the local copy
  cp --verbose ${fullpath} /tmp/${myfile}
fi

done



##################################################
# merge .dat files into one
#

if [ -e ${1}.dat ]
then
    echo ${1} " already merged!"
else
    ## proceed with computation
    ## this is where the 100 dat files are
    ## merged into 1 single large file
    ls ${1}_???.dat | wc -l
    ./modmerge ./ "${1}_???.dat" > ${1}.dat
fi
rm -f ${1}_???.dat



##################################################
# make the .res file
#

if [ -e ${1}.res.gz ]
then
    echo ${1} " already resed!"
elif [ -e ${1}.dat ]
then
  ## this will also reduce the nfsd traffic
  ## copy over the local versions

  ## copy the DAT file
  cp --verbose ${1}.dat /tmp/
  ## copy the static PDB file
  cp --verbose `grep -h "Static molecule" ${1}*.dat | head -1 | awk '{print $4}'` /tmp/
  ## copy the mobile PDB file
  cp --verbose `grep -h "Mobile molecule" ${1}*.dat | head -1 | awk '{print $4}'` /tmp/

  ## proceed with computation
  ##  remember that all execs/data are local
  echo "computing res file..."
  /tmp/scoredat.exe /tmp/distances300r.exe /tmp/eval300.exe /tmp/${1}.dat  /tmp/tRNA_score.exe > /tmp/${1}.res

  gzip -9 /tmp/${1}.res
  mv /tmp/${1}.res.gz ${1}.res.gz
  echo "done with " ${1}

  ## cleanup
  rm /tmp/${1}.dat
fi
