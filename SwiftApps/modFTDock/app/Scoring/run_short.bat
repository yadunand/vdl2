#!/bin/bash

# destroy results files
rm 2RNK.dat 2RNK.res.gz

# prepare DAT files
tar xvf some_dat.tar
rm 2RNK_0{1,2,3,4,5,6,7,8,9}?.dat
rm 2RNK_00{1,2,3,4,5,6,7,8,9}.dat

# make the run
time ./scoredat.bat 2RNK
