#! /bin/bash

oomlog=/tmp/oom.log
#oomlog=/lustre/beagle/$USER/oom.log
host=$(hostname -s)
date=$(date)

lowmem=4000
verylowmem=2000
maxtries=10
startsleep=1

version=$1
shift

# if mem is low, wait for it to recover before starting

for i in $(seq 0 $maxtries); do
  freeMB=$(free -m | grep cache: | awk '{print $4}')
  if [ $freeMB -lt $lowmem ]; then
    if [ $i = $maxtries ]; then
      echo "$host $(date) freeMB = $freeMB below yellow mark $lowmem after $maxtries $startsleep sec pauses. Exiting." >>$oomlog
      exit 7
    else
      echo "$host $(date) freeMB = $freeMB below yellow mark $lowmem on try $i. Sleeping $startsleep sec." >>$oomlog
      sleep $startsleep
    fi
  else
    break
  fi
done

#/lustre/beagle/wilde/mp/mp01/app/modftdock${version} $* &
#Put the path of your own modftdock location
/home/ketan/SwiftApps/modFTdock/app/modftdock${version} $* &
dockpid=$!

# watch for mem dropping very low - kill dock process if below red mark

while true; do
  sleep 1
  freeMB=$(free -m | grep cache: | awk '{print $4}')
  if [ $freeMB -lt $verylowmem ]; then
      kill -9 $dockpid
      echo "$host $(date) freeMB = $freeMB below red mark $verylowmem. Killed dock process $pid." >>$oomlog
      break;
  fi
done &

monitorpid=$!

wait $dockpid
rc=$?

kill $monitorpid

if [  -d output ]; then
    echo "output dir exists"
else
    mkdir -p output # FIXME: this mkdir should not be needed when running under swift, but is needed for standalone tests
fi

if [ -e *.dat ]; then
    mv *.dat output/
fi

if [ -e *.txt ]; then
   mv *.txt output/
fi
#exit
exit $rc

