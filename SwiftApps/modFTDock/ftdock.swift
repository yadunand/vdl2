type file_pdb;
type file_dat;

app (file_dat dat_file)
  do_one_dock(
      string        param_root,
      string        param_modulo,
      file_pdb param_file_static,
      file_pdb param_file_mobile )
{
  modftdock 32
            "-modulo" @param_modulo
            "-root"   @param_root
            "-static" @param_file_static
            "-mobile" @param_file_mobile
            "-calculate_grid" @arg("grid","2.5")
            "-angle_step" "10"
            "-keep" "10"
            "-noelec";
}

string modulus = @arg("m","100");
string str_roots[] = readData( @arg( "list" ) );

foreach str_root in str_roots
{
  trace( str_root );

  string str_file_static = @strcat( @arg("in", "input/"), str_root, ".pdb" );
  string str_file_mobile = "input/4TRA.pdb";

  file_pdb file_static < single_file_mapper; file = str_file_static >;
  file_pdb file_mobile < single_file_mapper; file = str_file_mobile >;
  file_dat dat_files[] < simple_mapper;
                              padding = 3,
                              location = @arg("out", "output"),
			      prefix  = @strcat( str_root, "_" ),
			      suffix  = ".dat" >;

  // break docking jobs + do 'em in parallel
  int n = @toint(@arg("n","1"));
  foreach mod_index in [0:n-1]
  {
    string str_modulo = @strcat(mod_index, ":", modulus);
    dat_files[ mod_index ] = do_one_dock( str_root,
					  str_modulo,
					  file_static,
					  file_mobile );
  }
}

