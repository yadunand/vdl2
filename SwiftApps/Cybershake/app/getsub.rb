#!/home/ketan/ruby-install/bin/ruby

require 'optparse'

options = {}
OptionParser.new do |opts|
  opts.on("-l", "-location") { |v| options[:location] = v}
  opts.on("-n", "-sitename") { |v| options[:sitename] = v}
  opts.on("-s", "-source") { |v| options[:source] = v}
  opts.on("-r", "-rupture") { |v| options[:rupture] = v}
end.parse!

puts "x #{options[:location]}/#{options[:sitename]}_#{options[:source]}_#{options[:rupture]}_subfx.sgt"
puts "y #{options[:location]}/#{options[:sitename]}_#{options[:source]}_#{options[:rupture]}_subfy.sgt"

