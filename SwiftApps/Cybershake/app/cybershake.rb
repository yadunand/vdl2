#!/home/ketan/ruby-install/bin/ruby

require 'active_record'
require 'logger'

ActiveRecord::Base.establish_connection(
  :adapter => "mysql",
  :database => "CyberShake",
  :host => "focal.usc.edu",
  :username => "cybershk_ro",
  :password => "CyberShake2007"
)
#ActiveRecord::Base.logger = Logger.new(STDERR)
require 'composite_primary_keys'

class Site < ActiveRecord::Base
  set_table_name "CyberShake_Sites"
  set_primary_key "CS_Site_ID"

  def name
    read_attribute(:CS_Short_Name)
  end

  def lat
    read_attribute(:CS_Site_Lat)
  end

  def lon
    read_attribute(:CS_Site_Lon)
  end
end

class Run < ActiveRecord::Base
  set_table_name "CyberShake_Runs"
  set_primary_keys "Site_ID", "ERF_ID"
  belongs_to :site, :foreign_key => "Site_ID"
  has_many :ruptures, :foreign_key => ["CS_Site_ID", "ERF_ID"]

  def erf
    read_attribute(:ERF_ID)
  end

  def variation_scenario
    read_attribute(:Rup_Var_Scenario_ID)
  end
end

class Rupture < ActiveRecord::Base
  set_table_name "CyberShake_Site_Ruptures"

  def source
    read_attribute(:Source_ID)
  end

  def index
    read_attribute(:Rupture_ID)
  end
end

class Variation < ActiveRecord::Base
  set_table_name "Rupture_Variations"
  set_primary_key "Rup_Var_ID"

  def self.digest
    Digest::MD5.hexdigest(self.to_s).to_s
  end

  def self.columns=(cached_columns)
    @cached_columns= cached_columns
    def self.columns
      @cached_columns
    end
    @cached_columns
  end
end
require 'memcached'
require 'digest/md5'
cache = Memcached.new
begin
  Variation.columns = cache.get(Variation.digest)
rescue Memcached::NotFound
  Variation.columns
  cache.set(Variation.digest, Variation.columns)
end
cache = nil
