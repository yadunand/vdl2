#!/home/ketan/ruby-install/bin/ruby

$LOAD_PATH << "/scratch/local/ketan/cybershake"
require 'cybershake'
require 'memcached'
require 'digest/md5'

cache = Memcached.new

runid = ARGV[0].to_i
x = nil
begin
  x = cache.get(runid.to_s)
rescue Memcached::NotFound
  x = Run.find_by_Run_ID(runid)
  cache.set(runid.to_s, x)
end
digest = Digest::MD5.hexdigest(x.to_yaml).to_s + "site"
begin
  x.site = cache.get(digest)
rescue Memcached::NotFound
  x.site
  cache.set(digest,x.site)
end
puts "name lat lon erf variation_scenario"
puts "#{x.site.name} #{x.site.lat} #{x.site.lon} #{x.erf} #{x.variation_scenario}"
