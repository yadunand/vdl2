#!/home/ketan/ruby-install/bin/ruby

$LOAD_PATH << "/scratch/local/ketan/cybershake"
require 'cybershake'
require 'memcached'
require 'digest/md5'

runid = ARGV[0].to_i
x = Run.find_by_Run_ID(runid)
puts "source index size"

digest = Digest::MD5.hexdigest(x.to_yaml).to_s
cache = Memcached.new

begin
  x.ruptures = cache.get(digest)
rescue Memcached::NotFound
  x.ruptures
  cache.set(digest, x.ruptures)
end

x.ruptures.each { |rup|
  options = {}
  options[:erf] = x.erf 
  options[:variation_scenario] = x.variation_scenario
  options[:source] = rup.source
  options[:rupture] = rup.index
  digest = Digest::MD5.hexdigest(options.to_s).to_s

  begin
    variations = cache.get(digest)
  rescue Memcached::NotFound
    variations = Variation.find(:all, :conditions => {
        :Rup_Var_Scenario_ID => options[:variation_scenario],
        :ERF_ID => options[:erf], 
        :Source_ID => options[:source], :Rupture_ID => options[:rupture] },
        :order => "Rup_Var_ID")
    cache.set(digest, variations)
  end
  len = variations.size
  puts "#{rup.source} #{rup.index} #{len}"
}

