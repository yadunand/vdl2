#!/home/ketan/ruby-install/bin/ruby

require 'optparse'

options = {}
OptionParser.new do |opts|
  opts.on("-r", "-runid") { |v| options[:runid] = v}
  opts.on("-l", "-location") { |v| options[:location] = v}
  opts.on("-s", "-sitename") { |v| options[:sitename] = v}
end.parse!

if options[:location] == "NA" then
  puts "x #{options[:sitename]}/#{options[:sitename]}_fx_#{options[:runid]}.sgt"
  puts "y #{options[:sitename]}/#{options[:sitename]}_fy_#{options[:runid]}.sgt"
else
  puts "x #{options[:location]}/#{options[:sitename]}/#{options[:sitename]}_fx_#{options[:runid]}.sgt"
  puts "y #{options[:location]}/#{options[:sitename]}/#{options[:sitename]}_fy_#{options[:runid]}.sgt"
end
