#!/bin/bash

if [ $# -lt 2 ]; then
        echo "Usage: $0 <directory> <output_file>"
        exit -1
fi

DIR=$1
OUT=$2

find $DIR -name '*.grm' | zip -v $OUT -@

