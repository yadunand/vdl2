#!/bin/bash

export OSG_APP=/home/aespinosa/Documents/cybershake/osg_simulated

EXTRACT=$OSG_APP/engage/scec/JBSim3d/bin/jbsim3d
SEISMOGRAM=$OSG_APP/engage/scec/JBSim3d/bin/jbsim3d
PEAKCALC=$OSG_APP/engage/scec/SpectralAcceleration/p2utils/surfseis_rspectra

SGT_X=$OSG_DATA/engage/scec/LGU/LGU_fx_664.sgt
SGT_Y=$OSG_DATA/engage/scec/LGU/LGU_fy_664.sgt

$EXTRACT ${@:1:9}
#$EXTRACT $1 $2 $3 $4 $5 sgt_xfile=$SGT_X sgt_yfile=$SGT_Y $8 $9
$SEISMOGRAM ${@:10:11}
$PEAKCALC ${@:21:12}
