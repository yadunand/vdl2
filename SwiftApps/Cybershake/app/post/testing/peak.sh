/home/ketan/cybershake/post/SpectralAcceleration/p2utils/surfseis_rspectra \
  simulation_out_pointsX=2 simulation_out_pointsY=1 \
  simulation_out_timesamples=3000 simulation_out_timeskip=0.1 \
  surfseis_rspectra_seismogram_units=cmpersec \
  surfseis_rspectra_output_units=cmpersec2 surfseis_rspectra_output_type=aa \
  surfseis_rspectra_period=all surfseis_rspectra_apply_filter_highHZ=5.0 \
  surfseis_rspectra_apply_byteswap=no  \
  in=Seismogram_TEST_218_256_127.grm out=PeakVals_TEST_218_256_127.bsa
