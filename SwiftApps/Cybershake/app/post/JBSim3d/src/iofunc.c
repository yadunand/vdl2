#include        "include.h"
#include        "structure.h"
#include        "function.h"

FILE *fopfile(char *name,char *mode)
{
FILE *fp;

if((fp = fopen(name,mode)) == NULL)
   {
   fprintf(stderr,"CAN'T FOPEN FILE = %s, MODE = %s\n", name, mode);
   exit(-1);
   }
return(fp);
}

int opfile_ro(char *name)
{
int fd;
if ((fd = open (name, O_RDONLY, 0444)) == -1)
   fprintf (stderr, "CAN'T OPEN FILE %s\n", name);
return (fd);
}

int opfile(char *name)
{
int fd;
if ((fd = open (name, O_RDWR, 0664)) == -1)
   fprintf (stderr, "CAN'T OPEN FILE %s\n", name);
return (fd);
}

int croptrfile(char *name)
{
int fd;
if ((fd = open (name, O_CREAT | O_TRUNC | O_RDWR, 0664)) == -1)
   fprintf (stderr, "CAN'T OPEN FILE %s\n", name);
return (fd);
}

int reed(int fd, void *pntr, int length)
{
int temp;
if ((temp = read(fd, pntr, length)) < length)
   {
   fprintf (stderr, "READ ERROR\n");
   fprintf (stderr, "%d attempted  %d read\n", length, temp);
   exit(-1);
   }
return(temp);
}

int rite(int fd, void *pntr, int length)
{
int temp;
if ((temp = write(fd, pntr, length)) < length)
   {
   fprintf (stderr, "WRITE ERROR\n");
   fprintf (stderr, "%d attempted  %d written\n", length, temp);
   exit(-1);
   }
return(temp);
}

void *check_realloc(void *ptr,size_t len)
{
ptr = (char *) realloc (ptr,len);

if(ptr == NULL)
   {
   fprintf(stderr,"*****  memory reallocation error\n");
   exit(-1);
   }

return(ptr);
}

void *check_malloc(size_t len)
{
char *ptr;

ptr = (char *) malloc (len);
 
if(ptr == NULL)
   {
   fprintf(stderr,"*****  memory allocation error\n");
   exit(-1);
   }
 
return(ptr);
}

void fortran_rite(int fd,int nargs, ...)
{
va_list ap;
void *ptr[MAX_VAR_LIST];
int len[MAX_VAR_LIST];
int totlen = 0;
int i;

va_start(ap,nargs);
for(i=0;i<nargs;i++)
   {
   ptr[i] = va_arg(ap,void *);
   len[i] = va_arg(ap,int);
   totlen = totlen + len[i];
   }
va_end(ap);

rite(fd,&totlen,sizeof(int));

for(i=0;i<nargs;i++)
   rite(fd,ptr[i],len[i]);

rite(fd,&totlen,sizeof(int));
}

void write_seis_ascii(char* sname, char* comp, float *st, float *dt, int nt, float *ts, char* filename) {
  char stitle[128], header[128];
  int i, j, nt6;
  char* full_filename = malloc(sizeof(char) * (strlen(filename) + strlen(comp) + 2));  full_filename = strcpy(full_filename, filename);
  full_filename = strcat(full_filename, ".");
  full_filename = strcat(full_filename, comp);
  FILE* fpw = fopfile(full_filename,"w");

  sprintf(stitle,"%-10s%3s %s\n",sname,comp,"TITLE");
  fprintf(fpw,"%s",stitle);

  sprintf(header,"%d %12.5e 0 0 %12.5e 0.0 0.0 0.0\n",nt,*dt,*ts);
  fprintf(fpw,"%s",header);

  for (i=0; i<nt; i++) {
	fprintf(fpw,"%13.5e",st[i]);
	if ((i+1)%6==0) {
	  fprintf(fpw,"\n");
	}
  }
  if (nt%6!=0) {
	fprintf(fpw,"\n");
  }
  fflush(fpw);
  fclose(fpw);
  free(full_filename);
}

void write_seis_binary(float* st, float* dt, int nt, float* ts, char* filename) {
  int written;
  char outfile[256];
  FILE* fpout;
  //sprintf(outfile, "%s.%s", filename, comp);
  fpout = fopfile(filename, "wb");
  written = fwrite(st, sizeof(float), nt, fpout);
  if (written!=nt) {
	fprintf(stderr, "Error in writing to file %s; wrote %d instead of %d floats.\n", filename, written, nt);
	exit(-2);
  }
  fflush(fpout);
  fclose(fpout);
}

void write_seis(char* filename, char *sname,char *comp,float *st,float *dt,int nt,float *ts, int output_binary) {
  FILE *fopfile(), *fpw;
  int i, j, nt6;
  //char outfile[256];
  
  //sprintf(outfile,"%s.%s",dir,stat,comp);
  
  if (!output_binary) {
	write_seis_ascii(sname, comp, st, dt, nt, ts, filename);
  } else {
	write_seis_binary(st, dt, nt, ts, filename);
  }
}


char *skipval(int j,char *str)
{
while(str[0] == ' ' || str[0] == '\t' || str[0] == '\b' || str[0] == '\n')
   str++;

while(j--)
   {
   while(str[0] != ' ' && str[0] != '\t' && str[0] != '\b' && str[0] != '\n')
      str++;

   while(str[0] == ' ' || str[0] == '\t' || str[0] == '\b' || str[0] == '\n')
      str++;
   }

return(str);
}

void makedir(char *ipath)
{
struct stat sbuf;
char stmp[256], str[128], path[1024];
int rtn, j;
mode_t mode = 00777;

strcpy(path,ipath);

j = 0;
while(path[j] != '\0')
   j++;

j--;
while(path[j] == '/')
   j--;
path[j+1] = '\0';
path[j+2] = '\0';

j = 0;
while(path[j] != '\0')
   {
   while(path[j] != '/' && path[j] != '\0')
      j++;

   if(j != 0)
      {
      strncpy(stmp,path,j);
      stmp[j] = '\0';

      rtn = stat(stmp,&sbuf); /* stat directory path to see if it already exists */

      if(rtn == -1 && errno == ENOENT) /* try to make the directory path */
         {
         rtn = mkdir(stmp,mode);

         if(rtn == -1)
            {
            if(errno != EEXIST)
               {
               sprintf(str,"makedir() cannot make directory %s, exiting",stmp);
               perror(str);
               exit(-1);
               }
            }
         }

      else if(rtn == -1 && errno != ENOENT) /* some other problem */
         {
         sprintf(str,"problem with stat() on %s, exiting",stmp);
         perror(str);
         exit(-1);
         }
      }
   j++;
   }

/*
   Double check to make sure directory exists.  This is a brute-force
   method, but I ran inot problems with automounted directories using the
   error-checking above.  RWG 9/20/99
*/

rtn = mkdir(stmp,mode);
if(rtn == -1 && errno != EEXIST)
   {
   sprintf(str,"makedir() cannot make directory %s, exiting",stmp);
   perror(str);
   exit(-1);
   }

}
