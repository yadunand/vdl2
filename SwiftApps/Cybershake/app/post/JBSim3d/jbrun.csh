#! /bin/csh

set BINDIR = bin

set STATS = ( USC )
set STATFILE = ../StatInfo/cybersites.ll

set OUTDIR = Vel0.5

set RUPDIR = ../../ruptures/Srf
set RUPMODFILE = hart_sgm-2m.10x10_rvf0.8.srf

set SGT_MAIN_GFDIR = ../../data/SgtFiles
set SGT_LOCAL_GFDIR = ../../data/ExtractSgtFiles

set SEIS_FILE = USC_hart_2.grm

\mkdir -p $SGT_LOCAL_GFDIR $OUTDIR

set s = 0
foreach stat ( $STATS )
@ s ++

#set slon = `gawk -v s=$stat 'BEGIN{x=-999.;}{if($3==s)x=$1;}END{print x;}' $STATFILE `
#set slat = `gawk -v s=$stat 'BEGIN{x=-999.;}{if($3==s)x=$2;}END{print x;}' $STATFILE `
set slon = -118.286 
set slat =  34.0192

$BINDIR/jbsim3d stat=$stat slon=$slon slat=$slat \
                   rupmodfile=$RUPDIR/$RUPMODFILE \
                   sgt_xfile=${SGT_MAIN_GFDIR}/${stat}_fx.sgt \
                   sgt_yfile=${SGT_MAIN_GFDIR}/${stat}_fy.sgt \
                   #sgt_zfile=${SGT_MAIN_GFDIR}/${stat}_fz.sgt \
                   extract_sgt=1 \
                   sgtdir=$SGT_LOCAL_GFDIR \
                   extract_sgt_xfile=${stat}_fx-sub001.sgt \
                   extract_sgt_yfile=${stat}_fy-sub001.sgt \
                   #extract_sgt_zfile=${stat}_fz-sub001.sgt

$BINDIR/jbsim3d stat=$stat slon=$slon slat=$slat \
                outdir=$OUTDIR \
                   rupmodfile=$RUPDIR/$RUPMODFILE \
                   sgt_xfile=${SGT_LOCAL_GFDIR}/${stat}_fx-sub001.sgt \
                   sgt_yfile=${SGT_LOCAL_GFDIR}/${stat}_fy-sub001.sgt \
                   #sgt_zfile=${SGT_LOCAL_GFDIR}/${stat}_fz-sub001.sgt \
				   seis_file=${SEIS_FILE}

end
