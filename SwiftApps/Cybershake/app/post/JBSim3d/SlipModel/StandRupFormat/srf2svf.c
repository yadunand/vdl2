#include "include.h"
#include "structure.h"
#include "function.h"
#include "defs.h"

main(int ac,char **av)
{
int nseg;
char infile[256], type[64], outfile[256];

struct standrupformat srf;

int inbin = 0;
float maxslip;

sprintf(infile,"stdin");
sprintf(outfile,"stdout");
sprintf(type,"slip");
nseg = 0;

setpar(ac,av);
getpar("infile","s",infile);
getpar("outfile","s",outfile);
getpar("type","s",type);
getpar("nseg","d",&nseg);
getpar("inbin","d",&inbin);
endpar();

read_srf(&srf,infile,inbin);
write_maxsvf(outfile,&srf,type,nseg,&maxslip);
fprintf(stderr,"maxslip= %f\n",maxslip);
}
