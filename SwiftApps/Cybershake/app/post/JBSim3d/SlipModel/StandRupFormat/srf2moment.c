#include "include.h"
#include "structure.h"
#include "function.h"
#include "defs.h"

main(int ac,char **av)
{
char infile[256], velfile[256];

struct standrupformat srf;
struct velmodel vmod;

int inbin = 0;

sprintf(infile,"stdin");

setpar(ac,av);
getpar("infile","s",infile);
mstpar("velfile","s",velfile);
endpar();

read_velmodel(velfile,&vmod);
read_srf(&srf,infile,inbin);
get_moment(&srf,&vmod);
}
