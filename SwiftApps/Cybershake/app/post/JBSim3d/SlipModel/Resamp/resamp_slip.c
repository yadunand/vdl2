#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/file.h>
#include <sys/procfs.h>
#include <sys/resource.h>
#include <sys/signal.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <sys/types.h>

#define MAXTOL 0.01
#define         RPERD           0.017453293

struct complex
   {
   float re;
   float im;
   };

void *check_malloc(int);
FILE *fopfile(char*, char*);

void fft2d(struct complex *, int, int, int,float *,float *);
void resamp(struct complex *,int,int,struct complex *,int,int,float *,float *,float *,float *,long *,float *,float *);

void resample_slip(float *,int,int,float,float,float *,int,int,float,float);
void smooth_slip(float *,int,int,float *);

double frand(void);
double Xsfrand(long *);
double sfrand(long *);
double gaus_rand(float *,float *,long *);

main(int ac,char **av)
{
FILE *fpr, *fpw;
struct complex *slip0, *slip1;
float *rslip, flen, fwid, dx, dy, dx2, dy2, amp0, amp, lamp;
float dkx, dky, kx, ky, xx, yy;
int nx, ny, nxfine, nyfine, nx2, ny2;
int i, j, k, l, ip0, ipf, ip, i0, j0;
char infile[256], slipfile[256], specfile[256], str[512];
char lisafile[512], lhead0[512], lhead1[512], lhead2[512];

float xl = -1.0;
float yl = -1.0;

float *s1, *s2;
int smooth = 0;
int lisaformat = 0;
int norm2one = 1;
int npad = 0;

float mech[2], rake[2], *rake0, ss, ds;
int nmech = 1;

float pi = 3.14159265;

long seed = 0;

float dxcorner = -1.0;
float dycorner = -1.0;

float default_rake = 0.0;

rake[0] = 0.0;
rake[1] = 0.0;

lisafile[0] = '\0';

setpar(ac,av);
mstpar("infile","s",infile);
mstpar("slipfile","s",slipfile);
mstpar("specfile","s",specfile);
mstpar("nxfine","d",&nxfine);
mstpar("nyfine","d",&nyfine);
getpar("xl","f",&xl);
getpar("yl","f",&yl);
getpar("dxcorner","f",&dxcorner);
getpar("dycorner","f",&dycorner);
getpar("seed","d",&seed);
getpar("npad","d",&npad);
getpar("smooth","d",&smooth);
getpar("norm2one","d",&norm2one);
getpar("lisaformat","d",&lisaformat);
if(lisaformat)
   {
   mstpar("flen","f",&flen);
   mstpar("fwid","f",&fwid);
   mstpar("nx","d",&nx);
   mstpar("ny","d",&ny);
   getpar("lisafile","s",lisafile);
   mstpar("rake1","f",&rake[0]);
   getpar("nmech","d",&nmech);
   if(nmech == 2)
      mstpar("rake2","f",&rake[1]);
   getpar("default_rake","f",&default_rake);
   }
endpar();

fpr = fopfile(infile,"r");

if(lisaformat)
   {
   slip0 = (struct complex *) check_malloc (nx*ny*sizeof(struct complex));
   rake0 = (float *) check_malloc (nx*ny*sizeof(float));

   fgets(str,512,fpr);
   fgets(str,512,fpr);
   fgets(lhead0,512,fpr);
   fgets(lhead1,512,fpr);
   fgets(lhead2,512,fpr);

   for(i=0;i<nx;i++)
      {
      for(j=0;j<ny;j++)
         {
         ip0 = i + j*nx;

	 ss = 0.0;
	 ds = 0.0;
	 mech[1] = 0.0;
	 for(k=0;k<nmech;k++)
	    {
            fscanf(fpr,"%f",&mech[k]);
	    ss = ss + mech[k]*cos(rake[k]*RPERD);
	    ds = ds + mech[k]*sin(rake[k]*RPERD);
	    }

	 if(ss != 0.0)
	    {
	    rake0[ip0] = (atan(ds/ss))/RPERD;
	    if(ss < 0.0)
	       rake0[ip0] = rake0[ip0] + 180.0;
	    }
	 else if(ds >= 0.0)
	    rake0[ip0] = 90.0;
	 else
	    rake0[ip0] = -90.0;

         if(ss == 0.0 && ds == 0.0)
            rake0[ip0] = default_rake;

         slip0[ip0].re = sqrt(mech[0]*mech[0] + mech[1]*mech[1]);
         slip0[ip0].re = sqrt(ss*ss + ds*ds);
	 slip0[ip0].im = 0.0;
         }
      }
   }
else
   {
   fscanf(fpr,"%f %f",&flen,&fwid);
   fscanf(fpr,"%d",&nx);
   fscanf(fpr,"%d",&ny);

   slip0 = (struct complex *) check_malloc (nx*ny*sizeof(struct complex));

   for(ip0=0;ip0<nx*ny;ip0++)
      {
      fscanf(fpr,"%f",&slip0[ip0].re);
      slip0[ip0].im = 0.0;
      }
   }

fclose(fpr);

if(xl < 0.0)
   xl = flen;
if(yl < 0.0)
   yl = fwid;

dx = flen/nx;
dy = fwid/ny;

dx2 = dx/nxfine;
dy2 = dy/nyfine;

nx2 = nx*nxfine;
ny2 = ny*nyfine;

dkx = 1.0/(nx2*dx2);
dky = 1.0/(ny2*dy2);

dxcorner = dx/dxcorner;
if(dxcorner < 0.0)
   dxcorner = 1;

dycorner = dy/dycorner;
if(dycorner < 0.0)
   dycorner = 1;

fprintf(stderr,"nx=%4d ny=%4d dx=%12.5e dy=%12.5e\n",nx,ny,dx,dy);
fprintf(stderr,"              dxcorner=%12.5f dycorner=%12.5f\n",dxcorner,dycorner);
fprintf(stderr,"nx2=%4d ny2=%4d dx2=%12.5e dy2=%12.5e\n",nx2,ny2,dx2,dy2);

slip1 = (struct complex *) check_malloc (nx2*ny2*sizeof(struct complex));

fft2d(slip0,nx,ny,-1,&dx,&dy);

resamp(slip0,nx,ny,slip1,nx2,ny2,&dkx,&dky,&xl,&yl,&seed,&dxcorner,&dycorner);

/* truncate any negative slip values => should double check that spectra is
   not changed too much  */

if(xl < 1.0e+14 && yl < 1.0e+14)
   {
   fft2d(slip1,nx2,ny2,1,&dkx,&dky);
   for(j=0;j<ny2*nx2;j++)
      {
      if(slip1[j].re < 0.0)
         slip1[j].re = 1.0e-04;
      }
   fft2d(slip1,nx2,ny2,-1,&dx2,&dy2);
   }
/*
*/

fpw = fopfile(specfile,"w");

fprintf(fpw,"dkx=%14.7e dky=%14.7e kxmax=%12.5e kymax=%12.5e\n",dkx,dky,0.5/dx2,0.5/dy2);

amp0 = 1.0;
if(norm2one)
   amp0 = dx2*dy2*sqrt(slip1[0].re*slip1[0].re + slip1[0].im*slip1[0].im);

if(amp0 == 0.0)
   amp0 = 1.0;

for(j=0;j<=ny2/2;j++)
   {
   if(j<=ny2/2)
      ky = j*dky;
   else
      ky = (j-ny2)*dky;

   for(i=0;i<=nx2/2;i++)
      {
      if(i<=nx2/2)
         kx = i*dkx;
      else
         kx = (i-nx2)*dkx;

      ip = i + j*nx2;

      /* dx2*dy2 for FFT normalization */
      amp = dx2*dy2*sqrt(slip1[ip].re*slip1[ip].re + slip1[ip].im*slip1[ip].im);
      amp = amp/amp0;

      lamp = -1.e+20;
      if(amp > 0.0)
	 lamp = log10(amp);

      fprintf(fpw,"%14.7e %14.7e %12.5e %12.5e\n",kx,ky,lamp,amp);
      }
   }
fclose(fpw);

/*
for(j=0;j<ny2;j++)
   {
   for(i=0;i<nx2;i++)
      {
      fprintf(stderr,"%12.5e %12.5e\n",slip1[i + j*nx2].re,slip1[i+j*nx2].im);
      }
   }
*/

fft2d(slip1,nx2,ny2,1,&dkx,&dky);

fpw = fopfile(slipfile,"w");

for(j=0;j<ny2;j++)
   {
   yy = (j+0.5)*dy2;
   for(i=0;i<nx2;i++)
      {
      xx = (i+0.5)*dx2;
      fprintf(fpw,"%12.5e %12.5e %12.5e\n",xx,yy,slip1[i + j*nx2].re);
      }
   }
fclose(fpw);

if(lisaformat && lisafile[0] != '\0')
   {
   fpw = fopfile(lisafile,"w");

   fprintf(fpw,"RESAMPLED lisa format slipmodel from file \"%s\"\n",infile);
   if(nmech == 1)
      fprintf(fpw,"nx= %d ny=%d\n",nx2,ny2);
   else
      fprintf(fpw,"nx= %d ny=%d nmech= %d (%3.0f %3.0f)\n",nx2,ny2,nmech,rake[0],rake[1]);

   fprintf(fpw,"%s",lhead0);
   fprintf(fpw,"%s",lhead1);
   fprintf(fpw,"%s",lhead2);

   for(i=0;i<nx2;i++)
      {
      i0 = i/nxfine;

      for(j=0;j<ny2;j++)
         {
         j0 = j/nyfine;
	 ip0 = i0 + j0*nx;
	 ip = i + j*nx2;

         fprintf(fpw,"%8.0f",slip1[ip].re*cos((rake0[ip0]-rake[0])*RPERD));
	 if(nmech == 2)
            fprintf(fpw,"%8.0f",slip1[ip].re*sin((rake0[ip0]-rake[0])*RPERD));

         fprintf(fpw,"\n");
         }
      }
   fclose(fpw);
   }
}

void resamp(struct complex *s0,int nx0,int ny0,struct complex *s1,int nx1,int ny1,float *dkx,float *dky,float *xl,float *yl,long *seed,float *dxc,float *dyc)
{
int i, j, ip0, ip1;
int i1, j1, nxc2, nyc2;
float kx, ky, amp, amp0, phs, xl2, yl2;

float pi = 3.14159265;

amp0 = sqrt(s0[0].re*s0[0].re + s0[0].im*s0[0].im);
if(*xl > 1.0e+14 || *yl > 1.0e+14)
   amp0 = 0.0;

xl2 = (*xl)*(*xl);
yl2 = (*yl)*(*yl);

nxc2 = nx0/2;
nyc2 = ny0/2;

nxc2 = (int)(0.5*nx0*(*dxc) + 0.5);
if(nxc2 > nx0/2)
   nxc2 = nx0/2;

nyc2 = (int)(0.5*ny0*(*dyc) + 0.5);
if(nyc2 > ny0/2)
   nyc2 = ny0/2;

fprintf(stderr,"%d %d\n",nxc2,nyc2);

for(j=0;j<ny0;j++)
   {
   if(j <= ny0/2)
      j1 = j;
   else
      j1 = ny1 - (ny0 - j);

   for(i=0;i<nx0;i++)
      {
      if(i <= nx0/2)
         i1 = i;
      else
         i1 = nx1 - (nx0  - i);

      ip0 = i + j*nx0;
      ip1 = i1 + j1*nx1;

      s1[ip1].re = s0[ip0].re;
      s1[ip1].im = s0[ip0].im;
      }

   if(j1 <= ny1/2)
      ky = j1*(*dky);
   else
      ky = (j1-ny1)*(*dky);

   for(i1=nxc2+1;i1<=nx1-nxc2;i1++)
      {
      if(i1 <= nx1/2)
         kx = i1*(*dkx);
      else
         kx = (i1-nx1)*(*dkx);

      amp = kx*kx*xl2 + ky*ky*yl2;
      amp = amp0/sqrt(1.0 + amp*amp);

      phs = pi*sfrand(seed);

      ip1 = i1 + j1*nx1;

      s1[ip1].re = amp*cos(phs);
      s1[ip1].im = amp*sin(phs);
      }
   }

for(j1=nyc2+1;j1<=ny1-nyc2;j1++)
   {
   if(j1 <= ny1/2)
      ky = j1*(*dky);
   else
      ky = (j1-ny1)*(*dky);

   for(i1=0;i1<nx1;i1++)
      {
      if(i1 <= nx1/2)
         kx = i1*(*dkx);
      else
         kx = (i1-nx1)*(*dkx);

      amp = kx*kx*xl2 + ky*ky*yl2;
      amp = amp0/sqrt(1.0 + amp*amp);

      phs = pi*frand();

      ip1 = i1 + j1*nx1;

      s1[ip1].re = amp*cos(phs);
      s1[ip1].im = amp*sin(phs);
      }
   }
}

void fft2d(struct complex *xc,int n1,int n2,int isgn,float *d1,float *d2)
{
int i, j, ip;
float *space;
struct complex *xtc;
float normf;

normf = (*d1)*(*d2);

space = (float *) check_malloc (2*(n1+n2)*sizeof(float));

for(j=0;j<n2;j++)
   fourg_(xc+j*n1,&n1,&isgn,space);

xtc = (struct complex *) check_malloc (n2*sizeof(struct complex));

for(i=0;i<n1;i++)
   {
   for(j=0;j<n2;j++)
      {
      ip = i + j*n1;

      xtc[j].re = xc[ip].re;
      xtc[j].im = xc[ip].im;
      }

   fourg_(xtc,&n2,&isgn,space);

   for(j=0;j<n2;j++)
      {
      ip = i + j*n1;

      xc[ip].re = normf*xtc[j].re;
      xc[ip].im = normf*xtc[j].im;
      }
   }

free(space);
free(xtc);
}

FILE *fopfile(char *name,char *mode)
{
FILE *fp;

if((fp = fopen(name,mode)) == NULL)
   {
   fprintf(stderr,"CAN'T FOPEN FILE = %s, MODE = %s\n", name, mode);
   exit(-1);
   }
return(fp);
}

void *check_malloc(int len)
{
void *ptr;

ptr = (void *) malloc (len);

if(ptr == NULL)
   {
   fprintf(stderr,"*****  memory allocation error\n");
   exit(-1);
   }

return(ptr);
}

void resample_slip(float *rslip,int nx,int ny,float dx,float dy,float *slip,int nx0,int ny0,float dx0,float dy0)
{
int ix, iy, ip, ip0, ix0, iy0, ix1, iy1;
float xp1, yp1, xp2, yp2, x1, y1, x2, y2, xa1, ya1, xa2, ya2;
float area, darea, sum;

area = dx*dy;

for(iy=0;iy<ny;iy++)
   {
   yp1 = iy*dy;
   yp2 = yp1 + dy;

   iy1 = (int)(yp1/dy0);

   for(ix=0;ix<nx;ix++)
      {
      xp1 = ix*dx;
      xp2 = xp1 + dx;

      ix1 = (int)(xp1/dx0);

      y1 = iy1*dy0;
      y2 = y1 + dy0;

      sum = 0.0;
      while(y1 < yp2)
         {
         if(y1 < yp1)
            ya1 = yp1;
         else
            ya1 = y1;

         if(y2 > yp2)
            ya2 = yp2;
         else
            ya2 = y2;

         iy0 = (int)(y1/dy0);
         if(iy0 >= 0 && iy0 < ny0)
            {
            x1 = ix1*dx0;
            x2 = x1 + dx0;

            while(x1 < xp2)
               {
               if(x1 < xp1)
                  xa1 = xp1;
               else
                  xa1 = x1;

               if(x2 > xp2)
                  xa2 = xp2;
               else
                  xa2 = x2;

               ix0 = (int)(x1/dx0);
               if(ix0 >= 0 && ix0 < nx0)
                  {
                  darea = (xa2-xa1)*(ya2-ya1)/area;
                  ip0 = ix0 + iy0*nx0;
                  sum = sum + slip[ip0]*darea;
                  }

               x1 = x2;
               x2 = x1 + dx0;
               }
            }

         y1 = y2;
         y2 = y1 + dy0;
         }

      ip = ix + iy*nx;
      rslip[ip] = sum;
      }
   }
}

static  long    frandx = 1;

/* frand() returns a uniform distribution of random numbers
 * in the range -1.0 -> 1.0.
 */
double frand(void)
{
frandx = (frandx * 1103515245 + 12345) & 0x7fffffff;
return((double)(frandx)/1073741824.0 - 1.0);
}

/* sfrand() returns a uniform distribution of random numbers
 * in the range -1.0 -> 1.0.
 */
double sfrand(long *seed)
{
*seed = ((*seed) * 1103515245 + 12345) & 0x7fffffff;
return((double)(*seed)/1073741824.0 - 1.0);
}

void smooth_slip(float *s1,int nx,int ny,float *stmp)
{
int ip, ix, iy;
float c0, c1, sum;

c0 = 1.0;
c1 = 0.2;

stmp[0] = (c1*(s1[1] + s1[nx] + s1[nx+1]) + c0*s1[0])/(c0+3*c1);
for(ix=1;ix<nx-1;ix++)
   {
   ip = ix;
   stmp[ip] =   (c1*(s1[ip-1]                + s1[ip+1])
	       + c1*(s1[ip+nx-1] + s1[ip+nx] + s1[ip+nx+1])
	       + c0*s1[ip])/(c0+5*c1);
   }
stmp[nx-1] = (c1*(s1[nx-2] + s1[2*nx-1] + s1[2*nx-2]) + c0*s1[nx-1])/(c0+3*c1);

for(iy=1;iy<ny-1;iy++)
   {
   ip = iy*nx;
   stmp[ip] = (c1*(s1[ip-nx] + s1[ip-nx+1])
	       + c1*(        + s1[ip+1])
	       + c1*(s1[ip+nx] + s1[ip+nx+1])
	       + c0*s1[ip])/(c0+5*c1);

   for(ix=1;ix<nx-1;ix++)
      {
      ip = ix + iy*nx;
      stmp[ip] = (c1*(s1[ip-nx-1] + s1[ip-nx] + s1[ip-nx+1])
	       + c1*(s1[ip-1]                + s1[ip+1])
	       + c1*(s1[ip+nx-1] + s1[ip+nx] + s1[ip+nx+1])
	       + c0*s1[ip])/(c0+8*c1);
      }

   ip = nx-1 + iy*nx;
   stmp[ip] = (c1*(s1[ip-nx-1] + s1[ip-nx])
	       + c1*(s1[ip-1]             )
	       + c1*(s1[ip+nx-1] + s1[ip+nx])
	       + c0*s1[ip])/(c0+5*c1);
   }

ip = (ny-1)*nx;
stmp[ip] = (c1*(s1[ip+1] + s1[ip-nx] + s1[ip-nx+1]) + c0*s1[ip])/(c0+3*c1);
for(ix=1;ix<nx-1;ix++)
   {
   ip = ix + (ny-1)*nx;
   stmp[ip] = (c1*(s1[ip-nx-1] + s1[ip-nx] + s1[ip-nx+1])
	       + c1*(s1[ip-1]                + s1[ip+1])
	       + c0*s1[ip])/(c0+5*c1);
   }
ip = nx-1 + (ny-1)*nx;
stmp[ip] = (c1*(s1[ip-1] + s1[ip-nx] + s1[ip-nx-1]) + c0*s1[ip])/(c0+3*c1);

for(iy=0;iy<ny;iy++)
   {
   for(ix=0;ix<nx;ix++)
      {
      ip = ix + iy*nx;
      s1[ip] = stmp[ip];
      }
   }
}

double gaus_rand(float *sigma,float *mean,long *seed)
{
double r = 0.0;
double six = 6.0;
double one = 1.0;
double half = 0.5;
int i;

for(i=0;i<12;i++)
   r = r + sfrand(seed);

return((double)((r - six)*(*sigma) + *mean));
}
