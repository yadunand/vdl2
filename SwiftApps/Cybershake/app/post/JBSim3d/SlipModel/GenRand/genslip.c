#include "include.h"
#include "structure.h"
#include "function.h"
#include "defs.h"

main(int ac,char **av)
{
FILE *fpr, *fpw;
struct complex *cslip;
float *slip, *rupt;
float flen, fwid, dx, dy, amp0, amp, lamp;
float dkx, dky, kx, ky, xx, yy, zz;
float cosA, sinA, cosD, sinD, dd, sn, se;
int nx, ny;
int i, j, k, ip, nt6, it;
char infile[256], slipfile[256], specfile[256], ruptfile[256], str[512];
char outfile[256];

float bigM;
float xl, yl;

int flip_at_surface = 0;
int ny_in, ny_str, ny_end, nyout;

float pi = 3.14159265;
double rperd = 0.017453293;

float mag;
float side_taper = 0.1;
float bot_taper = 0.1;

long seed = 0;

int kmodel = 1;   /* default is somerville */

float shypo, dhypo, strike, rake, rt;
struct velmodel rvmod;
double rayp, rupt_rad;

float rvfrac = 0.8;
float shal_vrup = 1.0;
float htol = 0.1;

float smax, savg, sf;
float tsfac = -0.0;

float dtop, dip, mom;
struct velmodel vmod;
char velfile[128];

struct standrupformat srf;
struct srf_planerectangle *prect_ptr;
struct srf_prectsegments *prseg_ptr;
struct srf_allpoints *apnts_ptr;
struct srf_apointvalues *apval_ptr;

float trise, dt, area;
float *stf, *slon, *slat, *sdep, elon, elat;
int ig, ntot;
int nt = NTMAX;

slipfile[0] = '\0';
specfile[0] = '\0';
ruptfile[0] = '\0';

sprintf(infile,"stdin");
sprintf(outfile,"stdout");

sprintf(srf.version,"1.0");

setpar(ac,av);

mstpar("mag","f",&mag);
mstpar("flen","f",&flen);
mstpar("fwid","f",&fwid);
mstpar("nx","d",&nx);
mstpar("ny","d",&ny_in);

mstpar("dtop","f",&dtop);
mstpar("dip","f",&dip);
mstpar("shypo","f",&shypo);
mstpar("dhypo","f",&dhypo);
mstpar("elon","f",&elon);
mstpar("elat","f",&elat);
mstpar("strike","f",&strike);
mstpar("rake","f",&rake);
mstpar("dt","f",&dt);
getpar("nt","d",&nt);

getpar("rvfrac","f",&rvfrac);
getpar("shal_vrup","f",&shal_vrup);
getpar("tsfac","f",&tsfac);

getpar("slipfile","s",slipfile);
getpar("specfile","s",specfile);
getpar("ruptfile","s",ruptfile);

getpar("infile","s",infile);
getpar("outfile","s",outfile);

mstpar("velfile","s",velfile);

getpar("kmodel","d",&kmodel);
getpar("flip_at_surface","d",&flip_at_surface);
getpar("seed","d",&seed);
getpar("side_taper","f",&side_taper);
getpar("bot_taper","f",&bot_taper);

endpar();

dx = flen/nx;
dy = fwid/ny_in;

/*
   For now, set parameters here, later will read in from 'infile'
*/

slon = (float *)check_malloc(nx*ny_in*sizeof(float));
slat = (float *)check_malloc(nx*ny_in*sizeof(float));
sdep = (float *)check_malloc(nx*ny_in*sizeof(float));

cosA = cos(strike*rperd);
sinA = sin(strike*rperd);
cosD = cos(dip*rperd);
sinD = sin(dip*rperd);
for(j=0;j<ny_in;j++)
   {
   dd = (j + 0.5)*dy;
   yy = dd*cosD;
   zz = dtop + dd*sinD;
   for(i=0;i<nx;i++)
      {
      ip = i + j*nx;
      xx = (i+0.5)*dx - 0.5*flen;

      se = xx*sinA + yy*cosA;
      sn = xx*cosA - yy*sinA;
      set_ll(&elon,&elat,&slon[ip],&slat[ip],&sn,&se);

      sdep[ip] = zz;
      }
   }

read_velmodel(velfile,&vmod);
conv2vrup(&vmod,&rvmod,&dip,&dtop,&fwid,&rvfrac,&shal_vrup);

if(kmodel != MAI_FLAG)
   kmodel = SOMERVILLE_FLAG;

ny = ny_in;
if(flip_at_surface)
   ny = 2*ny_in;

bigM = log(10.0);

/* generic fault length/width scaling */
xl = flen;
yl = fwid;

if(xl > 2.0*yl)
   xl = 2.0*yl;
if(yl > 2.0*xl)
   yl = 2.0*xl;

/* somerville scaling */
xl = exp(bigM*(0.5*mag - 1.72));
yl = exp(bigM*(0.5*mag - 1.93));

/* mai scaling */
xl = exp(bigM*(0.5*mag - 2.50));
yl = exp(bigM*(0.3333*mag - 1.50));

/* modified somerville scaling */
xl = exp(bigM*(0.5*mag - 2.00));
yl = exp(bigM*(0.5*mag - 2.00));

/*
fprintf(stderr,"xl= %13.5f yl= %13.5f\n",xl,yl);
*/

dkx = 1.0/(nx*dx);
dky = 1.0/(ny*dy);

cslip = (struct complex *) check_malloc (nx*ny*sizeof(struct complex));
init_slip(cslip,nx,ny,&side_taper,&bot_taper);

fft2d(cslip,nx,ny,-1,&dx,&dy);
kfilt(cslip,nx,ny,&dkx,&dky,&xl,&yl,&seed,kmodel);
fft2d(cslip,nx,ny,1,&dkx,&dky);

/*
   truncate any negative slip values => should double check that spectra is
   not changed too much
*/

for(j=0;j<ny*nx;j++)
   {
   if(cslip[j].re < 0.0)
      cslip[j].re = 0.0;
   }

ny_str = 0;
ny_end = ny;
if(flip_at_surface)
   ny_str = ny/2;

nyout = ny_end - ny_str;

slip = (float *) check_malloc (nx*nyout*sizeof(float));
rupt = (float *) check_malloc (nx*nyout*sizeof(float));

/* check moment and scale slip */

mom = exp(bigM*1.5*(mag + 10.7));
scale_slip(slip,cslip,nx,nyout,ny_str,&dx,&dy,&dtop,&dip,&mom,&vmod,&savg,&smax);

/* calculate rupture time */

if((smax-savg) != (float)(0.0))
   sf = 1.0/(smax-savg);
else
   tsfac = sf = 0.0;

for(j=0;j<nyout;j++)
   {
   yy = (j + 0.5)*dy;
   for(i=0;i<nx;i++)
      {
      xx = (i+0.5)*dx - 0.5*flen;

      get_rupt(&rvmod,&htol,&dhypo,&yy,&shypo,&xx,&rayp,&rupt_rad,&rt);
      rupt[i+j*nx] = rt + sf*tsfac*(slip[i + j*nx] - savg);
      }
   }

fprintf(stderr,"mag= %.2f mom= %13.5e avgslip= %.0f maxslip= %.0f\n",mag,mom,savg,smax);

if(slipfile[0] != '\0')
   write_field(slipfile,slip,nx,nyout,&dx,&dy);

if(ruptfile[0] != '\0')
   write_field(ruptfile,rupt,nx,nyout,&dx,&dy);

if(specfile[0] != '\0')
   write_spec(specfile,cslip,nx,ny,&dx,&dy,&dkx,&dky,&xl,&yl,kmodel);

/*
   load rupture model into Standard Rupture Format and write output
*/

/* rise time from somerville */
trise = 1.8e-09*exp(log(mom)/3.0);

sprintf(srf.type,"PLANE");

prect_ptr = &srf.srf_prect;
prect_ptr->nseg = 1;
prseg_ptr = prect_ptr->prectseg;
prseg_ptr = (struct srf_prectsegments *)check_malloc(prect_ptr->nseg*sizeof(struct srf_prectsegments));

apnts_ptr = &srf.srf_apnts;
apnts_ptr->np = 0;
apval_ptr = apnts_ptr->apntvals;
apval_ptr = (struct srf_apointvalues *)check_malloc(sizeof(struct srf_apointvalues));

prseg_ptr[0].elon = elon;
prseg_ptr[0].elat = elat;
prseg_ptr[0].nstk = nx;
prseg_ptr[0].ndip = nyout;
prseg_ptr[0].flen = flen;
prseg_ptr[0].fwid = fwid;
prseg_ptr[0].dlen = dx;
prseg_ptr[0].dwid = dy;
prseg_ptr[0].stk = strike;
prseg_ptr[0].dip = dip;
prseg_ptr[0].dtop = dtop;
prseg_ptr[0].shyp = shypo;
prseg_ptr[0].dhyp = dhypo;

ntot = (prseg_ptr[0].nstk)*(prseg_ptr[0].ndip);
area = prseg_ptr[0].dlen*prseg_ptr[0].dwid*1.0e+10;

apval_ptr = (struct srf_apointvalues *)check_realloc(apval_ptr,(apnts_ptr->np+ntot)*sizeof(struct srf_apointvalues));

for(j=0;j<nyout;j++)
   {
   for(i=0;i<nx;i++)
      {
      ip = i + j*nx;

      apval_ptr[ip].stf1 = (float *)check_malloc(nt*sizeof(float));
      stf = apval_ptr[ip].stf1;

      apval_ptr[ip].dt = dt;
      if(slip[ip] > MINSLIP)
         apval_ptr[ip].nt1 = gen_stf(&trise,stf,nt,&dt,&sdep[ip]);
      else
         apval_ptr[ip].nt1 = 0;

      if(apval_ptr[ip].nt1)
         apval_ptr[ip].stf1 = (float *)check_realloc(apval_ptr[ip].stf1,(apval_ptr[ip].nt1)*sizeof(float));
      else
         free(apval_ptr[ip].stf1);

      apval_ptr[ip].lon = slon[ip];
      apval_ptr[ip].lat = slat[ip];
      apval_ptr[ip].dep = sdep[ip];
      apval_ptr[ip].stk = prseg_ptr[0].stk;
      apval_ptr[ip].dip = prseg_ptr[0].dip;
      apval_ptr[ip].area = area;
      apval_ptr[ip].tinit = rupt[ip];
      apval_ptr[ip].rake = rake;
      apval_ptr[ip].slip1 = slip[ip];

      apval_ptr[ip].slip2 = 0.0;
      apval_ptr[ip].nt2 = 0;
      apval_ptr[ip].slip3 = 0.0;
      apval_ptr[ip].nt3 = 0;
      }
   }
apnts_ptr->np = apnts_ptr->np + ntot;

if(strcmp(outfile,"stdout") == 0)
   fpw = stdout;
else
   fpw = fopfile(outfile,"w");

fprintf(fpw,"%s\n",srf.version);

fprintf(fpw,"%s %d\n",srf.type,prect_ptr->nseg);
for(ig=0;ig<prect_ptr->nseg;ig++)
   {
   fprintf(fpw,"%10.4f %9.4f %5d %5d %8.2f %8.2f\n",prseg_ptr[ig].elon,
                                                     prseg_ptr[ig].elat,
                                                     prseg_ptr[ig].nstk,
                                                     prseg_ptr[ig].ndip,
                                                     prseg_ptr[ig].flen,
                                                     prseg_ptr[ig].fwid);
   fprintf(fpw,"%4.0f %4.0f %8.2f %8.2f %8.2f\n",prseg_ptr[ig].stk,
                                                 prseg_ptr[ig].dip,
                                                 prseg_ptr[ig].dtop,
                                                 prseg_ptr[ig].shyp,
                                                 prseg_ptr[ig].dhyp);
   }

fprintf(fpw,"POINTS %d\n",apnts_ptr->np);
for(i=0;i<apnts_ptr->np;i++)
   {
   fprintf(fpw,"%10.4f %9.4f %9.4f %4.0f %4.0f %12.5e %10.4f %12.5e\n",
                                              apval_ptr[i].lon,
                                              apval_ptr[i].lat,
                                              apval_ptr[i].dep,
                                              apval_ptr[i].stk,
                                              apval_ptr[i].dip,
                                              apval_ptr[i].area,
                                              apval_ptr[i].tinit,
                                              apval_ptr[i].dt);
   fprintf(fpw,"%4.0f %8.2f %6d %8.2f %6d %8.2f %6d\n",
                                              apval_ptr[i].rake,
                                              apval_ptr[i].slip1,
                                              apval_ptr[i].nt1,
                                              apval_ptr[i].slip2,
                                              apval_ptr[i].nt2,
                                              apval_ptr[i].slip3,
                                              apval_ptr[i].nt3);

   stf = apval_ptr[i].stf1;
   nt6 = (apval_ptr[i].nt1)/6;
   for(k=0;k<nt6;k++)
      {
      for(j=0;j<6;j++)
         {
         it = 6*k + j;
         fprintf(fpw,"%13.5e",stf[it]);
         }
      fprintf(fpw,"\n");
      }

   if(6*nt6 != (apval_ptr[i].nt1))
      {
      for(j=6*nt6;j<(apval_ptr[i].nt1);j++)
         fprintf(fpw,"%13.5e",stf[j]);

      fprintf(fpw,"\n");
      }

   stf = apval_ptr[i].stf2;
   for(it=0;it<apval_ptr[i].nt2;it++)
      fprintf(fpw,"%13.5e\n",stf[it]);

   stf = apval_ptr[i].stf3;
   for(it=0;it<apval_ptr[i].nt3;it++)
      fprintf(fpw,"%13.5e\n",stf[it]);
   }

fclose(fpw);
}
