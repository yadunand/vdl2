#include "include.h"
#include "structure.h"
#include "function.h"
#include "defs.h"

main(int ac,char **av)
{
FILE *fpr, *fpw;
float tlon0, tlat0, tlon1, tlat1;
float lon0, lat0, lon1, lat1;
float tclon, tclat, sn, se, nadj, eadj;
float local_stk, local_len;
float dsub, wid, dtop, avgdip, avgrak, mag;
float *slon, *slat, *sstk, *sdip, *srak, *sdep, *ddep, *dlon, *dlat;
int i, nstk, np, j, ndip;
char infile[256], str[512];
char outfile[256];

float pi = 3.14159265;
double rperd = 0.017453293;
double dperr = 57.29577951;

sprintf(infile,"stdin");
sprintf(outfile,"stdout");

setpar(ac,av);

getpar("infile","s",infile);
getpar("outfile","s",outfile);

mstpar("wid","f",&wid);
mstpar("dsub","f",&dsub);
mstpar("dtop","f",&dtop);
mstpar("dip","f",&avgdip);
mstpar("rake","f",&avgrak);
mstpar("mag","f",&mag);

endpar();

slon = NULL;
slat = NULL;
sstk = NULL;
sdip = NULL;
srak = NULL;
sdep = NULL;
ddep = NULL;
dlon = NULL;
dlat = NULL;

if(strcmp(infile,"stdin") == 0)
   fpr = stdin;
else
   fpr = fopfile(infile,"r");

fgets(str,512,fpr);
sscanf(str,"%f %f",&tlon0,&tlat0);
np = 0;
while(fgets(str,512,fpr) != NULL)
   {
   sscanf(str,"%f %f",&tlon1,&tlat1);

   set_ne(&tlon0,&tlat0,&tlon1,&tlat1,&sn,&se);

   local_len = sqrt(sn*sn + se*se);
   nstk = (int)(local_len/dsub + 0.5);

   if(sn != 0.0)
      local_stk = dperr*atan(se/sn);
   else
      local_stk = 90.0;

   if(sn < 0.0)
      local_stk = local_stk + 180.0;

   while(local_stk < 0.0)
      local_stk = local_stk + 360.0;

   while(local_stk >= 360.0)
      local_stk = local_stk - 360.0;

   nadj = 0.5*(local_len - nstk*dsub)*cos(local_stk*rperd);
   eadj = 0.5*(local_len - nstk*dsub)*sin(local_stk*rperd);

   set_ll(&tlon0,&tlat0,&lon0,&lat0,&nadj,&eadj);
   tlon0 = lon0;
   tlat0 = lat0;

   slon = (float *)check_realloc(slon,(np+nstk)*sizeof(float));
   slat = (float *)check_realloc(slat,(np+nstk)*sizeof(float));
   sstk = (float *)check_realloc(sstk,(np+nstk)*sizeof(float));
   sdip = (float *)check_realloc(sdip,(np+nstk)*sizeof(float));
   srak = (float *)check_realloc(srak,(np+nstk)*sizeof(float));
   sdep = (float *)check_realloc(sdep,(np+nstk)*sizeof(float));
   ddep = (float *)check_realloc(ddep,(np+nstk)*sizeof(float));
   dlon = (float *)check_realloc(dlon,(np+nstk)*sizeof(float));
   dlat = (float *)check_realloc(dlat,(np+nstk)*sizeof(float));

   nadj = dsub*cos(avgdip*rperd)*cos((local_stk+90.0)*rperd);
   eadj = dsub*cos(avgdip*rperd)*sin((local_stk+90.0)*rperd);

   set_ll(&tlon0,&tlat0,&dlon[np],&dlat[np],&nadj,&eadj);
   dlon[np] = dlon[np] - tlon0;
   dlat[np] = dlat[np] - tlat0;
   ddep[np] = dsub*sin(avgdip*rperd);

   for(i=0;i<nstk;i++)
      {
      nadj = (i+1)*dsub*cos(local_stk*rperd);
      eadj = (i+1)*dsub*sin(local_stk*rperd);

      set_ll(&tlon0,&tlat0,&lon1,&lat1,&nadj,&eadj);

      tclon = 0.5*(lon0 + lon1);
      tclat = 0.5*(lat0 + lat1);

      dlon[i+np] = dlon[np];
      dlat[i+np] = dlat[np];
      ddep[i+np] = ddep[np];
      slon[i+np] = tclon + 0.0*dlon[np];
      slat[i+np] = tclat + 0.0*dlat[np];
      sstk[i+np] = local_stk;
      sdip[i+np] = avgdip;
      srak[i+np] = avgrak;
      sdep[i+np] = dtop + 0.5*ddep[np];

      lon0 = lon1;
      lat0 = lat1;
      }

   np = np + nstk;

   tlon0 = tlon1;
   tlat0 = tlat1;
   }
fclose(fpr);

nstk = np;
ndip = (int)(wid/dsub + 0.5);

if(strcmp(infile,"stdout") == 0)
   fpw = stdout;
else
   fpw = fopfile(outfile,"w");

fprintf(fpw,"Probability = 1.4031105E-4\n");
fprintf(fpw,"Magnitude = %.2f\n",mag);
fprintf(fpw,"GridSpacing = %.2f\n",dsub);
fprintf(fpw,"NumRows = %d\n",ndip);
fprintf(fpw,"NumCols = %d\n",nstk);
fprintf(fpw,"# Lat  Lon  Depth  Rake  Dip  Strike\n");

for(j=0;j<ndip;j++)
   {
   for(i=0;i<nstk;i++)
      {
      fprintf(fpw,"%.5f\t%.5f\t%.5f\t%.1f\t%.1f\t%.3f\n",slat[i]+j*dlat[i],
                                                         slon[i]+j*dlon[i],
                                                         sdep[i]+j*ddep[i],
                                                         srak[i],
                                                         sdip[i],
                                                         sstk[i]);
      }
   }

fclose(fpw);
}
