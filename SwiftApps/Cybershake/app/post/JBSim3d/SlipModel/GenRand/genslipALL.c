#include "include.h"
#include "structure.h"
#include "function.h"
#include "defs.h"

main(int ac,char **av)
{
FILE *fpr, *fpw;
struct complex *cslip;
float *slip, *rupt;
float flen, fwid, dx, dy, amp0, amp, lamp;
float dkx, dky, kx, ky, xx, yy, zz;
int nx, ny;
int i, j, k, l, ip;
char infile[256], slipfile[256], specfile[256], ruptfile[256], str[512];
char outfile[256];

float bigM;
float xl, yl;

int flip_at_surface = 0;
int ny_in, ny_str, ny_end, nyout;

float pi = 3.14159265;

float mag;
float side_taper = 0.1;
float bot_taper = 0.1;

long seed = 0;

int kmodel = 1;   /* default is somerville */

float shypo, dhypo, rake, rt;
struct velmodel rvmod;
double rayp, rupt_rad;

float rvfrac = 0.8;
float shal_vrup = 1.0;
float htol = 0.1;

float smax, savg, sf;
float tsfac = -0.0;

float dtop, dip, mom;
struct velmodel vmod;
char velfile[128];

slipfile[0] = '\0';
specfile[0] = '\0';
ruptfile[0] = '\0';

sprintf(infile,"stdin");
sprintf(outfile,"stdout");

setpar(ac,av);

mstpar("mag","f",&mag);
mstpar("flen","f",&flen);
mstpar("fwid","f",&fwid);
mstpar("nx","d",&nx);
mstpar("ny","d",&ny_in);

mstpar("dtop","f",&dtop);
mstpar("dip","f",&dip);
mstpar("shypo","f",&shypo);
mstpar("dhypo","f",&dhypo);
mstpar("rake","f",&rake);

getpar("rvfrac","f",&rvfrac);
getpar("shal_vrup","f",&shal_vrup);
getpar("tsfac","f",&tsfac);

getpar("slipfile","s",slipfile);
getpar("specfile","s",specfile);
getpar("ruptfile","s",ruptfile);

getpar("infile","s",infile);
getpar("outfile","s",outfile);

mstpar("velfile","s",velfile);

getpar("kmodel","d",&kmodel);
getpar("flip_at_surface","d",&flip_at_surface);
getpar("seed","d",&seed);
getpar("side_taper","f",&side_taper);
getpar("bot_taper","f",&bot_taper);

endpar();

fpr = fopfile(velfile,"r");

fgets(str,512,fpr);
sscanf(str,"%d",&vmod.nlay);

vmod.vp = (float *)check_malloc(vmod.nlay*sizeof(float));
vmod.vs = (double *)check_malloc(vmod.nlay*sizeof(double));
vmod.den = (float *)check_malloc(vmod.nlay*sizeof(float));
vmod.th = (float *)check_malloc(vmod.nlay*sizeof(float));
vmod.dep = (float *)check_malloc(vmod.nlay*sizeof(float));
vmod.mu = (float *)check_malloc(vmod.nlay*sizeof(float));
vmod.invb2 = (double *)check_malloc(vmod.nlay*sizeof(double));

for(i=0;i<vmod.nlay;i++)
   {
   fgets(str,512,fpr);
   sscanf(str,"%f %f %lf %f",&vmod.th[i],&vmod.vp[i],&vmod.vs[i],&vmod.den[i]);

   if(i==0)
      vmod.dep[i] = vmod.th[i];
   else
      vmod.dep[i] = vmod.dep[i-1] + vmod.th[i];

   vmod.mu[i] = vmod.vs[i]*vmod.vs[i]*vmod.den[i]*1.0e+10;  /* in CMS units */
   }
fclose(fpr);

rvmod.nlay = vmod.nlay;
rvmod.vs = (double *)check_malloc(rvmod.nlay*sizeof(double));
rvmod.th = (float *)check_malloc(rvmod.nlay*sizeof(float));
rvmod.invb2 = (double *)check_malloc(rvmod.nlay*sizeof(double));

conv2vrup(&vmod,&rvmod,&dip,&dtop,&fwid,&rvfrac,&shal_vrup);

if(kmodel != MAI_FLAG)
   kmodel = SOMERVILLE_FLAG;

ny = ny_in;
if(flip_at_surface)
   ny = 2*ny_in;

dx = flen/nx;
dy = fwid/ny_in;

bigM = log(10.0);

/* mai scaling */
xl = exp(bigM*(0.5*mag - 2.50));
yl = exp(bigM*(0.3333*mag - 1.50));

/* generic fault length/width scaling */
xl = flen;
yl = fwid;

if(xl > 2.0*yl)
   xl = 2.0*yl;
if(yl > 2.0*xl)
   yl = 2.0*xl;

/* somerville scaling */
xl = exp(bigM*(0.5*mag - 1.72));
yl = exp(bigM*(0.5*mag - 1.93));

/* modified somerville scaling */
xl = exp(bigM*(0.5*mag - 2.00));
yl = exp(bigM*(0.5*mag - 2.00));

/*
fprintf(stderr,"xl= %13.5f yl= %13.5f\n",xl,yl);
*/

dkx = 1.0/(nx*dx);
dky = 1.0/(ny*dy);

cslip = (struct complex *) check_malloc (nx*ny*sizeof(struct complex));
init_slip(cslip,nx,ny,&side_taper,&bot_taper);

fft2d(cslip,nx,ny,-1,&dx,&dy);
kfilt(cslip,nx,ny,&dkx,&dky,&xl,&yl,&seed,kmodel);
fft2d(cslip,nx,ny,1,&dkx,&dky);

/*
   truncate any negative slip values => should double check that spectra is
   not changed too much
*/

for(j=0;j<ny*nx;j++)
   {
   if(cslip[j].re < 0.0)
      cslip[j].re = 1.0e-04;
   }

ny_str = 0;
ny_end = ny;
if(flip_at_surface)
   ny_str = ny/2;

nyout = ny_end - ny_str;

slip = (float *) check_malloc (nx*nyout*sizeof(float));
rupt = (float *) check_malloc (nx*nyout*sizeof(float));

/* check moment and scale slip */

mom = exp(bigM*1.5*(mag + 10.7));
scale_slip(slip,cslip,nx,nyout,ny_str,&dx,&dy,&dtop,&dip,&mom,&vmod,&savg,&smax);

/* calculate rupture time */

if((smax-savg) != (float)(0.0))
   sf = 1.0/(smax-savg);
else
   tsfac = sf = 0.0;

for(j=0;j<nyout;j++)
   {
   yy = (j + 0.5)*dy;
   for(i=0;i<nx;i++)
      {
      xx = (i+0.5)*dx - 0.5*flen;

      get_rupt(&rvmod,&htol,&dhypo,&yy,&shypo,&xx,&rayp,&rupt_rad,&rt);
      rupt[i+j*nx] = rt + sf*tsfac*(slip[i + j*nx] - savg);
      }
   }

fprintf(stderr,"mag= %.2f mom= %13.5e avgslip= %.0f maxslip= %.0f\n",mag,mom,savg,smax);

/*
write_srf(outfile,slip,nx,nyout,&dx,&dy);
*/

if(slipfile[0] != '\0')
   write_field(slipfile,slip,nx,nyout,&dx,&dy);

if(ruptfile[0] != '\0')
   write_field(ruptfile,rupt,nx,nyout,&dx,&dy);

if(specfile[0] != '\0')
   write_spec(specfile,cslip,nx,ny,&dx,&dy,&dkx,&dky,&xl,&yl,kmodel);
}

void kfilt(struct complex *s0,int nx0,int ny0,float *dkx,float *dky,float *xl,float *yl,long *seed,int kflag)
{
int i, j, ip;
float kx, ky, fac, amp, amp0, phs, xl2, yl2;

float pi = 3.14159265;
float hcoef = 1.8;  /* H=0.8, hcoef = H + 1 */

amp0 = sqrt(s0[0].re*s0[0].re + s0[0].im*s0[0].im);
if(*xl > 1.0e+14 || *yl > 1.0e+14)
   amp0 = 0.0;

xl2 = (*xl)*(*xl);
yl2 = (*yl)*(*yl);

for(j=0;j<ny0;j++)
   {
   if(j <= ny0/2)
      ky = j*(*dky);
   else
      ky = (j - ny0)*(*dky);

   for(i=0;i<nx0;i++)
      {
      if(i <= nx0/2)
         kx = i*(*dkx);
      else
         kx = (i - nx0)*(*dkx);

      ip = i + j*nx0;

      amp = kx*kx*xl2 + ky*ky*yl2;

      /* default is somerville scaling */
      fac = amp0/sqrt(1.0 + amp*amp);

      if(kflag == MAI_FLAG) /* mai scaling */
         {
         fac = exp((hcoef)*log(1.0+amp));
         fac = amp0/sqrt(fac);
	 }

      if(kflag == SOMERVILLE_FLAG)      /* somerville scaling */
         fac = amp0/sqrt(1.0 + amp*amp);

      phs = pi*sfrand(seed);

/* 
   Do not alter phase of lowest (k=0) and 2nd lowest (k=dk) wavenumbers
   so average slip and edge taper are not significantly modified
*/
      if(i > 1 && j > 1)
         {
         s0[ip].re = fac*cos(phs);
         s0[ip].im = fac*sin(phs);
	 }
      }
   }
}

void fft2d(struct complex *xc,int n1,int n2,int isgn,float *d1,float *d2)
{
int i, j, ip;
float *space;
struct complex *xtc;
float normf;

normf = (*d1)*(*d2);

space = (float *) check_malloc (2*(n1+n2)*sizeof(float));

for(j=0;j<n2;j++)
   fourg_(xc+j*n1,&n1,&isgn,space);

xtc = (struct complex *) check_malloc (n2*sizeof(struct complex));

for(i=0;i<n1;i++)
   {
   for(j=0;j<n2;j++)
      {
      ip = i + j*n1;

      xtc[j].re = xc[ip].re;
      xtc[j].im = xc[ip].im;
      }

   fourg_(xtc,&n2,&isgn,space);

   for(j=0;j<n2;j++)
      {
      ip = i + j*n1;

      xc[ip].re = normf*xtc[j].re;
      xc[ip].im = normf*xtc[j].im;
      }
   }

free(space);
free(xtc);
}

FILE *fopfile(char *name,char *mode)
{
FILE *fp;

if((fp = fopen(name,mode)) == NULL)
   {
   fprintf(stderr,"CAN'T FOPEN FILE = %s, MODE = %s\n", name, mode);
   exit(-1);
   }
return(fp);
}

void *check_malloc(size_t len)
{
void *ptr;

ptr = (void *) malloc (len);

if(ptr == NULL)
   {
   fprintf(stderr,"*****  memory allocation error\n");
   exit(-1);
   }

return(ptr);
}

static  long    frandx = 1;

/* frand() returns a uniform distribution of random numbers
 * in the range -1.0 -> 1.0.
 */
double frand(void)
{
frandx = (frandx * 1103515245 + 12345) & 0x7fffffff;
return((double)(frandx)/1073741824.0 - 1.0);
}

/* sfrand() returns a uniform distribution of random numbers
 * in the range -1.0 -> 1.0.
 */
double sfrand(long *seed)
{
*seed = ((*seed) * 1103515245 + 12345) & 0x7fffffff;
return((double)(*seed)/1073741824.0 - 1.0);
}

void init_slip(struct complex *sc,int nx,int ny,float *st,float *bt)
{
float xdamp, ydamp;
int ix, iy, xb, yb;

for(ix=0;ix<nx*ny;ix++)
   {
   sc[ix].re = 0.5;
   sc[ix].im = 0.0;
   }

xb = (int)((*st)*nx + 0.5);
if(xb < 0)
   xb = 0;

yb = (int)((*bt)*ny + 0.5);
if(yb < 0)
   yb = 0;

for(iy=0;iy<yb;iy++)
   {
   ydamp = (float)(iy+1)/(float)(yb);
   for(ix=0;ix<nx;ix++)
      {
      xdamp = 1.0;
      if(ix < xb)
         xdamp = (float)(ix+1)/(float)(xb);
      if(ix > nx-xb)
         xdamp = (float)(nx-ix)/(float)(xb);

      sc[ix+iy*nx].re = xdamp*ydamp*sc[ix+iy*nx].re;
      sc[ix+(ny-1-iy)*nx].re = xdamp*ydamp*sc[ix+(ny-1-iy)*nx].re;
      }
   }

for(iy=yb;iy<ny-yb;iy++)
   {
   for(ix=0;ix<xb;ix++)
      {
      xdamp = (float)(ix+1)/(float)(xb);

      sc[ix+iy*nx].re = xdamp*sc[ix+iy*nx].re;
      sc[(nx-1-ix)+iy*nx].re = xdamp*sc[(nx-1-ix)+iy*nx].re;
      }
   }
}

void write_field(char *file,float *aa,int nx,int ny,float *dx,float *dy)
{
FILE *fpw, *fopfile();
float xx, yy;
int i, j;

fpw = fopfile(file,"w");
for(j=0;j<ny;j++)
   {
   yy = (j + 0.5)*(*dy);
   for(i=0;i<nx;i++)
      {
      xx = (i+0.5)*(*dx);
      fprintf(fpw,"%12.5e %12.5e %12.5e\n",xx,yy,aa[i+j*nx]);
      }
   }
fclose(fpw);
}

void write_spec(char *file,struct complex *slip,int nx,int ny,float *dx,float *dy,float *dkx,float *dky,float *xl,float *yl,int kflag)
{
FILE *fpw;
float kx, ky, amp, amp0, lamp;
float xl2, yl2, fac;
int i, j, ip;

float hcoef = 1.8;  /* H=0.8, hcoef = H + 1 */

xl2 = (*xl)*(*xl);
yl2 = (*yl)*(*yl);

fft2d(slip,nx,ny,-1,dx,dy);

fpw = fopfile(file,"w");

/* this will normalize max spectrum to one */
amp0 = sqrt(slip[0].re*slip[0].re + slip[0].im*slip[0].im);

for(j=0;j<=ny/2;j++)
   {
   if(j<=ny/2)
      ky = j*(*dky);
   else
      ky = (j-ny)*(*dky);

   for(i=0;i<=nx/2;i++)
      {
      if(i<=nx/2)
         kx = i*(*dkx);
      else
         kx = (i-nx)*(*dkx);

      ip = i + j*nx;

      amp = kx*kx*xl2 + ky*ky*yl2;

      /* default is somerville scaling */
      fac = 1.0/sqrt(1.0 + amp*amp);

      if(kflag == MAI_FLAG) /* mai scaling */
         {
         fac = exp((hcoef)*log(1.0+amp));
         fac = 1.0/sqrt(fac);
	 }

      if(kflag == SOMERVILLE_FLAG)      /* somerville scaling */
         fac = 1.0/sqrt(1.0 + amp*amp);

      amp = sqrt(slip[ip].re*slip[ip].re + slip[ip].im*slip[ip].im);
      amp = amp/amp0;

      lamp = -1.e+20;
      if(amp > 0.0)
	 lamp = log10(amp/fac);

      fprintf(fpw,"%13.5e %13.5e %12.5e %12.5e\n",kx,ky,lamp,amp);
      }
   }
fclose(fpw);

fft2d(slip,nx,ny,1,dkx,dky);
}

void conv2vrup(struct velmodel *vm,struct velmodel *rvm,float *dip,float *ztop,float *wid,float *rvf,float *shal_vr)
{
int i, j, k;
float invsinA, dep, zbot;
char string[256];

float rperd = 0.017453293;
float dmin = 4.0;
float dmax = 6.0;
float rvfac;

i = 0;
dep = vm->th[0];
while(dep < (*ztop))
   {
   i++;
   dep = dep + vm->th[i];
   }

zbot = *ztop + (*wid)*sin((*dip)*rperd);
invsinA = 1.0/sin((*dip)*rperd);

if(dep >= dmax)
   rvfac = (*rvf);
else if(dep < dmax && dep > dmin)
   rvfac = (*rvf)*(1.0 - (1.0 - (*shal_vr))*(dmax-dep)/(dmax-dmin));
else
   rvfac = (*rvf)*(*shal_vr);

rvm->th[0] = invsinA*(dep - (*ztop));
rvm->vs[0] = rvfac*vm->vs[i];

j = i;
k = 0;
while(dep < zbot)
   {
   j++; k++;
   dep = dep + vm->th[j];

   if(dep >= dmax)
      rvfac = (*rvf);
   else if(dep < dmax && dep > dmin)
      rvfac = (*rvf)*(1.0 - (1.0 - (*shal_vr))*(dmax-dep)/(dmax-dmin));
   else
      rvfac = (*rvf)*(*shal_vr);

   rvm->th[k] = invsinA*vm->th[j];
   rvm->vs[k] = rvfac*vm->vs[j];
   }

rvm->nlay = k + 1;

for(i=0;i<rvm->nlay;i++)
   rvm->invb2[i] = 1.0/(rvm->vs[i]*rvm->vs[i]);
}

get_rupt(vm,h,srcd,recd,srcr,recr,p,rad,tt)
struct velmodel *vm;
float *h, *srcr, *recr, *recd, *tt, *srcd;
double *p, *rad;
{
double sth, rth, rng;
float sdep, rdep;
float tol;
float tup, thead;
int k, slay, rlay, linc;

float tenth = 0.1;
double eps = 1.0e-12;

tol = tenth*(*h);

rng = *srcr - *recr;
if(rng < 0.0)
   rng = -rng;

k = 0;
sdep = vm->th[0];
while((*srcd) > sdep)
   {
   k++;
   sdep = sdep + vm->th[k];
   }
slay = k;

k = 0;
rdep = vm->th[0];
while((*recd) > rdep)
   {
   k++;
   rdep = rdep + vm->th[k];
   }
rlay = k;

sth = sdep - *srcd;
rth = rdep - *recd;
get_headtime(vm,slay,&sth,rlay,&rth,&rng,&thead);

if(slay != rlay)
   {
   if(sdep > rdep)
      {
      sth = vm->th[slay] - (sdep - *srcd);
      rth = rdep - *recd;
      linc = -1;
      }
   else
      {
      sth = sdep - *srcd;
      rth = vm->th[rlay] - (rdep - *recd);
      linc = 1;
      }

/*
   bisection method
*/
    bisect_p(vm,slay,&sth,rlay,&rth,p,&eps,&tol,&rng,linc);

         /* get path length and travel time for correct ray parameter */

   get_radtime(vm,slay,&sth,rlay,&rth,p,rad,&tup,linc);
   }
else
   {
   *rad = sqrt(rng*rng + ((*srcd)-(*recd))*((*srcd)-(*recd)));
   tup = (*rad)/vm->vs[slay];
   }

*tt = thead;
if(tup < thead)
   *tt = tup;
/*
else
   fprintf(stderr,"*** thead selected\n");

fprintf(stderr,"*** thd= %f tup= %f\n",thead,tup);
*/
}

bisect_p(vm,slay,sth,rlay,rth,p,eps,tol,rng,linc)
struct velmodel *vm;
double *sth, *rth, *p, *rng, *eps;
float *tol;
int linc, slay, rlay;
{
double tp0, pp, pm, p0, r0, delr;
int i, ic;

int nc = 100;

p0 = 1.0/vm->vs[slay];
for(i=slay+linc;i!=rlay;i=i+linc)
   {
   tp0 = 1.0/vm->vs[i];
   if(tp0 < p0)
      p0 = tp0;
   }
tp0 = 1.0/vm->vs[rlay];
if(tp0 < p0)
   p0 = tp0;

*p = *eps;

get_range(vm,slay,sth,rlay,rth,p,&r0,linc);

if(r0 < *rng)  /* if not, then p=0 (vertical ray) */
   {
                   /* bracket range with ray parameter extremes */

   ic = 0;
   while(r0 < *rng && ic < nc)
      {
      ic++;
      *p = p0*(1.0 - (*eps)/(double)(ic*ic));
      get_range(vm,slay,sth,rlay,rth,p,&r0,linc);
      }

   pp = *p;
   pm = *eps;

   delr = r0 - *rng;

/*
   use bisection to converge to correct ray parameter
*/

   ic = 0;
   while(delr > *tol)
      {
      *p = 0.5*(pp + pm);

      if(*p == pp || *p == pm) /* beyond double precision accuracy */
         break;

      get_range(vm,slay,sth,rlay,rth,p,&r0,linc);
      if(r0 >= *rng)
         {
         delr = r0 - *rng;
         pp = *p;
         }
      else
         {
         delr = *rng - r0;
         pm = *p;
         }

      ic++;
      if(ic > nc)
         break;
      }
   }
else
   *p = 0.0;
}

get_range(vm,slay,sth,rlay,rth,p,r0,linc)
struct velmodel *vm;
double *sth, *rth, *p, *r0;
int linc, slay, rlay;
{
int i;
double denom, arg;
double invp2;

invp2 = 1.0/((*p)*(*p));

denom = sqrt(invp2*vm->invb2[slay] - 1.0);
*r0 = (*sth)/denom;

for(i=slay+linc;i!=rlay;i=i+linc)
   {
   denom = sqrt(invp2*vm->invb2[i] - 1.0);
   *r0 = *r0 + vm->th[i]/denom;
   }

denom = sqrt(invp2*vm->invb2[rlay] - 1.0);
*r0 = *r0 + (*rth)/denom;
}

get_radtime(vm,slay,sth,rlay,rth,p,r0,tt,linc)
struct velmodel *vm;
double *sth, *rth, *p, *r0;
float *tt;
int linc, slay, rlay;
{
int i;
double r1, rad, arg;
double denom, invp2;

if(*p > 0.0)
   {
   arg = 1.0 - (*p)*(*p)*vm->vs[slay]*vm->vs[slay];
   denom = sqrt(arg);

   *r0 = (*sth)/denom;
   *tt = *r0/vm->vs[slay];

   for(i=slay+linc;i!=rlay;i=i+linc)
      {
      arg = 1.0 - (*p)*(*p)*vm->vs[i]*vm->vs[i];
      denom = sqrt(arg);

      rad = vm->th[i]/denom;
      *r0 = *r0 + rad;
      *tt = *tt + rad/vm->vs[i];
      }

   arg = 1.0 - (*p)*(*p)*vm->vs[rlay]*vm->vs[rlay];
   denom = sqrt(arg);

   rad = (*rth)/denom;
   *r0 = *r0 + rad;
   *tt = *tt + rad/vm->vs[rlay];
   }
else
   {
   *r0 = *sth;
   *tt = *r0/vm->vs[slay];

   for(i=slay+linc;i!=rlay;i=i+linc)
      {
      *r0 = *r0 + vm->th[i];
      *tt = *tt + vm->th[i]/vm->vs[i];
      }

   *r0 = *r0 + *rth;
   *tt = *tt + (*rth)/vm->vs[rlay];
   }
}

get_headtime(mod,slay,sth,rlay,rth,rad,tt)
struct velmodel *mod;
double *sth, *rth, *rad;
float *tt;
int slay, rlay;
{
int vflag, j, jj, jst, jnd;
double inv2, rc, tinc, arg;

jst = rlay;
if(slay > rlay)
   jst = slay;

*tt = 1.0e+5;
for(jnd=jst+1;jnd<mod->nlay;jnd++)
   {
   jj = rlay;
   if(slay < rlay)
      jj = slay;

   vflag = 1;
   for(j=jj;j<jnd;j++)
      {
      if(mod->vs[j] > mod->vs[jnd])
	 vflag = -1;
      }

   if(vflag == 1)
      {
      tinc = (*rad)/mod->vs[jnd];
      inv2 = 1.0/(mod->vs[jnd]*mod->vs[jnd]);

      arg = 1.0/(mod->vs[slay]*mod->vs[slay]) - inv2;
      arg = sqrt(arg);
      tinc = tinc + (*sth)*arg;
      rc = (*sth)/(arg*mod->vs[jnd]);

      for(j=slay+1;j<jnd;j++)
         {
         arg = 1.0/(mod->vs[j]*mod->vs[j]) - inv2;
         arg = sqrt(arg);
         tinc = tinc + mod->th[j]*arg;
         rc = rc + mod->th[j]/(arg*mod->vs[jnd]);
         }

      for(j=rlay+1;j<jnd;j++)
         {
         arg = 1.0/(mod->vs[j]*mod->vs[j]) - inv2;
         arg = sqrt(arg);
         tinc = tinc + mod->th[j]*arg;
         rc = rc + mod->th[j]/(arg*mod->vs[jnd]);
         }

      arg = 1.0/(mod->vs[rlay]*mod->vs[rlay]) - inv2;
      arg = sqrt(arg);
      tinc = tinc + (*rth)*arg;
      rc = rc + (*rth)/(arg*mod->vs[jnd]);

      if(tinc < *tt && rc < (*rad))
         *tt = tinc;
      }
   }
}

void scale_slip(float *s,struct complex *cs,int nx,int ny,int nys,float *dx,float *dy,float *dtop,float *dip,float *mom,struct velmodel *vm,float *savg,float *smax)
{
float sum, fac, sinD, area;
float zz;
int i, j, k;

float rperd = 0.017453293;

sinD = sin((*dip)*rperd);
area = (*dx)*(*dy)*1.0e+10;    /* in CMS units */

sum = 0.0;
for(j=0;j<ny;j++)
   {
   zz = (*dtop) + sinD*(j + 0.5)*(*dy);

   k = 0;
   while(zz > vm->dep[k] && k < (vm->nlay)-1)
      k++;

   fac = area*vm->mu[k];
   for(i=0;i<nx;i++)
      sum = sum + fac*cs[i + (j+nys)*nx].re;
   }

fac = (*mom)/sum;

*smax = 0.0;
*savg = 0.0;
for(j=0;j<ny;j++)
   {
   for(i=0;i<nx;i++)
      {
      s[i + j*nx] = fac*cs[i + (j+nys)*nx].re;

      *savg = *savg + s[i + j*nx];
      if(s[i + j*nx] > *smax)
         *smax = s[i + j*nx];
      }
   }
*savg = (*savg)/(float)(nx*ny);
}
