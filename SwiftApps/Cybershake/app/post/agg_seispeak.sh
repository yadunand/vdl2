#!/bin/bash

#SEISMOGRAM=/gpfs/pads/swift/aespinosa/science/cybershake/apps/JBSim3d/bin/jbsim3d
SEISMOGRAM=/home/ketan/cybershake/post/JBSim3d/bin/jbsim3d
PEAKCALC=/home/ketan/cybershake/post/SpectralAcceleration/p2utils/surfseis_rspectra
#PEAKCALC=/gpfs/pads/swift/aespinosa/science/cybershake/apps/SpectralAcceleration/p2utils/surfseis_rspectra

seismogram() {
  $SEISMOGRAM \
      extract_sgt=0 outputBinary=1 mergeOutput=1  \
      stat=$1 slon=$2 slat=$3 ntout=$4 \
      \
      sgt_xfile=$5 \
      sgt_yfile=$6 \
      \
      rupmodfile=$7 \
      seis_file=$8
}

peakcalc() {
  $PEAKCALC \
      simulation_out_pointsX=2 simulation_out_pointsY=1 \
      surfseis_rspectra_seismogram_units=cmpersec \
      surfseis_rspectra_output_units=cmpersec2 \
      surfseis_rspectra_output_type=aa \
      surfseis_rspectra_apply_byteswap=no \
      \
      simulation_out_timesamples=$1 \
      simulation_out_timeskip=$2 \
      surfseis_rspectra_period=$3 \
      surfseis_rspectra_apply_filter_highHZ=$4 \
      \
      in=$5 \
      out=$6
}

n=${11}
subx=$9
suby=${10}
seisargs="$1 $2 $3 $4"
peakargs="$5 $6 $7 $8"

exitcode=0

shift 11
echo Number of jobs: $n
echo ----
for i in `seq 1 $n`; do
  # rupvar, seismogram, peakval triples
  echo "Running seismogram..."
  echo seismogram $seisargs $subx $suby $1 ${@:(1+n):1} > /dev/stderr
  seismogram $seisargs $subx $suby $1 ${@:(1+n):1}
  if [ ! $? -eq 0 ]; then
    exitcode=$?
    echo "seismogram failed... Code: $exitcode" > /dev/stderr
  else
    echo "seismogram succeed..." > /dev/stderr
  fi
  echo "Running peakcalc..."
  echo peakcalc $peakargs ${@:(1+n):1} ${@:(1+n*2):1} > /dev/stderr
  peakcalc $peakargs ${@:(1+n):1} ${@:(1+n*2):1}
  if [ ! $? -eq 0 ]; then
    exitcode=$?
    echo "Failed peakcalc... Code: $exitcode" > /dev/stderr
    #/bin/dd if=/dev/zero of=${@:(1+n*2):1} count=1 bs=24000
    #exit $?
  else
    echo "peakcalc succeed..." > /dev/stderr
  fi
  shift 1
done

