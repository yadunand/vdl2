#!/bin/bash

#SEISMOGRAM=/gpfs/pads/swift/aespinosa/science/cybershake/apps/JBSim3d/bin/jbsim3d
#PEAKCALC=/gpfs/pads/swift/aespinosa/science/cybershake/apps/SpectralAcceleration/p2utils/surfseis_rspectra

SEISMOGRAM=/home/ketan/cybershake/post/JBSim3d/bin/jbsim3d
PEAKCALC=/home/ketan/cybershake/post/SpectralAcceleration/p2utils/surfseis_rspectra

echo $ARGS

$SEISMOGRAM ${@:1:11}
$PEAKCALC ${@:12:12}
