#!/home/ketan/ruby-install/bin/ruby

require 'erb'
require 'ostruct'

# File: mk_catalog.rb
# Description: Generates sites.xml and tc.data for the cybershake workflow

# starting ports for the templates
coaster_service = 65000
worker_service  = 64000

swift_tc = %q[
# Utility apps
localhost getsite          /home/aespinosa/workflows/cybershake/getsite.rb          INSTALLED INTEL32::LINUX null
localhost getrupture       /home/aespinosa/workflows/cybershake/getrupture.rb       INSTALLED INTEL32::LINUX null
localhost variation_mapper /home/aespinosa/workflows/cybershake/variation_mapper.rb INSTALLED INTEL32::LINUX null
localhost mkoffset         /home/aespinosa/workflows/cybershake/offset.rb           INSTALLED INTEL32::LINUX null

# PADS
PADS      extract        /gpfs/pads/swift/aespinosa/science/cybershake/apps/JBSim3d/bin/jbsim3d                            INSTALLED INTEL32::LINUX GLOBUS::maxwalltime="00:05:00"
PADS      seispeak_local /home/aespinosa/Documents/cybershake/post/seispeak.sh                                             INSTALLED INTEL32::LINUX GLOBUS::maxwalltime="00:10:00"


<% ctr = 0
   sites.each_key do |name|
     jm       = sites[name].jm
     url      = sites[name].url
     app_dir  = sites[name].app_dir
     data_dir = sites[name].data_dir
     throttle = sites[name].throttle %>
<%= name %> seismogram       <%= app_dir %>/JBSim3d/bin/jbsim3d                            INSTALLED INTEL32::LINUX GLOBUS::maxwalltime="00:05:00"
<%= name %> surfeis_rspectra <%= app_dir %>/SpectralAcceleration/p2utils/surfseis_rspectra INSTALLED INTEL32::LINUX GLOBUS::maxwalltime="00:05:00"
<%= name %> seispeak         <%= app_dir %>/seispeak.sh                                    INSTALLED INTEL32::LINUX GLOBUS::maxwalltime="00:10:00"
<%= name %> seispeak_agg     <%= app_dir %>/agg_seispeak.sh                                INSTALLED INTEL32::LINUX GLOBUS::maxwalltime="04:00:00"
<%   ctr += 1
   end %>
]

coaster_sites = %q[
<config>
  <pool handle="localhost">
    <filesystem provider="local" />
    <execution provider="local" />
    <workdirectory >/var/tmp</workdirectory>
    <profile namespace="karajan" key="jobThrottle">0.20</profile>
  </pool>

  <pool handle="PADS">
    <execution provider="coaster-persistent" url="http://communicado.ci.uchicago.edu:<%= coaster_service - 1 %>" jobmanager="local:local" />

    <profile namespace="globus" key="workerManager">passive</profile>

    <profile namespace="karajan" key="initialScore">10000.0</profile>
    <profile namespace="karajan" key="jobThrottle">3.66</profile>

    <profile namespace="globus" key="lowOverallocation">36</profile>

    <gridftp  url="local://localhost"/>
    <workdirectory>/gpfs/pads/swift/aespinosa/swift-runs</workdirectory>
  </pool>
<% ctr = 0
   sites.each_key do |name|
     jm       = sites[name].jm
     url      = sites[name].url
     app_dir  = sites[name].app_dir
     data_dir = sites[name].data_dir
     throttle = sites[name].throttle %>

  <pool handle="<%=name%>">
    <execution provider="coaster-persistent" url="http://localhost:<%= coaster_service + ctr %>" jobmanager="local:local" />

    <profile namespace="globus" key="workerManager">passive</profile>

    <profile namespace="karajan" key="initialScore">10000</profile>
    <profile namespace="karajan" key="jobThrottle"><%=throttle%></profile>
    <profile namespace="globus" key="jobsPerNode">16</profile>
    <gridftp  url="gsiftp://<%=url%>"/>
    <workdirectory><%=data_dir%>/swift_scratch</workdirectory>
  </pool>
<%   ctr += 1
   end %>
</config>
]

condor_sites = %q[
<config>
  <pool handle="localhost">
    <filesystem provider="local" />
    <execution provider="local" />
    <workdirectory >/var/tmp</workdirectory>
    <profile namespace="karajan" key="jobThrottle">0.20</profile>
  </pool>

  <pool handle="PADS">
    <execution provider="coaster-persistent" url="https://communicado.ci.uchicago.edu:<%= coaster_service - 1 %>"
        jobmanager="local:local" />

    <profile namespace="globus" key="workerManager">passive</profile>

    <profile namespace="karajan" key="initialScore">10000.0</profile>
    <profile namespace="karajan" key="jobThrottle">3.66</profile>

    <profile namespace="globus" key="lowOverallocation">36</profile>

    <gridftp  url="local://localhost"/>
    <workdirectory>/gpfs/pads/swift/aespinosa/swift-runs</workdirectory>
  </pool>
<% ctr = 0
   sites.each_key do |name|
     jm       = sites[name].jm
     url      = sites[name].url
     app_dir  = sites[name].app_dir
     data_dir = sites[name].data_dir
     throttle = sites[name].throttle %>

  <pool handle="<%=name%>">
    <execution provider="condor" url="none"/>

    <profile namespace="globus" key="jobType">grid</profile>
    <profile namespace="globus" key="gridResource">gt2 <%=url%>/jobmanager-<%=jm%></profile>

    <profile namespace="karajan" key="initialScore">20.0</profile>
    <profile namespace="karajan" key="jobThrottle"><%=throttle%></profile>
    <% if name =~ /FNAL_FERMIGRID/ %>
      <profile namespace="globus" key="condor_requirements">GlueHostOperatingSystemRelease =?= "5.3" && GlueSubClusterName =!= GlueClusterName</profile>
    <% end %>

    <gridftp  url="gsiftp://<%=url%>"/>
    <workdirectory><%=data_dir%>/swift_scratch</workdirectory>
  </pool>
<%   ctr += 1
   end %>
</config>
]

def ress_query(class_ads)
  cmd = "condor_status -pool engage-central.renci.org"
  class_ads[0..-2].each do |class_ad|
    cmd << " -format \"%s|\" #{class_ad}"
  end
  cmd << " -format \"%s\\n\" #{class_ads[-1]}"
  `#{cmd}`
end

def ress_parse
  dir_suffix = "/engage/scec"
  class_ads  = [
    "GlueSiteUniqueID", "GlueCEInfoHostName", "GlueCEInfoJobManager",
    "GlueCEInfoGatekeeperPort", "GlueCEInfoApplicationDir", "GlueCEInfoDataDir",
    "GlueCEInfoTotalCPUs"
  ]
  ress_query(class_ads).each_line do |line|
    line.chomp!
    next if line == ""
    set = line.split("|")

    value = OpenStruct.new

    value.jm       = set[class_ads.index("GlueCEInfoJobManager")]
    value.url      = set[class_ads.index("GlueCEInfoHostName")]
    value.total    = set[class_ads.index("GlueCEInfoTotalCPUs")].to_i
    next if value.total == 999999
    value.throttle = (value.total.to_f - 2.0) / 100.0
    name           = set[class_ads.index("GlueSiteUniqueID")] + "__" +  value.url

    value.app_dir = set[class_ads.index("GlueCEInfoApplicationDir")]
    value.app_dir.sub!(/\/$/, "")
    value.data_dir = set[class_ads.index("GlueCEInfoDataDir")]
    value.data_dir.sub!(/\/$/, "")

    value.app_dir += dir_suffix
    value.data_dir += dir_suffix

    # Hard-wired exceptions
    value.app_dir  = "/osg/app"                     if name =~ /GridUNESP_CENTRAL/
    value.data_dir = "/osg/data"                    if name =~ /GridUNESP_CENTRAL/
    value.app_dir.sub!(dir_suffix, "/engage-scec")  if name =~ /BNL-ATLAS/
    value.data_dir.sub!(dir_suffix, "/engage-scec") if name =~ /BNL-ATLAS/

    yield name, value
  end
end

def dump(file, template, binding)
  file_out = File.open(file, "w")
  file_out.puts ERB.new(template, 0, "%<>").result(binding)
  file_out.close
end

# Non-working sites
blacklist = []
# Only these sites
whitelist = IO.readlines(ARGV[0]).map { |line| line.chomp }

# Removes duplicate site entries (i.e. multilpe GRAM endpoints)
sites = {}
ress_parse do |name, value|
  next if blacklist.index(name) and not blacklist.empty?
  next if not whitelist.index(name) and not whitelist.empty?
  sites[name] = value if sites[name] == nil
end

dump("coaster_osg.xml", coaster_sites, binding)
dump("condor_osg.xml", condor_sites, binding)
dump("tc.data", swift_tc, binding)
