#!/home/ketan/ruby-install/bin/ruby

$: << "/scratch/local/ketan/cybershake"
require 'cybershake'
require 'optparse'
require 'memcached'
require 'digest/md5'

options = {}
OptionParser.new do |opts|
  opts.on("-e", "-erf") { |v| options[:erf] = v.to_i}
  opts.on("-v", "-var_scen") { |v| options[:variation_scenario] = v.to_i}
  opts.on("-s", "-source") { |v| options[:source] = v.to_i}
  opts.on("-r", "-rupture") { |v| options[:rupture] = v.to_i}
  opts.on("-l", "-location") { |v| options[:location] = v }
end.parse!

cache = Memcached.new
digest = Digest::MD5.hexdigest(options.to_s).to_s

begin
  variations = cache.get(digest)
rescue Memcached::NotFound
  variations = Variation.find(:all, :conditions => {
      :Rup_Var_Scenario_ID => options[:variation_scenario],
      :ERF_ID => options[:erf], 
      :Source_ID => options[:source], :Rupture_ID => options[:rupture] },
      :order => "Rup_Var_ID")
  cache.set(digest, variations)
end
location = options[:location]
variations.each do |var|
  puts  "#{location}/#{options[:source]}/#{options[:rupture]}/#{var.Rup_Var_LFN.sub(/e35_rv3_/,'')}"
end
