type datafile;
type outfile;
type cvmfile;
type e3dfile;

/* Following are the output files from the precvm process */
datafile coord<"processed/coords_GC_TEST">;
datafile gridfiletest<"processed/gridfile_TEST">;
datafile gridout<"processed/gridout_TEST">;
datafile model<"processed/model">;
datafile modelparam<"processed/model_params_GC_TEST">;
datafile modelbox_file<"processed/TEST.modelbox">;

/*Following file is the input to preSGT and vmeshgen, do not know the source of this file*/
datafile model_coord<"data/model_coords_GC_TEST">;


datafile modelbound<"processed/model_bounds_GC_TEST">;

/* Following are the outputs from preSGT*/
datafile fdloc<"processed/TEST.fdloc">;
datafile faultlist<"processed/TEST.faultlist">;
datafile radiusfile<"processed/TEST.radiusfile">;
datafile cordfile<"processed/TEST.cordfile">;

/* Following are the outputs from the vmeshmerge but are represented as strings in the commandline */
datafile sgt_p<"processed/v4_sgt-TEST.p">;
datafile sgt_s<"processed/v4_sgt-TEST.s">;
datafile sgt_d<"processed/v4_sgt-TEST.d">;
datafile gridout2<"processed/gridout">;

/* These files are created by genmod aka vmeshgen */
datafile zdepths<"processed/zdepths.meter">;
cvmfile cvms[] <simple_mapper; location="processed", prefix="v4_sgt-", suffix=".cvm">;

/* These files are produced as a result of simsgt */
e3dfile e3dsx[] <simple_mapper; location="processed", prefix="TEST-fx_sgt-", suffix=".e3d">;
e3dfile e3dsy[] <simple_mapper; location="processed", prefix="TEST-fy_sgt-", suffix=".e3d">;

/* Final outputs */
datafile merged_sgt_fx<"TEST_fx_816.sgt">;
datafile merged_sgt_fy<"TEST_fy_816.sgt">;

/* Standard outputs */
outfile outvmeshgen<"stdout/vmeshgen.std.out">;
outfile outprecvm<"stdout/precvm.std.out">;
outfile outpresgt<"stdout/presgt.std.out">;
outfile outvmeshmerge<"stdout/vmeshmerge.std.out">;
outfile outsimsgtx<"stdout/simsgtx.std.out">;
outfile outsimsgty<"stdout/simsgty.std.out">;
outfile outsgtmergex<"stdout/sgtmergex.std.out">;
outfile outsgtmergey<"stdout/sgtmergey.std.out">;


app (outfile o, datafile modelbox, datafile gridfile_TEST, datafile gridout_TEST, datafile model_coords_GC_TEST, datafile model_params_GC_TEST, datafile model_bounds_GC_TEST) precvm (string region, int count){
  precvm region count @modelbox 
         @gridfile_TEST @gridout_TEST
         @model_coords_GC_TEST @model_params_GC_TEST
         @model_bounds_GC_TEST stdout=@o;

}

app (outfile o) PreSgt (string region, string count, datafile modelbox, datafile gridout_data, datafile model_coord_data, datafile fdloc_data, datafile _faultlist, datafile _radiusfile, datafile _cordfile, string nx, string ny, string nz, string h, string xsrc, string ysrc, string ixmin, string ixmax, string iymin, string iymax, string izstart, string izmax, string modellon, string modellat, string modelrot){

  presgt region count 
       @modelbox @gridout_data 
       @model_coord_data @fdloc_data 
       @_faultlist @_radiusfile 
       @_cordfile 
       "cqsub"
       "-q prod-devel -n 1 -t 60"
       "/scratch/projects/tg/tera3d/CyberShake2007/software-9.2.0/PreSgt/bin/gen_sgtgrid"
       @strcat("nx=", nx) @strcat("ny=", ny)
       @strcat("nz=", nz) @strcat("h=", h)
       @strcat("xsrc=", xsrc) @strcat("ysrc=", ysrc) 
       @strcat("ixmin=", ixmin) @strcat("ixmax=", ixmax) 
       @strcat("iymin=", iymin) @strcat("iymax=", iymax)
       @strcat("izstart=", izstart) 
       @strcat("izmax=", izmax)
       @strcat("radiusfile=", @filename(_radiusfile))
       @strcat("outfile=", @filename(_cordfile))
       @strcat("modellon=", modellon) 
       @strcat("modellat=", modellat) 
       @strcat("modelrot=", modelrot)
       @strcat("faultlist=", @filename(_faultlist))
       stdout=@o;
}

app (outfile o) vmeshgen (string region, datafile gridout_data, datafile _model_coord){

  genmod region @gridout_data @_model_coord stdout=@o;
}

app (outfile o) vmeshmerge (string region, datafile gridfile_TEST, string _sgtp, string _sgts, string _sgtd, outfile _outvmeshgen){

  mergecvm region @gridfile_TEST _sgtp _sgts _sgtd stdout=@o;
}

app (outfile o) simsgt (string region, datafile gridout_test, datafile modelbox, datafile _fdloc, datafile _cordfile, string x_or_y, outfile _outvmeshmerge){

simsgt region @gridout_test @modelbox @_fdloc @_cordfile x_or_y stdout=@o;
}

app (outfile o) mergesgt (string region, string x_or_y, string outsgt, outfile _outsimsgt_x_or_y, string test_fx_or_fy_816){

mergesgt region test_fx_or_fy_816 x_or_y outsgt stdout=@o;
}

/* Execution starts here */

//(outprecvm, modelbox_file, gridfiletest, gridout, coord, modelparam, modelbound) = precvm ("TEST", 35);


/* This process implicitly also refers to rupture variations, this means the rupture variations location should also be unique!! */
//outpresgt = PreSgt ("TEST", "35", modelbox_file, gridout, coord, fdloc, faultlist, radiusfile, cordfile, "800", "1550", "200", "0.200000", "510", "492", "20", "780", "20", "1530", "1", "180", "-117.921940", "33.566690", "-55.000000");

//outvmeshgen = vmeshgen ("TEST", gridout, model_coord);

/* The strings "v4_sgt-TEST.p", "v4_sgt-TEST.s", "v4_sgt-TEST.d" are the output files names generated by the app */
//outvmeshmerge = vmeshmerge ("TEST", gridfiletest, "v4_sgt-TEST.p", "v4_sgt-TEST.s", "v4_sgt-TEST.d", outvmeshgen);

outsimsgtx = simsgt ("TEST", gridout, modelbox_file, fdloc, cordfile, "x", outvmeshmerge); 
outsimsgty = simsgt ("TEST", gridout, modelbox_file, fdloc, cordfile, "y", outvmeshmerge);

/* The inputs to these are TEST-f${COMPONENT}_sgt-<num>.e3d */
//(outsgtmergex) = mergesgt ("TEST", "x", "/work/01739/ketan/scec/swift/SGT", outsimsgtx, "TEST_fx_816.sgt");
//(outsgtmergey) = mergesgt ("TEST", "y", "/work/01739/ketan/scec/swift/SGT", outsimsgty, "TEST_fy_816.sgt");

