/* == Type Declarations == */
type Variation;
type StationFile;
type RuptureFile;
type VariationFile;

type file;

type Station {
    string name;
    float lat;
    float lon;
    int erf;
    int variation_scenario;
}

type Rupture {
    int source;
    int index;
    int size;
}

app (StationFile _stat) getsite_file(int _run_id) {
    getsite _run_id stdout=@filename(_stat);
}

(Station _stat) get_site(int _run_id) {
    StationFile file<"/var/tmp/site_tmp">;
    file = getsite_file(_run_id);
    _stat = readData(file);
}

app (RuptureFile _rup) getrupture_file(int _run_id) {
    getrupture _run_id stdout=@filename(_rup);
}

(Rupture _rup[]) get_ruptures(int _run_id, Station _site) {
    RuptureFile file<single_file_mapper; file=@strcat(_site.name, "/rup_tmp")>;
    file = getrupture_file(_run_id);
    _rup = readData(file);
}

app (VariationFile _var) getvariation_file(Station _site, Rupture _rup,
        string _loc) {
    variation_mapper "-e" _site.erf "-v" _site.variation_scenario
        "-l" _loc "-s" _rup.source "-r" _rup.index stdout=@_var;
}

(string _vars[]) get_variations(Station _site, Rupture _rup, string _loc){
    string fname = @strcat(_rup.source, "_", _rup.index);
    VariationFile file<single_file_mapper;
    file=@strcat(_site.name, "/varlist/", _rup.source, "/", fname, ".txt")>;
    file = getvariation_file(_site, _rup, _loc);
    _vars = readData(file);
}

app (file o) cat (Variation _var)
{
  cat @filename(_var) stdout=@o;
}

file outfile[]<simple_mapper; location="outdir", prefix="f.",suffix=".out">;


/* Main program */
int run_id = 644;

Station site = get_site(run_id);

Rupture rups[] = get_ruptures(run_id, site);

foreach rup, rup_idx in rups {
   string var_str[] = get_variations( site, rup, "/gpfs/pads/swift/aespinosa/science/cybershake/RuptureVariations" );
    Variation vars[] <array_mapper; files=var_str>;
   // trace(@filename(vars[rup.size-1]));
    outfile[rup_idx] = cat(vars[rup.size-1]);            
}

