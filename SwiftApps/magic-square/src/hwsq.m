function m = hwsq(infile,outfile,fac)
n = dlmread(infile)
if ischar(fac)
    fac=str2num(fac);
end
m = myf1(n,fac)
dlmwrite(outfile,m)
