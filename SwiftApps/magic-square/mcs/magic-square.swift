type file;

app (file outdata) hwsq (file indata, int factor)
{
  runhwsq @indata @outdata factor;
}

file degreeData<"degree.dat">;

int factors[] = [0:9];
file squareMats[] <simple_mapper; prefix="sqmat.",suffix=".dat">;

foreach f, i in factors {
  squareMats[i] = hwsq (degreeData, f);
}

