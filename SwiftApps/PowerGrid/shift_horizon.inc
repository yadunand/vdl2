# power flow equations
# shift horizon for economic dispatch (used for year-long simulations)
# V. M. Zavala - MCS/ANL 2010

# collect some results
let            clgen_cost[idx_cl+1] := tot_gen_cost[1,1];
let{i in BUS}      lmps[i,idx_cl+1] := pfeq.dual[1,1,i];
let          clslack_cost[idx_cl+1] := slack_cost[1];


