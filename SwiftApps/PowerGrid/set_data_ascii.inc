# power flow equations
# set model data
# V. M. Zavala - MCS/ANL 2010

# read line data
table Lines_data IN "/homes/ketan/victor-work/SwiftTrans/Illinois/Lines_data.tab": 
LIN <- [LIN], status, snd_bus, rec_bus, X, V, Pmax, install_cost;
read table Lines_data;

# read bus data
table bus_data IN "/homes/ketan/victor-work/SwiftTrans/Illinois/bus_data.tab": 
BUS <- [BUS], lat_bus, lon_bus;
read table bus_data;

# read generator data
table Gen_data IN "/homes/ketan/victor-work/SwiftTrans/Illinois/Gen_data.tab": 
GEN <- [GEN], bus_gen, np_cap, sum_cap, win_cap, min_cap, fuel, min_hrate,
              min_power, max_ur,  max_dr, lat_gen, lon_gen;
read table Gen_data;

# dispatch horizon data
read  T < "/homes/ketan/victor-work/SwiftTrans/Illinois/dispatch_data.dat";
read  H < "/homes/ketan/victor-work/SwiftTrans/Illinois/dispatch_data.dat";
read NS < "/homes/ketan/victor-work/SwiftTrans/Illinois/dispatch_data.dat";
read{i in GEN} Pgen_init[i] < "/homes/ketan/victor-work/SwiftTrans/Illinois/dispatch_data.dat";
close "/homes/ketan/victor-work/SwiftTrans/Illinois/dispatch_data.dat"; 

# read fuel data
table fuel_data IN "/homes/ketan/victor-work/SwiftTrans/Illinois/fuel_data.tab":
FUEL <- [FUEL], Description, HV, Unitprice, Units;
read table fuel_data;

# read load data
table time_load IN "/homes/ketan/victor-work/SwiftTrans/Illinois/time_load.tab":     TIME <- [TIME];
read table time_load;

table load_load IN "/homes/ketan/victor-work/SwiftTrans/Illinois/load_load.tab":     LOAD <- [LOAD], bus_load;
read table load_load;

#table testtime_load IN "/homes/ketan/victor-work/SwiftTrans/Illinois/testtime_load.tab": TESTTIME <- [TESTTIME];
#read table testtime_load;

read{i in TIME,j in LOAD} loads[i,j] < "/homes/ketan/victor-work/SwiftTrans/Illinois/Loads.dat";
close Illinois/Loads.dat;

# read wind data
table wind_data IN "/homes/ketan/victor-work/SwiftTrans/Illinois/wind_data.tab":
WIND <- [WIND], bus_wind, wind_share;
read table wind_data;

read{i in TIMEp} wind_total[1,i] < "/homes/ketan/victor-work/SwiftTrans/Illinois/wind_data.dat";
close Illinois/wind_data.dat;

# compute generation costs ($/MW)
let{i in GEN} gen_cost[i] := 1e-3*(min_hrate[i]/HV[fuel[i]])*Unitprice[fuel[i]];

# generate wind scenarios - artificial - 
let{s in SCEN, t in TIMEp: s   >  1} wind_total[s,t] := Normal(wind_total[1,t],wind_total[1,t]*0.01);
# current wind is known 
let{s in SCEN, t in TIMEp: t  ==  1} wind_total[s,t] := wind_total[1,t];
