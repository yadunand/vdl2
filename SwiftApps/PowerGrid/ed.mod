# power flow equations
# ampl model declaration
# V. M. Zavala - MCS/ANL 2010

# scenarios 
param NS;                        # number of scenarios 
set SCEN := 1..NS;               # set of scenarios 

# dispatch horizon
param T;                         # total number of time steps in closed-loop simulation
param H;                         # horizon length  of dispatch horizon
param dt;                        # length of time step [hr]
set TIMEp:=1..H;                 # set of times in dispatch horizon                       
set TIMEm := 1..H-1;             # set of times in dispatch horizon minus one
param new_cycle;

# lines
set LIN;                         # set of lines
param  status{LIN}, symbolic;    # line status
param snd_bus{LIN};              # sending bus
param rec_bus{LIN};              # receiving bus
param       X{LIN};              # reactance - Ohm
param       V{LIN};              # voltage drop - kV
param    Pmax{LIN};              # maximum line capacity - MW
param install_cost{LIN};         # annualized installation cost $/yr 

# buses
set BUS;                         # set of buses
param lat_bus{BUS};              # latitude of bus 
param lon_bus{BUS};              # longitude of bus
param      ref_bus, symbolic;    # reference bus

# generators
set GEN;
param    bus_gen{GEN};            # generator bus
param     np_cap{GEN};            # name place capacity - MW
param    sum_cap{GEN};            # summer capacity - MW
param    win_cap{GEN};            # winter capacity - MW
param    min_cap{GEN};            # minimum capacity - MW
param       fuel{GEN}, symbolic;  # type of fuel for generator
param  min_hrate{GEN};            # heat rate at minimum capacity - BTU/kWh 
param  min_power{GEN};            # power at minimum capacity - MW
param     max_ur{GEN};            # minimum up ramp - MW
param     max_dr{GEN};            # maximum down ramp - MW
param    lat_gen{GEN};            # latitude of generation bus
param    lon_gen{GEN};            # longitude of generation bus
param   gen_cost{GEN};            # generation cost $/MWhr
param  Pgen_init{GEN};            # initial conditions generators MW

# fuels
set FUEL;
param   Description{FUEL}, symbolic;  # fuel description
param            HV{FUEL};            # heating value (Mbtu/unit)
param     Unitprice{FUEL};            # unit price ($/unit)
param         Units{FUEL}, symbolic;  # units

# loads
set TIME;                        # set of times for loads
set LOAD;                        # set of loads
set TESTTIME:=1..T;              # set of test times
param   bus_load{LOAD};          # load bus
param loads{TIME,LOAD};          # loads in space and time - MW

# wind data
set WIND;
param wind_total{SCEN,TIMEp};       # total wind power - MW
param   bus_wind{WIND};             # wind bus
param wind_share{WIND};             # wind share of total power MW/MW
 
# collection of data from closed-loop simulation
param clgen_cost{TESTTIME};           # closed-loop generation cost $/MWh 
param clslack_cost{TESTTIME};         # closed-loop infeasibility cost $/MWh 
param lmps{BUS,TESTTIME};             # closed-loop lmps $/MWh 
 
# variables
var  Pgen{SCEN,TIMEp,j in  GEN}  >= 0, <= np_cap[j];                                # generation levels - MW
var Pload{SCEN,TIMEp,j in LOAD};                                                    # load levels - MW
var theta{SCEN,TIMEp,j in  BUS}  >= -3.14/2, <= 3.14/2;                             # bus angles - radians (keep it bounded by 90 degrees)
var     P{SCEN,TIMEp,j in  LIN};                                                    # power flows - per unit MW (keep bounded by line capacity)

var     slackp{TIMEp,BUS}>=0;
var     slackm{TIMEp,BUS}>=0;

# power flow equations
pfeq{s in SCEN, t in TIMEp, j in BUS}:  
         +(sum{i in LIN:  j ==  rec_bus[i]}     P[s,t,i])
         -(sum{i in LIN:  j ==  snd_bus[i]}     P[s,t,i])
         +(sum{i in GEN:  j ==  bus_gen[i]}  Pgen[s,t,i]) 
         -(sum{i in LOAD: j == bus_load[i]} Pload[s,t,i]) = slackp[t,j]-slackm[t,j];
         
# angle at reference bus
pgeq_ref{s in SCEN,t in TIMEp}:  
    theta[s,t,ref_bus] = 0;

# power flows [MW]
Peq{s in SCEN,t in TIMEp,i in LIN}: 
    P[s,t,i] - V[i]*V[i]*(theta[s,t,snd_bus[i]]-theta[s,t,rec_bus[i]])/X[i] =  0;    
         
# upper bound power flows [MW]
Pequb{s in SCEN,t in TIMEp,i in LIN}:  P[s,t,i] <=  Pmax[i];

# lower bound power flows [MW]
Peqlb{s in SCEN,t in TIMEp,i in LIN}:  P[s,t,i] >= -Pmax[i];

# non-anticipativity constraints - for first time step - 
 nonant_gen{s in SCEN,t in TIMEm, i in  GEN: t == 1}:  Pgen[s,t,i] =  Pgen[1,t,i]; 
nonant_load{s in SCEN,t in TIMEm, i in LOAD: t == 1}: Pload[s,t,i] = Pload[1,t,i]; 

# individual costs
var   tot_gen_cost{s in SCEN,t in TIMEp} = (sum{i in GEN}  gen_cost[i]*Pgen[s,t,i]); 
var               slack_cost{t in TIMEp} = 1e3*sum{j in BUS} (slackp[t,j]+slackm[t,j]);

# cost function
minimize cost: sum{s in SCEN,t in TIMEp} (tot_gen_cost[s,t]+slack_cost[t]); 
