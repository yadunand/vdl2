type datfile;
type result_file;

app (result_file r) ed_app(datfile d){
 runedipopt @d stdout=@r;
}

//array file mapper mapping from a datfile var to datfiles.
datfile datafiles[] <simple_mapper; location="params", prefix="swift_param", suffix=".dat">;
result_file resfiles[]<simple_mapper; location="results", prefix="result", suffix=".res">;

//run in a foreach loop
foreach d, idx in datafiles {

//  result_file resfiles[]<simple_mapper; location="results", prefix="result", suffix=".res">;
  resfiles[idx] = ed_app(d);

}

