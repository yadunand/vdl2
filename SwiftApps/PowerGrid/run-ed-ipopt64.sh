#!/bin/bash

/home/vzavala/ampl64 <<END
reset;
model /homes/ketan/victor-work/SwiftTrans/ed.mod;
option solver "/home/ketan/CoinIpopt/bin/ipopt";

include /homes/ketan/victor-work/SwiftTrans/set_data_ascii.inc;

let ref_bus := 22671; 

let{s in SCEN,t in TIMEp, j in GEN}   Pgen[s,t,j]:= np_cap[j]/2;
let{s in SCEN,t in TIMEp, j in BUS}  theta[s,t,j]:= 0.1;

param idx_cl;

read idx_cl < ${1};
close ${1};

fix{s in SCEN, t in TIMEp, j in LOAD}  Pload[s,t,j] := loads[t+idx_cl*(H+0),j];

solve;

include /homes/ketan/victor-work/SwiftTrans/shift_horizon.inc;

include /homes/ketan/victor-work/SwiftTrans/print_results.inc;
END

