clc
clear all
close all 

% load Loads.dat;
% 
% siz = size(Loads);
% 
% for j=1:siz(1)
% 
%     totload(j) = sum(Loads(j,:));
%     
% end

load total_loads.dat;
load wind_data.dat;

totgen = linspace(216884,216884,8760);

price = 0.07*1000; % $/MWh
avgdailycost = mean(total_loads*price)*24

semilogy(total_loads,'black','LineWidth',1.2)
hold on
semilogy(1.7*wind_data,'Color',[0.4 0.4 0.4])
hold on
semilogy(2.2*wind_data,'Color',[0.4 0.4 0.4])
hold on
semilogy(3.3*wind_data,'Color',[0.4 0.4 0.4])
hold on
semilogy(totgen,'b--','LineWidth',2)
grid on
xlabel('Time [hr]')
ylabel('Power [MW]')
text(5*24+3,0.9e5,'Load','Fontsize',10)
text(5*24+3,4e4,'30% Wind','Fontsize',10)
text(5*24+3,2.7e4,'20% Wind','Fontsize',10)
text(5*24+3,1.9e4,'10% Wind','Fontsize',10)
axis([0 5*24+2 4e3 1.5e5])
set(gca,'FontSize',14)
set(get(gca, 'xlabel'), 'FontSize', 14)
set(get(gca, 'ylabel'), 'FontSize', 14)
print -depsc load_gen.eps

figure(2)
subplot(2,1,1)
plot(total_loads,'Color',[0.6 0.6 0.6])
xlabel('Time [hr]')
ylabel('Load [MW]')
axis([0 8760 0.6e5 1.7e5])
print -depsc load_total_illinois.eps
