# power flow equations
# print results for analysis (year-long simulation)
# V. M. Zavala - MCS/ANL 2010

for{i in BUS} { 
printf{t in idx_cl+1..idx_cl+1}: "%e ",  max(-100,min(lmps[i,t],500))  > /homes/ketan/victor-work/SwiftTrans/Results/lmp_results.dat; printf "\n " > /homes/ketan/victor-work/SwiftTrans/Results/lmp_results.dat;} 
close /homes/ketan/victor-work/SwiftTrans/Results/lmp_results.dat;

printf{t in idx_cl+1..idx_cl+1}: "%e ",    clgen_cost[t] > /homes/ketan/victor-work/SwiftTrans/Results/gencost_results.dat; printf "\n " > /homes/ketan/victor-work/SwiftTrans/Results/gencost_results.dat;
close /homes/ketan/victor-work/SwiftTrans/Results/gencost_results.dat;

printf{t in idx_cl+1..idx_cl+1}: "%e ",  clslack_cost[t] > /homes/ketan/victor-work/SwiftTrans/Results/slackcost_results.dat; printf "\n " > /homes/ketan/victor-work/SwiftTrans/Results/slackcost_results.dat;
close /homes/ketan/victor-work/SwiftTrans/Results/slackcost_results.dat;

