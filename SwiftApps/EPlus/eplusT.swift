import files;
import io;
import string;
import sys;

app (file _epout, file _epoutxml) runEP (file _imf, file _epw, int _orientation, int _height)
{
  /*runenergyplus CHICAGO-EXAMPLE.imf CHICAGO.epw*/
  "/home/ketan/EPlus/examples/RunEP.sh" "--imf" _imf "--epw" _epw  "--out" _epout "--outxml" _epoutxml
            "--params" "ORIENTATION" _orientation "HEIGHT" _height;
}

app (file _outjson) postproc (file _xml, file _parser) {
  "/home/ketan/EPlus/examples/postproc.sh" "--xml" _xml "--parser" _parser "--outjson" _outjson;
}

main {
 file epconfig = input_file ("/home/ketan/EPlus/examples/CHICAGO-EXAMPLE.imf");
 file epweather = input_file ("/home/ketan/EPlus/examples/CHICAGO.epw");
 file parser = input_file ("/home/ketan/EPlus/examples/parse.js");
 file epout[][];
 file epoutxml[][];
 file epoutjson[][];

 foreach orientation in [0:20:10] {
   foreach height in [1:2] {
    file out<sprintf("ep.%i.%i.out.tgz", orientation, height)>;
    file outxml<sprintf("ep.%i.%i.xml", orientation, height)>;
    file outjson<sprintf("ep.%i.%i.json", orientation, height)>;
    (out, outxml) = runEP (epconfig, epweather, orientation, height);
    epout[orientation][height] = out;
    epoutxml[orientation][height] = outxml;
    outjson = postproc (outxml, parser);
    epoutjson[orientation][height] = outjson;
  }
}
}
