#! /bin/bash

set -x

installModule()
{
 $HOME/node-v0.10.20-linux-x64/bin/npm install xml2json
}

echo starting

usage="$0 --xml xmlfile --parser parserfile --outjson outjsonfile" 

while [ $# -gt 0 ]; do
  case $1 in
    --xml) xml=$2; shift 2;;
    --parser) parser=$2; shift 2;;
    --outjson) outjson=$2; shift 2;;
    *) echo $usage 1>&2
     exit 1;;
  esac
done

TASKDIR=$PWD
WORKDIR=$(mktemp -d $OSG_WN_TMP/RunEP.XXXXXX)
ABSPARSER=$(cd $(dirname $parser); pwd)/$(basename $parser)
ABSOUTJSON=$(cd $(dirname $outjson); pwd)/$(basename $outjson)
ABSXML=$(cd $(dirname $xml); pwd)/$(basename $xml)

cd $WORKDIR
#installModule
cp $ABSPARSER parse.js
cp $ABSXML eplustbl.xml

$HOME/node-v0.10.20-linux-x64/bin/node parse.js

RC=$?
cp $WORKDIR/output.json $ABSOUTJSON

cd $WORKDIR
rm -rf $WORKDIR
exit $RC
        
