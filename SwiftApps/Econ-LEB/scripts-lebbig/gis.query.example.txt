<wfs:GetFeature service="WFS" version="1.0.0"  outputFormat="GML2"  xmlns:topp="http://www.openplans.org/topp" xmlns:wfs="http://www.opengis.net/wfs"  xmlns:ogc="http://www.opengis.net/ogc"  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.0.0/WFS-basic.xsd">

  <wfs:Query typeName="sf:changwat" propertyName="topp:SHAPE_AREA">
  <ogc:Filter>
   <ogc:FeatureId fid="changwat.2"/>
  </ogc:Filter>
  </wfs:Query>
</wfs:GetFeature>


<wfs:GetFeature service="WFS" version="1.1.0"
  xmlns:topp="http://www.openplans.org/topp"
  xmlns:wfs="http://www.opengis.net/wfs"
  xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/wfs
                      http://schemas.opengis.net/wfs/1.1.0/wfs.xsd">
  <wfs:Query typeName="sf:villages">
    <Filter>
          <PropertyIsEqualTo>
                <PropertyName>cdd_id</PropertyName>
                <Literal>53070306</Literal>
          </PropertyIsEqualTo>
    </Filter>
    </wfs:Query>
</wfs:GetFeature>


<wfs:GetFeature service="WFS" version="1.1.0"
  xmlns:topp="http://www.openplans.org/topp"
  xmlns:wfs="http://www.opengis.net/wfs"
  xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/wfs
                      http://schemas.opengis.net/wfs/1.1.0/wfs.xsd">
  <wfs:Query typeName="sf:village_survey">
    <Filter>
          <PropertyIsEqualTo>
                <PropertyName>cdd_id</PropertyName>
                <Literal>53070306</Literal>
          </PropertyIsEqualTo>
    </Filter>
    </wfs:Query>
</wfs:GetFeature>


http://serc.ci.uchicago.edu:8080/geoserver/wfs?request=GetFeature&version=1.1.0&typeName=sf:village_survey&propertyName=wlth96,cdd_id&BBOX=26475.33561,1461146.24342,652600.97443,1822726.13925


<!-- Performs a get feature with a bounding box filter.      -->
<!-- The BBOX filter is a convenience for a <Not><Disjoint>, -->
<!-- it fetches all features that spatially interact with the given box. -->
<!-- This example also shows how to request specific properties, in this -->
<!-- case we just get the STATE_NAME and PERSONS -->

<wfs:GetFeature service="WFS" version="1.1.0"
  xmlns:topp="http://www.openplans.org/topp"
  xmlns:wfs="http://www.opengis.net/wfs"
  xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:gml="http://www.opengis.net/gml"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/wfs
                      http://schemas.opengis.net/wfs/1.1.0/wfs.xsd">
  <wfs:Query typeName="sf:village_survey">
    <wfs:PropertyName>cdd_id</wfs:PropertyName>
    <wfs:PropertyName>wlth96</wfs:PropertyName>
    <ogc:Filter>
      <ogc:BBOX>
        <ogc:PropertyName>the_geom</ogc:PropertyName>
        <gml:Envelope srsName="http://www.opengis.net/gml/srs/epsg.xml#24047">
           <gml:lowerCorner>26475.33561 1461146.24342</gml:lowerCorner>
           <gml:upperCorner>652600.97443 1822726.13925</gml:upperCorner>
        </gml:Envelope>
      </ogc:BBOX>
   </ogc:Filter>
  </wfs:Query>
</wfs:GetFeature>



<wfs:GetFeature service="WFS" version="1.1.0"
  xmlns:topp="http://www.openplans.org/topp"
  xmlns:wfs="http://www.opengis.net/wfs"
  xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:gml="http://www.opengis.net/gml"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/wfs
                      http://schemas.opengis.net/wfs/1.1.0/wfs.xsd">
<wfs:Query typeName="sf:village_survey">
<wfs:PropertyName>cdd_id</wfs:PropertyName>
<wfs:PropertyName>wlth96</wfs:PropertyName>
<ogc:Filter>
<ogc:Overlaps>
<ogc:PropertyName>Geometry</ogc:PropertyName>
<gml:Polygon srsName="http://www.opengis.net/gml/srs/epsg.xml#63266405">
<gml:outerBoundaryIs>
<gml:LinearRing>
<gml:posList>100.6347 16.3623 103.13964 15.6865 100.0195 14.4559 100.6347 16.3623</gml:posList>
</gml:LinearRing>
</gml:outerBoundaryIs>
</gml:Polygon>
</ogc:Overlaps>
</ogc:Filter>
</wfs:Query>
</wfs:GetFeature>

<gml:posList>100.634765625 16.36230951024084, 103.1396484375 15.686509572551435,100.01953125 14.455958231194037,100.634765625 16.36230951024084</gml:posList>


<wfs:GetFeature service="WFS" version="1.1.0"
  xmlns:topp="http://www.openplans.org/topp"
  xmlns:wfs="http://www.opengis.net/wfs"
  xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:gml="http://www.opengis.net/gml"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/wfs
                      http://schemas.opengis.net/wfs/1.1.0/wfs.xsd">
<wfs:Query typeName="sf:villages">
<wfs:PropertyName>area</wfs:PropertyName>
<ogc:Filter>
<ogc:Overlaps>
<ogc:PropertyName>the_geom</ogc:PropertyName>
<gml:Polygon srsName="EPSG:24047">
<gml:outerBoundaryIs>
<gml:LinearRing>
<gml:posList>100.6347 16.3623 103.13964 15.6865 100.0195 14.4559 100.6347 16.3623</gml:posList>
</gml:LinearRing>
</gml:outerBoundaryIs>
</gml:Polygon>
</ogc:Overlaps>
</ogc:Filter>
</wfs:Query>
</wfs:GetFeature>


left-bottom=(100.4150390625,14.541049898060399) right-top=(103.9306640625,16.615137799987075)
left-bottom=(98.7451171875,17.874203439657503) right-top=(100.5908203125,19.041348796589013)
left-bottom=(100.37109375,14.413400165206088) right-top=(101.337890625,14.881087159090649)
left-bottom=(96.767578125,5.025282908609294) right-top=(106.611328125,20.159098270646925)

NOT
<wfs:GetFeature service="WFS" version="1.1.0"
  xmlns:topp="http://www.openplans.org/topp"
  xmlns:wfs="http://www.opengis.net/wfs"
  xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:gml="http://www.opengis.net/gml"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/wfs
                      http://schemas.opengis.net/wfs/1.1.0/wfs.xsd">
  <wfs:Query typeName="sf:villages">
    <wfs:PropertyName>area</wfs:PropertyName>
    <ogc:Filter>
      <ogc:BBOX>
        <ogc:PropertyName>the_geom</ogc:PropertyName>
        <gml:Envelope srsName="http://www.opengis.net/gml/srs/epsg.xml#24047">
           <gml:lowerCorner>100.3710 14.4134</gml:lowerCorner>
           <gml:upperCorner>101.3378 14.8810</gml:upperCorner>
        </gml:Envelope>
      </ogc:BBOX>
   </ogc:Filter>
  </wfs:Query>
</wfs:GetFeature>


http://serc.ci.uchicago.edu:8080/geoserver/wfs?request=GetFeature&version=1.1.0&typeName=sf:villages&propertyName=perimeter,area&BBOX=96.7675,5.02528,106.6113,20.1590

bbox=267774.4517477189,533939.4525984447,1304354.4574975406,2361344.09453179&styles=&Format=application/openlayers&request=GetMap&layers=sf:changwat&width=362&height=550&srs=EPSG:24047
