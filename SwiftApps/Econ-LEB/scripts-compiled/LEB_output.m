function LEB_output
global out1 out2;
global in1;

load(in1);
load ent.mat;
load criterion.mat;
load results.mat;

out = fopen(out1, 'w+');
fprintf(out, '%12s%1s%12s%1s%12s%1s%12s\n', 'tambon_id', ',', 'model_ent_level_1994', ',','data_ent_level_1994', ',','model_data_diff');
for i = 1:size(vilid);
	fprintf(out, '%8i%1s%8.6f%1s%8.6f%1s%8.6f\n', vilid(i), ',', en0(i,5), ',',bus2(i,5), ',',en0(i,5)-bus2(i,5));
end;
fclose(out);

out = fopen(out2, 'w+');
fprintf(out, '%12s%8.6f\n','criterion=',crb);
fprintf(out, '%30s\n','Parameters of the model');
for i = 1:size(Para0');
	fprintf(out, '%12.3e\n',Para0(i));
end;
fclose(out);

