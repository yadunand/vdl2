function[bin,his]=binhist(vnr, wealth, F)


%find bin and history
%load vill07c
%load vill07

vilw=wealth;
wsp=linspace(min(vilw),max(vilw),200);

N=hist(vilw,wsp);
nc=cumsum(N);

nc1=nc-vnr;
ind=[1:200];
nc2=(nc1>=0).*ind;
bin=min(nc2(nc2>0));

his=F(vnr,:);
