clear all;

calstr = 'SESBall';
addpath(genpath('../'));
wel = 1; % Set to 1 if data for welfare calculations has to be stored.
dis = 1; % Set to 1 if distribution each period are to be printed.  
fin = 1; % Set to 0 if linear liberalization. 
ini = 0; % Set to 0 if whole distribution is used.
 
load datec;
%groutec;
itec = 1;

wgp = [1 1 1 1]; 

if (strncmp(calstr,'Pr',2) == 1) 
   if ini > 0 
      load Parcalibpry; % y for young  
   else
      load Parcalibpra; % a for all 
   end;
   %       m     omega  beta  alpha   rho    sigma   gamma    xi gamgr nu
   %Par = [.00559 .256  .00330 .97545 999 .00432 .01538 .12905 .0246 0]; 
   %farminc = 10727; 
else
   if ini > 0 
      load Parcalibsesy; 
   else
      load Parcalibsesa; 
   end;
       %    m   omega  beta  alpha   rho      sigma   gamma    xi  gamgr nu
  %Par = [-0.5933 0.321 26.9478 -4.5375 0.0016 0.2046 0.02744 0.1127 0 0.026];%xavi paper
%    
   
%YOU CAN CHANGE THE PARAMETERS HERE AND ALSO SOME ON LINE 64 
%Par0=[0.1896 0.23  1.1432 1 0.1795 0 0.03 0.8703 0 0.05];%DAVIDOK

%Par0=[-1 0.24  .094 1.0011 0.0033 0 0.012 0.0566 .002 0];%HYEOKOK

%Par0=[-1 0.2559  0.0927 1.3972 0.0002 0.0003 0.0129 0.0865 0.0020 0.0006];

%Par0=[0.4256 .27273 .090997 .91855 .0033 0 .012 .0566 .002 .020758]; %Felkner-Townsend
%Par0=[-1 .27273 .090997 .91855 .0033 0 .012 .0566 .002 .020758]; %%Felkner-Townsend m=-1 ftn2
%Par0=[-0.1265 0.6274 0.0125 1.6962 0.0040 0.0020 0.0136 0.0343 0.0019 0];
%Par0=[0.5918 0.7184 0.0049 1.4810 0.0052 0.0014 0.0226 0.0596 0.0010 0.0014];  %cur. best

%Par0=[0.5000 0.6000 0.0500 1.5000 0.0050 0 0.0300 0.0600 0.0030 0.0010]; %best new search all
%Par0=[0.4218 0.5791 0.0536 1.0519 0.0056 0.0001 0.0346 0.0663 0.0035 0.0009]; %best fit
Par0=[.6202 0.5791 0.0536 1.0519 0.0056 0.0001 0.0346 0.0663 0.0035 0.0009]; %best fit bin
%load res07_00xx
%Par0=Para0;

%Par0=[-.1 0.6000 0.0500 1.0000 0.0050 0.0020 0.0300 0.0600 0.0010 0.0010];  %best search bin1
%Par0=[-0.5000 0.6000 0.0500 1.5000 0.0020 0.0020 0.0100 0.0200 0.0010 0.0010]; %best search bin2
%Par0=[0 0.2000 0.2000  1.5000  0.0050  0.0020  0.0300  0.0600  0.0030  0.0010]; %best search bin3

%Par0=[-0.1012 0.6391 0.0491 1.0127 0.0049 0.0020 0.0305 0.0599 0.0010 0.0010]; %best estimate bin1
%Par0=[-0.5142 0.5601 0.0470 1.5095 0.0020 0.0021 0.0096 0.0207 0.0010 0.0010]; %best estimate bin2 
%Par0=[-0.0002 0.2410 0.1802 1.5889 0.0050 0.0019 0.0305 0.0601 0.0031 0.0009]; %best estimate bin3
%5/28/03


%Search only over m
%Para0=Par0(1);
Para0=Par0;             %search over all

%Para0=[.7156,27273,.090997,.91855,.020758];
%Par=[0.1896 0.23 1.1494 1.1521 0.1795 -0.0333 0.03 0.8703 0 0.01];%david boot
   %Par=[0.1896 0.23 1.1447 1.0761 0.1795 -0.0166 0.03 0.8703 0 0.01];%david boot2
   
   %farminc = 19274;% xavi paper SES
   %farminc=29085.75;% hyeok SES
   farminc=28602; %alex 
  
   end;

if (strcmp(calstr,'SESBallBn') == 1)
   Bn = 1;
   Ball = 1; 
elseif (strcmp(calstr,'SESBGRBn') == 1)
   Bn = 1;
   Ball = 0; 
elseif (strcmp(calstr,'SESBn') == 1)
   Bn = 1;
   Ball = 1; 
elseif (strcmp(calstr,'SESBall') == 1)
   Bn = 0;
   Ball = 1; 
   %wthai=47866.25;
    %wthai=68861.21;

% DAVID'S CALIBRATED PARAMETERS

%Par(10)=wthai*scale-Par(7)%nu
% Par(10)=0.05;%nu
% Par(2) = 0.23;%omega
% Par(9) = 0;%gamgr

    
% HYEOK'S CALIBRATED PARAMETERS

% %Par(10)=wthai*scale-Par(7)%nu
% Par(10)=0.01;%nu
% Par(2) = 0.23;%omega
% Par(9) = 0;%gamgr

% XAVI'S CALIBRATED PARAMETERS

%    Par(10) = 0.026;%nu
%    Par(2) = 0.321;%omega
%    Par(9) = 0;% gamgr

elseif (strcmp(calstr,'SESBGR') == 1)
   Bn = 0;
   Ball = 0; 
elseif (strcmp(calstr,'PrBGR') == 1)
   Bn = 0;
   Ball = 0; 
elseif (strcmp(calstr,'PrBall') == 1)
   Bn = 0;
   Ball = 1; 
end;

if Ball == 1
   wst = 6;
   wgs = [20 20 20 20 20]; 
else
   wst = 1;
   wgs = [100 0 0 0 0]; 
end;
clear Ball;
if Bn == 1
   Parcal = Resnc(max(find(Resnc(:,5)==wst)),[1:3]);
else
   Parcal = Resc(max(find(Resc(:,5)==wst)),[1:3]);
end;

scale = Par0(7)/farminc;
options=optimset('Disp','Iter');

crb=1000;
en0=[];

%initialize
save crb07_r3new crb Para0
%save entsZ07_1 en0

disp(Para0)
%X=fminsearch('crit2b',Para0,options,groutec',sratesec',lashrec2',pentec',giniec',wgs,wgp,itec,calstr,wel,fin,ini,dis,scale);
%X=fminbnd('crit2b',-1,1,options,groutec',sratesec',lashrec2',pentec',giniec',wgs,wgp,itec,calstr,wel,fin,ini,dis,scale);

X=feval('crit2b',Para0,groutec',sratesec',lashrec2',pentec',giniec',wgs,wgp,itec,calstr,wel,fin,ini,dis,scale);

%convert the data
%dat;
load entsZ07_r3new
load crb07_r3new
save res07_r3new vilid mbus dbus crb Para0 dgr en0


