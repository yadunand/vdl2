type file{};

(file modelOut, file paramOut) compiledLEB (file sampleIn, file paramIn, file dependencies[]){
	app{
		compiledLEB @filename(sampleIn) @filename(paramIn) @filename(modelOut) @filename(paramOut);
	}
}


// The float parameters determine the parameter range as follows:
// for each parameter, we are sending a tuple (float initiaValue, float stepSize, int RangeSize) 

(file modelOut, file paramOut) paramCompiledBatchedLEB (file sampleIn,  float fi1, float fs1, int ir1, float fi2, float fs2, int ir2, float fi3, float fs3, int ir3, float fi4, float fs4, int ir4, float fi5, float fs5, int ir5, float fi6, float fs6, int ir6, float fi7, float fs7, int ir7, float fi8, float fs8, int ir8, float fi9, float fs9, int ir9, float fi10, float fs10, int ir10, int batchIndex,int  batchSize, file dependencies[] ) {
	app{
		paramCompiledBatchedLEB @filename(sampleIn)  @filename(modelOut) @filename(paramOut) fi1 fs1 ir1 fi2 fs2 ir2 fi3 fs3 ir3 fi4 fs4 ir4 fi5 fs5 ir5 fi6 fs6 ir6 fi7 fs7 ir7 fi8 fs8 ir8 fi9 fs9 ir9 fi10 fs10 ir10 batchIndex batchSize;
	}
}

file modelOut<"model_output_grid.mat">;
file paramOut<"param_output_grid.mat">;


paramEstimation(){

	// startinig parameters
	float m=0.0;				// -1:1: 0.01
	float omega=0.2;		//0.1: 0.6 0.05
	float beta=0.2;			
	float alpha=1.5;
	float rho = 0.005;
	float sigma = 0.002;
	float gamma = 0.03;
	float xi = 0.06;
	float g = 0.003;			// 0.003 0.05  0.005 
	float nyu = 0.001;
	
	 
	
	file sampleIn<"inputs-compiled/sample.mat">;
	String dependencyFileNames="inputs-compiled/criterion.mat inputs-compiled/datec.mat  inputs-compiled/leb76a.mat inputs-compiled/newbins.mat inputs-compiled/Parcalibsesa.mat "; 
	file dependencies[] <fixed_array_mapper; files=dependencyFileNames>;
	
	float mStep=0.01;
	float omegaStep=0.05;
	float gStep=0.05;
	
	int mRangeSize=1;
	int omegaRangeSize=1;
	int gRangeSize=1;
	
	int mRange=[0:mRangeSize];
	int omegaRange=[0:omegaRangeSize];
	int gRange=[0:gRangeSize];

	int batchSize=5;

	foreach mIndex in mRange {	
	foreach	omegaIndex in omegaRange{
	foreach gIndex in gRange{
	
		//batches start at zero, include rangeSize
		int batchPos = gIndex+(gRangeSize+1)*omegaIndex+(gRangeSize+1)*(omegaRangeSize+1)*mIndex;
		int batchIndex = batchPos %/ batchSize;
		int batchPosIndex = batchPos %% batchSize;

		if (batchPosIndex == 0){
			print(@strcat("Submit batch #: ",batchPos));
			file outParam<single_file_mapper; file=@strcat("param-",mIndex,"-",omegaIndex,"-",gIndex,".out")>;
			file outModel<single_file_mapper; file=@strcat("model-",mIndex,"-",omegaIndex,"-",gIndex,".out")>;
			(outModel, outParam) = paramCompiledBatchedLEB (sampleIn, m, mStep, mRangeSize, omega, omegaStep, omegaRangeSize, beta,0.0,0, alpha,0.0,0, rho,0.0,0, sigma,0.0,0, gamma,0.0,0, xi,0.0,0, g, gStep,gRangeSize, nyu, 0.0,0, batchIndex, batchSize, dependencies);
		}

	}
	}
	}
}


paramEstimation();

