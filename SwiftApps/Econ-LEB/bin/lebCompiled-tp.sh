#!/bin/bash

if [ $# -ne 4 ]; then 
	echo "want 4 args";
	exit 1
fi

for i in `ls inputs-compiled`; do ln -s inputs-compiled/$i $i; done

/home/tiberius/local/ECON-LEB/bin/run_LEBgrid-tp.sh /home/andrewj/CADGrid/TestArea/mcrROOT/v76 $1 $2 $3 $4