#!/bin/bash

if [ $# -ne 4 ]; then 
	echo "want 4 args";
	exit 1
fi

for i in `ls inputs-compiled`; do ln -s inputs-compiled/$i $i; done

/home/tstef/local/x32/ECON-LEB/bin/run_LEBgrid-uc32.sh /home/andrewj/CADGrid/TestArea/mcrROOT/v73 $1 $2 $3 $4
