#!/bin/bash

if [ $# -ne 4 ]; then 
	echo "want 4 args";
	exit 1
fi

for i in `ls inputs-compiled`; do ln -s inputs-compiled/$i $i; done

/u/ac/tstef/local/x32/ECON-LEB/bin/run_LEBgrid-ncsa-w.sh /u/ac/tstef/local/MCR/v77 $1 $2 $3 $4
