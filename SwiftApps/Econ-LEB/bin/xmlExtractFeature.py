#!/usr/bin/python

import sys
import xml.dom.minidom

def main():
    if len(sys.argv) !=3:
        print("Use like this: xmlExtractFeature.py inXML outTXT")
        sys.exit(1)
    
    outputFile = open(sys.argv[2],'w')
    gisDoc = xml.dom.minidom.parse(sys.argv[1])

    for e in gisDoc.getElementsByTagName("sf:village_survey"):
        #print e.lastChild.toxml()
        #outputFile.write(e.lastChild.toxml() + "\n")
		
		wealth = e.getElementsByTagName("sf:wlth96").item(0)
		if wealth != None: 
			wealthValue = wealth.lastChild.toxml()
		else:
			#TODO: FIXME: what to do about this ?
			wealthValue = "NA"
		#print wealthValue
		
		villageID = e.getElementsByTagName("sf:cdd_id").item(0)
		villageIDValue = villageID.lastChild.toxml()
		
		outputFile.write(villageIDValue+ ", " + wealthValue + "\n")
		
    outputFile.close()

if __name__ == "__main__": main()
