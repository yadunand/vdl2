#!/bin/bash

MINCRITERION=0
BESTMATCH=""

for i in param*.out; do \
CRITERION=`head -1 $i | awk -F'=' '{print$2}'` ;\
#echo $CRITERION ;\
if [[ `echo "$CRITERION < $MINCRITERION" | bc -l` ]]  ;  then \
#echo "New Min: $CRITERION";\
MINCRITERION=$CRITERION; \
BESTMATCH=$i; fi ;\
done
echo "WINNER: $BESTMATCH with $MINCRITERION"

cp ${BESTMATCH} WINNER.PARAM.OUT
cp model${BESTMATCH:5} WINNER.MODEL.OUT
