#!/bin/bash

for i in `ls inputs-compiled`; do ln -s inputs-compiled/$i $i; done

# generate the param files
/home/ba01/u102/stef/local/ECON-LEB/bin/paramCompiledBatchedLEB-tp.py $*

# execute the LEB model 
# NOTE: can be run in parallel, needs more work
for i in param-*.csv; do /home/ba01/u102/stef/local/ECON-LEB/bin/run_LEBgrid_Purdue.sh /home/ba01/u102/stef/local/MCR-Purdue/v77 $1 $i $i.model.out $i.param.out; done

#merge chose the best output and return it

MINCRITERION=0
BESTMATCH=""

for i in param*.csv; do \
CRITERION=`head -1 $i.param.out | awk -F'=' '{print$2}'` ;\
#echo $CRITERION ;\
if [[ `echo "$CRITERION <  $MINCRITERION" | bc -l` ]]  ;  then \
MINCRITERION=$CRITERION; BESTMATCH=$i; fi ;\
done
echo "WINNER: $BESTMATCH with $MINCRITERION"

cp $BESTMATCH.model.out $2
cp $BESTMATCH.param.out $3

