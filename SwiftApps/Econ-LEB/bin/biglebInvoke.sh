#!/bin/bash

#echo "execute /usr/local/bin/octave $1";

for i in `ls inputs`; do ln -s inputs/$i $i; done
for j in `ls scripts-lebbig`; do ln -s scripts-lebbig/$j $j; done


export OCTAVE_VILLAGE_RANGE_START=$2
export OCTAVE_VILLAGE_BATCH_SIZE=$3
export OCTAVE_OUTPUT_FILENAME=$4

/usr/local/bin/octave $1


