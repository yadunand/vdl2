#!/usr/bin/python
#for i in `ls inputs-compiled`; do ln -s inputs-compiled/$i $i; done
#echo "$4,$5,$6,$7,$8,$9,${10},${11},${12},${13}" > param.csv
#/home/tiberius/local/ECON-LEB/bin/run_LEBgrid-tp.sh /home/andrewj/CADGrid/TestArea/mcrROOT/v76 $1 param.csv $2 $3

import sys
import os


def run(program, *args):
    pid = os.fork()
    if not pid:
        os.execvp(program, (program,) +  args)
    return os.wait()[0]

if len(sys.argv) <  35:
    print "You need 35 arguments"
    sys.exit(1)

m=float(sys.argv[4])
omega=float(sys.argv[7])
beta=    float( sys.argv[10])       
alpha=float( sys.argv[13])
rho = float( sys.argv[16])
sigma = float( sys.argv[19])
gamma =float(  sys.argv[22])
xi =float( sys.argv[25])
g = float( sys.argv[28])
nyu =float( sys.argv[31])

mStep=float(sys.argv[5])
omegaStep=float(sys.argv[8])
betaStep=   float(  sys.argv[11])   
alphaStep=float(sys.argv[14])
rhoStep = float(sys.argv[17])
sigmaStep = float(sys.argv[20])
gammaStep = float(sys.argv[23])
xiStep = float(sys.argv[26])
gStep =float(sys.argv[29])
nyuStep = float(sys.argv[32])

mRange=int(sys.argv[6])
omegaRange=int(sys.argv[9])
betaRange=     int(sys.argv[12] ) 
alphaRange=int(sys.argv[15])
rhoRange = int(sys.argv[18])
sigmaRange = int(sys.argv[21])
gammaRange = int(sys.argv[24])
xiRange = int(sys.argv[27])
gRange=int(sys.argv[30])
nyuRange = int(sys.argv[33])

batchIndex = int(sys.argv[34])
batchSize = int(sys.argv[35])

betaIndex=0
alphaIndex=0
rhoIndex=0
sigmaIndex=0
gammaIndex=0
xiIndex=0
nyuIndex=0

# Swift goes between zero and the rangeNo inclusively
# NOTE: This loop has to be "IN SYNC" with the loop from the swift script

for mIndex in range(mRange+1):
    for omegaIndex in range(omegaRange+1):
        for gIndex in range(gRange+1):
            
            # replace if more loops are to be exeecuted
            loopIndex = gIndex + omegaIndex * (gRange+1) + mIndex * (gRange+1) * (omegaRange+1)

            if loopIndex < (batchIndex * batchSize):
                continue;
            if loopIndex >= ((batchIndex + 1) * batchSize):
                continue

            f = open("param-"+str(mIndex)+"-"+str(omegaIndex)+"-"+str(gIndex)+".csv","w");
            try:
               f. write (str(m+mIndex*mStep)+", "+str( omega+omegaIndex*omegaStep)+", "+str(beta+betaIndex*betaStep)+", "+str(alpha+alphaIndex*alphaStep)+", "+str(rho+rhoIndex*rhoStep)+", "+str(sigma+sigmaIndex*sigmaStep)+", "+str(gamma+gammaIndex*gammaStep)+", "+str(xi+xiIndex*xiStep)+", "+str(g+gIndex*gStep)+", "+str(nyu+nyuIndex*nyuStep))
               #moved to the bask script
               #run("/home/tiberius/local/ECON-LEB/bin/run_LEBgrid-tp.sh"," /home/andrewj/CADGrid/TestArea/mcrROOT/v76  "+sys.argv[1] +"param-"+str(mIndex)+"-"+str(omegaIndex)+"-"+str(gIndex)+".out  "+sys.argv[2]+" "+ sys.argv[3]);
            finally:
                f.close()
