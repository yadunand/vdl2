/*
 * MATLAB Compiler: 4.7 (R2007b)
 * Date: Mon Jan  7 13:29:12 2008
 * Arguments: "-B" "macro_default" "-o" "LEBgrid" "-W" "main" "-T" "link:exe"
 * "-d" "bin" "-v" "scripts-compiled/LEB_main.m" "-a"
 * "scripts-compiled/binhist.m" "-a" "scripts-compiled/crit2b.m" "-a"
 * "scripts-compiled/inidista2.m" "-a" "scripts-compiled/labdemc.m" "-a"
 * "scripts-compiled/labdem.m" "-a" "scripts-compiled/LEB_output.m" "-a"
 * "scripts-compiled/lmeq2sc.m" "-a" "scripts-compiled/mdist.m" "-a"
 * "scripts-compiled/rescalfn18cb.m" "-a" "scripts-compiled/resdistc.m" "-a"
 * "scripts-compiled/resdistnc.m" 
 */

#include "mclmcr.h"

#ifdef __cplusplus
extern "C" {
#endif
const unsigned char __MCC_LEBgrid_session_key[] = {
    '1', 'D', '9', '6', 'A', '4', 'E', '3', '4', '8', '8', '8', '7', '0', '3',
    'C', '5', 'B', 'F', '4', 'A', 'E', 'C', '3', 'D', '2', '3', 'B', 'A', 'E',
    '0', '8', '0', 'F', 'B', '8', 'E', '3', 'E', '3', 'F', '5', '0', 'C', '0',
    'E', '3', '7', '2', '3', 'D', '8', 'F', '4', 'A', 'C', 'C', 'E', '9', '7',
    '0', 'A', '5', '5', '4', '9', '7', '1', '9', '4', '1', '4', '4', 'C', 'F',
    'E', 'A', '7', '2', '9', 'A', '7', 'D', 'F', '1', '1', '9', '0', '7', '9',
    '3', '0', '1', 'F', '7', '9', '5', 'F', '2', '2', '4', '3', '7', 'D', '4',
    'A', '5', '2', '3', 'F', '5', '1', '9', 'E', 'C', '8', '0', 'B', '0', 'A',
    'C', 'F', 'D', 'D', '9', 'B', '7', '8', 'F', '3', '3', '9', 'A', 'B', '4',
    'F', '4', '5', '6', '7', 'A', '2', 'B', 'F', '3', 'D', 'C', 'F', '2', 'D',
    'B', '0', '8', '3', '0', 'C', 'A', 'A', 'A', 'B', '9', 'C', '4', '7', '1',
    'E', '4', '6', '1', '9', '5', 'B', 'B', '2', 'B', '2', '8', 'F', '2', '3',
    '2', '8', '4', '3', 'B', '3', '2', '5', '2', '8', 'B', '7', 'A', 'B', 'B',
    'D', '5', 'B', '6', '2', 'B', '3', '6', 'A', 'B', '6', '9', 'C', '1', '6',
    '7', 'A', 'B', 'B', 'D', '9', '2', 'E', '3', 'D', 'F', '6', 'F', '4', '9',
    'D', '5', '7', '4', '4', '5', 'E', '1', 'B', '0', '2', 'E', '8', 'F', 'C',
    'C', '9', '4', '5', '1', 'E', 'F', '2', '6', '1', '5', 'F', '5', 'F', '6',
    '5', '\0'};

const unsigned char __MCC_LEBgrid_public_key[] = {
    '3', '0', '8', '1', '9', 'D', '3', '0', '0', 'D', '0', '6', '0', '9', '2',
    'A', '8', '6', '4', '8', '8', '6', 'F', '7', '0', 'D', '0', '1', '0', '1',
    '0', '1', '0', '5', '0', '0', '0', '3', '8', '1', '8', 'B', '0', '0', '3',
    '0', '8', '1', '8', '7', '0', '2', '8', '1', '8', '1', '0', '0', 'C', '4',
    '9', 'C', 'A', 'C', '3', '4', 'E', 'D', '1', '3', 'A', '5', '2', '0', '6',
    '5', '8', 'F', '6', 'F', '8', 'E', '0', '1', '3', '8', 'C', '4', '3', '1',
    '5', 'B', '4', '3', '1', '5', '2', '7', '7', 'E', 'D', '3', 'F', '7', 'D',
    'A', 'E', '5', '3', '0', '9', '9', 'D', 'B', '0', '8', 'E', 'E', '5', '8',
    '9', 'F', '8', '0', '4', 'D', '4', 'B', '9', '8', '1', '3', '2', '6', 'A',
    '5', '2', 'C', 'C', 'E', '4', '3', '8', '2', 'E', '9', 'F', '2', 'B', '4',
    'D', '0', '8', '5', 'E', 'B', '9', '5', '0', 'C', '7', 'A', 'B', '1', '2',
    'E', 'D', 'E', '2', 'D', '4', '1', '2', '9', '7', '8', '2', '0', 'E', '6',
    '3', '7', '7', 'A', '5', 'F', 'E', 'B', '5', '6', '8', '9', 'D', '4', 'E',
    '6', '0', '3', '2', 'F', '6', '0', 'C', '4', '3', '0', '7', '4', 'A', '0',
    '4', 'C', '2', '6', 'A', 'B', '7', '2', 'F', '5', '4', 'B', '5', '1', 'B',
    'B', '4', '6', '0', '5', '7', '8', '7', '8', '5', 'B', '1', '9', '9', '0',
    '1', '4', '3', '1', '4', 'A', '6', '5', 'F', '0', '9', '0', 'B', '6', '1',
    'F', 'C', '2', '0', '1', '6', '9', '4', '5', '3', 'B', '5', '8', 'F', 'C',
    '8', 'B', 'A', '4', '3', 'E', '6', '7', '7', '6', 'E', 'B', '7', 'E', 'C',
    'D', '3', '1', '7', '8', 'B', '5', '6', 'A', 'B', '0', 'F', 'A', '0', '6',
    'D', 'D', '6', '4', '9', '6', '7', 'C', 'B', '1', '4', '9', 'E', '5', '0',
    '2', '0', '1', '1', '1', '\0'};

static const char * MCC_LEBgrid_matlabpath_data[] = 
  { "LEBgrid/", "toolbox/compiler/deploy/", "scripts-compiled/",
    "$TOOLBOXMATLABDIR/general/", "$TOOLBOXMATLABDIR/ops/",
    "$TOOLBOXMATLABDIR/lang/", "$TOOLBOXMATLABDIR/elmat/",
    "$TOOLBOXMATLABDIR/elfun/", "$TOOLBOXMATLABDIR/specfun/",
    "$TOOLBOXMATLABDIR/matfun/", "$TOOLBOXMATLABDIR/datafun/",
    "$TOOLBOXMATLABDIR/polyfun/", "$TOOLBOXMATLABDIR/funfun/",
    "$TOOLBOXMATLABDIR/sparfun/", "$TOOLBOXMATLABDIR/scribe/",
    "$TOOLBOXMATLABDIR/graph2d/", "$TOOLBOXMATLABDIR/graph3d/",
    "$TOOLBOXMATLABDIR/specgraph/",
    "$TOOLBOXMATLABDIR/graphics/", "$TOOLBOXMATLABDIR/uitools/",
    "$TOOLBOXMATLABDIR/strfun/", "$TOOLBOXMATLABDIR/imagesci/",
    "$TOOLBOXMATLABDIR/iofun/", "$TOOLBOXMATLABDIR/audiovideo/",
    "$TOOLBOXMATLABDIR/timefun/", "$TOOLBOXMATLABDIR/datatypes/",
    "$TOOLBOXMATLABDIR/verctrl/", "$TOOLBOXMATLABDIR/codetools/",
    "$TOOLBOXMATLABDIR/helptools/", "$TOOLBOXMATLABDIR/demos/",
    "$TOOLBOXMATLABDIR/timeseries/", "$TOOLBOXMATLABDIR/hds/",
    "$TOOLBOXMATLABDIR/guide/", "$TOOLBOXMATLABDIR/plottools/",
    "toolbox/local/", "toolbox/shared/optimlib/" };

static const char * MCC_LEBgrid_classpath_data[] = 
  { "" };

static const char * MCC_LEBgrid_libpath_data[] = 
  { "" };

static const char * MCC_LEBgrid_app_opts_data[] = 
  { "" };

static const char * MCC_LEBgrid_run_opts_data[] = 
  { "" };

static const char * MCC_LEBgrid_warning_state_data[] = 
  { "off:MATLAB:dispatcher:nameConflict" };


mclComponentData __MCC_LEBgrid_component_data = { 

  /* Public key data */
  __MCC_LEBgrid_public_key,

  /* Component name */
  "LEBgrid",

  /* Component Root */
  "",

  /* Application key data */
  __MCC_LEBgrid_session_key,

  /* Component's MATLAB Path */
  MCC_LEBgrid_matlabpath_data,

  /* Number of directories in the MATLAB Path */
  36,

  /* Component's Java class path */
  MCC_LEBgrid_classpath_data,
  /* Number of directories in the Java class path */
  0,

  /* Component's load library path (for extra shared libraries) */
  MCC_LEBgrid_libpath_data,
  /* Number of directories in the load library path */
  0,

  /* MCR instance-specific runtime options */
  MCC_LEBgrid_app_opts_data,
  /* Number of MCR instance-specific runtime options */
  0,

  /* MCR global runtime options */
  MCC_LEBgrid_run_opts_data,
  /* Number of MCR global runtime options */
  0,
  
  /* Component preferences directory */
  "LEBgrid_F88561BDA2061F661917DDB5C5987107",

  /* MCR warning status data */
  MCC_LEBgrid_warning_state_data,
  /* Number of MCR warning status modifiers */
  1,

  /* Path to component - evaluated at runtime */
  NULL

};

#ifdef __cplusplus
}
#endif


