/*
 * MATLAB Compiler: 4.7 (R2007b)
 * Date: Wed Jan 16 14:35:50 2008
 * Arguments: "-B" "macro_default" "-o" "LEBgrid_Purdue" "-W" "main" "-T"
 * "link:exe" "-d" "bin" "-v" "scripts-compiled/LEB_main.m" "-a"
 * "scripts-compiled/binhist.m" "-a" "scripts-compiled/crit2b.m" "-a"
 * "scripts-compiled/inidista2.m" "-a" "scripts-compiled/labdemc.m" "-a"
 * "scripts-compiled/labdem.m" "-a" "scripts-compiled/LEB_output.m" "-a"
 * "scripts-compiled/lmeq2sc.m" "-a" "scripts-compiled/mdist.m" "-a"
 * "scripts-compiled/rescalfn18cb.m" "-a" "scripts-compiled/resdistc.m" "-a"
 * "scripts-compiled/resdistnc.m" 
 */

#include "mclmcr.h"

#ifdef __cplusplus
extern "C" {
#endif
const unsigned char __MCC_LEBgrid_Purdue_session_key[] = {
    '7', '2', '1', '6', '5', '4', '0', '6', '8', 'E', 'F', '7', '2', 'F', 'A',
    '0', '3', 'C', 'C', '6', 'D', '3', '2', '5', '2', 'B', '0', 'E', '9', '1',
    'D', '0', '7', 'B', '0', '1', 'A', '7', '6', '1', '2', 'D', '5', 'F', 'C',
    '9', '1', '1', '9', '6', '8', '9', '2', '4', '6', 'D', '8', '8', '0', 'D',
    '8', 'A', 'A', '3', 'D', '6', '5', '9', '1', '1', '6', '4', 'C', '8', '4',
    '7', 'C', '5', 'E', 'F', 'A', 'A', 'A', '4', 'B', 'A', '5', 'A', '1', '9',
    'B', '5', '7', 'F', 'B', '6', '7', '1', '7', '1', '1', '6', '9', 'D', '6',
    '2', '7', '4', '6', 'B', '3', 'A', '6', '1', '7', '3', '3', 'B', '5', '2',
    'B', '5', '0', '5', '8', '8', '1', 'D', 'D', '6', 'F', '5', 'B', 'E', '4',
    '2', '7', '3', 'B', '3', '7', 'E', 'C', 'E', '2', '3', 'C', 'A', 'C', 'D',
    '7', 'B', 'E', '5', '1', '1', 'D', 'A', '3', 'D', '2', 'D', '9', '8', '2',
    '3', 'B', 'F', '5', '0', '8', '6', '9', '2', '3', '7', '8', 'B', 'C', '0',
    'B', '2', '4', '3', 'A', 'E', '0', 'B', 'B', '2', '2', '3', 'C', '0', '4',
    '3', '6', '7', 'A', '5', 'E', 'A', 'C', 'C', '3', '1', '6', '9', '1', 'C',
    '1', '9', 'D', '4', '3', '8', 'B', 'C', '0', 'C', '9', 'C', 'E', '8', '0',
    '6', 'E', 'B', 'D', 'A', '1', '8', '1', '1', 'A', 'C', '1', '3', '3', 'C',
    '4', '6', '9', 'E', '7', 'A', '8', '1', '1', 'C', '6', 'B', '7', '3', '0',
    '7', '\0'};

const unsigned char __MCC_LEBgrid_Purdue_public_key[] = {
    '3', '0', '8', '1', '9', 'D', '3', '0', '0', 'D', '0', '6', '0', '9', '2',
    'A', '8', '6', '4', '8', '8', '6', 'F', '7', '0', 'D', '0', '1', '0', '1',
    '0', '1', '0', '5', '0', '0', '0', '3', '8', '1', '8', 'B', '0', '0', '3',
    '0', '8', '1', '8', '7', '0', '2', '8', '1', '8', '1', '0', '0', 'C', '4',
    '9', 'C', 'A', 'C', '3', '4', 'E', 'D', '1', '3', 'A', '5', '2', '0', '6',
    '5', '8', 'F', '6', 'F', '8', 'E', '0', '1', '3', '8', 'C', '4', '3', '1',
    '5', 'B', '4', '3', '1', '5', '2', '7', '7', 'E', 'D', '3', 'F', '7', 'D',
    'A', 'E', '5', '3', '0', '9', '9', 'D', 'B', '0', '8', 'E', 'E', '5', '8',
    '9', 'F', '8', '0', '4', 'D', '4', 'B', '9', '8', '1', '3', '2', '6', 'A',
    '5', '2', 'C', 'C', 'E', '4', '3', '8', '2', 'E', '9', 'F', '2', 'B', '4',
    'D', '0', '8', '5', 'E', 'B', '9', '5', '0', 'C', '7', 'A', 'B', '1', '2',
    'E', 'D', 'E', '2', 'D', '4', '1', '2', '9', '7', '8', '2', '0', 'E', '6',
    '3', '7', '7', 'A', '5', 'F', 'E', 'B', '5', '6', '8', '9', 'D', '4', 'E',
    '6', '0', '3', '2', 'F', '6', '0', 'C', '4', '3', '0', '7', '4', 'A', '0',
    '4', 'C', '2', '6', 'A', 'B', '7', '2', 'F', '5', '4', 'B', '5', '1', 'B',
    'B', '4', '6', '0', '5', '7', '8', '7', '8', '5', 'B', '1', '9', '9', '0',
    '1', '4', '3', '1', '4', 'A', '6', '5', 'F', '0', '9', '0', 'B', '6', '1',
    'F', 'C', '2', '0', '1', '6', '9', '4', '5', '3', 'B', '5', '8', 'F', 'C',
    '8', 'B', 'A', '4', '3', 'E', '6', '7', '7', '6', 'E', 'B', '7', 'E', 'C',
    'D', '3', '1', '7', '8', 'B', '5', '6', 'A', 'B', '0', 'F', 'A', '0', '6',
    'D', 'D', '6', '4', '9', '6', '7', 'C', 'B', '1', '4', '9', 'E', '5', '0',
    '2', '0', '1', '1', '1', '\0'};

static const char * MCC_LEBgrid_Purdue_matlabpath_data[] = 
  { "LEBgrid_Purdue/", "toolbox/compiler/deploy/",
    "scripts-compiled/", "$TOOLBOXMATLABDIR/general/",
    "$TOOLBOXMATLABDIR/ops/", "$TOOLBOXMATLABDIR/lang/",
    "$TOOLBOXMATLABDIR/elmat/", "$TOOLBOXMATLABDIR/elfun/",
    "$TOOLBOXMATLABDIR/specfun/", "$TOOLBOXMATLABDIR/matfun/",
    "$TOOLBOXMATLABDIR/datafun/", "$TOOLBOXMATLABDIR/polyfun/",
    "$TOOLBOXMATLABDIR/funfun/", "$TOOLBOXMATLABDIR/sparfun/",
    "$TOOLBOXMATLABDIR/scribe/", "$TOOLBOXMATLABDIR/graph2d/",
    "$TOOLBOXMATLABDIR/graph3d/", "$TOOLBOXMATLABDIR/specgraph/",
    "$TOOLBOXMATLABDIR/graphics/", "$TOOLBOXMATLABDIR/uitools/",
    "$TOOLBOXMATLABDIR/strfun/", "$TOOLBOXMATLABDIR/imagesci/",
    "$TOOLBOXMATLABDIR/iofun/", "$TOOLBOXMATLABDIR/audiovideo/",
    "$TOOLBOXMATLABDIR/timefun/", "$TOOLBOXMATLABDIR/datatypes/",
    "$TOOLBOXMATLABDIR/verctrl/", "$TOOLBOXMATLABDIR/codetools/",
    "$TOOLBOXMATLABDIR/helptools/", "$TOOLBOXMATLABDIR/demos/",
    "$TOOLBOXMATLABDIR/timeseries/", "$TOOLBOXMATLABDIR/hds/",
    "$TOOLBOXMATLABDIR/guide/", "$TOOLBOXMATLABDIR/plottools/",
    "toolbox/local/", "toolbox/shared/optimlib/" };

static const char * MCC_LEBgrid_Purdue_classpath_data[] = 
  { "" };

static const char * MCC_LEBgrid_Purdue_libpath_data[] = 
  { "" };

static const char * MCC_LEBgrid_Purdue_app_opts_data[] = 
  { "" };

static const char * MCC_LEBgrid_Purdue_run_opts_data[] = 
  { "" };

static const char * MCC_LEBgrid_Purdue_warning_state_data[] = 
  { "off:MATLAB:dispatcher:nameConflict" };


mclComponentData __MCC_LEBgrid_Purdue_component_data = { 

  /* Public key data */
  __MCC_LEBgrid_Purdue_public_key,

  /* Component name */
  "LEBgrid_Purdue",

  /* Component Root */
  "",

  /* Application key data */
  __MCC_LEBgrid_Purdue_session_key,

  /* Component's MATLAB Path */
  MCC_LEBgrid_Purdue_matlabpath_data,

  /* Number of directories in the MATLAB Path */
  36,

  /* Component's Java class path */
  MCC_LEBgrid_Purdue_classpath_data,
  /* Number of directories in the Java class path */
  0,

  /* Component's load library path (for extra shared libraries) */
  MCC_LEBgrid_Purdue_libpath_data,
  /* Number of directories in the load library path */
  0,

  /* MCR instance-specific runtime options */
  MCC_LEBgrid_Purdue_app_opts_data,
  /* Number of MCR instance-specific runtime options */
  0,

  /* MCR global runtime options */
  MCC_LEBgrid_Purdue_run_opts_data,
  /* Number of MCR global runtime options */
  0,
  
  /* Component preferences directory */
  "LEBgrid_Purdue_31893FC4A63CB64EEE7287148B494668",

  /* MCR warning status data */
  MCC_LEBgrid_Purdue_warning_state_data,
  /* Number of MCR warning status modifiers */
  1,

  /* Path to component - evaluated at runtime */
  NULL

};

#ifdef __cplusplus
}
#endif


