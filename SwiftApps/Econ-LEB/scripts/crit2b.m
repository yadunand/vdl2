function[cr]=crit2b(Para,grincec2,sratesec2,lashrec2,pentec2,giniec2,wgstat,wgper,itec,calstr,wel,fin,ini,printdis,scale)

%Par=[Para,0.03 0.8703 0 0.05];
%bpts=200;
disp(Para)
%Para5=max(0,Para(5));
%Par=[Para,.5077,.1423,.5519,0.0033, 0, .012,.8703,0,.0010];
%Par=[Para 0.24  .094 1.0011 0.0033 0 0.012 0.0566 .002 0];%HYEOKOK
%Par=[-0.9885    0.2456    0.0909    1.3222    0.0005    0.0003    0.0124    0.0693    0.0020    0.0007
Par=Para;
%Par=[Para,.27273,.090997,.91855,0.0033, 0, .012,.0566,.002,.020758];
%Par=[Para .27273 .090997 .91855 .0033 0 .012 .0566 .002 .020758]; %Felkner-Townsend
%Par=[Para 0.5791 0.0536 1.0519 0.0056 0.0001 0.0346 0.0663 0.0035 0.0009]; %best fit
%Par=[Para 0.6391 0.0491 1.0127 0.0049 0.0020 0.0305 0.0599 0.0010 0.0010]; %best estimate bin1
%Par=[Para 0.5601 0.0470 1.5095 0.0020 0.0021 0.0096 0.0207 0.0010 0.0010];  %best estimate bin2
%Par=[Para 0.2410 0.1802 1.5889 0.0050 0.0019 0.0305 0.0601 0.0031 0.0009]; %best estimate bin3
%try
cr=rescalfn18cb(Par,grincec2,sratesec2,lashrec2,pentec2,giniec2,wgstat,wgper,itec,calstr,wel,fin,ini,printdis,scale);
% catch
%    cr=1000;
%    disp('error')
%     return
% end


load crb07_r3new

if cr<=crb
    crb=cr;
    Para0=Para;
    %load entsZ07_00x
    %save ents407_00x en0 crb Para0
    save crb07_r3new crb Para0
end
disp(['Current Best is: ',num2str(crb)])
disp('Achieved For: ')
disp(Para0)