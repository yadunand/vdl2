alter table leb_results_mjr_bin3_021408 add column pk_id serial unique;

DROP VIEW leb_results_dtop6_bin1_021808_gisview ;

CREATE OR REPLACE VIEW leb_results_dtop6_bin2_021808_gisview AS 
 SELECT l.pk_id, l.model_ent_level_1994, l.data_ent_level_1994, l.diff, t.the_geom
   FROM leb_results_dtop6_bin2_021808 l, tambon t
  WHERE substr(t.key_::text, 1, 6) = ltrim(rtrim(l.id::text))
  and substr(t.key_::text, 1, 6) not in

  (

select substr(key_::text, 1, 6) from tambon
group by substr(key_::text, 1, 6)
having count(substr(key_::text, 1, 6)) >1
  )
