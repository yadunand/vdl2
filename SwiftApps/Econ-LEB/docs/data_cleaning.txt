
--the following script allows to find out unreasonable business growth data points
SELECT tambon_id, CAST(avg(tambon_population) AS INT) as average_population,
 MIN(retail_business) AS min_retail_business, MAX(retail_business) as max_retail_business,  
 MIN(cottage_business) AS min_cottage_business, MAX(cottage_business)  as max_cottage_business 
  FROM tambon_business  
group by tambon_id
having max(retail_business) - min(retail_business) >50
or   max(cottage_business) - min(cottage_business) >50
tambon_id


--the number of cottage industry businessmen went up from 0 to 1001 in 2 years
update cdd_sde_cdd_94_geca_tb_raw
set n_cottage = 0
where tbid =630404;

update cdd_sde_cdd_94_geca_tb_raw
set n_cottage =0
where tbid = 101102;
--the number of retail industry businessmen went up from 8 to 60 in 2 years and then down to 1
update cdd_sde_cdd_92_geca_tb_raw
set n_retail =8
where tbid=100109;

--the number of retail industry businessmen went up from 1 to 279 in 2 years 
update cdd_sde_cdd_94_geca_tb_raw
set n_retail =1
where tbid=101102;


--the number of retail industry businessmen went up from 1 to 81 in 2 years
update cdd_sde_cdd_88_geca_tb_raw
set n_retail =1
where tbid=
110108;
--the number of retail industry businessmen went up from 0 to 133 in 2 years
update cdd_sde_cdd_94_geca_tb_raw
set n_retail =1
where tbid=212501;
--the number of cottage industry businessmen went up from 0 to 111 in 2 years
update cdd_sde_cdd_88_geca_tb_raw
set n_cottage =0
where tbid =
270305;
--the number of retail industry businessmen went up from 375 to 0 in 2 years
update cdd_sde_cdd_86_geca_tb_raw
set n_retail =0
where tbid =531003;
--the number of retail industry businessmen went up from 0 to 385 in 2 years
update cdd_sde_cdd_88_geca_tb_raw
set n_retail =0
where tbid =711404;


--the following script allows to find out unreasonable wealth related data points
SELECT tambon_id, CAST(avg(tambon_population) as INT) as average_population, MIN(flush_toilets) as min_flush_toilets, MAX(flush_toilets) as max_flush_toilets,
MIN(tv_sets) as min_tv_sets, MAX(tv_sets) as max_tv_sets,
MIN(motorcycles) as min_motorcycles, MAX(motorcycles) as max_motorcycles,
MIN(pickup_trucks) as min_pickup_trucks, MAX(pickup_trucks) as max_pickup_trucks
FROM tambon_wealth
group by tambon_id 
having  
   MAX(flush_toilets) - MIN(flush_toilets) > 0.5*avg(tambon_population)
OR MAX(tv_sets) - MIN(tv_sets) > 0.5*avg(tambon_population)
OR MAX(motorcycles) -MIN(motorcycles) >0.5*avg(tambon_population)
OR MAX(pickup_trucks) -MIN(pickup_trucks) > 0.5*avg(tambon_population)
order by tambon_id


