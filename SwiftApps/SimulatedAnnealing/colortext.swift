global string e33; // = @sprintf("\\e") or  @arg("e33");

if( @arg("e33","null") == "null" ) {
  e33 = @sprintf("\\e");
}
else {
  e33 = @arg("e33");
}

global string endEscape = "[0m";

// Color codes used in Andrey's cpp optimizer:

global int Black = 0;
global int Blue = 1;
global int BlackOnBlue = 2;
global int Pink = 3;
global int GoldOnBlue = 4;
global int BlueOnGray = 5;
global int Red = 6;
global int Gold = 7;
global int BlueOnGold = 8;
global int Gray = 9;
global int BlackOnGray = 10;

# Black = 0;	   
# Blue = 1;	   
# BlackOnBlue = 2; 
# Pink = 3;	   
# GoldOnBlue = 4;  
# BlueOnGray = 5;  
# Red = 6;	   
# Gold = 7;	   
# BlueOnGold = 8;  
# Gray = 9;	   
# BlackOnGray = 10;

global string colorCode[] = [
 "[1;29m",      # Black = 0;	   
 "[1;34m",	# Blue = 1;	   
 "[1;44m",	# BlackOnBlue = 2; 
 "[1;35m",	# Pink = 3;	   
 "[1;33;44m",	# GoldOnBlue = 4;  
 "[1;47;34m",	# BlueOnGray = 5;  
 "[1;1;31m",	# Red = 6;	   
 "[1;1;33m",	# Gold = 7;	   
 "[1;1;43;34m",	# BlueOnGold = 8;  
 "[1;1;37m",	# Gray = 9;	   
 "[1;30;47m",	# BlackOnGray = 10;
];

(string s) color(int c, string ins)
{
  s = @strcat(e33,colorCode[c],ins,e33,endEscape);
}

(string s) ncolor(int c, string ins) // Can use this version if \\e handling is available in current Swift
{
  s = @sprintf(@strcat("\\e",colorCode[c],ins,"\\e",endEscape));  // sprintf applies \\e escape processing
}

(string s) OLDcolor(int c, string ins)
{
    switch(c){
        case 0:
            s = @strcat(e33,"[1;29m",ins,e33,"[0m");
        case 1:
            s = @strcat(e33,"[1;34m",ins,e33,"[0m");
        case 2:
            s = @strcat(e33,"[1;44m",ins,e33,"[0m");
        case 3:
            s = @strcat(e33,"[1;35m",ins,e33,"[0m");
        case 4:
            s = @strcat(e33,"[1;33;44m",ins,e33,"[0m");
        case 5:
            s = @strcat(e33,"[1;47;34m",ins,e33,"[0m");
        case 6:
            s = @strcat(e33,"[1;1;31m",ins,e33,"[0m");
        case 7:
            s = @strcat(e33,"[1;1;33m",ins,e33,"[0m");
        case 8:
            s = @strcat(e33,"[1;1;43;34m",ins,e33,"[0m");
        case 9:
            s = @strcat(e33,"[1;1;37m",ins,e33,"[0m");
        case 10:
            s = @strcat(e33,"[1;30;47m",ins,e33,"[0m");
        default:
            s = ins;
    }
}
