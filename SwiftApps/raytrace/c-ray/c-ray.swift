type scene;
type image;
type scene_template;
type movie;

int threads;
int steps;
string resolution;
int delay;
int quality;

threads = @toint(@arg("threads","1"));
resolution = @arg("resolution","800x600");
steps = @toint(@arg("steps","10"));
delay = @toint(@arg("delay", "0"));
quality = @toint(@arg("quality", "75"));

scene_template template <"tscene">;

app (scene out) generate (scene_template t, int i, int total) {
	genscenes i total @t stdout=@out;
}

app (image o) render (scene i, string r, int t) {
	cray "-s" r "-t" t stdin=@i stdout=@o;
}

app (movie o) convert (image i[], int d, int q) {
	convert "-delay" d "-quality" q @filenames(i) @filename(o);
}

app (image outimg) ppm2gif (image inimg) {
    convert @filename(inimg) @filename(outimg);
}

scene scene_files[] <simple_mapper; location=".", prefix="scene.", suffix=".data">;
image image_files[] <simple_mapper; location=".", prefix="scene.", suffix=".ppm">;
image gif_files[] <simple_mapper; location=".", prefix="scene.", suffix=".gif">;
movie output_movie <"output.gif">;
 
foreach i in [1:steps] {
  scene_files[i] = generate(template, i, steps);
  image_files[i] = render(scene_files[i], resolution, threads);
  gif_files[i] = ppm2gif(image_files[i]);
}

#output_movie=convert(image_files, delay, quality);
output_movie=convert(gif_files, delay, quality);

