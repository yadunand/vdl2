#!/usr/bin/python
import os
import sys
import subprocess
import pykoa
import pykoa.tools
from pykoa.koaexception import cli_exception_handler

def setup_opts(argv):
    help_screen = """
goswift_stat [options] <run-number>

Checks the status of a run started in the background using goswift by reading the last n lines of the stdout/stderr file. Only 1 run is allowed to get the status of at a time.

Default number of lines is 10.

Type 'man goswift_stat' for details. \
"""

    parser = pykoa.tools.get_option_parser(help_screen)
    parser.add_option("--debug", dest="debugging",
                      default=False, action="store_true",
                      help="Get status from the last 10 lines of the Swift log file.")

    parser.add_option("-n", dest="lines",
                      default="10",
                      help="Set the number of lines to be printed out from the swift.out or log file.")

    (options, args) = pykoa.tools.parse_args(parser, argv)

    return (parser, options, args)

@cli_exception_handler
def main( argv=sys.argv[1:] ):
    (parser, options, args) = setup_opts(argv)

    if len(args) != 1:
        parser.print_help()
        return 1

    status = 0
    home = os.environ["HOME"]
    swift_dir = home +"/Swift"

    if not os.access( swift_dir, os.F_OK ):
        sys.stderr.write( "Swift has not been run on this machine before.: "+swift_dir+" does not exist\n")
        return 1

    work_dir = ""

    # Get the work directory
    try:
        work_dir = "run."+str(int(args[0]))
    except ValueError:
        sys.stderr.write( "Please specify a valid run id: "+args[0]+"\n" )
        return 1

    run_dir = swift_dir + "/" + work_dir
    if not os.access( run_dir, os.F_OK ):
        sys.stderr.write( run_dir+" does not exist!\n" )
        return 1

    try:
        int(options.lines)
    except ValueError:
        sys.stderr.write("Invalid value for '-n': '"+options.lines+"'\n")
        return 1

    # output from the log file if the log file exists
    if options.debugging:
        if not os.access( run_dir+"/script-"+args[0]+".log", os.F_OK):
            sys.stderr.write( run_dir+"/script-"+args[0]+".log does not exitst!\n" )
            return 1

        cmd = "tail -"+str(options.lines)+" "+run_dir+"/script-"+args[0]+".log"
        status = subprocess.call( cmd, close_fds=True,
                                  shell=True )
    # output from the swift.out file if it exists
    else:
        if not os.access( run_dir+"/swift.out", os.F_OK):
            sys.stderr.write( run_dir+"/swift.out does not exitst!\n" )
            return 1

        cmd = "tail -"+str(options.lines)+" "+run_dir+"/swift.out"
        status = subprocess.call( cmd, close_fds=True,
                                  shell=True )
           
    return status
    
if __name__ == "__main__":
    rc = main()
    sys.exit(rc)
