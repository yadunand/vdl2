#!/usr/bin/python
import os
import sys

import pykoa
import pykoa.tools
from pykoa.koaexception import cli_exception_handler

def setup_opts(argv):
    help_screen = """
goswift_info [options] <run-number>

Report information about a swiftrun, such as what is in the info file, tc file, sites file, and config file.

At least one options must be specified.

Type 'man goswift_info' for details. \
"""

    parser = pykoa.tools.get_option_parser(help_screen)
    parser.add_option("--tc", dest="tc",
                       default=False, action="store_true",
                       help="Print the entire tc file that was used during the run")

    parser.add_option("--sites", dest="sites",
                      default=False, action="store_true",
                      help="Print the entire sites file that was used during the run")

    parser.add_option("--config", dest="config",
                      default=False, action="store_true",
                      help="Print the entire config file that was used during the run")

    parser.add_option("--script", dest="script",
                      default=False, action="store_true",
                      help="Print the entire script file that was used during the run")

    parser.add_option("--info", dest="info",
                      default=False, action="store_true",
                      help="Print the info file that was created for the run")

    parser.add_option("--all", dest="all",
                      default=False, action="store_true",
                      help="Print all file that were used during the run")

    (options, args) = pykoa.tools.parse_args(parser, argv)

    return (parser, options, args)

def print_info(run_id):
    run_directory = os.environ["HOME"]+"/Swift/run."+str(run_id);
    sys.stdout.write("--------------BEGIN INFO FILE----------------\n")
    with open(run_directory+"/swift.info", "r") as swift_info:
        for line in swift_info.readlines():
            sys.stdout.write(line)
    sys.stdout.write("--------------END INFO FILE----------------\n\n")
    return 0

def print_tc(run_id):
    run_directory = os.environ["HOME"]+"/Swift/run."+str(run_id);
    sys.stdout.write("--------------BEGIN TC FILE-------------------\n")
    with open(run_directory+"/tc", "r") as swift_tc:
        for line in swift_tc.readlines():
            sys.stdout.write(line)
    sys.stdout.write("--------------END TC FILE-------------------\n\n")
    return 0

def print_sites(run_id):
    run_directory = os.environ["HOME"]+"/Swift/run."+str(run_id);
    sys.stdout.write("--------------BEGIN SITES FILE---------------\n")
    with open(run_directory+"/sites.xml", "r") as swift_sites:
        for line in swift_sites.readlines():
            sys.stdout.write(line)
    sys.stdout.write("--------------END SITES FILE---------------\n\n")
    return 0

def print_config(run_id):
    run_directory = os.environ["HOME"]+"/Swift/run."+str(run_id);
    sys.stdout.write("--------------BEGIN CONFIG FILE--------------\n")
    with open(run_directory+"/cf", "r") as swift_config:
        for line in swift_config.readlines():
            sys.stdout.write(line)
    sys.stdout.write("--------------END CONFIG FILE--------------\n\n")
    return 0

def print_script(run_id):
    run_directory = os.environ["HOME"]+"/Swift/run."+str(run_id);
    sys.stdout.write("--------------BEGIN SCRIPT FILE--------------\n")
    with open(run_directory+"/script.swift", "r") as swift_script:
        for line in swift_script.readlines():
            sys.stdout.write(line)
    sys.stdout.write("--------------END SCRIPT FILE--------------\n\n")
    return 0


@cli_exception_handler
def main( argv=sys.argv[1:] ):
    (parser, options, args) = setup_opts(argv)

    if len(args) != 1:
        parser.print_help()
        return 1

    if options.all:
        print_info(args[0])
        print_tc(args[0])
        print_sites(args[0])
        print_config(args[0])
        print_script(args[0])
        return 0

    if options.info:
        print_info(args[0])

    if options.tc:
        print_tc(args[0])

    if options.sites:
        print_sites(args[0])
    
    if options.config:
        print_config(args[0])

    if options.script:
        print_script(args[0])

    if not options.info and not options.tc and not options.sites and not options.config and not options.script:
        sys.stderr.write("Please specify at least one options. Type 'help goswift_info' for a list of valid options\n")

    return 0

if __name__ == "__main__":
    rc = main()
    sys.exit(rc)
