import os
import sys
import subprocess

import pykoa
import pykoa.tools
from pykoa.koaexception import cli_exception_handler

def setup_opts(argv):
    help_screen = """
goswift_list

List all run ids in the users run directory.

Type 'man goswift_list' for details. \
"""

    parser = pykoa.tools.get_option_parser(help_screen)
    
    (options, args) = pykoa.tools.parse_args(parser, argv)

    return (parser, options, args)

def main( argv=sys.argv[1:] ):
    (parser, options, args) = setup_opts(argv)

    if len(args) != 0:
        parser.print_help()
        return 1

    run_directory = os.environ["HOME"]+"/Swift";
    cmd = "/bin/ls -lGht "+run_directory+" | awk '{print $5, $6, $7, $8}'"

    ret = subprocess.call(cmd, close_fds=True,
                          shell=True)


    if ret != 0:
        sys.stderr.write( "FAILED TO LIST DIRECTORY\n" )
        pykoa.debug("Listing directory failed with error code: "+str(ret) + ": /bin/ls -lGht -w" + run_directory)
        return ret

    return 0

if __name__ == "__main__":
    rc = main()
    sys.exit(rc)
