import os
import sys
import commands

import pykoa
import pykoa.tools
from pykoa.koaexception import cli_exception_handler

def setup_opts(argv):
    help_screen = """
goswift_manage [option] <run-number>

Stop a currently running run or resume a stopped run.

Type 'man goswift_manage' for details. \
"""

    parser = pykoa.tools.get_option_parser(help_screen)
    parser.add_option("--stop", dest="stop",
                      default=False, action="store_true",
                      help="Stop/resume a run my the run id")

    (options, args) = pykoa.tools.parse_args(parser, argv)

    return (parser, options, args)

def stop(run_id):
    user = os.environ["USER"]
    (status, output) = commands.getstatusoutput("ps -u "+user+" -H -f")
    if status != 0:
        sys.stderr.write("Failed to get a list of running processes\n")
        return 1

    ppid = -1

    with open(os.environ["HOME"]+"/Swift/run."+run_id+"/swift.info") as swift_info:
        for line in swift_info.readlines():
            line_parsed = line.split(":")
            if line_parsed[0] == "Swift pid":
                ppid = int(line_parsed[1].strip())
                break

    if ppid == -1:
        sys.stderr.write("Could not find the Swift pid from the swift.info file\n")
        return 1

    processes = [ppid]
    for line in output.split("\n"):
        parsed_line = line.split()
        try:
            new_ppid = int(parsed_line[2])
        except ValueError:
            new_ppid = -1
        if ppid == new_ppid:
            processes.append(int(parsed_line[1]))
            ppid = int(parsed_line[1])
    for pid in processes:
        # Handle error in which process has already been stopped.
        os.kill(pid, 9)

    return 0

@cli_exception_handler
def main( argv=sys.argv[1:] ):
    (parser, options, args) = setup_opts(argv)
    ret = -1

    if len(args) != 1:
        parser.print_help()
        return 1

    if options.stop:
        sys.stdout.write("Stopping Swift run number: "+args[0]+"\n")
        ret = stop(args[0])

    return ret

if __name__ == "__main__":
    rc = main()
    sys.exit(rc)
