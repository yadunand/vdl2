#!/bin/bash -x

# Usage: $0 <dataset_name> <tagname> <tagname> ...
#e.g. sh locatewrap.sh cofeemug color size material
# Required: Env variable catloc pointing to the directory containing catalog.py

if [ $# -le 1 ]
then
 echo "Usage: $0 <dataset_name> <tagnamei1> LIKE <value1> <tagname2> LIKE <valu2> ..."
 exit 1
fi

dsetname=$1
shift

for tagvalpair in "$@"
do
  tagname=`echo $tagvalpair|awk -F= '{print $1}'`
  tagval=`echo $tagvalpair|awk -F= '{print $2}'`

  python $catloc/catalog.py -text query_datasets $catid name EQUAL $dsetname
  exprstr="{\"name\":\"$dsetname\"}"
  retstr=$(python $catloc/catalog.py create_dataset $catid $exprstr)

  python $catloc/catalog.py create_annotation_def $catid "$tagname" 'text'
  dsetid=`echo $retstr |awk -F, '{print $2}'`

  python $catloc/catalog.py add_dataset_annotation $catid $dsetid "{\"$tagname\":\"$tagval\"}"

done

