type file;

app (file _out, file _err) tag (string _datasetname, string[] _tagargs){
    tag_dataset _datasetname _tagargs stdout=@_out stderr=@_err;
}

app (file _ds) locate (string _tagargs){
    locate_dataset _tagargs stdout=@_ds;
}

file tagout[]<simple_mapper; location="outdir", prefix="tag.",suffix=".out">;
file tagerr[]<simple_mapper; location="outdir", prefix="tag.",suffix=".err">;

foreach i in [1:3]{
  (tagout[i], tagerr[i]) = tag(@strcat(@arg("dsname","/data/maheshwari/z"), i), @strsplit(@arg("tagarg"), "\\s"));
}

file ds<"ds.txt">;

ds=locate(@arg("locatearg","atagname=tagvalue1"));

