#!/bin/bash -x

# Usage: $0 <dataset_name> <tagname>=<value>
#e.g. sh tagwrap.sh cofeemug color=white size=16oz material=clay
# Required: Env variable catloc pointing to the directory containing catalog.py

if [ $# -le 1 ]
then
 echo "Usage: $0 <dataset_name> <tagnamei1>=<value1> <tagname2>=<valu2> ..."
 exit 1
fi

dsetname=$1
shift

for tagvalpair in "$@"
do
  tagname=`echo $tagvalpair|awk -F= '{print $1}'`
  tagval=`echo $tagvalpair|awk -F= '{print $2}'`

  python $catloc/catalog.py -text query_datasets $catid name EQUAL $dsetname
  exprstr="{\"name\":\"$dsetname\"}"
  retstr=$(python $catloc/catalog.py create_dataset $catid $exprstr)

  python $catloc/catalog.py create_annotation_def $catid "$tagname" 'text'
  dsetid=`echo $retstr |awk -F, '{print $2}'`

  python $catloc/catalog.py add_dataset_annotation $catid $dsetid "{\"$tagname\":\"$tagval\"}"

done

