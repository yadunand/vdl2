<!DOCTYPE html>
<html lang="en">
   <head>
      <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
      <title>Try Swift</title>
      <link rel="stylesheet" type="text/css" href="css/tryswift.css">

      <!-- Google analytics -->
      <script type="text/javascript">
         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', 'UA-25069254-1']);
         _gaq.push(['_trackPageview']);

         (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();
      </script>
   </head>

   <body>
      <div id="scriptNav">
         <div id="example">
            Example: 
            <select id="scriptDropdown">
            </select>
         </div>
         <div id="explain">
         </div>
      </div> <!-- scriptNav -->
     
      <!-- Swift code editor -->   
      <div id="editor">
         <script src="js/ace/ace.js" type="text/javascript" charset="utf-8"></script>
         <script>
            var editor = ace.edit("editor");
            editor.setFontSize('13px');
            editor.setTheme("ace/theme/solarized_light");
            editor.getSession().setMode("ace/mode/text");
            editor.setShowPrintMargin(false);
            editor.setHighlightActiveLine(false);
            editor.setDisplayIndentGuides(false);
         </script>
      </div> <!-- editor -->

     <!-- Execute and reset buttons -->
     <div id="editorButtons">
        <button id="executeButton">Execute</button>
        <button id="resetButton">Reset</button>
        <select id="outputs">
           <option>File outputs</option>
        </select>
     </div> <!-- editorButtons -->
     
     <!-- Textual swift output and ticker -->       
     <div id="swiftOutputDiv">
        <pre id="swiftOutput"></pre> 
     </div> <!-- swiftOutputDiv -->
            
      <script>
         var scriptCounter=0;
         var scripts=[
         <?php
            $scriptFiles = array();
            $directory = opendir("scripts");
            while($script = readdir($directory)) {
               if(substr($script, -6) == ".swift") {
                  $scriptFiles[] = $script;
               }   
            }   
            closedir($directory);
            sort($scriptFiles);
            foreach($scriptFiles as $script) {
               $description = file_get_contents(str_replace(".swift", ".txt", "scripts/$script"));
               $description = preg_replace('~[\r\n]+~', '', $description);
               $info = str_replace(".swift", ".html", $script);
               echo "[ \"scripts/$script\", \"$description\", \"scripts/$info\" ],";
            }
         ?> ];
      </script>
      <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
      <script src="js/logtail.js"></script>
      <script type="text/javascript">

      // Popup a new browser window in center of screen
      function popupwindow(url, name, w, h) {
         var left = (screen.width/2)-(w/2);
         var top = (screen.height/2)-(h/2);
         return window.open(url, name, 'width='+w+', height='+h+', top='+top+', left='+left);
      } 
    
      // Fill editor with script selected from dropdown 
      function updateScript(index) {
         $.get(scripts[index][0], null, function (data) {
            editor.setValue(data);
            editor.gotoLine(1);
         }, "text");
         $('#explain').html("<button id=\"explainButton\" value=\"" + scripts[index][2] + "\">Explain</button>");
      }         

      function resize() {
         $('#editor').height( $(document).height() * .55 );
         $('#swiftOutputDiv').height( $(document).height() * .35 );
      }

      function hideOutputs() {
         $('#outputs').css('visibility', 'hidden');
      }

      function showOutputs() {
         $('#outputs').css('visibility', 'visible');
      } 

      $(document).ready(function() {
         resize();
         updateScript(0);
         $(window).resize(function() { resize(); });
         $('#resetButton').click(function() { 
            updateScript($('#scriptDropdown').find('option:selected').index()); 
            hideOutputs();
         })

         $.each(scripts, function(key, value) {   
            $('#scriptDropdown')
               .append($("<option></option>")
               .attr("value",key[0])
               .text(value[1])); 
         });

         $('#scriptDropdown').change(function() {
            updateScript($(this).find('option:selected').index());
            hideOutputs();
         });
 
         $(document).on("change", "#outputs", function(){
            popupwindow($('#outputs').val(), '', 800, 600); 
            // $('#swiftOutput').load($("#outputs").val());
         });

         $(document).on("click", "#explainButton", function(){
            popupwindow($(this).attr("value"), $(this).text(), 800, 600);
            return false;
         });

         $('#executeButton').click(function() {
           sourceCode=editor.getSession().getValue();
           if(!sourceCode) {
              alert("Source text is empty!");
              return false;
           }
           $('#outputs').html('<option>File outputs</option>');

           $.post("tryswift.php", {source: sourceCode}).done(function(data) { 
              var urlArray = data.split("\n");
              tailf(urlArray[0], "#swiftOutput");
              $.post("getfiles.php", {dir: urlArray[1]}).done(function(filedata) {
                 $('#outputs').append(filedata);
                 showOutputs();
              });
           });
        })
     })
     </script>
   </body>
</html>
