#!/bin/bash

src="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
dst=$1

if [ -z "$dst" ]; then
   echo Usage: pushTo.sh destination
   exit 1
fi

if [ ! -d "$dst" ]; then
  echo Directory $dst does not exist
  exit 1
fi

echo Copying from $src to $dst
rsync -r --exclude=.svn --exclude=pushTo.sh $src/* $dst
