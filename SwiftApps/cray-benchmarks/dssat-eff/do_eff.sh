#!/bin/bash

export PATH=$PWD/swift-tickerfix/bin:$PATH
export STORAGE=$HOME

# tar and remove
movelogs()
{
    dir=`ls -1rtd run* | tail -1`
    tar cvfz $dir.tar.gz $dir
    cp $dir.tar.gz $STORAGE && rm $dir.tar.gz && rm -rf $dir/*
}

./dssat hera 100 3600 3200 $PWD 100 && movelogs
./dssat hera 300 3600 6400 $PWD 100 && movelogs
./dssat hera 500 3600 12800 $PWD 100 && movelogs
./dssat hera 588 3600 12800 $PWD 100 && movelogs

./dssat hera 100 3600 3200 $PWD 200 && movelogs
./dssat hera 300 3600 6400 $PWD 200 && movelogs
./dssat hera 500 3600 12800 $PWD 200 && movelogs
./dssat hera 588 3600 12800 $PWD 200 && movelogs

./dssat hera 100 3600 3200 $PWD 150 && movelogs
./dssat hera 300 3600 6400 $PWD 150 && movelogs
./dssat hera 500 3600 12800 $PWD 150 && movelogs
./dssat hera 588 3600 12800 $PWD 150 && movelogs

