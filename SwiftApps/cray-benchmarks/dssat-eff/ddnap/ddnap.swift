type file;

app ddnap (string delay, string i, string o)
{
  ddnap i o delay;
}

string delay=@arg("s","1");
string infile=@arg("input");
string outfile=@arg("output");

foreach j in [1:@toint(@arg("n","1"))] {
  ddnap(delay, infile, outfile);
}
