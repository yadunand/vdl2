#!/bin/bash

for log in $@
do
   stime=`head -1 $log`
   etime=`tail -1 $log`
   python -c "print 10000 / ( $etime - $stime )"
done
