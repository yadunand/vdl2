#To be run as a gnuplot script as follows:
# $ gnuplot plotit.gp

set terminal png enhanced size font "/usr/share/fonts/truetype/msttcorefonts/verdana.ttf" 18 1000,1000
#set term postscript eps enhanced 
#set terminal svg enhanced size 1000 1000

set style line 2 linecolor rgb "blue"
set output "activeplot.png"
set nokey
set xlabel "Time in sec" font "/usr/share/fonts/truetype/msttcorefonts/verdana.ttf,20"
set ylabel "number of active jobs" font "/usr/share/fonts/truetype/msttcorefonts/verdana.ttf,20"
set title "Active jobs" font "/usr/share/fonts/truetype/msttcorefonts/verdana.ttf,22"
plot "plot_active.txt" using 1:2 with lines lc rgb "skyblue" lw 4 notitle;

set output "cumulativeplot.png"
set xlabel "Time in seconds"
set ylabel "number of completed jobs"
set title "Cumulative jobs"
plot "plot_cumulative.txt" using 1:2 with lines

