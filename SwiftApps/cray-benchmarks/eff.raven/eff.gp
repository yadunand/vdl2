#set terminal png font "/usr/share/fonts/truetype/msttcorefonts/verdana.ttf" 12 size 4000,2800
#set term postscript eps enhanced 
#set terminal svg enhanced size 2000 800
#set terminal postscript enhanced
set terminal pdf

#set output "eff90.ps"
#set output "eff90.png"
set output "eff90.pdf"

set decimal locale
#set format x "%'g"

set style line 1 lt 1 lw 8 pt 3 lc rgb "red"
set style line 2 lt 1 lw 8 pt 3 lc rgb "red"
set style line 3 lt 1 lw 8 pt 3 lc rgb "blue"
set style line 4 lt 1 lw 8 pt 3 lc rgb "blue"
set yrange [90:100]

set key above
set xlabel "Number of Processors" 
set ylabel "Efficiency" 
plot "eff100.txt" using 1:5 with linespoints pt 5 lw 5 ps 0.5 lc rgb "red" title "100 second tasks", "eff200.txt" using 1:5 with linespoints pt 7 ps 0.5 lw 5 lc rgb "orange" title "200 second tasks", "eff400.txt" using 1:5 with linespoint pt 9 ps 0.5 lw 5 lc rgb "#1E90FF" title "400 seconds"
