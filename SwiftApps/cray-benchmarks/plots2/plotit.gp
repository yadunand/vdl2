#To be run as a gnuplot script as follows:

# $ gnuplot plotit.gp

set terminal png enhanced size 1000,1000
#set term postscript eps enhanced 
#set terminal svg enhanced size 1000 1000
set style line 1 linecolor rgb "blue"
set output "activeplot.png"
set nokey
set xlabel "Time in sec"
set ylabel "number of active jobs"
set title "Active jobs"
plot "plot_active.txt" using 1:2 with line

#multiplies the active jobs by 24 to take into account the number of openmp
#procs running on Beagle
set output "cumulativeplot-openmp.png"
set xlabel "Time in seconds"
set ylabel "number of completed jobs"
set title "Cumulative SciColSim-openMP jobs"
plot "plot_cumulative.txt" using 1:($2*24) with lines

set output "cumulativeplot.png"
set xlabel "Time in seconds"
set ylabel "number of completed jobs"
set title "Cumulative jobs"
plot "plot_cumulative.txt" using 1:2 with lines

set output "scs.png"
set xlabel "Evolution"
set ylabel "Value of T"
set title "SciColSim evolution Results"
plot "T.data" using 1 with lines 

set output "scs_loss.png"
set title "SciColSim evolution loss Results"
set xlabel "Evolution"
set ylabel "Value of loss(AR)"
plot "anneal.data" using 1 with lines 

set output "multiloss.png"
set title "SciColSim evolution loss Results"
set key auto
set yrange [0:200]
set xlabel "Evolution"
set ylabel "loss"
plot "multiloss.txt" using 3 with lines title "multiloss mean val",  "multiloss.txt" using ($3+$4) with lines title "+stddev", "multiloss.txt" using ($3-$4) with lines title "-stddev"
