#!/bin/bash

for log in $@
do
   stime=`head -1 $log`
   etime=`tail -1 $log`
   expr $etime - $stime
done
