set terminal png enhanced font "/usr/share/fonts/truetype/msttcorefonts/verdana.ttf" 16 size 1000,1000

set output "expectedtime.png"
set multiplot
set xlabel "Number of Processors" 

set ylabel "Time in Seconds" 
plot "eff.txt" using 1:2 with lines lc rgb "green" lw 2 title "Optimal Time", "eff.txt" using 1:3 with lines lc rgb "red" lw 2 title "Real Time"
