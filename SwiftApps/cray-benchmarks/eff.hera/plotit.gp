#To be run as a gnuplot script as follows:
# $ gnuplot plotit.gp

set terminal png enhanced font "/usr/share/fonts/truetype/msttcorefonts/verdana.ttf" 18 size 1000,1000
#set term postscript eps enhanced 
#set terminal svg enhanced size 1000 1000

#set linestyle 3 lt 3 lw 1 
set output "activeplot.png"
set nokey
set xlabel "Time in Seconds" font "/usr/share/fonts/truetype/msttcorefonts/verdana.ttf,20"
set ylabel "Active Jobs" font "/usr/share/fonts/truetype/msttcorefonts/verdana.ttf,20"
set title "Active jobs" font "/usr/share/fonts/truetype/msttcorefonts/verdana.ttf,22"
plot "plot_active.txt" using 1:2 with lines lc rgb "orange" lw 2 title "Active Tasks"
# "plot_available.txt" using 1:2 with filledcurves lc rgb "skyblue" lw 2 title "Available Cores"
#plot "plot_available.txt" using 1:2 with filledcurves lc rgb "skyblue" lw 2 title "Available Cores", "plot_active.txt" using 1:2 with filledcurves lc rgb "orange" lw 2 title "Active Tasks"
set output "cumulativeplot.png"
set xlabel "Time in seconds"
set ylabel "number of completed jobs"
set title "Cumulative jobs"
plot "plot_cumulative.txt" using 1:2 with lines

