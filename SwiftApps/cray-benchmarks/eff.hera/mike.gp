#set terminal png size 2000,800
#set term postscript eps enhanced 
#set terminal svg enhanced size 2000 800
#set terminal postscript enhanced
set nokey
set terminal pdf
set output "mike.pdf"
set decimal locale
set format y "%'g"

# enhanced font "/usr/share/fonts/truetype/msttcorefonts/verdana.ttf" 16 size 1000,400
set style line 1 lt 1 lw 8 pt 3 lc rgb "red"
set style line 2 lt 1 lw 8 pt 3 lc rgb "red"
set style line 3 lt 1 lw 8 pt 3 lc rgb "blue"
set style line 4 lt 1 lw 8 pt 3 lc rgb "blue"
#set output "eff90.png"

set xlabel "Number of Processors" 
set ylabel "Efficiency" 
set yrange [50:100]
plot "mike.txt" using 1:2 with linespoints pt 5 linewidth 5 ps 0.5 lc rgb "red" 

