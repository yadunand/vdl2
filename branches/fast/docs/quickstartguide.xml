<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd" [] >

<article>
	<articleinfo revision="0.2">
		<title>Swift Quick Start Guide</title>
		<subtitle>Source Control $LastChangedRevision: 2810 $</subtitle>
	
		<abstract>
			<formalpara>
				<title></title>
				<para>
				
					The impatient may find the <ulink
					url="reallyquickstartguide.php">Swift Really Quick Start
					Guide</ulink> to be more convenient.

					This guide describes the steps needed to download, install,
					configure, and run the basic examples for Swift. If you are
					using a pre-installed version of Swift, you can skip 
					directly to the <link linkend="configure">configuration
					section</link>.

				</para>
			</formalpara>
		</abstract>
	</articleinfo>

	<sect1 id="download">
		<title>Downloading a Swift Distribution</title>
		
		<para>
		
			There are three main ways of getting the Swift implementation: <link
			linkend="dl-stable">stable releases</link>, <link
			linkend="dl-nightly">nightly builds</link>, and the <link
			linkend="dl-repository">source code repository</link>. 

		</para>
		
		<sect2 id="dl-stable">
			<title>Stable Releases</title>
		
			<para>
			
				Stable releases can be obtained from the Swift download page:
				<ulink
				url="http://www.ci.uchicago.edu/swift/downloads/index.php#stable">Swift
				Downloads Page</ulink>. Once you downloaded the package, please
				move to the <link linkend="install">install section</link>.

			</para>
		</sect2>
		
		<sect2 id="dl-nightly">
			<title>Nightly Builds</title>
		
			<para>
			
				Swift builds and tests are being run every day. The <ulink
				url="http://www.ci.uchicago.edu/swift/downloads/index.php#nightly">Swift
				downloads page</ulink> contains links to the latest build and
				test page. The nightly builds reflect a development version of
				the Swift code and should not be used in production mode. After
				downloading a nightly build package, please continue to the
				<link linkend="install">install section</link>.
			
			</para>
		</sect2>
		
		<sect2 id="dl-repository">
			<title>Source Repository</title>
		
			<para>
			
				Details about accessing the Swift source repository together with
				build instructions are available on the <ulink
				url="http://www.ci.uchicago.edu/swift/downloads/index.php#nightly">Swift
				downloads page</ulink>. Once built, the <filename
				class="directory">dist/swift-svn</filename> directory
				will contain a self-contained build which can be used in place or moved to a different location.
				You should then proceed to the <link
				linkend="configure">configuration section</link>.
			
			</para>
		</sect2>
	</sect1>
	
	<sect1 id="install">
		<title>Installing a Swift Binary Package</title>
		
		<para>
		
			Simply unpack the downloaded package (<filename
			class="file">swift-&lt;version&gt;.tar.gz</filename>) into a
			directory of your choice:
			
<screen>
<prompt>&gt;</prompt> <command>tar</command> <option>-xzvf</option> <filename
class="file">swift-&lt;version&gt;.tar.gz</filename>
</screen>
			
			This will create a <filename
			class="directory">swift-&lt;version&gt;</filename> directory
			containing the build.
		
		</para>
	</sect1>
	
	<sect1 id="configure">
		<title>Configuring Swift</title>
		
		<para>
		
			This section describes configuration steps that need to be taken in
			order to get Swift running. Since all command line tools provided
			with Swift can be found in the <filename
			class="directory">bin/</filename> directory of the Swift distribution, it may
			be a good idea to add this directory to your <envar>PATH</envar>
			environment variable:
			
<screen>
<prompt>&gt;</prompt> <command>export</command> <envar>PATH</envar>=<filename
class="directory">/path/to/swift/bin</filename>:<envar>$PATH</envar>
</screen>
			
		</para>
		<sect2 id="security"><title>Grid Security</title>
			<para>For local execution of jobs, no grid security configuration
				is necessary.
			</para>
			<para>However, when submitting jobs to a remote machine using Globus
				Toolkit services, Swift makes use of the
				<ulink
				url="http://www.globus.org/toolkit/docs/4.0/security/key-index.html">
				Grid Security Infrastructure (GSI)</ulink> for authentication
				and authorization. The requirements for this are detailed in
				the following sections. Note that GSI is not required to be
				configured for local execution (which will usually be the
				case when first starting with Swift).
			</para>

		<sect3 id="certs">
	
			<title>User Certificate</title>
			<para>
			
				GSI requires a certificate/private key
				pair for authentication to 
				<ulink url="http://www.globus.org/toolkit">Globus Toolkit</ulink>
				services. The certificate and private key should
				be placed into the <filename
				class="file">~/.globus/usercert.pem</filename> and <filename
				class="file">~/.globus/userkey.pem</filename> files,
				respectively.
			
			</para>
		
		</sect3>
		
		<sect3 id="cas">
		
			<title>Certificate Authorities Root Certificates</title>
			
			<para>
			
				The Swift client libraries are generally required to authenticate
				the services to which they connect. This process requires the
				presence on the Swift submit site of the root certificates used
				to sign the host certificates of services used. These root
				certificates need to be installed in either (or both) the
				<filename class="directory">~/.globus/certificates</filename>
				and <filename
				class="directory">/etc/grid-security/certificates</filename>
				directories. A package with the root certificates of the
				certificate authorities used in the <ulink
				url="http://www.teragrid.org">TeraGrid</ulink> can be found
				<ulink
				url="http://security.teragrid.org/TG-CAs.html">here</ulink>.
			
			</para>
		
		</sect3>
		</sect2>
				
		<sect2>
		
			<title>Swift Properties</title>
			
			<para>
			
				A Swift properties file (named <filename
				class="file">swift.properties</filename>) can be used to
				customize certain configuration aspects of Swift. A shared
				version of this file, <filename
				class="file">etc/swift.properties</filename>
				in the installation directory
				can be used to provide installation-wide defaults. A per-user
				properties file, <filename
				class="file">~/.swift/swift.properties</filename> can be used for
				user specific settings. Swift first loads the shared
				configuration file and, if present, the user configuration file.
				Any properties not explicitly set in the user configuration file
				will be inherited from the shared configuration file. Properties
				are specified in the following format:

<screen>
<property>name</property>=<parameter>value</parameter>
</screen>

				For details about the various properties Swift accepts, please
				take a look at the <ulink
				url="http://www.ci.uchicago.edu/swift/guides/userguide.php#properties">Swift
				Properties Section</ulink> in the <ulink
				url="http://www.ci.uchicago.edu/swift/guides/userguide.php">Swift
				User Guide</ulink>.

			</para>
		
		</sect2>
	</sect1>
	
	<sect1 id="examples">
		
		<title>Running Swift Examples</title>
		
		<para>
		
			The Swift examples can be found in the <filename
			class="directory">examples</filename> directory in the Swift distribution.
			The examples are written in the <ulink
			url="http://www.ci.uchicago.edu/swift/guides/userguide/language.php">SwiftScript
			language</ulink>, and have <filename class="file">.swift</filename> as
			a file extension. 

		</para>
		
		<para>
		
			The Grid Security Infrastructure, which Swift uses, works with
			limited time certificates called proxies. These proxies can be
			generated from your user certificate and private key using one of
			<command>grid-proxy-init</command> or
			<command>cog-proxy-init</command> (the latter being a Java Swing
			interface to the former).
		
		</para>
				
		<para>
		
			Execution of a Swift workflow is done using the
			<command>swift</command> command, which takes the Swift
			workflow file name as an argument:
			
<screen>
<prompt>&gt;</prompt> <command>cd examples/swift</command>
<prompt>&gt;</prompt> <command>swift</command> <option><filename
class="file">first.swift</filename></option>
</screen>

			The <ulink 		
			url="http://www.ci.uchicago.edu/swift/guides/userguide.php#swiftcommand">Swift
			Command Options Section</ulink> in the <ulink 			
			url="http://www.ci.uchicago.edu/swift/guides/userguide.php">Swift 			
			User Guide</ulink> contains details about the various options of the
			<command>swift</command>.
		
		</para>
		
	</sect1>
</article>
