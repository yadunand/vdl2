<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd" [] >

<article>
	<articleinfo revision="$LastChangedRevision$">
		<title>Swift log plotting and the some internal mechanics of Swift</title>
	</articleinfo>

	<section id="overview">
		<title>Overview</title>
<para>
This document attempts to explain some of the meaning of the Swift
log-processing plots, giving an explanation of how some of Swift's
execution mechanism works and of some of the terminology used.
</para>
	</section>

<section id="execute"><title>'execute' - SwiftScript app {} block invocations</title>

<para>
When a SwiftScript program invokes a application procedure (one with an
app {} block), an 'execute' appears in the log file in START state. When
all attempts at execution have finished (either successfully or unsuccessfully)
then the execute will go into END_SUCCESS or END_FAILURE state. A workflow
is successful if and only if all invocations end in END_SUCCESS.
</para>

<para>
The execute states represent progress
through the karajan procedure defined in
<filename>libexec/execute-default.k</filename>.
</para>
<para>State changes for execute logs are defined by karajan log calls throughout
this file.
</para>
<inlinemediaobject><imageobject><imagedata fileref="execute.png"></imagedata></imageobject></inlinemediaobject>

<para>An execute consists of multiple attempts to perform
<link linkend="execute2">execute2</link>s, with retries and replication
as appropriate. Retries and replication are not exposed through the states
of 'execute's.
</para>
<para>
Executes are uniquely identified within a run by their karajan thread ID,
which is present in the log files as the thread= parameter on execute
log messsages.
</para>

<para>
Here is a simple SwiftScript program which runs a foreach loop (<filename>few.swift</filename>):
<programlisting>
p() { 
    app {
        sleep "10s";
    }
}

foreach i in [1:8] {
    p();
}
</programlisting>

</para>

<para>
Using the <command>swift-plot-log</command> from the log processing module,
this graph gets generated to summarise execute state transitions:
</para>
<para>
<inlinemediaobject><imageobject><imagedata fileref="plot-tour/pregenerated/execute.png"></imagedata></imageobject></inlinemediaobject>
</para>
<para>
In this graph, the forloop calls p() eight times. Because there are no
dependencies between those eight invocations, they are all invoked at the same
time, around 1s into the run. This is show on the graph by the JOB_START line
going from zero up to eight at around x=1s. As time passes, the sleep jobs
complete, and as they do so the number of jobs in END_SUCCESS state increases.
When all eight jobs are in END_SUCCESS state, the run is over.
</para>
<para>Here is a program with some data dependencies between invocations (<filename>dep.swift</filename>):

<programlisting>
$ cat dep.swift 
type file;

p(file f) { 
    app {
        sleep "10s";
    }
}

(file o) q() {
    app {
        touch @o;
    }
}

file intermediate = q();
p(intermediate);
</programlisting>

</para>

<para>
Here is a plot of the execute states for this program:
</para>
<para><inlinemediaobject><imageobject><imagedata fileref="plot-tour/pregenerated/execute-dep.png"></imagedata></imageobject></inlinemediaobject>
</para>
<para>
In this run, one invocation starts (q()) fairly quickly, 
but the other invocation (of p()) does not - instead, it does not start until
approximately the time that the q() invocation has reached END_SUCCESS. 
</para>

<para>
Finally in this section on 'execute', here is a demonstration of how the above
two patterns fit together in one program (<filename>few2.swift</filename>:
<programlisting>
type file;

(file o) p(file i) { 
    app {
        sleepcopy @i @o;
    }
}

file input &lt;"input"&gt;;
file output[];

foreach i in [1:8] {
    file intermediate;
    intermediate = p(input);
    output[i] = p(intermediate);
}
</programlisting>
</para>


<para>
In total the program has 16 invocations of p(), dependent on each other in
pairs. The dependencies can be plotted like this:

<screen>
$ <userinput>swift -pgraph few2.dot few2.swift</userinput>
$ dot -Tpng -o few2.png few2.dot 
</screen>

yielding this graph:
</para>

<para><inlinemediaobject><imageobject><imagedata fileref="plot-tour/pregenerated/few2.png"></imagedata></imageobject></inlinemediaobject> </para>

<para>
When this program is run, the first row of 8 invocations can all start at the
beginning of the program, because they have no dependencies (aside from on
the input file). This can be seen around t=4 when the start line jumps up to 8.
The other 8 invocations can only begin when the invocations they are dependent
on have finished. This can be seen in the graph - every time one of the first
invocations reaches END_SUCCESS, a new invocation enters START.
</para>

<para><inlinemediaobject><imageobject><imagedata fileref="plot-tour/pregenerated/execute-many-dep.png"></imagedata></imageobject></inlinemediaobject> </para>

</section>
<section id="execute2"><title>execute2 - one attempt at running an execute</title>
<para>
An execute2 is one attempt to execute an app procedure. execute2s are invoked
by <link linkend="execute">execute</link>, once for each retry or replication
attempt.
</para>
<para>The states of an execute2 represent progress through the execute2 karajan
procedure defined in <filename>libexec/vdl-int.k</filename>
</para>
<inlinemediaobject><imageobject><imagedata fileref="execute2.png"></imagedata></imageobject></inlinemediaobject>
<para>
Before an execute2 makes its first state log entry, it chooses a site to run on.
Then at the start of file stage-in, the execute2 goes into THREAD_ASSOCIATION
state. Once stagein is completed, the JOB_START state is entered, indicating
that execution of the job executable will now be attempted. Following that,
STAGING_OUT indicates that the output files are being staged out. If everything
is completed successfully, the job will enter JOB_END state.
</para>
<para>There are two exceptions to the above sequence: JOB_CANCELLED indicates that
the replication mechanism has cancelled this job because a different execute2
began actual execution on a site for the same execute. APPLICATION_EXCEPTION
indicates that there was an error somewhere in the attempt to stage in,
actually execute or stage out. If a job goes into APPLICATION_EXCEPTION state
then it will generally be retried (up to a certain number of times defined
by the "execution.retries" parameter) by the containing <link linkend="execute">execute</link>.
</para>

<para>
In this example, we use a large input file to slow down file staging so that
it is visible on an execute2 graph (<filename>big-files.swift</filename>):
<programlisting>
type file;  
  
(file o) p(file i) {   
    app {  
        sleepcopy @i @o;  
    }  
}  
  
file input &lt;"biginput"&gt;;  
file output[];  
  
foreach i in [1:8] {  
    output[i] = p(input);  
}  
</programlisting>
</para>

<para>
<inlinemediaobject><imageobject><imagedata fileref="plot-tour/pregenerated/execute2.png"></imagedata></imageobject></inlinemediaobject></para>

<para>
There is an initial large input file that must be staged in. This causes the first
jobs to be in stagein state for a period of time (the space between the
ASSOCIATED and JOB_START lines at the lower left corner of the graph). All
invocations share a single input file, so it is only staged in once and
shared between all subsequent invocations - once the file has staged in at the
start, there is no space later on between the ASSOCIATED and JOB_START lines
because of this.
</para>
<para>
Conversely, each invocation generates a large output file without there being
any sharing. Each of those output files must be staged back to the submit
side, which in this application takes some time. This can be seen by the large
amount of space between the STAGING_OUT and JOB_END lines.
</para>
<para>
The remaining large space on the graph is between the JOB_START and STAGING_OUT
lines. This represents the time taken to queue and execute the application
executable (and surrounding Swift worker-side wrapper, which can sometimes
have non-negligable execution times - this can be seen in the
<link linkend="info">info section</link>).
</para>

</section>

<section id="info"><title>wrapper info logs</title>
<para>
When a job runs, it is wrapped by a Swift shell script on the remote site that
prepares the job environment, creating a temporary directory and moving
input and output files around. Each wrapper invocation corresponds to a single
application execution. For each invocation of the wrapper, a log file is created.
Sometimes that log file is moved back to the submission side (when there is
an error during execution, or when the setting 
<ulink url="http://www.ci.uchicago.edu/swift/guides/userguide.php#engineconfiguration">wrapper.always.transfer</ulink>=true
is set) and placed in a <filename>*.d/</filename> directory corresponding in
name to the main log file.
</para>

<inlinemediaobject><imageobject><imagedata fileref="info.png"></imagedata></imageobject></inlinemediaobject>
<para>The states of the info logs represent progress through the wrapper
script, <filename>libexec/wrapper.sh</filename>.
</para>

<para>
For the same run of <filename>big-files.swift</filename> as shown in the
<link linkend="execute2">execute2 section</link>, here is a plot of states
in wrapper info log files:
</para>

<para>
<inlinemediaobject><imageobject><imagedata fileref="plot-tour/pregenerated/info.png"></imagedata></imageobject></inlinemediaobject></para>

<para>
The trace lines on this graph fit entirely within the space between JOB_START 
and STAGING_OUT on the corresponding execute2 graph, because the Swift worker node
wrapper script does not run until the submit side of Swift has submitted a
job for execution and that job has begun running.
</para>

<para>
Many of the lines on this plot are very close together, because many of the
operations take minimal time. The main space between lines is between
EXECUTE and EXECUTE_DONE, where the actual application executable is executing;
and between COPYING_OUTPUTS and RM_JOBDIR, where the large output files are
copied from a job specific working directory to the site-specific shared
directory. It is quite hard to distinguish on the graph where overlapping
lines are plotted together.
</para>

<para>
Note also that minimal time is spent copying input files into the job-specific
directory in the wrapper script; that is because in this run, the wrapper
script is using the default behaviour of making symbolic links in the job-specific
directory; symbolic links are usually cheap to create compared to copying file
content. However, if the <ulink url="http://www.ci.uchicago.edu/swift/guides/userguide.php#envvars">SWIFT_JOBDIR_PATH</ulink> parameter is set, then Swift will
copy the input file to the specified job directory instead of linking. This
will generally result in much more time being spent preparing the job directory
in the Swift wrapper, but in certain circumstances this time is overwhelmingly
offset by increased performance of the actual application executable (so on
this chart, this would be seen as an increased job directory preparation time,
but a reduced-by-more application executable time).
</para>


</section>

<section><title>Relation of logged entities to each other</title>
<para>Here is a simple diagram of how some of the above log channels along
with other pieces fit together:</para>
<inlinemediaobject><imageobject><imagedata fileref="logrelations.png"></imagedata></imageobject></inlinemediaobject>

</section>

</article>

