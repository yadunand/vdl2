Swift User Guide
================

:toc:
:icons:
:numbered:

include::overview[]

include::gettingStarted[]

include::language[]

include::configuration[]

include::debugging[]

link:http://swift-lang.org/docs/index.php[home]
