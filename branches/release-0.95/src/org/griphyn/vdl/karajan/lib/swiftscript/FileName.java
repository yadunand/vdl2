/*
 * Copyright 2012 University of Chicago
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.griphyn.vdl.karajan.lib.swiftscript;

import k.rt.Stack;

import org.globus.cog.karajan.analyzer.ArgRef;
import org.globus.cog.karajan.analyzer.CompilationException;
import org.globus.cog.karajan.analyzer.Scope;
import org.globus.cog.karajan.analyzer.Signature;
import org.globus.cog.karajan.compiled.nodes.Node;
import org.globus.cog.karajan.parser.WrapperNode;
import org.griphyn.vdl.karajan.lib.SwiftFunction;
import org.griphyn.vdl.mapping.AbstractDataNode;
import org.griphyn.vdl.mapping.DSHandle;
import org.griphyn.vdl.mapping.RootDataNode;
import org.griphyn.vdl.type.Type;
import org.griphyn.vdl.type.Types;

public class FileName extends SwiftFunction {
	private ArgRef<AbstractDataNode> var;
	private boolean inAppInvocation;
    
    @Override
    public Node compile(WrapperNode w, Scope scope) throws CompilationException {        
        Node self = super.compile(w, scope);
        // either execute(arguments(this)) or execute(named(stdxxx, this))
        if (hasAncestor(this, "swift:execute")) {
            inAppInvocation = true;
        }
        return self;
    }

    @Override
    protected Signature getSignature() {
        return new Signature(params("var"));
    }
    
    @Override
    protected Type getReturnType() {
        return Types.STRING;
    }

    @Override
	public Object function(Stack stack) {
        AbstractDataNode var = this.var.getValue(stack); 
		String s = argList(filename(var), inAppInvocation);
		DSHandle result = new RootDataNode(Types.STRING, s);
		if (PROVENANCE_ENABLED) {
		    int provid = SwiftFunction.nextProvenanceID();
		    SwiftFunction.logProvenanceParameter(provid, var, "input");
		    SwiftFunction.logProvenanceResult(provid, result, "filename");
		}
		
		return result;
	}
}
