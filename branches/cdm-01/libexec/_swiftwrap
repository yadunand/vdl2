#!/bin/bash
# this script must be invoked inside of bash, not plain sh
# note that this script modifies $IFS

infosection() {
        echo >& "$INFO"
	echo "_____________________________________________________________________________" >& "$INFO"
	echo >& "$INFO"
	echo "        $1" >& "$INFO" 
	echo "_____________________________________________________________________________" >& "$INFO"
	echo >& "$INFO"
}

info() {
	infosection "uname -a"
	uname -a 2>&1 >& "$INFO"
	infosection "id"
	id 2>&1 >& "$INFO"
	infosection "env"
	env 2>&1 >& "$INFO"
	infosection "df"
	df 2>&1 >& "$INFO"
        if [ -e "/proc/cpuinfo" ]; then
		infosection "/proc/cpuinfo"
		cat /proc/cpuinfo 2>&1 >& "$INFO"
	fi
	if [ -e "/proc/meminfo" ]; then
		infosection "/proc/meminfo"
		cat /proc/meminfo 2>&1 >& "$INFO"
	fi
	infosection "command line"
	echo $COMMANDLINE 2>&1 >& "$INFO"
	if [ -f "$STDOUT" ] ; then
		infosection "stdout"
		cat $STDOUT >& "$INFO"
	fi
	if [ -f "$STDERR" ] ; then
		infosection "stderr"
		cat $STDERR >& "$INFO"
	fi
}

logstate() {
	echo "Progress " `date +"%Y-%m-%d %H:%M:%S.%N%z"` " $@" >& "$INFO"
}

log() {
	echo "$@" >& "$INFO"
}

fail() {
	EC=$1
	shift
	if [ "$STATUSMODE" = "files" ]; then
		echo $@ >"$WFDIR/status/$JOBDIR/${ID}-error"
	fi
	log $@
	info
	if [ "$STATUSMODE" = "files" ]; then
		exit 0
	else
		exit $EC
	fi
}

checkError() {
	if [ "$?" != "0" ]; then
		fail $@
	fi
}

checkEmpty() {
	if [ "$1" == "" ]; then
		shift
		fail 254 $@
	fi
}

checkparamfile() {
	log "checking for paramfile"
	if [ "$1" == "-p" ]; then
		JOBDIR=$2
		PARAMFILE=${WFDIR}/parameters/${JOBDIR}/param-${ID}
	fi
	log "paramfile is: $PARAMFILE"
}

getarg() {
	NAME=$1
	shift
	VALUE=""
	SHIFTCOUNT=0
	if [ "$PARAMFILE" == "" ] && [ "$1" == "$NAME" ]; then
		shift
		let "SHIFTCOUNT=$SHIFTCOUNT+1"
		while [ "${1:0:1}" != "-" ] && [ "$#" != "0" ]; do
			VALUE="$VALUE $1"
			shift
			let "SHIFTCOUNT=$SHIFTCOUNT+1"
		done
		VALUE="${VALUE:1}"
	elif [ "$PARAMFILE" != "" ] && grep -E "^$NAME " $PARAMFILE ; then
		VALUE=$(grep -E "^$NAME " $PARAMFILE | cut -d ' ' -f 2-)
	else
		fail 254 "Missing $NAME argument"
	fi
}

openinfo() {
	exec 3<> $1
	INFO=3
}

closeinfo() {
	exec 3>&-
}

contains() {
	ARRAY=$1
	X=$2

	for a in ${!ARRAY}
	do
		if [[ ${a} == ${X} ]]; then 
			return 0
		fi
	done
	return 1
}

# Output: 
cdm_lookup() {
	FILE=$1
	CDM_FILE=$2

	RESULT="DEFAULT"
	if [ -f shared/cdm.pl ]; then 
		RESULT=$( perl shared/cdm.pl lookup $FILE < $CDM_FILE )
	fi
	echo $RESULT
}

cdm_action() {
	log "CDM_ACTION: $@"

	local JOBDIR=$1  # Given jobdir
	local MODE=$2    # INPUT or OUTPUT
	local FILE=$3    # User file
	local POLICY=$4  # DIRECT, BROADCAST, ... 
	shift 4
	local ARGS=$@

	case $POLICY in 
		DIRECT)
			DIRECT_DIR=${ARGS[0]}
			log "CDM[DIRECT]: Linking to $DIRECT_DIR/$FILE via $JOBDIR/$FILE"
			if [ $MODE == "INPUT" ]; then 
				[ -f "$DIRECT_DIR/$FILE" ]
				checkError 254 "CDM[DIRECT]: $DIRECT_DIR/$FILE does not exist!"
				ln -s $DIRECT_DIR/$FILE $JOBDIR/$FILE
				checkError 254 "CDM[DIRECT]: Linking to $DIRECT_DIR/$FILE failed!"
			elif [ $MODE == "OUTPUT" ]; then 
				mkdir -p $DIRECT_DIR
				checkError 254 "CDM[DIRECT]: mkdir -p $DIRECT_DIR failed!"
				touch $DIRECT_DIR/$FILE
				checkError 254 "CDM[DIRECT]: Touching $DIRECT_DIR/$FILE failed!"
				ln -s $DIRECT_DIR/$FILE $JOBDIR/$FILE
				checkError 254 "CDM[DIRECT]: Linking to $DIRECT_DIR/$FILE failed!"
			else 
				fail 254 "Unknown MODE: $MODE"
			fi
			;;
 		LOCAL)
 			TOOL=${ARGS[0]}
 			REMOTE_DIR=${ARGS[1]}
 			FLAGS=${ARGS[3]}
 			log "CDM[LOCAL]: Copying $DIRECT_DIR/$FILE to $JOBDIR/$FILE"
 			if [ $MODE == "INPUT" ]; then 
 				[ -f "$DIRECT_DIR/$FILE" ]
 				checkError 254 "CDM[LOCAL]: $REMOTE_DIR/$FILE does not exist!"
 				$TOOL $FLAGS $REMOTE_DIR/$FILE $JOBDIR/$FILE
 				checkError 254 "CDM[LOCAL]: Tool failed!"
 			elif [ $MODE == "OUTPUT" ]; then 
 				log "CDM[LOCAL]..."
 			else 
 				fail 254 "Unknown MODE: $MODE"
 			fi
 			;;
		BROADCAST)
			BROADCAST_DIR=${ARGS[0]}
			
			if [ $MODE == "INPUT" ]; then 
				log "CDM[BROADCAST]: Linking $JOBDIR/$FILE to $BROADCAST_DIR/$FILE"
				[ -f "$BROADCAST_DIR/$FILE" ]
				checkError 254 "CDM[BROADCAST]: $BROADCAST_DIR/$FILE does not exist!"
				ln -s $BROADCAST_DIR/$FILE $JOBDIR/$FILE
				checkError 254 "CDM[BROADCAST]: Linking to $BROADCAST_DIR/$FILE failed!"
			else 
				echo "CDM[BROADCAST]: Skipping output file: ${FILE}"
			fi
			;;
		GATHER)
			if [ $MODE == "INPUT" ]; then 
				fail 254 "Cannot GATHER an input file!"
			fi
	esac
}

cdm_local_output()
{
 	L=$1

	if [[ $CDM_FILE == "" ]]; then 
		return
	fi

 	CDM_POLICY=$( cdm_lookup $L $CDM_FILE )
	if [[ $CDM_POLICY == "LOCAL" ]]; then 
		cdm_local_output_perform $L $CDM_POLICY
	fi
}

cdm_local_output_perform()
{
	L=$1
	TOOL=$2
	REMOTE_DIR=$3
	FLAGS=$3
	log "Copying $REMOTE_DIR/$FILE to $JOBDIR/$FILE"
	mkdir -p $REMOTE_DIR
	checkError 254 "CDM[LOCAL]: mkdir -p $REMOTE_DIR failed!"
	$TOOL $FLAGS $JOBDIR/$FILE $REMOTE_DIR/$FILE
	checkError 254 "CDM[LOCAL]: Tool failed!"
}

cdm_gather()
{
	GATHER_OUTPUT=${*}
	if [[ $CDM_FILE == "" ]]; then 
		return
	fi
	if [[ $GATHER_OUTPUT == "" ]]; then 
		return
	fi
	
	cdm_gather_action $GATHER_MAX $GATHER_OUTPUT
}

COMMANDLINE=$@

# get the parent directory of the directory containing _swiftwrap, to use
# as the run directory
# this assumes that _swiftwrap is being executed from the top level of
# the shared directory, and that shared directory is in the top level
# of the workflow run directory
WFDIR=$(dirname $(dirname $0))

cd $WFDIR

# make the WFDIR absolute
WFDIR=$(pwd)
PARAMFILE=

openinfo "wrapper.log"
ID=$1
checkEmpty "$ID" "Missing job ID"

shift

checkparamfile "$@"

# JOBDIR might have been assigned through the -p option, or might
# be a parameter here
if [ "$JOBDIR" == "" ] ; then
	getarg "-jobdir" "$@"
	JOBDIR=$VALUE
	shift $SHIFTCOUNT
fi

getarg "-scratch" "$@"
SCRATCH=$VALUE
shift $SHIFTCOUNT

if [ "X$PROGRESSIVE_INFO" == "X" ] && [ "X$SCRATCH" != "X" ]; then
	INFODIR=$SCRATCH
else
	INFODIR=$WFDIR/info/$JOBDIR
fi
checkEmpty "$JOBDIR" "Missing job directory prefix"
mkdir -p $INFODIR
closeinfo

rm -f "$INFODIR/${ID}-info"
openinfo "$INFODIR/${ID}-info"

logstate "LOG_START"
infosection "Wrapper"

getarg "-e" "$@"
EXEC=$VALUE
shift $SHIFTCOUNT

getarg "-out" "$@"
STDOUT=$VALUE
shift $SHIFTCOUNT

getarg "-err" "$@"
STDERR=$VALUE
shift $SHIFTCOUNT

getarg "-i" "$@"
STDIN=$VALUE
shift $SHIFTCOUNT

getarg "-d" "$@"
DIRS=$VALUE
shift $SHIFTCOUNT

getarg "-if" "$@"
INF=$VALUE
shift $SHIFTCOUNT

getarg "-of" "$@"
OUTF=$VALUE
shift $SHIFTCOUNT

getarg "-k" "$@"
KICKSTART=$VALUE
shift $SHIFTCOUNT

getarg "-cdmfile" "$@"
CDM_FILE=
if [ "X$VALUE" != "X" ]; then 
	CDM_FILE=shared/$VALUE
fi
shift $SHIFTCOUNT

getarg "-status" "$@"
STATUSMODE=$VALUE
shift $SHIFTCOUNT

declare -a CMDARGS
if [ "$PARAMFILE" == "" ] && [ "$1" == "-a" ] ; then
	shift
	CMDARGS=("$@")
elif [ "$PARAMFILE" != "" ] ; then
	CMDARGS=()
	FIRST=1
	while read line ; do
		if [ "$FIRST" == "1" ] ; then
			CMDARGS=("$line")
			FIRST=0
		else
			CMDARGS=("${CMDARGS[*]}" "$line")
		fi
	done < <(grep -E "^-a " $PARAMFILE | cut -d " " -f 2-)
else
	fail 254 "Missing arguments (-a option)"
fi

if [ "$STATUSMODE" = "files" ]; then
	mkdir -p $WFDIR/status/$JOBDIR
fi

if [ "X$CDM_FILE" != "X" ]; then
	logstate "SOURCE_CDM_LIB $WFDIR/shared/cdm_lib.sh"
	source $WFDIR/shared/cdm_lib.sh
	checkError 254 "Could not source: $WFDIR/shared/cdm_lib.sh"
fi

if [ "X$SCRATCH" != "X" ]; then
	log "Job directory mode is: local copy"
	DIR=$SCRATCH/$JOBDIR/$ID
	COPYNOTLINK=1
else
	log "Job directory mode is: link on shared filesystem"
	DIR=jobs/$JOBDIR/$ID
	COPYNOTLINK=0
fi

PATH=$PATH:/bin:/usr/bin

if [ "$PATHPREFIX" != "" ]; then
	export PATH=$PATHPREFIX:$PATH
fi

if [ "$SWIFT_EXTRA_INFO" != "" ]; then
	log "EXTRAINFO=$($SWIFT_EXTRA_INFO)"
fi

if [ "X${EXEC:0:1}" != "X/" ] ; then
	export ORIGEXEC=$EXEC
	export EXEC=$(which $EXEC)
	if [ "X$EXEC" = "X" ] ; then
		fail 254 "Cannot find executable $ORIGEXEC on site system path"
	fi
fi

log "DIR=$DIR"
log "EXEC=$EXEC"
log "STDIN=$STDIN"
log "STDOUT=$STDOUT"
log "STDERR=$STDERR"
log "DIRS=$DIRS"
log "INF=$INF"
log "OUTF=$OUTF"
log "KICKSTART=$KICKSTART"
log "CDM_FILE=$CDM_FILE"
log "ARGS=$@"
log "ARGC=$#"

IFS="|"

logstate "CREATE_JOBDIR"
mkdir -p $DIR
checkError 254 "Failed to create job directory $DIR"
log "Created job directory: $DIR"

logstate "CREATE_INPUTDIR"
for D in $DIRS ; do
	mkdir -p "$DIR/$D" 2>&1 >>"$INFO"
	checkError 254 "Failed to create input directory $D"
	log "Created output directory: $DIR/$D"
done

logstate "LINK_INPUTS"
for L in $INF ; do
        CDM_POLICY=$( cdm_lookup $L $CDM_FILE )
	if [ $CDM_POLICY != "DEFAULT" ]; then
		log "CDM_POLICY: $L -> $CDM_POLICY"
    	        eval cdm_action $DIR "INPUT" $L $CDM_POLICY 
		continue
	fi
	if [ $COPYNOTLINK = 1 ]; then
		cp "$WFDIR/shared/$L" "$DIR/$L" 2>&1 >& $INFO
		checkError 254 "Failed to copy input file $L"
		log "Copied input: $WFDIR/shared/$L to $DIR/$L"
	else
		[ -f $WFDIR/shared/$L ]
		checkError 254 "Could not locate input file: $L" 
		ln -s "$WFDIR/shared/$L" "$DIR/$L" 2>&1 >& $INFO
		checkError 254 "Failed to link input file $L"
		log "Linked input: $WFDIR/shared/$L to $DIR/$L"
	fi
done

logstate "LINK_CDM_OUTPUTS"
SKIPPED_OUTPUT=()
GATHER_OUTPUT=()
for L in $OUTF ; do
	CDM_POLICY=$( cdm_lookup $L $CDM_FILE )
	if [[ $CDM_POLICY != "DEFAULT" && 
              $CDM_POLICY != "BROADCAST"* ]]; then
		log "CDM_POLICY: $L -> $CDM_POLICY" 
    	        eval cdm_action $DIR "OUTPUT" $L $CDM_POLICY
		SKIPPED_OUTPUT=( $SKIPPED_OUTPUT $L )
	fi
	if [ $CDM_POLICY == "GATHER" ]; then 
		GATHER_OUTPUT=( $GATHER_OUTPUT $L )
	elif [ $CDM_POLICY == "LOCAL" ]; then 
		CDM_LOCAL_OUTPUT=( $CDM_LOCAL_OUTPUT $L )
	fi
done

logstate "EXECUTE"
cd $DIR


if [ ! -f "$EXEC" ]; then
	fail 254 "The executable $EXEC does not exist"
fi
if [ ! -x "$EXEC" ]; then
	fail 254 "The executable $EXEC does not have the executable bit set"
fi
if [ "$KICKSTART" == "" ]; then
	if [ "$STDIN" == "" ]; then
		if [ "$SWIFT_GEN_SCRIPTS" != "" ]; then
			echo "#!/bin/bash" > run.sh
			echo "\"$EXEC\" \"${CMDARGS[@]}\" 1>\"$STDOUT\" 2>\"$STDERR\"" >> run.sh
			chmod +x run.sh
		fi
		"$EXEC" "${CMDARGS[@]}" 1>"$STDOUT" 2>"$STDERR"
	else
		if [ "$SWIFT_GEN_SCRIPTS" != "" ]; then
			echo "#!/bin/bash" > run.sh
			echo "\"$EXEC\" \"${CMDARGS[@]}\" 1>\"$STDOUT\" 2>\"$STDERR\" <\"$STDIN\"" >> run.sh
			chmod +x run.sh
		fi
		"$EXEC" "${CMDARGS[@]}" 1>"$STDOUT" 2>"$STDERR" <"$STDIN"
	fi
	checkError $? "Exit code $?"
else
	if [ ! -f "$KICKSTART" ]; then
		log "Kickstart executable ($KICKSTART) not found"
		fail 254 "The Kickstart executable ($KICKSTART) was not found"		
	elif [ ! -x "$KICKSTART" ]; then
		log "Kickstart executable ($KICKSTART) is not executable"
		fail 254 "The Kickstart executable ($KICKSTART) does not have the executable bit set"
	else
		mkdir -p $WFDIR/kickstart/$JOBDIR
		log "Using Kickstart ($KICKSTART)"
		if [ "$STDIN" == "" ]; then
			"$KICKSTART" -H -o "$STDOUT" -e "$STDERR" "$EXEC" "$@" 1>kickstart.xml 2>"$STDERR"
		else
			"$KICKSTART" -H -o "$STDOUT" -i "$STDIN" -e "$STDERR" "$EXEC" "$@" 1>kickstart.xml 2>"$STDERR"
		fi
		export APPEXIT=$?
		mv -f kickstart.xml "$WFDIR/kickstart/$JOBDIR/$ID-kickstart.xml" 2>&1 >& "$INFO"
		checkError 254 "Failed to copy Kickstart record to shared directory"
		if [ "$APPEXIT" != "0" ]; then
			fail $APPEXIT "Exit code $APPEXIT"
		fi
	fi
fi

cd $WFDIR

log "Moving back to workflow directory $WFDIR"
logstate "EXECUTE_DONE"
log "Job ran successfully"

MISSING=
for O in $OUTF ; do
	if [ ! -f "$DIR/$O" ]; then
		if [ "$MISSING" == "" ]; then
			MISSING=$O
		else
			MISSING="$MISSING, $O"
		fi
	fi
done
if [ "$MISSING" != "" ]; then
	fail 254 "The following output files were not created by the application: $MISSING"
fi

logstate "MOVING_OUTPUTS $OUTF"
for O in $OUTF ; do
	if ! contains SKIPPED_OUTPUT $O ; then
		mv "$DIR/$O" "$WFDIR/shared/$O" 2>&1 >&	"$INFO"
		checkError 254 "Failed to move output file $O to shared directory"
	fi
done

cdm_local_output $CDM_LOCAL_OUTPUT
cdm_gather $GATHER_OUTPUT

logstate "RM_JOBDIR"
rm -rf "$DIR" 2>&1 >& "$INFO"
checkError 254 "Failed to remove job directory $DIR" 

if [ "$STATUSMODE" = "files" ]; then
	logstate "TOUCH_SUCCESS"
	touch $WFDIR/status/${JOBDIR}/${ID}-success
fi

logstate "END"

closeinfo

if [ "X$PROGRESSIVE_INFO" == "X" ] && [ "X$SCRATCH" != "X" ]; then
        mkdir -p "$WFDIR/info/$JOBDIR"
	mv "$INFODIR/${ID}-info" "$WFDIR/info/$JOBDIR/${ID}-info"
fi

# ensure we exit with a 0 after a successful execution
exit 0

# Local Variables: 
# mode: sh
# sh-basic-offset: 8
# End:
