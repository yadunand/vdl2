
STRUCTURE: 

* This stuff was originally supposed to be controlled by 
  bin/swift-plot-log
* There is main makefile named "makefile" 
* "makefile" reads extra rules from a bunch of other makefiles 
  "*.mk" 
* These makefile rules use various shell scripts

NOTES: 

* Insert "set -x" into shell scripts that look interesting
* 

USE CASES: 

* Plot coasters utilization

PLOT_HOME=/scratch/wozniak/cog/modules/swift/libexec/log

make -f ${PLOT_HOME}/coasters.mk LOG=./map-20100606-2328-nv9xntzg.log SWIFT_PLOT_HOME=${PLOT_HOME} coaster-block-timeline.png

