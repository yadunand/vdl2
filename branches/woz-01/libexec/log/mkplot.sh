#!/bin/bash

# MKPLOT
# New log plot entry point

# Usage: 
# mklog.sh -h 
#               print help 
# mklog.sh -l 
#               list pngs we know work
# mklog.sh <log> <png> 
#               generate a png from a log

message_help()
{
  echo "See the top of this file for usage"
  exit 0
}

message_list()
{
  echo "Known PNG output:"
  for PNG in ${KNOWN_PNGS[@]}
   do
   printf "\t"
   echo ${PNG}
  done
  exit 0
}

# List of known PNGs: what we know will work
KNOWN_PNGS=()
KNOWN_PNGS=( ${KNOWN_PNGS[@]} coaster-block-timeline.png )

export SWIFT_PLOT_HOME=$( dirname $0)/../../libexec/log

while getopts "hl" OPTION
 do
 case ${OPTION}
   in
   h) message_help ;;
   l) message_list ;;
 esac
done

# Input parameters
# The log file to plot
LOG=$1
# The plot to generate
PNG=$2 

if [[ ${LOG} == "" ]] 
  then
  echo "No log file given!"
  exit 1
fi

if [[ ${PNG} == "" ]] 
  then
  echo "No png target given!"
  exit 1
fi

MAKEFILE=${SWIFT_PLOT_HOME}/makefile
make -f ${MAKEFILE} ${PNG}
