coaster-block-timeline.png: coaster-blocks.data
	gnuplot $(SWIFT_PLOT_HOME)/coaster-block-timeline.plot

coaster-blocks.data: coasters.tmp
	extract-coaster-blocks.sh < coasters.tmp > coaster-blocks.data

coasters.tmp: $(LOG)
	extract-coaster-timeline $(LOG)

# block-count-table.tmp: extract-timeline

extract-coaster-qwait: coasters.tmp
	extract-coaster-qwait $(LOG)

coaster-qwait-count.data: extract-coaster-qwait
coaster-qwait-size.data: extract-coaster-qwait
coaster-qwait-wtime.data: extract-coaster-qwait
coaster-block-utilization-vs-count.data: extract-block-utilization-vs-count

extract-block-utilization: coasters.tmp
	extract-coaster-block-utilization $(LOG)

extract-block-utilization-vs-count: coasters.tmp
	extract-block-utilization-vs-count $(LOG)

coaster-block-utilization.data:
	extract-coaster-block-utilization

coaster-qwait-count.png: coaster-qwait-count.data
	gnuplot $(SWIFT_PLOT_HOME)/coaster-qwait-count.plot

coaster-qwait-size.png: coaster-qwait-size.data
	gnuplot $(SWIFT_PLOT_HOME)/coaster-qwait-size.plot

coaster-qwait-wtime.png: coaster-qwait-wtime.data
	gnuplot $(SWIFT_PLOT_HOME)/coaster-qwait-wtime.plot

coaster-block-utilization.png: coaster-block-utilization.data
	gnuplot $(SWIFT_PLOT_HOME)/coaster-block-utilization.plot

coaster-block-utilization-vs-count.png: coaster-block-utilization-vs-count.data
	gnuplot $(SWIFT_PLOT_HOME)/coaster-block-utilization-vs-count.plot
