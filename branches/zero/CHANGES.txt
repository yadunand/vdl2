(12/23/06)
------------------------- 0 rc 2 ---------------------------

*** svn copy is broken so I'm noting the revision here: 115

(12/14/06)

*** Fixed the default mapper to be concurrent_mapper

*** Added passing of tc.data profiles to the job (e.g
    user-defined environment variables, jobtype, count, etc.)

------------------------- 0 rc 1 ---------------------------
(12/07/06)

*** Added dynamic loading of the TC file

*** Added cleanup which is done at the end of the
    whole run

(12/06/06)

*** Moved to CI SVN

*** Added tests directory

(11/22/06)

*** Changed to a pure Karajan adaptive scheduler instead
    of a Pegasus site selector
	
*** Added "client-side-caching" of remote files

*** Added a basic wrapper to capture exit codes and do other
    work before and after executing the job
