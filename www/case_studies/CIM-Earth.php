<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - Case Studies</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">				
	<h1>Case Studies</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">
	<h3>Energy/Climate/Economics Modelling</h3>
	<p>The CIM-EARTH project develops a large-scale integrated modeling framework for decision makers in climate and energy policy.  (Foster, Elliott)</p>
	<img src="images/CIM-Earth.jpg" style="padding:0px 0px 15px 20px;" />
	<p><b>Approach.</b> Beagle is being used to study land use, land cover, and the impacts of climate change on agriculture and the global food supply.  Using a DSSAT 4.0 (“decision support system for agrotechnology transfer”) crop systems model ported from Windows, a parallel simulation framework was implemented using Swift. Benchmarks of this framework have been performed on a prototype simulation campaign, measuring yield and climate impact for a single crop (maize) across the conterminous USA with daily weather data and climate model output spanning 120 years (1981-2100) and 16 different configurations of local management (fertilizer and irrigation) and cultivar choice.</p>
	<p><b>Results.</b> Preliminary results of parallel DSSAT on Beagle have been presented in an NSF/advisory board meeting of the CIM-EARTH project. At right, top 2 maps: Preliminary results of parallel DSSAT: maize yields across the USA with intensive nitrogen application and full irrigation; bottom 2 maps show results with no irrigation. Each model run is ~120,000 DSSAT invocations.</p>
	<a href="index.php" class="trailingLink">view more case studies</a>
	<p>&nbsp;</p>
	</div>
  <!-- end .grid_9 -->
  <div class="grid_3">
  <?php require('../inc/case_study_sidebar.php') ?>
    <p>&nbsp;</p>
  </div>
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
				
  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
