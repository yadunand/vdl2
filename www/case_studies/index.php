<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - Case Studies</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">				
	<h1>Case Studies</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_12">
	<h3>Energy/Climate/Economics Modelling</h3>
	<!--<a href="CIM-Earth.php"><img src="" height="100" width="100" alt="Energy/Climate/Economics Modelling" border="0" align="left" style="padding-right:20px" /></a>-->
	<p>The CIM-EARTH project develops a large-scale integrated modeling framework for decision makers in climate and energy policy.  (Foster, Elliott)</p>
	<a href="../case_studies/CIM-Earth.php" class="trailingLink">learn more</a>
	<p>&nbsp;</p>
	 <div class="clear"></div>
	 
	<h3>Protein Structure Prediction</h3>
	<p>The laboratories of Karl Freed and Tobin Sosnick use Beagle to develop and validate methods to predict protein structure using homology-free approaches.</p>
	<a href="../case_studies/Protein-Structure.php" class="trailingLink">learn more</a>
	<p>&nbsp;</p>
	 <div class="clear"></div>
	
	<h3>Protein-RNA Interaction Modeling</h3>
	<p>M. Parisien (with T. Sosnick, T. Pan, and K. Freed) used Beagle to develop a first-generation algorithm for the prediction of the RNA-protein interactome.</p>
	<a href="Protein-RNA.php" class="trailingLink">learn more</a>
	<p>&nbsp;</p>
	 <div class="clear"></div>
	
	<h3>Glass Structure Modeling</h3>
	<p>This project models of aspects of glass structure at a theoretical chemistry level. (Hocky/Reichman)</p>
	<a href="Glass-Structure.php" class="trailingLink">learn more</a>
	<p>&nbsp;</p>
	 <div class="clear"></div>
	
	<h3>Modeling Climate Impact on Hydrology</h3>
	<p>Projecting biofuel production impact on hydrology (E. Yan)</p>
	<a href="Hydrology.php" class="trailingLink">learn more</a>
	<p>&nbsp;</p>
	 <div class="clear"></div>
	
	<h3>Computational Neuroscience Application Research Infrastructure</h3>
	<p>The Computational Neuroscience Applications Research Infrastructure
(CNARI) uses Swift for maintaining, serving, and
analyzing massive amounts of fMRI data.</p>
	<a href="CNARI.php" class="trailingLink">learn more</a>
	<p>&nbsp;</p>
	 <div class="clear"></div>
	
	
	
	</div>
  <!-- end .grid_9 -->
  <!--<div class="grid_3">
  	<h3>Right Header</h3>
    <p>Right column content.</p>
    <p>&nbsp;</p>
  </div>-->
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
				
  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
