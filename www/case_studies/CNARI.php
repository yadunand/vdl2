<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - Case Studies</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">				
	<h1>Case Studies</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">
	<h3>Computational Neuroscience Application Research Infrastructure</h3>
	<p>
Functional magnetic resonance imaging (fMRI) has led to an enormous growth in the study of cognitive
neuroanatomy. Combined with advances in high-field electrophysiology, fMRI has led to
a fast-growing field of human neuroscience.
<br><br>
Researchers led by Dr. Steven Small of UCalifornia Irvine developed a new computational framework that
facilitates fMRI experimentation and analysis, and which has led to some rethinking of the nature of
experimental design and analysis.
</p>
	<img src="images/CNARI-fig2.png" style="padding:0px 0px 15px 20px;" />
        <p>
        <i>Graphical depiction of permutations of original data. Brain activity data were collected from 24 participants that were exposed to two experimental conditions. A single
permutation consisted of (a) switching the condition labels of the experimental conditions, for 1 or more participants, (b) re-calculating the group-level effects, and (c) spatially
clustering the results to identify what is the largest cluster found for the current permutation. This procedure results in a sampling distribution for the largest cluster in each
permutation, which is then used to threshold the original data.</i></p>
<br>
	<p><b>Approach.</b>
The Computational Neuroscience Applications Research Infrastructure (CNARI) incorporates novel methods for maintaining, serving, and analyzing massive amounts of fMRI data. CNARI uses Swift to describe, execute, and share parallel workflows, and relational database technology to provide new methods to organize and analyze experimental results.
<br><br>
Swift runs CNARI workflows on a wide variety of resources, including TeraGrid/XSEDE systems and institutional computing clusters. By using CNARI, it is possible to perform naturalistic, network-based, statistically valid experiments in systems neuroscience on a very large scale, with ease of data manipulation and analysis, within reasonable
computational time scales.
<!-- 	
<p><b>Results.</b> 
Results of this research demonstrate that plausible changes in temperature and precipitation caused by increases in atmospheric greenhouse gas concentrations could have major impacts on both
</p>
-->

	<p>&nbsp;</p>
	</div>
  <!-- end .grid_9 -->
  <div class="grid_3">
  <?php require('../inc/case_study_sidebar.php') ?>
    <p>&nbsp;</p>
  </div>
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
				
  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
