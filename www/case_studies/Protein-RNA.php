<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - Case Studies</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">				
	<h1>Case Studies</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">
	<h3>Protein-RNA Interaction Modeling</h3>
	<p>
M. Parisien (with T. Sosnick, T. Pan, and K. Freed) used Beagle to develop a first-generation algorithm for the prediction of the RNA-protein interactome.
</p>
	<img src="images/rna.png" style="padding:0px 0px 15px 20px;" />
	<p><b>Approach.</b>
Non-coding RNAs often function in cells through specific interactions with their protein partners. Experiments alone cannot provide a complete picture of the RNA-protein interactome. To complement experimental methods, computational approaches are highly desirable. No existing method, however, can provide genome-wide predictions of docked RNA-protein complexes.
the application of computational predictions, together with experimental methods, will provide a more complete understanding on cellular networks and function of RNPs. The approach makes use of a rigid-body docking algorithm and a scoring function custom- tailored for protein-tRNA interactions. Using Swift, Beagle screened about 300 proteins per day on 80 nodes of 24 cores (11% of the total XE6’s power).	
	<p><b>Results.</b> 
The scoring function can identify the native docking conformation in large sets of decoys (100,000) for many known protein-tRNA complexes (4TRA shown here). (left) Scores for true positive complexes (●)(N=28) are compared to true negative ones of low (▼)(N=40) and high (▲) (N=40) isoelectric points. (right) Because the density curve of the true positives, which have pI < 7, has minimal overlap with the curve of the low pI true negatives (blue area), the scoring function has the specificity to identify tRNA-binding proteins.
</p>
	<p>&nbsp;</p>
	</div>
  <!-- end .grid_9 -->
  <div class="grid_3">
  <?php require('../inc/case_study_sidebar.php') ?>
    <p>&nbsp;</p>
  </div>
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
				
  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
