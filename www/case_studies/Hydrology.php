<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - Case Studies</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">				
	<h1>Case Studies</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">
	<h3>Modeling Climate/Crop Impacts on Hydrology</h3>
	<p>Modeling the hydrology of the Mississippi River Basin to assess the impacts of climate change, crop growth, and energy production driven by energy/economics factors.
</p>
	<img src="images/hydrology.jpg" style="padding:0px 0px 15px 20px;" />
	<p><b>Approach.</b> Swift is used to perform large-scale watershed simulations using SWAT, the Soil and Water Assessment Tool, to study the impacts of global climate change, large-scale biomass feedstock production, and potential water demands on the regional hydrologic system in the Upper Mississippi River Basin. These studies use Swift to perform calibration, sensitivity studies, and optimization using SCE (shuffled complex evolution) optimization methods. All of these computational modeling procedures involve multiple runs of thousands of invocations of SWAT to simulate the temporal and spatial changes in evapotranspiration, soil moisture, runoff, groundwater recharge,  crop yields, as well as water quality in streams such as nitrogen, phosphorous, and eroded sediments in regional hydrologic system.</p>
	
	<p><b>Results.</b> Results of this research demonstrate dynamic interrelationships among water quantity and quality, biofuel production, crop growth/management, and climate change. The modeling tool developed in this study can be used to address future sustainability of the regional/local hydrologic systems.
</p>
	<p>&nbsp;</p>
	</div>
  <!-- end .grid_9 -->
  <div class="grid_3">
  <?php require('../inc/case_study_sidebar.php') ?>
    <p>&nbsp;</p>
  </div>
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
				
  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
