<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - Case Studies</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">				
	<h1>Case Studies</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">
	<h3>Glass Structure Modeling</h3>
	<p>
This project models of aspects of glass structure at a theoretical chemistry level, by Glen Hocky and 
<a href="http://www.columbia.edu/cu/chemistry/groups/reichman/">David Reichmann, Columbia University Department of Chemistry.</a>
</p>
	<!-- <img src="images/glass2.jpg" style="padding:0px 0px 15px 20px;" /> -->
	<img src="images/glass1.png" height="30%" style="padding:0px 0px 15px 20px;" />
						     

	<p><b>Approach.</b>
Recent studies of the glass transition in model systems have focused on calculating from theory or simulation what is known as the “mosaic length”. This project  evaluated a new “cavity method” for measuring this length scale.  Correlation functions are calculated at the interior of cavities of varying sizes and averaged over many independent simulations to determine a thermodynamic length. Using Swift on the UChicago Beagle supercomputer and the Open Science Grid, Hocky investigated whether this thermodynamic length causes variations among seemingly identical systems. Close to one million Beagle CPU hours were used, and many hundreds of thousand of hours of computing were performed on the Open Science Grid.
	<p><b>Results.</b> 

Three simple models of glassy behavior were studied. All appeared the same, but only two of the models have particles relaxing at the same rate for the same temperature. This would imply that the glass structure does not dictate the dynamics. A new computational technique was used to extract a length scale on which the liquid is ordered in an otherwise undetectable way.  Results showed that using this length we can distinguish the two systems which have the same dynamics as separate from the third which has faster dynamics than the other two. 

A manuscript describing the study and findings is in preparation.
</p>
	<p>&nbsp;</p>
	</div>
  <!-- end .grid_9 -->
  <div class="grid_3">
  <?php require('../inc/case_study_sidebar.php') ?>
    <p>&nbsp;</p>
  </div>
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
				
  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
