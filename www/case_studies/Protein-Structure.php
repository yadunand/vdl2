<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - Case Studies</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">				
	<h1>Case Studies</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">
	<h3>Protein Structure Prediction</h3>
	<p>The laboratories of Karl Freed and Tobin Sosnick use Swift to develop and validate methods to predict protein structure using homology-free approaches.</p>
	<img src="images/protein2.jpg" style="padding:0px 0px 15px 20px;" />
	<p><b>Approach.</b>A. Adhikari (under K. Freed and T. Sosnick) has developed new structure prediction
	techniques based on Monte Carlo simulated annealing  which employ novel, compact molecular 
        representations and innovative “moves” of the protein backbone to achieve accurate prediction 
        with far less computation then previous methods. One of the applications of the method involves 
        rebuilding local regions in protein structures, called “loop modeling”,  a problem which the group
        tackled with considerable success in the CASP protein folding tournament(shown in right). They are 
        now testing further algorithmic innovations using the computational power of Beagle.
	
	<p><b>Results.</b> 
        The group is now developing a new iterative algorithm for predicting protein structure and folding 
        pathway starting only from the amino acid sequence. In progress, no publications yet from Beagle studies.
</p>
	<p>&nbsp;</p>
	</div>
  <!-- end .grid_9 -->
  <div class="grid_3">
  <?php require('../inc/case_study_sidebar.php') ?>
    <p>&nbsp;</p>
  </div>
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
				
  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
