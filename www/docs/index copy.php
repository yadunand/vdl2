<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - A system for rapid and reliable specification, execution, and management of large scale workflows</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">				
	<h1>Documentation</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">
	<h3>Swift Quickstart Guide</h3>

<ul>
	<li>Swift Quickstart Guide [<a href="../guides/quickstartguide.php">html</a>]</li>
</ul>

<p>
	Use the Quickstart Guide to help you install and configure swift and run a
	simple 'Hello World' example.
</p>

<h3>Swift Tutorials</h3>

<ul>
	<li><strong>latest (0.92)</strong>
        [<a href="../guides/tutorial.php">html</a>] [<a href="../guides/tutorial.pdf">pdf</a>] 
        </li>
	<li><strong>trunk</strong>
        [<a href="../guides/trunk/tutorial/tutorial.html">html</a>] [<a href="../guides/trunk/tutorial/tutorial.pdf">pdf</a>]
        </li>
</ul>


<ul>
	<li><strong>Swift Tutorial (on your own machine)</strong> [<a href="../guides/tutorial.php">html</a>] [<a href="../guides/tutorial.pdf" onClick="javascript: pageTracker._trackPageview('/swift/guides/tutorial.php');">pdf</a>]</li>
</ul>

<p>
	This Swift tutorial carries on where the Quickstart Guide leaves off,
	introducing you to Swift environment and the SwiftScript language in more
	depth.
</p>

<ul>
	<li><strong>Swift Tutorial (on a UC machine)</strong> [<a href="http://www.ci.uchicago.edu/osgedu/schools/swiftlab/">html</a>] </li>
</ul>

<p>
	This tutorial (part of the Open Science Grid teaching lab) introduces
	you to the Swift environment and the SwiftScript language in more
	depth using a provided training account where installation of the
	software has already been performed.
</p>


<h3>Swift User Guide</h3>
<ul>
<p>
	The User Guide provides more detailed reference documentation and background
	information on swift. It is assumed that the reader is already familiar with
	the material in the Quickstart and Tutorial documents.
</p>
	<li><strong>trunk</strong>
          [<a href="../guides/trunk/userguide/userguide.html">html</a>] [<a href="../guides/trunk/userguide/userguide.pdf">pdf</a>]
        </li>
	<li><strong>latest (0.92)</strong>
          [<a href="../guides/release-0.92/userguide/">multi-page html</a>]
        </li>
	<li><strong>historical (0.91)</strong>
          [<a href="../guides/release-0.91/userguide/">multi-page html</a>]
        </li>
</ul>


<h3>Log Processing tools</h3>
<ul>
	<li>Swift log processing tools [<a href="../guides/log-processing.php">html</a>] [<a href="../guides/log-processing.pdf" onClick="javascript: pageTracker._trackPageview('/swift/guides/log-processing.pdf');">pdf</a>]</li>
</ul>

<p>
	The Swift log processing tools can be used to analyse the log files of
	Swift runs - as a webpage, and in several processed text formats. This
	document gives some usage information for those tools.
</p>

<h3>Elsewhere</h3>

<p>
We've taught a hands on Swift tutorial on a number of occasions. This is
harder to do alone because it relies on a number of brain image processing
applications which you probably don't have installed.
[<a href="http://www.ci.uchicago.edu/osgedu/modules/#workflow_swift">html</a>]
</p>

<h3>Historical - SwiftScript Language</h3>

<p>

	The SwiftScript language reference was a specification for an
	earlier version of the language, and is probably of little interest to the majority of users.

	<ul>
    	<li> SwiftScript Language Reference - latest work in progress [<a href="../guides/historical/languagespec.php">html</a>] [<a href="../guides/languagespec.pdf" onClick="javascript: pageTracker._trackPageview('/swift/guides/historical/languagespec.pdf');">pdf</a>]</li>
	    <li> SwiftScript Language Reference v 0.6 [<a href="../guides/languagespec-0.6.php">html</a>] [<a href="../guides/historical/languagespec.pdf" onClick="javascript: pageTracker._trackPageview('/swift/guides/languagespec-0.6.pdf');">pdf</a>]</li>
	</ul>
</p>

	<p>&nbsp;</p>
	</div>
  <!-- end .grid_9 -->
  <div class="grid_3">
  	<h3>Right Header</h3>
    <p>Right column content.</p>
    <p>&nbsp;</p>
  </div>
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
				
  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
