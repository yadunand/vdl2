<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1" />
<title></title>
</head>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<link href="../css/orbit-1.2.3.css" rel="stylesheet" type="text/css">
<?php require('../inc/header2.php') ?>
<br><br>
<body>

<style>
#usagelist { list-style: circle inside none; }
</style>
<div style="width:700px; margin-left:350px;">

<h1>Usage Statistics Collection by Swift</h1>
<br>
Swift sends usage statistics back to the Swift development team to 
measure worldwide usage and and improve reliability and usability.
<br><br>
<ul id="usagelist">
  <li><a href="#whyarewedoingthis">Why are we doing this?</a></li>
  <li><a href="#optout">Opt-out</a></li>
  <li><a href="#whatissent">What is sent?</a></li>
  <li><a href="#howisthedatasent">How is the data sent?</a></li>
  <li><a href="#whenisthedatasent">When is it sent?</a></li>
  <li><a href="#whatwillitbeusedfor">What will the data be used for?</a></li>
  <li><a href="#feedback">Feedback</a>    </li>
</ul>

<h2><a name="whyarewedoingthis"></a>Why are we doing this?</h2>  
The Swift development team receives support from government funding agencies. In
  a time of funding scarcity, these agencies must be able to demonstrate that
  the scientific community is benefiting from their investment. To this end,
  we want to provide generic usage data about such things as the following: 
<br><br>
<UL id="usagelist">
  <LI>How many people use Swift   
  <LI>Average job length 
  <LI>Swift exit codes</LI>
</UL>
To this end, we have added support to Swift that will allow installations
to send us generic usage statistics. By participating in this project, you help
our funders to justify continuing their support for the software on which you
rely. The data sent is as generic as possible (see <a href=#whatissent>What is Sent?</a> below).
<br><br>
<h2><a name="optout"></a>Opt-out</h2> 
We are using opt-out rather than opt-in. The reason is that we <it>need</it> this
data - it is a requirement for funding. We're
sure our fellow users would be willing to help show that Swift works
and is in use. Realistically, however, we know that if it requires any additional
effort to set up usage statistic reporting, it would drastically reduce the number
of users that would actually report the data. To be effective, we need to require
zero additional effort.<br><br>
By not opting out, and allowing these statistics to be reported back, you
are explicitly supporting the further development of Swift.
<br><br>
If you must opt out of usage reporting, set "SWIFT_USAGE_STATS=0" in your environment.   
<br><br>
<h2><a name="whatissent" id="whatissent"></a>What is sent?</h2> 
<ul id="usagelist">
    <li>Anonymized user ID</li>
    <li>Anonymized Swift script ID</li>
    <li>Swift script length</li>
    <li>Swift exit code</li>
    <li>IP address </li>
    <li>Hostname</li>
</ul>

<h2><a name="howisthedatasent"></a>How is the data sent?</h2>The data is sent via UDP. While this may cause us to lose some data, it drastically reduces the possibility that the usage statistics reporting can adversely affect the operation of the software.
<br><br>
<h2><a name="whenisthedatasent"></a>When is the data sent?</h2>Once when Swift starts a script, and once when the script is completed. 

<br><br>
<h2><a name="whatwillitbeusedfor"></a>What will the data be used for?</h2> 

The data will be used for answering questions such as:<br><br>
<ul id="usagelist">
  <li>How many unique users are using Swift?</li>
  <li>To determine patterns of usage - is activity increasing or decreasing?</li>
</ul>
We will also try and mine the data to answer operational questions such as:<br><br>
<ul id="usagelist">
  <li> What percentage
            of the jobs run complete successfully? </li>

  <li>Of the ones that fail, what is the most common fault code returned?</li>
</ul>
<h2><a name="feedback"></a>Feedback</h2>
Please send us your feedback at <a href="mailto:swift-devel@ci.uchicago.edu">swift-devel@ci.uchicago.edu</a>.
Feedback from our user communities will be useful in determining our path forward
with this in the future. We do ask that if you have concerns or objections, please
be specific in your feedback. For example: "Our site has a policy against sending
such data" is good information for us to know in the future.  A link to such
a policy would be even better.
<br><br>
<!--end div of content-->
</div>
</body>
</html>
