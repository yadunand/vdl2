<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - Documentation</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<!-- Google analytics -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25069254-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">
	<h1>Documentation</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">
<br>
<h3>Swift Quickstart</h3>
	This guide describes the steps needed to download, install, configure, and run the basic examples for Swift.
<br><br>
<ul>
<li type="circle" style="margin-left:50px;">
      <a href="../guides/release-0.96/quickstart/quickstart.html">Quickstart Guide</a>
</li>
</ul>
<p>&nbsp;</p>

<h3>Swift Tutorials</h3>
      <a href="../swift-tutorial/doc/tutorial.html">This hands-on tutorial</a></li> gives a quick (~20 minute) taste of Swift. It shows you how to run Swift workflows on
computation resources such as :
      <br><br>
         <ul>
      <li type="circle" style="margin-left: 50px;">Ad-hoc nodes</li>
      <li type="circle" style="margin-left: 50px;">Beagle (UChicago)</li>
      <li type="circle" style="margin-left: 50px;">Blues (LCRC)</li>
      <li type="circle" style="margin-left: 50px;">Edison (NERSC)</li>
      <li type="circle" style="margin-left: 50px;">Elastic Cloud Compute (Amazon)</li>
      <li type="circle" style="margin-left: 50px;">Midway (UChicago)</li>
      <li type="circle" style="margin-left: 50px;">Open Science Grid</li>
      <li type="circle" style="margin-left: 50px;">Stampede (XSEDE/TACC)</li>
      <li type="circle" style="margin-left: 50px;">Swan (Cray MPN)</li>
         </ul>

      For a complete list go <a href="http://swift-lang.org/swift-tutorial/doc/tutorial.html#_strong_tutorial_section_two_strong">here</a></li>.

<p>&nbsp;</p>
<h3>Swift User Guide</h3>
        The User Guide provides more detailed reference documentation and background
        information on swift. It is assumed that the reader is already familiar with
        the material in the Quickstart and Tutorial documents.
<br><br>
<ul>
        <li type="circle" style="margin-left:50px;">Latest (0.96)
        [<a href="../guides/release-0.96/userguide/userguide.html">html</a>] [<a href="../guides/release-0.96/userguide/userguide.pdf">pdf</a>]
        </li>
        <li type="circle" style="margin-left:50px;">Interim (0.95)
        [<a href="../guides/release-0.94/userguide/userguide.html">html</a>] [<a href="../guides/release-0.94/userguide/userguide.pdf">pdf</a>]
        </li>
        <li type="circle" style="margin-left:50px;">Trunk
          [<a href="../guides/trunk/userguide/userguide.html">html</a>] [<a href="../guides/trunk/userguide/userguide.pdf">pdf</a>]
        </li>
</ul>
<p>&nbsp;</p>


<h3>Swift Siteguide</h3>
        This document will guide new users to run Swift in a variety of different environments.
<br><br>
<ul>
       <li type="circle" style="margin-left:50px;">Latest (0.96) Under construction</li>
        <li type="circle" style="margin-left:50px;">Interim (0.95) [<a href="../guides/release-0.95/siteguide/siteguide.html">html</a>] [<a href="../guides/release-0.95/siteguide/siteguide.pdf">pdf</a>]</li>
        <li type="circle" style="margin-left:50px;">Trunk [<a href="../guides/trunk/siteguide/siteguide.html">html</a>] [<a href="../guides/trunk/siteguide/siteguide.pdf">pdf</a>]</li>
</ul>
<p>&nbsp;</p>




The following are older tutorials for Swift 0.95/0.94 :
<br><br>
<ul>
<li type="circle" style="margin-left: 50px;"><a href="../tutorials/localhost/tutorial.html">Swift on your local machine</a></li>
<li type="circle" style="margin-left: 50px;"><a href="../tutorials/cloud/tutorial.html">Swift on Clouds and Ad Hoc collections of workstations</a></li>
<li type="circle" style="margin-left: 50px;"><a href="../tutorials/osgconnect/tutorial.html">Swift on OSG Connect</a></li>
<li type="circle" style="margin-left: 50px;"><a href="../tutorials/cray/tutorial.html">Swift on Crays</a></li>
<li type="circle" style="margin-left: 50px;"><a href="../tutorials/midway/tutorial.html">Swift on RCC Midway Cluster at UChicago / Slurm</a></li>
</ul>
These take about 20 minutes. Try the one(s) for the computing resources you have access to.<br>
Our older tutorial -- worth a quick read -- can be found <a href="../tutorials/old/tutorial.html">here</a>
<p>&nbsp;</p>

<!--
<h3>Search All Documentation</h3>
<div id="cse" style="width: 100%;">Loading</div>
<script src="//www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript"> 
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('007067124251691692991:pbx7ijbjeea');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    customSearchControl.draw('cse');
  }, true);
</script>
-->

<!-- 
<h3>Swift Cookbook</h3>
        
	This cookbook is a collection of recipes involving setting up and running Swift. It is compiled by various members of the Swift community.
<br><br>
<ul>
        <li type="circle" style="margin-left:50px;">Latest (0.93) [<a href="../guides/release-0.93/cookbook/cookbook.html">html</a>] [<a href="../guides/release-0.93/cookbook/cookbook.pdf">pdf</a>]</li>
</ul>
-->

	<p>&nbsp;</p>
	</div>
  <!-- end .grid_9 -->
  <div class="grid_3">
  	<?php require('../inc/docs_sidebar.php') ?>
  </div>
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
				
  </div>
<!-- end .container_12 -->

<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>

</body>
</html>
