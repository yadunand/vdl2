<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<title>Swift</title>
		<link href="../css/style2.css" rel="stylesheet" type="text/css" />
	</head>
	<!-- Change body id name to title of new content. There is an body id style in the css where you can add the new name -->
	<body id="Name_of_page">
		<!-- entire page container -->
		<div id="container">
			<!-- header -->
			<div id="header">
				<?php require('../inc/header.php') ?>
			</div>
			<!-- end header -->
			<!-- nav -->
			<div id="nav">
				<?php require('../inc/nav.php') ?>
			</div>
			<!-- end nav -->
			<!-- content container -->
			<div id="content">
				<!-- left side content -->
				<div id="left">
				
<h1>PAGENAME</h1>
<h2>H2</h2>
<h3>H3</h3>
<p>
	content goes here
</p>
<ul>
	list:
	<li>bullet point</li>
</ul>
<p>
	<span class="highlight"> something stands out</span>
</p>

				</div>
				<!-- end left side content -->
				<!-- right side content -->
				<div id="right">
					<?php require('../inc/side_content.php') ?>
				</div>
				<!-- end right side content -->
			</div>
			<!-- end content container-->
			<!-- footer -->
			<div id="footer"><?php require('../inc/footer.php') ?></div> 
			<!-- end footer -->

		</div>
		<!-- end entire page container -->
	</body>
</html>
