<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Swift</title>
<link href="../css/style2.css" rel="stylesheet" type="text/css" />
</head>

<body id="apps">
<!-- entire page container -->
<div id="container">
<!-- header -->
<div id="header">
<?php require('../inc/header.php') ?>
</div>
<!-- end header -->
<!-- nav -->
<div id="nav">
  <?php require('../inc/nav.php') ?>
</div>
<!-- end nav -->
<!-- content container -->
<div id="content">
<!-- left side content -->
<div id="left">
<h1>APPLICATIONS</h1>
<p>A big ole' list that uses Swift.</p> 

</div>
<!-- end left side content -->
<!-- right side content -->
<div id="right">
<?php require('../inc/apps_sidebar.php') ?>
</div>
<!-- end right side content -->
</div>
<!-- end content container-->
<!-- footer -->
 <div id="footer"><?php require('../inc/footer.php') ?></div> 
 <!-- end footer -->

</div>
<!-- end entire page container -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
