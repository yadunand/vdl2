<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="robots" content="noindex">
<title>Swift - Support</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">
<!--	<h1>Support</h1> -->
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">

<!--
    <h3>Contact the Swift team for more information or to report problems</h3>

    <p>For more info about Swift, send email to the Swift team at <a href="mailto:info@swift-lang.org?subject=Question regarding Swift parallel scripting">info@swift-lang.org</a></p>

    <p>To report problems with Swift, send us a support ticket by emailing <a href="mailto:support@swift-lang.org?subject=Problem with Swift parallel scripting language">support@swift-lang.org</a></p><br>
-->
	<h3>Join the Swift Community via our email lists</h3>
	<p>If you are using Swift or planning to try it, subscribe to the <a href="https://lists.ci.uchicago.edu/cgi-bin/mailman/listinfo/swift-user">swift-user</a> email list to hear what other users are doing, ask questions, and listen in on and join the community discussion.</p>
<p>If you'd like to contribute as a developer, or listen to the discussion of implementation issues, please join the <a href="https://lists.ci.uchicago.edu/cgi-bin/mailman/listinfo/swift-devel">swift-devel</a> email list.</p>
<p><b>Note that you need to subscribe to these lists in order to send messages to them.</b> Other messages will be moderated and may be significantly delayed.</p>
	<table class="mailinglists">
		<tr>
			<th width="20%" valign="top">List</th>
			<th width="10%" valign="top" nowrap="nowrap">List Info<br/>(subscribe here)</th>
			<th width="8%" valign="top">Archives</th>
			<th width="30%" valign="top">Purpose</th>

		</tr>
		<tr>
			<td valign="top">
				<a href="mailto:swift-user@ci.uchicago.edu">swift-user</a>
			</td>
			<td align="center" valign="top">
				<a href="https://lists.ci.uchicago.edu/cgi-bin/mailman/listinfo/swift-user">subscribe</a>
			</td>
			<td align="center" valign="top">
				<a href="http://lists.ci.uchicago.edu/pipermail/swift-user/">archives</a>
			</td>
			<td valign="top">
				Swift user community discussion and general questions. 
			</td>
		</tr>
		<tr>
			<td valign="top">
				<a href="mailto:swift-devel@ci.uchicago.edu">swift-devel</a>			
			</td>
			<td align="center" valign="top">
				<a href="https://lists.ci.uchicago.edu/cgi-bin/mailman/listinfo/swift-devel">subscribe</a>
			</td>
			<td align="center" valign="top">
				<a href="http://lists.ci.uchicago.edu/pipermail/swift-devel/">archives</a>
			</td>

			<td valign="top">
				The Swift developers mailing list.
			</td>
		</tr>
<!--
                <tr>
                	<td align="top">
				<a href="mailto:swift-support@ci.uchicago.edu">swift-support</a>
			</td>
                        <td></td>
			<td></td>
			<td valign="top">
				Contact the Swift support team directly with any questions

			</td> -->
		</tr>
               
	</table>

	<p>&nbsp;</p>


        <h3>Reporting Problems</h3>
        <p>The best way to report problems and receive assistance from the Swift team <b>and</b> community members is to <a href="https://lists.ci.uchicago.edu/cgi-bin/mailman/listinfo/swift-user">join the swift-user email list</a>.</p>
	<p>When reporting an issue to swift-support, please be very precise and detailed. Send us all relevant files and output, and be sure to include the Swift *.log files that were generated 
        as a part of your run. If the log files are too large to include in an email, please provide 
        access to the logs at a publicly readable URL or directory.</p>
        <br>

          <h3>Search Mailing List Archive</h3>
           <div id="cse" style="width: 100%;">Loading</div>
           <script src="//www.google.com/jsapi" type="text/javascript"></script>
           <script type="text/javascript"> 
              google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
              google.setOnLoadCallback(function() {
              var customSearchControl = new google.search.CustomSearchControl('007067124251691692991:zxjheipt4is');
              customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
              customSearchControl.draw('cse');
              }, true);
           </script>
</div> 
	</div>
  <!-- end .grid_9 -->
  <!-- 
    <div class="grid_3">
  	<h3>Bug Reports</h3>
    <p>We have a Bugzilla, at <a href="http://bugzilla.mcs.anl.gov/swift/">bugzilla.mcs.anl.gov/swift/</a>. </p>
    <p>&nbsp;</p>
  </div>
  -->
  <!-- end .grid_3 -->
  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
