<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>The Swift Parallel Scripting Language</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<link href="../css/orbit-1.2.3.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.orbit-1.2.3.min.js"></script>
<!--[if IE]>
			     <style type="text/css">
			         .timer { display: none !important; }
			         div.caption { background:transparent; filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000,endColorstr=#99000000);zoom: 1; }
			    </style>
			<![endif]-->
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<!-- Google analytics -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25069254-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<!-- Run the plugin -->
		<script type="text/javascript">
			$(window).load(function() {
				$('#featured').orbit({
					bullets: true,
					});
			});
		</script>
</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
  <div class="grid_12">
    <h1 class="tagline">
      <!-- A system for rapid and reliable specification, execution, and management of large scale workflows. -->
      The <b>Swift parallel scripting language</b>. Easy parallel scripting on multicores, clusters, clouds and supercomputers
    </h1>
  </div>
  <!-- end .grid_12 -->
  <div class="clear"></div>
  <div class="grid_9">
  	<div class="container">
		<?php require('../inc/main_rotator.php') ?>
		<img src="../images/rotator_shadow.png" width="674" height="10" />
	</div>
    <div style="font-size:24px; margin-left:5%; padding-top:10px;">
      Join the <a href="../support">Swift community</a> or contact us at <a href="mailto:swift-user@ci.uchicago.edu?subject=Question regarding Swift parallel scripting">swift-user mailing list </a></div>
  </div>
  <!-- end .grid_9 -->
  <div class="grid_3">
	<div id="box-prime"><div class="button_175"><a href="../tryswift">Try Swift online</a></div>Try Swift from your browser!</div>
	<div id="box-prime"><div class="button_175"><a href="../docs/index.php">Get Started</a></div>Run tutorial examples today!</div>
    <div id="box-prime"><div class="button_175"><a href="../downloads" onClick="javascript: pageTracker._trackPageview('../downloads');">Download Now</a></div><b>Current: </b> 0.96.2, 2015/08/05
	<br /></div>
    <div id="box-prime"><div class="button_175">
    <a href="../Swift-T/index.php">Try Swift/T</a></div>
    Our MPI-based version.
    </div>

<!--
        <div id="feature">
		<h5>Try Swift on the web</h5>
		<p>
		<a href="../tryswift" class="trailingLinkPDF">Try Swift now</a></p>
        </div>
-->

  </div>
  <!-- end .grid_3 -->

  <div class="clear"></div>
  <div class="grid_6">
    <p class="indented">

<b>Swift</b> lets you write parallel scripts that run many copies of
ordinary programs concurrently, using statements like this:

<pre>
foreach protein in proteinList {
  runBLAST(protein);
}
</pre>


<p class="highlight-B">

<b>Swift is parallel:
</b> it runs multiple programs concurrently as
soon as their inputs are available, reducing the need for complex
parallel programming.  <!-- </li> -->

</p>


<p class="highlight-A">

<b>Swift is easy:</b>
Short, simple
scripts can do large-scale work.
The same script runs on
multicore computers, clusters, grids, clouds, and
supercomputers.
</p>

<!-- FIXME: [Swift runtime picture here, in right column] -->

<p class="highlight-C">
<b>Swift is fast:</b>
it can run a million programs, thousands at a time, launching hundreds per second.</p>

<p class="highlight-D">
<b>Swift is flexible:</b>
its being used in many fields of
science, engineering, and business. Read the case studies.
</p>

  </div>
  <!-- end .grid_6 -->
  <div class="grid_3">
  	<div id="feature">
    	<h3>
    	  Who&rsquo;s Using Swift
    	</h3>
		<ul class="swift-bullet">
			<li>physical sciences</li>
			<li>biological sciences</li>
			<li>social sciences</li>
			<li>humanities</li>
			<li>computer science</li>
			<li>education</li>
		</ul>
		<a href="../papers/index.php" class="trailingLink">read the papers</a>
	</div>
	<!-- end feature -->

        <!--
        <div id="feature">
		<h3>
     	 Supported Platforms
    	</h3>
		<p class="indented-half">Cillum brisket ad veniam exercitation andouille. Cillum turkey ut jowl in tail. Reprehenderit nisi pig ex culpa qui, ea meatball ball tip tenderloin pancetta pork. </p>
		<a href="../downloads/index.php" class="trailingLink">explore downloads</a>
	</div>
        -->
	<!-- end feature -->
  </div>
  <!-- end .grid_3 -->
  <div class="grid_3">
        <!--
  	<div id="feature">
    	<h3>
     	 What Users Are Saying
   		</h3>
		<p class="quote">A few lines of SwiftScript described how our analysis procedures should be applied to thousands of fMRI images. Our computation was performed quickly and reliably. Life is good.</p>
		<p class="quote-speaker">Dr. Marielle Goodwin<br />
Director of Brain Trauma Research<br />
University of Chicago Hospitals</p>
<a href="../case_studies/index.php" class="trailingLink">more case studies</a>
	</div>
        -->
	<!-- end feature -->
	<div id="feature">
	<h3>What&rsquo;s New</h3>

        <div id="feature">
        <!-- Saving text for next year. -Justin
		<h5>The ERROR Workshop</h5>
                <center>
                <a href="http://press3.mcs.anl.gov/errorworkshop">
                <img src="ERROR_2015.png"
                     height="140"
                     hspace="20"
                     align="right"/></a>
                </center>

                <p><small>Submit a paper about unexpected or negative
                results using Swift or other scientific tools!</small></p>
                </div>
        -->

        <div id="feature">
		<h5>Swift @ <a href="http://sc15.supercomputing.org">SC 2015</a></h5>
		<p>Swift will be featured in many events: <br>
                    <a href="http://sc14.supercomputing.org/schedule/event_detail?evid=pap450">
                      <a href="http://works.cs.cf.ac.uk/program.php">WORKS (Wozniak)</a> <br>
                      <a href="http://datasys.cs.iit.edu/events/MTAGS15/program.html">MTAGS (Ozik)</a> <br>
                      <a href="http://vis.lbl.gov/Events/ISAV-2015">ISAV (Dorier)</a> <br>
                      <!-- <a href="http://www.vi-hps.org/symposia/other/espt-sc15.html">ESPT (Subei)</a> -->
                  </p>
        </div>

        <div id="feature">
        <h5><b>Outreach:</b> Swift &amp; Tcl</h5>
		<p>Swift/T: Dataflow composition of Tcl scripts for petascale computing (Tcl/Tk Conference 2015)</p>
		<p><a href="https://core.tcl.tk/conference/tcl2015/abstracts.html#T12">Abstract</a></p>
        </div>

	</div>
	<!-- end feature -->
  </div>
  <!-- end .grid_3 -->
  <div class="clear"></div>
  <div class="grid_12">
    <p>&nbsp;</p>
  </div>
  <!-- end .grid_12 -->
  <div class="clear"></div>
  </div>
  <!-- end .grid_12 -->
  <div class="clear"></div>
</div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?>
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
