<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - A system for rapid and reliable specification, execution, and management of large scale workflows</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>
<div class="container_12">
  <a href="../main/index.php"><img src="../images/logo_swift.png" width="328" height="92" alt="Swift" align="left" border="0" /></a>
  <ul id="nav">
    <li><a href="../downloads/index.php">downloads</a></li> 
	<li><a href="../documentation/index.php">documentation</a></li> 
	<li><a href="../case_studies/index.php">case studies</a></li> 
	<li><a href="../papers/index.php">papers</a></li> 
	<li class="last"><a href="../support/index.php">support</a></li> 
  </ul>
  <div class="grid_12">
    <h1 class="tagline">
      A system for rapid and reliable specification, execution, and management of large scale workflows.
    </h1>
  </div>
  <!-- end .grid_12 -->
  <div class="clear"></div>
  <div class="grid_9">
    <p>
      700
    </p>
  </div>
  <!-- end .grid_9 -->
  <div class="grid_3">
    <p>
      220
    </p>
  </div>
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
  <div class="grid_6">
    <p>
      460
    </p>
  </div>
  <!-- end .grid_6 -->
  <div class="grid_3">
    <p>
      220
    </p>
  </div>
  <!-- end .grid_3 -->
  <div class="grid_3">
    <p>
      220
    </p>
  </div>
  <!-- end .grid_3 -->
  <div class="clear"></div>
  <div class="grid_12">
    <p>&nbsp;</p>
  </div>
  <!-- end .grid_12 -->
  <div class="clear"></div>
  </div>
  <!-- end .grid_12 -->
  <div class="clear"></div>
</div>
<!-- end .container_12 -->
<div id="footer">
    <ul>
		<li><a href="../downloads/index.php">downloads</a></li> 
		<li><a href="../documentation/index.php">documentation</a></li> 
		<li><a href="../case_studies/index.php">case studies</a></li> 
		<li><a href="../papers/index.php">papers</a></li> 
		<li class="last"><a href="../support/index.php">support</a></li>
	</ul>
	<div class="footer-blurb">The Swift project is supported by the National Science Foundation with additional support from NIH, Argonne National Laboratory and the University of Chicago Computation Institute.</div>
  </div>
</body>
</html>