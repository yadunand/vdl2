<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
                <meta name="robots" content="noindex">
		<title>Swift: Links</title>
		<link href="../css/style2.css" rel="stylesheet" type="text/css" />
	</head>

	<body id="about">
		<!-- entire page container -->
		<div id="container">
			<!-- header -->
			<div id="header">
				<?php require('../inc/header.php') ?>
			</div>
			<!-- end header -->
			<!-- nav -->
			<div id="nav">
				<?php require('../inc/nav.php') ?>
			</div>
			<!-- end nav -->
			<!-- content container -->
			<div id="content">
				<!-- left side content -->
				<div id="left">
				
<h1>LINKS</h1>

<p>
	Swift incorporates several existing technologies to achieve its
	results.
</p> 

<h4>Globus Toolkit</h4>

<p>
	Swift uses the <a href="http://www.globus.org/">Globus Toolkit</a> as
	middleware to talk to various resources.
</p>

<h4>Java CoG Kit</h4>

<p>
	The <a href="http://wiki.cogkit.org/index.php/Main_Page">CoG
	Kit</a> provides a high level uniform interface to different versions of the
	Globus Toolkit. It also contains the Karajan language and interpreter which
	is used as an underlying execution engine for Swift.
</p>

<h4>TeraGrid</h4>

<p>
	"<a href="http://www.teragrid.org/">TeraGrid</a> is an open scientific
	discovery infrastructure combining leadership class resources at nine
	partner sites to create an integrated, persistent computational resource."
</p>

</div>
				<!-- end left side content -->
				<!-- right side content -->
				<div id="right">
					<?php require('../inc/links_sidebar.php') ?>
				</div>
				<!-- end right side content -->
			</div>
			<!-- end content container-->
			<!-- footer -->
			<div id="footer"><?php require('../inc/footer.php') ?></div> 
			<!-- end footer -->

		</div>
		<!-- end entire page container -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
	</body>
</html>
