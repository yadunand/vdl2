#!/home/davidk/php-5.3.6/sapi/cli/php

<?php
include('geoipcity.inc');
include('geoipregionvars.php');
define ('HOSTNAME', 'db.ci.uchicago.edu');
define ('USERNAME', 'swiftusage');
define ('PASSWORD', 'Taif1TCM');
define ('DATABASE_NAME', 'swiftusage');

# Connect to database
$db = mysql_connect(HOSTNAME, USERNAME, PASSWORD) or die ('Unable to connect');
mysql_select_db(DATABASE_NAME);

# Create HTML table of some basic information
$query = "SELECT COUNT(DISTINCT user_id) FROM swiftusage";
$result = mysql_query($query);
$num_unique_users = mysql_fetch_row($result);
$html_file = fopen("table.html", "w");
$latlons_file = fopen("latlons", "w");
fwrite($html_file, "<table border=\"1\">\n<tr><td>Total Unique Users</td><td>" .  $num_unique_users[0] . "</td></tr>\n");

# Total number of runs
$query = "SELECT COUNT(START_TIME) FROM swiftusage";
$result = mysql_query($query);
$num_runs = mysql_fetch_row($result);
fwrite($html_file, "<tr><td>Total Runs</td><td>" . $num_runs[0] . "</td></tr>\n");

# Total run time 
#$query = "SELECT SEC_TO_TIME(SUM(stop_time) - SUM(start_time)) FROM swiftusage WHERE stop_time IS NOT NULL";
#$result = mysql_query($query);
#$real_time = mysql_fetch_row($result);
#fwrite($html_file, "<tr><td>Accumulated Real Time (hh:mm:ss)</td><td>" . $real_time[0] . "</td>\n");

# Average run time
#$query = "SELECT SEC_TO_TIME(AVG(TIMESTAMPDIFF(SECOND, start_time, stop_time))) FROM swiftusage WHERE stop_time IS NOT NULL";
#$result = mysql_query($query);
#$avg_time_per_run = mysql_fetch_row($result);
#fwrite($html_file, "<tr><td>Average Run Time (hh:mm:ss)</td><td>" . $avg_time_per_run[0] . "</td></tr>\n");

# Longest run
#$query = "SELECT SEC_TO_TIME(MAX(TIMESTAMPDIFF(SECOND, start_time, stop_time))) FROM swiftusage WHERE stop_time IS NOT NULL";
#$result = mysql_query($query);
#$max_run = mysql_fetch_row($result);
#fwrite($html_file, "<tr><td>Longest Run (hh:mm:ss)</td><td>" . $max_run[0] . "</td></tr>\n");
#fwrite($html_file, "<tr><td>Average Time Per Swift Run</td><td>" . $n
fwrite($html_file, "</table>\n");
fclose($html_file);

# GeoIP info
$query = "select distinct(ip_address) from swiftusage";
$result = mysql_query($query);
$gi = geoip_open("GeoLiteCity.dat", GEOIP_STANDARD);
$count = 0;
while($row = mysql_fetch_array($result)) {
   $record = geoip_record_by_addr($gi, $row['ip_address']);
   fwrite($latlons_file, $record->city . "," . $record->latitude . "," . $record->longitude . "," . $count . "\n");
   $count++; 
}
fclose($latlons_file);

# Monthly information
$query = "SELECT * FROM swiftusage ORDER BY start_time";
$result = mysql_query($query);
$months = array();
$allusers = array();
while ($row = mysql_fetch_array($result)) {
   $month_string = substr($row['start_time'], 0, 7);
   $year_string_temp = substr($month_string, 2, 2);
   $month_string_temp = ltrim(substr($month_string, 5, 2), 0);
   $month_string = "$month_string_temp-$year_string_temp";
   $user = $row['user_id'];
  
   if(!isset($months[$month_string]['runs'])) { $months[$month_string]['runs'] = 0; }
   if(!isset($months[$month_string]['users'])) { $months[$month_string]['users'] = array(); } 
   if(!isset($months[$month_string]['newusers'])) { $months[$month_string]['newusers'] = array(); }
   if(!isset($months[$month_string]['longruns1'])) { $months[$month_string]['longruns1'] = 0; }
   if(!isset($months[$month_string]['longruns5'])) { $months[$month_string]['longruns5'] = 0; }

   # Number of runs
   $months[$month_string]['runs']++;

   # Unique users per month
   if(!in_array($user, $months[$month_string]['users'])) {
      array_push($months[$month_string]['users'], $user);
   }

   # New users per month
   if(!in_array($user, $allusers)) {
      array_push($months[$month_string]['newusers'], $user);
      array_push($allusers, $user);
   }
}

# Total number of runs greater than 1 minute
$query = "SELECT * FROM swiftusage WHERE (stop_time - start_time) > 60";
$result = mysql_query($query);
while($row = mysql_fetch_array($result)) {
   $month_string = substr($row['start_time'], 0, 7);
   $year_string_temp = substr($month_string, 2, 2);
   $month_string_temp = ltrim(substr($month_string, 5, 2), 0);
   $month_string = "$month_string_temp-$year_string_temp";
   $months[$month_string]['longruns1']++;
}

# Total number of runs greater than 5 minutes
$query = "SELECT * FROM swiftusage WHERE (stop_time - start_time) > 300";
$result = mysql_query($query);
while($row = mysql_fetch_array($result)) {
   $month_string = substr($row['start_time'], 0, 7); 
   $year_string_temp = substr($month_string, 2, 2); 
   $month_string_temp = ltrim(substr($month_string, 5, 2), 0); 
   $month_string = "$month_string_temp-$year_string_temp";
   $months[$month_string]['longruns5']++;
}
 
# Total runs
$usageFile = fopen("usage.txt", 'w');
foreach($months as $key => $value) {
   fwrite($usageFile, "$key " . $value['runs'] . "\n");
}
fclose($usageFile);

# Total runs longer than 1 minute
$longUsage1File = fopen("longUsage1.txt", 'w');
foreach($months as $key => $value) {
  fwrite($longUsage1File, "$key " . $value['longruns1'] . "\n");
}
fclose($longUsage1File);

# Total runs longer than 5 minutes
$longUsage5File = fopen("longUsage5.txt", 'w');
foreach($months as $key => $value) {
  fwrite($longUsage5File, "$key " . $value['longruns5'] . "\n");
}
fclose($longUsage5File);

# Unique users per month
$users_per_month_file = fopen("users_per_month.txt", 'w');
foreach($months as $key => $value) {
   fwrite($users_per_month_file, "$key " . count($value['users']) . "\n");
}
fclose($users_per_month_file);

# New users per month
$new_users_per_month_file = fopen("new_users_per_month.txt", 'w');
foreach($months as $key => $value) {
   fwrite($new_users_per_month_file, "$key " . count($value['newusers']) . "\n");
}
fclose($new_users_per_month_file);

mysql_free_result($result);
mysql_close($db);
?>

