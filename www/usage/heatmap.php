<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Heatmaps</title>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
      #panel {
        position: absolute;
        top: 5px;
        left: 50%;
        margin-left: -180px;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=visualization"></script>
    <script>

var map, pointarray, heatmap;

var locationData = [
   <?php
      $lines = file('http://swiftlang.org/usage/latlons');
      foreach($lines as $line_num => $line) {
         list($city, $lat, $lon, $num) = split(',', $line);
         $num = rtrim($num);
         if(!is_numeric($lat) || !is_numeric($lon)) { continue; }
         echo "new google.maps.LatLng($lat, $lon),\n";
      }
   ?>
];

var latlngbounds = new google.maps.LatLngBounds();     
<?php
foreach($lines as $line_num => $line) {
         list($city, $lat, $lon, $num) = split(',', $line);
         $num = rtrim($num);
         if(!is_numeric($lat) || !is_numeric($lon)) { continue; }   
         echo "latlngbounds.extend( new google.maps.LatLng($lat, $lon));\n";
}
?>

function initialize() {
  var mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(37.774546, -122.433523),
    mapTypeId: google.maps.MapTypeId.SATELLITE,
    panControl: false,
    zoomControl: false,
    scaleControl: false,
    streetViewControl: false,
    draggable: false,
  };

  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  map.fitBounds(latlngbounds);

  var pointArray = new google.maps.MVCArray(locationData);

  heatmap = new google.maps.visualization.HeatmapLayer({
    data: pointArray
  });
  heatmap.set('opacity', 1.0);
  heatmap.set('radius', 15);

  var gradient = [
    'rgba(0, 255, 255, 0)',
    'rgba(0, 255, 255, 1)',
    'rgba(0, 191, 255, 1)',
    'rgba(0, 127, 255, 1)',
    'rgba(0, 63, 255, 1)',
    'rgba(0, 0, 255, 1)',
    'rgba(0, 0, 223, 1)',
    'rgba(0, 0, 191, 1)',
    'rgba(0, 0, 159, 1)',
    'rgba(0, 0, 127, 1)',
    'rgba(63, 0, 91, 1)',
    'rgba(127, 0, 63, 1)',
    'rgba(191, 0, 31, 1)',
    'rgba(255, 0, 0, 1)'
  ]
  heatmap.set('gradient', gradient);

  heatmap.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);

    </script>
  </head>
  <body>
    <div id="map-canvas"></div>
  </body>
</html>
