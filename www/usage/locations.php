<html> 
<head> 
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" /> 
  <title>Google Maps Multiple Markers</title> 
  <script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>
</head> 
<body>
  <div id="map" style="width: 100%; height: 100%;"></div>

  <script type="text/javascript">
    var locations = [
    <?php
       $lines = file('http://swiftlang.org/usage/latlons');
       foreach($lines as $line_num => $line) {
           list($city, $lat, $lon, $num) = split(',', $line);
           $num = rtrim($num);
           if(!is_numeric($lat) || !is_numeric($lon)) { continue; }
    ?> 
    [ '<?php echo $city ?>', <?php echo $lat ?>, <?php echo $lon ?>, <?php echo $num ?> ], <?php } ?>

    ];

    var latlngbounds = new google.maps.LatLngBounds(); 

    <?php
       $lines = file('http://swiftlang.org/usage/latlons');
       foreach($lines as $line_num => $line) {
           if(empty($lat) || empty($lon)){ continue; }
           list($city, $lat, $lon, $num) = split(',', $line);
           $num = rtrim($num);
           if(!is_numeric($lat) || !is_numeric($lon)) { continue; }
    ?>
    latlngbounds.extend( new google.maps.LatLng(<?php echo $lat ?>, <?php echo $lon ?>));
    <?php } ?>
    

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.HYBRID,
      panControl: false,
      zoomControl: false,
      scaleControl: false,
      streetViewControl: false,
      draggable: false,
    });
    map.fitBounds(latlngbounds);

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>
</body>
</html>

