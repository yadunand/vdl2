<h3>Current Swift Project Team</h3>

<ul class="swift-bullet">
  <li>Tim Armstrong</li>
  <li>Yadu N Babuji</li>
  <li>Ian Foster</li>
  <li>Mihael Hategan</li>
  <li>Dan Katz</li>
  <li>David Kelly</li>
  <li>Ketan Maheshwari</li>
  <li>Mike Wilde</li>
  <li>Justin Wozniak</li>
  <li>Zhao Zhang<li>
</ul>

<h3>Swift Alumni</h3>

<ul class="swift-bullet">
  <li>Ben Clifford</li>
  <li>Allan Espinosa</li>
  <li>Sarah Kenny</li>
  <li>Jon Monette</li>
  <li>Veronika Nefedova</li>
  <li>Tibi Stef-Praun</li>
  <li>Yong Zhao</li>
</ul>

<!-- <h4>NEWS ITEMS</h4>
<p>Interesting news items can go here.</p>-->
