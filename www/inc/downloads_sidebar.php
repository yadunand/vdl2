<div id="box-prime">
<!-- <h2><a name="stable"></a>Latest Release</h2> -->
<div class="button_175"><a href="../packages/swift-0.96.2.tar.gz">Download Now</a></div>
<b>Swift-0.96.2</b> current version<br />2015/08/05
<br />
</div>
<p>&nbsp;</p>
<h3>Getting Started </h3>
<p><strong>Swift Quickstart Guide</strong> [<a href="../guides/release-0.95/quickstart/quickstart.html">html</a>]</p>
<p>Use the Quickstart Guide to help you install and configure swift and run a simple 'Hello World' example.</p>

<h3>Bug Reports</h3>
	<p>We have a Bugzilla, at <a href="http://bugzilla.mcs.anl.gov/swift/">bugzilla.mcs.anl.gov/swift/</a>. </p>

