<a href="../main/index.php"><img src="../images/logo_swift.png" width="328" height="92" alt="Swift" align="left" border="0" /></a>
<div id="holder"><div id="mover" style="
<?php 
	$p = $_SERVER['PHP_SELF'];
	if (strpos($p, 'main')) echo 'visibility:hidden;';
 	elseif (strpos($p, 'downloads')) echo 'left:370px;';
	elseif (strpos($p, 'docs')) echo 'left:505px;';
	elseif (strpos($p, 'case_studies')) echo 'left:644px;';
	elseif (strpos($p, 'papers')) echo 'left:750px;';
	elseif (strpos($p, 'support')) echo 'left:836px;';
	else echo 'visibility:hidden;';
	?>"><img src="../images/uvula.png" width="20" height="8" /></div></div>
  <ul id="nav">
    <li><a href="../downloads/index.php"<?php if (strpos($p, 'downloads')) echo' class="current"'?>>downloads</a></li> 
	<li><a href="../docs/index.php"<?php if (strpos($p, 'docs')) echo' class="current"'?>>documentation</a></li> 
    <li><a href="../case_studies/index.php"<?php if (strpos($p, 'case_studies')) echo' class="current"'?>>case studies</a></li> 
	<li><a href="../papers/index.php"<?php if (strpos($p, 'papers')) echo' class="current"'?>>papers</a></li> 
	<li class="last"><a href="../support/index.php"<?php if (strpos($p, 'support')) echo' class="current"'?>>support</a></li>
  </ul>
