<h3>Highlights</h3>

<h4>Technology</h4>
<p><em>Swift: A language for distributed parallel scripting (2011)</em> [<a href="../papers/SwiftLanguageForDistributedParallelScripting.pdf" target="_blank">pdf</a>]</p><br />
<p><em>Parallel Scripting for Applications at the Petascale and Beyond (2009)</em> [<a href="../papers/SwiftParallelScripting.pdf" target="_blank">pdf</a>]</p><br />
<p><em>Coasters: uniform resource provisioning and access for clouds and grids (2011)</em> [<a href="../papers/UCC-coasters.pdf" target="_blank">pdf</a>]</p><br />

<h4>Applications</h4>

<p><em>Design and Implementation of ‘Many Parallel Task’ Hybrid Subsurface Model (2011)</em> [<a href="http://datasys.cs.iit.edu/events/MTAGS11/p03.pdf" target="_blank">pdf</a>]</p><br />
<p><em>OpenMx: An Open Source Extended Structural Equation Modeling Framework (2011)</em> [<a href="../papers/openmx.pdf" target="_blank">pdf</a>]</p><br />
<p><em>Modeling large regions in proteins: Applications to loops, termini, and folding (2011)</em> [<a href="../papers/767_fta.pdf" target="_blank">pdf</a>]</p><br />
<p><em>Parallel High-resolution Climate Data Analysis using Swift (2011)</em> [<a href="../papers/highresclimatedata.pdf" target="_blank">pdf</a>]</p><br />

