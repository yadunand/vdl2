<div id="footer">
    <ul>
		<li><a href="../downloads/index.php">downloads</a></li> 
		<li><a href="../docs/index.php">documentation</a></li>
		<li><a href="../case_studies/index.php">case studies</a></li> 
		<li><a href="../papers/index.php">papers</a></li> 
		<li><a href="../support/index.php">support</a></li>
		<li class="last"><a href="../about/index.php">about</a></li>
	</ul>
<div class="footer-blurb">The Swift project is supported by the National Science Foundation and US Department of Energy Office of Science, with additional support from the National Institutes of Health, Argonne National Laboratory and the University of Chicago Computation Institute.</div>
  </div>
