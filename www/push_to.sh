#!/bin/bash 

# PUSH_TO.SH
# Use './push_to.sh -h' for usage

set -u

usage()
{
   echo   "usage: push_to.sh [-u] <DESTINATION>"
   printf "\t -u: Use cp -u to update file based on timestamp\n"
}

# File permissions
CHMOD_DIRECTORY_MODE="775"
CHMOD_FILE_MODE="664"
GROUP="vdl2-svn"

UPDATE=0
while getopts "hu" OPTION
do
   case $OPTION in
      h) usage ; exit 0 ;;
      u) UPDATE=1       ;;
      *) exit 1         ;; # Bash prints an error message
   esac
done
shift $(( OPTIND -1 ))

U=""
if (( UPDATE ))
then
   U=u
fi

# Verify command line arguments
if [ -n "${1:-}" ]; then
   DESTINATION=$1
else
   echo "You must provide DESTINATION!"
   usage
   exit 1
fi

# Make sure we are calling this script in the same directory as the script is located
pushd $(dirname $(readlink -f $0)) > /dev/null 2>&1

# Long list of files to explicitly include follows
FILES=(
css/style1col.css
css/960_12_col.css
css/reset.css
css/style2.css
css/style.css
css/orbit-1.2.3.css
css/style3.css
images/bg_quotes.png
images/bg_box_shaded.png
images/bullet.png
images/workflow.jpg
images/bullet_prev_32.png
images/rotator.png
images/icon_special_A.png
images/button_bg_175_ro.png
images/bullet_swift.png
images/logo_swift.png
images/bullet_pdf.png
images/workflow.gif
images/rotator_shadow.png
images/uvula.png
images/icon_special_D.png
images/icon_special_C.png
images/button_bg_175_sp.png
images/orbit/timer-black.png
images/orbit/bullets.jpg
images/orbit/pause-black.png
images/orbit/spinner.gif
images/orbit/left-arrow.png
images/orbit/mask-black.png
images/orbit/right-arrow.png
images/orbit/loading.gif
images/orbit/rotator-black.png
images/rotator/bg_rotator1.jpg
images/rotator/bg_rotator-glass.jpg
images/rotator/bg_rotator3.png
images/rotator/bg_rotator2.png
images/rotator/bg_rotator1.png
images/rotator/bg_rotator3a.jpg
images/rotator/2011_08_neuroscience.jpg
images/rotator/bg_rotator3.jpg
images/rotator/bg_rotator-hydrology.jpg
images/rotator/bg_rotator2.jpg
images/rotator/bg_rotator2a.jpg
images/icon_special_B.png
images/graphics/h_backgrnd2.gif
images/graphics/h_ckgrnd.gif
images/graphics/h_backgrnd.gif
images/graphics/banner.gif
images/graphics/swift_sketch.gif
images/graphics/banner2.gif
images/bg_page.png
images/button_bg_175.png
images/bullet_next_32.png
about/index.php
Swift-T/index.php
Swift-T/*.html
Swift-T/*.png
Swift-T/images/*.png
Swift-T/downloads/*.tar.gz
index.html
shCoreu.js
js/jquery-1.5.1.min.js
js/jquery.orbit-1.2.3.min.js
main/index.php
main/template.php
main/ERROR_2015.png
case_studies/images/CIM-Earth.jpg
case_studies/images/climate.png
case_studies/images/glass.png
case_studies/images/protein1.jpg
case_studies/images/protein1-old.png
case_studies/images/protein2.jpg
case_studies/images/protein2.png
case_studies/images/protein3.png
case_studies/images/rdcep1.png
case_studies/images/rdcep2.png
case_studies/images/rdcep3.png
case_studies/images/rdcep4.png
case_studies/images/rna.png
case_studies/images/hydrology.jpg
case_studies/images/glass2.jpg
case_studies/index.php
case_studies/CIM-Earth.php
case_studies/Hydrology.php
case_studies/Glass-Structure.php
case_studies/Protein-Structure.php
case_studies/Protein-RNA.php
case_studies/index.php
case_studies/CIM-Earth.php
case_studies/CNARI.php
case_studies/images/CNARI-fig2.png
case_studies/images/glass1.png
style.css
sitemap.xml
apps/index.php
apps/style2.css
links/index.php
downloads/archive.php
downloads/index.php
downloads/release-notes-0.5.txt
downloads/release-notes-0.9.txt
downloads/release-notes-0.7.txt
downloads/release-notes-0.6.txt
downloads/release-notes-0.8.txt
README.txt
tests/tests.pl
dhtml.js
updatenodocs.sh
template_info/template.php
papers/index.php
papers/pdfs/*.pdf
usage/charts.swf
usage/usage.php
usage/resources/cursor/cursor.swf
usage/resources/cursor/cursor.fla
usage/resources/scroll/slider_handle_L.gif
usage/resources/scroll/slider_handle.gif
usage/resources/scroll/slider_over_L.gif
usage/resources/scroll/button_2_idle.jpg
usage/resources/scroll/slider_L.gif
usage/resources/scroll/button_2_over.jpg
usage/resources/scroll/slider_over_R.gif
usage/resources/scroll/button_1_over.jpg
usage/resources/scroll/slider_handle_R.gif
usage/resources/scroll/button_all_over.jpg
usage/resources/scroll/slider_bg.gif
usage/resources/scroll/slider_R.gif
usage/resources/scroll/button_1_idle.jpg
usage/resources/scroll/button_all_idle.jpg
usage/resources/scroll/button_all_press.jpg
usage/resources/scroll/button_2_press.jpg
usage/resources/scroll/button_1_press.jpg
usage/resources/chart_in_swf/chart_in_swf.fla
usage/resources/button_rollover/button_rollover.swf
usage/resources/button_rollover/button_rollover.fla
usage/resources/preview_scroll/preview_handle_1.fla
usage/resources/preview_scroll/preview_handle_2.swf
usage/resources/preview_scroll/black.swf
usage/resources/preview_scroll/black.fla
usage/resources/preview_scroll/preview_handle_2.fla
usage/resources/preview_scroll/preview_handle_1.swf
usage/resources/full_screen/full_screen.swf
usage/resources/full_screen/full_screen.fla
usage/geoip.inc
usage/geoipregionvars.php
usage/charts_library/mxno.swf
usage/charts_library/arst.swf
usage/charts_library/buno.swf
usage/charts_library/ars3.swf
usage/charts_library/arno.swf
usage/charts_library/piim.swf
usage/charts_library/pino.swf
usage/charts_library/scno.swf
usage/charts_library/brfl.swf
usage/charts_library/clfl.swf
usage/charts_library/dono.swf
usage/charts_library/clp3.swf
usage/charts_library/ar3d.swf
usage/charts_library/pi3d.swf
usage/charts_library/cl3d.swf
usage/charts_library/lnno.swf
usage/charts_library/dollar.jpg
usage/charts_library/cnno.swf
usage/charts_library/pie.jpg
usage/charts_library/pono.swf
usage/charts_library/clno.swf
usage/charts_library/clim.swf
usage/charts_library/brno.swf
usage/charts_library/cls3.swf
usage/charts_library/clst.swf
usage/charts_library/brst.swf
usage/geoipcity.inc
usage/locations.php
usage/GeoLiteCity.dat
usage/AC_RunActiveContent.js
usage/update_data.php
cookbook/cookbook-asciidoc.txt
cookbook/figures/coaster_setup.png
cookbook/cookbook-asciidoc.html
support/index.php
update.sh
inc/footer.php
inc/downloads_sidebar.php
inc/support_sidebar.php
inc/papers_sidebar.php
inc/home_sidebar.php
inc/rotator.php
inc/header2.php
inc/main_rotator.php
inc/about_sidebar.php
inc/apps_sidebar.php
inc/nav.php
inc/docs_sidebar.php
inc/case_study_sidebar.php
inc/links_sidebar.php
inc/footer2.php
inc/side_content.php
inc/header.php
docs/tracking_policy.php
docs/license.php
docs/usage_data.php
docs/index.php
shBrushVDL2.js
ATPESC_2014/*.html
ATPESC_2014/**/*.html
ATPESC_2014/**/*.png
)

# Make sure destination directory exists
if [ ! -d "$DESTINATION" ]; then
   mkdir $DESTINATION > /dev/null 2>&1 || exit 1
   chgrp $GROUP $DESTINATION > /dev/null 2>&1
   chmod $CHMOD_DIRECTORY_MODE $DESTINATION > /dev/null 2>&1
fi

# Copy files to destination
for FILE in ${FILES[@]}
do
   destination_dir=`dirname $FILE`
   mkdir -p $DESTINATION/$destination_dir
   chgrp $GROUP $DESTINATION/$destination_dir > /dev/null 2>&1
   chmod $CHMOD_DIRECTORY_MODE $DESTINATION/$destination_dir > /dev/null 2>&1
   cp -${U}v $FILE $DESTINATION/$destination_dir || exit 1
   chgrp $GROUP $DESTINATION/$FILE > /dev/null 2>&1
   chmod $CHMOD_FILE_MODE $DESTINATION/$FILE > /dev/null 2>&1
done

popd > /dev/null 2>&1

# Local Variables:
# sh-basic-offset: 3
# End:
