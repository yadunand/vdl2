#!/usr/bin/perl 
use strict;
use lib '/home/davidk/modules/';
use File::Basename;
use Lingua::EN::Numbers::Ordinate;

print <<'END';
Content-type: text/html

<html>
<head>
<title>Swift Testing</title>
<link rel="stylesheet" type="text/css" href="../css/style1col.css" />
</head>
<body>
END

my @table_data=();
my $count=1;
my @months = ("Zero", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

foreach my $swift_version(<*/>) {
   $swift_version =~ s/\///;
   $table_data[0] .= "<th>$swift_version</th>";
    
   my $previous_month=0;

   foreach my $test_result(<$swift_version/*/test*.html>) {
      (my $ignore, my $year, my $month, my $day) = (split('-', (split('/', $test_result))[1]));
      $day =~ s/^0*//;
      my $remote_html;
      my $remote_html = dirname($test_result) . "/result_links.html";
      $table_data[$count] .= "<td>$months[$month] " . ordinate($day) . ", $year <a href=\"$test_result\">[local]</a>";
      if( -e "$remote_html" ) {
         $table_data[$count] .= "<a href=\"$remote_html\">[remote]</a>\n";
      }
      $table_data[$count] .= "</td>\n";
      $count++;
   }
 
   $count=1;
}

# Print table
print "<center><table border=\"1\" width=\"600\">\n";
print "<tr>$table_data[0]</tr>\n";
foreach my $td(@table_data[1..$#table_data]) {
   print "<tr>$td</tr>\n";
}
print "</table></center></body></html>\n";
