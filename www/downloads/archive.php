<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - Downloads</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">

<p>&nbsp;</p>
<h2>Older releases</h2>
<p>&nbsp;</p>

<h3>Swift 0.93 - 2011/11</h3>
<p>Precompiled binary distribution
[<a href="../packages/swift-0.93.tar.gz"
onClick="javascript: pageTracker._trackPageview('/swift/packages/swift-0.93.tar.gz');"
>swift-0.93.tar.gz</a>]
<p>&nbsp;</p>


<h3>Swift 0.92.1 - 2011/04/13</h3>
<p>Precompiled binary distribution
[<a href="../packages/swift-0.92.1.tar.gz"
onClick="javascript: pageTracker._trackPageview('/swift/packages/swift-0.92.1.tar.gz');"
>swift-0.92.1.tar.gz</a>]
<p>&nbsp;</p>

<h3>Swift 0.91 - 2010/12/23</h3>
<p>Precompiled binary distribution
[<a href="../packages/swift-0.91.tar.gz"
onClick="javascript: pageTracker._trackPageview('/swift/packages/swift-0.91.tar.gz');"
>swift-0.91.tar.gz</a>]
<p>&nbsp;</p>

<h3>Swift 0.9 - 2009/04/27</h3>
<p>Precompiled binary distribution
[<a href="../packages/swift-0.9.tar.gz"
onClick="javascript: pageTracker._trackPageview('/swift/packages/swift-0.9.tar.gz');"
>swift-0.9.tar.gz</a>]</p>
<p>&nbsp;</p>


<h3>Swift 0.8 - 2009/01/30</h3>
<p>Precompiled binary distribution
[<a href="../packages/swift-0.8.tar.gz"
onClick="javascript: pageTracker._trackPageview('/swift/packages/swift-0.8.tar.gz');"
>swift-0.8.tar.gz</a>]
<p>&nbsp;</p>

<h3>Swift 0.7 - 2008/11/11</h3>

<p>Precompiled binary distribution
[<a href="../packages/vdsk-0.7.tar.gz">vdsk-0.7.tar.gz</a>]
</p>
<p>&nbsp;</p>

<h3>Swift 0.6 - 2008/08/25</h3>
<p>Precompiled binary distribution
[<a href="../packages/vdsk-0.6.tar.gz">vdsk-0.6.tar.gz</a>]
</p>
<p>&nbsp;</p>

<h3>Swift 0.5 - 2008/04/16</h3>
<p>Precompiled binary distribution
[<a href="../packages/vdsk-0.5.tar.gz">vdsk-0.5.tar.gz</a>]
</p>
<p>&nbsp;</p>

<h3>Swift 0.4 - 2008/03/18</h3>
<p>Precompiled binary distribution
[<a href="../packages/vdsk-0.4.tar.gz">vdsk-0.4.tar.gz</a>]
</p>
<p>&nbsp;</p>

<h3>Swift 0.3 - 2007/10/04</h3>
<p>Precompiled binary distribution
[<a href="../packages/vdsk-0.3.tar.gz">vdsk-0.3.tar.gz</a>]
</p>
<p>&nbsp;</p>

<h3>Swift 0.2 - 2007/07/19</h3>
<p>Precompiled binary distribution
[<a href="../packages/vdsk-0.2.tar.gz">vdsk-0.2.tar.gz</a>]
</p>
<p>&nbsp;</p>

<h3>Swift 0.1 - 2007/03/02</h3>
<p>Precompiled binary distribution
[<a href="../packages/vdsk-0.1.tar.gz">vdsk-0.1.tar.gz</a>]
</p>
<p>&nbsp;</p>

<h3>Swift 0 RC3</h3>
<p>Precompiled binary distribution
[<a href="../packages/vdsk-0-rc3.tar.gz">vdsk-0-rc3.tar.gz</a>]
</p>
<p>&nbsp;</p>

<h3>Swift 0 RC2</h3>
<p>Precompiled binary distribution
[<a href="../packages/vdsk-0-rc1.tar.gz">vdsk-0-rc1.tar.gz</a>] 
<p>&nbsp;</p>

<h3>Swift 0 RC1</h3>
<p>Precompiled binary distribution
[<a href="../packages/vdsk-0-rc1.tar.gz">vsdk-0-rc1.tar.gz</a>]
</p>
<p>&nbsp;</p>

<h3>Swift 0 RC0</h3>
<p>Precompiled binary distribution
[<a href="../packages/vdsk-0-rc0.tar.gz">vdsk-0-rc0.tar.gz</a>]
</p>
<p>&nbsp;</p>
</div>

  <!-- end .grid_9 -->
  <div class="grid_3">
    <?php require('../inc/downloads_sidebar.php') ?>
  </div>
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
				
  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
