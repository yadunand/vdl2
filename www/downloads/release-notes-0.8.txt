These are the release notes for Swift 0.8

More information about Swift can be found at http://www.ci.uchicago.edu/swift/

Swift 0.8 is built from Swift SVN r2448 and cog SVN r2261

The following are significant changes since Swift 0.7 was released:

Log plotting:
 * The swift log-processing package is now included in the release. This
   can be used to plot graphs of Swift logs using the swift-plot-log
   command:

    $ swift-plot-log first-200901010000-abcdefg.log

   which will create a report in report-first-200901010000-abcdefg/index.html

Execution:
 * Some job execution systems do not set the initial job working directory
   as specified by Swift. Previously Swift was unable to execute on such
   systems. As of 0.8, Swift sets the initial job working directory in a
   more robust fashion. This problem commonly affected OSG sites running
   Condor.

 * Application success/failure status can now be taken from the CoG
   provider layer, instead of using success/failure files on the remote
   file system. This can reduce the load on the remote file system, but
   does not work with all job execution mechanisms. Specifically, this
   mechanism will not work with GRAM2 or clustering.

Local execution:
 * A deadlock caused when jobs output large amounts of data to stderr has been
   fixed.

Commandline client:
 * A number of error messages have been improved.
 * Console output is much less verbose: the progress ticker appears more often,
   and individual application start and end messages are no longer shown. This
   improves the quality of console output when jobs are executed at a high rate.

Language:
 * Handling of assignment statements in declarations and out of declarations
   has been made more homogenous.  Previously, some assignments could only
   be made in a declaration statement (such as assigning arrays); and some
   assignments could only be made away from a declaration statement (such as
   expressions whose values were computed by some slow process). These two
   forms of assignment should now be interchangeable.

 * Mapper parameters can now be results of slow computations, rather than
   needing to be known by the time that declarations are first encountered.
   This remedies previous unintuitive behaviour.


