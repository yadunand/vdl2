<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - Downloads</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<!-- Google analytics -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25069254-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">
	<h1>Downloads</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">

<h2>License</h2>
<p>
Swift is licensed under the
<a href="../docs/license.php">Apache License, Version 2.0</a>.<br></br>
</p>
</h2>

<h2>Usage Tracking</h2>
<p>
Swift collects worldwide usage information in order to justify funding and
improve reliability and usability. More information about this is available in the
<a href="../docs/tracking_policy.php">Swift Usage Tracking Policy</a>.<br>
</p>
</h2><br>

<h2>Latest Stable Release</h2>
<p>
<p><b>Swift 0.96.2</b><br><br>
This release adopts a simplified, consolidated and more expressive <a href="../guides/release-0.96/userguide/userguide.html#_configuration">configuration file format</a>.

This is the recommended
release for new users and we strongly urge our current users to migrate.
<br><br>
Precompiled binary distribution
[<a href="../packages/swift-0.96.2.tar.gz"
onClick="javascript: pageTracker._trackPageview('../packages/swift-0.96.2.tar.gz');"
 >swift-0.96.2.tar.gz</a>] [<a href="../guides/release-0.96/releasenotes/releasenotes.html">Release notes</a>]


<h2>Interim Release</h2>
<p>
<p><b>Swift 0.95 - 2014/01/09</b><br><br>
Swift 0.95 includes several major bug-fixes from the 0.94 release. Since there are no major features in this release,
this release shares documentation with the previous Swift 0.94 release.
<br><br>
Precompiled binary distribution
[<a href="../packages/swift-0.95.tar.gz"
onClick="javascript: pageTracker._trackPageview('../packages/swift-0.95.tar.gz');"
>swift-0.95.tar.gz</a>] [<a href="../guides/release-0.95/releasDDenotes/releasenotes.html">Release notes</a>]

<h2>Source Code</h2>

<p>
The source code for Swift is available to developers who have an interest in contributing
new features. The code is hosted on <a href="https://github.com/swift-lang/swift-k">Github</a>.
To download and compile the source code for the latest release Swift-0.96, you will need
<a href="http://ant.apache.org">Apache Ant</a> and
<a href="http://www.oracle.com/technetwork/java/javase/downloads/index.html">Java JDK</a>.
</p>

<p>
	<br/>
	Here are the steps:
</p>

	<div class="highlight">
        $ git clone https://github.com/swift-lang/swift-k.git swift-0.96
        $ cd swift-0.96
        $ git checkout -b release-0.96-swift
        $ ant redist
	</div>
	<br/>
    NOTE: The dist directory will contain the complete build.
</p>
<p></p>

<h3>Development Version</h3>

<p>
	The development version of Swift is aimed at developers and testers. The development
    code has the highest chance of containing buggy and untested code. If you need
    stability please use the <b>latest release</b>.
</p>

<p>
    <b> Please note that we are migrating from subversion to git, with <a href="https://github.com/swift-lang/swift-k">github</a> for hosting </b>
</p>

	<div class="highlight">
        $ git clone https://github.com/swift-lang/swift-k.git swift-devel
        $ cd swift-devel
        $ ant redist
	</div>
<p>&nbsp;</p>

For previous release, see <a href="archive.php">Older Downloads</a></p>
	</div>
  <!-- end .grid_9 -->
  <div class="grid_3">
    <?php require('../inc/downloads_sidebar.php') ?>
  </div>
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
				
  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
