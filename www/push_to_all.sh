hosts="ec2-23-20-156-199.compute-1.amazonaws.com ec2-54-87-91-46.compute-1.amazonaws.com ec2-54-196-24-118.compute-1.amazonaws.com ec2-23-20-140-94.compute-1.amazonaws.com ec2-54-82-19-125.compute-1.amazonaws.com ec2-184-72-180-226.compute-1.amazonaws.com ec2-23-20-187-161.compute-1.amazonaws.com ec2-54-81-204-169.compute-1.amazonaws.com ec2-54-205-193-6.compute-1.amazonaws.com ec2-54-237-183-152.compute-1.amazonaws.com"

for host in $hosts
do
   scp www.tar.gz ubuntu@$host:
   ssh ubuntu@$host "cd /var/www && tar xfz /home/ubuntu/www.tar.gz"
done
