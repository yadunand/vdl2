<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="robots" content="noindex">
<title>Swift - Papers</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<!-- Google analytics -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25069254-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">
	<h1>Papers</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">
	<h3>Swift Language and Runtime Technology</h3>

<!-- TEMPLATE -->
<!--
<div class="publication">
	<span class="authors"></span>
        <span class="title"></span>
	<span class="source"></span>
        <span class="date">.</span>
        [ <a href="pdfs/.pdf">pdf</a> ]
</div>
-->


<div class="publication">
	<span class="authors">Justin M. Wozniak, Hemant Sharma,
	Timothy G. Armstrong, Michael Wilde, Jonathan D. Almer, Ian
	T. Foster </span>
        <span class="title">Big data staging with MPI-IO for
        interactive X-ray science</span>
        <span class="source">Proc. IEEE/ACM Symposium on Big Data Computing
        </span>
        <span class="date">2014.</span>
        [ <a href="pdfs/Swift_MPI-IO_2014.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">James C. Phillips, John E. Stone, Kirby
	L. Vandivort, Timothy G. Armstrong, Justin M. Wozniak, Michael
	Wilde, Klaus Schulten</span>
        <span class="title">Petascale Tcl with NAMD, VMD, and Swift/T</span>
        <span class="source">Proc. Workshop for High Performance
        Technical Computing in Dynamic Languages at SC, 2014.
        </span>
        <span class="date">2014.</span>
        [ <a href="pdfs/Swift_NAMD_2014.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Justin M. Wozniak, Michael Wilde, Ian T. Foster</span>
        <span class="title">Language features for scalable distributed-memory dataflow computing </span>
	<span class="source">Proc Data-flow Execution Models for
	Extreme-scale Computing at PACT</span>
        <span class="date">2014.</span>
        [ <a href="pdfs/DFM_2014.pdf">pdf</a> ]
</div>


<div class="publication">
	<span class="authors">Timothy G. Armstrong, Justin M. Wozniak, Michael Wilde, Ian T. Foster</span>
        <span class="title">Compiler techniques for massively scalable implicit task parallelism</span>
	<span class="source">Proc. SC</span>
        <span class="date">2014.</span>
        [ <a href="pdfs/Swift_2014.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors"> Scott J. Krieder, Justin M. Wozniak, Timothy Armstrong, Michael Wilde, Daniel S. Katz, Benjamin Grimmer, Ian T. Foster, Ioan Raicu</span>
        <span class="title">Design and evaluation of the GeMTC framework for GPU-enabled many-task computing</span>
	<span class="source">Proc. HPDC</span>
        <span class="date">2014.</span>
        [ <a href="pdfs/GeMTC_2014.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Ketan Maheshwari, Justin M. Wozniak, Hao Yang, Daniel S. Katz, Matei Ripeanu, Victor Zavala, Michael Wilde </span>
        <span class="title">Evaluating storage systems for scientific data in the cloud</span>
	<span class="source">Proc. ScienceCloud (Best Paper Awardee)</span>
        <span class="date">2014.</span>
        [ <a href="pdfs/ScientificStorageCloud_2014.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Justin M. Wozniak, Timothy G. Armstrong, Daniel S. Katz, Michael Wilde, Ian T. Foster</span>
        <span class="title">Toward computational experiment management via multi-language applications</span>
	<span class="source">Proc. ASCR Workshop on Software Productivity for Extreme-Scale Science</span>
        <span class="date">2014.</span>
        [ <a href="pdfs/Multilanguage_2014.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Michael Wilde, Justin M. Wozniak, Timothy Armstrong, Daniel S. Katz, Ian T. Foster</span>
        <span class="title"> Productive composition of extreme-scale applications using implicitly parallel dataflow</span>
	<span class="source">Proc. ASCR Workshop on Software Productivity for Extreme-Scale Science</span>
        <span class="date">2014.</span>
        [ <a href="pdfs/Dataflow_2014.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors"> Ketan Maheshwari, David Kelly, Scott J. Krieder, Justin M. Wozniak, Daniel S. Katz, Zhi-Gang Mei, Mainak Mookherjee </span>
        <span class="title">Reusability in science: From initial user engagement to dissemination of results</span>
	<span class="source">Proc. Workshop on Sustainable Software for Science: Practice and Experiences at SC</span>
        <span class="date">2013.</span>
        [ <a href="pdfs/Reusability_2013.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Ketan Maheshwari, Alex Rodriguez, David Kelly, Ravi Madduri, Justin M. Wozniak, Michael Wilde, Ian T. Foster</span>
        <span class="title">Extending the Galaxy portal with parallel and distributed execution capability </span>
	<span class="source">Proc. DataCloud </span>
        <span class="date">2013.</span>
        [ <a href="pdfs/Swift-Galaxy_2013.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors"> Ketan Maheshwari, Alex Rodriguez, David Kelly, Ravi Madduri, Justin M. Wozniak, Michael Wilde, Ian T. Foster </span>
        <span class="title">Enabling multi-task computation on Galaxy-based gateways using Swift</span>
	<span class="source">Proc. Science Gateway Institute Workshop</span>
        <span class="date">2013.</span>
        [ <a href="pdfs/Galaxy-Gateways_2013.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Justin M. Wozniak, Timothy G. Armstrong, Ketan Maheshwari, Ewing L. Lusk, Daniel S. Katz, Michael Wilde, Ian T. Foster</span>
        <span class="title">Turbine: A distributed-memory dataflow engine for high performance many-task applications. </span>
	<span class="source">Fundamenta Informaticae 128(3) </span>
        <span class="date">2013.</span>
        [ <a href="pdfs/Turbine_2013.pdf">pdf</a> ]
</div>


<div class="publication">
	<span class="authors">Justin M. Wozniak, Tom Peterka, Timothy G. Armstrong, James Dinan, Ewing Lusk, Michael Wilde, Ian T. Foster</span>
        <span class="title">Dataflow coordination of data-parallel tasks via MPI 3.0 </span>
	<span class="source">Proc. EuroMPI</span>
        <span class="date">2013.</span>
        [ <a href="pdfs/Swift_MPI_2013.pdf">pdf</a> ]
</div>


<div class="publication">
	<span class="authors"> Justin M. Wozniak, Michael Wilde, and Daniel S. Katz </span>
        <span class="title">JETS: Language and system support for many-parallel-task workflows </span>
	<span class="source"> J. Grid Computing 11(3), 2013. </span>
        <span class="date">2013.</span>
        [ <a href="pdfs/JETS_2013.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Justin M. Wozniak, Timothy G. Armstrong, Michael Wilde, Daniel Katz, Ewing Lusk, Ian T. Foster </span>
        <span class="title">Swift/T: Large-scale application composition via distributed-memory data flow processing </span>
	<span class="source">Proc CCGrid</span>
        <span class="date">2013.</span>
        [ <a href="pdfs/Swift_2013.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Ketan Maheshwari, Kenneth Birman, Justin M. Wozniak, Devin Van Zandt </span>
        <span class="title">Evaluating cloud computing techniques for smart power grid design using parallel scripting </span>
	<span class="source">Proc CCGrid</span>
        <span class="date">2013.</span>
        [ <a href="pdfs/Swift_PowerGrid_2013.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Justin M. Wozniak, Anthony Chan, Timothy G. Armstrong, Michael Wilde, Ewing Lusk, Ian T. Foster </span>
        <span class="title">A model for tracing and debugging large-scale task-parallel programs with MPE </span>
	<span class="source">Proc. Workshop on Leveraging Abstractions and Semantics in High-performance Computing at PPoPP</span>
        <span class="date">2013.</span>
        [ <a href="pdfs/Swift_MPE_2013.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Justin M. Wozniak, Timothy G. Armstrong, Michael Wilde, Ketan Maheshwari, Daniel S. Katz, Matei Ripeanu, Ewing L. Lusk, and Ian T. Foster</span>
        <span class="title">Turbine: A distributed-memory dataflow engine for extreme-scale many-task applications </span>
	<span class="source">Proc. Workshop on Scalable Workflow Enactment Engines and Technologies at SIGMOD</span>
        <span class="date">2012.</span>
        [ <a href="pdfs/Turbine_2012.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Timothy G. Armstrong, Justin M. Wozniak, Michael Wilde, Ketan Maheshwari, Daniel S. Katz, Matei Ripeanu, Ewing L. Lusk, and Ian T. Foster</span>
        <span class="title">ExM: High level dataflow programming for extreme-scale systems </span>
	<span class="source">Proc. HotPar (poster series)</span>
        <span class="date">2012.</span>
        [ <a href="pdfs/ExM_2012.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Michael Wilde, Mihael Hategan, Justin M. Wozniak, Ben Clifford, Daniel S. Katz, Ian Foster</span>
        <span class="title">Swift: A language for distributed parallel scripting</span>
	<span class="source">Parallel Computing</span>
        <span class="date">2011.</span>
        [ <a href="pdfs/SwiftLanguageForDistributedParallelScripting.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Mihael Hategan, Justin Wozniak, Ketan Maheshwari</span>
	<span class="title">Coasters: uniform resource provisioning and access for clouds and grids</span>
        <span class="source">4th IEEE/ACM International Conference on Utility and Cloud Computing</span>
        <span class="date">2011.</span>
        [ <a href="pdfs/UCC-coasters.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Michael Wilde, Ian Foster, Kamil Iskra, Pete Beckman, Zhao Zhang, Allan Espinosa, Mihael Hategan, Ben Clifford, Ioan Raicu</span>
	<span class="title">Parallel Scripting for Applications at the Petascale and Beyond</span>
	<span class="source">Computer, Vol. 42, No. 11</span>
	<span class="date">2009.</span>
	[ <a href="pdfs/SwiftParallelScripting.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Zhao Y., Hategan, M., Clifford, B., Foster, I., vonLaszewski, G., Raicu, I., Stef-Praun, T. and Wilde, M</span>
	<span class="title">Swift: Fast, Reliable, Loosely Coupled Parallel Computation</span>
	<span class="source">IEEE International Workshop on Scientific Workflows</span>
	<span class="date">2007.</span>
	[ <a href="pdfs/Swift-SWF07.pdf">pdf</a> ]
</div>

<p>&nbsp;</p>
<h3>Swift Applications</h3>

<div class="publication">
        <span class="authors">Agarwal, K., Chase, J., Schuchardt, K., Scheibe, T., Palmer, B., Elsethagen, T.</span>
        <span class="title">Design and Implementation of ‘Many Parallel Task’ Hybrid Subsurface Model</span>
        <span class="source">4th Workshop on Many-Task Computing on Grids and Supercomputers</span>
        <span class="date">2011.</span>
        [ <a href="http://datasys.cs.iit.edu/events/MTAGS11/p03.pdf">pdf</a> ]
</div>


<div class="publication">
        <span class="authors">Adhikari, A. Peng, J., Wilde, M., Xu, J., Freed, K., Sosnick, T.</span>
        <span class="title">Modeling large regions in proteins: Applications to loops, termini, and folding</span>
        <span class="source">Protein Science</span>
        <span class="date">2011.</span>
        [ <a href="pdfs/767_fta.pdf">pdf</a> ]
</div>

<div class="publication">
        <span class="authors">Boker, S., Neale, M., Maes, H., Wilde, M., Spiegel, M., Brick, T., Spies, J., Estabrook, R., Kenny, S., Bates, T., et al.</span>
        <span class="title">OpenMx: An Open Source Extended Structural Equation Modeling Framework</span>
        <span class="source">Psychometrika - Vol. 76, No.2, 306-317</span>
        <span class="date">April 2011</span>
        [ <a href="pdfs/openmx.pdf">pdf</a> ]
</div>

<div class="publication">
        <span class="authors">Woitaszek, M., Dennis, J., Sines, T.</span>
        <span class="title">Parallel High-resolution Climate Data Analysis using Swift.</span>
        <span class="source">4th Workshop on Many-Task Computing on Grids and Supercomputers</span>
        <span class="date">2011.</span>
        [ <a href="pdfs/highresclimatedata.pdf">pdf</a> ]
</div>


<div class="publication">
        <span class="authors">Uram, T., Papka, M., Hereld, M., Wilde, M.</span>
        <span class="title">A solution looking for lots of problems: Generic Portals for Science Infrastructure</span>
        <span class="source">Proceedings of the 2011 TeraGrid Conference: Extreme Digital Discovery </span>
        <span class="date">2011.</span>
        [ <a href="pdfs/tg2011_portal.pdf">pdf</a> ]
</div>

<div class="publication">
        <span class="authors">Wu, W., Uram, T., Wilde, M., Hereld, M., Papka, M.</span>
        <span class="title">Accelerating Science Gateway Development with Web 2.0 and Swift</span>
        <span class="source">TG 10 - Proceedings of the 2010 TeraGrid Conference </span>
        <span class="date">2010.</span>
        [ <a href="pdfs/sciencegateway.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Joe DeBartolo, Glen Hocky, Michael Wilde, Jinbo Xu, Karl F. Freed, and Tobin R. Sosnick</span>
	<span class="title">Protein Structure Prediction Enhanced with Evolutionary Diversity: SPEED</span>
	<span class="source">Protein Science Journal</span>
	<span class="date">Jan 2010.</span>
</div>

<div class="publication">
	<span class="authors">Andriy Fedorov, Benjamin Clifford, Simon K. Warfield, Ron Kikinis, Nikos Chrisochoides</span>
	<span class="title">Non-Rigid Registration for Image-Guided Neurosurgery on the TeraGrid: A Case Study </span>
	<span class="source">College of William and Mary Technical Report</span>
	<span class="date">2009.</span>
	[ <a href="http://www.wm.edu/as/computerscience/documents/cstechreports/WM-CS-2009-05.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Stef-Praun, T., Clifford, B., Foster, I., Hasson, U., Hategan, M., Small, S., Wilde, M and Zhao,Y.</span>
	<span class="title">Accelerating Medical Research using the Swift Workflow System</span>
	<span class="source">Health Grid</span>
	<span class="date">2007.</span>
	[ <a href="pdfs/HealthGrid-2007-VDL2Bric.submitted-revised.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Stef-Praun, T., Madeira, G., Foster, I., and Townsend, R.</span>
	<span class="title">Accelerating solution of a moral hazard problem with Swift</span>
	<span class="source">e-Social Science</span>
	<span class="date">2007.</span>
	[ <a href="pdfs/SwiftForSocialSciences-2007.pdf">pdf</a> ]
</div>

<h3>Virtual Data Language Applications</h3>
<div class="publication">
	<span class="authors">Nefedova, V., Jacob, R., Foster, I., Liu, Y., Liu, Z., Deelman, E., Mehta, G. and Vahi, K.,</span>
	<span class="title">Automating Climate Science: Large Ensemble Simulations on the TeraGrid with the GriPhyN Virtual Data System.</span>
	<span class="source">2nd IEEE International Conference on eScience and Grid Computing,</span>
	<span class="date">2006.</span>
	[ <a href="pdfs/AutomatingClimateScience.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Horn, J.V., Dobson, J., Woodward, J., Wilde, M., Zhao, Y., Voeckler, J. and Foster, I.</span>
	<span class="title">Grid-Based Computing and the Future of Neuroscience Computation.</span>
	<span class="source">Methods in Mind, MIT Press,</span>
	<span class="date">2006.</span>
</div>

<div class="publication">
        <span class="authors">Sulakhe, D., Rodriguez, A., Wilde, M., Foster, I. and Maltsev, N.,</span>
        <span class="title">Using Multiple Grid Resources for Bioinformatics Applications in GADU.</span>
        <span class="source">IEEE/ACM International Symposium on Cluster Computing and Grid,</span>
        <span class="date">2006.</span>
        [ <a href="pdfs/GridResourcesForGADU.pdf">pdf</a> ]
</div>

<div class="publication">
        <span class="authors">Sulakhe, D., Rodriguez, A., D'Souza, M., Wilde, M., Nefedova, V., Foster, I. and Maltsev, N.</span>
        <span class="title">GNARE: An Environment for Grid-Based High-Throughput Genome Analysis.</span>
        <span class="source">Journal of Clinical Monitoring and Computing.</span>
        <span class="date">2005.</span>
        [ <a href="pdfs/BioGrid2005.pdf">pdf</a> ]
</div>

<div class="publication">
        <span class="authors">Bardeen, M., Gilbert, E., Jordan, T., Nepywoda, P., Quigg, E., Wilde, M. and Zhao, Y.</span>
        <span class="title">The QuarkNet/Grid Collaborative Learning e-Lab.</span>
        <span class="source">Future Generation Computer Systems, 22 (6),</span>
        <span class="pages">700-708.</span>
        <span class="date">2005.</span>
        [ <a href="pdfs/clag_paper.pdf">pdf</a> ]
</div>

<div class="publication">
        <span class="authors">Arbree, A., Avery, P., Bourilkov, D., Cavanaugh, R., Rodriguez, J., Graham, G., Wilde, M. and Zhao, Y.,</span>
        <span class="title">Virtual Data in CMS Analysis.</span>
        <span class="source">Computing in High Energy and Nuclear Physics,</span>
        <span class="date">2003.</span>
        [ <a href="pdfs/VirtualDataInCMS.pdf">pdf</a> ]
</div>

<div class="publication">
        <span class="authors">Arbree, A., Avery, P., Bourilkov, D., Cavanaugh, R., Katageri, S., Graham, G., Rodriguez, J., Voeckler, J. and Wilde, M.,</span>
        <span class="title">Virtual Data in CMS Production.</span>
        <span class="source">Computing in High Energy and Nuclear Physics,</span>
        <span class="date">2003.</span>
        [ <a href="pdfs/VDS-CMS.pdf">pdf</a> ]
</div>

<div class="publication">
        <span class="authors">Annis, J., Zhao, Y., Voeckler, J., Wilde, M., Kent, S. and Foster, I.,</span>
        <span class="title">Applying Chimera Virtual Data Concepts to Cluster Finding in the Sloan Sky Survey.</span>
        <span class="source">SC2002, Baltimore, MD,</span>
        <span class="date">2002.</span>
        [ <a href="pdfs/SSDS-SC02.pdf">pdf</a> ]
</div>

<div class="publication">
        <span class="authors">Zhao, Y.</span>
        <span class="title">Virtual Galaxy Clusters: An Application of the GriPhyN Virtual Data Toolkit to Sloan Digital Sky Survey Data.</span>
        <span class="source">MS thesis, University of Chicago, GriPhyN-2002-06,</span>
        <span class="date">2002.</span>
</div>

<p>&nbsp;</p>
<h3>Research Leading up to Swift</h3>

<div class="publication">
	<span class="authors">Zhao, Y., Wilde, M. and Foster, I.</span>
	<span class="title">Virtual Data Language: A Typed Workflow Notation for Diversely Structured Scientific Data.</span>
	<span class="source">Taylor, I.J., Deelman, E., Gannon, D.B. and Shields, M. eds. Workflows for eScience, Springer,</span>
	<span class="date">2007.</span>
	<span class="pages">258-278.</span>
</div>

<div class="publication">
	<span class="authors">Zhao, Y., Wilde, M. and Foster, I.,</span>
	<span class="title">Applying the Virtual Data Provenance Model.</span>
	<span class="source">International Provenance and Annotation Workshop, Chicago, Illinois,</span>
	<span class="date">2006.</span>
	[ <a href="pdfs/VirtualDataProvenance.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">V&ouml;ckler, J.-S., Mehta, G., Zhao, Y., Deelman, E. and Wilde, M.,</span>
	<span class="title">Kickstarting Remote Applications.</span>
	<span class="source">2nd International Workshop on Grid Computing Environments,</span>
	<span class="date">2006.</span>
	[ <a href="pdfs/Kickstarting2006.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Zhao, Y., Dobson, J., Foster, I., Moreau, L. and Wilde, M.</span>
	<span class="title">A Notation and System for Expressing and Executing Cleanly Typed Workflows on Messy Scientific Data.</span>
	<span class="source">SIGMOD Record 34 (3)</span>
	<span class="pages">37-43</span>
        <span class="date">2005.</span>
	[ <a href="pdfs/p37-special-sw-section-6.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Moreau, L., Zhao, Y., Foster, I., Voeckler, J. and Wilde, M.,</span>
	<span class="title">XDTM: XML Data Type and Mapping for Specifying Datasets.</span>
	<span class="source">European Grid Conference,</span>
	<span class="date">2005.</span>
	[ <a href="pdfs/egc05.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Zhao, Y., Wilde, M., Foster, I., Voeckler, J., Jordan, T., Quigg, E. and Dobson, J.,</span>
	<span class="title">Grid Middleware Services for Virtual Data Discovery, Composition, and Integration.</span>
	<span class="source">2nd International Workshop on Middleware for Grid Computing,</span>
	<span class="date">2004.</span>
	[ <a href="pdfs/p57-zhao.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Foster, I., Voeckler, J., Wilde, M. and Zhao, Y.,</span>
	<span class="title">The Virtual Data Grid: A New Model and Architecture for Data-Intensive Collaboration.</span>
	<span class="source">Conference on Innovative Data Systems Research,</span>
	<span class="date">2003.</span>
	[ <a href="pdfs/ModelAndArchForDataCollab2003.pdf">pdf</a> ]
</div>


<div class="publication">
	<span class="authors">Foster, I., Voeckler, J., Wilde, M. and Zhao, Y.,</span>
	<span class="title">Chimera: A Virtual Data System for Representing, Querying, and Automating Data Derivation.</span>
	<span class="source">14th Intl. Conf. on Scientific and Statistical Database Management, Edinburgh, Scotland,</span>
	<span class="date">2002.</span>
	[ <a href="pdfs/Chimra2002.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">V&ouml;ckler, J.-S., Wilde, M. and Foster, I.</span>
	<span class="title">The GriPhyN Virtual Data System.</span>
	<span class="source">Technical Report GriPhyN-2002-02,</span>
	<span class="date">2002.</span>
</div>

<div class="publication">
	<span class="authors">Zhao, Y., Wilde, M., Foster, I., Voeckler, J., Dobson, J., Gilbert, E., Jordan, T. and Quigg, E.</span>
	<span class="title">Virtual Data Grid Middleware Services for Data-intensive Science.</span>
	<span class="source">Concurrency and Computation: Practice and Experience, 18 (6),</span>
	<span class="pages">595-608.</span>
        <span class="date">2000.</span>
	[ <a href="pdfs/cpe2000.pdf">pdf</a> ]
</div>

<p>&nbsp;</p>
<h3>Related Research</h3>

<div class="publication">
        <span class="authors">Armstrong, T.</span>
        <span class="title">Integrating Task Parallelism into the Python Programming Language</span>
        <span class="source">University of Chicago, Department of Computer Science</span>
        <span class="date">May 2011</span>
        [ <a href="pdfs/armstrong-masters.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Moreau, L. and others,</span>
	<span class="title">The First Provenance Challenge,</span>
	<span class="source">Concurrency and Computation: Practice and Experience.</span>
	<span class="date">2008.</span>
	[ <a href="pdfs/challenge-editorial.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Raicu, I., Zhao Y., Dumitrescu, C., Foster, I. and Wilde, M</span>
	<span class="title">Falkon: a Fast and Light-weight tasK executiON framework </span>
	<span class="source">Supercomputing Conference</span>
	<span class="date">2007.</span>
	[ <a href="pdfs/Falkon_SC07_v24.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">von Laszewski, G., Hategan, M. and Kodeboyina, D.</span>
	<span class="title">Java CoG Kit Workflow.</span>
	<span class="source">Taylor, I.J., Deelman, E., Gannon, D.B. and Shields, M. eds. Workflows for Science,</span>
	<span class="date">2007.</span>
	<span class="pages">340-356.</span>
	[ <a href="pdfs/vonLaszewski-workflow-book.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Meyer, L., Scheftner, D., Voeckler, J., Mattoso, M., Wilde, M. and Foster, I.,</span>
	<span class="title">An Opportunistic Algorithm for Scheduling Workflows on Grids.</span>
	<span class="source">VECPAR'06, Rio De Janiero,</span>
	<span class="date">2006.</span>
	[ <a href="pdfs/OpportunisticAlgoritmForSchedulingWokflows.pdf">pdf</a> ]
</div>

<div class="publication">
	<span class="authors">Malewicz, G., Foster, I., Rosenberg, A. and Wilde, M.,</span>
	<span class="title">A Tool for Prioritizing DAGMan Jobs and Its Evaluation.</span>
	<span class="source">IEEE International Symposium on High Performance Distributed Computing,</span>
	<span class="date">2006.</span>
	[ <a href="pdfs/jogc_03.pdf">pdf</a> ]
</div>

	<p>&nbsp;</p>
	</div>
  <!-- end .grid_9 -->
  <div class="grid_3">
  	<?php require('../inc/papers_sidebar.php') ?>
  </div>
  <!-- end .grid_3 -->

  <div class="clear"></div>

  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?>
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>

<!--
 Local Variables:
 mode: html
 End:
-->
