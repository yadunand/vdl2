#!/bin/sh

# OBSOLETE: DO NOT RUN THIS
# See swift/docs/build_docs.sh for docs management
# Do a simple svn update to update the plain HTML

# Run this script from /ci/www/projects/swift

crash()
{
  echo $1
  exit 1
}

update_guide()
{
  DIR=$1
  echo "--------- Updating guide: ${DIR} ----------"
  pushd ${DIR} || crash "Could not enter: ${DIR}"
  [[ ! -d formatting/docbook ]] && \
    ln -s ../formatting/docbook formatting/docbook
  [[ ! -d formatting/fop ]]&& \
    ln -s ../../fop formatting/fop
  svn update
  chmod -R g+w .svn 2>/dev/null
  chmod -R g+w formatting/.svn 2>/dev/null
  ./buildguides.sh
  pushd userguide
  chmod a+r *.php *.jpeg *.png
  chmod g+w *.php
  chmod g+w *.pdf
  popd
  chmod -R g+w userguide
  chmod a+rx userguide
  popd
  return 0
}

# Ensure tools are group-usable:
chmod g+rw $0
chmod g+rw updatenodocs.sh

umask 002
echo "--------- Updating www...  ----------"
svn update
chmod -R g+w .svn 2>/dev/null

find . -name .svn -prune -o -name "*.php" | xargs -n 100 chmod -f g+rw,a+r

echo "--------- Updating docs... ----------"

for SET in guides/trunk guides/release-0.91
do
  update_guide ${SET}
done

echo "---------     All done     ----------"

