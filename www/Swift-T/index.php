<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift/T - High Performance Dataflow Computing</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<!-- Google analytics -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25069254-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">
	<h1>Swift/T - High Performance Dataflow Computing</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">
<br>
<h3>Introduction</h3>

<p>
Swift/T is a completely new implementation of the Swift language for
high-performance computing.  In this implementation, the Swift script is
translated into an MPI program that uses the Turbine (hence, /T) and
<a href="http://www.mcs.anl.gov/project/adlb-asynchronous-dynamic-load-balancer">ADLB</a>
runtime libraries for highly scalable dataflow processing over MPI,
without single-node bottlenecks.
</p>

<center>
  <div class="button_175"
       title="Entry point for Swift/T language and runtime guides">
    <a href="guide.html">User Guide</a></div>
  <div class="button_175"
       title="Plenty of useful examples for easy parallel programming">
    <a href="gallery.html">Gallery</a></div>
  <div class="button_175"
       title="Swift/T installation packages and prior versions">
    <a href="downloads.html">Downloads</a></div>
</center>

<h3>Language Overview</h3>

<p>
Swift is a naturally concurrent language with C-like syntax.  It is
primarily used to manage calls to leaf tasks- external functions
written in C, C++, Fortran, Python, R, Tcl, Julia, Qt Script, or
executable programs.  The Swift language coordinates the distribution
of data to these tasks and schedules them for concurrent execution
across the nodes of a large system.
</p>

<p>
Swift has conventional programming structures- functions, if, for,
arrays, etc.  Some functions are connected to leaf tasks, which are
expected to do the bulk of the computationally intensive work.  Tasks
run when their input data is available.  Data produced by a task may
be fed into the next using syntax like:
</p>

<div class="code">y = f(x);
z = g(y);
</div>

<p>
If tasks are able to run concurrently, they do: in this, case, two
executions of <tt>g()</tt> run concurrently.
</p>

<div class="code">y  = f(x);
z1 = g(y, 1);
z2 = g(y, 2);
</div>

<p>
Swift loops and other features provide additional concurrency
constructs. Data movement is implicitly performed over MPI.
</p>

<p>
The Swift compiler, STC, breaks a Swift script into leaf tasks and
control tasks.  These are managed at runtime by the scalable,
distributed-memory runtime consisting of Turbine and ADLB.  For
example, the following code is broken up into the task diagram:
</p>

<style>
table
{
  border: 1px solid #999;
  margin-left:  auto;
  margin-right: auto;
}
</style>

<img src="images/spawngraph.png" width="45%" style="float: right;  border: 1px solid #999; "/>

<div class="code" style="width:50%; z-index:-1;">int X = 100, Y = 100;
int A[][];
int B[];
foreach x in [0:X-1] {
  foreach y in [0:Y-1] {
    if (check(x, y)) {
      A[x][y] = g(f(x), f(y));
    } else {
      A[x][y] = 0;
    }
  }
  B[x] = sum(A[x]);
}
</div>


<h3>Differences from Swift/K</h3>

<p>
The previous implementation of the Swift language is Swift/K, which
runs on the Karajan grid workflow engine (hence, /K). Karajan and its
libraries excel at wide-area computation, exploiting diverse
schedulers (PBS, Condor, etc.) and data transfer technologies.
</p>

<style>
ul
{
  list-style-type: circle;
  margin-left:50px;
}
</style>

<ul>
  <li>
    Swift/K is designed to execute a workflow of program executions
    across wide area resources.
  </li>
  <li>
    Swift/T is designed to translate a Swift script into an MPI
    program composing native code libraries for execution on a single
    large system.
  </li>
</ul>

<h3>Migration to Swift/T</h3>

<p>
  Swift/T is able to execute Swift/K-style app functions and is a
  natural migration from Swift/K.  Key differences include:
</p>

<ul>
  <li>
    Enhanced performance: 1.5 billion tasks/s
  </li>
  <li>
    Ability to call native code functions (C, C++, Fortran)
  </li>
  <li>
    Ability to execute scripts in embedded interpreters (Python, R, Tcl, Julia, etc.)
  </li>
  <li>
    Enhanced builtin libraries (string, math, system, etc.)
  </li>
  <li>
    Runs as a single MPI job
  </li>
</ul>

<p>&nbsp;</p>
</div>

  <div class="clear"></div>

  </div>
<!-- end .container_12 -->

<!-- footer -->
<?php require('../inc/footer2.php') ?>
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>

</body>
</html>

<!--
 Local Variables:
 mode: html
 End:
-->
