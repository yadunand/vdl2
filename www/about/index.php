<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Swift - About</title>
<link href="../css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style3.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://use.typekit.com/dbf2lqy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<!-- Google analytics -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25069254-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>

<body>
<div class="container_12">
	<!-- header -->
	<?php require('../inc/header2.php') ?>
	<!-- end header -->
<div class="grid_12 page-header">				
	<h1>About</h1>
</div>
<div class="clear"></div>
<!-- end .grid_12 -->

 <div class="grid_9">
<p>
The Swift parallel scripting language enables scientists, engineers,
and data analysts to express and coordinate parallel invocations of
application programs on distributed and parallel computing platforms:
one of the dominant modes of performing computation in science and
engineering.
</p>
<p>
Swift has been fruitfully employed in many diverse domains, including
biochemistry, neuroscience, climate, earthquake
simulation, hydrology, energy, economics, social network analysis,
mass media analysis, materials science, and astronomy.  It runs on a
variety of platforms, and enables users to move their application
scripts between them with relative ease.  As personal computers become
increasingly parallel, Swift enables users to leverage this parallel
power with little or no experience in parallel programming. And Swift
opens up complex cyberinfrastructure like the Open Science Grid,
TeraGrid/XSEDE and FutureGrid to a wide range and scale of scientific
user communities, broadening participation in high performance
computing.
</p>
<p>
Swift comprises a programming model, scripting language, and runtime
engine.  Its implicitly parallel programming model allows users with
minimal programming expertise to transparently utilize parallel and
distributed systems. The scripting language is minimal and standalone,
The Swift programming model has also been embedded into the highly
popular R language for data analysis (via the SwiftR package), and has
been experimentally embedded into Python via the PyDFlow package.
</p>
<p>
Swift has been actively engaged in training and education in parallel
computing.  From its inception, Swift has been leveraged as a base to
broaden participation in scientific computing, and has created
opportunities for, and leveraged the skills and contributions of
numerous graduate and undergraduate students.
Swift provides the parallel computing base for the secondary education
project "I2U2: Interactions in Understanding the Universe,"
(http://www.i2u2.org) which teaches the concepts of parallel and
distributed computing to our next generation of scientists at the high
school level.
</p>
<p> Swift incorporates several existing toolkits. </p> 

<h4>Globus Toolkit</h4>
<p>Swift uses the <a href="http://www.globus.org/" target="_blank">Globus Toolkit</a> as middleware to talk to various resources.</p>

<h4>Java CoG Kit</h4>
<p>The <a href="http://wiki.cogkit.org/index.php/Main_Page" target="_blank">CoG Kit</a> provides a high level uniform interface to different versions of the Globus Toolkit. It also contains the Karajan language and interpreter which is used as an underlying execution engine for Swift.</p>

<!--
<h4>TeraGrid</h4>
<p>
"<a href="http://www.teragrid.org/">TeraGrid</a> is an open scientific discovery infrastructure combining leadership class resources at nine partner sites to create an integrated, persistent computational resource."</p>
	<p>&nbsp;</p>
-->
	</div>
  <!-- end .grid_9 -->
  <div class="grid_3">
  	<?php require('../inc/about_sidebar.php') ?>
    <p>&nbsp;</p>
  </div>
  <!-- end .grid_3 -->
  
  <div class="clear"></div>
				
  </div>
<!-- end .container_12 -->
<!-- footer -->
<?php require('../inc/footer2.php') ?> 
<!-- end footer -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-106257-5");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
