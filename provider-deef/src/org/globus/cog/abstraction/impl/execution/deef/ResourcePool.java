package org.globus.cog.abstraction.impl.execution.deef;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import org.globus.cog.abstraction.interfaces.Task;
import org.globus.cog.abstraction.interfaces.Status;

import org.apache.axis.message.addressing.Address;
import org.apache.axis.message.addressing.EndpointReferenceType;

import org.globus.GenericPortal.stubs.Factory.CreateResource;
import org.globus.GenericPortal.stubs.Factory.CreateResourceResponse;
import org.globus.GenericPortal.stubs.Factory.FactoryPortType;
import org.globus.GenericPortal.stubs.Factory.service.FactoryServiceAddressingLocator;

import org.globus.GenericPortal.stubs.GPService_instance.UserJob;
import org.globus.GenericPortal.stubs.GPService_instance.UserJobResponse;
import org.globus.GenericPortal.stubs.GPService_instance.InitResponse;
import org.globus.GenericPortal.stubs.GPService_instance.Init;
import org.globus.GenericPortal.stubs.GPService_instance.DeInitResponse;
import org.globus.GenericPortal.stubs.GPService_instance.DeInit;
import org.globus.GenericPortal.stubs.GPService_instance.GPPortType;
import org.globus.GenericPortal.stubs.GPService_instance.Executable;
import org.globus.GenericPortal.stubs.GPService_instance.service.GPServiceAddressingLocator;
import org.oasis.wsrf.lifetime.WSResourceLifetimeServiceAddressingLocator;
import org.oasis.wsrf.lifetime.ImmediateResourceTermination;
import org.globus.GenericPortal.common.Notification;

import org.globus.cog.abstraction.impl.common.task.InvalidServiceContactException;
import org.globus.cog.abstraction.impl.common.task.TaskSubmissionException;

import org.globus.common.CoGProperties;


public class ResourcePool {
    static Logger logger = Logger.getLogger(ResourcePool.class.getName());
    private EndpointReferenceType factoryEPR = null;
    private ArrayList eprPool = null;
    private ArrayList gptPool = null;
    private List execQueue = null;
    private List completedQueue = null;

    private FactoryPortType gpFactory = null;
    private String server = "";
    private int num = 1;
    private static int crtResource = 0;
    // private static ResourcePool rp = null;
    private static HashMap rpmap = null;
    private Map tasks;
    private Notification userNot = null;
    private NotificationThread notThread;
    private SubmissionThread subThread;
    private StatusThread statThread;
    private String machID;
    private Executable[] execs = null;
    private UserJob job = null;

    public static synchronized ResourcePool instance(String server, int num) throws InvalidServiceContactException {
        ResourcePool rp = null;

        if( rpmap == null )
            rpmap = new HashMap();
        rp = (ResourcePool) rpmap.get(server);
	if (rp == null) {
	    rp = new ResourcePool();
            rpmap.put(server, rp);
	    rp.server = server;
	    rp.num = num;
	    rp.tasks = Collections.synchronizedMap(new HashMap());
	    int SO_TIMEOUT = 0;
	    //rp.userNot = new Notification(SO_TIMEOUT, logger.isDebugEnabled());
            rp.userNot = new Notification(SO_TIMEOUT, true);
	    logger.debug("Notification initialized on port: " + rp.userNot.recvPort);
	    rp.machID = getMachNamePort(rp.userNot);
	    rp.execQueue = Collections.synchronizedList(new LinkedList());
	    rp.completedQueue = Collections.synchronizedList(new LinkedList());
	    rp.subThread = new SubmissionThread(rp);
	    rp.notThread = new NotificationThread(rp.tasks, rp.userNot, rp.completedQueue);
	    rp.statThread = new StatusThread(rp.tasks, rp.completedQueue);
	    rp.createResources(server, num);

	    /*
	    String userID = "127.0.0.1";
	    try {
		userID = java.net.InetAddress.getLocalHost().getHostAddress();
	    } catch (Exception e) {
		logger.error("ERROR: java.net.InetAddress.getLocalHost().getHostName() failed " + e);
	    }
	    rp.job = new UserJob();
	    rp.job.setUserID(userID);
	    rp.job.setPassword("");
	    rp.execs = new Executable[1];
	    rp.job.setExecutables(rp.execs);
	    */

	    Thread sThread = new Thread(rp.subThread);
	    Thread nThread = new Thread(rp.notThread);
	    Thread tThread = new Thread(rp.statThread);
	    logger.debug("Start the submission thread");
	    sThread.start();
	    logger.debug("Start the notification thread");
	    nThread.start();
	    logger.debug("Start the status thread");
	    tThread.start();
	}
	//TODO: check server and num to match
	return rp;
    }

    private void createResources(String server, int num) throws InvalidServiceContactException {
        try {

	    FactoryServiceAddressingLocator factoryLocator = new FactoryServiceAddressingLocator();

	    // Get factory portType
	    if (gpFactory == null) {
	        factoryEPR = new EndpointReferenceType();
	        factoryEPR.setAddress(new Address(server));

		gpFactory = factoryLocator.getFactoryPortTypePort(factoryEPR);

	    }

	    if (eprPool != null)
		return;

	    eprPool = new ArrayList();
	    gptPool = new ArrayList();

	    int i = 0;
	    while (i < num) {
		// Create resource and get endpoint reference of WS-Resource
		CreateResourceResponse createResponse = gpFactory
		    .createResource(new CreateResource());

		EndpointReferenceType instanceEPR = createResponse.getEndpointReference();

		logger.debug("Resource created succesful! " + instanceEPR);

		GPServiceAddressingLocator instanceLocator = new GPServiceAddressingLocator();
		GPPortType gGP = instanceLocator.getGPPortTypePort(instanceEPR);

		Init initMsg = new Init();
		initMsg.setValid(true);
		InitResponse ir = gGP.init(initMsg);

		if (ir.isValid()) {
		    logger.debug("Initialized FalkonWS.");
		} else {
		    logger.error("Failed to initialize FalkonWS.");
		    throw new RuntimeException("Failed to initialize FalkonWS.");
		}

		eprPool.add(instanceEPR);
		gptPool.add(gGP);
		i++;
	    }

	    // auto-destroy, should we forget it, or die in an orderly fashion
	    Runtime.getRuntime().addShutdownHook( new Thread() {
		public void run() {
		    cleanup();
	        }
	    });

         } catch (Exception e) {
	     throw new InvalidServiceContactException("Invalid service contact: ", e);
         }
    }

    public synchronized EndpointReferenceType getNextResource() {
	int next = crtResource;
	crtResource = (crtResource + 1) % num;
	return (EndpointReferenceType) eprPool.get(next);
    }

    public synchronized GPPortType getNextResourcePort() {
	int next = crtResource;
	crtResource = (crtResource + 1) % num;
	return (GPPortType) gptPool.get(next);
    }

    public Task removeTask(String jobID) {
	return (Task) tasks.remove(jobID);
    }

    public void submit(Task task, Executable exec) {
	if (exec == null)
	    return;
	exec.setNotification(machID);

	String jobID = exec.getId();
	//synchronized (tasks) {
	    tasks.put(jobID, task);
	    execQueue.add(exec);
	//}

	/*
	execs[0] = exec;
	try {
	    GPPortType gGP = rp.getNextResourcePort();
	    UserJobResponse jobRP = gGP.userJob(job);
	    if (!jobRP.isValid()) {
		task.setStatus(Status.FAILED);
		//throw new RuntimeException("submission failed");
	    }
	} catch (Exception e) {
	    task.setStatus(Status.FAILED);
	}
	*/
    }

    public static String getMachNamePort(Notification userNot){
        //String machIP = VDL2Config.getIP();
	String machIP = CoGProperties.getDefault().getIPAddress();
        String machNamePort = new String (machIP +  ":" + userNot.recvPort);
        logger.debug("WORKER: Machine ID = " + machNamePort);
	return machNamePort;
    }

    public int getExecQueueSize() {
	return execQueue.size();
    }

    public Executable removeFirstExec() throws NoSuchElementException {
	return (Executable) execQueue.remove(0);
    }

    public void cleanup()
	{
	    logger.info("Destroying remote service instance... dummy function, this doesn't really do anything...");
	}


    public void cleanup_old() {
        logger.debug("Destroying remote service instance");

        try {
	    userNot.destroy();
	    notThread.setExit(true);
	    subThread.setExit(true);
	    statThread.setExit(true);
            DeInit deInit = new DeInit();
            deInit.setValid(true);
	    for (int i=0; i < num; i++) {
		EndpointReferenceType instanceEPR = (EndpointReferenceType)eprPool.get(i);

		GPPortType gGP = (GPPortType)gptPool.get(i);

		DeInitResponse dr = gGP.deInit(deInit);
		if (dr.isValid()) {
		    logger.debug("DeInit GenericPortalWS");
		} else {
		    logger.debug("Failed to deInit GenericPortalWS");
		}
		WSResourceLifetimeServiceAddressingLocator locator =
		    new WSResourceLifetimeServiceAddressingLocator();
		ImmediateResourceTermination port =
		    locator.getImmediateResourceTerminationPort(instanceEPR);
		port.destroy(new org.oasis.wsrf.lifetime.Destroy());
	    }
        } catch (Exception e) {
            logger.warn("Unable to destroy remote service");
        }
    }
}
    
