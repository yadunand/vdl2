/*
 * Created on Jul 23, 2004
 */
package org.globus.cog.abstraction.impl.execution.deef;

import java.io.File;

import org.apache.axis.AxisProperties;
import org.apache.axis.configuration.EngineConfigurationFactoryDefault;
import org.apache.log4j.Logger;
import org.globus.cog.abstraction.impl.common.AbstractionProperties;
import org.globus.common.CoGProperties;
import org.globus.wsrf.config.ContainerConfig;

public class Boot {

	private static Logger logger = Logger.getLogger(Boot.class);

	public static void boot() {
		logger.debug("Booting Falkon");
		org.globus.cog.abstraction.impl.execution.gt4_0_0.Boot.boot();
	}
}
