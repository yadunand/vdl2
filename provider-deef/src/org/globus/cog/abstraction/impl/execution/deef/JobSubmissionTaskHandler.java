// ----------------------------------------------------------------------
// This code is developed as part of the Java CoG Kit project
// The terms of the license can be found at http://www.cogkit.org/license
// This message may not be removed or altered.
// ----------------------------------------------------------------------

package org.globus.cog.abstraction.impl.execution.deef;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import org.apache.axis.message.addressing.EndpointReferenceType;
import org.apache.log4j.Logger;
import org.globus.GenericPortal.stubs.GPService_instance.DataFiles;
import org.globus.GenericPortal.stubs.GPService_instance.DeInit;
import org.globus.GenericPortal.stubs.GPService_instance.DeInitResponse;
import org.globus.GenericPortal.stubs.GPService_instance.Executable;
import org.globus.GenericPortal.stubs.GPService_instance.GPPortType;
import org.globus.cog.abstraction.impl.common.StatusImpl;
import org.globus.cog.abstraction.impl.common.task.IllegalSpecException;
import org.globus.cog.abstraction.impl.common.task.InvalidSecurityContextException;
import org.globus.cog.abstraction.impl.common.task.InvalidServiceContactException;
import org.globus.cog.abstraction.impl.common.task.TaskSubmissionException;
import org.globus.cog.abstraction.interfaces.DelegatedTaskHandler;
import org.globus.cog.abstraction.interfaces.JobSpecification;
import org.globus.cog.abstraction.interfaces.ServiceContact;
import org.globus.cog.abstraction.interfaces.Status;
import org.globus.cog.abstraction.interfaces.Task;
import org.oasis.wsrf.lifetime.ImmediateResourceTermination;
import org.oasis.wsrf.lifetime.WSResourceLifetimeServiceAddressingLocator;


//import org.globus.GenericPortal.clients.GPService_instance.*;

public class JobSubmissionTaskHandler implements DelegatedTaskHandler
{
    static Logger logger = Logger.getLogger(JobSubmissionTaskHandler.class
                                            .getName());
    private Task task = null;

    private ResourcePool resourcePool = null;

    private EndpointReferenceType factoryEPR = null;
    private EndpointReferenceType instanceEPR = null;
    private GPPortType gGP = null;

    //private Notification userNot;

    //private String machID;

    private static int counter = 0;

    public void submit(Task task) throws IllegalSpecException,
    InvalidSecurityContextException, InvalidServiceContactException,
    TaskSubmissionException {
        if (this.task != null)
        {
            throw new TaskSubmissionException(
                                             "JobSubmissionTaskHandler cannot handle two active jobs simultaneously");
        } else
        {
            this.task = task;
            int st = this.task.getStatus().getStatusCode();
            if (st == Status.UNSUBMITTED)
            {
                doSubmit();
            } else
            {
                throw new TaskSubmissionException(
                                                 "Cannot submit COMPLETED, FAILED, or CANCELED tasks");
            }
        }
    }


    private void doSubmit() throws TaskSubmissionException,
    IllegalSpecException, InvalidSecurityContextException,
    InvalidServiceContactException {
        JobSpecification spec;
        try
        {
            spec = (JobSpecification) this.task.getSpecification();
        } catch (Exception e)
        {
            throw new IllegalSpecException(
                                          "Exception while retreiving Job Specification", e);
        }

        ServiceContact serviceContact = this.task.getService(0)
                                        .getServiceContact();

        String server = serviceContact.getContact();

        logger.debug("server:" + server);

        try
        {

            /*
            FactoryServiceAddressingLocator factoryLocator = new FactoryServiceAddressingLocator();
    
            FactoryPortType gpFactory;
    
            // Get factory portType
            factoryEPR = new EndpointReferenceType();
            factoryEPR.setAddress(new Address(server));
    
            gpFactory = factoryLocator.getFactoryPortTypePort(factoryEPR);
    
            // Create resource and get endpoint reference of WS-Resource
            CreateResourceResponse createResponse = gpFactory
            .createResource(new CreateResource());
    
            instanceEPR = createResponse.getEndpointReference();
    
            logger.debug("Resource created succesful! " + instanceEPR);
    
            GPServiceAddressingLocator instanceLocator = new GPServiceAddressingLocator();
    
            gGP = instanceLocator.getGPPortTypePort(instanceEPR);
    
            Init initMsg = new Init();
            initMsg.setValid(true);
            InitResponse ir = gGP.init(initMsg);
    
            if (ir.isValid()) {
                    logger.debug("Initialized FalkonWS.");
                } else {
                    logger.error("Failed to initialize FalkonWS.");
            throw new RuntimeException("Failed to initialize FalkonWS.");
                }
    
            */

            resourcePool = ResourcePool.instance(server, 1);

            //gGP = resourcePool.getNextResourcePort();
            //logger.debug("Got resource from resource pool: " + gGP);

        } catch (Exception e)
        {
            this.task.setStatus(Status.FAILED);
            throw new InvalidServiceContactException("Invalid service contact: ", e);
        }

        //UserJob job = prepareSpecification(spec);
        Executable job = prepareSpecification(spec);

        /*
        String userID = "127.0.0.1";
            try {
            userID = java.net.InetAddress.getLocalHost().getHostAddress();
            } catch (Exception e) {
            logger.error("ERROR: java.net.InetAddress.getLocalHost().getHostName() failed " + e);
        }
    
        job.setUserID(userID);
        job.setPassword("");
        */

        try
        {
            resourcePool.submit(this.task, job);
            this.task.setStatus(Status.SUBMITTED);
            logger.info("Job submitted");
            if (spec.isBatchJob())
            {
                this.task.setStatus(Status.COMPLETED);
            }
        } catch (Exception e)
        {
            this.task.setStatus(Status.FAILED);
            throw new TaskSubmissionException("submisson to GPWS failed." + e);
        }

    }

    public void suspend() throws InvalidSecurityContextException,
    TaskSubmissionException {
        // not implemented yet
    }

    public void resume() throws InvalidSecurityContextException,
    TaskSubmissionException {
        // not implemented yet
    }
    
    public void cancel() throws InvalidSecurityContextException,
    TaskSubmissionException {
        cancel(null);
    }

    public void cancel(String message) throws InvalidSecurityContextException,
    TaskSubmissionException {
        try
        {
            this.task.setStatus(new StatusImpl(Status.CANCELED, message, null));
            //cleanup();
        } catch (Exception e)
        {
            //cleanup();
            throw new TaskSubmissionException("Cannot cancel job", e);
        }
    }


    private int getMaxWallTime(JobSpecification spec) {

        String s = (String)spec.getAttribute("maxwalltime");
        if (s != null)
        {
            return timeToSeconds(s);
        }
        else
            return 0;
        
    }

    /**
     * Valid times formats: Minutes, Hours:Minutes, Hours:Minutes:Seconds
     */
    public static int timeToSeconds(String time) {
        String[] s = time.split(":");
        try
        {
            if (s.length == 1)
            {
                return 60 * Integer.parseInt(s[0]);
            } else if (s.length == 2)
            {
                return 60 * Integer.parseInt(s[1]) + 3600 * Integer.parseInt(s[0]);
            } else if (s.length == 3)
            {
                return Integer.parseInt(s[2]) + 60 * Integer.parseInt(s[1]) + 3600
                * Integer.parseInt(s[0]);
            }
        } catch (NumberFormatException e)
        {
        }
        throw new IllegalArgumentException("Invalid time specification: " + time);
    }

    private Executable prepareSpecification(JobSpecification spec)
    throws IllegalSpecException, TaskSubmissionException {
        // if the job specification is explicitly specified
        //UserJob job = new UserJob();

        Executable exec = new Executable();

        if (spec.getDirectory() != null)
        {
            logger.debug("directory: " + spec.getDirectory());
            exec.setDirectory(spec.getDirectory());
        }
        /*
            if (spec.getAttribute("count") != null) {
                job.setCount(new PositiveInteger(spec.getAttribute("count")
                        .toString()));
            }
        
            if (spec.getStdInput() != null) {
                job.setStdin(spec.getStdInput());
            }
    
        
            if (spec.getAttribute("jobType") != null) {
                job.setJobType(JobTypeEnumeration.fromString(spec.getAttribute(
                        "jobType").toString()));
            }
    
            if (spec.getAttribute("maxWallTime") != null) {
                job.setMaxWallTime(new Long(spec.getAttribute("maxWallTime")
                        .toString()));
            }
        */

        Vector v = spec.getArgumentsAsVector();
        exec.setArguements((String[]) v.toArray(new String[0]));

        boolean batchJob = spec.isBatchJob();
        if (spec.isRedirected())
        {
            throw new IllegalSpecException(
                                          "The Falkon provider does not support redirection");
        }
        boolean localExecutable = spec.isLocalExecutable();

        if (localExecutable)
        {
            throw new IllegalSpecException(
                                          "The Falkon provider does not support file staging");
        }

        exec.setCommand(spec.getExecutable());
        logger.debug("executable: " + spec.getExecutable());

        //exec.setStdout(spec.getStdOutput());
        //exec.setStderr(spec.getStdError());

        Collection names = spec.getEnvironmentVariableNames();
        if (names.size() == 0)
        {
            exec.setEnvironment(null);
        } else
        {
            String[] envp = new String[names.size()];
            Iterator in = names.iterator();
            int index = 0;
            while (in.hasNext())
            {
                String name = (String) in.next();
                envp[index++] = name + "=" + spec.getEnvironmentVariable(name);
            }
            exec.setEnvironment(envp);
        }

        exec.setId(String.valueOf(this.task.getIdentity()));

        //TODO
        //should be set to the maximum walltime for the specific task... 0 means infinite
        try
        {
            exec.setWallTime(getMaxWallTime(spec)*1000);
        } catch (Exception e)
        {
            if (logger.isDebugEnabled()) e.printStackTrace();
            exec.setWallTime(0);
        }

        //TODO
        //Data Caching specific things...


        /*
        <xsd:complexType name="Executable">
            <xsd:sequence>
                <xsd:element name="id" type="xsd:string"
                    minOccurs="0" maxOccurs="1"/>     
                <xsd:element name="notification" type="xsd:string"
                    minOccurs="0" maxOccurs="1"/>     
                <xsd:element name="command" type="xsd:string"/>
                <xsd:element name="arguements" type="xsd:string"
                 minOccurs="0" maxOccurs="unbounded"/>
                <xsd:element name="environment" type="xsd:string"
                 minOccurs="0" maxOccurs="unbounded"/>
                <xsd:element name="directory" type="xsd:string"/>
                <xsd:element name="wallTime" type="xsd:int"/>
                <xsd:element name="dataCaching" type="xsd:boolean"/>
                <xsd:element name="inputData" type="tns:DataFiles"/>
                <xsd:element name="outputData" type="tns:DataFiles"/>
            </xsd:sequence>
        </xsd:complexType>

        <xsd:complexType name="DataCache">
            <xsd:sequence>
                <xsd:element name="numCaches" type="xsd:int"/>
                <xsd:element name="cacheLocation" type="xsd:string"
                 minOccurs="0" maxOccurs="unbounded"/>
            </xsd:sequence>
        </xsd:complexType>

        <xsd:complexType name="DataFiles">
            <xsd:sequence>
                <xsd:element name="logicalName" type="xsd:string"
                 minOccurs="0" maxOccurs="unbounded"/>
                <xsd:element name="fileURL" type="xsd:string"
                 minOccurs="0" maxOccurs="unbounded"/>
                <xsd:element name="fileSize" type="xsd:int"
                 minOccurs="0" maxOccurs="unbounded"/>
                <xsd:element name="dataCache" type="tns:DataCache"
                 minOccurs="0" maxOccurs="unbounded"/>
            </xsd:sequence>
        </xsd:complexType>
        */


        //TODO
        //should be set to true if data caching is to be enabled and handled by Falkon
        //this should probably be set from a config file...
        exec.setDataCaching(false);

        //TODO
        if (exec.isDataCaching())
        {

            DataFiles inputData = new DataFiles();
            //this is an array of logical names of the input files... in theory it could be any ASCII strings
            inputData.setLogicalName(null);
            //this is an array of  URL from which the above files can be retrieved from... 
            //for now, only shared file system file paths and HTTP servers are supported, will support FTP and gridFTP server in the future...  
            //this could be set to null if the file is nowhere on persistent disk (shared file system, ftp server, etc... and the files are only in the Falkon cache)
            inputData.setFileURL(null);
            //this is not used at the moment, as the size is discovered dynamically when needed, but could help out the scheduler (which does not attempt to discover the file size) to make better decissions...
            inputData.setFileSize(null);
            //this should be set to null, as Falkon uses this to keep track of where all the caches are... this could be used at the completion of a task, for the end client to retrieve data from the Falkon cache, if it was not marked to be staged back
            inputData.setDataCache(null);
            exec.setInputData(inputData);

            DataFiles outputData = new DataFiles();
            //same as input above
            outputData.setLogicalName(null);
            //if the output file is to be persisted, the URL (shared file system path) should be given...
            //if this is not specified and a null value is given, then the output will remain transient data (which could be deleted at any point in time without warning by Falkon)
            outputData.setFileURL(null);
            //same as input above
            outputData.setFileSize(null);
            //same as input above
            outputData.setDataCache(null);
            exec.setOutputData(outputData);

        }

        //exec.setNotification(machID);

        //Executable execs[] = new Executable[1];
        //execs[0] = exec;
        //job.setExecutables(execs);

        return exec;
    }

    private void cleanup() {
        logger.debug("Destroying remote service for task "
                     + this.task.getIdentity().toString());
        try
        {
            DeInit deInit = new DeInit();
            deInit.setValid(true);
            DeInitResponse dr = this.gGP.deInit(deInit);
            if (dr.isValid())
            {
                logger.debug("DeInit GenericPortalWS");
            } else
            {
                logger.debug("Failed to deInit GenericPortalWS");
            }

            WSResourceLifetimeServiceAddressingLocator locator =
            new WSResourceLifetimeServiceAddressingLocator();
            ImmediateResourceTermination port;
            port = locator.getImmediateResourceTerminationPort(instanceEPR);
            port.destroy(new org.oasis.wsrf.lifetime.Destroy());
        } catch (Exception e)
        {
            logger.warn("Unable to destroy remote service for task "
                        + this.task.getIdentity().toString(), e);
        }
    }

}
