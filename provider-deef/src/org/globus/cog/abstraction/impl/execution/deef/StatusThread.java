//----------------------------------------------------------------------
//This code is developed as part of the Java CoG Kit project
//The terms of the license can be found at http://www.cogkit.org/license
//This message may not be removed or altered.
//----------------------------------------------------------------------

package org.globus.cog.abstraction.impl.execution.deef;

import java.util.Map;
import java.util.List;

import org.apache.log4j.Logger;
import org.globus.cog.abstraction.interfaces.Status;
import org.globus.cog.abstraction.interfaces.Task;

import org.globus.GenericPortal.stubs.GPService_instance.GPPortType;
import org.globus.GenericPortal.stubs.GPService_instance.Executable;
import org.globus.GenericPortal.stubs.GPService_instance.UserJob;
import org.globus.GenericPortal.stubs.GPService_instance.UserJobResponse;

public class StatusThread implements Runnable {
    static Logger logger = Logger.getLogger(StatusThread.class
					    .getName());

    private Map tasks;
    private List completedQueue;
    private boolean exit = false;

    private final int POLL_STEP = 100;

    public StatusThread(Map tasks, List completedQueue){
	this.tasks = tasks;
	this.completedQueue = completedQueue;
    }
    
    public void setExit(boolean exit) {
	this.exit = exit;
    }

    public void run() {
        while(!exit) {
            try {
		if (completedQueue.size() > 0) {
		    String res=(String)completedQueue.remove(0);

		    String[] toks = res.split(" ");
		    if (toks.length == 2) {
			String jobID = toks[0];
			Task task = null;
			//synchronized (tasks) {
			    task = (Task) tasks.remove(jobID);
			//}
			if (task == null) {
			    logger.warn("Could not find task for jobID "+jobID);
			    //missing in the map? ignore
			    continue;
			}
			logger.info("Found task for jobID "+jobID);
			if (toks[1].equals("0")) {
			    task.setStatus(Status.COMPLETED);
			} else{
			    task.setStatus(Status.FAILED);
			}
		    }
		} else {
		    Thread.sleep(POLL_STEP);
		}
            } catch (Exception e) {
		logger.debug("Error removing tasks",e);
		// e.printStackTrace();
		//exit = true;
            } 
        }
    }
}
