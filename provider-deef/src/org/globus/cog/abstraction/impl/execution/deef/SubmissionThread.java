//----------------------------------------------------------------------
//This code is developed as part of the Java CoG Kit project
//The terms of the license can be found at http://www.cogkit.org/license
//This message may not be removed or altered.
//----------------------------------------------------------------------

package org.globus.cog.abstraction.impl.execution.deef;

import java.util.HashMap;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.globus.cog.abstraction.interfaces.Status;
import org.globus.cog.abstraction.interfaces.Task;

import org.globus.GenericPortal.stubs.GPService_instance.GPPortType;
import org.globus.GenericPortal.stubs.GPService_instance.Executable;
import org.globus.GenericPortal.stubs.GPService_instance.UserJob;
import org.globus.GenericPortal.stubs.GPService_instance.UserJobResponse;

public class SubmissionThread implements Runnable {
    static Logger logger = Logger.getLogger(SubmissionThread.class
					    .getName());
    ResourcePool rp;
    //LinkedList execQueue;
    private Executable[] execs = null;
    private UserJob job = null;

    private boolean exit = false;

    private final int POLL_STEP = 200;
    private final int MAX_EXEC_NUM = 20;

    public SubmissionThread(ResourcePool rp){
	this.rp = rp;
	//this.execQueue = execQueue;
	String userID = "127.0.0.1";
	try {
	    userID = java.net.InetAddress.getLocalHost().getHostAddress();
            logger.debug("Falkon:userID: " + userID);
	} catch (Exception e) {
            logger.error("ERROR: java.net.InetAddress.getLocalHost().getHostName() failed " + e);
	}
	job = new UserJob();
	job.setUserID(userID);
	job.setPassword("");
    }
    
    public void setExit(boolean exit) {
	this.exit = exit;
    }

    public void run() {
        while(!exit) {
            try {
		int size = rp.getExecQueueSize();
		if (size == 0) {
		    Thread.sleep(POLL_STEP);
		    continue;
		}

		int num_execs = Math.min(size, MAX_EXEC_NUM);
		logger.debug(num_execs + " jobs bundled");
                logger.debug("Falkon: " + num_execs + " jobs bundled...");
		int i = num_execs;
		while ( i <= size ) {
		    execs = new Executable[num_execs];
		    for (int j=0; j<num_execs; j++) {
			execs[j] = rp.removeFirstExec();
			logger.debug("submitting " + execs[j].getId() + " to Falkon.");
		    }
		    job.setExecutables(execs);

		    GPPortType gGP = rp.getNextResourcePort();
		    UserJobResponse jobRP = gGP.userJob(job);
		    if (!jobRP.isValid()) {
                        logger.debug("Falkon: submission failed");
			throw new RuntimeException("submission failed!");
		    }
                    else
                    {
                        logger.debug("Falkon: submission succeeded!");

                    }
		    i += num_execs;
		}
                logger.debug("Falkon: sleep " + POLL_STEP + " ms...");
		Thread.sleep(POLL_STEP);
            } catch (Exception e) {
		setStatus(execs);
                logger.debug("Falkon: setStatus(execs)");
		e.printStackTrace();
            } 
        }
    }

    public void setStatus (Executable execs[]) {
	try {
	    for (int i=0; i<execs.length; i++) {
		Task task = rp.removeTask(execs[i].getId());
		task.setStatus(Status.FAILED);
                logger.debug("Falkon: setStatus(Status.FAILED): " + i);
	    }
	} catch (Exception e) {
	    //no-op
	    e.printStackTrace();
	}
	//this.exit = true;
    }
}
