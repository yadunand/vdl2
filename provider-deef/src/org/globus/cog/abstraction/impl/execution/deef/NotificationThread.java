//----------------------------------------------------------------------
//This code is developed as part of the Java CoG Kit project
//The terms of the license can be found at http://www.cogkit.org/license
//This message may not be removed or altered.
//----------------------------------------------------------------------

package org.globus.cog.abstraction.impl.execution.deef;

import java.util.Map;
import java.util.List;

import org.apache.log4j.Logger;
import org.globus.cog.abstraction.interfaces.Status;
import org.globus.cog.abstraction.interfaces.Task;

import org.globus.GenericPortal.common.Notification;

public class NotificationThread implements Runnable {
    static Logger logger = Logger.getLogger(NotificationThread.class
					    .getName());
    private Map tasks = null;
    private Notification userNot = null;
    private List completedQueue = null;

    private boolean exit = false;

    private final int POLL_STEP = 10;
    private final int MAX_ERR = 20;
    private int count = 0;

    public NotificationThread(Map tasks, Notification userNot, List completedQueue){
	this.tasks = tasks;    
	this.userNot = userNot;
	this.completedQueue = completedQueue;
    }
    
    public void setExit(boolean exit) {
	this.exit = exit;
    }

    public void run() {
        while(!exit) {
            try {
		if (tasks != null && tasks.size() > 0) {
                    logger.debug("Falkon: waiting for notifications...");
		    //String res = userNot.recvString();
                    String res[] = userNot.recvStrings();

                    if (res != null)
                    {
                    
                    for (int i=0;i<res.length;i++)
                    {
                        logger.debug("Falkon: notification["+i+"]: " + res[i]);
                        completedQueue.add(res[i]);
                    }

                    if (res.length <= 0)
                    {
                        logger.debug("Falkon: notification: res.length = " + res.length);

                    }

		    
                    }
                    else
                    {
                        logger.debug("Falkon: notification: null");

                    }
		    /*
		    String[] toks = res.split(" ");
		    if (toks.length == 2) { 
			String jobID = toks[0];
			Task task = null;
			synchronized (tasks) {
			    task = (Task) tasks.remove(jobID);
			}
			if (task == null) {
			    //missing in the map? ignore
			    continue;
			}
			if (toks[1].equals("0")) {
			    completedQueue.add(task);
			    //task.setStatus(Status.COMPLETED);
			} else{
			    task.setStatus(Status.FAILED);
			}
		    }
		    */
		} 
		//Thread.sleep(POLL_STEP);
            } catch (Exception e) {
		//should count number of exceptions to exit
		count++;
		e.printStackTrace();
                logger.debug("Falkon: error in NotificationThread #: " + count, e);
		if (count >= MAX_ERR) {
		    userNot.destroy();
		    exit = true;
                    logger.debug("Falkon: ready to exit NotificationThread...");
		}
            }
            logger.debug("Falkon: not ready to exit NotificationThread yet......");

        }
    }
}
